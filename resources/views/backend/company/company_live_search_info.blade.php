<table class="table table-hover table-bordered">
    <thead class="table-header-bg">
        <tr>
            <th>S/N</th>
            <th>Company Name</th>
            <!-- <th>Vendor Code</th> -->
            <th style="width:180px;">Address</th>
            <th style="width:180px;">State</th>
            <th>Phone Number</th>
            <th>Email Address</th>
            <!-- <th>Vat/ Tin No</th> -->
            <th>CEO/ CTO</th>
            <th>Action</th>
        </tr>
    </thead>
    
    <tbody>
        
            <?php $sl = 1; ?>
            @foreach ( $company_info as $info )                                      
                    <?php
                        $class = ( $sl%2 == 0 ) ? 'table-background-color' : 'table-background';
                    ?> 
                    <tr class="{{ $class }}">
                        <td>{{ $sl++ }}</td>
                        <td> {{ $info->company_name }} </td>
                        <!-- <td> {{ $info->vendor }} </td> -->
                        <td> {{ $info->address }} </td>
                        <td> {{ $info->state }} </td>
                        <td> {{ $info->phone }} </td>
                        <td> {{ $info->email }} </td>
                        <!-- <td> {{ $info->tin_no }} </td> -->
                        <td> {{ $info->ceo }} </td>
                        <td>
                           <!-- <a href="{{ URL::to( '/edit-company-info/'. $info->id ) }}" class="glyphicon glyphicon-pencil"> </a> |
                            -->
                            <a href="{{ URL::to( '/destroy-company-info/'. $info->id ) }}" id="confirm-delete" class="confirm-admin-delete glyphicon glyphicon-trash" onclick="return confirmDelete();"> </a>
                        </td>
                    </tr>

            @endforeach 
        

    </tbody>
</table>