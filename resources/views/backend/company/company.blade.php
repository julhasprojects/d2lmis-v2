@extends( 'backend.index' )

@section( 'content_area' )
<div class="bg-title">
    <div class="page-title reports-title">
        <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
            <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
            <li class="completed"><a href="javascript:void(0);"> Settings </a></li>
            <li class="active">  <a href="javascript:void(0);"> New Company Profile Setup </a></li>
        </ul>

    </div>
</div>
@include('master.message')
@include('master.error')
<div id="deletenotification"></div>
<div class="row bg-newline" id="main-wrapper">
    <div class="row">
        <div class="col-md-12 left-box">
            <div class="panel panel-white">
                <div class="panel-body">
                    <h4 class="title-house">Company Information Setup</h4>
                    <ul class="nav nav-tabs">
                       
                        <li  @if (session('rdstatus')) @else class="active" @endif><a data-toggle="tab" href="#company"> Company Setup </a></li>
                        
                        <li @if (session('rdstatus'))  class="active" @endif><a data-toggle="tab" href="#branch"> Branch Setup</a></li>
                    </ul>
                    <div class="tab-content tab-advaced">
                        @if (session('rdstatus'))
                            <div id="company" class="tab-pane fade">
                        @else
                            <div id="company" class="tab-pane fade in active">
                        @endif
                            <div class="panel-body">
                                {!! Form::open(array('url'=>'company-profile','role'=>'form','enctype' => 'multipart/form-data', 'files' => true,'data-toggle'=>'validator','method'=>'match'))!!}
                                    
                                    <div class="form-box-row">
                                        <div class="col-md-6 col-xs-12 col-lg-6 company-column">
                                            <div class="form-group">
                                                <label for="compnay_name" class="col-md-4 control-label">Company Name <span class="required">*</span></label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="companyname" id="companyname"  maxlength="25" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-lg-6 company-column">
                                            <div class="form-group">
                                                <label for="vendor" class="col-md-4 control-label">Vendor Code</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="vendor" id="vendor"  maxlength="20" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-box-row">
                                        <div class="col-md-6 col-xs-12 col-lg-6 company-column">
                                            <div class="form-group">
                                                <label for="address" class="col-md-4 control-label">Address <span class="required">*</span></label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="address" id="address">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-lg-6 company-column">
                                            <div class="form-group">
                                                <label for="state" class="col-md-4 control-label">State</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="state" id="state">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-box-row">
                                        <div class="col-md-6 col-xs-12 col-lg-6 company-column">
                                            <div class="form-group">
                                                <label for="phone" class="col-md-4 control-label">Phone Number <span class="required">*</span></label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control phonevalidation"  oninput="checkNumberFieldLength(this);"  maxlength="11" name="phone" id="phone">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-lg-6 company-column">
                                            <div class="form-group">
                                                <label for="fax" class="col-md-4 control-label">Fax No</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="fax" id="fax">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-box-row">
                                        <div class="col-md-6 col-xs-12 col-lg-6 company-column">
                                            <div class="form-group">
                                                <label for="email" class="col-md-4 control-label">Email Address</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="email" id="email">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-lg-6 company-column">
                                            <div class="form-group">
                                                <label for="tin_no" class="col-md-4 control-label">Vat/ Tin No</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="tin_no" id="tin_no">
                                                </div>
                                            </div>
                                        </div>
                                    </div>  

                                    <div class="form-box-row">
                                        <div class="col-md-6 col-xs-12 col-lg-6 company-column">
                                            <div class="form-group">
                                                <label for="email" class="col-md-4 control-label">Company Website</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="company_website" id="company_website">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-lg-6 company-column">
                                            <div class="form-group">
                                                <label for="tin_no" class="col-md-4 control-label">Copyright Text</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="copyright_text" id="copyright_text">
                                                </div>
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="form-box-row">
                                        <div class="col-md-6 col-xs-12 col-lg-6 company-column">
                                            <div class="form-group">
                                                <label for="Company Shortname" class="col-md-4 control-label">Company Shortname</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="company_shortname" id="company_shortname">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-lg-6 company-column">
                                            <div class="form-group">
                                                <label for="Facebook Id" class="col-md-4 control-label"> Facebook Id </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="facebook_id" id="facebook_id">
                                                </div>
                                            </div>
                                        </div>
                                    </div>    

                                    <div class="form-box-row">
                                        <div class="col-md-6 col-xs-12 col-lg-6 company-column">
                                            <div class="form-group">
                                                <label for="Twitter" class="col-md-4 control-label">Twitter</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="twitter" id="twitter">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-lg-6 company-column">
                                            <div class="form-group">
                                                <label for="linkedin" class="col-md-4 control-label"> LinkedIn </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="linkedin" id="linkedin">   
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        <div class="col-md-6 col-xs-12 col-lg-6 company-column">
                                            <div class="form-group">
                                                <label for="google_plus" class="col-md-4 control-label"> Google +</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="google_plus" id="google_plus">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-lg-6 ">
                                            <div class="col-md-8">
                                                 <div class="form-group">
                                                    <label for="inputPassword3" class="col-md-4 control-label">Company Logo</label>
                                                    <div class="col-md-8">
                                                        <label class="control-label col-md-3">Photo</label>
                                                        <div class="col-md-9">
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                                                </div>
                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                                                </div>
                                                                <div>
                                                                    <span class="btn default btn-file">
                                                                       <span class="fileinput-new"> Select image </span>
                                                                       <span class="fileinput-exists"> Change </span>
                                                                       <input type="file" name="profileImage">
                                                                    </span>
                                                                    <a href="#" class="btn red fileinput-exists" data-dismiss="fileinput">
                                                                    Remove </a>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="clearfix margin-top-10">
                                                                <span class="label label-danger"> NOTE! </span> Image Size must be (872px by 724px)
                                                            </div> 
                                                        </div>
                                                    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn submit-button-bg">Create</button>
                                        </div>
                                    </div>
                                {!!  Form::close() !!}
                            </div>
                            <br>
                            <div class="panel panel-white">
                                <h4 class="title-house">Company Information</h4>
                                <div class="panel-body">
                                    <div class="panel-heading clearfix padding-left-box">
                                        <div class="col-md-4 padding-left-zero">
                                            <div class="form-group">                        
                                                <input type="text" class="form-control" id="cpcompanyinfo" placeholder="Find Information">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <div id="company_info_result">
                                                <table class="table table-hover table-bordered">
                                                    <thead class="table-header-bg">
                                                        <tr>
                                                            <th>S/N</th>
                                                            <th>Company Name</th>
                                                            <th style="width:180px;">Address</th>
                                                            <th style="width:180px;">State</th>
                                                            <th>Phone Number</th>
                                                            <th>Email Address</th>
                                                            <th>CEO/ CTO</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                     {{ csrf_field() }}
                                                    <tbody>
                                                        <?php $sl = 1; ?>
                                                        @foreach ( $company_info as $info )                                      
                                                            <?php
                                                                $class = ( $sl%2 == 0 ) ? 'table-background-color' : 'table-background';
                                                            ?> 
                                                            <tr class="{{ $class }} company_{{ $info->id }}">
                                                                <td>{{ $sl++ }}</td>
                                                                <td> {{ $info->company_name }} </td>
                                                               <!-- <td> {{ $info->vendor }} </td> -->
                                                                <td> {{ $info->address }} </td>
                                                                <td> {{ $info->state }} </td>
                                                                <td> {{ $info->phone }} </td>
                                                                <td> {{ $info->email }} </td>
                                                               <!-- <td> {{ $info->tin_no }} </td> -->
                                                                <td> {{ $info->ceo }} </td>
                                                                <td>
                                                                    <a href="{{ URL::to('/company-profile/'.$info->id.'/edit') }}" class="glyphicon glyphicon-pencil icon-edit" >  |</a> 
                                                                    <!--   {!! Form::open(['url' => '/company-profile/'.$info->id, 'files'=>true,'method' => 'delete','class' => 'from-delete']) !!}
                                                                      <button type="submit" class="delete-options"><i class="fa fa-trash-o"></i></button>
                                                                    {!! Form::close() !!} -->
                                                                    <button type="submit" class="delete-options" onclick="companySetupDeleted('{{ $info->id }}','{{ $info->branch_name }}')"><i class="fa fa-trash-o"></i></button>
                                                                </td>
                                                            </tr>
                                                        @endforeach 
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if (session('rdstatus'))
                            <div id="branch" class="tab-pane fade in {{ session('rdstatus') }}">
                        @else
                            <div id="branch" class="tab-pane fade">
                        @endif
                            <div class="panel-body">
                                {!! Form::open(array('url'=>'branch/setup','role'=>'form','data-toggle'=>'validator','method'=>'match'))!!}
                                    <div class="form-box-row">
                                        <div class="col-md-6 col-xs-12 col-lg-6 company-column">
                                            <div class="form-group">
                                                <label for="branchname" class="col-md-4 control-label">Branch Name <span class="required">*</span>:</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="branchname" id="branchname">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-lg-6 company-column">
                                            <div class="form-group">
                                                <label for="compnay_name" class="col-md-4 control-label">Branch Address <span class="required">*</span>:</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="branchaddress" id="branchaddress">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn submit-button-bg">Create</button>
                                        </div>
                                    </div>
                            
                                {!!  Form::close() !!}
                            </div>    
                            <div class="panel-body">
                                <div class="panel">
                                    <h4 class="basic-information"> Branch Information </h4>
                                    <div class="panel-body">
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered" id="table-responsive">
                                                    <thead class="table-header-bg">
                                                        <tr>
                                                            <th>S/N</th>
                                                            <th>Branch Name</th>
                                                            <th>Address</th>
                                                            <th>Created at</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    {{ csrf_field() }}
                                                    <tbody>
                                                        <?php $sl = 1; ?>
                                                        @foreach ( $branchinfo as $info )                                      
                                                            <?php
                                                                $class = ( $sl%2 == 0 ) ? 'table-background-color' : 'table-background';
                                                            ?> 
                                                            <tr class="{{ $class }} branch_{{ $info->id }}" >
                                                                <td> {{ $sl++ }} </td>
                                                                <td> {{ $info->branch_name }} </td>
                                                                <td> {{ $info->branch_address }} </td>
                                                                <td> {{ $info->created_at }} </td>
                                                                <td>
                                                                    <a href="{{ URL::to('/branch/setup/'.$info->id.'/edit') }}" class="glyphicon glyphicon-pencil icon-edit" >  |</a> 
                                                                    <!-- {!! Form::open(['url' => '/branch/setup/'.$info->id, 'files'=>true,'method' => 'delete','class' => 'from-delete']) !!} -->
                                                                        <button type="submit" class="delete-options" onclick="branchSetupDeleted('{{ $info->id }}','{{ $info->branch_name }}')"><i class="fa fa-trash-o"></i></button>
                                                                    <!-- {!! Form::close() !!} -->
                                                                </td>
                                                            </tr>
                                                        @endforeach 
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>       
                        </div>
                    </div>
                </div>
            </div>
        </div>
                
    </div>
</div>
@include('master.delete')
@stop