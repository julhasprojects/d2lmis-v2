@extends( 'backend.index' )
@section( 'content_area' )
<div class="bg-title">
    <div class="page-title reports-title">
        <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
            <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
            <li class="completed"><a href="javascript:void(0);"> Settings </a></li>
            <li class="active"> <a href="javascript:void(0);"> Update Company Profile </a></li>
        </ul>
    </div>
</div>
 
    @include( 'master.message' )
    @include( 'master.error' )
    <div class="row column-box">
        <div class="col-md-12 left-box">
            <div class="panel panel-white">
                <h4 class="title-house">Update Company Information</h4>
                <div class="panel-body">
                    {!! Form::open(['url' => 'company-profile/'.$company->id, 'enctype' => 'multipart/form-data',  'data-toggle'=>'validator', 'files'=>true,'method' => 'PUT']) !!} 
                    <div class="form-box-row">
                        <div class="col-md-6 col-xs-12 col-lg-6">
                            <div class="form-group">
                                <label for="inputPassword3" class="col-md-4 control-label">Company Name <span class="required">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="companyname" id="companyname" value="{{ $company->company_name }}">
                                    <input type="hidden" class="form-control" name="id" id="id" value="{{ $company->id }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-lg-6">
                            <div class="form-group">
                                <label for="inputPassword3" class="col-md-4 control-label">Vendor Code</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="vendor" id="vendor" value="{{ $company->vendor }}" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-box-row">
                        <div class="col-md-6 col-xs-12 col-lg-6">
                            <div class="form-group">
                                <label for="inputPassword3" class="col-md-4 control-label">Address <span class="required">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="address" id="address" value="{{ $company->address }}" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-lg-6">
                            <div class="form-group">
                                <label for="inputPassword3" class="col-md-4 control-label">State</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="state" id="state" value="{{ $company->state }}" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-box-row">
                        <div class="col-md-6 col-xs-12 col-lg-6">
                            <div class="form-group">
                                <label for="inputPassword3" class="col-md-4 control-label">Phone Number <span class="required">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control phonevalidation"  oninput="checkNumberFieldLength(this);"  maxlength="11" name="phone" id="phone" value="{{ $company->phone }}" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-lg-6">
                            <div class="form-group">
                                <label for="inputPassword3" class="col-md-4 control-label">Fax No</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="fax" id="fax" value="{{ $company->fax }}" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-box-row">
                        <div class="col-md-6 col-xs-12 col-lg-6">
                            <div class="form-group">
                                <label for="inputPassword3" class="col-md-4 control-label">Email Address</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="email" id="email" value="{{ $company->email }}" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-lg-6">
                            <div class="form-group">
                                <label for="inputPassword3" class="col-md-4 control-label">Vat/ Tin No</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="tin_no" id="tin_no" value="{{ $company->tin_no }}" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-box-row">
                        <div class="col-md-6 col-xs-12 col-lg-6">
                            <div class="form-group">
                                <label for="inputPassword3" class="col-md-4 control-label">Tax</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="tax" id="tax" value="{{ $company->tax }}" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-lg-6">
                            <div class="form-group">
                                <label for="inputPassword3" class="col-md-4 control-label">CEO/ CTO</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="ceo" id="ceo" value="{{ $company->ceo }}" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-box-row">
                        <div class="col-md-6 col-xs-12 col-lg-6">
                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">Company Website</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="company_website" id="company_website" value="{{ $company->company_website }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-lg-6">
                            <div class="form-group">
                                <label for="tin_no" class="col-md-4 control-label">Copyright Text</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="copyright_text" id="copyright_text" value="{{ $company->copyright_text }}">
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="form-box-row">
                        <div class="col-md-6 col-xs-12 col-lg-6">
                            <div class="form-group">
                                <label for="Company Shortname" class="col-md-4 control-label">Company Shortname</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="company_shortname" id="company_shortname" value="{{ $company->company_shortname }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-lg-6">
                            <div class="form-group">
                                <label for="Facebook Id" class="col-md-4 control-label"> Facebook Id </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="facebook_id" id="facebook_id" value="{{ $company->facebook_id }}">
                                </div>
                            </div>
                        </div>
                    </div>    
                    <div class="form-box-row">
                        <div class="col-md-6 col-xs-12 col-lg-6">
                            <div class="form-group">
                                <label for="Twitter" class="col-md-4 control-label">Twitter</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="twitter" id="twitter" value="{{ $company->twitter }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-lg-6">
                            <div class="form-group">
                                <label for="linkedin" class="col-md-4 control-label"> LinkedIn </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="linkedin" id="linkedin" value="{{ $company->linkedin }}">   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-box-row">
                        <div class="col-md-6 col-xs-12 col-lg-6">
                            <div class="form-group">
                                <label for="google_plus" class="col-md-4 control-label"> Google +</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="google_plus" id="google_plus" value="{{ $company->google_plus }}">
                                </div>
                            </div>
                        </div>
                       
                        <div class="col-md-6 col-xs-12 col-lg-6">
                            <div class="form-group">
                                <label for="inputPassword3" class="col-md-4 control-label">Commence of Business</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="comence_bness" id="comence_bness" value="{{ $company->comence_bness }}" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12 col-lg-6">
                            <div class="form-group">
                                <label for="inputPassword3" class="col-md-4 control-label">Company Logo</label>
                                <div class="col-md-8">
                                     <label class="control-label col-md-3">Photo</label>
                                <div class="col-md-9">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                             @if(!empty($company->logo))
                                                <img src="{{ URL::asset( 'assets/images' .'/'. $company->logo) }}" alt=""/>
                                            @else
                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                            @endif
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                        </div>
                                        <div>
                                            <span class="btn default btn-file">
                                               <span class="fileinput-new"> Select image </span>
                                               <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="profileImage">
                                            </span>
                                            <a href="#" class="btn red fileinput-exists" data-dismiss="fileinput">
                                            Remove </a>
                                        </div>
                                    </div>
                                    
                                    <div class="clearfix margin-top-10">
                                        <span class="label label-danger"> NOTE! </span> Image Size must be (872px by 724px)
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="hiddenimage" value="{{ $company->logo }}">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn submit-button-bg">Update</button>
                        </div>
                    </div>
                    {!!  Form::close() !!}
                </div>
            </div>
        </div>
                
    </div>


@stop