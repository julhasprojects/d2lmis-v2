@extends( 'backend.index' )
@section( 'content_area' )
<div class="bg-title">
    <div class="page-title reports-title">
        <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
            <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
            <li class="completed"><a href="javascript:void(0);"> Settings </a></li>
            <li class="active"> <a href="javascript:void(0);"> Update Branch Information </a></li>
        </ul>
    </div>
</div>
    
    @include('master.message')
    @include('master.error')
    <div class="row column-box" id="main-wrapper">
        <div class="col-md-12 left-box">
            <div class="panel panel-white">
                <h4 class="title-house">Update Branch Information</h4>
                <div class="panel-body">
                    {!! Form::open(['url' => '/branch/setup/'.$branchInfo->id, 'files'=>true,'method' => 'PUT']) !!} 
                        <div class="form-box-row">
                            <div class="col-md-6 col-xs-12 col-lg-6 company-column">
                                <div class="form-group">
                                    <label for="branchname" class="col-md-4 control-label">Branch Name <span class="required">*</span>:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="branchname" id="branchname" value="{{ $branchInfo->branch_name }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-lg-6 company-column">
                                <div class="form-group">
                                    <label for="compnay_name" class="col-md-4 control-label">Branch Address <span class="required">*</span>:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="branchaddress" id="branchaddress"  value="{{ $branchInfo->branch_address }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn submit-button-bg">Update</button>
                            </div>
                        </div>
                    {!!  Form::close() !!}
                </div>
            </div>
        </div>
                
    </div>


@stop