@extends( 'backend.index' )

@section( 'content_area' )

	<div class="bg-title">
	    <div class="page-title reports-title">
	        <ul class="breadcrumb  col-md-5 col-xs-12 col-lg-5 col-sm-5 pull-left">
	            <li class="completed"><a href="javascript:void(0);">Dashboard </a></li>
	            <li class="completed"><a href="javascript:void(0);"> Settings </a></li>
	            <li  class="active"><a href="javascript:void(0);"> Create new user </a></li>
	        </ul>
	    </div>
	</div>

	@include('master.error')
	@include('master.message')
	<div id="ramadanmenu" class="menu-menu-1-container bottom-menubar">
		<div id="menu-button">Menu</div>
		<ul id="menu-menu-1" class="menu">
			<!-- <li><a href="{{ URL::to( '/hrm/dashboard' ) }}" title="">Dashboard</a></li> -->
			<li>
				<a href="{{ URL::to( 'role/permission/' ) }}" title="Master cc collection">
					<span>
					   	<span class="title">View User Role Assign </span> 
				   	</span>
			   </a>
			</li>
			
			
		</ul>
	</div>	
	{{ csrf_field() }}
	<div id="main-wrapper" class="main-admin-information">
		<div class="panel panel-body">
			<ul class="nav nav-tabs">
			  <li class="active"><a data-toggle="tab" href="#active">Active users</a></li>
			  <li><a data-toggle="tab" href="#deactive"> Deactivated Users </a></li>
			</ul>
			<div class="tab-content">
				<div id="active" class="tab-pane fade in active">
					<div class="admin-active-information">
						<div class="panel panel-white">
							<h4 class="title-house">  Create new user </h4>
							
							<div class="panel-body">
								<!-- {!!  Form::open( array( 'url' => 'add-new-admin', 'role'=>'form', ' data-toggle'=>'validator', 'method' => 'POST' ) ) !!} -->
									<div>
										<div class="form-group  col-md-6 col-xs-12 col-lg-6 col-sm-6 pull-left">
											<div class="form-group">
												<label for="inputPassword3" class="col-md-4 control-label">Employee Name </label>
												<div class="col-md-8">
													<select name="" id="beadminempresult-live-change" class="chosen-select-width">
														@foreach($employee as $employeeInformation)
															<option value="{{ $employeeInformation->employeeID }}"> {{ $employeeInformation->fullName }}</option>
														@endforeach
													</select>
												</div>
											</div>
										</div>
										<div id='beadminempresult-live-change-result'> </div>
									</div>
									<br/>
								
							</div>
						</div>
			
					</div>	
					<div class="admin-active-information">
						<div class="panel panel-white">
							<div class="panel-body">
								<h4 class="title-house all-user-list-title">All Users List</h4>
								<div class="col-md-4 padding-left-zero">
									<div class="form-group">						
										<input type="text" class="form-control" id="aaemployeeinfo" placeholder="Find user information">
									</div>
								</div>
							</div>
							<hr>
							<div class="panel-body">
								<div class="table-responsive">
									<div id="aaadminsearchresult">
										<table class="table table-hover table-bordered">
											<thead class="table-header-bg">
												<tr>
													<th>S/N</th>
													<th>User ID</th>
													<th>Full Name</th>
													<th>UserName</th>
													<th>Mobile</th>
													<th>Email</th>
													<th>Valid From</th>
													<th>Valid To</th>
													<th>Created Date</th>
													<th>Status</th>
													<th>Action</th>
													<th>Assign Role</th>
												</tr>
											</thead>
											<tbody>
											<?php $sl = 1; ?>
											@foreach ( $admin_list as $admin )						 				
				                               <?php
				                                    $class = ( $sl%2 == 0 ) ? 'table-background-color' : 'table-background';

				                                ?> 
												<tr class="user_list_{{ $admin->emp_id }} {{ $class }}">
													<th> {{ $sl++ }} </th>
													<td> {{ $admin->emp_id }} </td>
													<td> {{ $admin->name }} </td>									
													<td> {{ $admin->username }} </td>
													<td> {{ $admin->mobile }} </td>
													<td> {{ $admin->email }} </td>
													<td> {{ $admin->valid_form }} </td>
													<td> {{ $admin->valid_to }} </td>
													<td> {{ $admin->created_at }} </td>
													<td><button class="btn btn-xs btn-success"> Active </button></td>
													<td>
														<!-- {!! Form::open(['url' => '/admin-information/'.$admin->emp_id, 'files'=>true,'method' => 'delete','class' => 'from-delete']) !!} -->
		                                    			    <button type="submit" class="delete-options"  onclick="adminDestroy('{{ $admin->emp_id }}','{{ $admin->name }}')"><i class="fa fa-trash-o"></i></button>
		                                    			<!-- {!! Form::close() !!}   -->
														<a href="{{ URL::to('/admin-information/'.$admin->emp_id.'/edit') }}" alt="edit" class="confirm-admin-delete glyphicon icon-pencil"> </a> 
													</td>
													<td>
														<a href="{{ URL::to('/user/role/'.$admin->id.'/edit') }}" class="btn btn-warning"> Click Here </a>
													</td>
												</tr>
											@endforeach	
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>		
				</div>
				<div id="deactive" class="tab-pane fade in">
					<div class="table-responsive">
						<div id="aaadminsearchresult">
							<table class="table table-hover table-bordered">
								<thead class="table-header-bg">
									<tr>
										<th>S/N</th>
										<th>User ID</th>
										<th>Full Name</th>
										<th>UserName</th>
										<th>Mobile</th>
										<th>Email</th>
										<th>Valid From</th>
										<th>Valid To</th>
										<th>Created Date</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								<?php $sl = 1; ?>
								@foreach ( $deactiveClient as $admin )						 				
	                               <?php
	                                    $class = ( $sl%2 == 0 ) ? 'table-background-color' : 'table-background';

	                                ?> 
									<tr class="{{ $class }}">
										<th> {{ $sl++ }} </th>
										<td> {{ $admin->emp_id }} </td>
										<td> {{ $admin->name }} </td>									
										<td> {{ $admin->username }} </td>
										<td> {{ $admin->mobile }} </td>
										<td> {{ $admin->email }} </td>
										<td> {{ $admin->valid_form }} </td>
										<td> {{ $admin->valid_to }} </td>
										<td> {{ $admin->created_at }} </td>
										<td> <button class="btn btn-xs btn-danger"> Deactive </button> </td>
										<td>
											 
											<a href="{{ URL::to('/admin-information/'.$admin->emp_id.'/edit') }}" alt="edit" class="confirm-admin-delete glyphicon icon-pencil"> </a> 
										</td>
									</tr>
								@endforeach	
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>

@include('master.delete')
@stop