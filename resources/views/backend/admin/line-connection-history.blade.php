@extends( 'backend.index' )

@section( 'content_area' )

<div class="row">
   <div class="col-lg-12 col-md-12">
		<div class="panel panel-white">
			<div class="vai-client-total shadow">
				<div class="panel-heading clearfix">
	                <div class="col-md-3 padding-left-zero">
	                    <div class="form-group">                        
	                        <a class="btn btn-primary" href="{{ URL::to('#') }}"> Export All Connection History </a>
	                    </div>
	                </div>
	                 <div class="col-md-4">
	                    <div class="form-group">                        
	                        <div class="col-md-10">
	                            <input type="text" class="form-control" id="linehistorySrearchkey" placeholder="Search by id/ username/ fullname">
	                        </div>
	                    </div>
	                </div>
	                <div class="col-md-4">
	                    <div class="form-group">                        
	                        <div class="col-md-10">
	                            <select class="form-control" name="lineconnectiontchangedtechnician" id='lineconnectiontchangedtechnician' required>
		                            <option value="">Changed By Technican</option>
		                            @foreach($employee as $emp) 
		                                <option value="{{ $emp->employeeID }}">{{ $emp->fullName }}</option>
		                            @endforeach
	                        	</select>
	                        </div>
	                    </div>
	                </div>
	            </div>    
				<div class="panel-heading">
					<center><h4 class="panel-title white-color">Line Connection History</h4> </center>
				</div>
			</div>

			<div class="panel-body">
				<div class="table-responsive project-stats">  
					<div id="line-disconnect-report-search-result">
						  <table class="table table-hover table-bordered">
								<thead class="table-header-bg">
									<tr>
	                                <th>S/N</th>
	                                <th>Client Id</th>
	                                <th>Full Name</th>
	                                <th>Phone</th>
	                                <th>Address</th>
	                                <th>Assigned By</th>
	                                <th>Connection By</th>
	                                <th>Connection Date</th>
	                            </tr>
						</thead>
						<tbody>
							 <?php $sl = 1; ?> 
	                            @foreach ( $newlineRequest as $line )
	                                <?php
	                                    $class = ( $sl%2 == 0 ) ? 'table-background-color' : 'table-background';
	                                ?>                               
	                            <tr class="{{ $class }}">
	                                <th scope="row">{{ $sl++ }}</th>
	                                <th scope="row">{{ $line->newconid }}</th>
	                                <td>{{ $line->firstname.' '.$line->lastname }}</td>
	                                <td>{{ $line->mobilenumber}}</td>
	                                <td>{{ 'R-'. $line->road. ', H-'. $line->house.', F-'. $line->flat_no.', A-'.$line->region_name }}</td>
	                                <td>
				                        <?php
	                                        foreach ($employee as $info) {
	                                        if($info->employeeID == $line->emp_id) {
	                                            ?>
	                                     {{ $info->fullName }} 
	                                    <?php            
	                                        }
	                                    }    
	                                    ?>
	                                </td>
	                                <td>
				                        <?php
	                                        foreach ($employee as $info) {
	                                        if($info->employeeID == $line->accepted_by) {
	                                            ?>
	                                     {{ $info->fullName }} 
	                                    <?php            
	                                        }
	                                    }    
	                                    ?>
	                                </td>
									<td>{{ date("d M Y, h:i A", strtotime($line->created_at) ) }}</td>
	                           </tr>
							@endforeach 
						</tbody>                        
					</table>
					{!! str_replace('/?', '?',$newlineRequest->render()); !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop;