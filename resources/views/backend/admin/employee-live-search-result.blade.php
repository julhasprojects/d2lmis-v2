<table class="table table-hover table-bordered">
    <thead class="table-header-bg">
        <tr>
            <th>S/N</th>
            <th>Employee ID</th>
            <th>Firstname</th>
            <th>Mobile No</th>
            <th>Email Address</th>
        </tr>
    </thead>
    <tbody>
        <?php $sl = 1; ?>
        @foreach ( $employee as $info )
                <?php
                    $class = ( $sl%2 == 0 ) ? 'table-background-color' : 'table-background';
                ?> 
                <tr class="{{ $class }}">
                <th> {{ $sl++ }} </th>
                <td> {{ $info->employeeID }} </td>
                <td> {{ $info->fullName }} </td>                                      
                <td> {{ $info->mobileNumber }} </td>
                <td> {{ $info->email }} </td>
            </tr>
        @endforeach 
    </tbody>
</table>