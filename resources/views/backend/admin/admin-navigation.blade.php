<div id="ramadanmenu" class="menu-menu-1-container bottom-menubar">
	<div id="menu-button">Menu</div>
	<ul id="menu-menu-1" class="menu">
		<li><a href="{{ URL::to( '/isperp-dashboard' ) }}">Dashboard</a></li>
		<li><a href="{{ URL::to( '/admin/log' ) }}">Admin Log</a></li>
		<li><a href="{{ URL::to( '/admin/log/summary/report' ) }}">Admin Log Summary</a></li>
		<li><a href="{{ URL::to( '/download/excel' ) }}">Btrc Report </a></li>
		<li><a href="{{ URL::to( '/master/region/reports' ) }}">Region Reports</a></li>
	</ul>
</div>