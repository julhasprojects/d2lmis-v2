@extends( 'backend.index' )

@section( 'content_area' )
<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules</a></li>
        <li class="active"><a href="javascript:void(0);">Admin Log Summary Reports</a></li>
    </ul>
</div>
@include('backend.admin.admin-navigation')
<div id="main-wrapper">
	<div class="row">
	   <div class="col-lg-12 col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading">
					<center><h4 class="panel-title">Admin Log Information</h4> </center>
				</div>
				<div class="panel-body">
					<div class="table-responsive project-stats">  
					  	<table class="table table-hover table-bordered" id="tableresponsive">
							<thead class="table-header-bg">
								<tr>
									<th>SN</th>
									<th>UserId</th>
		                            <th>FullName</th>
									<th>Number of Login</th>
									
									<th>Last Login</th>
									<th>Last Login IP</th>
								</tr>
							</thead>
							<tbody>
								<?php $sl = 1; ?>
									@foreach ($adminlog_information as $info) 					 
										<?php
							                $class = ( $sl%2 == 0 ) ? 'table-background-color' : 'table-background';
							            ?>                               
								        <tr class="{{ $class }}">				                       
					                        <td> {{ $sl++ }} </td>
					                        <td> {{ $info->user_id }} </td>
					                        <td> {{ $info->name }} </td>
		                                    <td> {{ $info->user_count }} </td>
		                                    
					                        <td> {{ $info->login_date }} </td>
					                        <td> {{ $info->system_ip }} </td>
					                    </tr>
									@endforeach
	                       </tbody>									
						</table>
						{!! str_replace('/?', '?',$adminlog_information->render()); !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop;