 <!-- Datepicker -->	
 <script src="{{ URL::asset( 'assets/plugins/jquery/jquery-2.1.3.min.js' ) }}"></script>	
    <link href="{{ URL::asset( 'assets/css/datepicker.css' ) }}" rel="stylesheet" />
      <!-- Datepicker-->
    <script src="{{ URL::asset( 'assets/js/bootstrap-datepicker.js' ) }}"></script>
    <script src="{{ URL::asset( 'assets/js/datepicker.js' ) }}"></script>
@foreach( $employee as $info )

<div class="row">
	<div class="col-md-6 col-xs-12 col-lg-6 company-column">
		<div class="form-group">
			<label for="emp_id" class="col-md-4 control-label">Employee ID</label>
			<div class="col-md-8">
				<input type="text" class="form-control" name="emp_id" id="emp_id" value="{{ $info->employeeID }}" readonly="">
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xs-12 col-lg-6 company-column">
		<div class="form-group">
			<label for="inputPassword3" class="col-md-4 control-label">Full Name</label>
			<div class="col-md-8">
				<input type="text" class="form-control" name="fullName" id="fullName" value="{{ $info->fullName }}" readonly="">
			</div>
		</div>
	</div>
	
	<div class="col-md-6 col-xs-12 col-lg-6 company-column">
		<div class="form-group">
			<label for="join_date" class="col-md-4 control-label">Date of Joining</label>
			<div class="col-md-8">
				<input type="text" class="form-control  baijoindate" name="join_date" id="join_date" value="{{ $info->joiningDate }}" readonly>
			</div>
		</div>
	</div>
	
	<div class="col-md-6 col-xs-12 col-lg-6 company-column">
		<div class="form-group">
			<label for="inputPassword3" class="col-md-4 control-label">Mobile</label>
			<div class="col-md-8">
				<input type="text" class="form-control" name="mobileNumber" id="mobileNumber"value="{{ $info->mobileNumber }}" readonly >
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xs-12 col-lg-6 company-column">
		<div class="form-group">
			<label for="inputPassword3" class="col-md-4 control-label">Email Address</label>
			<div class="col-md-8">
				<input type="text" class="form-control" name="email" id="email"value="{{ $info->email }}" readonly>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xs-12 col-lg-6 company-column">
		<div class="form-group">
			<label for="inputPassword3" class="col-md-4 control-label">Username</label>
			<div class="col-md-8">
				<input type="text" class="form-control" name="username" id="username" >
			</div>	
		</div>
	</div>
	
	<div class="col-md-6 col-xs-12 col-lg-6 company-column">
		<div class="form-group">
			<label for="password" class="col-md-4 control-label">Password</label>
			<div class="col-md-8">
				<input type="password" class="form-control" name="password" id="password">
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xs-12 col-lg-6 company-column">
		<div class="form-group">
			<label for="re_password" class="col-md-4 control-label">Re-Password</label>
			<div class="col-md-8">
				<input type="password" class="form-control" name="re_password" id="re_password">									        
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xs-12 col-lg-6 company-column">
		<div class="form-group">
			<label for="inputPassword3" class="col-md-4 control-label">Valid From</label>
			<div class="col-md-8">
				<input type="text" class="form-control baivalidfrom" name="valid_from" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd"  name="valid_from" id="valid_from" value="{{ Input::old('valid_from') }}">
			</div>
		</div>
	</div>	

	<div class="col-md-6 col-xs-12 col-lg-6 company-column">
		<div class="form-group">
			<label for="inputPassword3" class="col-md-4 control-label">Valid To</label>
			<div class="col-md-8">
				<input type="text" class="form-control baivalidto" name="valid_to" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd"  name="valid_to" id="valid_to" value="{{ Input::old('valid_to') }}">
			</div>
		</div>
	</div>

	<div class="col-md-6 col-xs-12 col-lg-6 company-column">
		<div class="form-group">
			<label for="inputPassword3" class="col-md-4 control-label">User Type</label>
			<div class="col-md-8">
				<select class="form-control" name="user_type" id="user_type" >
				    <option >Select User Type</option>
				    <option value="1">Chairman</option>
				    <option value="2">Managing Director</option>
				    <option value="3">Billing Officer</option>
				    <option value="4">Customer Care</option>
				</select>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xs-12 col-lg-6 company-column">
		<div class="form-group">
			<label for="inputPassword3" class="col-md-4 control-label">Status</label>
			<div class="col-md-8">
				<select class="form-control" name="status" id="status" >
				    <option value="1">Active</option>
				    <option value="0">Inactive</option>
				</select>									
			</div>
		</div>
	</div>	
</div>	
@endforeach