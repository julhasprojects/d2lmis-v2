@extends( 'backend.index' )

@section( 'content_area' )
   <script type="text/javascript">
        $(document).ready(function(){
        $('#viewcomplaintable').DataTable({
           'paging' : true  ,
           "pageLength": 50,
           'searching' : true,
        });
        
        });

    </script>
<div class="page-title">
    <h3 class="col-md-6 col-xs-12 col-lg-6 col-sm-6"> Reseller Clients List </h3>
    <div class="btn-group col-md-6 col-xs-12 col-lg-6 col-sm-6" role="group" aria-label="Basic example">
        <div class="btn-group pull-right" role="group" aria-label="First group">
            <a class="btn btn-success" href="{{ URL::to('/export/all-newline-connection') }}">Export</a>
            <button type="button" class="btn btn-info"  href="javascript:void()" onClick="window.print()">Print</button>
            <button type="button" class="btn btn-primary">PDF</button>
        </div>
    </div>
</div>
<div id="main-wrapper">
	    <table class="table table-hover table-bordered display" id="viewcomplaintable" style="width: 100%; cellspacing: 0;">
            <thead class="table-header-bg">
	        <tr>
	            <th>S/N</th>
	            <th>ID</th>
	            <th>Name</th>
	            <th>User Name</th>
	            <th>Address</th>
	            <th>Mobile</th>
	           
	        </tr>
	    </thead>
	    <tbody>
	        <?php $sl = 1; ?>
	   		 @foreach ( $resellerClients as $resellerClientsdata )
						<?php
	                        $class = ( $sl%2 == 0 ) ? 'table-background-color' : 'table-background';
	                    ?> 
		        <tr class="{{$class}}">
		            <td>{{ $sl++ }}</td>
		            <td>{{ $resellerClientsdata->newconid }}</td>
		            <td>{{ $resellerClientsdata->firstname . $resellerClientsdata->lastname }}</td>                                                            
		            <td>{{ $resellerClientsdata->username  }}</td>                                                            
		            <td>{{ 'R-'.$resellerClientsdata->road. ', H-'. $resellerClientsdata->house.', F-' . $resellerClientsdata->flat_no . $resellerClientsdata->block_no }}</td>
		          	<td>{{ $resellerClientsdata->mobilenumber }}</td> 
		        </tr>
	   		@endforeach 
	    </tbody>
	</table>    
</div>
@stop