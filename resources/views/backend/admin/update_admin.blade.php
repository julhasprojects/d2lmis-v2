@extends( 'backend.index' )

@section( 'content_area' )
<div class="bg-title">
    <div class="page-title reports-title">
        <ul class="breadcrumb  col-md-5 col-xs-12 col-lg-5 col-sm-5 pull-left">
            <li class="completed"><a href="javascript:void(0);">Dashboard </a></li>
            <li class="completed"><a href="javascript:void(0);"> Settings </a></li>
            <li  class="active"><a href="javascript:void(0);">Update Admin Profile</a></li>
        </ul>
    </div>
</div>
   @include('master.message')
   @include('master.error')
	<div class="row column-box">
		<div class="col-md-12 left-box">
			<div class="panel panel-white">
				<h4 class="title-house">Update User Information</h4>
				<div class="panel-body">
					{!! Form::open(['url' => '/admin-information/'.$admin_list->emp_id, 'files'=>true,'method' => 'PUT']) !!} 	
						<div class="form-box-row">
							<div class="form-group  col-md-6 col-xs-12 col-lg-6 col-sm-6 pull-left">
								<div class="form-group">
									<label for="inputPassword3" class="col-md-4 control-label">Employee Name <span class="required">*</span></label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="emp_name" id="emp_name" value="{{ $admin_list->name }}" >
										<input type="hidden" class="form-control" name="id" id="id" value="{{ $admin_list->id }}">										
									</div>
								</div>
							</div>
							<div class="form-group  col-md-6 col-xs-12 col-lg-6 col-sm-6 pull-left">
								<div class="form-group">
									<label for="inputPassword3" class="col-md-4 control-label">Employee ID <span class="required">*</span></label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="emp_id" id="emp_id" value="{{ $admin_list->emp_id }}" disabled>
									</div>
								</div>
							</div>
						</div>
						
						<div class="form-box-row">
							<div class="form-group  col-md-6 col-xs-12 col-lg-6 col-sm-6 pull-left">
								<div class="form-group">
									<label for="inputPassword3" class="col-md-4 control-label">Mobile <span class="required">*</span></label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="mobile" id="mobile" value="{{ $admin_list->mobile }}">
									</div>
								</div>
							</div>
							<div class="form-group  col-md-6 col-xs-12 col-lg-6 col-sm-6 pull-left">
								<div class="form-group">
									<label for="inputPassword3" class="col-md-4 control-label">Email Address <span class="required">*</span></label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="email" id="email" value="{{ $admin_list->email }}" readonly>
									</div>
								</div>
							</div>
						</div>
						<div class="form-box-row">
							<div class="form-group  col-md-6 col-xs-12 col-lg-6 col-sm-6 pull-left">
								<div class="form-group">
									<label for="inputPassword3" class="col-md-4 control-label">Username <span class="required">*</span></label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="username" id="username" value="{{ $admin_list->username }}" >
									</div>
								</div>
							</div>
							
							<div class="form-group  col-md-6 col-xs-12 col-lg-6 col-sm-6 pull-left">
								<div class="form-group">
									<label for="inputPassword3" class="col-md-4 control-label">New Password <span class="required">*</span></label>
									<div class="col-md-8">
										<input type="password" class="form-control" name="password" id="password">
										<input type="hidden" class="form-control" name="oldpassword" id="oldpassword" value="{{ $admin_list->password }}" >
									</div>
								</div>
							</div>
							
						</div>
						<div class="form-box-row">
							<div class="form-group  col-md-6 col-xs-12 col-lg-6 col-sm-6 pull-left">
								<div class="form-group">
									<label for="inputPassword3" class="col-md-4 control-label">ReType Password <span class="required">*</span></label>
									<div class="col-md-8">
										<input type="password" class="form-control" name="re_password" id="re_password">
									</div>
								</div>
							</div>
							<div class="form-group  col-md-6 col-xs-12 col-lg-6 col-sm-6 pull-left">
								<div class="form-group">
									<label for="inputPassword3" class="col-md-4 control-label">Valid From</label>
									<div class="col-md-8">
										<input type="text" class="form-control beavalidto" data-date-format="yyyy-mm-dd" name="valid_form" id="valid_to" value="{{ $admin_list->valid_form }}">
									</div>
								</div>
							</div>
							
							
						</div>
						<div class="form-box-row">
							<div class="form-group  col-md-6 col-xs-12 col-lg-6 col-sm-6 pull-left">
								<div class="form-group">
									<label for="inputPassword3" class="col-md-4 control-label">Valid To</label>
									<div class="col-md-8">
										<input type="text" class="form-control beavalidto" data-date-format="yyyy-mm-dd" name="valid_to" id="valid_to" value="{{ $admin_list->valid_to }}">
									</div>
								</div>
							</div>
						
						</div>	
						<div class="form-box-row">
							<div class="form-group  col-md-6 col-xs-12 col-lg-6 col-sm-6 pull-left">
								<div class="form-group">
									<label for="inputPassword3" class="col-md-4 control-label">Status</label>
									<div class="col-md-8">
										
										<select class="form-control" name="status" id="status" >
											
											<option >Select One</option>
										    <option value="1" @if ($admin_list->status == 1) selected @endif>Active</option>
										    <option value="0"  @if ($admin_list->status == 0) selected @endif>Inactive</option>
										</select>									
									</div>
									
								</div>
							</div>
						
						</div>
						
						<div class="form-group">
							<center class="col-sm-12">
								<button type="submit" class="btn btn-success">Update</button>
							</center>
						</div>
					</div>
			</div>
		</div>
				
	</div>
@stop;