<table class="table table-hover table-bordered">
			<thead class="table-header-bg">
				<tr>
                <th>S/N</th>
                <th>Client Id</th>
                <th>Full Name</th>
                <th>Phone</th>
                <th>Address</th>
                <th>Assigned By</th>
                <th>Connection By</th>
                <th>Connection Date</th>
            </tr>
	</thead>
	<tbody>
		 <?php $sl = 1; ?> 
		 @if ( count($newlineRequest) > 0 )
            @foreach ( $newlineRequest as $line )
                <?php
                    $class = ( $sl%2 == 0 ) ? 'table-background-color' : 'table-background';
                ?>                               
            <tr class="{{ $class }}">
                <th scope="row">{{ $sl++ }}</th>
                <th scope="row">{{ $line->newconid }}</th>
                <td>{{ $line->firstname.' '.$line->lastname }}</td>
                <td>{{ $line->mobilenumber}}</td>
                <td>{{ 'R-'. $line->road. ', H-'. $line->house.', F-'. $line->flat_no.', A-'.$line->region_name }}</td>
                <td>
                    <?php
                        foreach ($employee as $info) {
                        if($info->employeeID == $line->emp_id) {
                            ?>
                     {{ $info->fullName }} 
                    <?php            
                        }
                    }    
                    ?>
                </td>
                <td>
                    <?php
                        foreach ($employee as $info) {
                        if($info->employeeID == $line->accepted_by) {
                            ?>
                     {{ $info->fullName }} 
                    <?php            
                        }
                    }    
                    ?>
                </td>
				<td>{{ date("d M Y, h:i A", strtotime($line->created_at) ) }}</td>
           </tr>
		@endforeach 
		@else
            <tr> <td colspan="10"> <center>No result found </center></td> </tr> 
        @endif
	</tbody>                        
</table>