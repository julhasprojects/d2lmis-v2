<table class="table table-hover table-bordered">
    <thead class="table-header-bg">
        <tr>
            <th>S/N</th>
            <th>Admin ID</th>
            <th>Full Name</th>
            <th>UserName</th>
            <th>Mobile</th>
            <th>Email</th>
            <th>Valid From</th>
            <th>Valid To</th>
            <th>Created Date</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    <?php $sl = 1; ?>
    @foreach ( $admin_list as $admin )                                      
        <?php
            $class = ( $sl%2 == 0 ) ? 'table-background-color' : 'table-background';
        ?> 
        <tr class="{{ $class }}">
            <th> {{ $sl++ }} </th>
            <td> {{ $admin->emp_id }} </td>
            <td> {{ $admin->name }} </td>                                   
            <td> {{ $admin->username }} </td>
            <td> {{ $admin->mobile }} </td>
            <td> {{ $admin->email }} </td>
            <td> {{ $admin->valid_form }} </td>
            <td> {{ $admin->valid_to }} </td>
            <td> {{ $admin->created_at }} </td>
            <td>

            <!--    <a href="{{ URL::to( '/edit-admin/'. $admin->emp_id ) }}" class="glyphicon glyphicon-pencil"> </a> | -->
                {!! Form::open(['url' => '/admin-information/'.$admin->emp_id, 'files'=>true,'method' => 'delete','class' => 'from-delete']) !!}
                    <button type="submit" class="delete-options"><i class="fa fa-trash-o"></i></button>
                {!! Form::close() !!}  
                <a href="{{ URL::to('/admin-information/'.$admin->emp_id.'/edit') }}" alt="edit" class="confirm-admin-delete glyphicon icon-pencil"> </a> 
            </td>
        </tr>

    @endforeach 

    </tbody>

</table>