@extends( 'backend.index' )

@section( 'content_area' )

<div class="page-title">
    <h3>Employee Based All Reports Combinations</h3>
    <!--
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="active">Employee Based All Reports Combinations </li>
        </ol>
    </div>
    -->
</div>

<div id="main-wrapper">
    <h3 class="btn-success" style="text-align:center;font-size:14px; color:#fff;">
        {!! Session::get( 'message' ) !!}   
    </h3>
<div class="row">
<div class="col-md-12">
    <div class="panel panel-white">
            <div class="panel-heading clearfix">
              
                <div class="col-md-3">
                    <div class="form-group">                        
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="individual-employee-report" placeholder="Searck By Keyword">
                        </div>
                    </div>
                </div>
            
                <div class="col-md-3">
                    <div class="form-group">
                        
                        <div class="col-md-10">
                        	<select class="form-control" name="findEmployeeByGender" id="findEmployeeByGender">
                        		<option value="">Search By Gender</option>
                        		<option value="male">Male</option>
                        		<option value="female">Female</option>
                        	</select>	
                            <!-- <input type="text" class="form-control" id="benlineliveinfosearch" placeholder="Find New Line Request">
                            -->
                        </div>
                    </div>
                </div>               
            {!! Form::open(array('url'=>'individual/employee/date/search','role'=>'form','method'=>'POST'))!!}
               <div class='col-md-2'>
					<div class="form-group">
						<div class='input-group'>
							<input type='text' class="form-control bbibillfrom" data-date-format="yyyy-mm-dd" name='datefrom' id='datefrom' placeholder="Date From"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar" ></span>
							</span>
						</div>
					</div>
				</div>
				<div class='col-md-2'>
					<div class="form-group">
						<div class='input-group'>
							<input type='text' class="form-control bbibillto" data-date-format="yyyy-mm-dd" name='dateto' id='dateto' placeholder="Date To"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar" ></span>
							</span>
						</div>
					</div>
				</div>
                <div class='col-md-2'>
                    <div class="form-group">
                        <div class='input-group'>
                            <button class="btn submit-button-bg"> Submit</button>
                        </div>
                    </div>
                </div>
            {!!Form::close()!!}
            </div>
            <div class="panel-heading clearfix">
              
                <div class="col-md-3">
                    <div class="form-group">
                        
                        <div class="col-md-10">
                           <select class="form-control" name="findEmployeeByDepartment" id="findEmployeeByDepartment">
                        		<option value="">Search By Department</option>
                        		<option value="Software">Software</option>
                                <option value="Hardware">Hardware</option>
                                <option value="Networking">Networking</option>
                        		<option value="dfgh">Store</option>
                        	</select>
                        </div>
                    </div>
                </div>
            
                <div class="col-md-3">
                    <div class="form-group">
                        
                        <div class="col-md-10">
                          <select class="form-control" name="findEmployeeByDesignation" id="findEmployeeByDesignation">
                        		<option value="">Search By Designation</option>
                        		<option value="Accountants">Accountants</option>
                        		<option value="Engnineer">Engnineer</option>
                                <option value="Technician">Technician</option>
                        		<option value="HR">HR</option>
                        	</select>
                        </div>
                    </div>
                </div>
               
                <!--
               <div class='col-md-3'>
					<div class="form-group">
						<select class="form-control">
                    		<option value="">Search By Status</option>
                    		<option value="Active">Active</option>
                    		<option value="Regined">Regined</option>
                        </select>
					</div>
				</div>
                -->
            </div>
            <hr/>
            <div class="panel-body">
                <div class="table-responsive">
                <div id="employee-report-result">
                    <table class="table table-hover table-bordered">
                        <thead class="table-header-bg">
                            <tr>
                                <th>S/N</th>
                                <th>Employee ID</th>
                                <th>Full Name</th>
                                <!--<th>Joining Date</th>
                                 <th>Address</th> -->
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Gender</th>
                                <th>Age</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>                      
                            <?php $sl = 1; ?>
                            @foreach ( $emp_list as $emp )
                                <?php
                                    $class = ( $sl%2 == 0 ) ? 'table-background-color' : 'table-background';
                                ?> 
                            <tr class="{{ $class }}">
                                <th scope="row">{{ $sl++ }}</th>
                                <td>{{ $emp->emp_id }}</td>
                                <td>{{ $emp->firstname . $emp->lastname }}</td>                              
                                <!--<td>{{ $emp->created_at }}
                                <td>{{ $emp->address }}</td> -->
                                <td>{{ $emp->emailaddress }}</td>
                                <td>{{ $emp->homephone }}</td>
                                <td>{{ $emp->gender }}</td>
                                <td>{{ $emp->age }}</td>
                                <td>
                                  <a href="{{ URL::to( '/individual/employee/details-report/'. $emp->emp_id ) }}">Details</a> 
                                </td>
                            </tr>
                            @endforeach 
                        </tbody>
                    </table>
                    {!! str_replace('/?', '?',$emp_list->render()); !!}
                </div>                
            </div>
        </div>
    </div>                    
                
    </div>
</div>


@stop