<div class="table-responsive">
	<table class="table table-hover table-bordered display" id="viewcomplaintable" style="width: 100%; cellspacing: 0;">
        <thead class="table-header-bg">
            <tr>
                <th>SN</th>
                <th>Full Name</th>
                <th>Designation</th>
                <th>Email</th>
                <th>Mobile Number</th>
                <th>Address</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $sl = 1; ?> 

                @foreach ($designationWiseEmployeeDetails as $info)                        
                    <?php
                        $class = ( $sl%2 == 0 ) ? 'table-background-color' : 'table-background';
                    ?>                               
                    <tr class="edit_tr {{ $class }} " >                                      
                       	<td><span class="table-column"> {{ $sl++ }} </span></td> 
                       	<td><span class="table-column"> {{ $info -> fullName}} </span></td> 
                       	<td><span class="table-column"> {{ $info -> designation}} </span></td> 
                       	<td><span class="table-column"> {{ $info -> email}} </span></td> 
                       	<td><span class="table-column"> {{ $info -> mobileNumber}} </span></td> 
                       	<td><span class="table-column table-address "> {{ $info -> localAddress}} </span></td> 
                       	<td>
                            <span class="table-column">
                       		   <a href="{{ URL::to( '/hrm/employees/'. $info->employeeID ).'/edit'}}"><i class="fa fa-edit icon-action"></i></a>
							</span>
                    	</td>
                    </tr>
                @endforeach
           
        </tbody>                        
    </table>
</div>