@extends( 'backend.index' )

@section( 'content_area' )

<div id="main-wrapper">

	<div class="panel panel-white">
		<div class="panel-body">
			<ul class="nav nav-tabs col-md-2 col-xs-12 col-sm-2 col-lg-2 tab-menu profile-left-sidebar">
				<li> <a hef="#">  <img src="{{ URL::asset( 'assets/images/'.$emp_details->profileImage ) }}" class="img-circle author" alt="author">  </a> </li>
				<li class="active"><a data-toggle="tab" href="#basicInfo"> <i class="fa fa-address-book-o  social-media" aria-hidden="true"></i>  Basic Information</a></li>
				<li><a data-toggle="tab" href="#Offical"> <i class="fa fa-book social-media" aria-hidden="true"></i> Official Info </a></li>
				<li><a data-toggle="tab" href="#contact"> <i class="fa fa-sliders social-media" aria-hidden="true"></i> Contact Info </a></li>
				<li><a data-toggle="tab" href="#account"> <i class="fa fa-book social-media" aria-hidden="true"></i> Basic Account Details </a></li>
				<li><a data-toggle="tab" href="#documents"> <i class="fa fa-home social-media" aria-hidden="true"></i> Documents </a></li>
			</ul>

			<div class="tab-content  col-md-10 col-xs-10 col-sm-10 col-lg-10 profile-right-sidebar">
				
			
				<div id="basicInfo" class="tab-pane fade in active">
					<div class="row client-information-box">
						<div class="client-header">
					    	<h4 class="client-title" id="accountAuthChange">Basic Informationn</h4>
						</div>
						<div class="client-information-content">
							<table class="table table-row-bg table-hover">
							    <tbody>
							        <tr class="row">
									    <td class="client-first-label"> Employee ID </td>
									    <td class="client-first-label"> 
									    	{{ $emp_details->employeeID  }}
											
									    </td>
									</tr>
									<tr class="row">
									    <td class="client-first-label"> Full Name </td>
									    <td class="client-first-label"> {{ $emp_details->fullName }}  </td>
									</tr>
									<tr class="row">
									    <td class="client-first-label"> Address </td>
									    <td class="client-first-label"> {{  $emp_details->localAddress }}  </td>
									</tr>
									<tr class="row">
									    <td class="client-first-label"> Full Name </td>
									    <td class="client-first-label"> {{ $emp_details->fullName }}  </td>
									</tr>
									<tr class="row">
									    <td class="client-first-label"> Phone </td>
									    <td class="client-first-label"> {{ $emp_details->mobileNumber }}  </td>
									</tr>
									<tr class="row">
									    <td class="client-first-label"> E-Mail </td>
									    <td class="client-first-label"> {{ $emp_details->email }}  </td>
									</tr>
									<tr class="row">
									    <td class="client-first-label"> Date of Birth </td>
									    <td class="client-first-label"> {{ $emp_details->date_of_birth }}  </td>
									</tr>
									<tr class="row">
									    <td class="client-first-label"> E-Mail </td>
									    <td class="client-first-label"> {{ $emp_details->email }}  </td>
									</tr>
									
					
									</tr>
									<tr class="row">
									    <td class="client-first-label"> Gender </td>
									    <td class="client-first-label"> {{ $emp_details->gender }}  </td>
									</tr>
									<tr class="row">
									    <td class="client-first-label"> Age </td>
									    <td class="client-first-label">  </td>
									</tr>
									<tr class="row">
									    <td class="client-first-label"> NID </td>
									    <td class="client-first-label">  </td>
									</tr>
								</tbody>
							</table>
						</div>				
					</div>		
				</div>
				<div id="Offical" class="tab-pane fade">
					<div class="row client-information-box">
						<div class="client-header">
					    	<h4 class="client-title" id="accountAuthChange">Official Information</h4>
						</div>
							<div class="client-information-content">
								<table class="table table-row-bg table-hover">
								    <tbody>
								        <tr class="row">
										    <td class="client-first-label"> Employee ID </td>
										    <td class="client-first-label">  {{ $emp_details->employeeID }} </td>
										</tr>
										<tr class="row">
										    <td class="client-first-label"> Date of Joining </td>
										    <td class="client-first-label"> {{ $emp_details->joiningDate }} </td>
										</tr>
										<tr class="row">
										    <td class="client-first-label">Employee Status </td>
										    <td class="client-first-label"> {{ $emp_details->status }} </td>
										</tr>
										<tr class="row">
										    <td class="client-first-label"> Location </td>
										    <td class="client-first-label">  {{ $emp_details->permanentAddress }} </td>
										</tr>
										<tr class="row">
										    <td class="client-first-label"> Designtion </td>
										    <td class="client-first-label">  {{ $emp_details->designation }}  </td>
										</tr>
										<tr class="row">
										    <td class="client-first-label"> Department </td>
										    <td class="client-first-label"> {{ $emp_details->deptName }} </td>
										</tr>
									</tbody>
								</table>
							</div>				
					</div>	
				</div>
				<div id="contact" class="tab-pane fade">
					<div class="row client-information-box">
						<div class="client-header">
					    	<h4 class="client-title" id="accountAuthChange">Contact Details</h4>
						</div>
						<div class="client-information-content">
							<table class="table table-row-bg table-hover">
							    <tbody>
							        <tr class="row">
									    <td class="client-first-label"> Present Address</td>
									    <td class="client-first-label"> {{ $emp_details->localAddress }} </td>
									</tr>
									<tr class="row">
									    <td class="client-first-label"> Permanent Address:</td>
									    <td class="client-first-label"> {{ $emp_details->permanentAddress }} </td>
									</tr>
									
									<tr class="row">
									    <td class="client-first-label"> Relationship </td>
									    <td class="client-first-label"> </td>
									</tr>
									<tr class="row">
									    <td class="client-first-label"> Mobile </td>
									    <td class="client-first-label"> {{ $emp_details->mobileNumber }} </td>
									</tr>
									<tr class="row">
									    <td class="client-first-label"> Telephne </td>
									    <td class="client-first-label"> {{ $emp_details->mobileNumber }} </td>
									</tr>
									<tr class="row">
									    <td class="client-first-label"> Email </td>
									    <td class="client-first-label"> {{ $emp_details->email }} </td>
									</tr>
									<tr class="row">
									    <td class="client-first-label"> Website </td>
									    <td class="client-first-label"> </td>
									</tr>
								</tbody>
							</table>
						</div>				
					</div>
				</div>
				<div id="account" class="tab-pane fade">
					<div class="row client-information-box">
						<div class="client-header">
					    	<h4 class="client-title" id="accountAuthChange"> Basic Account Details </h4>
						</div>
						<div class="client-information-content">
							<table class="table table-row-bg table-hover">
							    <tbody>
							        <tr class="row">
									    <td class="client-first-label"> Present Address</td>
									    <td class="client-first-label"> {{ $emp_details->localAddress }} </td>
									</tr>
								</tbody>
							</table>
						</div>			
					</div>
				</div>	
				<div id="documents" class="tab-pane fade">
					<div class="row client-information-box">
						<div class="client-header">
					    	<h4 class="client-title" id="accountAuthChange"> Documents </h4>
						</div>
						<!-- <div class="client-information-content">
							<table class="table table-row-bg table-hover">
							    <tbody>
							        <tr class="row">
									    <td class="client-first-label"> Present Address</td>
									    <td class="client-first-label"> {{ $emp_details->localAddress }} </td>
									</tr>
								</tbody>
							</table>
						</div>	
					</div> -->
				</div>
			</div>
		</div>				
	</div>

</div>

@stop