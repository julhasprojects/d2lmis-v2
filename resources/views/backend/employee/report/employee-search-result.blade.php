<table class="table table-hover table-bordered">
    <thead class="table-header-bg">
        <tr>
            <th>S/N</th>
            <th>Employee ID</th>
            <th>Full Name</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Gender</th>
            <th>Joining Date</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $sl = 1; ?>
        @foreach ( $emp_list as $emp )
            <?php
                $class = ( $sl%2 == 0 ) ? 'table-background-color' : 'table-background';
            ?> 
        <tr class="{{ $class }}">
            <td>{{ $sl++ }}</td>
            <td>{{ $emp->employeeID }}</td>
            <td>{{ $emp->fullName }}</td>
            <td>{{ $emp->email }}</td>
            <td>{{ $emp->mobileNumber }}</td>
            <td>{{ $emp->gender }}</td>
            <td>{{ $emp->joiningDate }}</td>
            <td>
              <a href="{{ URL::to( '/individual/employee/details-report/'. $emp->employeeID ) }}">Details</a> 
            </td>
        </tr>                            
        @endforeach 
    </tbody>
</table>
</table>