@extends( 'backend.index' )

@section( 'content_area' )
<div class="bg-title">
    <div class="page-title reports-title">
        <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
            <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
            <li class="completed"><a href="javascript:void(0);"> Reports</a></li>
            <li  class="completed"><a href="javascript:void(0);">  Employee  </a></li>
            <li  class="active"><a href="javascript:void(0);"> Individual Employee Report List</a></li>
        </ul>
   </div>
</div>        
@include('modules.hrm.hrm-navigation')
<div id="main-wrapper">
    <h3 class="btn-success" style="text-align:center; font-size:14px; color:#fff;">
        {!! Session::get( 'message' ) !!}   
    </h3>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                    <div class="panel-heading clearfix">
                        <h4 class="panel-title">Employee List</h4>
                        <div class="col-md-4">
                            <div class="form-group">                        
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="individual-employee-report" placeholder="Search Employee Information">
                                </div>
                            </div>
                        </div>
                    </div><hr>
                    <div id="loaderRun"></div>
                    <div class="panel-body">
                        <div class="table-responsive">
                        <div id="employee-report-result">
                            <table class="table table-hover table-bordered">
                                <thead class="table-header-bg">
                                    <tr>
                                        <th>S/N</th>
                                        <th>Employee ID</th>
                                        <th>Full Name</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                        <th>Gender</th>
                                        <th>Joining Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $sl = 1; ?>
                                    @foreach ( $emp_list as $emp )
                                        <?php
                                            $class = ( $sl%2 == 0 ) ? 'table-background-color' : 'table-background';
                                        ?> 
                                    <tr class="{{ $class }}">
                                        <td>{{ $sl++ }}</td>
                                        <td>{{ $emp->employeeID }}</td>
                                        <td>{{ $emp->fullName }}</td>
                                        <td>{{ $emp->email }}</td>
                                        <td>{{ $emp->mobileNumber }}</td>
                                        <td>{{ $emp->gender }}</td>
                                        <td>{{ $emp->joiningDate }}</td>
                                        <td>
                                          <a href="{{ URL::to( '/individual/employee/details-report/'. $emp->employeeID ) }}">Details</a> 
                                        </td>
                                    </tr>                            
                                    @endforeach 
                                </tbody>
                            </table>
                        </div>                
                    </div>
                </div>
            </div>                             
        </div>
    </div>
</div>

@stop