@extends( 'backend.index' )

@section( 'content_area' )
<div class="bg-title">
    <div class="page-title reports-title">
        <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
            <li class="completed">
                <a href="javascript:void(0);">Dashboard</a>
            </li>
            <li class="completed">
                <a href="javascript:void(0);"> Modules</a>
            </li>
            <li class="active">
                <a href="javascript:void(0);">  Update Employee Information</a>
            </li>
        </ul>
    </div>
</div>
@foreach ($emp_list as $list)
<div class="row column-box min-height">
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<h4 class="title-house">Package Information</h4>
			<div class="panel-body">
				<form method="POST" action="http://localhost/isperp/public/update-package" accept-charset="UTF-8" role="form" data-toggle="validator" novalidate="true"><input name="_token" value="tX2GYxun5qDxjf7xSdzd6r9jsVIDIAUc72chab4V" type="hidden">
					
                    <div class="col-md-6 col-xs-12 col-lg-6">
	                    <div class="form-group">
	                        <label for="employeeID" class="col-sm-3 control-label">Employee Id</label>
	                        <div class="col-sm-9">
	                            <input class="form-control" id="employeeID" name="employeeID" value="{{ $list -> employeeID }}" required="" type="text" readonly="">
	                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
	                            <div class="help-block with-errors"></div>
	                        </div>
	                    </div>
                    </div>
                    <div class="col-md-6 col-xs-12 col-lg-6">
	                    <div class="form-group">
	                        <label class="col-sm-3 control-label"> Name </label>
	                        <div class="col-sm-9">
	                            <input class="form-control date-picker" id="fullName" name="fullName" value="{{ $list -> fullName }}" required="" type="date">
	                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
	                            <div class="help-block with-errors"></div>
	                        </div>
	                    </div>
                    </div>
                    <div class="col-md-6 col-xs-12 col-lg-6">
	                    <div class="form-group">
	                        <label for="degignation" class="col-sm-3 control-label">Designation</label>
	                        <div class="col-sm-9">
	                            <select class="form-control" id="degignation" name="degignation" value="1" required=""> 
	                                <option value="Active"> Active </option>
	                                <option value="Inactive"> Inactive </option>
	                            </select>
	                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
	                            <div class="help-block with-errors"></div>
	                        </div>
	                    </div>
                    </div>
                    <div class="col-md-6 col-xs-12 col-lg-6">
						<div class="form-group has-success">
	                        <label for="email" class="col-sm-3 control-label">email</label>
	                        <div class="col-sm-9">
	                           <input class="form-control" id="id" name="id" value="3" type="hidden">
	                           <input class="form-control" id="email" name="email" value="{{ $list -> email }}" required="" type="text">
	                            <span class="glyphicon form-control-feedback glyphicon-ok" aria-hidden="true"></span>
	                            <div class="help-block with-errors"></div>
	                        </div>
	                    </div>
                    </div>
                    <div class="col-md-6 col-xs-12 col-lg-6">
						<div class="form-group has-success">
	                        <label for="mobileNumber" class="col-sm-3 control-label">Mobile Number</label>
	                        <div class="col-sm-9">
	                           <input class="form-control" id="id" name="id" value="3" type="hidden">
	                           <input class="form-control" id="mobileNumber" name="mobileNumber" value="{{ $list -> mobileNumber }}" required="" type="text">
	                            <span class="glyphicon form-control-feedback glyphicon-ok" aria-hidden="true"></span>
	                            <div class="help-block with-errors"></div>
	                        </div>
	                    </div>
                    </div>
                    <div class="col-md-6 col-xs-12 col-lg-6">
						<div class="form-group has-success">
	                        <label for="localAddress" class="col-sm-3 control-label">Address</label>
	                        <div class="col-sm-9">
	                           <input class="form-control" id="id" name="id" value="3" type="hidden">
	                           <input class="form-control" id="localAddress" name="localAddress" value="{{ $list -> localAddress }}" required="" type="text">
	                            <span class="glyphicon form-control-feedback glyphicon-ok" aria-hidden="true"></span>
	                            <div class="help-block with-errors"></div>
	                        </div>
	                    </div>
                    </div>
                    <div class="form-group">                            
         				<center><button type="submit" class="btn submit-button-bg">Update</button></center>
						</div>
                </form>
			</div>
		</div>
	</div>
			
</div>
@endforeach
@stop