	@include( 'master.admin_header' ) 
	    <main class="page-content content-wrap">
	    	<div class="panel-body-client">
	        	@yield( 'content_area' )
	        </div>
	    </main>
    @include( 'master.third_party_library' )
</body>
</html>