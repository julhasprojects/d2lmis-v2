@extends( 'backend.index' )

@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);"> Reports</a></li>
        <li class="completed"><a href="javascript:void(0);"> Billing</a></li>
        <li  class="active"><a href="javascript:void(0);"> Home User Bill Summary Report </a></li>
    </ul>
    <div class="btn-group button-group col-md-4 col-xs-12 col-lg-4 col-sm-4 pull-right" role="group" aria-label="Basic example">
        <div class="btn-group pull-right" role="group" aria-label="First group">
            <a class="btn btn-success" href="{{ URL::to('/export/bill/home/collector') }}">Export</a>
            <button type="button" class="btn btn-info" href="javascript:void()" onclick="window.print()">Print</button>
            <button type="button" class="btn btn-primary">PDF</button>
        </div>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
              

                <div class="panel-body">
                    <div class="table-responsive">
                        <div id="belbillgenerated-report-search-result" class="relative">
                            <table class="table table-hover table-bordered" id="print-searched-bill">
                                <thead class="table-header-bg">
                                    <tr>
                                        <th>S/N</th>
                                        <th>Full Name</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $sl=0 ?>
                                    @foreach($employees as $collector)
                                       
                                        <tr>
                                            
                                            <td>{{$sl++}}</td>
                                            <td>{{$collector->fullName}}</td>
                                            

                                        </tr>

                                    @endforeach
                                        

                                </tbody>
                            </table>
                            <div class="row-data">
                                
                                <h4 class="pull-right">
                                 
                                </h4> 
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>       
    </div>
</div>


@stop