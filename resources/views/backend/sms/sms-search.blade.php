<table class="table table-hover table-bordered display" id="tableresponsive">
    <thead class="table-header-bg">
    <tr>
        <th>S/N</th>
        <th>ID</th>
        <th>Number</th>
        
        <th>Message</th>
        <th>Send Time</th>
        
    
    </tr>
    </thead>
    <tbody>
        <?php 
            $sl = 1; 
            $total=0;
        ?>
         @foreach ( $smsHistory as $smsHistory )
            <?php
                $class = ( $sl%2 == 0 ) ? 'table-background-color' : 'table-background';
            ?> 
            <tr class="{{$class}}">
                <td>{{ $sl++ }}</td>
                <td>{{ $smsHistory->newconid }}</td>
                <td>{{ $smsHistory->mobile }}</td>
                <td>{{ $smsHistory->message }}</td>
                <td>{{ $smsHistory->created_at }}</td>
               
            </tr>

        @endforeach
         
    </tbody>
    
</table>  