@extends( 'backend.index' )
@section( 'content_area' )
@include('master.message')
<div class="bg-title">
    <div class="page-title reports-title">
        <ul class="breadcrumb  col-md-5 col-xs-12 col-lg-5 col-sm-5 pull-left">
            <li class="completed"><a href="javascript:void(0);">Dashboard </a></li>
            <li class="completed"><a href="javascript:void(0);"> Settings </a></li>
            <li  class="active"><a href="javascript:void(0);"> SMS Consumption  Repots  </a></li>
        </ul>
    </div>
</div>
@include('master.error')


<div id="deletenotification"></div>
<div id="main-wrapper" class="region-road-setup">
	<div class="col-md-12 colum-design-para ">
        <div class="panel panel-white">
            <div class="panel panel-body">
                <div class="panel-body">
                     <ul class="nav nav-tabs"  print-none>
                        <li class="active"><a data-toggle="tab" href="#allOnlineCollection"> All SMS Reports </a></li>
                        <li><a data-toggle="tab" href="#advanced"> Advanced Search</a></li>
                    </ul>
                    <div class="tab-content tab-advaced">
                        <div id="advanced" class="tab-pane fade  print-none advanced-search-minheight" >
                            <div class="row">
                                <div class='col-md-2'>
                                    <div class="form-group">
                                        <div class='input-group'>
                                            <select class="form-control chosen-select-width" name="billSummeryMonth" id="billSummeryMonth" required="">
                                                @for($m = 1;$m <= 12; $m++){
                                                    <?php 
                                                        $month =  date("F", mktime(0, 0, 0, $m));
                                                        $monthlyDate =  date("m", mktime(0, 0, 0, $m)); 
                                                    ?>
                                                    <option value='{{ $monthlyDate }}' @if($currentMonth == $month) selected @endif >{{ $month }}</option>
                                                @endfor
                                            </select> 
                                        </div>
                                    </div>
                                </div>
                                <div class='col-md-2'>
                                    <div class="form-group">
                                        <div class='input-group'>
                                            <select class="form-control chosen-select-width" id="billSummeryYear" name="billSummeryYear">
                                                @for($i = 2018; $i < date("Y")+1; $i++)
                                                    <option value="{{ $i }}" @if($currentyear ==$i) selected @endif >{{ $i }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class='col-md-1'>
                                    <div class="form-group">
                                        <div class='input-group'>
                                            <button class="btn btn-info" id="sendSMSHistory"> Search </button>
                                        </div>
                                    </div>
                                </div>

                               <div class='col-md-2'>
                                    <div class="form-group">
                                        <div class='input-group'>
                                            <input type='text' class="form-control bbibillfrom" data-date-format="yyyy-mm-dd" name="from" id="gbar_from_date" placeholder="Date From" required />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar" ></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class='col-md-2'>
                                    <div class="form-group">
                                        <div class='input-group'>
                                            <input type='text' class="form-control bbibillto" data-date-format="yyyy-mm-dd" name="to" id="gbar_to_date" placeholder="Date To" required />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar" ></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class='col-md-1'>
                                    <div class="form-group">
                                        <div class='input-group'>
                                            <button class="btn btn-info" id="smsDateSearch"> Search </button>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                           <div class="col-md-2 col-xs-12">
                                <div class="form-group">
                                    <select class="form-control select-form-desing" name="region" id="region" >
                                        <option value="">Search By Area</option>
                                        @foreach($region as $info)
                                            <option value="{{ $info->region_id }}"> {{ $info->region_name }} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                            <div class='col-md-2 col-xs-12'>
                                <div class="form-group">
                                    <input type="submit"  class="btn btn-info" id="smsRegionSearch">
                                </div>
                            </div>
                           
                            <div id="SmsReportsSearchLoader"></div>
                            <div id="smsReportsResult"></div>
                            
                        </div> 
                        <div id="allOnlineCollection" class="tab-pane fade  tab-print-body in active"> 
                            <table class="table table-hover table-bordered display" id="tableresponsive">
                                <thead class="table-header-bg">
                                <tr>
                                    <th>S/N</th>
                                    <th>ID</th>
                                    <th>Number</th>
                                    
                                    <th>Message</th>
                                    <th>Send Time</th>
                                    
                                
                                </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $sl = 1; 
                                        $total=0;
                                    ?>
                                     @foreach ( $smsHistory as $smsHistory )
                                        <?php
                                            $class = ( $sl%2 == 0 ) ? 'table-background-color' : 'table-background';
                                        ?> 
                                        <tr class="{{$class}}">
                                            <td>{{ $sl++ }}</td>
                                            <td>{{ $smsHistory->newconid }}</td>
                                            <td>{{ $smsHistory->mobile }}</td>
                                            <td>{{ $smsHistory->message }}</td>
                                            <td>{{ $smsHistory->created_at }}</td>
                                           
                                        </tr>

                                    @endforeach
                                     
                                </tbody>
                                
                            </table>    
                        </div>
                    </div>
                </div>
            </div>
    	</div>
	</div>
</div>

@stop    