<div class="navbar">
<div class="navbar-inner">    
<div class="topmenu-outer">
    <img src="{{ URL::asset( 'd2lmis-logo.png' ) }}" class="d2lmis-logo-position" height="40" width="110" alt="D2LMIS" />
<div class="top-menu" style="margin-top: -40px;">
    <nav class="nav">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right" style="margin-right:20px">
                    @if(Gate::check('admin.access'))
                        <li>
                            <a href="{{ URL::to( '/d2lmis-control-panel' ) }}" >
                                <span style="color:white;">
                                <img src="{{ URL::asset( 'assets/images/dashboard.gif' ) }}" style="height:25px;width:25px;" alt=""> &nbsp;Control Panel</span>
                            </a>
                        </li>
                    @endif
                    @if(Gate::check('clienthistory.access'))
                        <li>
                            <a class="waves-effect waves-button" data-toggle="modal" data-target=".client-history-modal-lg" href="#">
                                <span style="color:white;">
                                <img src="{{ URL::asset( 'assets/images/administration.png' ) }}" style="height:25px;width:25px; font-weight:bold;" alt=""> &nbsp;Product Search</span>
                            </a>
                        </li>
                    @endif
                    @if(Gate::check('newlinereg.access') || Gate::check('houseshiftmgt.access') || Gate::check('packagechange.access') || Gate::check('newline.access') || Gate::check('newlinecollection.access') || Gate::check('houseshiftmgt.access') || Gate::check('packagechange.access') || Gate::check('billgenerated.access') || Gate::check('complainview.access') || Gate::check('disconnect.request.access') || Gate::check('disconnectview.access') || Gate::check('billgenerated.access') || Gate::check('billingsupport.access') || Gate::check('smsemail.access'))
                    <li class="hover">
                        <a hre="#" class="dropdown-hover">
                            <span style="color:white;">
                            <img src="{{ URL::asset( 'assets/images/ISPERP-Modules.png' ) }}" alt=""> &nbsp;Modules</span>
                        </a>
                        <ul class="hover-menu">
                            <div class="hover-menu-content scrollbox">
                                <div class="caret-up-border"></div>
                                <div class="caret-up-background"></div>
                                <hr>

                                @if(Gate::check('newline.access'))
                                    <li>
                                        <a href="{{ URL::to( '/receive' ) }}">
                                            <img src="{{ URL::asset( 'assets/images/all-newline-connections.png' ) }}" class="img-responsive  hover-image" alt="" >
                                            <span class="user-name">Product Receive</span>                          
                                         </a>
                                    </li>
                                    <li>
                                        <a href="{{ URL::to( '/receive/show' ) }}">
                                            <img src="{{ URL::asset( 'assets/images/all-newline-connections.png' ) }}" class="img-responsive  hover-image" alt="" >
                                            <span class="user-name">Product Approval</span>                          
                                         </a>
                                    </li>
                                    
                                    <li>
                                        <a href="{{ URL::to( '/create-new-requisition' ) }}">
                                            <img src="{{ URL::asset( 'assets/images/active-employee.png' ) }}" class="img-responsive  hover-image" alt="" >
                                            <span class="user-name">  Product Requisition</span>                          
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ URL::to( '/requisition/approval' ) }}">
                                            <img src="{{ URL::asset( 'assets/images/active-employee.png' ) }}" class="img-responsive  hover-image" alt="" >
                                            <span class="user-name">   Requisition Approve</span>                          
                                        </a>
                                    </li>
                                @endif

                                @if(Gate::check('houseshiftmgt.access'))
                                    <li>
                                       <a href="{{ URL::to( '/stock/return' ) }}">
                                            <img src="{{ URL::asset( 'assets/images/all-hs-request.png' ) }}" class="img-responsive  hover-image" alt="" >
                                            <span class="user-name">Stock Return </span>                          
                                        </a>
                                    </li>
                                    <li>
                                       <a href="{{ URL::to( '/stock/return/show' ) }}">
                                            <img src="{{ URL::asset( 'assets/images/all-hs-request.png' ) }}" class="img-responsive  hover-image" alt="" >
                                            <span class="user-name">Stock Return Approve</span>                          
                                        </a>
                                    </li>
                                @endif

                                @if(Gate::check('packagechange.access'))
                                    <li>
                                        <a href="{{ URL::to( '/damage/stock' ) }}">
                                          <img src="{{ URL::asset( 'assets/images/all-pc-request.png' ) }}" class="img-responsive  hover-image" alt="" >
                                            <span class="user-name">Damage Stock </span>                          
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ URL::to( '/damage/stock/show' ) }}">
                                          <img src="{{ URL::asset( 'assets/images/all-pc-request.png' ) }}" class="img-responsive  hover-image" alt="" >
                                            <span class="user-name">Damage Stock </span>                          
                                        </a>
                                    </li>
                                @endif
                            </div>
                       </ul>
                    </li>
                    @endif

                   
                    @if(Gate::check('employeeassign.access') || Gate::check('hrmsetting.access') || Gate::check('packagesetup.access') || Gate::check('billdate.access') || Gate::check('billsheet.access') || Gate::check('complainsetting.access') || Gate::check('companysetup.access') || Gate::check('newusersetup.access') || Gate::check('customercaredeshboard.access') || Gate::check('copyrighttext.access') || Gate::check('dbbackaup.access') || Gate::check('complainsetting.access') || Gate::check('resgionsetup.access') || Gate::check('systempermission.panel.access') || Gate::check('superamdmin.access'))
                    <li class="hover">
                        <a hre="#" class="dropdown-hover">
                            <span style="color:white;">
                                <img src="{{ URL::asset( 'assets/images/general-settings.png' ) }}" alt=""> &nbsp;Settings
                            </span>
                        </a>
                        <ul class="hover-menu settings" >
                            <div class="caret-up-border"></div>
                            <div class="caret-up-background"></div>
                            <div class="hover-menu-content scrollbox" style="overflow:visible;">
                                <div class="apps-search-wrap">
                                </div>
                                <hr>
                                
                                @if(Gate::check('hrmsetting.access'))
                                <li>
                                    <a href="{{ URL::to( '/supplier' ) }}">
                                        <img src="{{ URL::asset( 'assets/images/hrm.png' ) }}" class="img-responsive  hover-image" alt="" >
                                        <span class="user-name">Suppliers Setup</span>                          
                                    </a>
                                </li>
                                @endif
                                @if(Gate::check('superamdmin.access'))
                                <li>
                                    <a href="{{ URL::to( '/manufacturer' ) }}">
                                       <img src="{{ URL::asset( 'assets/images/newline.png' ) }}" style="height:60px;width:50px" class="img-responsive  hover-image" alt="" >
                                       <span class="user-name">Manufacturer </span>                     
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ URL::to( '/product/category' ) }}">
                                        <img src="{{ URL::asset( 'assets/images/employee-reports.png' ) }}" class="img-responsive  hover-image" alt="" >
                                        <span class="user-name">Product Category</span>                          
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ URL::to( '/product/unit' ) }}">
                                        <img src="{{ URL::asset( 'assets/images/employee-reports.png' ) }}" class="img-responsive  hover-image" alt="" >
                                        <span class="user-name">Product Unit Setup</span>                          
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ URL::to( '/new/product/setup' ) }}">
                                       <img src="{{ URL::asset( 'assets/images/region-setup.png' ) }}" class="img-responsive  hover-image" alt="" >
                                       <span class="user-name">Product Setup</span>                        
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ URL::to( '/transaction/type' ) }}">
                                       <img src="{{ URL::asset( 'assets/images/region-setup.png' ) }}" class="img-responsive  hover-image" alt="" >
                                       <span class="user-name">Transaction type setup</span>                        
                                    </a>
                                </li>
                                @endif                              
                               
                                

                                @if(Gate::check('newusersetup.access'))
                                <li>
                                    <a class="waves-effect waves-button" href="{{ URL::to( '/admin-information' ) }}">
                                        <img src="{{ URL::asset( 'assets/images/administration.png' ) }}" class="img-responsive  hover-image" alt="" >
                                        <p> User Setup</p>
                                    </a>
                                </li>
                                @endif

                                @if(Gate::check('dbbackaup.access'))
                                 <li>
                                   <a href="{{ URL::to( '/database' ) }}" target="_blank">
                                       <img src="{{ URL::asset( 'assets/images/database-backup.png' ) }}" class="img-responsive  hover-image" alt="" >
                                       <span class="user-name">Database Backup </span> 
                                   </a>
                               </li> 
                                @endif
                               

                                @if(Gate::check('systempermission.panel.access'))
                                <li>
                                   <a href="{{ URL::to( '/role/permission' ) }}">
                                      <img src="{{ URL::asset( 'assets/images/view-complain.png' ) }}" class="img-responsive  hover-image" alt="" >
                                      <span class="user-name">System permission Panel</span>                     
                                   </a>
                               </li>
                                @endif

                                @if(Gate::check('companysetup.access'))
                                    <li>
                                        <a href="{{ URL::to( '/data/validations' ) }}" data-toggle="modal">
                                           <img src="{{ URL::asset( 'assets/images/data-validations.png' ) }}" class="img-responsive  hover-image" alt="company setup" >
                                           <span class="user-name"> Data Validations </span>                        
                                        </a>
                                    </li>
                                @endif
                                @if(Gate::check('global.settings.access'))
                                    <li>
                                        <a href="{{ URL::to( '/global/system/settings' ) }}">
                                           <img src="{{ URL::asset( 'assets/images/settings.png' ) }}" style="height:60px;width:50px" class="img-responsive  hover-image" alt="" >
                                           <span class="user-name"> Global System Settings </span>                     
                                        </a>
                                    </li>
                                @endif
                                @if(Gate::check('superamdmin.access'))
                                    <li>
                                        <a href="{{ URL::to( '/super/admin' ) }}">
                                           <img src="{{ URL::asset( 'assets/images/administrator.png' ) }}" class="img-responsive  hover-image" alt="" >
                                           <span class="user-name">System Administrator</span>                     
                                        </a>
                                    </li>
                                @endif
                                @if(Gate::check('sms.management.access'))
                                    <li>
                                        <a href="{{ URL::to( '/sms/management' ) }}">
                                           <img src="{{ URL::asset( 'assets/images/sms-mgt.png' ) }}" style="height:60px;width:50px" class="img-responsive  hover-image" alt="" >
                                           <span class="user-name"> SMS Settings </span>                     
                                        </a>
                                    </li>
                                @endif
                                

                                @if(Gate::check('companysetup.access'))
                                    <li>
                                        <a href="{{ URL::to( '/company-profile' ) }}" data-toggle="modal">
                                           <img src="{{ URL::asset( 'assets/images/company-profile.png' ) }}" class="img-responsive  hover-image" alt="company setup" >
                                           <span class="user-name"> Facility Info Setup </span>                        
                                        </a>
                                    </li>
                                @endif
                                    <li>
                                        <a href="{{ URL::to( '/email/setting' ) }}" data-toggle="modal">
                                           <img src="{{ URL::asset( 'assets/images/email.png' ) }}" class="img-responsive  hover-image" alt="company setup" >
                                           <span class="user-name"> Email Setting </span>                        
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ URL::to( '/prepaid/card/generator' ) }}" data-toggle="modal">
                                           <img src="{{ URL::asset( 'assets/images/credit-card4.png' ) }}" class="img-responsive  hover-image" alt="company setup" >
                                           <span class="user-name"> Barcode Generator</span>                        
                                        </a>
                                    </li>
                               
                            </div>
                       </ul>
                    </li>
                    @endif
                    <li>
                        <a href="{{ URL::to( '/d2lmis-dashboard' ) }}" target="_blank" >
                            <span style="color:white;">
                            <img src="{{ URL::asset( 'assets/images/reports-dashboard.png' ) }}" style="height:25px;width:25px;" alt=""> D2LMIS Dashboard</span>
                        </a>
                    </li>
                    <li class="hover">
                        <a hre="#" class="dropdown-hover">
                            <span style="color:white;"> <img src="{{ URL::asset( 'assets/images/admin.png' ) }}" alt=""> &nbsp;Profile</span>
                        </a>
                        <ul class="hover-menu">
                            <div class="caret-up-border"></div>
                            <div class="caret-up-background"></div>
                            <div class="hover-menu-content" style="overflow: hidden;">
                              
                                <li>
                                    <a class="waves-effect waves-button" id = "employeeprofiledata" data-toggle="modal" data-target=".admin-profile-modal-lg" href="#">
                                        <img src="{{ URL::asset( 'assets/images/accounts.png' ) }}" class="img-responsive  hover-image" alt="" >
                                        View Profile
                                     </a>
                                </li>
                                <li>
                                    <button  id="logout" class="log-out waves-effect waves-button waves-classic" onclick="logout('Are you sure want to logout??')">    
                                        <img src="{{ URL::to('assets/images/function-log-out.png' ) }}" class="img-responsive  hover-image" alt="" >
                                        <span style="width:100%">Log Out</span>
                                    </button>
                                </li>
                                <li>
                                    <a href="#" type="button"   data-toggle="modal" data-target="#help"><img src="{{ URL::asset( 'assets/images/help.png' ) }}" style="width:40px;" class="img-responsive  hover-image" alt="" >
                                       Quick Support 
                                    </a>
                                </li>
                                @if(Gate::check('user.manual'))
                                <li>
                                    <a href="{{ URL::to( '/user-manual' ) }}" type="button">
                                        <img src="{{ URL::asset( 'assets/images/user-manaul.png' ) }}" style="width:40px;" class="img-responsive  hover-image" alt="" >
                                       User Manual
                                    </a>
                                </li>
                                @endif
                            </div>
                       </ul>
                    </li>

                </ul><!-- Nav -->
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

</div><!-- Top Menu -->
</div>
</div>
</div><!-- Navbar -->

<!-- Help button html -->
<div id="help" class="  modal fade" role="dialog">
    <div class="modal-dialog hover-menus">
        <div class="modal-content hover-menus">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="accountAuthChange">Do you have a technical issue? Please knock us:</h4>
            </div>
            <hr/>
            <div class="modal-body">
                <div id="clientInformation" class="tab-pane  active" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <table class="table table-hover">
                                    <tbody>
                                        <tr> 
                                            <td>Julhas Sujan</td>                          

                                        </tr>
                                        <tr>
                                            <td>julhaspustcse@gmail.com</td>
                                        </tr>
                                        <tr>
                                            <td>julhas08 (Skype)</td>
                                        </tr>
                                        <tr>
                                            <td>+8801989 442856 </td>
                                        </tr>
                                        <tr>
                                            <td><a href="http://d2lmis.org" target="_blank">D2LMIS</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>                                    
                    </div>                                
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="logOutModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-times" aria-hidden="true"></i> </button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body" id="info">
                <p>
                     {{--Confirm Message Here from Javascript--}}
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn default">Cancel</button>
                <button type="button" data-dismiss="modal" class="btn red" id="logoutSubmit"><i class="fa fa-trash"></i> Confirm</button>
            </div>
        </div>
   </div>
</div> 


  