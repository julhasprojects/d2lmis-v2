@include( 'master.admin_header' ) 
<main class="page-content content-wrap">
    @include( 'backend.usermenu.main-navigation' )     
    @include('backend.left_sidebar' )
    <div class="page-inner">
        @yield( 'content_area' )
    </div>
</main>
@include( 'master.third_party_library' )
</body>
</html>