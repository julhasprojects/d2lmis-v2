<div id="ramadanmenu" class="menu-menu-1-container bottom-menubar">
	<div id="menu-button">Menu</div>
	<ul id="menu-menu-1" class="menu">
		<li><a href="{{ URL::to( '/sms/management' ) }}">Dashboard</a></li>
		<li>
			<a href="{{ URL::to( 'sms/management' ) }}">
				<span>
				   	<span class="title">SMS Settings</span> 
			   	</span>
		    </a>
		</li>
		<li>
		  	<a href="{{ URL::to( 'sms/type/setup' ) }}">
				<span>
				   	<span class="title"> SMS Type Setup</span> 
			   	</span>
		   </a>
		</li>
		<li>
		    <a href="{{ URL::to( 'sms/response' ) }}">
				<span>
				   	<span class="title"> SMS Respones</span> 
			   	</span>
		    </a>
		</li>
		<li>
		   <a href="{{ URL::to( 'sms/sending/history' ) }}">
				<span>
				   	<span class="title">SMS Sending History</span> 
			   	</span>
		   </a>
		</li>
	</ul>
</div>
