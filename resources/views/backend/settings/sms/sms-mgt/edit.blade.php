@extends( 'backend.index' )
@section( 'content_area' )
	
<div class="bg-title">
	<div class="page-title reports-title">
        <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
            <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
            <li class="completed"><a href="javascript:void(0);"> Modules</a></li>
            <li class="active"><a href="javascript:void(0);"> SMS Custom SMS </a></li>
        </ul>
	</div>		
</div>
@include('backend.settings.sms.sms-mgt.sms-menu')
@include('master.message')
@include('master.error')
<div id="main-wrapper">
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<h4 class="title-house">SMS Setting Update</h4>
			<div class="panel-body">
			
				{{Form::open(array('url'=>"sms/management/".$smsSettings->id,'class'=>'form-horizontal form-bordered','method'=>'PUT'))}}
				<div class="col-md-12">
	                <label class="control-label col-md-2">Operator Name: <span class="required">*</span></label>
	                <div class="col-md-8">
	                    {!! Form::text('name', $smsSettings->operator_name,['class'=>'form-control','placeholder'=>'Operator Name']) !!}
	                    <br>
	                </div>
	            </div>

	            <div class="col-md-12">
	                <label class="control-label col-md-2">Operator Short Code: <span class="required">*</span></label>
	                <div class="col-md-8">
	                    {!! Form::text('code', $smsSettings->short_code,['class'=>'form-control','placeholder'=>'Short code']) !!}
	                    <br>
	                </div>
	            </div>
				<div class="col-md-12">
	                <label class="control-label col-md-2">Username: <span class="required">*</span></label>
	                <div class="col-md-8">
	                    {!! Form::text('usrname', $smsSettings->username,['class'=>'form-control','placeholder'=>'usrname']) !!}
	                    <br>
	                </div>
	            </div>

	            <div class="col-md-12">
	                <label class="control-label col-md-2">Password: <span class="required">*</span></label>
	                <div class="col-md-8">
	                    {!! Form::text('password', $smsSettings->password,['class'=>'form-control','placeholder'=>'Password here']) !!}
	                    <br>
	                </div>
	            </div>
	            <div class="col-md-12">
	                <label class="control-label col-md-2">Sender: <span class="required">*</span></label>
	                <div class="col-md-8">
	                    {!! Form::text('sender', $smsSettings->sender,['class'=>'form-control','placeholder'=>'Sender']) !!}
	                    <br>
	                </div>
	            </div>

	            <div class="col-md-12">
	                <label class="control-label col-md-2">Username Param: <span class="required">*</span></label>
	                <div class="col-md-8">
	                    {!! Form::text('url', $smsSettings->url,['class'=>'form-control','placeholder'=>'username param']) !!}
	                    <br>
	                </div>
	            </div>
	            <div class="col-md-12">
	                <label class="control-label col-md-2">Password Param: <span class="required">*</span></label>
	                <div class="col-md-8">
	                    {!! Form::text('password_param', $smsSettings->username_param,['class'=>'form-control','placeholder'=>'Password Param']) !!}
	                    <br>
	                </div>
	            </div>

	            <div class="col-md-12">
	                <label class="control-label col-md-2">From Param: <span class="required">*</span></label>
	                <div class="col-md-8">
	                    {!! Form::text('from_param', $smsSettings->password_param,['class'=>'form-control','placeholder'=>'From Param']) !!}
	                    <br>
	                </div>
	            </div>
	            <div class="col-md-12">
	                <label class="control-label col-md-2"> To Param: <span class="required">*</span></label>
	                <div class="col-md-8">
	                    {!! Form::text('to_param', $smsSettings->from_param,['class'=>'form-control','placeholder'=>'To Param','col'=>'2','row'=>'2']) !!}
	                    <br>
	                </div>
	            </div>
				 <div class="col-md-12">
	                <label class="control-label col-md-2"> Message Param:</label>
	                <div class="col-md-8">
	                    {!! Form::text('message_param', $smsSettings->to_param,['class'=>'form-control','placeholder'=>'Message Param','col'=>'2','row'=>'2']) !!}
	                    <br>
	                </div>
	            </div>
	            <div class="col-md-12 text-center">
	                {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
	            </div>
	            {{ Form::close() }}
			</div>
		</div>
	</div>

</div>
@stop