@extends( 'backend.index' )
@section( 'content_area' )
@include('master.message')	
<div class="bg-title">
	<div class="page-title reports-title">
        <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
            <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
            <li class="completed"><a href="javascript:void(0);"> Modules</a></li>
            <li class="active"><a href="javascript:void(0);"> SMS Custom SMS </a></li>
        </ul>
	</div>		
</div>
<div id="deletenotification"></div>
@include('backend.settings.sms.sms-mgt.sms-menu')
@include('master.error')	
<div id="main-wrapper">
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<h4 class="title-house"> SMS Setting Update </h4>
			<div class="panel-body">
				 {{Form::open(array('url'=>"sms/global/setting/".$smsEnable->id,'class'=>'form-horizontal form-bordered','method'=>'post'))}}
					<div class="sms-setup">
						<div class="col-md-12">
			                <label class="col-md-3">SMS Setting Enable:</label>
			                <div class="col-md-8 global-settings">
			                    <div class="col-md-2 col-lg-2 col-xs-12 pull-left">
									<div class="checker"><span><input class="form control" @if($smsEnable->is_enable == 1) checked @endif name="is_enable" value="1" type="radio"></span></div> Yes
								</div>
								<div class="col-md-2 col-lg-2 col-xs-12 pull-left">
									<div class="checker"><span><input class="form control" @if($smsEnable->is_enable == 0) checked @endif  name="is_enable" value="0" type="radio"></span></div> No
								</div>
			                </div>
			            </div>
			        </div>
		            <div class="col-md-12 text-center">
		                {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
		            </div>
	            {{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@include('backend.settings.sms.sms-delete')
@stop