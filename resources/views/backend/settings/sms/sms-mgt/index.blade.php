@extends( 'backend.index' )
@section( 'content_area' )
@include('master.message')	
<div class="bg-title">
	<div class="page-title reports-title">
        <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
            <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
            <li class="completed"><a href="javascript:void(0);"> Modules</a></li>
            <li class="active"><a href="javascript:void(0);"> SMS Custom SMS </a></li>
        </ul>
	</div>		
</div>
<div id="deletenotification"></div>
@include('backend.settings.sms.sms-mgt.sms-menu')
@include('master.error')	
@include('master.confirm')	
<div id="main-wrapper">
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<h4 class="title-house"> SMS Setting Create </h4>
			<div class="panel-body">
				<ul class="nav nav-tabs">
					<li><a data-toggle="tab" href="#smsMultipleSetting" class="active">SMS Multiple Setting</a></li>
					<li @if (session('status')==2 ) @else  @endif><a data-toggle="tab" href="#smsSetting">SMS Setting</a></li>
					<li @if (session('status')==2 )  @endif><a data-toggle="tab" href="#smsEnable">Global Settings</a></li>
				</ul>
				<div class="tab-content">
					<div id="smsMultipleSetting" class="tab-pane fade in active">
						<table class="table table-bordered">
						    <thead class="table-header-bg">
						      	<tr>
							        <th>Sl.</th>
							        <th>SMS Sending</th>
							        <th>Action</th>
						      	</tr>
						    </thead>
						    <tbody>
						      	<tr>
							        <td>1</td>
							        <td>Line Man Job Assing?</td>
							        <td><input type="checkbox" id="sms_1" class="make-switch" data-on-color="success" data-on-text="On" data-off-text="Off" data-off-color="danger" value="1" onchange="smsGlobalSetting('1')"></td>
							       
						      	</tr>
						      	<tr>
						      		<td>2</td>
							        <td>Phone Number Change?</td>
							        <td><input type="checkbox" id="sms_2" class="make-switch" data-on-color="success" data-on-text="On" data-off-text="Off" data-off-color="danger" value="1" onchange="smsGlobalSetting('2')"></td>
							       
						      	</tr>
						      	<tr>
						      		<td>3</td>
							        <td>Mac Address Change?</td>
							        <td><input type="checkbox"  id="sms_3" class="make-switch" data-on-color="success" data-on-text="On" data-off-text="Off" data-off-color="danger" value="1" onchange="smsGlobalSetting('3')"></td>
							       
						      	</tr>
						      	<tr>
						      		<td>4</td>
							        <td>Line Acitve?</td>
							        <td><input type="checkbox"  id="sms_4" class="make-switch" data-on-color="success" data-on-text="On" data-off-text="Off" data-off-color="danger" value="1" onchange="smsGlobalSetting('4')"></td>
						      	</tr>	
						      	<tr>
						      		<td>5</td>
							        <td>Line Block/Discontinue?</td>
							        <td><input type="checkbox"  id="sms_5" class="make-switch" data-on-color="success" data-on-text="On" data-off-text="Off" data-off-color="danger" value="1" onchange="smsGlobalSetting('5')"></td>
						      	</tr>
						      	<tr>
						      		<td>7</td>
							        <td>Line Block/Discontinue Future?</td>
							        <td><input type="checkbox"  id="sms_6" class="make-switch" data-on-color="success" data-on-text="On" data-off-text="Off" data-off-color="danger" value="1" onchange="smsGlobalSetting('6')"></td>
						      	</tr>
						      	<tr>
						      		<td>8</td>
							        <td>Bulk SMS to current month’s defaulter ?</td>
							        <td><input type="checkbox"  id="sms_7" class="make-switch" data-on-color="success" data-on-text="On" data-off-text="Off" data-off-color="danger" value="1" onchange="smsGlobalSetting('7')"></td>
						      	</tr>
						      	<tr>
						      		<td>9</td>
							        <td> Bulk SMS to active users of one interface ?</td>
							        <td><input type="checkbox"  id="sms_8" class="make-switch" data-on-color="success" data-on-text="On" data-off-text="Off" data-off-color="danger" value="1" onchange="smsGlobalSetting('8')"></td>
						      	</tr>
						      
						      	<tr>
						      		<td>10</td>
							        <td>Bulk SMS to active users of one Box?</td>
							        <td><input type="checkbox"  id="sms_9" class="make-switch" data-on-color="success" data-on-text="On" data-off-text="Off" data-off-color="danger" value="1" onchange="smsGlobalSetting('9')"></td>
						      	</tr>
						      	<tr>
						      		<td>11</td>
							        <td>Single SMS to particular user?</td>
							        <td><input type="checkbox"  id="sms_10" class="make-switch" data-on-color="success" data-on-text="On" data-off-text="Off" data-off-color="danger" value="1" onchange="smsGlobalSetting('10')"></td>
						      	</tr>
						    </tbody>
						</table>
						
					</div>
					<div id="smsSetting" class="tab-pane fade">
						{{Form::open(array('url'=>"sms/management",'class'=>'form-horizontal form-bordered','method'=>'post'))}}
						<div class="col-md-12 sms-column ">
			                <label class="col-md-3">Operator Name: <span class="required">*</span></label>
			                <div class="col-md-8">
			                    {!! Form::text('name', null,['class'=>'form-control','placeholder'=>'Operator Name']) !!}
			                    <br>
			                </div>
			            </div>

			            <div class="col-md-12 sms-column ">
			                <label class="col-md-3">Operator Short Code: <span class="required">*</span></label>
			                <div class="col-md-8">
			                    {!! Form::text('code', null,['class'=>'form-control','placeholder'=>'Short code']) !!}
			                    <br>
			                </div>
			            </div>

						<div class="col-md-12 sms-column ">
			                <label class="col-md-3">Username: <span class="required">*</span></label>
			                <div class="col-md-8">
			                    {!! Form::text('usrname', null,['class'=>'form-control','placeholder'=>'usrname']) !!}
			                    <br>
			                </div>
			            </div>

			            <div class="col-md-12 sms-column ">
			                <label class="col-md-3">Password: <span class="required">*</span></label>
			                <div class="col-md-8">
			                    {!! Form::text('password', null,['class'=>'form-control','placeholder'=>'Password here']) !!}
			                    <br>
			                </div>
			            </div>

			            <div class="col-md-12 sms-column ">
			                <label class="col-md-3">Sender: <span class="required">*</span></label>
			                <div class="col-md-8">
			                    {!! Form::text('sender', null,['class'=>'form-control','placeholder'=>'Sender']) !!}
			                    <br>
			                </div>
			            </div>

			            <div class="col-md-12 sms-column ">
			                <label class="col-md-3">Username Param: <span class="required">*</span></label>
			                <div class="col-md-8">
			                    {!! Form::text('url', null,['class'=>'form-control','placeholder'=>'username param']) !!}
			                    <br>
			                </div>
			            </div>

			            <div class="col-md-12 sms-column ">
			                <label class="col-md-3">Password Param: <span class="required">*</span></label>
			                <div class="col-md-8">
			                    {!! Form::text('password_param', null,['class'=>'form-control','placeholder'=>'Password Param']) !!}
			                    <br>
			                </div>
			            </div>

			            <div class="col-md-12 sms-column ">
			                <label class="col-md-3">From Param: <span class="required">*</span></label>
			                <div class="col-md-8">
			                    {!! Form::text('from_param', null,['class'=>'form-control','placeholder'=>'From Param']) !!}
			                    <br>
			                </div>
			            </div>

			            <div class="col-md-12 sms-column ">
			                <label class="col-md-3"> To Param: <span class="required">*</span></label>
			                <div class="col-md-8">
			                    {!! Form::text('to_param', null,['class'=>'form-control','placeholder'=>'To Param','col'=>'2','row'=>'2']) !!}
			                    <br>
			                </div>
			            </div>

						<div class="col-md-12 sms-column ">
			                <label class="col-md-3"> Message Param:</label>
			                <div class="col-md-8">
			                    {!! Form::text('message_param', null,['class'=>'form-control','placeholder'=>'Message Param','col'=>'2','row'=>'2']) !!}
			                    <br>
			                </div>
			            </div>

			            <div class="col-md-12 text-center">
			                {!! Form::submit('Submit', ['class'=>'btn btn-primary']) !!}
			                <button class="btn btn-danger" type="reset">Reset</button>
			            </div>

			            {{ Form::close() }}


						<div class="table-responsove">
						    <table class="table table-hover table-bordered" id="tableresponsive">
						        <thead class="table-header-bg">
						            <tr>
						                <th><span class="box-info-sum">S/N</span></th>
						                <th><span class="box-info-sum">Operator Name </span></th>
						                <th><span class="box-info-sum">Opearator code</span></th>
						                <th><span class="box-info-sum">Username</span></th>
						                <th><span class="box-info-sum">password</span></th>
						                <th><span class="box-info-sum">sender</span></th>
						                <th><span class="box-info-sum">url</span></th>
						                <th><span class="box-info-sum">Username Param</span></th>
						                <th><span class="box-info-sum">Password Param</span></th>
						                <th><span class="box-info-sum">From Param</span></th>
						                <th><span class="box-info-sum">To Param</span></th>
						                <th><span class="box-info-sum">Message Param</span></th>
						                <th><span class="box-info-sum">Active</span></th>
						                <th><span class="box-info-sum">Action</span></th>
						            </tr>
						        </thead>
						        <tbody>
							        <?php 
							            $sl =1;
							        ?>
							        @foreach($smsSettings as $info)
							            <tr class="setting_{{ $info->id }}">
							                <td><span class="box-info-sum">{{ $sl++ }}</span></td>
							                <td><span class="box-info-sum">{{ $info->operator_name}}</span></td>
							                <td><span class="box-info-sum">{{ $info->short_code}}</span></td>
							                <td><span class="box-info-sum">{{ $info->username}}</span></td>
							                <td><span class="box-info-sum">{{ $info->password}}</span></td>
							                <td><span class="box-info-sum">{{ $info->sender}}</span></td>
							                <td><span class="box-info-sum">{{ $info->url}}</span></td>
							                <td><span class="box-info-sum">{{ $info->username_param}}</span></td>
							                <td><span class="box-info-sum">{{ $info->password_param}}</span></td>
							                <td><span class="box-info-sum">{{ $info->from_param}}</span></td>
							                <td><span class="box-info-sum">{{ $info->to_param}}</span></td>
							                <td><span class="box-info-sum">{{ $info->message_param}}</span></td>
							                <td><span class="box-info-sum">{{ $info->active}}</span></td>
							            	<td class="button-section">
			                                    <span class="both-options  sms-action-button">
			                                        <a class="btn purple btn-xs" href="{{ URL::to('/sms/management/'.$info->id .'/edit') }}"><i class="fa fa-edit"></i> </a>
			                                        <a class="btn red btn-xs"  href="javascript:;" onclick="settingDelete('{{ $info->id }}','{{ $info->operator_name }}')"><i class="fa fa-trash"></i> </a>
			                                    </span>
			                                </td>
							            </tr>
							        @endforeach
							      
							    </tbody>
						    </table>
						</div>
							
					</div>
					<div id="smsEnable" class="tab-pane fade @if (session('status')==2 )  in active @endif">
						 {{Form::open(array('url'=>"sms/global/setting",'class'=>'form-horizontal form-bordered','method'=>'post'))}}
							
							<div class="sms-setup">
								<div class="col-md-12">
					                <label class="col-md-3">SMS Setting Enable:</label>
					                <div class="col-md-8 global-settings">
					                    <div class="col-md-2 col-lg-2 col-xs-12 pull-left">
											<div class="checker"><span><input class="form control" name="is_enable" value="1" type="radio" checked></span></div> Yes
										</div>
										<div class="col-md-2 col-lg-2 col-xs-12 pull-left">
											<div class="checker"><span><input class="form control" name="is_enable" value="0" type="radio"></span></div> No
										</div>
					                </div>
					            </div>
					        </div>
					            <div class="col-md-12 text-center">
					                {!! Form::submit('Submit', ['class'=>'btn btn-primary']) !!}
					                
					            </div>
								<div class="table-responsove">
								    <table class="table table-hover table-bordered" id="tableresponsive">
								        <thead class="table-header-bg">
								            <tr>
								                <th><span class="box-info-sum">S/N</span></th>
								                <th><span class="box-info-sum">Status </span></th>
								                <th><span class="box-info-sum">Create </span></th>
								                <th><span class="box-info-sum">Action</span></th>
								            </tr>
								        </thead>
								        <tbody>
									        <?php 
									            $sl =1;
									        ?>
									        @foreach($smsEnable as $info)
									            <tr class="global_{{ $info->id }}">
									                <td><span class="box-info-sum">{{ $sl++ }}</span></td>
									                <td>
									                	<span class="box-info-sum">
									                		@if($info->is_enable == 1)
									                			<button class="btn btn-success"> Enable</button>
									                		@else
																<button class="btn btn-danger"> Disable</button>
									                		@endif
									                	</span>
									                </td>
									                <td><span class="box-info-sum">
									                	{{ $info->created_at}}
									                	</span>
									                </td>
									                <td class="button-section">
					                                    <span class="both-options  sms-action-button">
					                                        <a class="btn purple btn-xs" href="{{ URL::to('/sms/global/setting/'.$info->id .'/edit') }}"><i class="fa fa-edit"></i> </a>
					                                        <a class="btn red btn-xs"  href="javascript:;" onclick="globalSettingDelete('{{ $info->id }}','{{ $info->is_enable }}')"><i class="fa fa-trash"></i> </a>
					                                    </span>
					                                </td>
									            </tr>
									        @endforeach
									      
									    </tbody>
								    </table>
							    </div>
						
			            {{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('backend.settings.sms.sms-delete')
@stop