@extends( 'backend.index' )
@section( 'content_area' )

<div class="bg-title">
    <div class="page-title reports-title">
        <ul class="breadcrumb  col-md-5 col-xs-12 col-lg-5 col-sm-5 pull-left">
            <li class="completed"><a href="javascript:void(0);">Dashboard </a></li>
            <li class="completed"><a href="javascript:void(0);"> Reports </a></li>
            <li  class="active"><a href="javascript:void(0);"> Send Mobile Notification to All Clients</a></li>
        </ul>
	</div>
</div>
<div id="main-wrapper">
	<div class="row">
	    <div class="col-md-2">
	        <a href="#" class="btn btn-success btn-block">Back to Inbox</a>
	    </div>
	    <div class="col-md-6">
	        <h2>Compose</h2>
	    </div>
	    <div class="col-md-4">
	        <div class="compose-options">
	            <div class="pull-right">
	                <a href="#" class="btn btn-default"><i class="fa fa-file-text-o m-r-xs"></i>Draft</a>
	                <a href="#" class="btn btn-danger"><i class="fa fa-trash m-r-xs"></i>Discard</a>
	            </div>
	        </div>
	    </div>
    </div>

	<div class="row">
		<div class="col-md-2 col-sm-2 col-lg-2">
			      
			<ul class="list-unstyled mailbox-nav">
                <li><a href="#"><i class="fa fa-inbox"></i>Inbox <span class="badge badge-success pull-right">4</span></a></li>
                <li><a href="#"><i class="fa fa-sign-out"></i>Sent</a></li>
                <li><a href="#"><i class="fa fa-file-text-o"></i>Draft</a></li>
                <li><a href="#"><i class="fa fa-exclamation-circle"></i>Spam</a></li>
                <li><a href="#"><i class="fa fa-trash"></i>Trash</a></li>
            </ul>
		</div>
		 {{ csrf_field() }}
		<div class="col-md-10 col-sm-10 col-lg-10 sms-category pull-right">
			<!-- {!! Form::open(array('url'=>'send-client-sms','role'=>'form', 'data-toggle'=>'validator','method'=>'POST'))!!} -->
			  	<div class="form-group row">
				    <label class="col-md-3 col-xs-3 col-sm-3 col-lg-3" for="email">SMS Category:</label class="col-md-3 col-xs-3 col-sm-3 col-lg-3">
				    <div class="btn-group bootstrap-select dropup col-md-9 col-xs-9 col-sm-9 col-lg-9">
						<select class="selectpicker chosen-select-width" name="sms_category" id="sms_category" >
				    	   <optgroup label class="col-md-3 col-xs-3 col-sm-3 col-lg-3">
								<option value="">Select SMS Category</option>
								@foreach($smsType as $smsInfo)
									<option value="{{ $smsInfo->module_code}}">{{ $smsInfo->module_name}}</option>
								@endforeach
						    </optgroup>
						  </select>
					</div>
			  	</div>
			 	<div class="form-group row">
				    <label class="col-md-3 col-xs-3 col-sm-3 col-lg-3" for="email">Region:</label class="col-md-3 col-xs-3 col-sm-3 col-lg-3">
				    <div class="btn-group bootstrap-select dropup col-md-9 col-xs-9 col-sm-9 col-lg-9">
						<select class="selectpicker chosen-select-width" name="sms_region" id="sms_region" tabindex="-98">
				    	   <optgroup label class="col-md-3 col-xs-3 col-sm-3 col-lg-3">
								<option value="">Select Region </option>
								@foreach($regionInfo as $region)
									<option value="{{ $region->region_id}}">{{ $region->region_name}}</option>
								@endforeach
						    </optgroup>
						  </select>
					</div>
			  	</div>
			  	<div class="form-group row">
				    <label class="col-md-3 col-xs-3 col-sm-3 col-lg-3" for="email">Month:</label class="col-md-3 col-xs-3 col-sm-3 col-lg-3">
				    <div class="btn-group bootstrap-select dropup col-md-9 col-xs-9 col-sm-9 col-lg-9">
						<select class="selectpicker  chosen-select-width" name="month" tabindex="-98">
						    <option value="January"> January</option>
							   @for($m = 1;$m <= 12; $m++){
	                                <?php $month =  date("F", mktime(0, 0, 0, $m)); ?>
	                                <option value='{{ $month }}' @if($month == $currentMonth ) selected @endif>{{ $month }}</option> 
	                            @endfor	
						</select>
					</div>
			  	</div>
			  	<div class="form-group row">
				    <label class="col-md-3 col-xs-3 col-sm-3 col-lg-3" for="email">Year:</label class="col-md-3 col-xs-3 col-sm-3 col-lg-3">
				    <div class="btn-group bootstrap-select dropup col-md-9 col-xs-9 col-sm-9 col-lg-9">
						<select class="selectpicker  chosen-select-width" name="year" tabindex="-98">
						    @for($year = 2016; $year < date("Y") + 1; $year++){
                                <option value="{{ $year }}"@if($year == $currentYear) selected @endif> {{ $year }} </option> 
                            @endfor
					  	</select>
					</div>
			  	</div>
			  	<div class="form-group row">
				    <label class="col-md-3 col-xs-3 col-sm-3 col-lg-3" for="email">Message:</label class="col-md-3 col-xs-3 col-sm-3 col-lg-3">
				    <div class="btn-group bootstrap-select dropup col-md-9 col-xs-9 col-sm-9 col-lg-9">
						<textarea name="message" id="smsMessage"  class="form-control" cols="5" rows="2"></textarea>
					</div>
			  	</div>
			  	<button type="submit" class="btn btn-success pull-right"  onclick="smsSendCategory('{{ $smsInfo->module_name}}','{{ $smsInfo->module_code}}')"><i class="fa fa-send m-r-xs"></i> Send SMS</button>
			<!-- {!!Form::close()!!} -->
		</div>	
	</div>	

</div>	

@include('master.confirm')
@stop