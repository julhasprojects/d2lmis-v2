@extends( 'backend.index' )
@section( 'content_area' )
	
<div class="bg-title">
	<div class="page-title reports-title">
        <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
            <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
            <li class="completed"><a href="javascript:void(0);"> Modules</a></li>
            <li class="active"><a href="javascript:void(0);"> Respones SMS </a></li>
        </ul>
	</div>		
</div>
@include('backend.settings.sms.sms-mgt.sms-menu')
@include('master.message')
<div id="main-wrapper">
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<h4 class="title-house"> SMS Respones Create</h4>
			<div class="panel-body">
			
				{{Form::open(array('url'=>"sms/response",'class'=>'form-horizontal form-bordered','method'=>'post'))}}
				<div class="col-md-12 sms-column-bar">
	                <label class="control-label col-md-2">Operator ID:</label>
	                <div class="col-md-8">
                    	<select name="id" class="chosen-select-width">
                    		<option value=""></option>
                    		@foreach($operationId as $setting)
								<option value="{{ $setting->id }}"> {{ $setting->operator_name }} </option>
                    		@endforeach	
                    	</select>
	                </div>
	            </div>
				
	            <div class="col-md-12 sms-column-bar">
	                <label class="control-label col-md-2">Response Code:</label>
	                <div class="col-md-8">
	                    {!! Form::text('code', null,['class'=>'form-control','placeholder'=>'Respones code']) !!}
	                    
	                </div>
	            </div>

	            <div class="col-md-12 sms-column-bar">
	                <label class="control-label col-md-2"> Response Details:</label>
	                <div class="col-md-8">
	                    {!! Form::textarea('details', null,['class'=>'form-control responive-details','placeholder'=>'Response Detials','cols'=>'2','rows'=>'2']) !!}
	                </div>
	            </div>

	            <div class="col-md-12 text-center">
	                {!! Form::submit('Submit', ['class'=>'btn btn-primary']) !!}
	                <button class="btn btn-danger" type="reset">Reset</button>
	            </div>
	            {{ Form::close() }}
			</div>
		</div>
	</div>
	<div class="col-md-12 left-box sms-box">
		<div class="panel panel-white">
			<h4 class="title-house"> SMS Respones</h4>
			<div class="panel-body">	
				 <div class="table-responsove">
				    <table class="table table-hover table-bordered" id="tableresponsive">
				        <thead class="table-header-bg">
				            <tr>
				                <th><span class="box-info-sum">S/N</span></th>
				                <th><span class="box-info-sum"> ID</span></th>
				                <th><span class="box-info-sum">Respones</span></th>
				                <th><span class="box-info-sum">Details</span></th>
				                <th><span class="box-info-sum">Created At</span></th>
				                <th><span class="box-info-sum">Action</span></th>
				            </tr>
				        </thead>
				        <tbody>
					        <?php 
					            $sl =1;
					        ?>
					        @foreach($smsRespones as $info)
					            <tr class="respones_{{ $info->id }}">
					                <td><span class="box-info-sum">{{ $sl++ }}</span></td>
					                <td><span class="box-info-sum">{{ $info->operator_id}}</span></td>
					                <td><span class="box-info-sum">{{ $info->response_code}}</span></td>
					                <td><span class="box-info-sum">{{ $info->response_details}}</span></td>
					                <td><span class="box-info-sum">{{ $info->created_at}}</span></td>
					            	<td class="button-section">
	                                    <span class="both-options">
	                                        <a class="btn purple btn-xs" href="{{ URL::to('/sms/response/'.$info->id .'/edit') }}"><i class="fa fa-edit"></i> </a>
	                                        <a class="btn red btn-xs"  href="javascript:;" onclick="responseDelete('{{ $info->id }}','{{ $info->operator_id }}')"><i class="fa fa-trash"></i> </a>
	                                    </span>
	                                </td>
					            </tr>
					        @endforeach
					      
					    </tbody>
				    </table>
				</div>
			</div>
		</div>
	</div>
</div>
@include('backend.settings.sms.sms-delete')
@stop