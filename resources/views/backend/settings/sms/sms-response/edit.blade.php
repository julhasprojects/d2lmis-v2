@extends( 'backend.index' )
@section( 'content_area' )
	
<div class="bg-title">
	<div class="page-title reports-title">
        <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
            <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
            <li class="completed"><a href="javascript:void(0);"> Modules</a></li>
            <li class="active"><a href="javascript:void(0);"> Respones SMS Update</a></li>
        </ul>
	</div>		
</div>
@include('backend.settings.sms.sms-mgt.sms-menu')
@include('master.message')
<div id="main-wrapper">
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<h4 class="title-house">Respones SMS Update</h4>
			<div class="panel-body">
			
				 {{Form::open(array('url'=>"sms/response/".$smsRespones->id,'class'=>'form-horizontal form-bordered','method'=>'PUT'))}}
				
				<div class="col-md-12 sms-column-bar">
	                <label class="control-label col-md-2">Operator ID:</label>
	                <div class="col-md-8">
                    	<select name="id" class="chosen-select-width">
                    		<option value=""></option>
                    		@foreach($operationId as $setting)
								<option value="{{ $setting->id }}" @if($smsRespones->operator_id == $setting->id) selected @endif > {{ $setting->operator_name }} </option>
                    		@endforeach	
                    	</select>
	                </div>
	            </div>
			

	            <div class="col-md-12">
	                <label class="control-label col-md-2">Response Code:</label>
	                <div class="col-md-8">
	                    {!! Form::text('code', $smsRespones->response_code,['class'=>'form-control ','placeholder'=>'Respones code']) !!}
	                    <br>
	                </div>
	            </div>

	            <div class="col-md-12">
	                <label class="control-label col-md-2"> Response Details:</label>
	                <div class="col-md-8">
	                    {!! Form::textarea('details',  $smsRespones->response_details,['class'=>'form-control','placeholder'=>'Response Detials','cols'=>'2','rows'=>'2']) !!}
	                    <br>
	                </div>
	            </div>

	            <div class="col-md-12 text-center">
	                {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
	            </div>
	            {{ Form::close() }}
			</div>
		</div>
	</div>
	
</div>
@stop