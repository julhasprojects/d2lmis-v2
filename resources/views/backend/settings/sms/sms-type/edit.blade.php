@extends( 'backend.index' )
@section( 'content_area' )
	
<div class="bg-title">
	<div class="page-title reports-title">
        <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
            <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
            <li class="completed"><a href="javascript:void(0);"> Modules</a></li>
            <li class="active"><a href="javascript:void(0);"> Update SMS Type </a></li>
        </ul>
	</div>		
</div>
@include('backend.settings.sms.sms-mgt.sms-menu')
@include('master.message')
@include('master.error')
<div id="main-wrapper">
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<h4 class="title-house">SMS Type Update</h4>
			<div class="panel-body">
			
				{{Form::open(array('url'=>"sms/type/setup/".$smsTypes->id,'class'=>'form-horizontal form-bordered','method'=>'PUT'))}}
					<div class="col-md-12 form-group-send">
		                <label class="control-label col-md-2">Module Name:<span class="required">*</span></label>
		                <div class="col-md-8">
		                    {!! Form::text('name', $smsTypes->module_name,['class'=>'form-control','placeholder'=>'Module Name']) !!}
		                </div>
		            </div>

		            <div class="col-md-12 form-group-send">
		                <label class="control-label col-md-2">Module Code:<span class="required">*</span></label>
		                <div class="col-md-8">
		                    {!! Form::text('code', $smsTypes->module_code,['class'=>'form-control','placeholder'=>'Module code']) !!}
		                </div>
		            </div>
					<div class="col-md-12 form-group-send">
		                <label class="control-label col-md-2"> Message:<span class="required">*</span></label>
		                <div class="col-md-8">
		                    {!! Form::textarea('message', $smsTypes->message,['class'=>'form-control','placeholder'=>'Module Message','cols'=>'2','rows'=>'2']) !!}
		                </div>
		            </div>
		            <div class="col-md-12 form-group-send">
		                <label class="control-label col-md-2"> Status:</label>
		                <div class="col-md-8">
				            <select class="form-control select-form-desing" name="status" id="status">
			                    <option value="0">Select  Status</option>
			                    <option value="1" @if( $smsTypes->active_status==1) selected @endif>Active</option>
			                    <option value="0"  @if( $smsTypes->active_status==0) selected @endif>Deactive</option>
			                </select>
			            </div>
			        </div>
		            <div class="col-md-12 text-center">
		                {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
		            </div>
	            {{ Form::close() }}
			</div>
		</div>
	</div>

</div>
@stop