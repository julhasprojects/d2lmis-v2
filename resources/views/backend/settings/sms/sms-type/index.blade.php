@extends( 'backend.index' )
@section( 'content_area' )

<div class="bg-title">
	<div class="page-title reports-title">
        <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
            <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
            <li class="completed"><a href="javascript:void(0);"> Modules</a></li>
            <li class="active"><a href="javascript:void(0);"> SMS Type Setup </a></li>
        </ul>
	</div>		
</div>
<div id="deletenotification"></div>
@include('backend.settings.sms.sms-mgt.sms-menu')
@include('master.message')
@include('master.error')	
<div id="main-wrapper">
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<h4 class="title-house">Respones SMS Create</h4>
			<div class="panel-body">
			
				{{Form::open(array('url'=>"sms/type/setup",'class'=>'form-horizontal form-bordered','method'=>'post'))}}
				<div class="col-md-12">
	                <label class="control-label col-md-2">Module Name: <span class="required">*</span></label>
	                <div class="col-md-8">
	                    {!! Form::text('name', null,['class'=>'form-control','placeholder'=>'Module Name']) !!}
	                    <br>
	                </div>
	            </div>

	            <div class="col-md-12">
	                <label class="control-label col-md-2">Module Code: <span class="required">*</span></label>
	                <div class="col-md-8">
	                    {!! Form::text('code', null,['class'=>'form-control','placeholder'=>'Module code']) !!}
	                    <br>
	                </div>
	            </div>
				<div class="col-md-12">
	                <label class="control-label col-md-2"> Message: <span class="required">*</span></label>
	                <div class="col-md-8">
	                    {!! Form::textarea('message', null,['class'=>'form-control','placeholder'=>'Module Message','cols'=>'2','rows'=>'2']) !!}
	                    <br>
	                </div>
	            </div>
	            <div class="col-md-12">
	                <label class="control-label col-md-2"> Status:</label>
	                <div class="col-md-8">
			            <select class="form-control select-form-desing" name="status" id="status">
		                    <option value="0">Select  Status</option>
		                    <option name="scomtype" value="1">Active</option>
		                    <option name="scomtype" value="0">Deactive</option>
		                </select>
		            </div>
		        </div>
		        <br>
		        <div class="row sms-mgt">
		            <div class="col-md-12 text-center">
		                {!! Form::submit('Submit', ['class'=>'btn btn-primary']) !!}
		                <button class="btn btn-danger" type="reset">Reset</button>
		            </div>
	            </div>
	            {{ Form::close() }}
			</div>
			
		</div>
	</div>
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<h4 class="title-house">Setting</h4>
			<div class="panel-body">	
				 <div class="table-responsove">
				    <table class="table table-hover table-bordered" id="tableresponsive">
				        <thead class="table-header-bg">
				            <tr>
				                <th><span class="box-info-sum">S/N</span></th>
				                <th><span class="box-info-sum">Module Name </span></th>
				                <th><span class="box-info-sum">Module code</span></th>
				                <th><span class="box-info-sum">Message</span></th>
				                <th><span class="box-info-sum">Status</span></th>
				                <th><span class="box-info-sum">Action</span></th>
				            </tr>
				        </thead>
				        <tbody>
					        <?php 
					            $sl =1;
					        ?>
					        @foreach($smsTypes as $info)
					            <tr class="setting_{{ $info->id }}">
					                <td><span class="box-info-sum">{{ $sl++ }}</span></td>
					                <td><span class="box-info-sum">{{ $info->module_name}}</span></td>
					                <td><span class="box-info-sum">{{ $info->module_code}}</span></td>
					                <td><span class="box-info-sum">{{ $info->message}}</span></td>
					                <td>
					                	<span class="box-info-sum">
					                		@if($info->active_status == 1)
					                			<button class="btn btn-success btn-xs"> Active</button>
					                		@else
					                			<button class="btn btn-warning btn-xs"> Deactive</button>
					                		@endif
					                	</span>
					                </td>
					            	<td class="button-section">
	                                    <span class="both-options  sms-action-button">
	                                        <a class="btn purple btn-xs" href="{{ URL::to('/sms/type/setup/'.$info->id .'/edit') }}"><i class="fa fa-edit"></i> </a>
	                                        <a class="btn red btn-xs"  href="javascript:;" onclick="smsTypeSetup('{{ $info->id }}','{{ $info->module_name }}')"><i class="fa fa-trash"></i> </a>
	                                    </span>
	                                </td>
					            </tr>
					        @endforeach
					      
					    </tbody>
				    </table>
				</div>
			</div>
		</div>
	</div>
</div>
@include('backend.settings.sms.sms-delete')
@stop