@extends( 'backend.index' )
@section( 'content_area' )
	
<div class="bg-title">
	<div class="page-title reports-title">
        <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
            <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
            <li class="completed"><a href="javascript:void(0);"> Modules</a></li>
            <li class="active"><a href="javascript:void(0);"> SMS History </a></li>
        </ul>
	</div>		
</div>
@include('backend.settings.sms.sms-mgt.sms-menu')
<div id="main-wrapper">
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<h4 class="title-house">Send Custom SMS</h4>
			<div class="panel-body">	
				 <div class="table-responsove">
				    <table class="table table-hover table-bordered" id="tableresponsive">
				        <thead class="table-header-bg">
				            <tr>
				                <th><span class="box-info-sum">S/N</span></th>
				                <th><span class="box-info-sum">Client Id</span></th>
				                <th><span class="box-info-sum">Mobile Number</span></th>
				                <th><span class="box-info-sum">Type</span></th>
				                <th><span class="box-info-sum">Message</span></th>
				                <th><span class="box-info-sum">Created At</span></th>
				            </tr>
				        </thead>
				        <tbody>
					        <?php 
					            $sl =1;
					        ?>
					        @foreach($smsHistory as $info)
					            <tr>
					                <td><span class="box-info-sum">{{ $sl++ }}</span></td>
					                <td><span class="box-info-sum">{{ $info->newconid}}</span></td>
					                <td><span class="box-info-sum">{{ $info->mobile}}</span></td>
					                <td><span class="box-info-sum">{{ $info->module_name}}</span></td>
					                <td><span class="box-info-sum">{{ $info->message}}</span></td>
					                <td><span class="box-info-sum">{{ $info->created_at}}</span></td>
					            </tr>
					        @endforeach
					      
					    </tbody>
				    </table>
				</div>
			</div>
		</div>
	</div>
</div>
@stop