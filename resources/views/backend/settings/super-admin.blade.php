@extends( 'backend.index' )


@section( 'content_area' )
<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);">Settings</a></li>
        <li class="active"><a href="javascript:void(0);">Super admin setting</a></li>
    </ul>
</div>
@include('backend.settings.settings-menu')

<div id="main-wrapper">

	<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 left-box setting-box">
		<div class="panel-white">
			<h2 class="title-house">New line package cost caleulation</h2> 
			<div class="panel-body box-body">
				<div class="form-group box-group">
					<label for="packagecost" class="col-sm-4 label-box control-label">Package</label>
					<div class="col-sm-8">
						<select name="packagecost" id="packagecost" class="chosen-select-width packagecost">
							<option value="Daily">Daily</option>
							<option value="Monthly" selected=""> Monthly</option>
						</select>
					</div>
				</div>
				<div class="button-submit-area">
					<button class="btn btn-success submit-setting-button"> Submit </button>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 left-box setting-box">
		<div class="panel-white">
			<h2 class="title-house">Line auto disconnect</h2> 
			<div class="panel-body box-body">
				<div class="form-group box-group">
					<label for="packagecost" class="col-sm-4 label-box control-label">Disconnect Date</label>
					<div class="col-sm-8">
						<div class="form-group">
                            <div class="input-group">
                                <input class="form-control bbibillfrom" data-date-format="yyyy-mm-dd" name="datefrom" id="newlineDateForm" placeholder="Disconnect Date" required="" type="text">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
					</div>
				</div>
				<div  class="clearfix"></div>
				<div class="form-group box-group">
					<label for="clientname" class="col-sm-4 label-box control-label">Type</label>
					<div class="col-sm-8">
						<select name="" id="" class="chosen-select-width packagecost">
							<option value="Disconnect">Disconnect</option>
							<option value="No Disconnect" selected=""> No Disconnect</option>
						</select>
					</div>
				</div>
				<div  class="clearfix"></div>
				<div class="button-submit-area">
					<button class="btn btn-success submit-setting-button"> Submit </button>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 left-box setting-box">
		<div class="panel-white">
			<h2 class="title-house">Discount for earlier bill payment</h2> 
			<div class="panel-body box-body">
				<div class="form-group box-group">
					<label for="payment" class="col-sm-4 label-box control-label">Last day of payment</label>
					<div class="col-sm-8">
						<select name="" id="" class="chosen-select-width packagecost">
							@for($i=1; $i<='31'; $i++) 
								<option>{{ $i }}</option>
							@endfor
						</select>		
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="form-group box-group">
					<label for="clientname" class="col-sm-4 label-box control-label">Discount Type</label>
					<div class="col-sm-8">
						<select name="" id="" class="chosen-select-width packagecost">
							<option value="Flate Rate">Flate Rate</option>
							<option value="%" selected=""> %</option>
						</select>
					</div>
				</div>
				<div  class="clearfix"></div>
				<div class="form-group box-group">
					<label for="clientname" class="col-sm-4 label-box control-label">Amount</label>
					<div class="col-sm-8">
						<input type="text" class="amount form-control">
					</div>
				</div>
				<div  class="clearfix"></div>
				<div class="button-submit-area">
					<button class="btn btn-success submit-setting-button"> Submit </button>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 left-box setting-box">
		<div class="panel-white">
			<h2 class="title-house">Payment Method Setup</h2> 
			<div class="panel-body box-body">
				<div class="row method-accounts-row">
					<div class="form-group box-group">
						<div class="col-md-3 col-xs-12 padding-left-zero">
							 <img src="{{ URL::asset( 'assets/images/BKash_logo.png' ) }}" class="img-responsive payment-logo" alt="" >                    
						</div>
						<div class="col-md-9 col-xs-12 padding-left-zero">
							<div class="form-group box-group">
								<label for="clientname" class="col-sm-4 label-box control-label padding-left-zero">API LINK</label>
								<div class="col-sm-8">
									<input class="form-control" id="api_link" name="api_link" value=" " type="text">
								</div>
							</div>
							<div class="form-group box-group">
								<label for="clientname" class="col-sm-4 label-box control-label padding-left-zero">Mathod acccount name</label>
								<div class="col-sm-8">
									<input class="form-control" id="method" name="method" value="" type="text">
								</div>
							</div>
						</div>
					</div>	
					
					<div class="clearfix"></div>
				</div>
				<hr>
				<div class="row method-accounts-row">
					<div class="form-group box-group">
						<div class="col-md-3 col-xs-12">
							 <img src="{{ URL::asset( 'assets/images/rocket.jpg' ) }}" class="img-responsive payment-logo" alt="" >                   
						</div>
						
						<div class="col-md-9 col-xs-12 padding-left-zero">
							<div class="form-group box-group">
								<label for="clientname" class="col-sm-4 label-box control-label padding-left-zero">API LINK</label>
								<div class="col-sm-8">
									<input class="form-control" id="api_link" name="api_link" value=" " type="text">
								</div>
							</div>
							<div class="form-group box-group">
								<label for="clientname" class="col-sm-4 label-box control-label padding-left-zero">Mathod acccount name</label>
								<div class="col-sm-8">
									<input class="form-control" id="method" name="method" value="" type="text">
								</div>
							</div>
						</div>
						
					</div>	
				</div>
				<hr>
				<div class="row method-accounts-row">
					<div class="clearfix"></div>
					
					<div class="form-group box-group">
						<div class="col-md-3 col-xs-12">
							 <img src="{{ URL::asset( 'assets/images/paypalIcon.png' ) }}" class="img-responsive payment-logo" alt="" >                   
						</div>
						<div class="col-md-9 col-xs-12 padding-left-zero">
							<div class="form-group box-group">
								<label for="clientname" class="col-sm-4 label-box control-label padding-left-zero">API LINK</label>
								<div class="col-sm-8">
									<input class="form-control" id="api_link" name="api_link" value=" " type="text">
								</div>
							</div>
							<div class="form-group box-group">
								<label for="clientname" class="col-sm-4 label-box control-label padding-left-zero">Mathod acccount name</label>
								<div class="col-sm-8">
									<input class="form-control" id="method" name="method" value="" type="text">
								</div>
							</div>
						</div>
					</div>	
				</div>
			
				
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	
</div>


@stop