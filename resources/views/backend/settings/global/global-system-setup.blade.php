@extends( 'backend.index' )
@section( 'content_area' )
<div class="page-title reports-title">
<ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
<li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
<li class="completed"><a href="javascript:void(0);">Settings</a></li>
<li class="active"><a href="javascript:void(0);">Global System Settings</a></li>
</ul>
</div>
{{ csrf_field() }}
<div id="main-wrapper">
<div class="col-md-12 left-box">
<div class="panel-white">
<div id="mobile-script"></div>
<div class="panel-body">
	<div class="table-responsive"> 
		<table class="table table-hover table-bordered">
            <thead class="table-header-bg">
				<tr>
					<th>Setting's Name</th>
					<th>Action</th>
					<th>View</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Is microtik enable for this company?</td>
					<td>
						<div class="global-setting">
							<div class="checkbox">
                                <span class="checkbox-input">
                                	<input type="checkbox" id="globalSetting_1" onChange="globalSettingEnable('1','1','Is Microtiks enable for this company?')" class="make-switch" data-on-color="success" data-on-text="Yes" data-off-text="No" data-off-color="danger" value="1" @if($smsEnable->is_enable=='1' ) checked @endif >
                                </span>
                            </div>
                        </div>
                    </td>
					<td>	
						<a href="{{ URL::to( '/mikrotik/integration' ) }}" target="_blank" class="btn btn-warning button-dynamic">
							<i class="fa fa-eye"> </i>
						</a>
					</td>
				</tr>
				<tr>
					<td>Is sms notificaton enable for this company?</td>
					<td>
						<div class="global-setting">
							<div class="checkbox">
                                <span class="checkbox-input">
                                	<input type="checkbox" id="globalSetting_2"  onChange="globalSettingEnable('2','1','Is SMS enable for this company?')"  class="make-switch" data-on-color="success" data-on-text="Yes" data-off-text="No" data-off-color="danger" value="1"  @if($smsEnable->is_enable=='1' ) checked @endif>
                                </span>
                            </div>
                        </div>
                    </td>
				
					<td>	
						<a href="{{ URL::to( '/sms/management' ) }}" target="_blank" class="btn btn-warning button-dynamic">
							<i class="fa fa-eye"> </i>
						</a>
					</td>
				</tr>
				<tr>
					<td>Is email notification enable for this company?</td>
					<td>
						<div class="global-setting">
							<div class="checkbox">
                                <span class="checkbox-input">
                                	<input type="checkbox" id="globalSetting_3"  onChange="globalSettingEnable('3','1','Is SMS enable for this company?')"  class="make-switch" data-on-color="success" data-on-text="Yes" data-off-text="No" data-off-color="danger" value="1"  @if($emailEnable->is_enable=='1' ) checked @endif>
                                </span>
                            </div>
                        </div>
                    </td>
					<td>	
						<a href="{{ URL::to( '/sms/management' ) }}" target="_blank" class="btn btn-warning button-dynamic">
							<i class="fa fa-eye"> </i>
						</a>
					</td>
				</tr>
				<tr>
					<td>Do you want to allow inventory?</td>
					<td>
						<div class="global-setting">
							<div class="checkbox">
                                <span class="checkbox-input">
                                	<input type="checkbox" id="globalSetting_4"  onChange="globalSettingEnable('4','1','Is SMS enable for this company?')"  class="make-switch" data-on-color="success" data-on-text="Yes" data-off-text="No" data-off-color="danger" value="1" @if($inventory->is_enable=='1' ) checked @endif >
                                </span>
                            </div>
                        </div>
                    </td>
					<td>	
						<a href="{{ URL::to( '/global/system/settings' ) }}" target="_blank" class="btn btn-warning button-dynamic">
							<i class="fa fa-eye"> </i>
						</a>
					</td>
				</tr>
				<tr>
					<td>Is SSL payment gateway enable fot this company? </td>
					<td>
						<div class="global-setting">
							<div class="checkbox">
                                <span class="checkbox-input">
                                	<input type="checkbox"  onChange="globalSettingEnable('3','1','Is SMS enable for this company?')"  class="make-switch" data-on-color="success" data-on-text="Yes" data-off-text="No" data-off-color="danger" value="1">
                                </span>
                            </div>
                        </div>
                    </td>
					<td>	
						<a href="{{ URL::to( '/super/admin' ) }}" target="_blank" class="btn btn-warning button-dynamic">
							<i class="fa fa-eye"> </i>
						</a>
					</td>
				</tr>
				
				<tr>
					<td>What is the billing process cost for this company?</td>
					<td>
						<div class="global-setting">
							<div class="checkbox">
                                <span class="checkbox-input">
                                	<input type="checkbox"  onChange="globalSettingEnable('5','1','Is SMS enable for this company?')"  class="make-switch" data-on-color="success" data-on-text="Daily" data-off-text="Monthly" data-off-color="info" value="1">
                                </span>
                            </div>
                        </div>
                    </td>
					<td>	
						<a href="{{ URL::to( '/bill/date/and/system/setup' ) }}" target="_blank" class="btn btn-warning button-dynamic">
							<i class="fa fa-eye"> </i>
						</a>
					</td>
				</tr>
				<tr>
					<td>Do you allow package cost for this company?</td>
					<td>
						<div class="global-setting">
							<div class="checkbox">
                                <span class="checkbox-input">
                                	<input type="checkbox"  onChange="globalSettingEnable('6','1','Is SMS enable for this company?')"  class="make-switch" data-on-color="success" data-on-text="Yes" data-off-text="No" data-off-color="danger" value="1">
                                </span>
                            </div>
                        </div>
                    </td>
					<td>	
						<a href="{{ URL::to( '/setup/new/package' ) }}" target="_blank" class="btn btn-warning button-dynamic">
							<i class="fa fa-eye"> </i>
						</a>
					</td>
				</tr>
				<tr>
					<td>Do you want to allow real ip cost?</td>
					<td>
						<div class="global-setting">
							<div class="checkbox">
                                <span class="checkbox-input">
                                	<input type="checkbox" id="globalSetting_3" onChange="globalSettingEnable('3','1','Is SMS enable for this company?')"  class="make-switch" data-on-color="success" data-on-text="Yes" data-off-text="No" data-off-color="danger" value="1">
                                </span>
                            </div>
                        </div>
                    </td>
					<td>	
						<a href="{{ URL::to( '/global/system/settings' ) }}" target="_blank" class="btn btn-warning button-dynamic">
							<i class="fa fa-eye"> </i>
						</a>
					</td>
				</tr>
				<tr>
					<td>Do you want to allow real online payment?</td>
					<td>
						<div class="global-setting">
							<div class="checkbox">
                                <span class="checkbox-input">
                                	<input type="checkbox"  onChange="globalSettingEnable('8','1','Is SMS enable for this company?')"  class="make-switch" data-on-color="success" data-on-text="Yes" data-off-text="No" data-off-color="danger" value="1">
                                </span>
                            </div>
                        </div>
					</td>
					<td><a href="{{ URL::to( '/global/system/settings' ) }}" target="_blank" class="btn btn-warning button-dynamic"><i class="fa fa-eye"> </i></a></td>
				</tr>
				<tr>
					<td>Do you want to mobile number same format?</td>
					<td>
						<div class="global-setting">
							<div class="checkbox">
                                <span class="checkbox-input">
                                	<input type="checkbox"  onChange="globalSettingEnable('9','1','Is SMS enable for this company?')"  class="make-switch" data-on-color="success" data-on-text="Yes" data-off-text="No" data-off-color="danger" value="1">
                                </span>
                            </div>
                        </div>
					</td>
					<td></td>
				</tr>
				<tr>
					<td>Do you want to enable defaulter module?</td>
					<td>
						<div class="global-setting">
							<div class="checkbox">
                                <span class="checkbox-input">
                                	<input type="checkbox"  onChange="globalSettingEnable('10','1','Do you want to enable defaulter module??')"  class="make-switch" data-on-color="success" data-on-text="Yes" data-off-text="No" data-off-color="danger" value="1">
                                </span>
                            </div>
                        </div>
					</td>
					<td></td>
				</tr>
				
			</tbody>
		</table>
	</div>	
</div>	
</div>
</div>
</div>

<div id="pluginConfirmModel" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-times" aria-hidden="true"></i> </button>
    <h4 class="modal-title">Confirmation</h4>
</div>
<div class="modal-body" id="info">
    <p>
         {{--Confirm Message Here from Javascript--}}
    </p>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn default">Cancel</button>
    <button type="button" data-dismiss="modal" class="btn red" id="delete">Confirm</button>
</div>
</div>
</div>
</div>
@stop