@extends( 'backend.index' )
@section( 'content_area' )
@include( 'master.message' )
<div class="bg-title">
	<div class="page-title reports-title">
        <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
            <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
            <li class="completed"><a href="javascript:void(0);"> Modules</a></li>
            <li class="active"><a href="javascript:void(0);">Bill Collector Area Assign</a></li>
        </ul>
	</div>		
</div>
<div id="main-wrapper">
	<div class="row column-box">
		<div class="col-md-12 left-box">
			<div class="panel panel-white">
				<h4 class="title-house">Assign technician working areas </h4>
				<div class="panel-body">
					{!! Form::open(array('url'=>'assign/technician/region','role'=>'form', 'data-toggle'=>'validator', 'method'=>'POST'))!!}

						<div class="col-md-6 col-xs-12 col-lg-6 employee_assign">
							<div class="form-group">
								<label for="emp_id" class="col-md-4 control-label">Employee<span class="required">*</span></label>
								<div class="col-md-8">
									<select class="chosen-select-width" id="emp_id" name="emp_id" value="{{ Input::old('emp_id') }}" required>
										<option value="">Select Employee</option>
										{{ isset($name) ? $name : 'Default' }}
										@foreach ( $employee_info as $info )
											<option value="{{ $info->employeeID }}">{{ $info->fullName}}</option>
										@endforeach 
									</select>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									<div class="help-block with-errors"></div>									
								</div>
							</div>
						</div>

						<div class="col-md-6 col-xs-12 col-lg-6 employee_assign">
							<div class="form-group">
								<label for="district_id" class="col-md-4 control-label">District<span class="required">*</span></label>
								<div class="col-md-8">
									<select class="chosen-select-width" id="district_id" name="district_id" value="{{ Input::old('district_id') }}" required>
										<option value="">Select District Name</option>
										@foreach ( $districtName as $district )
											<option value="{{ $district->district_id }}">{{ $district->district_name }}</option>
										@endforeach
									</select>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									<div class="help-block with-errors"></div>
								</div>
							</div>
						</div>

						<div class="col-md-6 col-xs-12 col-lg-6 employee_assign">
							<div class="form-group">
								<label for="upazila_id" class="col-md-4 control-label">Upazila<span class="required">*</span></label>
								<div class="col-md-8">
									<select class="chosen-select-width" id="upazila_id" name="upazila_id" value="{{ Input::old('upazila_id') }}" required>
										<option value="">Select Upazila Name</option>
										@foreach ( $upazilaName as $upazila )
											<option value="{{ $upazila->upazila_id }}">{{ $upazila->upazila_name }}</option>
										@endforeach 
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-xs-12 col-lg-6 employee_assign">
							<div class="form-group">
								<label for="Branch Name" class="col-md-4 control-label"> Branch Name </label>
								<div class="col-md-8">
									<select class="chosen-select-width" id="branchRegion" name="branch_id"  required>
										<option value="">Select Branch Name</option>
										@foreach ( $branchInfo as $branch )
											<option value="{{ $branch->id }}">{{ $branch->branch_name }}</option>
										@endforeach 
									</select>
								</div>
							</div>
						</div>

						<div class="col-md-6 col-xs-12 col-lg-6 employee_assign">
							<div class="form-group">
								<label for="region_id" class="col-md-4 control-label">Region<span class="required">*</span></label>
								<div class="col-md-8">
									<select class="form-control" id="settings_assign_region_id" name="region_id" value="{{ Input::old('region_id') }}" required>
										<option value="">Select Region Name</option>
									</select>
								</div>
							</div>
						</div>

						<div class="col-md-6 col-xs-12 col-lg-6 employee_assign">
							<div class="form-group">
								<label for="assigntype" class="col-md-4 control-label">Billman Type<span class="required">*</span></label>
								<div class="col-md-8">
									<select class="chosen-select-width" id="assigntype" name="assigntype" required>
										<option value="1"> Primary </option>
										<option value="2"> Secondary </option>
									</select>
								</div>
							</div>
						</div>

						<div class="col-md-12 col-xs-12 col-lg-12 employee_assign">
							<div class="form-group">
								<label for="region_id" class="col-md-1  col-sm-2 col-lg-1 col-xs-4 control-label">Roads<span class="required">*</span></label>
								<div class="col-md-10  col-sm-10 col-lg-10 col-xs-10 checkbox-technician pull-right" id="displayAllRoads" name="displayAllRoads" required></div>
								<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								<div class="help-block with-errors"></div>
							</div>
						</div>

						<div class="col-md-12 col-xs-12 col-lg-12 employee_assign">
						<br>
							<div class="form-group">
								<div class="col-sm-12">
									<button type="submit"  class="btn btn-primary submit-button-bg">Assign Employee Region</button>
								</div>
							</div>			
						</div>			
                    {!!Form::close()!!}
				</div>
			</div>
		</div>
	</div>
	<div class="row column-box">
		<div class="col-md-12 left-box">
			<div class="panel panel-white">
				<div class="panel-body">
					<h2 class="title-house"> Employee Assign List </h2>
					<div class="table-responsive"> 
						<table class="table table-hover table-bordered">
		                    <thead class="table-header-bg">
		                        <tr>
		                            <th> S/N </th>
		                            <th> Region Name </th>
		                            <th> Road Name </th>
		                            <th> Assigned Collector </th>
		                            <th> Created At </th>
		                            <th> Updated At </th>
		                            <!-- <th> Action </th> -->
		                        </tr>
		                    </thead>
		                    <tbody>
		                    	<?php
		                    		$sl=1;
		                    	?>
		                    	@foreach($assingEmployeer as $info)
		                    		<tr>
				                    	<td>{{ $sl++ }}</td>
				                    	<td>{{ $info->region_name }}</td> 	
				                    	<td>
					                    	<ul class="list-region-name">
												<?php $road = explode(",",$info->region_roads);  ?>
												@foreach($road as $roadInfo)
													@if(!empty($roadInfo))
														@foreach($roadList as $roadListInfo)
															@if($roadListInfo->id == $roadInfo )
																<li>{{ $roadListInfo->road }}</li>
															@endif
														@endforeach
													@endif
												@endforeach
											</ul>
				                    	</td>
				                    	<td>{{ $info->fullName }}</td>
				                    	<td>{{ $info->created_at }}</td>
				                    	<td>{{ $info->updated_at }}</td>
				                    	<!-- <td>
				                    			                                    <a href="{{ URL::to('/customer/type/setup/'.$info->id.'/edit') }}" class="glyphicon glyphicon-pencil icon-edit" >  |</a> 
				                    			                                    {!! Form::open(['url' => '/customer/type/setup/'.$info->id, 'files'=>true,'method' => 'delete','class' => 'from-delete']) !!}
				                    			                                        <button type="submit" class="delete-options"><i class="fa fa-trash-o"></i></button>
				                    			                                    {!! Form::close() !!}  
				                    			                                </td> -->
			                    	</tr>
		                    	@endforeach	              
		                    </tbody>                                 
		                </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
