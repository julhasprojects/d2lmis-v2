@extends( 'backend.index' )
@section( 'content_area' )
@include( 'master.message' )
<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);">Settings</a></li>
        <li class="active"><a href="javascript:void(0);">Updated User Role</a></li>
    </ul>
</div>
@include('backend.settings.settings-menu')
@include( 'master.error' )
<div id="main-wrapper">
	<div class="col-md-12 col-xs-12 col-sm-12 left-box">
		<div class="panel-white">
			<h2 class="title-house"> Updated Role Permission</h2>
			<div class="panel-body">
				<div class="row justify-content-md-center">
					<div class="role-management-clientinfo">
						<div class="row">
							<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 user-information">
								<label class="col-md-3 col-lg-3 col-xs-12 pull-left" for="charge-amount">
									Role ID<span class="required">*</span>:
								</label>
								<div class="col-md-7 col-lg-7 col-xs-12 pull-left">
									{{ $roleInfo ->id }}
								</div>
							</div>
							<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 user-information">
								<label class="col-md-3 col-lg-3 col-xs-12 pull-left" for="charge-amount">
									Role Name<span class="required">*</span>:
								</label>
								<div class="col-md-7 col-lg-7 col-xs-12 pull-left">
									{{ $roleInfo ->name }}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 user-information">
								<label class="col-md-3 col-lg-3 col-xs-12 pull-left" for="charge-amount">
									Slug:
								</label>
								<div class="col-md-7 col-lg-7 col-xs-12 pull-left">
									{{ $roleInfo ->slug }}
								</div>
							</div>
							<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 user-information">
								<label class="col-md-3 col-lg-3 col-xs-12 pull-left" for="charge-amount">
									Description:
								</label>
								<div class="col-md-7 col-lg-7 col-xs-12 pull-left">
									{{ $roleInfo ->description }}
								</div>
							</div>
						</div>
						
					</div>
					{!! Form::open(['url' => '/role/permission/'.$roleInfo->id, 'files'=>true,'method' => 'PUT']) !!} 
					<h2 class="header-role"> Role</h2>
					
					<div class="role-management">
						
						@foreach($roleAll as $roleList)
							@if($roleInfo->id == $roleList->role_id)
								<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 role-column">
									<div class="user-info-all">
										<input type="checkbox"  checked class="form control"  name="permissionid[]" value="{{ $roleList->id }}"> &nbsp; {{ $roleList->name }}
			 						</div>
			 					</div>
			 				@else
			 					<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 role-column">
									<div class="user-info-all">
										<input type="checkbox"   class="form control"  name="permissionid[]" value="{{ $roleList->id }}"> &nbsp; {{ $roleList->name }}
			 						</div>
			 					</div>
			 				@endif
		 				@endforeach
	 					
						<div class="row pattern-area">
							<button class="btn btn-success button-settings-role"> Submit </button>
						</div>
					</div>
					{!!Form::close()!!}
				</div>
			</div>
		</div>
	</div>
</div>
@stop