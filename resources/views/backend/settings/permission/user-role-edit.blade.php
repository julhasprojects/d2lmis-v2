@extends( 'backend.index' )
@section( 'content_area' )
@include( 'master.message' )
<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);">Settings</a></li>
        <li class="active"><a href="javascript:void(0);">Update User Role Info</a></li>
    </ul>
</div>
@include('backend.settings.settings-menu')
@include( 'master.error' )
<div id="main-wrapper">
	<div class="col-md-12 col-xs-12 col-sm-12 left-box">
		<div class="panel-white">
			<h2 class="title-house"> Update Role Permission</h2>
			<div class="panel-body">
				<div class="row justify-content-md-center">
					<div class="role-management-clientinfo">
						<div class="row">
							<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 user-information">
								<label class="col-md-3 col-lg-3 col-xs-12 pull-left" for="charge-amount">
									User ID:
								</label>
								<div class="col-md-7 col-lg-7 col-xs-12 pull-left">
									{{ $userInfo->id }}
								</div>
							</div>
							<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 user-information">
								<label class="col-md-3 col-lg-3 col-xs-12 pull-left" for="charge-amount">
									User Name:
								</label>
								<div class="col-md-7 col-lg-7 col-xs-12 pull-left">
									{{ $userInfo->name }}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 user-information">
								<label class="col-md-3 col-lg-3 col-xs-12 pull-left" for="charge-amount">
									Empolyee Id:
								</label>
								<div class="col-md-7 col-lg-7 col-xs-12 pull-left">
									{{ $userInfo ->emp_id }}
								</div>
							</div>
							<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 user-information">
								<label class="col-md-3 col-lg-3 col-xs-12 pull-left" for="charge-amount">
									Mobile Number:
								</label>
								<div class="col-md-7 col-lg-7 col-xs-12 pull-left">
									{{ $userInfo ->mobile }}
								</div>
							</div>
						</div>
						
					</div>
					{!! Form::open(['url' => '/user/role', 'files'=>true,'method' => 'POST']) !!}
					<h2 class="header-role"> User Role</h2>
					<input type="hidden" value="{{  $userInfo ->id }}" name="usrid">	
					<div class="role-management">
		 				@foreach($unSelectAll as $roleList)	
		 					<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 role-column">
								<div class="user-info-all">
									<input type="checkbox" class="form control"  @foreach($roleUsed as $roleSelect) @if($roleSelect == $roleList->id)  checked @endif  @endforeach name="usrrole[]" value="{{ $roleList->id}}"> &nbsp; {{ $roleList->name}}
		 						</div>
		 					</div>	
		 				@endforeach
	 					
						<div class="row pattern-area">
							<button class="btn btn-success button-settings-role"> Submit </button>
						</div>
					</div>
					{!!Form::close()!!}
				</div>
			</div>
		</div>
	</div>
</div>
@stop