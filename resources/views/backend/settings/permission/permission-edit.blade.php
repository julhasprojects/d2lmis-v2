@extends( 'backend.index' )
@section( 'content_area' )
@include( 'master.message' )
<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);">Settings</a></li>
        <li class="active"><a href="javascript:void(0);">Permission information update</a></li>
    </ul>
</div>
@include('backend.settings.settings-menu')
@include( 'master.error' )
<div id="main-wrapper">
	<div class="col-md-12 col-xs-12 col-sm-12 left-box">
		<div class="panel-white">
			<h2 class="title-house"> Menu Role Permission</h2>
			<div class="panel-body">
				<div class="row justify-content-md-center">
					{!! Form::open(array('url'=>'new/role/permission/'.$permission->id,'role'=>'form','data-toggle'=>'validator','method'=>'PUT'))!!}
					<div class="col-md-12 col-xs-12">
						<div class="row from-group-box">
							<div class="col-md-6 padding-left-zero">
								<label class="col-md-4 col-lg-4 col-xs-12 pull-left" for="Permissionname">
									Permission Name <span class="required">*</span>:
								</label>
								<div class="col-md-8 col-lg-8 col-xs-12 pull-left padding-left-zero" >
									<input type="text" class="form-control" name="permissionname" id="permissionname" value="{{ $permission->name}}">
								</div>
		 					</div>

		 					<div class="col-md-6 padding-left-zero">
								<label class="col-md-4 col-lg-4 col-xs-12 pull-left" for="Permissionslug">
									Permission Slug <span class="required">*</span>:
								</label>
								<div class="col-md-8 col-lg-8 col-xs-12 pull-left padding-left-zero">
									<input type="text" class="form-control" name="permissionslug" id="permissionslug" value="{{ $permission->slug}}"> 
								</div>
		 					</div>
						</div>

						<div class="row from-group-box">
		 					<div class="col-md-6 padding-left-zero">
								<label class="col-md-4 col-lg-4 col-xs-12 pull-left" for="permissioncategory">
									Permission Category <span class="required">*</span>:
								</label>
								<div class="col-md-8 col-lg-8 col-xs-12 pull-left padding-left-zero">
									<select name="permissioncategory" id="permissioncategory" class="chosen-select-width">
										@foreach($permissionCategory as $category )
											@if($category->id == $permission->category)
												<option value="{{ $category->id }}" selected>{{ $category->category }}</option>
											@else
												<option value="{{ $category->id }}">{{ $category->category }}</option>
											@endif
										@endforeach
									</select>
								</div>
							</div>

							<div class="col-md-6 padding-left-zero padding-right-zero">
								<label class="col-md-4 col-lg-4 col-xs-12 pull-left" for="Permissionslug">
									Description:
								</label>
								<div class="col-md-8 col-lg-8 col-xs-12 pull-left padding-left-zero padding-right-zero">
									<textarea name="description" class="form-control" id="description" cols="30" rows="2"> {{ $permission->description}} </textarea>
								</div>
							</div>
						
						<div class="row pattern-area">
							<button class="btn btn-success button-settings"> Update </button>
						</div>
					</div>
					{!!Form::close()!!}
				</div>
			</div>
		</div>
	</div>
	
</div>
@stop