@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);">Settings</a></li>
        <li class="active"><a href="javascript:void(0);">New Menu Permission</a></li>
    </ul>
</div>
@include('backend.settings.settings-menu')
@include( 'master.message' )
@include( 'master.error' )
<div id="main-wrapper">
	<div class="col-md-12 col-xs-12 col-sm-12 left-box">
		<div class="panel-white">
			<h2 class="title-house"> Menu Role Permission</h2>
			<div class="panel-body">
				<div class="row justify-content-md-center">
					{!! Form::open(array('url'=>'new/role/permission','role'=>'form','data-toggle'=>'validator','method'=>'match'))!!}
					<div class="col-md-12 col-xs-12 col-span-3">
						<div class="row from-group-box">
							<div class="col-md-6 padding-left-zero">
								<label class="col-md-4 col-lg-4 col-xs-12 pull-left" for="Permissionname">
									Permission Name<span class="required">*</span>:
								</label>
								<div class="col-md-8 col-lg-8 col-xs-12 pull-left padding-left-zero" >
									<input type="text" class="form-control" name="permissionname" id="permissionname">
								</div>
							</div>
							<div class="col-md-6 padding-right-zero">
								<label class="col-md-4 col-lg-4 col-xs-12 pull-left " for="Permissionslug">
									Permission Slug<span class="required">*</span>:
								</label>
								<div class="col-md-8 col-lg-8 col-xs-12 pull-left padding-right-zero">
									<input type="text" class="form-control" name="permissionslug" id="permissionslug"> 
								</div>
							</div>
	 					</div>
						<div class="row from-group-box">
							<div class="col-md-6 padding-left-zero">
								<label class="col-md-4 col-lg-4 col-xs-12 pull-left" for="permissioncategory">
									Permission Category<span class="required">*</span>:
								</label>
								<div class="col-md-8 col-lg-8 col-xs-12 pull-left padding-left-zero">
									<select name="permissioncategory" id="permissioncategory" class="chosen-select-width">
										<option value="">Select Category</option>
										@foreach($permissionCategory as $category )
											<option value="{{ $category->id }}">{{ $category->category }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-6 padding-left-zero padding-right-zero">
								<label class="col-md-4 col-lg-4 col-xs-12 pull-left" for="Permissionslug">
									Description:
								</label>
								<div class="col-md-8 col-lg-8 col-xs-12 pull-left padding-left-zero padding-right-zero">
									<textarea name="description" class="form-control" id="description" cols="30" rows="2"></textarea>
								</div>
							</div>
	 					</div>
						<div class="row pattern-area">
							<button class="btn btn-success button-settings"> Create </button>
						</div>
					</div>
					{!!Form::close()!!}
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 left-box">
			<div class="panel panel-white">
				<div class="panel-body">
					<h2 class="title-house"> Permission Name </h2>
					<div class="table-responsive"> 
						<table class="table table-hover table-bordered" id="tableresponsive" id="tableresponsive">
		                    <thead class="table-header-bg">
		                        <tr>
		                            <th>SN</th>
		                            <th>Name</th>
		                            <th>Slug</th>
		                            <th>Description</th>
		                            <th>Category</th>
		                            <th>Action</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    	<?php
		                    		$sn=1;
		                    	?>
		                    	@foreach($permissionInfo as $info)
		                    		<tr>
				                    	<td>{{ $sn++ }}</td>
				                    	<td><span class="permission-name">{{ $info->name }} </span></td>
				                    	<td>{{ $info->slug }}</td>
				                    	<td>{{ $info->description }}</td>
				                    	<td>{{ $info->category_name }}</td>
				                    	<td>
		                                    <a href="{{ URL::to('/new/role/permission/'.$info->id.'/edit') }}" class="glyphicon glyphicon-pencil icon-edit" >  |</a> 
		                                    {!! Form::open(['url' => '/new/role/permission/'.$info->id, 'files'=>true,'method' => 'delete','class' => 'from-delete']) !!}
		                                        <button type="submit" class="delete-options"><i class="fa fa-trash-o"></i></button>
		                                    {!! Form::close() !!}  
		                                </td>
			                    	</tr>
		                    	@endforeach	              
		                    </tbody>                                 
		                </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop