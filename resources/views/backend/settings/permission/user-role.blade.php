@extends( 'backend.index' )
@section( 'content_area' )
@include( 'master.message' )
<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);">Settings</a></li>
        <li class="active"><a href="javascript:void(0);">User Permission</a></li>
    </ul>
</div>
@include('backend.settings.settings-menu')
@include( 'master.error' )
<div id="main-wrapper">

	<div class="row">
		<div class="col-md-12 left-box">
			<div class="panel panel-white">
				<div class="panel-body">
					<h2 class="title-house"> User List </h2>
					<div class="table-responsive"> 
						<table class="table table-hover table-bordered" id="tableresponsive">
		                    <thead class="table-header-bg">
		                        <tr>
		                            <th> S/N </th>
		                            <th> Employer ID  </th>
		                            <th> Name </th>
		                            <th> User name </th>
		                            <th> Email </th>
		                            <th> Department </th>
		                            <th> Designation </th>
		                            <th> Action </th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    	<?php
		                    		$sl=1;
		                    	?>
		                    	@foreach($userInfo as $info)
		                    		<tr>
				                    	<td>{{ $sl++ }}</td>
				                    	<td>{{ $info->emp_id }}</td>
				                    	<td>{{ $info->name }}</td>
				                    	<td>{{ $info->username }}</td>
				                    	<td>{{ $info->email }}</td>
				                    	<td>{{ $info->deptName }}</td>
				                    	<td>{{ $info->designation }}</td>
				                    	
				                    	<td>
				                    		<span class="action-page">
			                                    <a href="{{ URL::to('/user/role/'.$info->id.'/edit') }}" class="glyphicon glyphicon-pencil icon-edit" >  </a> 
			                                   
		                                    </span>
		                                </td>
			                    	</tr>
		                    	@endforeach	              
		                    </tbody>                                 
		                </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop