@extends( 'backend.index' )
@section( 'content_area' )
@include( 'master.message' )
<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);">Settings</a></li>
        <li class="active"><a href="javascript:void(0);">Role Permission</a></li>
    </ul>
</div>
@include('backend.settings.settings-menu')
@include( 'master.error' )
<div id="main-wrapper">

	<div class="row">
		<div class="col-md-12 left-box">
			<div class="panel panel-white">
				<div class="panel-body">
					<h2 class="title-house"> Role Name </h2>
					<div class="table-responsive"> 
						<table class="table table-hover table-bordered" id="tableresponsive">
		                    <thead class="table-header-bg">
		                        <tr>
		                            <th> SN </th>
		                            <th> Name </th>
		                            <th> ID  </th>
		                            <th> Email </th>
		                            <th> Action </th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    	<?php
		                    		$sl=1;
		                    	?>
		                    	@foreach($userInfo as $info)
		                    		<tr>
				                    	<td>{{ $sl++ }}</td>
				                    	<td>{{ $info->name }}</td>
				                    	<td>{{ $info->emp_id }}</td>
				                    	<td>{{ $info->email }}</td>
				                    	<td>
		                                    <a href="{{ URL::to('/user/role/'.$info->id.'/edit') }}" class="glyphicon glyphicon-pencil icon-edit" >  |</a> 
		                                    {!! Form::open(['url' => '/user/role/'.$info->id, 'files'=>true,'method' => 'delete','class' => 'from-delete']) !!}
		                                        <button type="submit" class="delete-options"><i class="fa fa-trash-o"></i></button>
		                                    {!! Form::close() !!}  
		                                </td>
			                    	</tr>
		                    	@endforeach	              
		                    </tbody>                                 
		                </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop