@extends( 'backend.index' )
@section( 'content_area' )
<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);">Settings</a></li>
        <li class="active"><a href="javascript:void(0);">Super Admin Settings</a></li>
    </ul>
</div>
@include('backend.settings.settings-menu')
@include( 'master.error' )
<div id="main-wrapper">
	<div class="col-md-12 col-xs-12 col-sm-12 left-box">
		<div class="panel-white">
			<h2 class="title-house"> User Role Permission</h2>
			<div class="panel-body permission-management-body">
				<a  href="{{ URL::to( '/permission/menu/category' ) }}" class="col-md-3 col-xs-12">
					<button class="btn btn-info btn-primary pull-left"> 
						Menu Permission Category <br>
						<i class="fa fa-angle-right permission-managment-icon" aria-hidden="true"></i>
					</button>
				</a>
				<a  href="{{ URL::to( '/new/role/permission' ) }}" class="col-md-3 col-xs-12">
					<button class="btn btn-info btn-lg pull-left""> 
						Add New Menu Permission <br>
						<i class="fa fa-angle-right permission-managment-icon" aria-hidden="true"></i>
					</button>
				</a>
				<a  href="{{ URL::to( '/role/permission/show' ) }}" class="col-md-3 col-xs-12">
					<button class="btn btn-success btn-lg pull-center"> 
						Create Role Permission  <br>
						<i class="fa fa-angle-right permission-managment-icon" aria-hidden="true"></i>
					</button>
				</a>
				<a  href="{{ URL::to( '/user/role' ) }}" class="col-md-3 col-xs-12">
					<button class="btn btn-warning btn-lg pull-right ">
						Assign User Role 
					</button>
				</a>
			</div>
		</div>
	</div>
	
</div>
@stop