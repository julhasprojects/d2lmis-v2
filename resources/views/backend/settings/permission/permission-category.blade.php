@extends( 'backend.index' )
@section( 'content_area' )
@include( 'master.message' )
<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);">Settings</a></li>
        <li class="active"><a href="javascript:void(0);">Menu Permission Category</a></li>
    </ul>
</div>
@include('backend.settings.settings-menu')
@include( 'master.error' )
@include( 'master.error' )
<div id="main-wrapper">
	<div class="col-md-12 col-xs-12 col-sm-12 left-box">
		<div class="panel-white">
			<h2 class="title-house"> Menu Role Permission</h2>
			<div class="panel-body">
				<div class="row justify-content-md-center">
					{!! Form::open(array('url'=>'permission/menu/category','role'=>'form','data-toggle'=>'validator','method'=>'match'))!!}
					<div class="col-md-12 col-xs-12 col-span-3">
						<div class="row from-group-box">
							<div class="col-md-6 padding-left-zero">
								<label class="col-md-4 col-lg-4 col-xs-12 pull-left" for="Permissionname">
									Permission Category Name<span class="required">*</span>:
								</label>
								<div class="col-md-8 col-lg-8 col-xs-12 pull-left padding-left-zero" >
									<input type="text" class="form-control" name="permissioncategory" id="permissioncategory" required="true" autofocus>
								</div>
							</div>
						</div>
						<div class="row pattern-area">
							<button class="btn btn-success button-settings"> Create </button>
						</div>
					</div>
					{!!Form::close()!!}
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 left-box">
			<div class="panel panel-white">
				<div class="panel-body">
					<h2 class="title-house"> Permission Category </h2>
					<div class="table-responsive"> 
						<table class="table table-hover table-bordered">
		                    <thead class="table-header-bg">
		                        <tr>
		                            <th>SN</th>
		                            <th>Category</th>
		                            <th>Created At</th>
		                            <th>Action</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    	<?php
		                    		$sn=1;
		                    	?>
		                    	@foreach($permissionCategory as $info)
		                    		<tr>
				                    	<td>{{ $sn++ }}</td>
				                    	<td>{{ $info->category }}</td>
				                    	<td>{{ $info->created_at }}</td>
				                    	<td>
		                                    <a href="{{ URL::to('/permission/menu/category/'.$info->id.'/edit') }}" class="glyphicon glyphicon-pencil icon-edit" >  |</a> 
		                                    {!! Form::open(['url' => '/permission/menu/category/'.$info->id, 'files'=>true,'method' => 'delete','class' => 'from-delete']) !!}
		                                        <button type="submit" class="delete-options"><i class="fa fa-trash-o"></i></button>
		                                    {!! Form::close() !!}  
		                                </td>
			                    	</tr>
		                    	@endforeach	              
		                    </tbody>                                 
		                </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop