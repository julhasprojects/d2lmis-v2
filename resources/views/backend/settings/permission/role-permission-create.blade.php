@extends( 'backend.index' )
@section( 'content_area' )
@include( 'master.message' )
<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);">Settings</a></li>
        <li class="active"><a href="javascript:void(0);">Update User Role Information</a></li>
    </ul>
</div>
@include('backend.settings.settings-menu')
@include( 'master.error' )
<div id="main-wrapper">
	<div class="col-md-12 col-xs-12 col-sm-12 left-box">
		<div class="panel-white">
			<h2 class="title-house"> Update Menu Permission Information</h2>
			<div class="panel-body">
				<div class="row justify-content-md-center">
					<div class="role-management-clientinfo">
						<div class="row">
							<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 user-information">
								<label class="col-md-3 col-lg-3 col-xs-12 pull-left" for="charge-amount">
									Role ID:
								</label>
								<div class="col-md-7 col-lg-7 col-xs-12 pull-left">
									{{ $roleInfo->id }}
								</div>
							</div>
							<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 user-information">
								<label class="col-md-3 col-lg-3 col-xs-12 pull-left" for="charge-amount">
									Role Name:
								</label>
								<div class="col-md-7 col-lg-7 col-xs-12 pull-left">
									{{ $roleInfo->name }}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 user-information">
								<label class="col-md-3 col-lg-3 col-xs-12 pull-left" for="charge-amount">
									Slug:
								</label>
								<div class="col-md-7 col-lg-7 col-xs-12 pull-left">
									{{ $roleInfo->slug }}
								</div>
							</div>
							<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 user-information">
								<label class="col-md-3 col-lg-3 col-xs-12 pull-left" for="charge-amount">
									Description:
								</label>
								<div class="col-md-7 col-lg-7 col-xs-12 pull-left">
									{{ $roleInfo->description }}
								</div>
							</div>
						</div>
					</div>
					{!! Form::open(['url' => '/role/permission/'.$roleInfo->id, 'files'=>true,'method' => 'PUT']) !!} 
					<h2 class="header-role"> Role Permission Setting</h2>
					<div class="role-management">
						
						@for($i = 0; $i < $langth; $i++) 
							<h3 class="allcategory_list"> {{ $allCategory[$i] }} </h3>

							<div class="row">
								<input type="checkbox" id="select_all_{{$i}}" class="select_all_{{$i}}"/> Select All
							</div>

							<div class="row">
							@foreach ($categoryWishPermission[$i] as $key => $permission)
								@if($permission->role_id == $roleInfo->id)
									<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 role-column">
										<div class="user-info-all">
											<input type="checkbox"  checked class="form control_{{$i}}"  name="permissionid[]" value="{{$permission->id }}"> &nbsp; {{ $permission->name}}
				 						</div>
					 				</div>
								@endif
							@endforeach
							@foreach ($categoryWishPermissionrole[$i] as $key => $permissionRole)
								
								<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 role-column">
									<div class="user-info-all">
										
										<input type="checkbox"  class="form control_{{$i}}"  name="permissionid[]" value="{{$permissionRole->id }}"> &nbsp; {{ $permissionRole->name}}
			 						</div>
				 				</div>
							
							@endforeach
				            
				       		</div>

				       @endfor
						<div class="row pattern-area">
							<button class="btn btn-success button-settings-role"> Submit </button>
						</div>
					</div>
					{!!Form::close()!!}
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		@for($i = 0; $i<$langth; $i++) 
			$(".select_all_{{$i}}").change(function(){  
			  $(".control_{{$i}}").prop('checked', $(this).prop("checked"));
			});

		  	$('.control_{{$i}}').change(function(){
				if(false == $(this).prop("checked")){ //if this item is unchecked
				  $(".select_all_{{$i}}").prop('checked', false); //change "select all" checked status to false
				}

				if ($('.control_{{$i}}:checked').length == $('.control_{{$i}}').length ){
				  $(".select_all_{{$i}}").prop('checked', true);
				}
		  	});
		@endfor
})
</script>
@stop