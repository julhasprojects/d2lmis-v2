@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);">Settings</a></li>
        <li class="active"><a href="javascript:void(0);">Role </a></li>
    </ul>
</div>
@include('backend.settings.settings-menu')
@include( 'master.message' )
@include( 'master.error' )
<div id="main-wrapper">
	<div class="col-md-12 col-xs-12 col-sm-12 left-box">
		<div class="panel-white">
			<h2 class="title-house"> Role Name </h2>
			<div class="panel-body">
				<div class="row justify-content-md-center">
					{!! Form::open(array('url'=>'role/permission','role'=>'form','data-toggle'=>'validator','method'=>'match'))!!}
					<div class="col-md-6 col-xs-12 col-span-3">
						<div class="row from-group-box">
							<label class="col-md-3 col-lg-3 col-xs-12 pull-left" for="rolename">
								Role Name<span class="required">*</span>:
							</label>
							<div class="col-md-7 col-lg-7 col-xs-12 pull-left padding-left-zero" >
								<input type="text" class="form-control" name="rolename" id="rolename">
							</div>
	 					</div>
	 					<div class="row from-group-box">
							<label class="col-md-3 col-lg-3 col-xs-12 pull-left" for="roleslug">
								Slug<span class="required">*</span>:
							</label>
							<div class="col-md-7 col-lg-7 col-xs-12 pull-left padding-left-zero">
								<input type="text" class="form-control" name="roleslug" id="roleslug"> 
							</div>
	 					</div>
						<div class="row from-group-box">
							<label class="col-md-3 col-lg-3 col-xs-12 pull-left" for="roleslug">
								Description :
							</label>
							<div class="col-md-7 col-lg-7 col-xs-12 pull-left padding-left-zero">
								<textarea name="description" class="form-control" id="description" cols="30" rows="5"></textarea>
							</div>
	 					</div>
						<div class="row pattern-area">
							<button class="btn btn-success button-settings"> Create </button>
						</div>
					</div>
					{!!Form::close()!!}
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 left-box">
			<div class="panel panel-white">
				<div class="panel-body">
					<h2 class="title-house"> Role Name List</h2>
					<div class="table-responsive"> 
						<table class="table table-hover table-bordered">
		                    <thead class="table-header-bg">
		                        <tr>
		                            <th>SN</th>
		                            <th>Name</th>
		                            <th>Slug</th>
		                            <th>Description</th>
		                            <th>Action</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    	<?php
		                    		$sn=1;
		                    	?>
		                    	@foreach($role as $info)
		                    		<tr>
				                    	<td>{{$sn++}}</td>
				                    	<td>{{$info->name}}</td>
				                    	<td>{{$info->slug}}</td>
				                    	<td><span class="descraption-role"> {{$info->description }} </span></td>
				                    	<td>
		                                    <a href="{{ URL::to('/role/permission/'.$info->id.'/edit') }}" class="glyphicon glyphicon-pencil icon-edit" >  |</a> 
		                                    {!! Form::open(['url' => '/role/permission/'.$info->id, 'files'=>true,'method' => 'delete','class' => 'from-delete']) !!}
		                                        <button type="submit" class="delete-options"><i class="fa fa-trash-o"></i></button>
		                                    {!! Form::close() !!}  
		                                </td>
			                    	</tr>
		                    	@endforeach	              
		                    </tbody>                                 
		                </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop