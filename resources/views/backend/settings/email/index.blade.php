@extends( 'backend.index' )
@section( 'content_area' )
@include('master.message')	
<div class="bg-title">
	<div class="page-title reports-title">
        <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
            <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
            <li class="completed"><a href="javascript:void(0);"> Modules</a></li>
            <li class="active"><a href="javascript:void(0);"> SMS Custom SMS </a></li>
        </ul>
	</div>		
</div>
<div id="deletenotification"></div>
@include('backend.settings.sms.sms-mgt.sms-menu')
@include('master.error')	
<div id="main-wrapper">
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<h4 class="title-house"> Email Setting Create </h4>
			<div class="panel-body">
				
				<div class="tab-content">
					
						 {{Form::open(array('url'=>"email/setting",'class'=>'form-horizontal form-bordered','method'=>'post'))}}
							
							<div class="sms-setup">
								<div class="col-md-12">
					                <label class="col-md-3">SMS Setting Enable:</label>
					                <div class="col-md-8 global-settings">
					                    <div class="col-md-2 col-lg-2 col-xs-12 pull-left">
											<div class="checker"><span><input class="form control" name="is_enable" value="1" type="radio" checked></span></div> Yes
										</div>
										<div class="col-md-2 col-lg-2 col-xs-12 pull-left">
											<div class="checker"><span><input class="form control" name="is_enable" value="0" type="radio"></span></div> No
										</div>
					                </div>
					            </div>
					        </div>
					            <div class="col-md-12 text-center">
					                {!! Form::submit('Submit', ['class'=>'btn btn-primary']) !!}
					                
					            </div>
								<div class="table-responsove">
								    <table class="table table-hover table-bordered" id="tableresponsive">
								        <thead class="table-header-bg">
								            <tr>
								                <th><span class="box-info-sum">S/N</span></th>
								                <th><span class="box-info-sum">Status </span></th>
								                <th><span class="box-info-sum">Create </span></th>
								                <th><span class="box-info-sum">Action</span></th>
								            </tr>
								        </thead>
								        <tbody>
									        <?php 
									            $sl =1;
									        ?>
									        @foreach($email as $info)
									            <tr class="global_{{ $info->id }}">
									                <td><span class="box-info-sum">{{ $sl++ }}</span></td>
									                <td>
									                	<span class="box-info-sum">
									                		@if($info->is_enable == 1)
									                			<button class="btn btn-success"> Enable</button>
									                		@else
																<button class="btn btn-danger"> Disable</button>
									                		@endif
									                	</span>
									                </td>
									                <td><span class="box-info-sum">
									                	{{ $info->created_at}}
									                	</span>
									                </td>
									                <td class="button-section">
					                                    <span class="both-options  sms-action-button">
					                                        <a class="btn purple btn-xs" href="{{ URL::to('/sms/global/setting/'.$info->id .'/edit') }}"><i class="fa fa-edit"></i> </a>
					                                        <a class="btn red btn-xs"  href="javascript:;" onclick="globalSettingDelete('{{ $info->id }}','{{ $info->is_enable }}')"><i class="fa fa-trash"></i> </a>
					                                    </span>
					                                </td>
									            </tr>
									        @endforeach
									      
									    </tbody>
								    </table>
							 
			            {{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('backend.settings.sms.sms-delete')
@stop