@extends( 'backend.index' )
@section( 'content_area' )
<?php 
	$setup_by = Session::get('user_id');
	if( !isset($setup_by) && empty($setup_by)) { 
        $setup_by = 1;
    } 
?> 
<div class="bg-title">
    <div class="page-title reports-title">
        <ul class="breadcrumb  col-md-5 col-xs-12 col-lg-5 col-sm-5 pull-left">
            <li class="completed"><a href="javascript:void(0);">Dashboard </a></li>
            <li class="completed"><a href="javascript:void(0);"> Setting </a></li>
            <li  class="active"><a href="javascript:void(0);"> Date setup Update</a></li>
        </ul>
    </div>
</div>
<div id="main-wrapper">
	<div class="row">
		<!-- Region setup -->
		<div class="col-md-6">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Bill Date Setup</h4>
					<a href="{{ URL::to('bill/date/and/system/setup/') }}" class="btn btn-success" style="float:right;">
						Back
					</a> 
	        	</div>
				<hr>
				
				{!! Form::open(array('url' => 'bill/date/and/system/setup/'.$editInfo->id, 'method' => 'PUT')) !!}
				
				<input type="hidden" name="setup_by" value="{{ $setup_by }}">		
				<div class="panel-body">	
					<div class="form-group">
						<label for="region" class="col-md-4 control-label">Start Date<span class="required">*</span></label>
						<div class="col-md-6">
							<select class="chosen-select-width" name="start_day" required autofocus>
								@for ($i = 1; $i <= 31; $i++)
									
									@if($i == $editInfo->start_day)
										@if( $i < 10 )
											<option value="0{{$i}}" selected> 0{{ $i }} </option>
										@else 
											<option value="{{$i}}" selected> {{ $i }} </option>
										@endif
									@endif
								    
								@endfor
								@for ($i = 1; $i <= 31; $i++)
									
									@if( $i < 10 )
										<option value="0{{$i}}"> 0{{ $i }} </option>
									@else 
										<option value="{{$i}}"> {{ $i }} </option>
									@endif
								    
								@endfor
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="region" class="col-md-4 control-label">End Date<span class="required">*</span></label>
						<div class="col-md-6">
							<select class="chosen-select-width" name="end_day" required autofocus>
								@for ($i = 1; $i <= 31; $i++)
									
									@if($i == $editInfo->end_day)
										@if( $i < 10 )
											<option value="0{{$i}}" selected> 0{{ $i }} </option>
										@else 
											<option value="{{$i}}" selected> {{ $i }} </option>
										@endif
									@endif
								    
								@endfor
								@for ($i = 1; $i <= 31; $i++)
									
									@if( $i < 10 )
										<option value="0{{$i}}"> 0{{ $i }} </option>
									@else 
										<option value="{{$i}}"> {{ $i }} </option>
									@endif
								    
								@endfor
							</select>
						</div>
					</div>
					<div class="form-group bill-date-setup">
						<label for="branch_id" class="col-md-4 control-label"> Branch Name<span class="required">*</span></label>
						<div class="col-md-6">
							<select class="chosen-select-width" name="branch_id" required autofocus>
								@foreach($branchInfo as $branch)
									<option value="{{ $branch->id }}" @if($branch->id == $editInfo->branch_id) selected @endif>{{ $branch->branch_name }}</option>
								@endforeach
							</select>
						</div>
					</div>
			    	<div class="form-group">
						<div class="col-md-12 col-sm-4">
							<center><button type="submit" class="btn btn-success"> Update </button> </center>
						</div>
					</div>	
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop    