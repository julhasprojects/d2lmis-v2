@extends( 'backend.index' )
@section( 'content_area' )

@include('master.message')
<div class="bg-title">
    <div class="page-title reports-title">
        <ul class="breadcrumb  col-md-5 col-xs-12 col-lg-5 col-sm-5 pull-left">
            <li class="completed"><a href="javascript:void(0);">Dashboard </a></li>
            <li class="completed"><a href="javascript:void(0);"> Settings </a></li>
            <li  class="active"><a href="javascript:void(0);"> Billing date and cycle setup </a></li>
        </ul>
    </div>
</div>
<br>
<?php  $entry_by = Session::get('emp_id'); ?>
<div id="main-wrapper">
	<div class="panel panel-body">
		<div class="panel panel-white">
			<ul class="nav nav-tabs">
		        <li class="active"><a data-toggle="tab" href="#date">Bill Date Setup</a></li>
		        <li><a data-toggle="tab" href="#billingCycle">Billing Cycle Setup </a></li>
		    </ul>
		    <div class="tab-content tab-advaced overflow-visable">
				<div id="date" class="tab-pane fade in active">
					<div class="row column-box">
						<!-- Region setup -->
						<div class="col-md-6 left-box">

							<div class="panel panel-white">
								<h4 class="title-house">Bill Date Setup</h4>
								<div class="panel-body panel-date-setup">

									{!! Form::open(array('url' => 'bill/date/and/system/setup', 'method' => 'post')) !!}
										<input type="hidden" name="setup_by" value="{{ $entry_by }}">		
										<div class="form-group bill-date-setup">
											<label for="region" class="col-md-4 control-label">Start Date<span class="required">*</span></label>
											<div class="col-md-6">
												<select class="chosen-select-width" name="start_day" required autofocus>
													
													@foreach($daysOfMonth as $key => $day)
														
														@if($day == "01")
															<option value="{{$day}}" selected="true"> {{ $day }} </option>
														@else 
															<option value="{{$day}}"> {{ $day }} </option>
														@endif

													@endforeach
													
												</select>
											</div>
										</div>
										<div class="form-group bill-date-setup">
											<label for="region" class="col-md-4 control-label">End Date<span class="required">*</span></label>
											<div class="col-md-6">
												<select class="chosen-select-width" name="end_day" required autofocus>
													
													@foreach($daysOfMonth as $key => $day)
														
														@if($day == date("t"))
															<option value="{{$day}}" selected="true"> {{ $day }} </option>
														@else 
															<option value="{{$day}}"> {{ $day }} </option>
														@endif

													@endforeach
													
												</select>
											</div>
										</div>
										<div class="form-group bill-date-setup">
											<label for="branch_id" class="col-md-4 control-label"> Branch Name<span class="required">*</span></label>
											<div class="col-md-6">
												<select class="chosen-select-width" name="branch_id" required autofocus>
													@foreach($branchInfo as $branch)
														<option value="{{ $branch->id }}" >{{ $branch->branch_name }}</option>
													@endforeach
												</select>
											</div>
										</div>
										
								    	<div class="form-group bill-date-setup">
											<div class="col-md-12 col-sm-6">
												<center><button type="submit" class="btn btn-success">Create</button> </center>
											</div>
										</div>	
									{!! Form::close() !!}
								</div>
								
							</div>
						</div>
						<!-- Show all region setup -->
						<div class="col-md-6 left-box">
							<div class="panel panel-white">
								<h4 class="title-house">Show All Bill Date Setup List</h4>
								<div class="panel-body panel-date-setup">
									{!! Form::open(array('url' => 'bill/date/and/system/setup', 'method' => 'post')) !!}
										<input type="hidden" name="setup_by" value="{{ $entry_by }}">		
										<table class="table table-bordered table-hover">
											<thead>
												<th>ID</th>
												<th>Start Day</th>
												<th>End Day</th>
												<th>Action</th>
											</thead>
											<tbody>
												<?php $sl = 1; ?>
												@foreach($allDate as $key=>$value)
													<tr>
														<td> {{ $sl++ }} </td>
														<td> {{ $value->start_day }} </td>
														<td> {{ $value->end_day}} </td>
														<td> 

															<a href="{{ URL::to('bill/date/and/system/setup/' . $value->id . '/edit') }}">Edit </a> &nbsp;&nbsp;
															<a href="{{ URL::to('bill/date/and/system/setup/' . $value->id ) }}">Delete </a>

														</td>
													</tr>
												@endforeach	

												
											</tbody>
										</table>
									{!! Form::close() !!}
								</div>
						</div>
					</div>
				</div>
				</div>
				<div id="billingCycle" class="tab-pane fade">
					<div class="col-md-12 col-xs-12 col-lg-12 col-sm-12 left-box">
						<div class="panel panel-white">

							<div class="panel panel-body pand-date-setup">
							<h2 class="title-house"> Billing Cycle Setup </h2>
								{!! Form::open(array('url' => 'bill/system/setup', 'method' => 'post')) !!}

									<div class="row from-group-box">
										<label class="col-md-3 col-lg-3 col-xs-12 pull-left" for="charge-amount">
											Billing Cycle :
										</label>
										<div class="col-md-7 col-lg-7 col-xs-12 pull-left padding-left-zero">
											<div class="col-md-6 col-lg-6 col-xs-12 pull-left">
												<input class="form control" name="costingtype" value="0" type="radio">  Daily
											</div>
											<div class="col-md-6 col-lg-6 col-xs-12 pull-right">
												<input class="form control" name="costingtype" value="1" checked="" type="radio">  Monthly
											</div>
										</div>

										<div class="form-group">
											<div class="col-md-10 col-sm-4">
												<br>
												<center><button type="submit" class="btn btn-success">Submit</button> </center>
											</div>
										</div>
				 					</div>
				 				{!! Form::close() !!}	
			 			</div>
			 		</div>
					<table class="table table-bordered table-hover">
						<thead>
							<th>ID</th>
							<th>Billing Type</th>
							<th>Created At</th>
							<th>Action</th>
						</thead>
						<tbody>
							<?php $sl = 1; ?>
							@foreach($billSystem as $key=>$info)
								<tr>
									<td> {{ $sl++ }} </td>
									<td> 
										@if($info->costing_type == 0)
											<button class="btn btn-success btn-xs"> Daily </button>
										@else
											<button class="btn btn-success btn-xs"> Monthly </button>
										@endif
									</td>
									<td> {{ $info->created_at }} </td>
									<td> 
										<a href="{{ URL::to('bill/system/setup/' . $info->id . '/edit') }}" class="glyphicon glyphicon-pencil icon-edit" >  | </a> &nbsp;&nbsp;
										{!! Form::open(['url' => '/bill/system/setup/'.$info->id, 'files'=>true,'method' => 'delete','class' => 'from-delete']) !!}
	                                        <button type="submit" class="delete-options"><i class="fa fa-trash-o"></i></button>
	                                    {!! Form::close() !!}  
									</td>
								</tr>
							@endforeach	
						</tbody>
					</table>
	 			
				</div>
				<div id="billingCycle" class="tab-pane fade">
					<div class="col-md-12 col-xs-12 col-lg-12 col-sm-12 left-box">
						<div class="panel panel-white">

							<div class="panel panel-body pand-date-setup">
							<h2 class="title-house"> Collection Setting </h2>
								{!! Form::open(array('url' => 'bill/collection/setting', 'method' => 'post')) !!}

									<div class="row from-group-box">
										<label class="col-md-3 col-lg-3 col-xs-12 pull-left" for="charge-amount">
											Billing Cycle :
										</label>
										<div class="col-md-7 col-lg-7 col-xs-12 pull-left padding-left-zero">
											<div class="col-md-6 col-lg-6 col-xs-12 pull-left">
												<input class="form control" name="collectiontype" value="0" type="radio">  Office
											</div>
											<div class="col-md-6 col-lg-6 col-xs-12 pull-right">
												<input class="form control" name="collectiontype" value="1" checked="" type="radio">  Billman
											</div>
										</div>

										<div class="form-group">
											<div class="col-md-10 col-sm-4">
												<br>
												<center><button type="submit" class="btn btn-success">Submit</button> </center>
											</div>
										</div>
				 					</div>
				 				{!! Form::close() !!}	
			 			</div>
			 		</div>
					<table class="table table-bordered table-hover">
						<thead>
							<th>ID</th>
							<th>Billing Type</th>
							<th>Created At</th>
							<th>Action</th>
						</thead>
						<tbody>
							<?php $sl = 1; ?>
							@foreach($billSystem as $key=>$info)
								<tr>
									<td> {{ $sl++ }} </td>
									<td> 
										@if($info->costing_type == 0)
											<button class="btn btn-success btn-xs"> Daily </button>
										@else
											<button class="btn btn-success btn-xs"> Monthly </button>
										@endif
									</td>
									<td> {{ $info->created_at }} </td>
									<td> 
										<a href="{{ URL::to('bill/system/setup/' . $info->id . '/edit') }}" class="glyphicon glyphicon-pencil icon-edit" >  | </a> &nbsp;&nbsp;
										{!! Form::open(['url' => '/bill/system/setup/'.$info->id, 'files'=>true,'method' => 'delete','class' => 'from-delete']) !!}
	                                        <button type="submit" class="delete-options"><i class="fa fa-trash-o"></i></button>
	                                    {!! Form::close() !!}  
									</td>
								</tr>
							@endforeach	
						</tbody>
					</table>
	 			
				</div>
			</div>
		</div>
	</div>
</div>
@stop    