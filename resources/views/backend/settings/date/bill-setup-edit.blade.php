@extends( 'backend.index' )
@section( 'content_area' )

@include('master.message')
<div class="bg-title">
    <div class="page-title reports-title">
        <ul class="breadcrumb  col-md-5 col-xs-12 col-lg-5 col-sm-5 pull-left">
            <li class="completed"><a href="javascript:void(0);">Dashboard </a></li>
            <li class="completed"><a href="javascript:void(0);"> Setting </a></li>
            <li  class="active"><a href="javascript:void(0);"> Billing  cycle setup Update</a></li>
        </ul>
    </div>
</div>
<br>
<div id="main-wrapper">
	<div class="panel panel-body">
		<div class="panel panel-white">
		
		    <div class="tab-content tab-advaced">
				<div id="billingCycle" class="tab-pane fade active in">
					<div class="col-md-12 col-xs-12 col-lg-12 col-sm-12 left-box">
						<div class="panel panel-white">
							<div class="panel panel-body pand-date-setup">
								{!! Form::open(array('url' => 'bill/system/setup/'. $billSystem->id, 'method' => 'PUT')) !!}
									<div class="row from-group-box">
										<label class="col-md-3 col-lg-3 col-xs-12 pull-left" for="charge-amount">
											Billing Cycle:
										</label>
										<div class="col-md-7 col-lg-7 col-xs-12 pull-left padding-left-zero">
											
											<div class="col-md-6 col-lg-6 col-xs-12 pull-left">
												<input class="form control" name="costingtype" value="0" type="radio" @if($billSystem->costing_type == '0') checked @endif>  Daily
											</div>
											<div class="col-md-6 col-lg-6 col-xs-12 pull-right">
												<input class="form control" name="costingtype" value="1"  type="radio" @if($billSystem->costing_type == '1') checked @endif>  Monthly
											</div>
										</div>

				 					</div>
				 					<div class="row">
				 						<div class="col-md-10">
				 							
											<div class="form-group button-setup-group">
												<center><button type="submit" class="btn btn-success bill-setup-button">Update</button> </center>
											</div>
				 						</div>
				 					</div>
				 				{!! Form::close() !!}	
				 				&nbsp;<a class="btn btn-warning" href="{{ URL::to( '/bill/date/and/system/setup' ) }}"> Back </a >
			 				</div>
			 			</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop    