@extends( 'backend.index' )

@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);"> Reports</a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory</a></li>
        <li  class="active"><a href="javascript:void(0);">  Master Box Reports </a></li>
    </ul>

    <div class="btn-group button-group col-md-4 col-xs-12 col-lg-4 col-sm-4 pull-right" role="group" aria-label="Basic example">
       
        <div class="btn-group pull-right" role="group" aria-label="First group">
            <a class="btn btn-success" href="{{ URL::to('export-master-packagechanged') }}">Export</a>
            <button type="button" class="btn btn-info"   href="javascript:void()" onClick="window.print()">Print</button>
            <button type="button" class="btn btn-primary">PDF</button>
        </div>
       
    </div>
</div>
@include('backend.inventory.box-menu')
@include('master.message')
<div id="main-wrapper" class="background-white box-inventory">
  
    <div class="col-md-12 left-box">
        <div class="panel panel-white">
            <h3 class="title-house">Update Box Information</h3>             
            <div class="panel-body">
                <div class="table-responsive">
                    <div id="line-disconnect-report-search-result">
                        <table class="table table-hover table-bordered" id="tableresponsive">
                            <thead class="table-header-bg">
                                <tr>
                                    <th>S/N</th>
                                    <th>Box No</th>
                                    <th>Box Adress</th>
                                    <th>Switch Name</th>
                                    <th>Switch Port No</th>
                                    <th>Mc Name</th>
                                    <th>Mc Number</th>
                                    <th>Total Port Number</th>
                                    <th>Total Connected Port</th>
                                    
                                    <th>Pop Address</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $sn =1;
                                ?>
                                @foreach($box as $info)
                                    <tr>
                                        <td>{{ $sn++ }}</td>
                                        <td>{{ $info->box_no}}</td>
                                        <td>{{ $info->box_address}}</td>
                                        <td>{{ $info->switch_name}}</td>
                                        <td>{{ $info->switch_port}}</td>
                                        <td>{{ $info->mc_name}}</td>
                                        <td>{{ $info->mc_number}}</td>
                                        <td>{{ $info->totalport}}</td>
                                        <td>{{ $info->countPort}}</td>
                                        
                                        <td>{{ $info->pop_address}}</td>
                                        <td>
                                           @if($info->deleted_status==1)
                                               @if($currentUserID!='1121')
                                                    {!! Form::open(['url' => '/box-info/reback/'.$info->id, 'files'=>true,'method' => 'post','class' => 'from-delete']) !!}
                                                        <button type="submit" class="delete-options"><i class="fa fa-eye"></i></button>
                                                    {!! Form::close() !!}
                                                @endif   
                                            @else
                                                <a href="{{ URL::to('/box-info/'.$info->id.'/edit') }}" class="edit-option"><i class="fa fa-edit"></i></a>
                                                @if($currentUserID!='1121')
                                                {!! Form::open(['url' => '/box-info/'.$info->id, 'files'=>true,'method' => 'delete','class' => 'from-delete']) !!}
                                                    <button type="submit" class="delete-options"><i class="fa fa-trash-o"></i></button>
                                                {!! Form::close() !!}   
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
           


@stop