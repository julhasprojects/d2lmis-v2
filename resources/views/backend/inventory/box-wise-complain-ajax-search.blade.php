<div class="panel-body">
       <div class="table-responsove">
            <table class="table table-hover table-bordered" id="masterPackageChangeDataTable">
                <thead class="table-header-bg">
                <th> S/N </th>
                <th> Box No </th>
                <th> Box Address</th>
                <th> Total Complain </th>
            </thead>
            <?php 
                $sn = 1;
                $totalComplain = 0;
            ?>
            <tbody>
                @foreach($boxComplain as $comlain)
                <tr>
                    <?php  $totalComplain+=$comlain->totalComlain; ?>
                    <td>{{ $sn++ }}</td>
                    <td>{{$comlain->box_no}}</td>
                    <td>{{$comlain->box_address}}</td>
                    <td>{{$comlain->totalComlain}}</td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="2"></td>
                    <td >Total</td>
                    <td> {{ $totalComplain }}</td>
                </tr>
            </tbody>
        </table>
        </div>
    </div>