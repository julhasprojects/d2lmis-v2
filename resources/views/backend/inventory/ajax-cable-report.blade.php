<table class="table table-hover table-bordered" id="masterPackageChangeDataTable">
            <thead class="table-header-bg">
                <tr>
                    <th>S/N</th>
                    <th>Client Id</th>
                    <th>Box No</th>
                    <th>Box Address</th>
                    <th>Client Address</th>
                    <th>Total Cable</th>
                    
                </tr>
            </thead>
            <tbody>
            	<?php 
            		$sn =1;
            		$totalCable = 0;
            	?>
            	@foreach($cable as $info)
            		<?php 
            			
            			$totalCable +=intval($info->cable_meter);
            		?>
            		<tr>
						<td>{{ $sn++ }}</td>
                        <td>{{ $info->newconid }}</td>
                        <td>{{ $info->box_no }}</td>
						<td>{{ $info->box_address }}</td>
						<td>{{ 'A-'.$info->region_name.', R-'.$info->road.', H-'.$info->house }}</td>
						<td>{{ $info->cable_meter }}</td>
						
					</tr>
            	@endforeach
            	<tr>
            		<td colspan="5"> Total Cable</td>
            		<td> {{ $totalCable}} M</td>
            	</tr>
            </tbody>
        </table>