
<div id="line-disconnect-report-search-result">
    <table class="table table-hover table-bordered" id="masterPackageChangeDataTable">
        <thead class="table-header-bg">
            <tr>
                
                <th>S/N</th>
               
                    <th>Newconid</th>
               
                <th>Box No</th>
                <th>Box Address</th>
                <th>Switch Name</th>
                <th>Switch No</th>
                <th>Mc Name</th>
                <th>Mc Number</th>
                <th>Port Number</th>
                <th>SFP Name</th>
                <th>SFP Number</th>
             	<th>Pop Name</th>
             	<th>Pop Address</th>
             	<th>TG Name</th>
             	<th>Install Date</th>
             	<th>Installed By</th>
             	<th>Action</th>
            </tr>
        </thead>
        <tbody>
        	<?php 
        		$sn =1;
        	?>
        	@foreach($box as $info)
        		<tr>
               
					<td>{{ $sn++ }}</td>
                  
                   
                    <td>{{ $info->newconid}}</td>
             
				    <td>
                            <a href="{{ URL::to('/inventory-box-by-clients/'.$info->box_no) }}" target="_blank">{{ $info->box_no}} </a>
                       
                    </td>
					<td>{{ $info->box_address}}</td>
					<td>{{ $info->switch_name}}</td>

					<td>
                        @if(isset($info->switch_no))
                            {{ $info->switch_no}}
                        @endif
                    </td>
					<td>{{ $info->mc_name}}</td>
					<td>{{ $info->mc_number}}</td>
					<td>{{ $info->switch_port}}</td>
					<td>{{ $info->sfp_name}}</td>
					<td>
                        @if(isset($info->sfp_Number))
                            {{ $info->sfp_Number}}
                        @endif
                    </td>
					<td>{{ $info->pop_name}}</td>
					<td>{{ $info->pop_address}}</td>
					<td>{{ $info->tg_name}}</td>
					<td>{{ $info->install_date}}</td>
					<td>{{ $info->install_by}}</td>
					<td>
						<a href="{{ URL::to('/box-info/'.$info->id.'/edit') }}" class="edit-option"><i class="fa fa-edit"></i></a>
						<!-- {!! Form::open(['url' => '/box-info/'.$info->id, 'files'=>true,'method' => 'delete','class' => 'from-delete']) !!}
                                                    <button type="submit" class="delete-options"><i class="fa fa-trash-o"></i></button>
                                                {!! Form::close() !!}    -->
					</td>
				</tr>
        	@endforeach
        	
        </tbody>
    </table>
</div>
