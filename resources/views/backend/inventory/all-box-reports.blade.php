@extends( 'backend.index' )

@section( 'content_area' )

 <div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);"> Reports</a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory</a></li>
        <li  class="active"><a href="javascript:void(0);"> Master Box Management</a></li>
    </ul>

    <div class="btn-group button-group col-md-4 col-xs-12 col-lg-4 col-sm-4 pull-right" role="group" aria-label="Basic example">
        
        <div class="btn-group pull-right" role="group" aria-label="First group">
            <a class="btn btn-success" href="{{ URL::to('export/free/port/report') }}">Export</a>
            <button type="button" class="btn btn-info"   href="javascript:void()" onClick="window.print()">Print</button>
            <button type="button" class="btn btn-primary">PDF</button>
        </div>
    </div>
</div>
@include('backend.inventory.box-menu')
@include('master.message')
<div id="main-wrapper">
 
    <div class="col-md-12">
    	<div class="panel panel-white">
			<div class="row">	
				<div class="top-search">
					<div class="row">
                        <div class='col-md-2 padding-left-zero'>
                            <div class="form-group">
                                <select class="chosen-select-width" name="switchPort" id="switchPort">
                                    <option value=""> Select Switch Type</option>
                                    <option value="1"> Total</option>
                                    <option value="2"> Single</option>
                                    <option value="3"> Double  </option>
                                </select>
                            </div>
                        </div>	
                        <div class='col-md-1'>
                            <div class="form-group">
                                <div class='input-group'>
                                    <button class="btn btn-primary" id="switchTypeSearch">Search</button>
                                </div>
                            </div>
                        </div>  
                        <div class='col-md-2 padding-left-zero'>
                            <div class="form-group">
                                <select class="chosen-select-width" name="pop" id="pop">
                                    <option value=""> Select Pop Address</option>
                                    @foreach($popInfo as $pop)
                                        <option value="{{ $pop->pop_address}}">{{ $pop->pop_address}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>  
                        <div class='col-md-1'>
                            <div class="form-group">
                                <div class='input-group'>
                                    <button class="btn btn-primary" id="popAdressSearch">Search</button>
                                </div>
                            </div>
                        </div> 	
                        <div class='col-md-2 padding-left-zero'>
                            <div class="form-group">
                                <select class="chosen-select-width" name="mcType" id="mcType">
                                    <option value=""> Select MC Type</option>
                                    <option value="1"> Box With MC</option>
                                    <option value="2"> Box Without MC</option>
                                </select>
                            </div>
                        </div>  
                        <div class='col-md-1'>
                            <div class="form-group">
                                <div class='input-group'>
                                    <button class="btn btn-primary" id="MCTypeSearch">Search</button>
                                </div>
                            </div>
                        </div>  	
                        <div class='col-md-2 padding-left-zero'>
                            <div class="form-group">
                                <select class="chosen-select-width" name="sfType" id="sfType">
                                    <option value=""> Select SFP Type</option>
                                    <option value="1"> Box With SFP</option>
                                    <option value="2"> Box Without SFP</option>
                                </select>
                            </div>
                        </div>  
                        <div class='col-md-1'>
                            <div class="form-group">
                                <div class='input-group'>
                                    <button class="btn btn-primary" id="SFPTypeSearch">Search</button>
                                </div>
                            </div>
                        </div> 
                        <div class='col-md-2 padding-left-zero'>
                            <div class="form-group">
                                <select class="chosen-select-width" name="month" id="month">
                                    <option value="1"> Disconnect Free Port </option>
                                     @for($m = 1;$m <= 12; $m++){
                                        <?php $month =  date("F", mktime(0, 0, 0, $m)); ?>
                                        <option value='{{ $month }}' @if($month == $currentMonth ) selected @endif>{{ $month }}</option> 
                                    @endfor
                                </select>
                            </div>
                        </div>  
                        <div class='col-md-2'>
                            <div class="form-group">
                                <div class='input-group'>
                                    <select class="form-control select-form-desing" id="year" name="year">
                                        @for($year = 2016; $year < date("Y") + 5; $year++){
                                            <option value="{{ $year }}"@if($year == $currentYear) selected @endif> {{ $year }} </option> 
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                          
                        <div class='col-md-2 padding-left-zero'>
                            <div class="form-group">
                                <select class="chosen-select-width" name="type" id="type">
                                    <option value="1"> Newline Use Port </option>
                                    <option value="2"> Disconnect Free Port </option>
                                    
                                </select>
                            </div>
                        </div>  
                        <div class='col-md-1'>
                            <div class="form-group">
                                <div class='input-group'>
                                    <button class="btn btn-primary" id="TotalPortSearch">Search</button>
                                </div>
                            </div>
                        </div>	
					</div>
				</div>
                <div id="masterBoxManagement"> </div>
                <div class="col-md-12">
    			    <div class="panel-body table-responsive">
                        <div id="boxManagementReports"> </div>    
    			    </div>
                </div>
			</div>
		</div>
	</div>
</div>			    



@stop