@extends( 'backend.index' )

@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);"> Reports</a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory</a></li>
        <li  class="active"><a href="javascript:void(0);">  Master Box Reports </a></li>
    </ul>

    <div class="btn-group button-group col-md-4 col-xs-12 col-lg-4 col-sm-4 pull-right" role="group" aria-label="Basic example">
     
        <div class="btn-group pull-right" role="group" aria-label="First group">
            <a class="btn btn-success" href="{{ URL::to('export/inventory/master/box/report') }}">Export</a>
            <button type="button" class="btn btn-info"   href="javascript:void()" onClick="window.print()">Print</button>
            <button type="button" class="btn btn-primary">PDF</button>
        </div>
        
    </div>
</div>
@include('backend.inventory.box-menu')
@include('master.message')
<div id="main-wrapper">

   <div class="col-md-12">
    	<div class="panel panel-white">
			<div class="row">
				<div class="advanced-search master-box-report">
					<div class="monthly-search col-md-6">
	                    <div class="col-md-5 padding-left-zero">
	                        <div class="form-group">
	                            <select class="chosen-select-width" name="boxmonth" id="boxmonth">
									<?php   

		                            	$months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
		                            	$currentMonth = date("m", strtotime($currentMonth));
		                            	
		                            ?>
	                            	@foreach($months as $key => $month) 
	                            		@if ($currentMonth == $month) 
	                            			<option value="{{ $month }}" selected> {{ date("F", mktime(0, 0, 0, $month, 10)) }} </option>
	                            		@else
	                            			<option value="{{ $month }}"> {{ date("F", mktime(0, 0, 0, $month, 10)) }} </option>
	                            		@endif
	                            	@endforeach
								</select>
							</div>
	                    </div>
	                    <div class="col-md-5 padding-left-zero">
	                        <div class="form-group">
	                 			<select class="form-control select-form-desing chosen-container chosen-container-single" id="boxyear" name="boxyear">
	                                @for($year = $currentYear; $year < $currentYear+5; $year++)
	                            		
	        							@if( $currentYear == $year) 
	                               	 		<option value="{{ $year }}" selected="selected">{{ $year }}</option>
	                               		@else
	                               		 	<option value="{{ $year }}">{{ $year }}</option>
	                               		@endif
	                                @endfor
		                        </select>
							</div>
						</div>
						<div class="col-md-2 padding-left-zero">
							<button class="btn btn-success" id="boxMonthlySearch"> Find </button>
						</div>
					</div>
					<div class="col-md-2 padding-left-zero">
                        <div class="form-group">
                        	<label> Installed By </label>
                            <select class="chosen-select-width" name="installby" id="installby">
                                <option value="">Search</option>
                                @foreach($employee as $list)
									<option value="{{$list->employeeID}}"> {{$list->fullName}} </option>
								@endforeach
							</select>
						</div>
                    </div>
                    <div class="col-md-2 padding-left-zero">
                        <div class="form-group">
                        	<label> Updated By </label>
                            <select class="chosen-select-width" name="updateby" id="updateby">
                                <option value="">Search</option>
								@foreach($employee as $list)
									<option value="{{$list->employeeID}}"> {{$list->fullName}} </option>
								@endforeach
							</select>
						</div>
                    </div>
				</div>
				<div class="advanced-search master-box-report">
					<div class="monthly-search col-md-6">
						<div class='col-md-5'>
		                    <div class="form-group">
		                        <select class="chosen-select-width" name="area" id="freeport_region_search">
		                            <option value="">Select Area</option>
		                            @foreach($region as $regionInfo)
		                                <option value="{{ $regionInfo->region_id }}"> {{ $regionInfo->region_name }} </option>
		                            @endforeach
		                        </select>
		                    </div>
		                </div>
		                <div class='col-md-5'>
		                    <div class="form-group">
		                        <select id="freeport_road_search" class="form-control" name="road"  placeholder="11">
		                            <option value=""> Select Road </option>
		                        </select>
		                    </div>
		                </div>
		                <div class='col-md-2 padding-left-zero'>
			                <div class="form-group">
			                    <div class='input-group'>
			                        <button class="btn btn-primary" id="regionBoxSearch">Find</button>
			                    </div>
			                </div>
			            </div> 
		            </div>
	            </div> 
				<div id="masterBoxLoader"> </div>
				<div class="body">
				    <div class="panel-body">
				        <div class="table-responsive"  id="masterBoxResult">
				            <div id="line-disconnect-report-search-result">
				            	
					                <table class="table table-hover table-bordered" id="tableresponsive">
					                    <thead class="table-header-bg">
					                        <tr>
					                     
					                            <th>S/N</th>
					                        
					                            <th>Box No</th>
					                            <th>Box Address</th>
					                            <th>Switch Name</th>
					                            <th>Switch No</th>
					                            <th>Mc Name</th>
					                            <th>Mc Number</th>
					                            <th>Port Number</th>
					                            <th>SFP Name</th>
					                            <th>SFP Number</th>
					                         	<th>Pop Name</th>
					                         	<th>Pop Address</th>
					                         	<th>TG Name</th>
					                         	<th>Install Date</th>
					                         	<th>Installed By</th>
					                         	<th>Action</th>
					                        </tr>
					                    </thead>
					                    <tbody>
					                    	<?php 
					                    		$sn =1;
					                    	?>
					                    	@foreach($box as $info)
					                    	@if($info->deleted_status==1)
					                    		<tr class="hide-bg">
					                    	@else
					                    		<tr>
					                    	@endif
					                    			
													<td>{{ $sn++ }}</td>
													
													<td>
														<a href="{{ URL::to('/inventory-box-by-clients/'.$info->box_no) }}" target="_blank">{{ $info->box_no}} </a>
													</td>
													<td>{{ $info->box_address}}</td>
													<td>{{ $info->switch_name}}</td>
													<td>{{ $info->switch_no}}</td>
													<td>{{ $info->mc_name}}</td>
													<td>{{ $info->mc_number}}</td>
													<td>{{ $info->switch_port}}</td>
													<td>{{ $info->sfp_name}}</td>
													<td>{{ $info->sfp_Number}}</td>
													<td>{{ $info->pop_name}}</td>
													<td>{{ $info->pop_address}}</td>
													<td>{{ $info->tg_name}}</td>
													<td>{{ $info->install_date}}</td>
													<td>{{ $info->install_by}}</td>
													<td>
														
														@if($info->deleted_status==1)
															@if($currentUserID!='1121')
																{!! Form::open(['url' => '/box-info/reback/'.$info->id, 'files'=>true,'method' => 'post','class' => 'from-delete']) !!}
				                                    			    <button type="submit" class="delete-options"><i class="fa fa-eye"></i></button>
				                                    			{!! Form::close() !!}   
			                                    			@endif
														@else
															<a href="{{ URL::to('/box-info/'.$info->id.'/edit') }}" class="edit-option"><i class="fa fa-edit"></i></a>
															@if($currentUserID!='1121')
																{!! Form::open(['url' => '/box-info/'.$info->id, 'files'=>true,'method' => 'delete','class' => 'from-delete']) !!}
				                                    			    <button type="submit" class="delete-options"><i class="fa fa-trash-o"></i></button>
				                                    			{!! Form::close() !!}  
			                                    			@endif 
		                                    			@endif
													</td>
												</tr>
					                    	@endforeach
					                    	
					                    </tbody>
					                </table>
				             
				            </div>
				        </div>
				    </div>
				</div>
		    </div>
		</div>
	</div>			    
</div>


@stop