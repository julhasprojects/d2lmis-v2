@extends( 'backend.index' )

@section( 'content_area' )

 <div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);"> Reports</a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory</a></li>
        <li  class="active"><a href="javascript:void(0);"> Box Wise Free Port Reports </a></li>
    </ul>

    <div class="btn-group button-group col-md-4 col-xs-12 col-lg-4 col-sm-4 pull-right" role="group" aria-label="Basic example">
        
        <div class="btn-group pull-right" role="group" aria-label="First group">
            <a class="btn btn-success" href="{{ URL::to('export/free/port/report') }}">Export</a>
            <button type="button" class="btn btn-info"   href="javascript:void()" onClick="window.print()">Print</button>
            <button type="button" class="btn btn-primary">PDF</button>
        </div>
    </div>
</div>
@include('backend.inventory.box-menu')
@include('master.message')
<div id="main-wrapper">
 
    <div class="col-md-12">
    	<div class="panel panel-white">
			<div class="row">	
				<div class="top-search">
					<div class="row">
						<div class='col-md-2 padding-left-zero'>
                            <div class="form-group">
                                <select class="chosen-select-width" name="area" id="freeport_region_search">
                                    <option value="">Select Area</option>
                                    @foreach($region as $regionInfo)
                                        <option value="{{ $regionInfo->region_id }}"> {{ $regionInfo->region_name }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class='col-md-2 padding-left-zero'>
                            <div class="form-group">
                                <select id="freeport_road_search" class="form-control" name="road"  placeholder="11">
                                    <option value=""> Select Road </option>
                                </select>
                            </div>
                        </div>
                        <div class='col-md-2'>
                            <div class="form-group">
                                <div class='input-group'>
                                    <button class="btn btn-primary" id="freeportsearch">Find</button>
                                </div>
                            </div>
                        </div>  	

                        					
					</div>
				</div>
                <div id="invenotrySearchLoader"> </div>
			    <div class="panel-body">
                    <div id="freeportsearchresult">
                        <div class="table-responsove">
    			            <table class="table table-hover table-bordered" id="masterPackageChangeDataTable">
                                <thead class="table-header-bg">
                                    <tr>
                                        <th><span class="box-info-sum">S/N</span></th>
                                        <th><span class="box-info-sum">Box No</span></th>
                                        <th><span class="box-info-sum">Box Address</span></th>
                                        <th><span class="box-info-sum">Switch Name</span></th>
                                        <th><span class="box-info-sum">Total Port</span></th>
                                        <th><span class="box-info-sum">Total Used Port</span></th>
                                        <th><span class="box-info-sum">Total Free Port </span></th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $sn =1;
                                        $totalFreePort = 0;
                                        $useTotalPorts  = 0;
                                        $alltotalPort  = 0;
                                    ?>
                                    @foreach($boxFreeport as $info)
                                        <?php
                                            $totalFreePort +=$info->free_port;
                                            $useTotalPorts +=$info->countPort;
                                            $alltotalPort  +=$info->totalport;
                                            $freePortUse   =abs($info->totalport-$info->countPort);
                                            
                                        ?>
                                        <tr>
                                            <td><span class="box-info-sum">{{ $sn++ }}</span></td>
                                            <td><span class="box-info-sum">{{ $info->box_no}}</span></td>
                                            <td><span class="box-info-sum">{{ $info->box_address}}</span></td>
                                            <td><span class="box-info-sum">{{ $info->switch_name}}</span></td>
                                            <td><span class="box-info-sum">{{ $info->totalport}}</span></td>
                                            @if(isset($info->countPort))
                                            <td><span class="box-info-sum">{{ $info->countPort}}</span></td>
                                            @endif
                                           
                                            <td><span class="box-info-sum">{{ $freePortUse}}</span></td>
                                            
                                        </tr>
                                    @endforeach
                                     <tr>
                                        <td colspan="3"></td>
                                        
                                        <td>Total Free Port</td>
                                        <td>{{$useTotalPorts}} </td>
                                        <td>{{$alltotalPort}} </td>
                                        <td>{{$totalFreePort}} </td>
                                    </tr>
                                </tbody>
                                <tbody>
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
			    </div>
			</div>
		</div>
	</div>
</div>			    



@stop