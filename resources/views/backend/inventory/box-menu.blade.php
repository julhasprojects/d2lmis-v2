<div id="ramadanmenu" class="menu-menu-1-container bottom-menubar">
		<!-- <div id="menu-button">Menu</div> -->
		<ul id="menu-menu-1" class="menu">
			<li>
				<a href="#" target="_blank">
					<i class="fa fa-dropbox"></i>
					Box Setup <i class="fa fa-angle-down" aria-hidden="true"></i>
				</a>
				<ul class="sub-menu">
                 	<li><a href="{{ URL::to( '/box-info/create' ) }}"> Setup New Box </a></li>
                    <li><a href="{{ URL::to( '/update-box-number' ) }}"> Update Box Info </a></li>    
                    <li><a href="{{ URL::to( '/box-info' ) }}">Update Box Drive Info </a></li>    
                </ul>
			</li>
			<li>
				<a href="{{ URL::to( '/inventory-box-reports' ) }}" >
					<i class="fa fa-dropbox"></i>
					Box Reports <i class="fa fa-angle-down" aria-hidden="true"></i>
				</a>
				<ul class="sub-menu">
                    <li><a href="{{ URL::to( '/inventory-box-reports' ) }}" >No of Clients Per Box </a></li>
                    <li><a href="{{ URL::to( '/box-info/' ) }}">All Box List</a></li>
                    <li><a href="{{ URL::to( '/inventory/free/port/report' ) }}">Free Port Per Box</a></li>
	                <li><a href="{{ URL::to( '/inventory/box/wise/complain' ) }}">Box Wise Complain Summary</a></li>
                	<li><a href="{{ URL::to( '/box-info/' ) }}">No Of Switch Per Box</a></li>
                	<li><a href="{{ URL::to( '/box-info/' ) }}">Box Searching Report</a></li>
                	<li><a href="{{ URL::to( '/box-info/' ) }}">Monthly Box Setup</a></li>
                	<li><a href="{{ URL::to( '/location/wise/box/reports/' ) }}">Location Wise Box Reports</a></li>
                </ul>
			</li>
			
			<li>
				<a href="#">
					<i class="fa fa-dropbox"></i>
					Cable Reports <i class="fa fa-angle-down" aria-hidden="true"></i>
				</a>
				<ul class="sub-menu">
                    <li><a href="{{ URL::to( '/inventory/master/cable/report' ) }}" >Master Cable Report </a></li>
                    <li><a href="{{ URL::to( '/inventory/master/cable/report' ) }}" >Clients By Area & Road</a></li>
                    <li><a href="{{ URL::to( '/box/wise/cable/report' ) }}" >Box Wise Cable Report</a></li>
                </ul>
			</li>
			<li>
				<a href="{{ URL::to( '/interface/setup' ) }}"> 
					<i class="fa fa-dropbox"></i> 
					Interface(pop)
				</a>
			</li>
			<li>
				<a href="{{ URL::to( '/master/box/management' ) }}"> 
					<i class="fa fa-dropbox"></i> 
					Master Box Management
				</a>
			</li>
		
		</ul>
	</div>
