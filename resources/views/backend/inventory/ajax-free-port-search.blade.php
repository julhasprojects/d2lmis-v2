<div class="table-responsove">
    <table class="table table-hover table-bordered" id="masterPackageChangeDataTable">
        <thead class="table-header-bg">
            <tr>
                <th><span class="box-info-sum">S/N</span></th>
                <th><span class="box-info-sum">Box No</span></th>
                <th><span class="box-info-sum">Box Address</span></th>
                <th><span class="box-info-sum">Switch Name</span></th>
                <th><span class="box-info-sum">Total Port</span></th>
                <th><span class="box-info-sum">Total Used Port</span></th>
                <th><span class="box-info-sum">Total Free Port </span></th>
               
            </tr>
        </thead>
        <tbody>
        <?php 
            $sn = 1;
            $totalFreePort  = 0;
            $useTotalPorts  = 0;
            $alltotalPort   = 0;
        ?>
        @foreach($boxFreeport as $info)
            <?php
                $totalFreePort += $info->free_port;
                $useTotalPorts += $info->countPort;
                $alltotalPort  += $info->totalport;
                $freePortUse   = abs($info->totalport-$info->countPort);
                
            ?>
            <tr>
                <td><span class="box-info-sum">{{ $sn++ }}</span></td>
                <td><span class="box-info-sum">{{ $info->box_no }}</span></td>
                <td><span class="box-info-sum">{{ $info->box_address }}</span></td>
                <td><span class="box-info-sum">{{ $info->switch_name }}</span></td>
                <td><span class="box-info-sum">{{ $info->totalport }}</span></td>
                @if(isset($info->countPort))
                <td><span class="box-info-sum">{{ $info->countPort }}</span></td>
                @endif
               
                <td><span class="box-info-sum">{{ $freePortUse }}</span></td>
                
            </tr>
        @endforeach
         <tr>
            <td colspan="3"></td>
            
            <td>Total Free Port</td>
            <td>{{ $useTotalPorts }} </td>
            <td>{{ $alltotalPort }} </td>
            <td>{{ $totalFreePort }} </td>
        </tr>
    </tbody>
        <tbody>
           
        </tbody>
    </table>
</div>