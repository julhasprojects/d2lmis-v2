@extends( 'backend.index' )

@section( 'content_area' )
<script type="text/javascript">
    $(document).ready(function(){
        $('#masterPackageChangeDataTable').DataTable({
           'paging' : true  ,
           "pageLength": 50,
           'searching' : true
        });
        
    });

</script>
 <div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);"> Reports</a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory</a></li>
        <li  class="active"><a href="javascript:void(0);"> Box Wise Complain </a></li>
    </ul>

    <div class="btn-group button-group col-md-4 col-xs-12 col-lg-4 col-sm-4 pull-right" role="group" aria-label="Basic example">
        
        <div class="btn-group pull-right" role="group" aria-label="First group">
            <a class="btn btn-success" href="{{ URL::to('export/inventory/box/wise/complain') }}">Export</a>
            <button type="button" class="btn btn-info"   href="javascript:void()" onClick="window.print()">Print</button>
            <button type="button" class="btn btn-primary">PDF</button>
        </div>
    </div>
</div>
@include('backend.inventory.box-menu')
@include('master.message')
<div id="main-wrapper">
   <div class="col-md-12">
    	<div class="panel panel-white">
			<div class="row">	
				<div class="top-search">
					<div class="row">
						<div class="col-md-6">
                            <div class="col-md-5 padding-left-zero">
                                <div class="form-group">
                                    <select class="chosen-select-width" name="complainmonth" id="complainmonth">
                                        <?php   

                                            $months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
                                            $currentMonth = date("m", strtotime($currentMonth));
                                            
                                        ?>
                                        @foreach($months as $key => $month) 
                                            @if ($currentMonth == $month) 
                                                <option value="{{ $month }}" selected> {{ date("F", mktime(0, 0, 0, $month, 10)) }} </option>
                                            @else
                                                <option value="{{ $month }}"> {{ date("F", mktime(0, 0, 0, $month, 10)) }} </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-5 padding-left-zero">
                                <div class="form-group">
                                    <select class="form-control select-form-desing chosen-container chosen-container-single" id="complainyear" name="complainyear">
                                        @for($year = $currentYear; $year < $currentYear+5; $year++)
                                            
                                            @if( $currentYear == $year) 
                                                <option value="{{ $year }}" selected="selected">{{ $year }}</option>
                                            @else
                                                <option value="{{ $year }}">{{ $year }}</option>
                                            @endif
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2 padding-left-zero">
                                <button class="btn btn-success" id="complainMonthlySearch"> Find </button>
                            </div>
                        </div>	
                        <div class="date-search col-md-6">
                            <div class="col-md-5 padding-left-zero">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control bbibillfrom" data-date-format="yyyy-mm-dd" name="complaindateform" id="complaindateform" placeholder="From" required="" type="text">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control bbibillto" data-date-format="yyyy-mm-dd" name="complaindateto" id="complaindateto" placeholder="To" required="" type="text">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>

                                    </div>
                                   
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="input-group">
                                        <button class="btn btn-primary" id="complainDateSearch"> Find </button>
                                    </div>
                                </div>
                            </div>  
                        </div>
                        </div>					
					</div>
			</div>
            <div id="loaderRun"> </div>
			<div  class="table-responsive" id="complainSearch">
			    <div class="panel-body">
			       <div class="table-responsove">
                        <table class="table table-hover table-bordered" id="masterPackageChangeDataTable">
                            <thead class="table-header-bg">
                            <th> S/N </th>
                            <th> Box No </th>
                            <th> Box Address</th>
                            <th> Total Complain </th>
                        </thead>
                        <?php 
                            $sl = 1;
                            $totalComplain=0;
                        ?>
                        <tbody>
                            @foreach($boxComplain as $comlain)
                            <tr>
                                <?php  $totalComplain += $comlain->totalComlain; ?>
                                <td>{{ $sl++ }}</td>
                                <td>{{$comlain->box_no}}</td>
                                <td>{{$comlain->box_address}}</td>
                                <td>{{$comlain->totalComlain}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tbody>
                            <tr>
                                <td colspan="2"></td>
                                <td >Total</td>
                                <td> {{ $totalComplain }}</td>
                            </tr>
                        </tbody>
                    </table>
				    </div>
				</div>
	        </div>
		</div>
	</div>			    
</div>


@stop