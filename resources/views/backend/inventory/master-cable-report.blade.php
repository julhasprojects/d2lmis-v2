@extends( 'backend.index' )

@section( 'content_area' )


 <div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);"> Reports</a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory</a></li>
        <li  class="active"><a href="javascript:void(0);">  Master Cable Reports </a></li>
    </ul>

    <div class="btn-group button-group col-md-4 col-xs-12 col-lg-4 col-sm-4 pull-right" role="group" aria-label="Basic example">
        
        <div class="btn-group pull-right" role="group" aria-label="First group">
            <a class="btn btn-success" href="{{ URL::to('export/inventory/master/cable/report') }}">Export</a>
            <button type="button" class="btn btn-info"   href="javascript:void()" onClick="window.print()">Print</button>
            <button type="button" class="btn btn-primary">PDF</button>
        </div>
    </div>
</div>
@include('backend.inventory.box-menu')
@include('master.message')
<div id="main-wrapper">
  
   <div class="col-md-12">
    	<div class="panel panel-white">
			<div class="row">	
				<div class="top-search">
					<div class="row">
						<!-- <div class="row clearfix"> -->
						<div class='col-md-12 padding-left-zero  cable-search'>
							<div class="col-md-7">
								<div class='col-md-3 padding-left-zero'>
									<div class="form-group">
										<select class="chosen-select-width"  name="areasearch" id="areasearch">
											<option value="">Select Area</option>
											@foreach($region as $regionInfo)
												<option value="{{ $regionInfo->region_id }}"> {{ $regionInfo->region_name }} </option>
											@endforeach
										</select>
									</div>
								</div>
								<div class='col-md-2'>
									<div class="form-group">
										<div class='input-group'>
											<button class="btn btn-primary" id="regionsearch">Submit</button>
										</div>
									</div>
								</div>	

								<div class='col-md-3 padding-left-zero'>
									<div class="form-group">
										<select class="chosen-select-width" name="area" id="admin-brstatement-road-list-export">
											<option value="">Select area</option>
											@foreach($region as $regionInfo)
												<option value="{{ $regionInfo->region_id }}"> {{ $regionInfo->region_name }} </option>
											@endforeach
										</select>
									</div>
								</div>
								<div class='col-md-3 padding-left-zero'>
									<div class="form-group">
										<select id="admin-brstatement-road-list-export-result" class="form-control" name="road"  placeholder="11"> </select>
									</div>
								</div>
								<div class='col-md-1'>
									<div class="form-group">
										<div class='input-group'>
											<button class="btn btn-primary" id="roadsearch">Submit</button>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-5">
	                            <div class="col-md-5 padding-left-zero">
	                                <div class="form-group">
	                                    <select class="chosen-select-width" name="cablemonth" id="cablemonth">
	                                        <?php   

	                                            $months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
	                                            $currentMonth = date("m", strtotime($currentMonth));
	                                            
	                                        ?>
	                                        @foreach($months as $key => $month) 
	                                            @if ($currentMonth == $month) 
	                                                <option value="{{ $month }}" selected> {{ date("F", mktime(0, 0, 0, $month, 10)) }} </option>
	                                            @else
	                                                <option value="{{ $month }}"> {{ date("F", mktime(0, 0, 0, $month, 10)) }} </option>
	                                            @endif
	                                        @endforeach
	                                    </select>
	                                </div>
	                            </div>
	                            <div class="col-md-5 padding-left-zero">
	                                <div class="form-group">
	                                    <select class="form-control select-form-desing chosen-container chosen-container-single" id="cableyear" name="cableyear">
	                                        @for($year = $currentYear; $year < $currentYear+5; $year++)
	                                            
	                                            @if( $currentYear == $year) 
	                                                <option value="{{ $year }}" selected="selected">{{ $year }}</option>
	                                            @else
	                                                <option value="{{ $year }}">{{ $year }}</option>
	                                            @endif
	                                        @endfor
	                                    </select>
	                                </div>
	                            </div>
	                            <div class="col-md-2 padding-left-zero">
	                                <button class="btn btn-success" id="cableMonthlySearch"> Submit </button>
	                            </div>
	                        </div>
						</div>								
					</div>
				</div>
				<div id="loaderRun"> </div>
				<div id="Today" class="">
				    <div class="panel-body">
				           <div id="cableResult"> </div>
				    </div>
				</div>
		    </div>
		</div>
	</div>			    
</div>


@stop