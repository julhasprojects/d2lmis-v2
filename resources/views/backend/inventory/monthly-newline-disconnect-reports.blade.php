<h4>Total Box : {{ $totalPort }}</h4>
<table class="table table-hover table-bordered" >
    <thead class="table-header-bg">
        <tr>
            <th>S/N</th>
            <th>CID</th>
            <th>Box No</th>
            <th>Box Address</th>
            <th>Switch Name</th>
            <th>Mc Name</th>
            <th>Mc Number</th>
            <th>Port Number</th>
            <th>SFP Name</th>
            <th>SFP Number</th>
            <th>Pop Name</th>
            <th>Pop Address</th>
            <th>TG Name</th>
            <th>Install Date</th>
            <th>Installed By</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $sl =1;
            $totalFreePort = 0;
        ?>
        @foreach($box as $info)

           <tr>
               <td>{{ $sl++ }}</td>
               <td>{{ $info->newconid }}</td>
               <td>{{ $info->box_no }}</td>
               <td>{{ $info->box_address }}</td>

                <td><span class="mcNumber">{{ $info->switch_name}}</span></td>
                <td><span class="mcNumber">{{ $info->mc_name}}</span> </td>
                <td>{{ $info->mc_number}}</td>
                <td>{{ $info->switch_port}}</td>
                <td><span class="mcNumber">{{ $info->sfp_name}}</span></td>
                <td>{{ $info->sfp_Number}}</td>
                <td>{{ $info->pop_name}}</td>
                <td>{{ $info->pop_address}}</td>
                <td>{{ $info->tg_name}}</td>
                <td>{{ $info->install_date}}</td>
                <td>
                    @foreach($employeeList as $employeeInfo)
                        @if($employeeInfo->employeeID == $info->install_by)
                        {{ $employeeInfo->fullName}}
                        @endif
                    @endforeach
                </td>
           </tr>

        @endforeach
         <tr>
            <td  colspan="5"></td>
            <td colspan="2" >
                @if($type == 1)
                    Total User port
                @else
                    Total Free port
                @endif
            </td>
            <td>{{$totalPort}}</td>
            <td></td>
        </tr>
        
    </tbody>
</table>