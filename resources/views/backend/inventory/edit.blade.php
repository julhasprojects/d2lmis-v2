@extends( 'backend.index' )

@section( 'content_area' )

 <div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);"> Reports</a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory</a></li>
        <li  class="active"><a href="javascript:void(0);">  Box Information update </a></li>
    </ul>

    <div class="btn-group button-group col-md-4 col-xs-12 col-lg-4 col-sm-4 pull-right" role="group" aria-label="Basic example">
        
        <div class="btn-group pull-right" role="group" aria-label="First group">
            <a class="btn btn-success" href="{{ URL::to('export-master-box-report') }}">Export</a>
            <button type="button" class="btn btn-info"   href="javascript:void()" onClick="window.print()">Print</button>
            <button type="button" class="btn btn-primary">PDF</button>
        </div>
    </div>
</div>
@include('backend.inventory.box-menu')
@include('master.message')
<div id="main-wrapper">
    
    <div class="col-md-12 left-box">
		<div class="panel panel-white">
			<h2 class="title-house"> Update  Box Information </h2>
				{!! Form::open(['url' => '/box-info/'.$info->id, 'files'=>true,'method' => 'PUT']) !!}  
	                <div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label" for="boxNumber">
										Box Number
										<span class="required">*</span>
									</label>
									<div class="col-md-8">
										<input id="boxNumber" class="form-control" name="boxNumber"  type="text" required="" value="{{ $info->box_no}}">
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
										<div class="help-block with-errors"></div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label" for="boxaddress">
										Box Address
									</label>
									<div class="col-md-8">
										<input id="boxaddress" class="form-control" name="boxaddress"  type="text"    value="{{ $info->box_address}}">
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
										<div class="help-block with-errors"></div>
									</div>
								</div>
							</div>
						</div>
						
				       
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label" for="switchname">
										Switch Name
									</label>
									<div class="col-md-8">
										<input id="switchname" class="form-control" name="switchname" type="text"  value="{{ $info->switch_name}}">
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
										<div class="help-block with-errors"></div>
									</div>
								</div>
							</div>
							
							<div class="col-md-6 pull-right">									
								<div class="form-group">
									<label class="col-md-4 control-label" for="Port">
										Switch Port Number
									</label>
									<div class="col-md-8">
										 <input type="text" class="form-control" id="tokenfield-1"  name="port" placeholder="Type Port and hit coma" value="{{ $info->switch_port}}"/>
									</div>
								</div>
							</div>
							
						</div>
						<div class="row">
							<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 pull-left">
								<div class="form-group">
									<label class="col-md-4 control-label" for="totalport">
										Total Port of the Switch
									</label>
									<div id="">
										<div class="col-md-8">
											<input id="fcnclientid" class="form-control" name="totalport" type="text" required="" readonly  value="{{ $info->totalport}}">
											<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
											<div class="help-block with-errors"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 pull-right">
								<div class="form-group">
									<label class="col-md-4 control-label" for="fcnclientid">
										Free Port
									</label>
									<div class="col-md-8">
										<input id="freeportupdate" class="form-control" name="freeport" type="text" readonly  value="{{ $info->totalport-$totalBoxUser->totalUserPort}}">
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
										<div class="help-block with-errors"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 pull-left">
								<div class="form-group">
									<label class="col-md-4 control-label" for="mcname">
										Mc Name
									</label>
									<div class="col-md-8">
										<input id="mcname" class="form-control mcname" name="mcname" type="text"  value="{{ $info->mc_name}}">
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
										<div class="help-block with-errors"></div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 pull-right">
								<div class="form-group">
									<label class="col-md-4 control-label" for="mcnumber">
										Total No Of Mc Number
									</label>
									<div class="col-md-8">
										<input id="mcnumber" class="form-control" name="mcnumber" type="text"  readonly  value="{{ $info->mc_number}}">
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
										<div class="help-block with-errors"></div>
									</div>
								</div>
							</div>
							
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label" for="sfpname">
										SFP Name
										
									</label>
									<div class="col-md-8">
										<input id="sfpname" class="form-control sfpname" name="sfpname" type="text"  value="{{ $info->sfp_name}}" >
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
										<div class="help-block with-errors"></div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label" for="sfpnumber">
										Total Num of SFP
										
									</label>
									<div class="col-md-8">
										<input id="sfpnumber" class="form-control" name="sfpnumber" type="text" readonly  value="{{ $info->sfp_number}}">
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
										<div class="help-block with-errors"></div>
									</div>
								</div>
							</div>
						</div>

						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label" for="popname">
										Pop Name
									</label>
									<div class="col-md-8">
										<select name="popname" id="popname" class="chosen-select-width">
											@foreach($popName as $popinfo)
												<option value="{{ $popinfo->interface_name }}" @if( $popinfo->interface_name == $info->pop_name ) selected @endif> {{ $popinfo->interface_name }} </option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label" for="popaddress">
										Pop Address
										
									</label>
									<div class="col-md-8">
										<input id="popaddress" class="form-control" name="popaddress" type="text"  value="{{ $info->pop_address}}">
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
										<div class="help-block with-errors"></div>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label" for="tgname">
										TG Name
									</label>
									<div class="col-md-8">
										<input id="tgname" class="form-control" name="tgname" type="text"  value="{{ $info->tg_name}}">
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
										<div class="help-block with-errors"></div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label" for="installdate">
										Install Date
										
									</label>
									<div class="col-md-8">
										<div class="input-group">
	                                        <input class="form-control installdate" data-date-format="yyyy-mm-dd" name="installdate" id="installdate" placeholder="install date"  value="{{ $info->install_date}}" readonly="" required="" type="text">
	                                        <span class="input-group-addon">
	                                            <span class="glyphicon glyphicon-calendar"></span>
	                                        </span>
	                                    </div>
										
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-xs-12">
								<div class="form-group">
									<label class="col-md-4 control-label" for="updateby">
										Install By
										<span class="required">*</span>
									</label>
									<div class="col-md-8">
										@foreach($employee as $emp_info)
											@if($emp_info->employeeID == $info->install_by)
												{{ $emp_info->fullName}}
											@endif
										@endforeach
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group">
									<label class="col-md-4 control-label" for="updateby">
										Updated By
										<span class="required">*</span>
									</label>
									<div class="col-md-8">
										<select name="updateby" id="updateby" class="chosen-select-width frontpanel-searc-regionbased">
											<option value=""> Select Updated By</option>
											@foreach($employee as $info)
												<option value="{{ $info->employeeID}}">{{ $info->fullName}}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group">
									<label class="col-md-4 control-label" for="branch">
										Branch
									</label>
									<div class="col-md-8 checkbox-submit">
										<select class="chosen-select-width" id="branch" name="branch" >
			                                <option value="">Select Branch</option>
			                                @foreach($branchInfo as $info)
			                                	<option value="{{ $info->id }}" @if($info->id == $bracnhID) selected @endif>{{ $info->branch_name }}</option>
			                                @endforeach
			                            </select>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							
						</div>
						<div class="row">
							<div class="col-md-12 col-xs-12 button-submit">
								<button class="btn btn-box-buton btn-success">Update </button>
							</div>
						</div>
					</div>
				{!!Form::close()!!}
		</div>
	</div>
    	    
</div>


@stop