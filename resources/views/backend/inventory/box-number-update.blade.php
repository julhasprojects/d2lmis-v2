@if(isset($boxInfo->box_address))
    <div class="form-group">
        <label for="input-Default" class="col-sm-2 control-label">Box Address</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="box_address" name="box_address" value="{{$boxInfo->box_address}}" >
        </div>
    </div>
@endif
<div class="form-group overall-form-update">
    <label for="input-Default" class="col-sm-2 control-label">Switch Name</label>
    <div class="col-sm-10">
        <select class="chosen-select-width" id="switch_name" name="switch_name" required value="{{ $boxInfo->fiber_root}}" >
            <option value="">Select Switch Name</option>
            @foreach($switchName as $switchInfo)
                <option value="{{  $switchInfo->switch_name }}">{{  $switchInfo->switch_name }}</option>
            @endforeach
        </select>
       
    </div>
</div>
<div class="row overall-form-update">
    <div class="form-group">
        <label for="linestatus" class="col-sm-2 control-label">Port/Switch</label>
        <div class="col-sm-10">
            <select multiple class="chosen-select-width" name="port[]" tabindex="16" >
                <option value="" selected="true"></option>
                <option value="5">5</option>
                <option value="8">8</option>
                <option value="16">16</option>
                <option value="24">24</option>
                <option value="48">48</option>
                <option value="MC">MC</option>
            </select>
        </div>
    </div>
</div>
<div class="row overall-form-update">
    <div class="form-group">
        <label for="input-Default" class="col-sm-2 control-label">Port Number</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="switch_no" name="switch_no" value="" >
        </div>
    </div>
</div>
<div class="form-group overall-form-update free-port">
    <label for="input-Default" class="col-sm-2 control-label">Free Port</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="free_port" name="free_port" value="{{ $boxInfo->free_port }}" >
    </div>
</div>
<div class="form-group overall-form-update">
    <label for="input-Default" class="col-sm-2 control-label">MC Name</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="mc_name" placeholder="Media Connector" name="mc_name" value="{{ $boxInfo->mc_name }}" >
    </div>
</div>
<div class="form-group overall-form-update">
    <label for="linestatus" class="col-sm-2 control-label">Fiber Root<span class="required">*</span></label>
    <div class="col-sm-10">
        <select class="chosen-select-width" id="fiber_root" name="fiber_root" required value="{{ $boxInfo->fiber_root}}" >
            <option value=""></option>
            <option value="">Select Fiber Address</option>
            <option value="POP Address">POP Address</option>
            <option value="Server">Server</option>
        </select>
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        <div class="help-block with-errors"></div>
    </div>
</div>
<div class="form-group overall-form-update">
    <label for="input-Default" class="col-sm-2 control-label">POP Address</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="pop_address" name="pop_address" value="{{ $boxInfo->pop_address}}" >
    </div>
</div>