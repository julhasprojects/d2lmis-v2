@extends( 'backend.index' )

@section( 'content_area' )

 <div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);"> Reports</a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory</a></li>
        <li  class="active"><a href="javascript:void(0);"> Box Wise Cable Reports </a></li>
    </ul>

    <div class="btn-group button-group col-md-4 col-xs-12 col-lg-4 col-sm-4 pull-right" role="group" aria-label="Basic example">
        
        <div class="btn-group pull-right" role="group" aria-label="First group">
            <a class="btn btn-success" href="{{ URL::to('export/inventory/master/cable/report') }}">Export</a>
            <button type="button" class="btn btn-info"   href="javascript:void()" onClick="window.print()">Print</button>
            <button type="button" class="btn btn-primary">PDF</button>
        </div>
    </div>
</div>
@include('backend.inventory.box-menu')
@include('master.message')

<div id="main-wrapper">
    
   <div class="col-md-12">
    	<div class="panel panel-white">
			<div class="row">	
				<div class="top-search">
					<div class="row">
										
					</div>
				</div>
				<div id="Today" class="">
				    <div class="panel-body">
                        <div class="table-responsove">
				            <table class="table table-hover table-bordered" id="responsivetabel">
                                <thead class="table-header-bg">
                                    <tr>
                                        <th><span class="box-info-sum">S/N</span></th>
                                        <th><span class="box-info-sum">Box No</span></th>
                                        <th><span class="box-info-sum">Box Address</span></th>
                                        <th><span class="box-info-sum">Cable Meter </span></th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $sn =1;
                                        $totalCable = 0;
                                    ?>
                                    @foreach($boxCable as $info)
                                        <?php
                                            $totalCable +=$info->sum;
                                        ?>
                                        <tr>
                                            <td><span class="box-info-sum">{{ $sn++ }}</span></td>
                                            <td><span class="box-info-sum">{{ $info->box_no}}</span></td>
                                            <td><span class="box-info-sum">{{ $info->box_address}}</span></td>
                                            <td><span class="box-info-sum">{{ $info->sum}}</span></td>
                                        </tr>
                                    @endforeach
                                   
                                </tbody>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td>Total</td>
                                        <td>{{$totalCable}} M</td>
                                    </tr>
                            </table>
                        </div>
				    </div>
				</div>
		    </div>
		</div>
	</div>			    
</div>


@stop