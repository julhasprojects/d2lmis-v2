@extends( 'backend.index' )

@section( 'content_area' )

 <div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);">Dashboard</a></li>
        <li class="completed"><a href="javascript:void(0);"> Reports</a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory</a></li>
        <li  class="active"> <a href="javascript:void(0);"> New Box Create </a></li>
    </ul>
</div>
@include('backend.inventory.box-menu')
@include('master.message')
<div id="main-wrapper">
 
    <div class="col-md-12 left-box">
		<div class="panel panel-white">
			<h2 class="title-house"> Create New Box </h2>
			{{ csrf_field() }}
			<!-- {!! Form::open(array('url'=>'box-info','role'=>'form','data-toggle'=>'validator','method'=>'match'))!!} -->
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-4 control-label" for="boxNumber">
								Box Number (Base Name)
								<span class="required">*</span>
							</label>
							<div class="col-md-8">
								<input id="boxNumber" class="form-control" name="boxNumber"  type="text" required="">
								<span id="boxnumbercheck"> </span>
								<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-4 control-label" for="boxaddress">
								Box Address
							</label>
							<div class="col-md-8">
								<input id="boxaddress" class="form-control" name="boxaddress"  type="text" >
								<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-4 control-label" for="switchname">
								Switch Name
							</label>
							<div class="col-md-8">
								<input id="switchname" class="form-control" name="switchname" type="text" >
								<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6 pull-right">
						<div class="form-group">
							<label class="col-md-4 control-label" for="Port">
								Switch Port Number
								
							</label>
							<div class="col-md-8">
								 <input type="text" class="form-control" id="tokenfield-1"  name="port" placeholder="Type Port and hit coma"/>
							</div>
						</div>
					</div>
					
				</div>
				<div class="row">
					<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 pull-left">
						<div class="form-group">
							<label class="col-md-4 control-label" for="totalport">
								Total Port Of the switch
							</label>
							<div id="">
								<div class="col-md-8">
									<input id="totalport" class="form-control" name="totalport" type="text"  readonly>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									<div class="help-block with-errors"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 pull-right">
						<div class="form-group">
							<label class="col-md-4 control-label" for="fcnclientid">
								Free Port
							</label>
							<div class="col-md-8">
								<input id="freeport" class="form-control" name="freeport" type="text" readonly>
								<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 pull-left">
						<div class="form-group">
							<label class="col-md-4 control-label" for="mcname">
								Mc Name
								
							</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="mcname"  name="mcname" placeholder="Type mc mame and hit coma"  />
								<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 pull-right">
						<div class="form-group">
							<label class="col-md-4 control-label" for="mcnumber">
								Total No Of Mc Number
								
							</label>
							<div class="col-md-8">
								<input id="mcnumberCreate" class="form-control" name="mcnumber" type="text"  readonly>
								<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
					
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-4 control-label" for="sfpname">
								SFP Name
							</label>
							<div class="col-md-8">
								<input id="sfpname" class="form-control sfpname" name="sfpname" type="text"  placeholder="Type sfc name and hit coma">
								<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-4 control-label" for="sfpnumber">
								Total Num of SFP
							</label>
							<div class="col-md-8">
								<input id="sfpnumberCreate" class="form-control" name="sfpnumber" type="text" readonly>
								<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
				</div>

				
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-4 control-label" for="popname">
								Pop Name(Interface)
							</label>
							<div class="col-md-8">
								
								<select name="popname" id="popname" class="chosen-select-width">
									@foreach($popName as $popinfo)
										<option value="{{ $popinfo->interface_name }}"> {{ $popinfo->interface_name }} </option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-4 control-label" for="popaddress">
								Pop Address
							</label>
							<div class="col-md-8">
								<input id="popaddress" class="form-control" name="popaddress" type="text">
								<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-4 control-label" for="tgname">
								TG Name
								
							</label>
							<div class="col-md-8">
								<input id="tgname" class="form-control" name="tgname" type="text">
								<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-4 control-label" for="installdate">
								Install Date
								<span class="required">*</span>
							</label>
							<div class="col-md-8">
								<div class="input-group">
                                    <input class="form-control installdate" name="installdate" id="installdate" placeholder="install date" value="<?php echo date('Y-m-d');?>" required type="text">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
								
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-4 control-label" for="installby">
								Installed by
								<span class="required">*</span>
							</label>
							<div class="col-md-8">
								<select name="installby" id="installby" class="chosen-select-width frontpanel-searc-regionbased">
									<option value=""> Select Install By</option>
									@foreach($employee as $info)
										<option value="{{ $info->employeeID}}">{{ $info->fullName}}</option>
									@endforeach
								</select>
								<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-xs-12">
						<div class="form-group">
							<label class="col-md-4 control-label" for="installby">
								Fiber Root
							</label>
							<div class="col-md-8">
								<select class="chosen-select-width" id="fiberroot" name="fiberroot" >
	                                <option value="">Select Fiber Address</option>
	                                <option value="POP Address">POP Address</option>
	                                <option value="Server">Server</option>
	                            </select>
								<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-xs-12">
						<div class="form-group">
							<label class="col-md-4 control-label" for="installby">
								Is it pop?
								
							</label>
							<div class="col-md-8 checkbox-submit">
								<input type="checkbox" data-group-cls="btn-group-justified" name="pop" id="pop">
							</div>
						</div>
					</div>
					<div class="col-md-6 col-xs-12">
						<div class="form-group">
							<label class="col-md-4 control-label" for="branch">
								Branch
							</label>
							<div class="col-md-8 checkbox-submit">
								<select class="chosen-select-width" id="branch" name="branch" >
	                                <option value="">Select Branch</option>
	                                @foreach($branchInfo as $info)
	                                	<option value="{{ $info->id }}" @if($info->id == $bracnhID) selected @endif>{{ $info->branch_name }}</option>
	                                @endforeach
	                            </select>
							</div>
						</div>
					</div>
				</div>
				
					
				<div class="row">
					<div class="col-md-12 col-xs-12 button-submit">
						<button class="btn btn-box-buton btn-success" id="boxCreateSubmit">Submit</button>
					</div>
				</div>
			</div>
			<!-- {!!Form::close()!!} -->
	
		</div>
	</div>
    	    
</div>


@stop