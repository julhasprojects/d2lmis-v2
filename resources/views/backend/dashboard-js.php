<script type="text/javascript">
// Create the chart
Highcharts.chart('rr-district-wise', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Reporting Rate District Wise by Month by Year'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Reporting Rate'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}%'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },

    "series": [
        {
            "name": "Gazipur",
            "colorByPoint": true,
            "data": [
                {
                    "name": "Gazipur",
                    "y": 94,
                    "drilldown": "GazipurDistrict"
                },
                {
                    "name": "Faridpur",
                    "y": 95,
                    "drilldown": "Faridpur"
                },
                {
                    "name": "Khulna",
                    "y": 98,
                },
                {
                    "name": "Pabna",
                    "y": 92,
                },
                {
                    "name": "Kushtia",
                    "y": 97,
                },
                {
                    "name": "Coxsbazar",
                    "y": 90,
                },
                {
                    "name": "Jamalpur",
                    "y": 94,
                },

            ]
        }
    ],
    "drilldown": {
        "series": [
            {
                "name": "Gazipur",
                "id": "GazipurDistrict",
                "data": [
                    [
                        "Tongi",
                        100
                    ],
                    [
                        "Kaligang",
                        99
                    ],
                    [
                        "Kapasia",
                        96
                    ],
                    [
                        "Gazipur Sadar",
                        100
                    ],
                    [
                        "Kaliakoir",
                        99
                    ],
                    [
                        "Sreepur",
                        100
                    ]
                ]
            }
        ]
    }
});

// Stock Information
Highcharts.chart('stockinformation', {

    chart: {
        type: 'bubble',
        plotBorderWidth: 1,
        zoomType: 'xy'
    },

    title: {
        text: 'Stock Information'
    },

    xAxis: {
        gridLineWidth: 1
    },

    yAxis: {
        startOnTick: false,
        endOnTick: false
    },

    series: [{
        data: [
            [9, 81, 63],
            [98, 5, 89],
            [51, 50, 73],
            [41, 22, 14],
            [58, 24, 20],
            [78, 37, 34],
            [55, 56, 53],
            [18, 45, 70],
            [42, 44, 28],
            [3, 52, 59],
            [31, 18, 97],
            [79, 91, 63],
            [93, 23, 23],
            [44, 83, 22]
        ],
        marker: {
            fillColor: {
                radialGradient: { cx: 0.4, cy: 0.3, r: 0.7 },
                stops: [
                    [0, 'rgba(255,255,255,0.5)'],
                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.5).get('rgba')]
                ]
            }
        }
    }, {
        data: [
            [42, 38, 20],
            [6, 18, 1],
            [1, 93, 55],
            [57, 2, 90],
            [80, 76, 22],
            [11, 74, 96],
            [88, 56, 10],
            [30, 47, 49],
            [57, 62, 98],
            [4, 16, 16],
            [46, 10, 11],
            [22, 87, 89],
            [57, 91, 82],
            [45, 15, 98]
        ],
        marker: {
            fillColor: {
                radialGradient: { cx: 0.4, cy: 0.3, r: 0.7 },
                stops: [
                    [0, 'rgba(255,255,255,0.5)'],
                    [1, Highcharts.Color(Highcharts.getOptions().colors[1]).setOpacity(0.5).get('rgba')]
                ]
            }
        }
    }]

});

// Stock Status at diff levels

Highcharts.chart('stockstatusdiflevel', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Stock Status at Different Levels'
    },
    xAxis: {
        categories: ['ORS Sachet', 'Misoprostol ', 'Magnesium', 'Sulbutamol', 'Zinc','Amoxicillin']
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Stock Status'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'percent'
        }
    },
    series: [{
        name: 'DRS',
        data: [3, 1, 6, 3, 5]
    }, {
        name: 'DH',
        data: [2, 2, 3, 2, 1]
    }, {
        name: 'UHC',
        data: [3, 4, 4, 2, 5]
    },{
        name: 'USC',
        data: [5, 3, 4, 7, 2]
    },{
        name: 'CC',
        data: [5, 3, 4, 7, 2]
    }]
});

// Consumption Trend

Highcharts.chart('medicinesconsumptions', {

    title: {
        text: 'Monthly Medicine Consumptions'
    },

    yAxis: {
        title: {
            text: 'Number'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2015
        }
    },

    series: [{
        name: 'ORS',
        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
    }, {
        name: 'Misoprostol',
        data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
    }, {
        name: 'Magnesium',
        data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
    }, {
        name: 'Zinc',
        data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227]
    }, {
        name: 'Tab. Metronidazole',
        data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111]
    }],

    responsive: {
        rules: [{
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});

// district wise consumption



Highcharts.chart('districtwiseconsumptionrate', {
    chart: {
        type: 'areaspline'
    },
    title: {
        text: 'Average fruit consumption during one week'
    },
    legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'top',
        x: 150,
        y: 100,
        floating: true,
        borderWidth: 1,
        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
    },
    xAxis: {
        categories: [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday'
        ],
        plotBands: [{ // visualize the weekend
            from: 4.5,
            to: 6.5,
            color: 'rgba(68, 170, 213, .2)'
        }]
    },
    yAxis: {
        title: {
            text: 'Fruit units'
        }
    },
    tooltip: {
        shared: true,
        valueSuffix: ' units'
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        areaspline: {
            fillOpacity: 0.5
        }
    },
    series: [{
        name: 'John',
        data: [3, 4, 3, 5, 4, 10, 12]
    }, {
        name: 'Jane',
        data: [1, 3, 4, 3, 3, 5, 4]
    }]
});
</script>