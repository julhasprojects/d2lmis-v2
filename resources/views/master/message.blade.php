<?php 
    if (Session::get( 'message' )) {
        $message = Session::get( 'message' );
         if(!empty($message))  {
    ?>  

        <script type="text/javascript"> 
            $( document ).ready(function() { 
                swal("Thank You!", "{{ $message }}", "success");
            });
        </script>
    <?php   
        }
    }
?>

<?php 
    if (Session::pull( 'valid' )) {
        $message = Session::get( 'message' );
            if(!empty($message))  {
    ?>

        <script type="text/javascript"> 
            $( document ).ready(function() { 
                swal("Thank You!", "{{ $message }}", "success");
            });
        </script>
    <?php  
        } 
    } else if (Session::pull( 'invalid' )) {
        $message = Session::get( 'message' );
        if(!empty($message))  {
    ?>      
        <script type="text/javascript"> 
            $( document ).ready(function() { 
                swal("Sorry!", "{{ $message }}", "error");
            });
        </script>
<?php     
    }  
    }
?> 