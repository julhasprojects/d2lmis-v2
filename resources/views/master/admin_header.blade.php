<!DOCTYPE html>
<html>
<head>

    <title>D2LMIS-CONTROL PANEL</title>
    
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">   
    <link href="{{ URL::asset( 'assets/plugins/pace-master/themes/blue/pace-theme-flash.css' ) }}" rel="stylesheet"/>
    <!-- <link href="{{ URL::asset( 'assets/plugins/uniform/css/uniform.default.min.css' ) }}" rel="stylesheet"/> -->
    
    <link href="{{ URL::asset( 'hrm/bootstrap-fileinput.css' ) }}" rel="stylesheet" />
    <link href="{{ URL::asset( 'assets/plugins/bootstrap/css/bootstrap.min.css' ) }}" rel="stylesheet" />
    <link href="{{ URL::asset( 'assets/plugins/fontawesome/css/font-awesome.css' ) }}" rel="stylesheet" />
    <link href="{{ URL::asset( 'assets/plugins/line-icons/simple-line-icons.css' ) }}" rel="stylesheet" />  
    <link href="{{ URL::asset( 'assets/plugins/offcanvasmenueffects/css/menu_cornerbox.css' ) }}" rel="stylesheet" />   
    <link href="{{ URL::asset( 'assets/plugins/waves/waves.min.css' ) }}" rel="stylesheet" />   
    <link href="{{ URL::asset( 'assets/plugins/switchery/switchery.min.css' ) }}" rel="stylesheet" />
    <link href="{{ URL::asset( 'assets/plugins/3d-bold-navigation/css/style.css' ) }}" rel="stylesheet" />
    <link href="{{ URL::asset( 'assets/plugins/slidepushmenus/css/component.css' ) }}" rel="stylesheet" />  
    <link href="{{ URL::asset( 'assets/plugins/weather-icons-master/css/weather-icons.min.css' ) }}" rel="stylesheet" />    
    <link href="{{ URL::asset( 'assets/plugins/metrojs/MetroJs.min.css' ) }}" rel="stylesheet" />   
    <link href="{{ URL::asset( 'assets/plugins/toastr/toastr.min.css' ) }}" rel="stylesheet" /> 
    <link href="{{ URL::asset( 'assets/css/fullcalendar.min.css' ) }}" rel="stylesheet" /> 
    <link href="{{ URL::asset( 'assets/css/navbar.css' ) }}" rel="stylesheet" /> 
    <link href="{{ URL::asset( 'assets/css/hrm.css' ) }}" rel="stylesheet" /> 
    <script src="{{ URL::asset( 'assets/js/moment.min.js' ) }}"></script>
    <!-- Datepicker --> 
    <script src="{{ URL::asset( 'assets/plugins/jquery/jquery-2.1.3.min.js' ) }}"></script>
    <link href="{{ URL::asset( 'assets/css/datepicker.css' ) }}" rel="stylesheet" />
    <!-- Modal/Popup --> 
      
    <script src="{{ URL::asset( 'assets/js/fullcalendar.min.js' ) }}"></script>
    <link href="{{ URL::asset( 'assets/css/component.css' ) }}" rel="stylesheet" />
    <!-- Theme Styles -->
    <link href="{{ URL::asset( 'assets/css/modern.css' ) }}" rel="stylesheet" />
    <link href="{{ URL::asset( 'assets/css/themes/white.css' ) }}" class="theme-color" rel="stylesheet" />
    <link href="{{ URL::asset( 'assets/css/custom.css' ) }}" rel="stylesheet" />
    <link href="{{ URL::asset( 'assets/css/responsive.css' ) }}" rel="stylesheet" />
    <link href="{{ URL::asset( 'assets/css/print.css' ) }}" rel="stylesheet" />
    <link href="{{ URL::asset( 'hrm/skin.css' ) }}" rel="stylesheet" />
    
  
    <script src="{{ URL::asset( 'assets/plugins/3d-bold-navigation/js/modernizr.js' ) }}"></script>
    <script src="{{ URL::asset( 'assets/plugins/offcanvasmenueffects/js/snap.svg-min.js' ) }}"></script>  
    <!--Send email notifi-->
    <!-- <link href="{{ URL::asset( 'assets/plugins/summernote-master/summernote.css' ) }}" rel="stylesheet" type="text/css"/> -->
    <!-- Sweet Alert Library for Validation -->
    <link href="{{ URL::asset( 'assets/css/sweetalert/sweetalert.css' ) }}" rel="stylesheet" />
    <!-- Country List-->
    <script src="{{ URL::asset( 'assets/js/countries.js' ) }}"></script>
    
    
    
    <!-- data table min.css -->
    <link rel="stylesheet" href="{{ URL::asset( 'assets/plugins/datatables/css/jquery.dataTables.min.css' ) }}">
    <link rel="stylesheet" href="{{ URL::asset( 'assets/plugins/datatables/css/buttons.dataTables.min.css')}}">
    <!-- data table Jquery -->
    <script src="{{ URL::asset( 'assets/plugins/datatables/js/jquery.datatables.min.js' ) }}"></script>
    <script src="{{ URL::asset( 'assets/plugins/datatables/js/dataTables.buttons.min.js' ) }}"></script>
    <script src="{{ URL::asset( 'assets/plugins/datatables/js/jszip.min.js' ) }}"></script>
    <script src="{{ URL::asset( 'assets/plugins/datatables/js/pdfmake.min.js' ) }}"></script>
    <script src="{{ URL::asset( 'assets/plugins/datatables/js/vfs_fonts.js' ) }}"></script>
    <script src="{{ URL::asset( 'assets/plugins/datatables/js/buttons.html5.min.js' ) }}"></script>
    <script src="{{ URL::asset( 'assets/plugins/datatables/js/buttons.colVis.min.js' ) }}"></script>
    <script src="{{ URL::asset( 'assets/js/custom.js' ) }}"></script>
    <script src="{{ URL::asset( 'assets/plugins/datatables/js/buttons.print.min.js' ) }}"></script>

    <script src="{{ URL::asset( 'assets/plugins/uniform/jquery.uniform.min.js' ) }}"></script>
    <script src="{{ URL::asset( 'assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js' ) }}"></script>
    

    <!-- High Chart Library -->
    <script src="{{ URL::asset( 'assets/plugins/highcharts/highcharts.js' ) }}"></script>
    <script src="{{ URL::asset( 'assets/plugins/highcharts/modules/data.js' ) }}"></script>
    <script src="{{ URL::asset( 'assets/plugins/highcharts/modules/exporting.js' ) }}"></script>

    <script src="{{ URL::asset( 'assets/plugins/highcharts/modules/drilldown.js' ) }}"></script>
    <script src="{{ URL::asset( 'assets/js/jquery.modern-ticker.min.js' ) }}"></script>
    <script src="{{ URL::asset( 'assets/js/jQuery.print.min.js' ) }}"></script>
    
    <script src="{{ URL::asset( 'assets/js/timedatepicker.js' ) }}"></script>
    <script src="{{ URL::asset( 'assets/js/bootstrapdatetime.js' ) }}"></script>
    
    <script src="{{ URL::asset( 'assets/js/barcode.js' ) }}"></script>    
    <!--Choosen Library-->
    <link href="{{ URL::asset( 'assets/css/chosen.css' ) }}" rel="stylesheet" />
     <!-- Print Chart Library -->
    <script src="{{ URL::asset( 'assets/js/jquery-printme.js' ) }}"></script>
    <!-- <script src="{{ URL::asset( 'assets/js/require.js' ) }}"></script> -->
    <script src="{{ URL::asset( 'assets/plugins/datatables/js/ColReorderWithResize.js' ) }}"></script>
    
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script type="text/javascript">
    const basePath = "http://localhost/demo.d2lmis.org/public/";    
    </script>
</head>

<body class="page-header-fixed front-backgroud small-sidebar">
 
