<!-- data table min.css -->
<link rel="stylesheet" href="{{ URL::asset( 'assets/plugins/datatables/css/jquery.dataTables.min.css' ) }}">
<link rel="stylesheet" href="{{ URL::asset( 'assets/plugins/datatables/css/buttons.dataTables.min.css')}}">
<!-- data table Jquery -->
<script src="{{ URL::asset( 'assets/plugins/jquery/jquery-2.1.3.min.js' ) }}" type="text/javascript"></script>
<script src="{{ URL::asset( 'assets/plugins/datatables/js/jquery.datatables.min.js' ) }}"></script>
<script src="{{ URL::asset( 'assets/plugins/datatables/js/dataTables.buttons.min.js' ) }}"></script>
<script src="{{ URL::asset( 'assets/plugins/datatables/js/jszip.min.js' ) }}"></script>
<script src="{{ URL::asset( 'assets/plugins/datatables/js/pdfmake.min.js' ) }}"></script>
<script src="{{ URL::asset( 'assets/plugins/datatables/js/vfs_fonts.js' ) }}"></script>
<script src="{{ URL::asset( 'assets/plugins/datatables/js/buttons.html5.min.js' ) }}"></script>
<script src="{{ URL::asset( 'assets/plugins/datatables/js/buttons.colVis.min.js' ) }}"></script>
<script src="{{ URL::asset( 'assets/js/custom.js' ) }}"></script>
<script src="{{ URL::asset( 'assets/plugins/datatables/js/buttons.print.min.js' ) }}"></script>
