@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Good Recive  </a></li>
    </ul>
</div>

@include('modules.inventory.master.navigation')
@include('modules.inventory.master.error')

<div id="main-wrapper">
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
				<h2 class="title-house"> Goods Receive</h2>
				 {!! Form::open(array('url'=>'supplier/'.$supplierInfo->id, 'class' =>'form col-md-12','method'=>'PUT'))!!}
		           <div class="col-md-12">
		                <label class="control-label col-md-2">Supplier Name:</label>
		                <div class="col-md-8">
		                    {!! Form::text('supplier_name', $supplierInfo->supplier_name,['class'=>'form-control','placeholder'=>'Supplier Name']) !!}
		                    <br>
		                </div>
		            </div>

		            <div class="col-md-12">
		                <label class="control-label col-md-2"> Company Details:</label>
		                <div class="col-md-8">
		                    {!! Form::text('company_details', $supplierInfo->company_details,['class'=>'form-control','placeholder'=>'Company Details']) !!}
		                    <br>
		                </div>
		            </div>

		            <div class="col-md-12">
		                <label class="control-label col-md-2"> Address:</label>
		                <div class="col-md-8">
		                    {!! Form::text('address',  $supplierInfo->address,['class'=>'form-control','placeholder'=>'Address Name']) !!}
		                    <br>
		                </div>
		            </div>

		            <div class="col-md-12">
		                <label class="control-label col-md-2"> Mobile:</label>
		                <div class="col-md-8">
		                    {!! Form::text('mobile',  $supplierInfo->mobile,['class'=>'form-control','placeholder'=>'Mobile', 'required']) !!}
		                    <br>
		                </div>
		            </div>

		            <div class="col-md-12">
		                <label class="control-label col-md-2"> Email:</label>
		                <div class="col-md-8">
		                    {!! Form::text('email',  $supplierInfo->email,['class'=>'form-control','placeholder'=>'Email']) !!}
		                    <br>
		                </div>
		            </div>


		            <div class="col-md-12 text-center">
		                {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
		                <!-- <button class="btn btn-danger" type="reset">Reset</button> -->
		            </div>

		            {{ Form::close() }}
			</div>
		</div>
	</div>
	
</div>
@stop