@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Good Recive  </a></li>
    </ul>
</div>

@include('modules.inventory.master.navigation')
@include('master.message')
@include('modules.inventory.master.error')

<div id="main-wrapper">
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
				<h2 class="title-house"> Supplier Create</h2>
				 {{Form::open(array('url'=>"supplier",'class'=>'form-horizontal form-bordered','method'=>'match'))}}

		           <div class="col-md-12">
		                <label class="control-label col-md-2">Supplier Name:</label>
		                <div class="col-md-8">
		                    {!! Form::text('supplier_name', null,['class'=>'form-control','placeholder'=>'Supplier Name']) !!}
		                    <br>
		                </div>
		            </div>

		            <div class="col-md-12">
		                <label class="control-label col-md-2"> Company Details:</label>
		                <div class="col-md-8">
		                    {!! Form::text('company_details', null,['class'=>'form-control','placeholder'=>'Company Details']) !!}
		                    <br>
		                </div>
		            </div>

		            <div class="col-md-12">
		                <label class="control-label col-md-2"> Address:</label>
		                <div class="col-md-8">
		                    {!! Form::text('address', null,['class'=>'form-control','placeholder'=>'Address Name']) !!}
		                    <br>
		                </div>
		            </div>

		            <div class="col-md-12">
		                <label class="control-label col-md-2"> Mobile:</label>
		                <div class="col-md-8">
		                    {!! Form::text('mobile', null,['class'=>'form-control','placeholder'=>'Mobile']) !!}
		                    <br>
		                </div>
		            </div>

		            <div class="col-md-12">
		                <label class="control-label col-md-2"> Email:</label>
		                <div class="col-md-8">
		                    {!! Form::text('email', null,['class'=>'form-control','placeholder'=>'Email']) !!}
		                    <br>
		                </div>
		            </div>


		            <div class="col-md-12 text-center">
		                {!! Form::submit('Submit', ['class'=>'btn btn-primary']) !!}
		                <button class="btn btn-danger" type="reset">Reset</button>
		            </div>

		            {{ Form::close() }}
			</div>
		</div>
	</div>
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
			 	<table class="table table-hover table-bordered display" id="tableresponsive" style="width: 100%; cellspacing: 0;">
				    <thead class="table-header-bg">
				        <tr>
				            <th>SN</th>
				            <th>Supplier ID</th>
				            <th>Supplier Name</th>
				            <th>Company Details</th>
				            <th>Address</th>
				            <th>Mobile</th>
				            <th>Email</th>
				            <th>Action</th>
				        </tr>
				    </thead>
				    <tbody>
				    	<?php $sn =1 ?>
				    	@foreach($supplierInfo as $info)
				    	<tr>
					    	<td> {{ $sn++ }}</td>
					    	<td> {{ $info->supplier_id}}</td>
					    	<td> {{ $info->supplier_name}}</td>
					    	<td>{{ $info->company_details}}</td>
					    	<td>{{ $info->address}}</td>
					    	<td>{{ $info->mobile}}</td>
					    	<td>{{ $info->email}}</td>
					    	<td> <a href="{{ URL::to('/supplier/'.$info->id.'/edit') }}" class="glyphicon glyphicon-pencil icon-edit" >  |</a> 
		                        {!! Form::open(['url' => '/supplier/'.$info->id, 'files'=>true,'method' => 'delete','class' => 'from-delete']) !!}
		                            <button type="submit" class="delete-options"><i class="fa fa-trash-o"></i></button>
		                        {!! Form::close() !!}  
		                       </td>
						</tr>
				    	@endforeach
				    </tbody>
			    </table>
			</div>
	    </div>
	</div>
	<div class="input-group">
       <button class="btn btn-success" id="dateSearch">Submit</button>
    </div>
</div>
@stop