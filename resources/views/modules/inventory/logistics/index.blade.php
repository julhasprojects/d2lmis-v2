@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Good Recive  </a></li>
    </ul>
</div>
@include('modules.inventory.master.navigation')
@include('modules.inventory.master.error')
<div id="main-wrapper">
    <div class="col-md-12 left-box">
        <div class="panel panel-white">
            <div class="panel panel-body">
                <h2 class="title-house"> Logistics Setting</h2>
                {!! Form::open(['url'=>'logistics/setting', 'class' =>'form col-md-12', 'enctype'=>'multipart/form-data']) !!}
                <div class="col-md-12">
                    <label class="control-label col-md-2"> Type :</label>
                    <div class="col-md-8">
                        <select class="form-control" name="type" required>
                            <option value="">--SELECT ONE--</option>
                           
                            <option value="logistics">Logistics</option>
                            <option value="goods">Goods</option>
                        </select>
                        <br>
                    </div>
                </div>

                <div class="col-md-12">
                    <label class="control-label col-md-2"> Name :</label>
                    <div class="col-md-8">
                        {!! Form::text('name', null,['class'=>'form-control','placeholder'=>'Name', 'required']) !!}
                        <br>
                    </div>
                </div>

                <div class="col-md-12">
                    <label class="control-label col-md-2"> Strength :</label>
                    <div class="col-md-8">
                        {!! Form::text('strength', null,['class'=>'form-control','placeholder'=>'Strength']) !!}
                        <br>
                    </div>
                </div>

                <div class="col-md-12">
                    <label class="control-label col-md-2"> Generic Name :</label>
                    <div class="col-md-8">
                        {!! Form::text('generic_name', null,['class'=>'form-control','placeholder'=>'Generic Name']) !!}
                        <br>
                    </div>
                </div>

                <div class="col-md-12">
                    <label class="control-label col-md-2"> Manufacturer :</label>
                    <div class="col-md-8">
                        {!! Form::text('manufacturer', null,['class'=>'form-control','placeholder'=>'Manufacturer', 'required']) !!}
                        <br>
                    </div>
                </div>

                <div class="col-md-12">
                    <label class="control-label col-md-2"> Unit :</label>
                    <div class="col-md-8">
                        {!! Form::text('description', null,['class'=>'form-control','placeholder'=>'Description']) !!}
                        <br>
                    </div>
                </div>


                <div class="col-md-12 text-center">
                    {!! Form::submit('Submit', ['class'=>'btn btn-primary']) !!}
                    <button class="btn btn-danger" type="reset">Reset</button>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-hover table-bordered" id="tableresponsive" style="width: 100%; cellspacing: 0;">
            <thead class="table-header-bg">
                <tr>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Generic Name</th>
                    <th>Manufacturer</th>
                    <th>Unit</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($logisticsDetails as $details)
                <tr>
                    <td>{!! $details->type or '---' !!}</td>
                    <td>{!! $details->name or '--' !!} [{!! $details->strength or '' !!}]</td>
                    <td>{!! $details->generic_name or '--' !!}</td>
                    <td>{!! $details->manufacturer or '--' !!}</td>
                    <td>{!! $details->description or '--' !!}</td>
                    <td>
                        <a href="{{ URL::to('/logistics/setting/'.$details->id.'/edit') }}" class="glyphicon glyphicon-pencil icon-edit" >  |</a> 
                        {!! Form::open(['url' => '/logistics/setting/'.$details->id, 'files'=>true,'method' => 'delete','class' => 'from-delete']) !!}
                            <button type="submit" class="delete-options"><i class="fa fa-trash-o"></i></button>
                        {!! Form::close() !!}  
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop