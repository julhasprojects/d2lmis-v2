@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Good Recive  </a></li>
    </ul>
</div>
@include('modules.inventory.master.navigation')
@include('modules.inventory.master.error')
<div id="main-wrapper">
    <div class="col-md-12 left-box">
        <div class="panel panel-white">
            <div class="panel panel-body">
                <h2 class="title-house"> Logistics Setting</h2>
                {!! Form::open(array('url'=>'logistics/setting/'.$logisticsDetails->id, 'class' =>'form col-md-12','method'=>'PUT'))!!}
                <div class="col-md-12">
                    <label class="control-label col-md-2"> Type :</label>
                    <div class="col-md-8">
                        <select class="form-control" name="type" required>
                            <option value="">--SELECT ONE--</option>
                            <option value="logistics" @if($logisticsDetails->type=='logistics') selected @endif>Logistics</option>
                            <option value="goods" @if($logisticsDetails->type=='goods') selected @endif>Goods</option>
                        </select>
                        <br>
                    </div>
                </div>

                <div class="col-md-12">
                    <label class="control-label col-md-2"> Name :</label>
                    <div class="col-md-8">
                        {!! Form::text('name', $logisticsDetails->name,['class'=>'form-control','placeholder'=>'Name',  'required', 'value' =>$logisticsDetails->id ]) !!}
                        <br>
                    </div>
                </div>

                <div class="col-md-12">
                    <label class="control-label col-md-2"> Strength :</label>
                    <div class="col-md-8">
                        {!! Form::text('strength',$logisticsDetails->strength,['class'=>'form-control','placeholder'=>'Strength']) !!}
                        <br>
                    </div>
                </div>

                <div class="col-md-12">
                    <label class="control-label col-md-2"> Generic Name :</label>
                    <div class="col-md-8">
                        {!! Form::text('generic_name', $logisticsDetails->generic_name,['class'=>'form-control','placeholder'=>'Generic Name']) !!}
                        <br>
                    </div>
                </div>

                <div class="col-md-12">
                    <label class="control-label col-md-2"> Manufacturer :</label>
                    <div class="col-md-8">
                        {!! Form::text('manufacturer', $logisticsDetails->manufacturer,['class'=>'form-control','placeholder'=>'Manufacturer', 'required']) !!}
                        <br>
                    </div>
                </div>

                <div class="col-md-12">
                    <label class="control-label col-md-2"> Unit :</label>
                    <div class="col-md-8">
                        {!! Form::text('description', $logisticsDetails->description,['class'=>'form-control','placeholder'=>'Description']) !!}
                        <br>
                    </div>
                </div>


                <div class="col-md-12 text-center">
                    {!! Form::submit('Submit', ['class'=>'btn btn-primary']) !!}
                    <button class="btn btn-danger" type="reset">Reset</button>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
    
</div>
@stop