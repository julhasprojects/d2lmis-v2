@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Products Receive  </a></li>
    </ul>
</div>
@include('modules.inventory.master.navigation')
@include('modules.inventory.master.error')
@include('master.message')
<div id="main-wrapper">

	<div class="col0-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
				<h2 class="title-house"> Products Receive</h2>
				{!! Form::open(['url'=>'receive', 'class' =>'form col-md-12', 'enctype'=>'multipart/form-data']) !!}
				{!! csrf_field() !!}
				<div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
					<label class="col-sm-3 control-label package-label" for="grandtotal">
						Transaction No
					</label>
	            	<div class="col-sm-9">
		            	<input type="text" name="transaction_id" id="transaction_id" class="form-control" value="<?php echo date('YmdHis');?>" readonly="">
		            </div>
          		</div>
          		<div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
					<label class="col-sm-3 control-label package-label" for="grandtotal">
						Product Receive ID:
					</label>
	            	<div class="col-sm-9">
		            	<input type="text" name="invoice_no" readonly id="invoice_no" class="form-control" value="{{ $productID }}">
		            </div>
          		</div>
	 	 		             
	            <div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
					<label class="col-sm-3 control-label package-label" for="grandtotal">
						Invoice No
					</label>
	            	<div class="col-sm-9">
		            	<input type="text" name="invoice_no" id="invoice_no" class="form-control">
		            </div>
          		</div>
          		
          		<div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
					<label class="col-sm-3 control-label package-label" for="grandtotal">
						Challan No
					</label>
	            	<div class="col-sm-9">
		            	<input type="text" name="challan_no" id="challan_no" class="form-control">
		            </div>
          		</div>
          		<div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
					<label class="col-sm-3 control-label package-label" for="grandtotal">
						Challan Date
					</label>
	            	<div class="col-sm-9">
		            	<div class="input-group date" data-date-format="yyyy-mm-dd">
		                    <input type="text" name="challan_date" id="challan_date" class="form-control"  placeholder="yyyy-mm-dd">
		                    <span class="input-group-btn"><button class="btn default" type="button"><i class="glyphicon glyphicon-th"></i></button></span>
		                </div>
		            </div>
          		</div>
          		<div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
					<label class="col-sm-3 control-label package-label" for="grandtotal">
						Supplier Name
					</label>
	            	<div class="col-sm-9">
		            	<select class="form-control" name="supplier_id" id="supplier_id" >
			                <option value="">Select Supplier</option>
			                @foreach($supplierInfo as $spInfo)
								<option value="{{ $spInfo->id }}">{{ $spInfo->supplier_name }}</option>
			                @endforeach
			            </select> 
		            </div>
          		</div>
          		
          		<div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
					<label class="col-sm-3 control-label package-label" for="grandtotal">
						Address
					</label>
	            	<div class="col-sm-9">
		            	<input type="textarea" name="supplier_address" id="supplier_address" class="form-control" readonly="">
		            </div>
          		</div>
          		<div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
					<label class="col-sm-3 control-label package-label" for="grandtotal">
						Received By
					</label>
	            	<div class="col-sm-9">
		            	<select class="form-control" name="received_by" >
            				<option value="">Received By</option>
                            @foreach($employeeInfo as $infobox)
			                	<option value="{{ $infobox->employeeID }}"> {{ $infobox->fullName }} </option>
			                @endforeach
                        </select>
		            </div>
          		</div>
          		<div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
					<label class="col-sm-3 control-label package-label" for="grandtotal">Received Date</label>
               		<div class="col-sm-9">
                        <div class="input-group date" data-date-format="yyyy-mm-dd">
                            
                            <input type="text" name="received_date" id="received_date" class="form-control"  placeholder="yyyy-mm-dd">
                            <span class="input-group-btn">
                            	<button class="btn default" type="button">
                            		<i class="glyphicon glyphicon-th"></i>
                            	</button>
                            </span>
                        </div>
                    </div>
                </div> 
          		<div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
					<label class="col-sm-3 control-label package-label" for="grandtotal">
						Remarks
					</label>
	            	<div class="col-sm-9">
		            	<input type="text" name="receive_remarks" id="receive_remarks" class="form-control">
		            </div>
          		</div> 

				<table id="table_gr" class="table table-bordered table-hover display dataTable table-header-bg">
	                <thead>
	                    <tr>
	                        <th>Category</th>
	                        <th>Product Name</th>
	                        <th>Opening</th>
	                        <th>Rec. Quantity</th>
	                        <th>Unit Price</th>
	                        <th>Total Price</th>
	                        <th>Lot No</th>
	                        <th>Expiry Date</th>
	                        <th>Mgf. Date</th>
	                        <th>Remarks</th>
	                        <th>Add Row </th>
	                    </tr>
	                </thead>
	                <tbody class="">
	                    <tr class="entry" id="property">
	                        <td class="col-md-1">
	                            <select class="form-control typeId" name="category_id[]" id="category_id">
	                               <option value="">Select Category</option>
					                @foreach($categoryList as $category)
										<option value="{{ $category->category_id }}">{{ $category->category_name }}</option>
					                @endforeach
	                            </select>
	                        </td>
	                        <td class="col-md-2">
	                            <select class="form-control nameId " name="product_id[]" id="product_id">
	                                <option value="">Select Product</option>
					                @foreach($productList as $product)
										<option value="{{ $product->product_id }}">{{ $product->product_name }}</option>
					                @endforeach
	                            </select>
	                        </td>
	                        <td>
	                        	{!! Form::number('opening[]', null,['class'=>'form-control','id'=>'opening','placeholder'=>'Opening']) !!}
	                        </td>

	                        <td>
	                        	{!! Form::number('quantity[]', null,['class'=>'form-control', 'id'=>'quantity','placeholder'=>'Received Quantity']) !!}
	                        </td>	
	                        <td>
	                        	{!! Form::number('unit_price[]', null,['class'=>'form-control','id'=>'unit_price','placeholder'=>'Unit Price']) !!}
	                        </td>
	                     
	                        <td>
	                        	{!! Form::number('total_price[]', null,['class'=>'form-control','id'=>'total_price','step'=>'0.02','readonly','placeholder'=>'Total Price']) !!}
	                        </td> 
	                        <td>{!! Form::text('comments[]', null,['class'=>'form-control','placeholder'=>'Lot No']) !!}</td>
	                        <td>{!! Form::text('comments[]', null,['class'=>'form-control','placeholder'=>'Expiry Date']) !!}</td>
	                                               
	                        <td>
	                            <div id="sandbox" class="sandbox span7 col-md-12">
	                                <div class="input-group date" data-date-format="yyyy-mm-dd">
	                                    <input type="text" name="warranty_period[]" id="warranty_period" class="form-control" placeholder="yyyy-mm-dd">
	                                    <span class="input-group-btn"><button class="btn default" type="button"><i class="glyphicon glyphicon-th"></i></button></span>
	                                </div>
	                            </div>
	                        </td>
	                        <td>{!! Form::text('comments[]', null,['class'=>'form-control','placeholder'=>'Comments']) !!}</td>

	                        <td class="col-md-1">
	                            <span class="input-group-btn">
	                                <button class="btn btn-primary btn-add" type="button">
	                                    Add <span class="glyphicon glyphicon-plus"></span>
	                                </button>
	                            </span>
	                        </td>
	                    </tr>
	                    <tr id="newPara"></tr>
	                </tbody>
	            </table>

          		<div class="col-md-12 text-center">
	                <br><br>
	                <button class="btn btn-primary" type="submit">Submit</button>
	            </div>
            	{!! Form::close() !!}	
			</div>
		</div>
	</div>
</div>

@include('modules.inventory.master.recive-script')

@stop