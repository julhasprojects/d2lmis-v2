@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Disapproved Approval  </a></li>
    </ul>
</div>
@include('modules.inventory.master.navigation')
@include('modules.inventory.master.error')
<div id="main-wrapper">
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
				<h2 class="title-house"> All Dis Approved Products</h2>
			 	{{ csrf_field() }}
                <div class="table-responsive">
                    <table class="table table-hover table-bordered display" id="tableresponsive" style="width: 100%; cellspacing: 0;">
    	    			<thead class="table-header-bg">
                            <tr>
                                <th>S/N</th>
                                <th>Product Name</th>
                                <th>Recived By</th>
                                <th>Unit Price(Tk)</th>
                                <th>Received Qty</th>
                                <th>Total Amount</th>
                                <th>Warranty Period</th>
                                <th>Comments</th>
                                <th>Transaction Date</th>	                         
                                <th>Approved Date</th>
                                <th>Disapproved By</th>
                                <th>Status</th>
                                <th>Action</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                        <?php $sn=1; ?>
                        @foreach($transactions as $transaction)
                            <tr class="odd gradeX" id="product_{{  $transaction->transactionID }}">
                                <td>{{ $sn++ }} </td>
                                <td>{{ $transaction->product_name }} </td>
                                <td>{{ $transaction->fullName }} </td>
                                <td>{{ $transaction->unit_price }}</td>
                                <td>{{ $transaction->quantity  }}</td>
                                <td>{{ $transaction->total_price }}</td>
                                <td>{{ $transaction->warranty_period }}</td>
                                <td>{{ $transaction->comments }}</td>
                                <td>{{ $transaction->challan_date}}</td>

                                <td>
                                    @if($transaction->approval_status  == 1)
                                        {{ $transaction->updated_at }}
                                    @else
                                        {{ $transaction->updated_at }}
                                    @endif
                                </td>
                                <td>
                                    @foreach($allEmployer as $info)
                                        @if( $info->employeeID == $transaction->approved_by)
                                            {{ $info->fullName }}
                                        @endif
                                    @endforeach
                                </td>
                                
                                 
                                <td>
                                    <span class="btn btn-danger btn-xs">Disapproved</span>
                                </td> 
                                <td>
                                    <span class="btn btn-success btn-xs" onclick="editReantry('{{ $transaction->product_id }}','{{ $transaction->quantity  }}','{{ $transaction->product_name  }}','0','{{ $transaction->transactionID  }}')">Reantary</span>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
			</div>
		</div>
	</div>
</div>
@include('modules.inventory.master.master-edit')
@include('modules.inventory.master.approved')
@stop