{{ csrf_field() }}
    <table class="table table-hover table-bordered display" id="tableresponsive" style="width: 100%; cellspacing: 0;">
		<thead class="table-header-bg">
            <tr>
                <th>S/N</th>
                <th>Product Name</th>
                <th>Requisition By</th>
                <th>Unit Price(Tk)</th>
                <th>Received Qty</th>
                <th>Total Amount</th>
                <th> Warranty Period</th>
                <th> Comments</th>
                <th> Transaction Date</th>	                         
                <th>Approved Date</th>
                <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php $sn=1; ?>
        @foreach($transactions as $transaction)
            <tr class="odd gradeX" id="product_{{  $transaction->transactionID }}">
                <td>{{ $sn++ }} </td>
                <td>{{ $transaction->product_name }}</td>
                <td>{{ $transaction->fullName }}</td>
                <td>{{ $transaction->unit_price}}</td>
                <td>{{ $transaction->quantity  }}</td>
                <td>{{ $transaction->total_price }}</td>
                <td>{{ $transaction->warranty_period }}</td>
                <td>{{ $transaction->comments }}</td>
                <td>{{ $transaction->received_date }}</td>

                <td>
                    @if($transaction->approval_status  == 1)
                        {{ $transaction->approval_date }}
                    @else
                        
                    @endif
                </td>
                    @if($transaction->approval_status == 0)
                        <td>
                            <span class="btn btn-warning">Not Approve Yet</span>
                        </td>    

                    <td>
                        <button class="btn btn-success btn-xs status {{ $transaction->id }}approved" href="javascript:;" onclick="approvedProduct('{{ $transaction->product_id }}','{{ $transaction->quantity  }}','{{ $transaction->product_name  }}','1','{{ $transaction->transactionID  }}')">
                            <i class="glyphicon glyphicon-check"></i>
                            Approve Now
                        </button>
                    </td>
                    <td>    
                        <a class="btn btn-info btn-xs status {{ $transaction->id }}approved" >
                            Edit<i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>    
                        <button class="btn btn-danger btn-xs status {{ $transaction->id }}approved" href="javascript:;" onclick="approvedProduct('{{ $transaction->product_id }}','{{ $transaction->quantity  }}','{{ $transaction->product_name  }}','0','{{ $transaction->transactionID  }}')">
                            <i class="glyphicon glyphicon-check"></i>
                            Disapprove
                        </button>
                    </td>    
                    @elseif($transaction->approval_status == 1)
                        <td>
                            <span class="btn btn-success">Approved</span>
                        </td>
                    @elseif($transaction->approval_status == 2)
                        <td>
                            <span class="btn btn-danger">Disapproved</span>
                        </td>                                         
               
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>