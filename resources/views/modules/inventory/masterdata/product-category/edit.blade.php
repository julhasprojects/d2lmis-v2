@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Products Upadate Category  </a></li>
    </ul>
</div>

@include('modules.inventory.master.navigation')
@include('modules.inventory.master.error')
@include('master.message')
<div id="deletenotification"></div>
<div id="main-wrapper">
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
				<h2 class="title-house"> Products Category Updated </h2>
				 {{Form::open(array('url'=>'product/category/'.$productCategory->category_id,'class'=>'form-horizontal form-bordered','method'=>'PUT'))}}

		            <div class="col-md-12">
		                <label class="control-label col-md-2"> Category Name:</label>
		                <div class="col-md-8">
		                    {!! Form::text('category_name', $productCategory->category_name,['class'=>'form-control','placeholder'=>'Category Name']) !!}
		                    <br>
		                </div>
		            </div>
					
					<div class="col-md-12">
		                <label class="control-label col-md-2"> Category Notes:</label>
		                <div class="col-md-8">
		                    {!! Form::textarea('category_notes', $productCategory->category_notes,['class'=>'form-control','placeholder'=>'Category Notes','col'=>'2','row'=>'2']) !!}
		                    <br>
		                </div>
		            </div>

		            <div class="col-md-12 text-center">
		                {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
		                <button class="btn btn-danger" type="reset">Reset</button>
		            </div>

		            {{ Form::close() }}
			</div>
		</div>
	</div>
	
</div>
@include('modules.inventory.master.delete')
@stop