@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Product List </a></li>
    </ul>
</div>

@include('modules.inventory.master.navigation')
@include('modules.inventory.master.error')

<div id="deletenotification"></div>
<div id="main-wrapper">
	
	<div class="col-md-12 col-xs-12 col-lg-12  left-box"> 
		<div class="panel-white">
			<div class="panel-body">
				{{ csrf_field() }}
				<h2 class="title-house"> Product    List </h2>
			 	<table class="table table-hover table-bordered display" id="tableresponsive" style="width: 100%; cellspacing: 0;">
				    <thead class="table-header-bg">
				        <tr>
				            <th>SN</th>
				            <th>Product ID</th>
				            <th>Category Name</th>
				            <th>Product Unit</th>
				            <th>Manufacturer</th>
				            <th>Product Name</th>
				            <th>Product Detials</th>
				            <th>Product Notes</th>
				            
				            <th>Action</th>
				        </tr>
				    </thead>
				    <tbody>
				    	<?php $sn =1 ?>
				    	@foreach($productSetup as $info)
					    	<tr id="product_Setup_{{ $info->product_id }}">
					    		<td> {{ $sn++ }} </td>
					    		<td> {{ $info->product_id }} </td>
					    		<td> {{ $info->category_name }} </td>
					    		<td> {{ $info->unit_name }} </td>
					    		<td> {{ $info->manufact_name }} </td>
					    		<td> {{ $info->product_name }} </td>
					    		<td> {{ $info->product_details }} </td>
					    		<td> {{ $info->product_notes }} </td>
					    		<td>  
					    			<a class="btn btn-success btn-xs" href="{{ URL::to('/new/product/setup/'.$info->product_id.'/edit') }}"  > <i class="fa fa-pencil"> </i></a>      
                                    <a class="btn btn-xs btn-danger" href="javascript:;" onclick="productSetupDeleted('{{$info->product_id}}','{{ $info->product_name }}')"><i class="fa fa-trash"></i></a>
                                </td>
					    	</tr>
				    	@endforeach
				    </tbody>
			    </table>
	    	</div>
		</div>
	</div>
</div>
@include('modules.inventory.master.delete')
@stop