@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Product Update </a></li>
    </ul>
</div>

@include('modules.inventory.master.navigation')
@include('modules.inventory.master.error')

<div id="deletenotification"></div>
<div id="main-wrapper">
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
				<h2 class="title-house"> Product Setup</h2>
				 {{Form::open(array('url'=>"new/product/setup/".$productSetup->product_id,'class'=>'form-horizontal form-bordered','method'=>'PUT'))}}
					<div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product ID:</label>
		                <div class="col-md-8">
		                    {!! Form::text('product_id', $productSetup->product_id ,['class'=>'form-control','placeholder'=>'Product ID','readonly']) !!}
		                </div>
		            </div>
		            <div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product Code:</label>
	                	<div class="col-md-8">
		                    {!! Form::text('product_code',$productSetup->product_code,['class'=>'form-control','placeholder'=>'Product Code']) !!}
		                </div>
		            </div>
		           <div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product Category(Item Type):</label>
		                <div class="col-md-8">
		                	<select class="form-control chosen-select-width" name="productcategory" id='productcategory'>
		                		<option value="">Select Product Category</option>
		                		@foreach($productCategory as $productCategoryInfo)
		                			<option value="{{  $productCategoryInfo->category_id }}" @if($productSetup->category_id ==  $productCategoryInfo->category_id ) selected @endif> {{  $productCategoryInfo->category_name }} </option>
		                		@endforeach
		                	</select>
		                </div>
		            </div>
					
					<div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product Unit:</label>
		                <div class="col-md-8">
		                	<select class="form-control chosen-select-width" name="productunit" id='productunit'>
		                		<option value="">Select Product Unit</option>
		                		@foreach($productUnits as $pproductUnitsInfo)
		                		<option value="{{  $pproductUnitsInfo->unit_id }}" @if($productSetup->punit_id ==  $pproductUnitsInfo->unit_id ) selected @endif> {{  $pproductUnitsInfo->unit_name }} </option>
		                		@endforeach
		                	</select>
		                </div>
		            </div>
					<div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Manufacturer(Band)</label>
		                <div class="col-md-8">
		                	<select class="form-control chosen-select-width" name="manufacturer" id='manufacturer'>
		                		<option value="">Select Manufacturer Unit</option>
		                		@foreach($manufacturerList as $manufacturerInfo)
		                			<option value="{{  $manufacturerInfo->manufact_id }}"  @if($productSetup->manufact_id ==  $manufacturerInfo->manufact_id ) selected @endif> {{  $manufacturerInfo->manufact_name }} </option>
		                		@endforeach
		                	</select>
		                </div>
		            </div>
		            <div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product Name:</label>
		                <div class="col-md-8">
		                    {!! Form::text('product_name', $productSetup->product_name,['class'=>'form-control','placeholder'=>'Product Name']) !!}
		                    <br>
		                </div>
		            </div>
		            <div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product details:</label>
		                <div class="col-md-8">
		                    {!! Form::text('product_details', $productSetup->product_details,['class'=>'form-control','placeholder'=>'Product Details']) !!}
		                    <br>
		                </div>
		            </div>
					 <div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product Notes:</label>
		                <div class="col-md-8">
		                    {!! Form::textarea('product_notes', $productSetup->product_notes,['class'=>'form-control','placeholder'=>'Product Notes']) !!}
		                    <br>
		                </div>
		            </div>
		            <div class="col-md-12 text-center">
		                {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
		                <button class="btn btn-danger" type="reset">Reset</button>
		            </div>

		            {{ Form::close() }}
			</div>
		</div>
	</div>
	
</div>
@include('modules.inventory.master.delete')
@stop