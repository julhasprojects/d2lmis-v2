@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Product Setup </a></li>
    </ul>
</div>

@include('modules.inventory.master.navigation')
@include('modules.inventory.master.error')

<div id="deletenotification"></div>
<div id="main-wrapper">
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
				<h2 class="title-house"> Product Setup</h2>
				 {{Form::open(array('url'=>"new/product/setup",'class'=>'form-horizontal form-bordered','method'=>'post'))}}
					<div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product ID:</label>
		                <div class="col-md-8">
		                    {!! Form::text('product_id', $productID,['class'=>'form-control','placeholder'=>'Product ID','readonly']) !!}
		                </div>
		            </div>
		            <div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product Code:</label>
	                	<div class="col-md-8">
		                    {!! Form::text('product_code', null,['class'=>'form-control','placeholder'=>'Product Code']) !!}
		                </div>
		            </div>
		           <div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product Category(Type)</label>
		                <div class="col-md-8">
		                	<select class="form-control chosen-select-width" name="productcategory" id='productcategory'>
		                		<option value="">Select Product Category</option>
		                		@foreach($productCategory as $productCategoryInfo)
		                		<option value="{{  $productCategoryInfo->category_id }}"> {{  $productCategoryInfo->category_name }} </option>
		                		@endforeach
		                	</select>
		                </div>
		            </div>
					
					<div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product Unit:</label>
		                <div class="col-md-8">
		                	<select class="form-control chosen-select-width" name="productunit" id='productunit'>
		                		<option value="">Select Product Unit</option>
		                		@foreach($productUnits as $pproductUnitsInfo)
		                		<option value="{{  $pproductUnitsInfo->unit_id }}"> {{  $pproductUnitsInfo->unit_name }} </option>
		                		@endforeach
		                	</select>
		                </div>
		            </div>
					<div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Manufacturer(Brand)</label>
		                <div class="col-md-8">
		                	<select class="form-control chosen-select-width" name="manufacturer" id='manufacturer'>
		                		<option value="">Select Manufacturer Unit</option>
		                		@foreach($manufacturerList as $manufacturerInfo)
		                			<option value="{{  $manufacturerInfo->manufact_id }}"> {{  $manufacturerInfo->manufact_name }} </option>
		                		@endforeach
		                	</select>
		                </div>
		            </div>
		            <div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product Name:</label>
		                <div class="col-md-8">
		                    {!! Form::text('product_name', null,['class'=>'form-control','placeholder'=>'Product Name']) !!}
		                    <br>
		                </div>
		            </div>
		            <div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product details:</label>
		                <div class="col-md-8">
		                    {!! Form::text('product_details', null,['class'=>'form-control','placeholder'=>'Product Details']) !!}
		                    <br>
		                </div>
		            </div>
					 <div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product Notes:</label>
		                <div class="col-md-8">
		                    {!! Form::textarea('product_notes', null,['class'=>'form-control','placeholder'=>'Product Notes']) !!}
		                    <br>
		                </div>
		            </div>
		            <div class="col-md-12 text-center">
		                {!! Form::submit('Submit', ['class'=>'btn btn-primary']) !!}
		                <button class="btn btn-danger" type="reset">Reset</button>
		            </div>

		            {{ Form::close() }}
			</div>
		</div>
	</div>
	<div class="col-md-12 col-xs-12 col-lg-12  left-box"> 
		<div class="panel-white">
			<div class="panel-body">
				{{ csrf_field() }}
				<h2 class="title-house"> Product   List </h2>
			 	<table class="table table-hover table-bordered display" id="tableresponsive" style="width: 100%; cellspacing: 0;">
				    <thead class="table-header-bg">
				        <tr>
				            <th>SN</th>
				            <th>Product ID</th>
				            <th>Category Name</th>
				            <th>Product Unit</th>
				            <th>Manufacturer</th>
				            <th>Product Code</th>
				            <th>Product Name</th>
				            <th>Product Detials</th>
				            <th>Product Notes</th>
				            
				            <th>Action</th>
				        </tr>
				    </thead>
				    <tbody>
				    	<?php $sn =1 ?>
				    	@foreach($productSetup as $info)
					    	<tr id="product_Setup_{{ $info->product_id }}">
					    		<td> {{ $sn++ }} </td>
					    		<td> {{ $info->product_id }} </td>
					    		<td> {{ $info->category_name }} </td>
					    		<td> {{ $info->unit_name }} </td>
					    		<td> {{ $info->manufact_name }} </td>
					    		<td> {{ $info->product_code }} </td>
					    		<td> {{ $info->product_name }} </td>
					    		<td> {{ $info->product_details }} </td>
					    		<td> {{ $info->product_notes }} </td>
					    		<td>  
					    			<a class="btn btn-success btn-xs" href="{{ URL::to('/new/product/setup/'.$info->product_id.'/edit') }}"  > <i class="fa fa-pencil"> </i></a>      
                                    <a class="btn btn-xs btn-danger" href="javascript:;" onclick="productSetupDeleted('{{$info->product_id}}','{{ $info->product_name }}')"><i class="fa fa-trash"></i></a>
                                </td>
					    	</tr>
				    	@endforeach
				    </tbody>
			    </table>
	    	</div>
		</div>
	</div>
</div>
@include('modules.inventory.master.delete')
@stop