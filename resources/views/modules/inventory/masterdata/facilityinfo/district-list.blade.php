@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> District List</a></li>
    </ul>
</div>

@include('modules.inventory.master.navigation')
@include('modules.inventory.master.error')
@include('master.message')
<div id="deletenotification"></div>
<div id="main-wrapper">
	
	<div class="col-md-12 col-xs-12 col-lg-12  left-box"> 
		<div class="panel-white">
			<div class="panel-body">
				{{ csrf_field() }}
				<h2 class="title-house"> District List </h2>
			 	<table class="table table-hover table-bordered display" id="tableresponsive" style="width: 100%; cellspacing: 0;">
				    <thead class="table-header-bg">
				        <tr>
				            <th>SN</th>
				            <th>Division Name</th>
				            <th>District Name</th>
				            <th>District Code</th>
				            <th>Created</th>
				            <th>Action</th>
				        </tr>
				    </thead>
				    <tbody>
				    	<?php $sn =1 ?>
				    	@foreach($districtList as $info)
					    	<tr id="masterfacility_delete_{{ $info->facility_id }}">
					    		<td> {{ $sn++ }} </td>
					    		<td> {{ $info->division_id }} </td>
					    		<td> {{ $info->district_name }} </td>
					    		<td> {{ $info->district_id }} </td>
					    		<td> {{ $info->created_at }} </td>
					    		<td>  
					    			<a class="btn btn-success btn-xs" href="{{ URL::to('/masterfacility/'.$info->manufact_id.'/edit') }}"  > <i class="fa fa-pencil"> </i></a>      
                                    <a class="btn btn-xs btn-danger" href="javascript:;" onclick="meanufacetDelete('{{$info->manufact_id}}','{{ $info->manufact_name }}')"><i class="fa fa-trash"></i></a>
                                </td>
					    	</tr>
				    	@endforeach
				    </tbody>
			    </table>
	    	</div>
		</div>
	</div>
</div>
@include('modules.inventory.master.delete')
@stop