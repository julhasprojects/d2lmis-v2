@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Products Unit create and  List </a></li>
    </ul>
</div>

@include('modules.inventory.master.navigation')
@include('modules.inventory.master.error')
@include('master.message')
<div id="deletenotification"></div>
<div id="main-wrapper">
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
				<h2 class="title-house"> Product Unit Create</h2>
				 {{Form::open(array('url'=>"product/unit",'class'=>'form-horizontal form-bordered','method'=>'post'))}}

		           <div class="col-md-12">
		                <label class="control-label col-md-2">Product Unit Name:</label>
		                <div class="col-md-8">
		                    {!! Form::text('unit_name', null,['class'=>'form-control','placeholder'=>'Unit Name']) !!}
		                    <br>
		                </div>
		            </div>

		            <div class="col-md-12">
		                <label class="control-label col-md-2">Product Unit Notes:</label>
		                <div class="col-md-8">
		                    {!! Form::textarea('details', null,['class'=>'form-control','placeholder'=>'Detials','col'=>'2','row'=>'2']) !!}
		                    <br>
		                </div>
		            </div>

		            <div class="col-md-12 text-center">
		                {!! Form::submit('Submit', ['class'=>'btn btn-primary']) !!}
		                <button class="btn btn-danger" type="reset">Reset</button>
		            </div>

		            {{ Form::close() }}
			</div>
		</div>
	</div>
	<div class="col-md-12 col-xs-12 col-lg-12  left-box"> 
		<div class="panel-white">
			<div class="panel-body">
				{{ csrf_field() }}
				<h2 class="title-house"> Product  Unit  List </h2>
			 	<table class="table table-hover table-bordered display" id="tableresponsive" style="width: 100%; cellspacing: 0;">
				    <thead class="table-header-bg">
				        <tr>
				            <th>SN</th>
				            <th>Units ID</th>
				            <th>Units Name</th>
				            <th>Details</th>
				            <th>Created At</th>
				            <th>Action</th>
				        </tr>
				    </thead>
				    <tbody>
				    	<?php $sn =1 ?>
				    	@foreach($productUnit as $info)
					    	<tr id="product_unit_{{ $info->unit_id }}">
					    		<td> {{ $sn++ }} </td>
					    		<td> {{ $info->unit_id }} </td>
					    		<td> {{ $info->unit_name }} </td>
					    		<td> {{ $info->unit_notes }} </td>
					    		<td> {{ $info->created_at }} </td>
					    		<td>  
					    			<a class="btn btn-success btn-xs" href="{{ URL::to('/product/unit/'.$info->unit_id.'/edit') }}"  > <i class="fa fa-pencil"> </i></a>      
                                    <a class="btn btn-xs btn-danger" href="javascript:;" onclick="productUnitDeleted('{{$info->unit_id}}','{{ $info->unit_name }}')"><i class="fa fa-trash"></i></a>
                                </td>
					    	</tr>
				    	@endforeach
				    </tbody>
			    </table>
	    	</div>
		</div>
	</div>
</div>
@include('modules.inventory.master.delete')
@stop