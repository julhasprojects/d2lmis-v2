@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Products Unit Update </a></li>
    </ul>
</div>

@include('modules.inventory.master.navigation')
@include('modules.inventory.master.error')
@include('master.message')
<div id="deletenotification"></div>
<div id="main-wrapper">
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
				<h2 class="title-house"> Unit Create</h2>
				 {{Form::open(array('url'=>"product/unit/".$productUnits->unit_id,'class'=>'form-horizontal form-bordered','method'=>'PUT'))}}

		           <div class="col-md-12">
		                <label class="control-label col-md-2">Unit Name:</label>
		                <div class="col-md-8">
		                    {!! Form::text('unit_name', $productUnits->unit_name,['class'=>'form-control','placeholder'=>'Unit Name']) !!}
		                    <br>
		                </div>
		            </div>

		            <div class="col-md-12">
		                <label class="control-label col-md-2"> Unit Notes:</label>
		                <div class="col-md-8">
		                    {!! Form::textarea('details', $productUnits->unit_notes,['class'=>'form-control','placeholder'=>'Detials','col'=>'2','row'=>'2']) !!}
		                    <br>
		                </div>
		            </div>

		            <div class="col-md-12 text-center">
		                {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
		                <!-- <button class="btn btn-danger" type="reset">Reset</button> -->
		            </div>

		            {{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop