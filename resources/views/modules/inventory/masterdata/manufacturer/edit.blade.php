@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Manufacturer Updated Information</a></li>
    </ul>
</div>

@include('modules.inventory.master.navigation')
@include('modules.inventory.master.error')
@include('master.message')
<div id="main-wrapper">
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
				<h2 class="title-house"> Manufacturer Craate</h2>
				 {{Form::open(array('url'=>"manufacturer/".$manufacturerList->manufact_id,'class'=>'form-horizontal form-bordered','method'=>'PUT'))}}
		           <div class="col-md-12">
		                <label class="control-label col-md-2">Manufacturer Name:</label>
		                <div class="col-md-8">
		                    {!! Form::text('menufacturer_name', $manufacturerList->manufact_name ,['class'=>'form-control','placeholder'=>'Manufacturer Name']) !!}
		                    <br>
		                </div>
		            </div>

		            <div class="col-md-12">
		                <label class="control-label col-md-2"> Manufacturer Date:</label>
		                <div class="col-md-8">
		                    {!! Form::text('menufacturer_date',  $manufacturerList->manufact_date,['class'=>'form-control bbibillto','placeholder'=>'Year-Month-Date']) !!}
		                    <br>
		                </div>
		            </div>

		            <div class="col-md-12">
		                <label class="control-label col-md-2"> Details:</label>
		                <div class="col-md-8">
		                    {!! Form::textarea('details',  $manufacturerList->manufact_details,['class'=>'form-control','placeholder'=>'Detials','col'=>'2','row'=>'2']) !!}
		                    <br>
		                </div>
		            </div>

		            <div class="col-md-12 text-center">
		                {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
		                <button class="btn btn-danger" type="reset">Reset</button>
		            </div>

		            {{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop