@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Requisition Disapproval  </a></li>
    </ul>
</div>
@include('modules.inventory.master.navigation')
@include('modules.inventory.master.error')
<div id="main-wrapper">
    {{ csrf_field() }}
	<div class="col0-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
				<h2 class="title-house">All Dis Approved Products</h2>
				 	<table class="table table-hover table-bordered display" id="tableresponsive" style="width: 100%; cellspacing: 0;">
		    			<thead class="table-header-bg">
	                        <tr>
	                            <th>S/N</th>
	                            <th>Product Name</th>
	                            <th>Resonsible Person</th>
	                            <th>Phone</th>
	                            <th>Instalaltion Date</th>
	                            <th>Requested Date </th>
	                            <th>Available Qty</th>
	                            <th>Requested Qty</th>	                         
                                <th>Approved Date</th>
	                            <th>Remarks/Reason</th>
                                <th>Status</th>
	                            <th>Action</th>
	                        </tr>
                        </thead>
                        <tbody>
                        <?php $sn=1; ?>
                        @foreach($requisitionAll as $requisition)
                            <tr class="odd gradeX">
                                <td>{{ $sn++ }}
                                </td>
                                <td>{{ $requisition->product_name }} </td>
                                <td>{{ $requisition->fullName }} </td>
                                <td>{{ $requisition->mobilenumber }}</td>
                                 <td>{{ $requisition->challan_date }}</td>
                                 <td>0</td>
                                
                                <td>{{ $requisition->client_id }}</td>
                                
                               
                                
                                <td>{{ $requisition->quantity }}</td>
                                <td>{{ $requisition->updated_at }}</td>
                                <td>{{ $requisition->remarks }}</td>
                                
                               
                              
                                <td>
                                    <span class="btn btn-danger btn-xs">Disapproved</span>
                                </td> 

                                <td>
                                    <span class="btn btn-success btn-xs" onclick="editReantry('{{ $requisition->product_id }}','{{ $requisition->quantity  }}','{{ $requisition->product_name  }}','0','{{ $requisition->transactionID  }}')">Reantary</span>    
                                </td>                                     
                            
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
			</div>
		</div>
	</div>
</div>
@include('modules.inventory.master.approved')
@stop