@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Requisition Approval  </a></li>
    </ul>
</div>
@include('modules.inventory.master.navigation')
@include('modules.inventory.master.error')
<div id="main-wrapper">
    {{ csrf_field() }}
	<div class="col0-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
				<h2 class="title-house"> All Approved Requisitions</h2>
				 	<table class="table table-hover table-bordered display" id="tableresponsive" style="width: 100%; cellspacing: 0;">
		    			<thead class="table-header-bg">
	                        <tr>
	                            <th>S/N</th>
	                            <th>Product Name</th>
	                            <th>Responsible Person</th>
	                            <th>Phone</th>
	                            <th>Installation Date</th>
	                            <th>Requested Date </th>
	                            <th>Available Qty</th>
	                            <th>Requested Qty</th>	                         
                                <th>Approved Date</th>
	                            <th>Remarks/Reason</th>
	                            <th colspan="3">Action</th>
	                        </tr>
                        </thead>
                        <tbody>
                        <?php $sn=1; ?>
                        @foreach($requisitionAll as $requisition)
                            <tr class="odd gradeX">
                                <td>{{ $sn++ }}
                                </td>
                                <td>{{ $requisition->product_name }} </td>
                                <td>{{ $requisition->fullName }} </td>
                                <td>{{ $requisition->mobilenumber }}</td>
                                 <td>{{ $requisition->challan_date }}</td>
                                 <td>0</td>
                                
                                <td>{{ $requisition->client_id }}</td>
                                
                               
                                
                                <td>{{ $requisition->quantity }}</td>
                                <td>{{ $requisition->updated_at }}</td>
                                <td>{{ $requisition->remarks }}</td>
                                 @if($requisition->approval_status == 0) 
                                    <td>
                                        <span class="btn btn-warning btn-xs">Not Approve Yet</span>
                                    </td>
                                    <td>
                                        <span class="button-apporved">
                                            <button class="btn btn-success btn-xs status {{ $requisition->id }}approved" href="javascript:;" onclick="approvedDisapprovedProduct('{{ $requisition->product_id }}','{{ $requisition->quantity  }}','{{ $requisition->product_name  }}','1','{{ $requisition->transactionID  }}','3')">
                                                <i class="glyphicon glyphicon-check"></i>
                                                Approve Now
                                            </button>
                                           
                                            <a class="btn btn-info btn-xs status {{ $requisition->id }}approved" data-toggle="modal" data-target="#editModal_{{ $requisition->storeid}}">
                                                Edit<i class="fa fa-edit"></i>
                                            </a>
                                            
                                            <button class="btn btn-danger btn-xs status {{ $requisition->id }}approved" href="javascript:;" onclick="approvedDisapprovedProduct('{{ $requisition->product_id }}','{{ $requisition->quantity  }}','{{ $requisition->product_name  }}','2','{{ $requisition->transactionID  }}','0')">
                                                <i class="glyphicon glyphicon-check"></i>
                                                Disapprove
                                            </button>
                                        </span>
                                    </td>   
                                @elseif($requisition->approval_status == 1)
                                    <td>
                                        <span class="btn btn-success btn-xs">Approved</span>
                                    </td>
                                    <td></td>
                                @elseif($requisition->approval_status == 2)
                                    <td>
                                        <span class="btn btn-danger btn-xs">Disapproved</span>
                                    </td>  
                                    <td></td>                                       
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
			</div>
		</div>
	</div>
</div>
@foreach($requisitionAll as $requisition)
<div id="editModal_{{ $requisition->storeid}}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-times" aria-hidden="true"></i> </button>
                <h4 class="modal-title">Updated Product Information </h4>
            </div>
            <div class="modal-body modal-inventory" id="info">
                {!! Form::open(['url'=>'update/storesdetails/products/'.$requisition->storeid, 'class' =>'form col-md-12', 'enctype'=>'multipart/form-data','method'=>'PUT']) !!}
                <div class="form-gorups listproducts" >
                    <select class="form-control nameId" name="product_id" id="product_id">
                        <option value="">Select Product</option>
                        @foreach($productList as $product)
                            <option value="{{ $product->product_id }}" @if($requisition->product_id == $product->product_id ) selected @endif >
                            {{ $product->product_name }}</option>
                        @endforeach
                    </select>
                    
                </div>
                <div class="form-gorups listproducts">
                    
                </div>
                <div class="form-gorups listproducts">
                    <input type="text" value="{{ $requisition->unit_price}}" class="form-control unitPriceEdit" id="unitPriceEdit_{{$requisition->storeid}}" placeholder='Unit Price' name="unit_price">
                   
                </div>
                <div class="form-gorups listproducts">
                    <input type="text" value="{{ $requisition->quantity}}" class="form-control quantityEdit" id="quantityEdit_{{$requisition->storeid}}" placeholder='Quantity' name="quantity">
                </div>
                <div class="form-gorups listproducts">
                    <input type="text" value="{{ $requisition->total_price}}" class="form-control totalPriceEdit" id="totalPriceEdit_{{$requisition->storeid}}" placeholder='Total Price' name="total_price" readonly>
                </div>
          
                <button type="button" data-dismiss="modal" class="btn default">Cancel</button>
                <button type="submit"  class="btn btn-success" > Update</button>
            {!! Form::close() !!}
            </div>
            
        </div>
   </div>
</div>
@endforeach
@include('modules.inventory.master.approved')
@stop