<table class="table table-hover table-bordered display" id="tableresponsive" style="width: 100%; cellspacing: 0;">
		<thead class="table-header-bg">
            <tr>
                <th>Sl.</th>
                <th>Product Name</th>
                <th>Current Stock</th>
                <th>Sale Price</th>
            
                <th>Add  </th>
            </tr>
        </thead>
        <tbody>
        	<?php $sl=1; ?>
        	@foreach($productInfo as $info)
        	<tr>
        		<td>{{ $sl++ }}</td>
        		<td><input type="hidden" value="{{ $info->id }}"><span id="productNameSale">{{ $info->product_name }}</span></td>
        		<td>{{ $info->qty }}</td>
        		<td>{{ $info->sale_price }}</td>
       
        		<td>
                    <button class="btn btn-primary" type="button" onclick="addProductSell('{{ $info->id }}','{{ $info->product_name }}','{{ $info->qty }}','{{ $info->sale_price }}','{{ $info->product_id }}')">
                      <span class="glyphicon glyphicon-plus"></span>
                    </button>
                </td>
        	</tr>
        	@endforeach
        </tbody>
</table>