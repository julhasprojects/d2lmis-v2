@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Damage Stock Return  </a></li>
    </ul>
</div>
@include('modules.inventory.master.navigation')
@include('modules.inventory.master.error')
@include('master.message')

<div id="main-wrapper">
    <div class="col-md-12 colum-design-para">
        <div class="panel panel-white">
            <div class="panel panel-body">
                <h2 class="title-house">Category</h2>
                @foreach($productList as $info)
                <div class="col-md-2 col-xs-12 col-lg-2">
            
                <table class="table table-bordered">
                    <thead>
                      <tr class="bg-success">
                        <th colspan="2">Tetrasoft</th>
                      </tr>
                    </thead>
                    <tbody>
                        <script>
                         $(document).ready(function () {
                            JsBarcode("#productID_{{ $info->product_id }}","{{ $info->product_id }}",{
                                    displayValue: false,
                                    width: 2,
                                    height:10,
                                });
                            });

                        </script>
                        <tr>
                            
                            <td colspan="2">
                                    <svg id="productID_{{ $info->product_id }}" class="barcodenewconid"></svg> 
                            </td>
                        </tr>
                        <tr>
                            <td> ID</td>
                            <td>{{ $info->product_id }}</td>
                        </tr>
                       
                        <tr>
                            <td> Sale Price</td>
                            <td>{{ $info->sale_price }}</td>
                        </tr>
                    </tbody>
                  </table>
                     
                </div> 
                @endforeach
            </div>
        </div>
    </div>

</div>

@stop