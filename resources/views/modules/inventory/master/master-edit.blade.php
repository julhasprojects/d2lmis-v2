@if(!empty($transaction))
<div id="editModel" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-times" aria-hidden="true"></i> </button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body" id="info">
                
                <div class="col-md-12 col-xs-12 col-lg-12 product-update-unit">        
                    <select class="form-control typeId" name="category_id" id="category_id">
                       <option value="">Select Category</option>
                        @foreach($categoryList as $category)
                            <option value="{{ $category->category_id }}">{{ $category->category_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-12 col-xs-12 col-lg-12 product-update-unit">  
                    <select class="form-control nameId" name="product_id" id="product_id">
                        <option value="">Select Product</option>
                        @foreach($productList as $product)
                            <option value="{{ $product->product_id }}">{{ $product->product_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-12 col-xs-12 col-lg-12 product-update-unit">        
                    {!! Form::number('unit_price',$transaction->unit_price,['class'=>'form-control','id'=>'unit_price','placeholder'=>'Unit Price']) !!}
                </div>
                <div class="col-md-12 col-xs-12 col-lg-12 product-update-unit">
                    {!! Form::number('quantity', $transaction->quantity,['class'=>'form-control', 'id'=>'quantity','placeholder'=>'Quantity']) !!}
                </div>
                <div class="col-md-12 col-xs-12 col-lg-12 product-update-unit">        
                    {!! Form::number('total_price', $transaction->total_price,['class'=>'form-control','id'=>'total_price','step'=>'0.02','readonly','placeholder'=>'Total Price']) !!}
                </div>
                 <div class="col-md-12 col-xs-12 col-lg-12 product-update-unit">  
                    <div class="input-group date" data-date-format="yyyy-mm-dd">
                        <input type="text" name="warranty_period" id="warranty_period" class="form-control" placeholder="yyyy-mm-dd">
                        <span class="input-group-btn"><button class="btn default" type="button"><i class="glyphicon glyphicon-th"></i></button></span>
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 col-lg-12 product-update-unit">   
                    {!! Form::text('comments', $transaction->comments,['class'=>'form-control','placeholder'=>'Comments']) !!}
                </div>
             
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn default">Cancel</button>
                <button type="button" data-dismiss="modal" class="btn red" id="delete"><i class="fa fa-trash"></i> Update</button>
            </div>
        </div>
   </div>
</div>
@endif