<div id="print" style="display: none;">
        <!-- <div id="officePrint" > -->
        <div class='application-form'>
            
            <div class='print-header-row'>
                <div class='print-pull-left'><img src="{{ URL::asset('assets/images/' . $companyinfo->logo) }}" alt='' /></div>
            </div>
            <hr>
            <div class="span-just" style="font-size: 18px;text-align: center;margin-top: 10px">
                Invoice /bill
            </div>
            
            <div class="print-body">
                <div class="col-md-6 col-xs-6 col-sm-6 leftjoin" style="width:50%;float:left">
                    <p>Customer Name:<span class="sale_name_print" id="sale_name_print"></span></p>
                    <p>Address:<span class="sale_address_print" id="sale_address_print"></span></p>
                    <p>Phone:<span class="sale_phone_print" id="sale_phone_print"></span></p>
                </div>
                <div class="col-md-6 col-xs-6 col-sm-6 leftjoin" style="width:50%;float:left">
                     <p>Bill:<span class="sale_bill_print" id="sale_bill_print"></span></p>
                     <p>Date:<span class="sale_date_print" id="sale_date_print"></span></p>
                     <p>Sold By:<span class="sale_sold_print" id="sale_sold_print"></span></p>
                </div>
            </div>
            <table class="table" style="width: 100%;border:1px solid #eee"><thead><tr style="border-bottom: 1px solid #eee"><th>Sl</th><th>Descraption</th><th>Quntity</th><th>Rate</th><th>Amount</th></tr></thead>
                <tbody id="table_result"></tbody>

            
                <tbody style="border-top: 1px solid #66">
                    <tr>
                        <td colspan="3"></td>
                        <td> Total:</td>
                        <td id="totalAmountPay" style="text-align: center;"></td>
                    </tr>
                </tbody>
            </table>
            <div class='print-option-row' style="margin-top: 30px">
                
              
                <div style="width: 50%;float: left;">
                    <div class='complain-groups print-pull-left signnatre'   style="margin-bottom: 10px;width: 100%"> Recived in good condition</div>
                    <h3>Remarks</h3>
                   <ul style="padding-left:0px;list-style: none;font-size: 12px;font-weight: normal;">
                        <li style="list-style: none;font-size: 12px;font-weight: normal;">1.GOODS ONCE SOLD CAN NOT BE RETURNED BACK.  </li>
                        <li style="list-style: none;font-size: 12px;font-weight: normal;">2.WARRANTY VOID IF STICKER REMOVED OR BROKEN. </li>
                        <li style="list-style: none;font-size: 12px;font-weight: normal;">3.WARRANTY VOID IN CASE OF PHYSICAL DAMAGE OR BURN. </li>
                        <li style="list-style: none;font-size: 12px;font-weight: normal;">4.EXCHANGE OF PRODUCT ACCEPTABLE WITHIN 24 OURS' WITH       
                    GOOD CONDITION.  </li>
                        <li style="list-style: none;font-size: 12px;font-weight: normal;">Web: www.delmis.com</li>
                    </ul> 
                </div>
                <div style="width: 50%;float: left;">
                      <div class='complain-groups print-pull-right signnatre'  style="margin-bottom: 10px;width: 100%"> For {{ $companyinfo->company_shortname }}</div>
                    <h3>Address</h3>
                    <ul style="padding-left:0px;list-style: none;font-size: 12px;font-weight: normal;">
                        <li style="list-style: none;font-size: 12px;font-weight: normal;">9,Mohammadia HousingLtd. MainRoad,</li>
                        <li style="list-style: none;font-size: 12px;font-weight: normal;">Mohammadpur Dhaka Phone: 02-9141223,01841-2SS-479</li>
                        <li style="list-style: none;font-size: 12px;font-weight: normal;">E-mail:info@delmis.com</li>
                        <li style="list-style: none;font-size: 12px;font-weight: normal;">Facebook: www.facebook.com/delmis.com</li>
                        <li style="list-style: none;font-size: 12px;font-weight: normal;">Web: www.delmis.com</li>
                    </ul>
                </div>
                <div style="width: 100%;float: left;"><p style="text-align: center;">*Thank You For Shopping With Us*</p></div>
                   
            </div>
        </div>
       
    </div>
<div id="posPrint" style="display: none;">
        <!-- <div id="officePrint" > -->
        <div class='application-form'>
            
            <div class='print-header-row'>
                <div class='print-pull-left'><img src="{{ URL::asset('assets/images/' . $companyinfo->logo) }}" alt='' /></div>
            </div>
            <hr>
            <div class="span-just" style="font-size: 18px;text-align: center;margin-top: 10px">
                Invoice /bill
            </div>
            
            <div class="print-body">
                <div class="col-md-6 col-xs-6 col-sm-6 leftjoin" style="width:50%;float:left">
                    <p>Customer Name:<span class="sale_name_print" id="pos_sale_name_print"></span></p>
                    <p>Address:<span class="sale_address_print" id="pos_sale_address_print"></span></p>
                    <p>Phone:<span class="sale_phone_print" id="pos_sale_phone_print"></span></p>
                </div>
                <div class="col-md-6 col-xs-6 col-sm-6 leftjoin" style="width:50%;float:left">
                     <p>Bill:<span class="sale_bill_print" id="pos_sale_bill_print"></span></p>
                     <p>Date:<span class="sale_date_print" id="pos_sale_date_print"></span></p>
                     <p>Sold By:<span class="sale_sold_print" id="pos_sale_sold_print"></span></p>
                </div>
            </div>
            <table class="table" style="width: 100%;border:1px solid #eee"><thead><tr style="border-bottom: 1px solid #eee"><th>Sl</th><th>Descraption</th><th>Quntity</th><th>Rate</th><th>Amount</th></tr></thead>
                <tbody id="pos_table_result"></tbody>

            
                <tbody style="border-top: 1px solid #66">
                    <tr>
                        <td colspan="3"></td>
                        <td> Total:</td>
                        <td id="pos_totalAmountPay" style="text-align: center;"></td>
                    </tr>
                </tbody>
            </table>
            <div class='print-option-row' style="margin-top: 05px">
                
              
                <div style="width: 100%;float: left;">
                    <div class='complain-groups print-pull-left signnatre'   style="margin-bottom: 10px;width: 100%"> Recived in good condition</div>
                    <h3>Remarks</h3>
                   <ul style="padding-left:0px;list-style: none;font-size: 12px;font-weight: normal;">
                        <li style="list-style: none;font-size: 12px;font-weight: normal;">1.GOODS ONCE SOLD CAN NOT BE RETURNED BACK.  </li>
                        <li style="list-style: none;font-size: 12px;font-weight: normal;">2.WARRANTY VOID IF STICKER REMOVED OR BROKEN. </li>
                        <li style="list-style: none;font-size: 12px;font-weight: normal;">3.WARRANTY VOID IN CASE OF PHYSICAL DAMAGE OR BURN. </li>
                        <li style="list-style: none;font-size: 12px;font-weight: normal;">4.EXCHANGE OF PRODUCT ACCEPTABLE WITHIN 24 OURS' WITH       
                    GOOD CONDITION.  </li>
                        <li style="list-style: none;font-size: 12px;font-weight: normal;">Web: www.delmis.com</li>
                    </ul> 
                </div>
                <div style="width: 100%;float: left;">
                      <div class='complain-groups print-pull-right signnatre'  style="margin-bottom: 10px;width: 100%"> For {{ $companyinfo->company_shortname }}</div>
                    <h3>Address</h3>
                    <ul style="padding-left:0px;list-style: none;font-size: 12px;font-weight: normal;">
                        <li style="list-style: none;font-size: 12px;font-weight: normal;">9,Mohammadia HousingLtd. MainRoad,</li>
                        <li style="list-style: none;font-size: 12px;font-weight: normal;">Mohammadpur Dhaka Phone: 02-9141223,01841-2SS-479</li>
                        <li style="list-style: none;font-size: 12px;font-weight: normal;">E-mail:info@delmis.com</li>
                        <li style="list-style: none;font-size: 12px;font-weight: normal;">Facebook: www.facebook.com/delmis.com</li>
                        <li style="list-style: none;font-size: 12px;font-weight: normal;">Web: www.delmis.com</li>
                    </ul>
                </div>
                <div style="width: 100%;float: left;"><p style="text-align: center;">*Thank You For Shopping With Us*</p></div>
                   
            </div>
        </div>
       
    </div>
<div class="modal fade bd-example-modal-lg model-sale" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New Sale /Product Requistion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container product-searching">
                    <div class="row">
                        <form>
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <label for="text">Type Product Id/Product Name</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="SearchProduct"   placeholder="Type Product Id/Product Name">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="salesLoader"></div>
                <div id="productSearchingResult"></div>
                <div class="row sale-product-reuissition">
                    <div class="col-md-6 col-xs-6">
                        <div class="row">
                            <div class="col-md-6 col-xs-6">
                                <label><input type="checkbox" value="">New Sale</label>
                            </div>
                            <div class="col-md-4 col-xs-4">
                                <a href="{{ URL::to( '/create-new-requisition/' ) }}">
                                   <input type="checkbox" value="">Product Requisition
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    {!! Form::open(['url'=>'sale/requisition', 'class' =>'form col-md-12', 'enctype'=>'multipart/form-data','method'=>'post']) !!}
                <div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
                    <label class="col-sm-3 control-label package-label" for="grandtotal">
                        Transaction No
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="requisition_no" id="requisition_no" class="form-control requisition_no" value="<?php echo date('YmdHis');?>" readonly="">
                    </div>
                </div>

                <div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
                    <label class="col-sm-3 control-label package-label" for="grandtotal">Select Date</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="selectDatetime" name="selectDatetime" placeholder="yyyy-mm-dd"  data-default="">
                     
                    </div>
               </div>              
                <div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
                    <label class="col-sm-3 control-label package-label" for="grandtotal">
                        Sales ID
                    </label>
                    <div class="col-sm-9">
                        <input type="number" name="sales_ID" id="sales_ID" class="form-control" value="{{ $salesID }}" readonly="">
                    </div>
                </div>
            

                <div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
                    <label class="col-sm-3 control-label package-label" for="grandtotal">
                        Sales Person
                    </label>
                    <div class="col-sm-9">
                        <select class="form-control" name="salesPerson" id="salesPerson" >
                            <option value="">Select Employee</option>
                            @foreach($empployeeList as $emp)
                                <option value="{{ $emp->employeeID }}" @if($entryBy == $emp->employeeID ) selected @endif>{{ $emp->fullName }}</option>
                            @endforeach
                        </select> 
                    </div>
                </div>
                
                <div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
                    <label class="col-sm-3 control-label package-label" for="grandtotal">
                        Customer Name
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="customer_name" id="customer_name" class="form-control" >
                    </div>
                </div>
                <div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
                    <label class="col-sm-3 control-label package-label" for="grandtotal">
                        Phone Number
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="customer_phone_number" id="customer_phone_number" class="form-control" value="0" >
                    </div>
                </div>
                <div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
                    <label class="col-sm-3 control-label package-label" for="grandtotal">
                        Email
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="customer_email" id="customer_email" class="form-control" >
                    </div>
                </div>
                <div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
                    <label class="col-sm-3 control-label package-label" for="grandtotal">
                        Customer Address
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="customer_address" id="customer_address" class="form-control" >
                    </div>
                </div>
                <div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
                    <label class="col-sm-3 control-label package-label" for="grandtotal">
                        Pay Date
                    </label>
                    <div class="col-sm-9">
                         <input type="text" class="form-control" id="pay_date" name="pay_date" placeholder="yyyy-mm-dd"  >
                     
                    </div>
                </div>
                
                <table id="table_gr" class="table table-bordered table-hover display dataTable table-header-bg">
                    <thead>
                        <tr>
                            <th>Select Item</th>
                            <th>Item Name</th>
                            <th>Current Stock</th>
                            <th>Sale Price</th>
                            <th> Qty</th>
                            <th> Discount</th>
                            <th> Adj.</th>
                            <th> Vat</th>
                            <th>Total Amount</th>
                            <th>Pay Amount</th>
                            <th>Charge Amount</th>
                            <th>Due Amount</th>
                            <th>Add  </th>
                        </tr>
                    </thead>
                    <tbody class="controls">
                        <tr class="entry">
                            <td class="col-md-1">
                                <select class="form-control typeId" name="category_id[]" id="category_id">
                                   <option value="">Select Category</option>
                                    @foreach($categoryList as $category)
                                        <option value=" {{ $category->category_id }}">{{ $category->category_name.'--'.$category->category_notes }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td class="col-md-1">
                                <select class="form-control product_id product_qty "  name="product_id[]" id="product_id">
                                    <option value="">Select Product</option>
                                    @foreach($productList as $product)
                                        <option value="{{ $product->product_id }}">{{ $product->product_name }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>

                                <input type="text" name="unit_price[]" placeholder="Unit Pric" id="available_products" readonly="" class="form-control" value="0">
                            </td>
                            <td>
                                {!! Form::number('sale_price[]', 0,['class'=>'form-control','placeholder'=>'Sale Price', 'id'=>'sale_price', 'value'=>'0','readonly']) !!}
                            </td>
                            <td>
                                {!! Form::number('quantity[]', 0,['class'=>'form-control','placeholder'=>'Qty', 'id'=>'quantity_calculate', 'value'=>'0']) !!}
                                <input type="hidden" id="hiddenqty">
                            </td> 
                            <td>
                                {!! Form::number('discount[]', 0,['class'=>'form-control','placeholder'=>'discount', 'id'=>'discount', 'value'=>'0']) !!}
                            </td>
                            <td>
                                {!! Form::number('adj[]', 0,['class'=>'form-control','placeholder'=>'Adj', 'id'=>'adj', 'value'=>'0']) !!}
                            </td> 
                            <td>
                                {!! Form::number('vat[]', 0,['class'=>'form-control','placeholder'=>'vat', 'id'=>'vat', 'value'=>'0']) !!}
                            </td> 
                            <td>
                                {!! Form::number('totalamount[]', 0,['class'=>'form-control','placeholder'=>'t. Amount', 'id'=>'totalAmount', 'value'=>'0','readonly']) !!}
                            </td> 
                            <td>
                                {!! Form::number('payamount[]', 0,['class'=>'form-control payamount','placeholder'=>'pamount', 'id'=>'payamount', 'value'=>'0','readonly']) !!}
                            </td> 
                            <td>
                                {!! Form::number('chageamount[]', 0,['class'=>'form-control','placeholder'=>'c. p.', 'id'=>'charge_amount', 'value'=>'0']) !!}
                            </td> 
                            <td>
                                {!! Form::number('dueamount[]', 0,['class'=>'form-control','placeholder'=>'D. Amount', 'id'=>'dueAmount', 'value'=>'0', 'readonly']) !!}
                            </td> 
                            <td>
                                <span class="input-group-btn">
                                    <button class="btn btn-primary btn-add" type="button">
                                      <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </span>
                            </td>
                        </tr>
                        <tr id="newPara"></tr>
                        <tr>
                            <td colspan="8"></td>
                            <td><span style="color:#666">Sub Total</span></td>
                            <td><span style="color:#666"><input type="text" value="0"  id="saleSubTotalAmount"></td>
                            <td colspan="3"></td>
                        </tr>
                        <tr>
                            <td colspan="8"></td>
                            <td><span style="color:#666">Total</span></td>
                            <td><span style="color:#666"><input type="text" value="0"  id="saleTotalAmount"></td>
                            <td colspan="3"></td>
                        </tr>
                    </tbody>

                </table>

                <div class="col-md-12 text-center">
                    <br><br>
                    <button class="btn btn-danger" type="reset">Cancel</button>

                    {!! Form::submit('Confirm Sale', ['class'=>'btn btn-primary']) !!}
                    
                </div>
                {!! Form::close() !!}
                <button class="btn btn-warning" type="reset" onclick="inventorySellPrint()">Print</button>
                <button class="btn btn-warning" type="reset" onclick="inventorySellPostPrint()">Poss Print</button>
                </div>
            </div>
          
        </div>
    </div>
</div>
<script type="text/javascript">
  $(function () {
       $('#selectDatetime').datetimepicker({
            format: 'YYYY-MM-DD hh:mm:ss',
            minDate: new Date(),
        });
        $('#pay_date').datetimepicker({
            format: 'YYYY-MM-DD',
            // minDate: new Date(),
        });
       
        
    });

    $(function() {
        $(document).on('click', '.btn-add', function(e) {
            var length = $(".typeId").length;
            
            $( "#newPara" ).before( '<tr id="property_'+ length + '"><td class="col-md-1"> <select class="form-control typeId" id="category_id_'+ length + '" name="category_id[]"> <option value="">Select Category</option> @foreach($categoryList as $category)<option value="{{ $category->category_id }}">{{ $category->category_name .'--'.$category->category_notes}}</option>@endforeach </select> </td><td class="col-md-1"> <select class="products-name form-control nameId" name="product_id[]" id="product_id_'+ length + '" onclick="productListAvailbleProduct('+length+')"> <option value="" >Select Product</option> @foreach($productList as $product)<option value="{{ $product->product_id }}">{{ $product->product_name }}</option> @endforeach </select> </td><td> <input   id="available_products_'+ length +'" class="form-control" placeholder="Stock Qty" name="unit_price[]" type="text" value="0"  readonly><td> <input   id="sale_price_'+ length +'" class="form-control" placeholder="Sale.price" name="sale_price[]" readonly type="text" id="sale_price_'+ length +'" value="0"><input  id="hidden_unit_price_'+ length +'" class="form-control" placeholder="Stock Qty" name="unit_price[]" type="hidden" id="available_products_"'+ length +' readonly>  </td><td> <input class="form-control qty-calculateds" placeholder="Qty" id="quantity_id_'+ length + '" value="0" onkeyup="productListAvailbleProduct('+length+')" name="quantity[]" type="number"> </td><td> <input class="form-control qty-calculateds" placeholder="discount" value="0" id="discount_'+ length + '" name="discount[]" type="number" value="0" onkeyup="productListAvailbleProduct('+length+')"> </td><td> <input class="form-control qty-calculateds" placeholder="Adj" id="adjustment_'+ length + '" name="adj[]" value="0" type="number"  onkeyup="productListAvailbleProduct('+length+')"> </td><td> <input class="form-control qty-calculateds" placeholder="Vat" value="0" id="vat_'+ length + '" name="vat[]" type="number"  onkeyup="productListAvailbleProduct('+length+')"> </td>><td> <input class="form-control qty-calculateds" value="0" placeholder="T P." id="totalAmount_'+ length + '" name="totalamount[]"  type="number"  onkeyup="productListAvailbleProduct('+length+')" readonly> </td><input class="form-control" value="0" placeholder="t. Amount" id="totalAmount" name="totalamount[]" type="number"><td><input class="form-control payamount" placeholder="pamount" value="0" id="payamount_'+ length + '" readonly onkeyup="dueAmountSetupInventory('+length+')" name="payamount[]" type="number"></td><td> <input class="form-control" placeholder="c. pamount" id="charge_amount" name="chageamount[]" value="0" type="number"></td><td> <input class="form-control" placeholder="D. Amount"  readonly name="dueamount[]" type="number" id="dueAmount_'+ length + '" value="0" onkeyup="productListAvailbleProduct('+length+')">/td><td> <span class="input-group-btn"> <button class="btn btn-danger btn-remove_'+ length+'" type="button">  <span class="glyphicon glyphicon-minus"></span> </button> </span> </td></tr>' );

                

                $('#category_id_'+ length ).on('click',function() {
        
                    var categoryId = $( this).val();

                    var value = '?categoryId=' + categoryId;  
                   
                    $.ajax({
                        type: "GET",
                        url: basePath + 'product/category/show' + value
                    }).success(function ( result ) {

                        $('#product_id_'+ length ).html( result );
                        $('#loader').slideUp(200,function(){        
                            $('#loader').remove();
                        });
                        $(".loader").fadeOut("slow"); 

                    }).error(function ( result ) {

                        console.log('Information not found');

                    }); 
                });

                $('.btn-remove_'+ length).on('click keyup',function() { 
                    $('#property_'+ length).hide();
                })
                
            })

       /* $('#unit_price,#quantity').on('click keyup',function() {
            
            var totalfirstUnit   = $('#unit_price').val();
            var totalfirstQuenty = $('#quantity').val();

            var totalPriceFirst  = totalfirstUnit * totalfirstQuenty;
            $('#total_price').val(totalPriceFirst);
        });*/

        $('#category_id' ).on('click',function() {
            
            var categoryId = $( this).val();

            var value = '?categoryId=' + categoryId;  
           
            $.ajax({
                type: "GET",
                url: basePath + 'product/category/show' + value
            }).success(function ( result ) {

                $('#product_id' ).html( result );
                $('#loader').slideUp(200,function(){        
                    $('#loader').remove();
                });
                $(".loader").fadeOut("slow"); 

            }).error(function ( result ) {

                console.log('Information not found');

            }); 
        });



    });
</script>