@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Current Stock  </a></li>
    </ul>
</div>
@include('modules.inventory.master.navigation')
@include('modules.inventory.master.error')
<div id="main-wrapper">

	<div class="col0-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
				<h2 class="title-house"> Current Stock</h2>
				 	<table class="table table-hover table-bordered display" id="tableresponsive" style="width: 100%; cellspacing: 0;">
		    			<thead class="table-header-bg">
	                        <tr>
	                            <th>S/N</th>
                                <th>Category</th>
                                <th>Product ID</th>
                                <th>Product Name</th>
                                <th>Product Details</th>
	                            <th>Stock Qty</th>
	                            <th>Last Update</th>
	                        </tr>
                        </thead>
                        <tbody>
                        <?php $sn=1; ?>
                        @foreach($currentStock as $currentStock)
                            <tr class="odd gradeX">
                                <td>{{ $sn++ }}
                                </td>
                                <td>{{ $currentStock->category_name }}
                                </td>
                                <td>{{ $currentStock->product_id}}</td>
                                <td>{{ $currentStock->product_name}}</td>
                                <td>{{ $currentStock->product_details}}</td>
                                <td>{{ $currentStock->stock_qty}}</td>
                                <td>{{ $currentStock->stockupdate_date }}</td>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
			</div>
		</div>
	</div>
</div>
@stop