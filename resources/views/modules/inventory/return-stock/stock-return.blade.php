@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Stock Return  Approval  </a></li>
    </ul>
</div>
@include('modules.inventory.master.navigation')
@include('modules.inventory.master.error')
<div id="main-wrapper">

	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
				<h2 class="title-house"> Stock Return Approve Products</h2>
			 	{{ csrf_field() }}
                <table class="table table-hover table-bordered display" id="tableresponsive" style="width: 100%; cellspacing: 0;">
	    			<thead class="table-header-bg">
                        <tr>
                            <th>S/N</th>
                            <th>Product Name</th>
                            <th>Return By</th>
                            <th>Reason</th>
                            <th>Return Quantity </th>
                            <th>status</th>
                            <th >Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $sn=1; ?>
                    @foreach($transactions as $transaction)
                        <tr class="odd gradeX" id="product_{{  $transaction->transactionID }}">
                            <td>{{ $sn++ }} </td>
                            <td>{{ $transaction->product_name }} </td>
                            <td>{{ $transaction->fullName}}</td>
                            <td>{{ $transaction->receive_remarks }}</td>
                            <td>{{ $transaction->quantity  }}</td>
                            @if($transaction->approval_status == 0) 
                                <td>
                                    <span class="btn btn-warning btn-xs">Not Approve Yet</span>
                                </td>
                                <td>
                                    <span class="button-apporved">
                                        <button class="btn btn-success btn-xs status {{ $transaction->id }}approved" href="javascript:;" onclick="approvedDisapprovedProduct('{{ $transaction->product_id }}','{{ $transaction->quantity  }}','{{ $transaction->product_name  }}','1','{{ $transaction->transactionID  }}','1')">
                                            <i class="glyphicon glyphicon-check"></i>
                                            Approve Now
                                        </button>
                                       
                                        <a class="btn btn-info btn-xs status {{ $transaction->id }}approved"   data-toggle="modal" data-target="#editModal_{{ $transaction->storeid}}">
                                            Edit<i class="fa fa-edit"></i>
                                        </a>
                                        
                                        <button class="btn btn-danger btn-xs status {{ $transaction->id }}approved" href="javascript:;" onclick="approvedDisapprovedProduct('{{ $transaction->product_id }}','{{ $transaction->quantity  }}','{{ $transaction->product_name  }}','2','{{ $transaction->transactionID  }}')">
                                            <i class="glyphicon glyphicon-check"></i>
                                            Disapprove
                                        </button>
                                    </span>
                                </td>   
                            @elseif($transaction->approval_status == 1)
                                <td>
                                    <span class="btn btn-success btn-xs">Approved</span>
                                </td>
                                <td></td>
                            @elseif($transaction->approval_status == 2)
                                <td>
                                    <span class="btn btn-danger btn-xs">Disapproved</span>
                                </td>  
                                <td></td>                                       
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
			</div>
		</div>
	</div>
</div>
@foreach($transactions as $transaction)
<div id="editModal_{{ $transaction->storeid}}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-times" aria-hidden="true"></i> </button>
                <h4 class="modal-title">Updated Product Information </h4>
            </div>
            <div class="modal-body modal-inventory" id="info">
                {!! Form::open(['url'=>'update/storesdetails/products/'.$transaction->storeid, 'class' =>'form col-md-12', 'enctype'=>'multipart/form-data','method'=>'PUT']) !!}
                <div class="form-gorups listproducts" >
                    <select class="form-control nameId" name="product_id" id="product_id">
                        <option value="">Select Product</option>
                        @foreach($productList as $product)
                            <option value="{{ $product->product_id }}" @if($transaction->product_id == $product->product_id ) selected @endif >
                            {{ $product->product_name }}</option>
                        @endforeach
                    </select>
                    
                </div>

                <div class="form-gorups listproducts">
                    <input type="text" value="{{ $transaction->quantity}}" class="form-control quantityEdit" id="quantityEdit_{{$transaction->storeid}}" placeholder='Quantity' name="quantity">
                </div>
               
                <button type="button" data-dismiss="modal" class="btn default">Cancel</button>
                <button type="submit"  class="btn btn-success" > Update</button>
            {!! Form::close() !!}
            </div>
            
        </div>
   </div>
</div>
@endforeach
@include('modules.inventory.master.approved')
@stop