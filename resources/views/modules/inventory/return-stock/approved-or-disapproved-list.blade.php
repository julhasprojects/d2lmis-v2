@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Stock Return  Approval  </a></li>
    </ul>
</div>
@include('modules.inventory.master.navigation')
@include('modules.inventory.master.error')
<div id="main-wrapper">

	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
				<h2 class="title-house"> Stock Return Approve Products</h2>
			 	{{ csrf_field() }}
                <table class="table table-hover table-bordered display" id="tableresponsive" style="width: 100%; cellspacing: 0;">
	    			<thead class="table-header-bg">
                        <tr>
                            <th>S/N</th>
                            <th>Product Name</th>
                            <th>Return By</th>
                            <th>Reason</th>
                            <th>Return Quantity </th>
                            <th>status</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $sn=1; ?>
                    @foreach($transactions as $transaction)
                        <tr class="odd gradeX" id="product_{{ $transaction->transactionID }}">
                            <td>{{ $sn++ }} </td>
                            <td>{{ $transaction->product_name }} </td>
                            <td>{{ $transaction->fullName}}</td>
                            <td>{{ $transaction->receive_remarks }}</td>
                            <td>{{ $transaction->quantity  }}</td>
                           
                             
                            @if($transaction->approval_status == 1)
                                <td>
                                    <span class="btn btn-success btn-xs">Approved</span>
                                </td>
                              
                            @elseif($transaction->approval_status == 2)
                                <td>
                                    <span class="btn btn-danger btn-xs">Disapproved</span>
                                    <span class="btn btn-success btn-xs" onclick="editReantry('{{ $transaction->product_id }}','{{ $transaction->quantity  }}','{{ $transaction->product_name  }}','0','{{ $transaction->transactionID  }}')">Reantary</span>
                                </td>  
                                                                 
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
			</div>
		</div>
	</div>
</div>

@include('modules.inventory.master.approved')
@stop