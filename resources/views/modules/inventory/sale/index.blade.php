@extends( 'backend.index' )
@section( 'content_area' )

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);">  Stock Return  </a></li>
    </ul>
</div>
@include('modules.inventory.master.navigation')
@include('modules.inventory.master.error')
@include('master.message')
<div id="main-wrapper">
    <div class="col-md-12 colum-design-para">
        <div class="panel panel-white">
            <div class="panel panel-body">
                <h2 class="title-house">Sales Reports</h2>
               
                <table class="table table-hover table-bordered display" id="tableresponsive" style="width: 100%; cellspacing: 0;">
                    <thead class="table-header-bg">
                        <tr>
                            <th>Sl.</th>
                            <th>Category</th>
                            <th>Product Name</th>
                            <th>Customer Name</th>
                            <th>Phone</th>
                            <th>Qty</th>
                            <th>Discount</th>
                            <th>Adj.</th>
                            <th>Vat.</th>
                            <th>T Amt.</th>
                            <th>P. Amt.</th>
                            <th>c. Amt.</th>
                            <th>D. Amt.</th>
                            <th>Sale Date</th>
                            <th>Pay Date</th>
                        </tr>
                    </thead>
                    <tbody class="controls">
                        <?php 
                            $sl = 1;
                            $totalQty = $totalDiscount = $totalAdjustment = $totalVat = $totalAmount =$totalPaymount =$totlChangeAmount=$totalDueAmount = 0;
                        ?>
            
                        @foreach($saleInfoList as $info)
                        <?php 
                            $totalQty        += $info->qty;
                            $totalDiscount   += $info->discount;
                            $totalAdjustment += $info->adjustment;
                            $totalVat        += $info->vat;
                            $totalAmount     += $info->total_amount;
                            $totalPaymount   += $info->pay_amount;
                            $totlChangeAmount   += $info->charge_amount;
                            $totalDueAmount   += $info->due_amount;
                        ?>
                        <tr >
                            <td>
                               {{ $sl++}}
                            </td>
                            <td>
                                {{ $info->category_name}}
                            </td>
                            <td>
                                {{ $info->product_name}}
                            </td>
                             <td>
                                {{ $info->name}}
                            </td>
                             <td>
                                {{ $info->phone_number}}
                            </td>
                            <td>
                                {{ $info->qty}}
                            </td>
                            <td>
                                {{ $info->discount}}
                            </td>
                            <td>
                                {{ $info->adjustment}}
                            </td>
                            <td>
                                {{ $info->vat}}
                            </td>
                            <td>
                                {{ $info->total_amount}}
                            </td>
                            <td>
                                {{ $info->pay_amount}}
                            </td>
                            <td>
                                {{ $info->charge_amount}}
                            </td>
                            <td>
                                {{ $info->due_amount}}
                            </td>
                            <td>
                                {{ $info->sale_date}}
                            </td>
                            <td>
                                {{ $info->pay_date}}
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="3"></td>
                            <td colspan="2"> Sub Total</td>
                            <td> {{ $totalQty }}</td>
                            <td> {{ $totalDiscount }}</td>
                            <td> {{ $totalAdjustment }}</td>
                            <td> {{ $totalVat }}</td>
                            <td> {{ $totalAmount }}</td>
                            <td> {{ $totalPaymount }}</td>
                            <td> {{ $totlChangeAmount }}</td>
                            <td> {{ $totalDueAmount }}</td>
                            <td colspan="2"> </td>
                        </tr>
                        <tr>
                            
                            <td colspan="6"></td>
                            <td>  Total</td>
                            <td> {{ $totalAmount }}</td>
                            <td> {{ $totalPaymount }}</td>
                            
                            <td colspan="4"> </td>
                        </tr>
                    </tbody>
                </table>
             
             
            </div>
        </div>
    </div>

</div>
@stop