<style>
    .modal-backdrop {
        z-index: 0;
    }
    .modal-title{
        font-size: 23px;
        line-height: 0.429;
        margin: 21px 0 0 175px;
    }
</style>
<div class="page-sidebar sidebar forntend-bg">
    <div class="page-sidebar-inner slimscroll">
        <div class="sidebar-header forntend-bg">
            <div class="sidebar-profile">
                <a href="#" id="profile-menu-link-remove">
                    <div class="sidebar-profile-image">
                        <img src="{{ URL::asset( 'assets/images/logo.png' ) }}" class="img-circle img-responsive" alt="">
                    </div>
                    <div class="sidebar-profile-details">
                        <span>Mazeda Cyber Cafe <br> <!-- <small>Project Director</small> --> </span>
                    </div>
                </a>
            </div>
        </div>
        <ul class="menu accordion-menu forntend-bg">
            <li class="active forntend-bg"><a href="{{ URL::to( '/front-panel' ) }}" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-dashboard"></span><p>Dashboard</p></a></li>
            <li><a href="#"data-target=".newline-request-modal-lg" data-toggle="modal" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-earphone"></span><p>New Line Request</p></a></li>
            <li><a href="#"data-target=".newhouse-change-request-modal-lg" data-toggle="modal" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-home"></span><p>House Change Request</p></a></li>

            <li><a href="#"data-target=".package-change-request-modal-lg" data-toggle="modal" class="waves-effect waves-button"><span class="menu-icon fa fa-wifi"></span><p>Package Change Request</p></a></li>

           <li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-book"></span><p>Request Management</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="{{ URL::to( '/newline-phone-request' ) }}">New Line Request</a></li>
                    <li><a href="{{ URL::to( '/package-change-request' ) }}">Package Change Request</a></li>
                    <li><a href="{{ URL::to( '/house-change-request' ) }}">House Shift Request</a></li>
                </ul>
            </li>
            <li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-list-alt"></span><p>Line Management</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="{{ URL::to( '/new-line-request-frontpanel' ) }}">New Line Connection</a></li> 
                    <!--<li><a href="{{ URL::to( '/setup-new-line' ) }}">New Line Setup</a></li>
                    <li><a href="{{ URL::to( '/client-agreement' ) }}">Agreement Form</a></li> -->
                    <!-- <li><a href="{{ URL::to( '/line-disconnect' ) }}">Line Disconnect</a></li> -->
                    <li><a href="#" data-target=".line-disconect-request-modal-lg" data-toggle="modal">Line Disconnect</a></li>
                </ul>
            </li>
            <!--
            <li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-briefcase"></span><p>Employee Mgt.</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="http://localhost/HRM/admin/employees/create" target="_BLANK">Add New Employee</a></li>
                    <li><a href="http://localhost/HRM/admin/employees" target="_BLANK">View All Employees</a></li>
                </ul>
            </li>
            -->
            <!--
            <li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-th"></span><p>Bill Management</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="{{ URL::to( '/bill-generate' ) }}">Bill Generate </a></li>
                </ul>
            </li>
            -->
            <!--
            <li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-th"></span><p>Package Info Setup</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="{{ URL::to( '/package-create' ) }}">New Package Setup</a></li>
                    <li><a href="{{ URL::to( '/bill-generate' ) }}">View Package</a></li>
                </ul>
            </li>
            -->
            <li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-list"></span><p>Complain Mgt.</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <!-- <li><a href="{{ URL::to( '/create-client-complain' ) }}">New Complain</a></li> -->
                    <li><a href="#" data-target=".client-complain-request-modal-lg" data-toggle="modal">New Complain</a></li>
                    <li><a href="{{ URL::to( '/view-client-complain' ) }}">View All Complains</a></li>
                </ul>
            </li>

        </ul>
    </div><!-- Page Sidebar Inner -->
</div><!-- Page Sidebar -->


            