<!--**************************************************/
*** @Author: JBRSOFT Development Team               *** 
*** @Version: ISPERP-V1.1                           ***
*** @Since: 2015                                    ***
*** @URL  : www.isperp.org                          ***
*** @Company Website: www.jbrsoft.com               *** 
*** @Office Email: jbrsoft10@gmail.com              *** 
*** @Copyright: 2017@JBRSOFT                        ***
/****************************************************-->
@include( 'master.login-header' ) 
@section( 'content_area' )

<img src="d2lmis-logo.png" class="d2lmis-logo-position" height="200" width="400" alt="D2LMIS" />
<div class="container-login">
    <input type="checkbox" id="switch">
    <label for="switch">
      <b class="top"></b>
      <b class="bottom"></b>
    </label>
  <div class="login-box">
    <h1>D2LMIS Login</h1>
   {!! Form::open(array('url'=>'login/custom','role'=>'form','method'=>'POST'))!!}
                        
      <p>
        <label for="user">Username</label>
        <input type="text" class="form-control" name="email" placeholder="Email" value="{{ Input::old('email') }}" required>
      </p>
      <p>
        <label for="pass">Password</label>
         <input type="password" class="form-control" name="password" placeholder="Password" required>
      </p>
      <p>
        <input type="submit" class="btn-block" value="Login">
      </p>
   {!!Form::close()!!}      
  </div>   
</div>
<button class="btn btn-info dhis2-login-button" data-toggle="modal" data-target="#loginModal">
  <img src="dhis2-logo.png" class="d2lmis-logo-position" height="30" width="70" alt="D2LMIS" />
      Login Using DHIS2!
</button>
<div id="loginModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <div class="wrapper fadeInDown">
          <div id="formContent">
            <!-- Tabs Titles -->

            <!-- Icon -->
            <div class="fadeIn first">
              <img src="assets/images/dhis2-logo-new.png" alt="DHIS2" />
            </div>

            <!-- Login Form -->
              <input type="text" id="username" class="fadeIn second" name="login" placeholder="Username">
              <input type="text" id="password" class="fadeIn third" name="login" placeholder="Password">
              <input type="submit" id="dhis2login" class="fadeIn fourth" value="Sign In"> 
              <span id="loader"></span>

            <!-- Remind Passowrd -->
            <div id="formFooter">
              <a class="underlineHover" href="#">Forgot Password?</a>
            </div>

          </div>
        </div>

  </div>
</div>
     
