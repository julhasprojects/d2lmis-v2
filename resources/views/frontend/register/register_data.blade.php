@include( 'master.admin_header' ) 
@section( 'content_area' )
<div id="main-wrapper">
<h3 class="btn-success" style="text-align:center; color:#EC3C40;">
    {!! Session::get( 'message' ) !!}   
    
</h3>
    <div class="row">
        <div class="col-md-3 center">
            <div class="login-box">
                <a href="index-2.html" class="logo-name text-lg text-center">MAZEDA Cabling</a>
                <p class="text-center m-t-md">Create a account</p>
                {!! Form::open(array('url'=>'add-register','role'=>'form','method'=>'POST'))!!}
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Name" name="name" required>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                    </div>
                    <label>
                        <input type="checkbox" required> Agree the terms and policy
                    </label>
                    <button type="submit" class="btn btn-success btn-block m-t-xs">Submit</button>
                    <p class="text-center m-t-xs text-sm">Already have an account?</p>
                    <a href="{{url::to('/')}}" class="btn btn-default btn-block m-t-xs">Login</a>
                {!!Form::close()!!}
                <p class="text-center m-t-xs text-sm">2015 &copy; by Steelcoders.</p>
            </div>
        </div>
    </div><!-- Row -->
</div><!-- Main Wrapper -->
@include( 'master.footer_area')
            