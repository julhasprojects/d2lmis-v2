    <!-- Javascripts -->
    
   
    <script src="<?php echo e(URL::asset( 'assets/plugins/jquery-ui/jquery-ui.min.js' )); ?>"></script>
    <!-- <script src="<?php echo e(URL::asset( 'assets/plugins/pace-master/pace.min.js' )); ?>"></script> -->
    <script src="<?php echo e(URL::asset( 'assets/plugins/jquery-blockui/jquery.blockui.js' )); ?>"></script>
     <!-- Datepicker-->
    <script src="<?php echo e(URL::asset( 'assets/plugins/bootstrap/js/bootstrap.min.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/js/bootstrap-datepicker.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/plugins/switchery/switchery.min.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/plugins/offcanvasmenueffects/js/classie.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/plugins/offcanvasmenueffects/js/main.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/plugins/waves/waves.min.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/plugins/3d-bold-navigation/js/main.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/plugins/waypoints/jquery.waypoints.min.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/plugins/jquery-counterup/jquery.counterup.min.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/plugins/toastr/toastr.min.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/plugins/flot/jquery.flot.min.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/plugins/flot/jquery.flot.time.min.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/plugins/flot/jquery.flot.symbol.min.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/plugins/flot/jquery.flot.resize.min.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/plugins/flot/jquery.flot.tooltip.min.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/plugins/curvedlines/curvedLines.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/plugins/metrojs/MetroJs.min.js' )); ?>"></script>
    
    <!-- Ajax Request -->
    <script src="<?php echo e(URL::asset( 'assets/js/backend/ajaxRequest.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/js/backend/backendReport.js' )); ?>"></script>
    
    <script src="<?php echo e(URL::asset( 'assets/js/function.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/js/frontend/frontajaxRequest.js' )); ?>"></script>
    
    <script src="<?php echo e(URL::asset( 'assets/js/inventory/inventory.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'hrm/hrm-js.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/js/datepicker.js' )); ?>"></script>
    <!-- Modal/Popup-->
    <script src="<?php echo e(URL::asset( 'assets/js/classie.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/js/modalEffects.js' )); ?>"></script>

    <script src="<?php echo e(URL::asset( 'assets/js/modern.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/js/pages/dashboard.js' )); ?>"></script>

    
    <!-- Sweet Alert Library for Validation -->
    <script src="<?php echo e(URL::asset( 'assets/js/sweetalert/sweetalert.min.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/js/sweetalert/sweetalert-dev.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'hrm/metronic.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'hrm/layout.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'hrm/bootstrap-fileinput.js' )); ?>"></script>
    <!-- Bootstrap Jquery Validator -->
    <script src="<?php echo e(URL::asset( 'assets/js/validator.js' )); ?>"></script>
   
    <script src="<?php echo e(URL::asset( 'assets/js/chosen.jquery.js' )); ?>"></script>
    <script src="<?php echo e(URL::asset( 'assets/js/prism.js' )); ?>"></script>
    <script type="text/javascript">
        
         var config = {
                '.chosen-select'           : {},
                '.chosen-select-deselect'  : {allow_single_deselect:true},
                '.chosen-select-no-single' : {disable_search_threshold:10},
                '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                '.chosen-select-width'     : {width:"95%"}
            }

            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }

    </script>