<?php $__env->startSection( 'content_area' ); ?>

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Approved Products </a></li>
    </ul>
</div>
<?php echo $__env->make('modules.inventory.master.navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('modules.inventory.master.error', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div id="main-wrapper">

	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
				<h2 class="title-house"> All Approved Products</h2>
			 	<?php echo e(csrf_field()); ?>

                <div class="table-responsive">
                    <table class="table table-hover table-bordered display" id="tableresponsive" style="width: 100%; cellspacing: 0;">
    	    			<thead class="table-header-bg">
                            <tr>
                                <th>S/N</th>
                                <th>Product Name</th>
                                <th>Recived By</th>
                                <th>Unit Price(Tk)</th>
                                <th>Received Qty</th>
                                <th>Total Amount</th>
                                <th> Warranty Period</th>
                                <th> Comments</th>
                                <th> Transaction Date</th>	                         
                                <th>Approved Date</th>
                                <th>Approved By</th>
                                <th>Status</th>
                         
                            </tr>
                        </thead>
                        <tbody>
                        <?php $sn=1; ?>
                        <?php $__currentLoopData = $transactions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $transaction): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="odd gradeX" id="product_<?php echo e($transaction->transactionID); ?>">
                                <td><?php echo e($sn++); ?> </td>
                                <td><?php echo e($transaction->product_name); ?> </td>
                                <td><?php echo e($transaction->fullName); ?> </td>
                                <td><?php echo e($transaction->unit_price); ?></td>
                                <td><?php echo e($transaction->quantity); ?></td>
                                <td><?php echo e($transaction->total_price); ?></td>
                                <td><?php echo e($transaction->warranty_period); ?></td>
                                <td><?php echo e($transaction->comments); ?></td>
                                <td><?php echo e($transaction->created_at); ?></td>

                                <td>
                                    <?php if($transaction->approval_status  == 1): ?>
                                        <?php echo e($transaction->updated_at); ?>

                                    <?php else: ?>
                                        
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php $__currentLoopData = $allEmployer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if( $info->employeeID == $transaction->approved_by): ?>
                                            <?php echo e($info->fullName); ?>

                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </td>
                                <?php if($transaction->approval_status == 0): ?> 
                                    <td>
                                        <span class="btn btn-warning btn-xs">Not Approve Yet</span>
                                    </td>
                                    <td>
                                        <span class="button-apporved">
                                            <button class="btn btn-success btn-xs status <?php echo e($transaction->id); ?>approved" href="javascript:;" onclick="approvedDisapprovedProduct('<?php echo e($transaction->product_id); ?>','<?php echo e($transaction->quantity); ?>','<?php echo e($transaction->product_name); ?>','1','<?php echo e($transaction->transactionID); ?>')">
                                                <i class="glyphicon glyphicon-check"></i>
                                                Approve Now
                                            </button>
                                           
                                            <a class="btn btn-info btn-xs status <?php echo e($transaction->id); ?>approved"  onclick="editProductInformation('<?php echo e($transaction->product_id); ?>','<?php echo e($transaction->quantity); ?>','<?php echo e($transaction->product_name); ?>','2','<?php echo e($transaction->transactionID); ?>')">
                                                Edit<i class="fa fa-edit"></i>
                                            </a>
                                            
                                            <button class="btn btn-danger btn-xs status <?php echo e($transaction->id); ?>approved" href="javascript:;" onclick="approvedDisapprovedProduct('<?php echo e($transaction->product_id); ?>','<?php echo e($transaction->quantity); ?>','<?php echo e($transaction->product_name); ?>','2','<?php echo e($transaction->transactionID); ?>')">
                                                <i class="glyphicon glyphicon-check"></i>
                                                Disapprove
                                            </button>
                                        </span>
                                    </td>   
                                <?php elseif($transaction->approval_status == 1): ?>
                                    <td>
                                        <span class="btn btn-success btn-xs">Approved</span>
                                    </td>
                                    <td></td>
                                <?php elseif($transaction->approval_status == 2): ?>
                                    <td>
                                        <span class="btn btn-danger btn-xs">Disapproved</span>
                                    </td>  
                                    <td></td>                                       
                                <?php endif; ?>
                             
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
			</div>
		</div>
	</div>
</div>
<?php echo $__env->make('modules.inventory.master.master-edit', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('modules.inventory.master.approved', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make( 'backend.index' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>