<?php echo $__env->make( 'master.admin_header' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<main class="page-content content-wrap">
    <?php echo $__env->make( 'backend.usermenu.main-navigation' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>     
    <?php echo $__env->make('backend.left_sidebar' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="page-inner">
        <?php echo $__env->yieldContent( 'content_area' ); ?>
    </div>
</main>
<?php echo $__env->make( 'master.third_party_library' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>