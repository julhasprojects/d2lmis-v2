<?php $__env->startSection( 'content_area' ); ?>

<div id="ramadanmenu" class="menu-menu-1-container bottom-menubar">
    <div id="menu-button">Menu</div>
    <ul id="menu-menu-1" class="menu">
        <li class="hover hrm-main-menubar">
            <a href="#">
                <span>
                    <i class="fa fa-users"></i>
                    <span class="title">Supply Tracking<i class="fa fa-angle-down"></i> </span> 
                </span>
           </a>
           <ul class="sub-menu">
                <div class="toltip"></div>
                <li class="sub-item">
                    <a href="<?php echo e(URL::to( '/receive' )); ?>">
                        <i class="fa fa-users"></i>
                        Supply flow from Central Stock
                    </a>
                </li>
                <li class="sub-item">
                    <a href="<?php echo e(URL::to( '/receive/show' )); ?>">
                        <i class="fa fa-briefcase"></i>
                       Supply at differnet facility level
                    </a>
                </li>
                <li class="sub-item">
                    <a href="<?php echo e(URL::to( '/receive/approved/products' )); ?>">
                        <i class="fa fa-briefcase"></i>
                        Supply from DRS
                    </a>
                </li>
                <li class="sub-item">
                    <a href="<?php echo e(URL::to( '/receive/disapproved/products' )); ?>">
                        <i class="fa fa-briefcase"></i>
                        Supply from other sources
                    </a>
                </li>
            </ul>
        </li>
        <li class="hover hrm-main-menubar">
            <a href="#" class="dropdown-hover">
                <span><i class="fa fa-money"></i>
                   <span class="title">Receive Summary <i class="fa fa-angle-down"></i></span> 
               </span>
           </a>
             <ul class="sub-menu">
                <div class="toltip"></div>
                <li class="sub-item">
                    <a href="<?php echo e(URL::to( 'create-new-requisition' )); ?>">
                        <i class="fa fa-money"></i>
                        National Receive
                    </a>
                </li>
                <li class="sub-item">
                    <a href="<?php echo e(URL::to( '/requisition/approval' )); ?>">
                        <i class="fa fa-money"></i>
                        District Level Receive                      
                    </a>
                </li>
                <li class="sub-item">
                    <a href="<?php echo e(URL::to( '/requisition/approved/all' )); ?>">
                        <i class="fa fa-money"></i>
                         Upazila Receive                       
                    </a>
                </li>
                <li class="sub-item">
                    <a href="<?php echo e(URL::to( '/requisition/disapproved/all' )); ?>">
                        <i class="fa fa-money"></i>
                        Union Level Receive                      
                    </a>
                </li>
                <li class="sub-item">
                    <a href="<?php echo e(URL::to( '/requisition/disapproved/all' )); ?>">
                        <i class="fa fa-money"></i>
                        Community Level Receive                      
                    </a>
                </li>
             
            </ul> 
        </li>
        <li class="hover hrm-main-menubar">
           <a href="#" class="dropdown-hover">
                <span>
                 <i class="fa fa-trophy"></i> &nbsp;Consumptions <i class="fa fa-angle-down"></i>
                </span>
            </a>
            <ul class="sub-menu">
                <div class="toltip"></div>
                
                <li class="sub-item">
                    <a href="<?php echo e(URL::to( 'current-stock' )); ?>">
                        <i class="fa fa-gift"></i>
                        Consumption Trend District Wise
                    </a>
                </li>
                <li class="sub-item">
                    <a href="<?php echo e(URL::to( 'stock/return' )); ?>">
                        <i class="fa fa-gift"></i>
                       Consumption Trend Facility Level
                    </a>
                </li>
                <li class="sub-item">
                    <a href="<?php echo e(URL::to( 'stock/return/show' )); ?>">
                        <i class="fa fa-gift"></i>
                       Average Monthly Consumption (AMC) by Facility
                    </a>
                </li>

                <li class="sub-item">
                    <a href="<?php echo e(URL::to( 'stocks/return/approved' )); ?>">
                        <i class="fa fa-gift"></i>
                        Average Monthly Distribution (AMD) by Facility
                    </a>
                </li>
                <li class="sub-item">
                    <a href="<?php echo e(URL::to( 'stocks/return/disapproved' )); ?>">
                        <i class="fa fa-gift"></i>
                       Rational Distribution
                    </a>
                </li>
            </ul> 
            
            <li class="hover hrm-main-menubar">
                <a href="#" class="dropdown-hover">
                    <span><i class="fa fa-cogs"></i>
                       <span class="title">Stock<i class="fa fa-angle-down"></i>
                    </span> 

                   </span>
               </a>
                 <ul class="sub-menu">
                    <div class="toltip"></div>
                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( '/new/product/setup' )); ?>">
                            National Stock
                        </a>
                    </li>
                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( '/product/category' )); ?>">
                            Monthly Stock
                        </a>
                    </li>
                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( '/product/unit' )); ?>">
                            Managers Stock Summary
                        </a>
                    </li>
                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( '/manufacturer' )); ?>">
                            Warehouse Stock Summary
                        </a>
                    </li>
                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( '/transaction/type' )); ?>">
                            Stock Transactions
                        </a>
                    </li>
                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( '/supplier' )); ?>">
                            Month of Stock (MoS) by Facility
                        </a>
                    </li>
                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( '/supplier' )); ?>">
                            Avaialability by Facility
                        </a>
                    </li>
                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( '/supplier' )); ?>">
                            Avaialability by Medicines
                        </a>
                    </li>
                 
                </ul> 
            </li>
            <li class="hover hrm-main-menubar">
                <a href="#" class="dropdown-hover">
                    <span>
                     <i class="fa fa-bar-chart"></i> Stock Out<i class="fa fa-angle-down"></i>
                    </span>
                </a>
                <ul class="sub-menu">
                    <div class="toltip"></div>
                    
                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( 'master/transaction' )); ?>">
                            <i class="fa fa-gift"></i>
                           Stock out medicines
                        </a>
                    </li>

                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( 'master/summary/reports' )); ?>">
                            <i class="fa fa-gift"></i>
                           Stock out facilities
                        </a>
                    </li>

                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( 'new/product/setup/show' )); ?>">
                            <i class="fa fa-gift"></i>
                          Potential Stockout
                        </a>
                    </li>
                </ul> 
            </li>

            <li class="hover hrm-main-menubar">
                <a href="#" class="dropdown-hover">
                    <span>
                     <i class="fa fa-bar-chart"></i> Expiry Tracking <i class="fa fa-angle-down"></i>
                    </span>
                </a>
                <ul class="sub-menu">
                    <div class="toltip"></div>
                    
                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( 'master/transaction' )); ?>">
                            <i class="fa fa-gift"></i>
                            Expired Medicines
                        </a>
                    </li>

                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( 'master/summary/reports' )); ?>">
                            <i class="fa fa-gift"></i>
                            Expired Medicines by Facility
                        </a>
                    </li>

                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( 'new/product/setup/show' )); ?>">
                            <i class="fa fa-gift"></i>
                            Nearest Expiry
                        </a>
                    </li>
                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( 'product/category/shows' )); ?>">
                            <i class="fa fa-gift"></i>
                           Lot Based Expiry
                        </a>
                    </li>
                </ul> 
            </li>

            
            <li class="hover hrm-main-menubar">
                <a href="#" class="dropdown-hover">
                    <span>
                     <i class="fa fa-bar-chart"></i> Reporting Rate <i class="fa fa-angle-down"></i>
                    </span>
                </a>
                <ul class="sub-menu">
                    <div class="toltip"></div>
                    
                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( 'master/transaction' )); ?>">
                            <i class="fa fa-gift"></i>
                            Expired Medicines
                        </a>
                    </li>

                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( 'master/summary/reports' )); ?>">
                            <i class="fa fa-gift"></i>
                            Expired Medicines by Facility
                        </a>
                    </li>

                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( 'new/product/setup/show' )); ?>">
                            <i class="fa fa-gift"></i>
                            Nearest Expiry
                        </a>
                    </li>
                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( 'product/category/shows' )); ?>">
                            <i class="fa fa-gift"></i>
                           Lot Based Expiry
                        </a>
                    </li>
                </ul> 
            </li>
            <li class="hover hrm-main-menubar">
                <a href="#" class="dropdown-hover">
                    <span>
                     <i class="fa fa-bar-chart"></i> D2LMIS Data <i class="fa fa-angle-down"></i>
                    </span>
                </a>
                <ul class="sub-menu">
                    <div class="toltip"></div>
                    
                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( 'master/transaction' )); ?>">
                            <i class="fa fa-gift"></i>
                           District Informaiton
                        </a>
                    </li>

                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( 'master/summary/reports' )); ?>">
                            <i class="fa fa-gift"></i>
                            Upazila Information
                        </a>
                    </li>

                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( 'new/product/setup/show' )); ?>">
                            <i class="fa fa-gift"></i>
                            Union Information
                        </a>
                    </li>
                    <li class="sub-item">
                        <a href="<?php echo e(URL::to( 'product/category/shows' )); ?>">
                            <i class="fa fa-gift"></i>
                           Communinty Information
                        </a>
                    </li>
                </ul> 
            </li>
        
    </ul>
</div>  
<!-- left content bar -->
<div class="col-md-12 col-xs-12 col-lg-12 mobile-padding-left-zero mobile-padding-right-zero">
	<div class="col-md-12 col-xs-12 col-lg-12">
		<div class="widget">
			<div class="widget-title">
				<h4><i class="fa fa-tags tags-icon"></i>  Reporting Rate District Wise by Month by Year</h4>
				<span class="tools">
					<a href="javascript:;" class="fa fa-chevron-down"></a>
					<a href="javascript:;" class="fa fa-remove"></a>
				</span>
			</div>
            <div class="widget-body">
                <div class="btn-group top-button-group">
                    <select class="selectpicker select-form-desing select-bar pull-left col-md-2 col-xs-2 " id="monthlyGeneratedBar">
                        <option value="column">Month</option>
                    </select>

                    <select class="chosen-select1 btn btn-default dropdown-toggle  col-md-2 col-xs-2 " tabindex="2" name="" id="yearlyGenerated" >
                        <option value="">Select</option>
                        <?php for($year = 2016; $year < date("Y")+1; $year++): ?>
                            <option value='<?php echo e($year); ?>'> <?php echo e($year); ?> </option>";
                        <?php endfor; ?>
                    </select>
                    <button class="btn btn-primary" id="bar_drr" value="Bar"><i class="icon-align-left"></i> Bar</button>
                                <button class="btn btn-primary" id="column_drr" value="Column"><i class="icon-bar-chart"></i> Col</button>
                                <button class="btn btn-primary" id="line_drr" value="Line"><i class="icon-magic"></i> Line</button>
                </div>
                <div class="loader-position">
                    <div id="rr-district-wise"></div>
    			</div>  
            </div>
		</div>		
	</div>
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="widget">
            <div class="widget-title">
                <h4><i class="fa fa-tags tags-icon"></i>Stock Information</h4>
                <span class="tools">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-remove"></a>
                </span>
            </div>
            <div class="widget-body">
                <div class="btn-group top-button-group">
                    <select class="selectpicker select-form-desing select-bar pull-left col-md-2 col-xs-2 " id="monthlyGeneratedBar">
                        <option value="column">Month</option>
                    </select>

                    <select class="chosen-select1 btn btn-default dropdown-toggle  col-md-2 col-xs-2 " tabindex="2" name="" id="yearlyGenerated" >
                        <option value="">Select</option>
                        <?php for($year = 2016; $year < date("Y")+1; $year++): ?>
                            <option value='<?php echo e($year); ?>'> <?php echo e($year); ?> </option>";
                        <?php endfor; ?>
                    </select>
                    <button class="btn btn-primary" id="bar_drr" value="Bar"><i class="icon-align-left"></i> Bar</button>
                                <button class="btn btn-primary" id="column_drr" value="Column"><i class="icon-bar-chart"></i> Col</button>
                                <button class="btn btn-primary" id="line_drr" value="Line"><i class="icon-magic"></i> Line</button>
                </div>
                <div id="stockinformation" ></div>
            </div>
        </div>      
    </div>
	<div class="col-md-6 col-xs-12 col-lg-6">
		<div class="widget">
			<div class="widget-title">
				<h4><i class="fa fa-tags tags-icon"></i> Stock Status at Different Levels</h4>
				<span class="tools">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-remove"></a>
                </span>
			</div>
            <div class="widget-body">
                <div class="btn-group top-button-group">
                    <select class="selectpicker select-form-desing select-bar pull-left col-md-2 col-xs-2 " id="monthlyGeneratedBar">
                        <option value="column">Month</option>
                    </select>

                    <select class="chosen-select1 btn btn-default dropdown-toggle  col-md-2 col-xs-2 " tabindex="2" name="" id="yearlyGenerated" >
                        <option value="">Select</option>
                        <?php for($year = 2016; $year < date("Y")+1; $year++): ?>
                            <option value='<?php echo e($year); ?>'> <?php echo e($year); ?> </option>";
                        <?php endfor; ?>
                    </select>
                    <button class="btn btn-primary" id="bar_drr" value="Bar"><i class="icon-align-left"></i> Bar</button>
                                <button class="btn btn-primary" id="column_drr" value="Column"><i class="icon-bar-chart"></i> Col</button>
                                <button class="btn btn-primary" id="line_drr" value="Line"><i class="icon-magic"></i> Line</button>
                </div>
                <div id="stockstatusdiflevel" ></div>
            </div>
        </div>		
	</div>
    <div class="col-md-6 col-xs-12 col-lg-6">
        <div class="widget">
            <div class="widget-title">
                <h4><i class="fa fa-tags tags-icon"></i> Medicine Consumption Trend - Yearly</h4>
                <span class="tools">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-remove"></a>
                </span>
            </div>
            <div class="widget-body">
                <div class="btn-group top-button-group">
                    <select class="selectpicker select-form-desing select-bar pull-left col-md-2 col-xs-2 " id="monthlyGeneratedBar">
                        <option value="column">Month</option>
                    </select>

                    <select class="chosen-select1 btn btn-default dropdown-toggle  col-md-2 col-xs-2 " tabindex="2" name="" id="yearlyGenerated" >
                        <option value="">Select</option>
                        <?php for($year = 2016; $year < date("Y")+1; $year++): ?>
                            <option value='<?php echo e($year); ?>'> <?php echo e($year); ?> </option>";
                        <?php endfor; ?>
                    </select>
                    <button class="btn btn-primary" id="bar_drr" value="Bar"><i class="icon-align-left"></i> Bar</button>
                                <button class="btn btn-primary" id="column_drr" value="Column"><i class="icon-bar-chart"></i> Col</button>
                                <button class="btn btn-primary" id="line_drr" value="Line"><i class="icon-magic"></i> Line</button>
                </div>
                <div id="medicinesconsumptions" ></div>
            </div>
        </div>      
    </div>
    
	<div class="col-md-12 col-xs-12 col-lg-12">
		<div class="widget">
			<div class="widget-title">
				<h4><i class="fa fa-tags tags-icon"></i> District Wise Consumption</h4>
				<span class="tools">
					<a href="javascript:;" class="fa fa-chevron-down"></a>
					<a href="javascript:;" class="fa fa-remove"></a>
				</span>
			</div>
            <div class="widget-body">
                 <div class="btn-group top-button-group">
                    <select class="selectpicker select-form-desing select-bar pull-left col-md-2 col-xs-2 " id="monthlyGeneratedBar">
                        <option value="column">Month</option>
                    </select>

                    <select class="chosen-select1 btn btn-default dropdown-toggle  col-md-2 col-xs-2 " tabindex="2" name="" id="yearlyGenerated" >
                        <option value="">Select</option>
                        <?php for($year = 2016; $year < date("Y")+1; $year++): ?>
                            <option value='<?php echo e($year); ?>'> <?php echo e($year); ?> </option>";
                        <?php endfor; ?>
                    </select>
                    <button class="btn btn-primary" id="bar_drr" value="Bar"><i class="icon-align-left"></i> Bar</button>
                                <button class="btn btn-primary" id="column_drr" value="Column"><i class="icon-bar-chart"></i> Col</button>
                                <button class="btn btn-primary" id="line_drr" value="Line"><i class="icon-magic"></i> Line</button>
                </div>
                <div id="districtwiseconsumptionrate" ></div>
		    </div>
        </div>	
	</div>
</div> 

<?php echo $__env->make('backend.dashboard-js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make( 'backend.index' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>