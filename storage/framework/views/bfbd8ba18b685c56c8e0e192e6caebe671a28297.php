<?php $__env->startSection( 'content_area' ); ?>

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Upazila List</a></li>
    </ul>
</div>

<?php echo $__env->make('modules.inventory.master.navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('modules.inventory.master.error', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('master.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div id="deletenotification"></div>
<div id="main-wrapper">
	
	<div class="col-md-12 col-xs-12 col-lg-12  left-box"> 
		<div class="panel-white">
			<div class="panel-body">
				<?php echo e(csrf_field()); ?>

				<h2 class="title-house"> Upazila List </h2>
			 	<table class="table table-hover table-bordered display" id="tableresponsive" style="width: 100%; cellspacing: 0;">
				    <thead class="table-header-bg">
				        <tr>
				            <th>SN</th>
				            <th>Division Name</th>
				            <th>District Name</th>
				            <th>Upazila Name</th>
				            <th>upazila Code</th>
				            <th>Created</th>
				            <th>Action</th>
				        </tr>
				    </thead>
				    <tbody>
				    	<?php $sn =1 ?>
				    	<?php $__currentLoopData = $upazilaList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					    	<tr id="masterfacility_delete_<?php echo e($info->facility_id); ?>">
					    		<td> <?php echo e($sn++); ?> </td>
					    		<td> <?php echo e($info->division_id); ?> </td>
					    		<td> <?php echo e($info->district_id); ?> </td>
					    		<td> <?php echo e($info->upazila_name); ?> </td>
					    		<td> <?php echo e($info->upazila_id); ?> </td>
					    		<td> <?php echo e($info->created_at); ?> </td>
					    		<td>  
					    			<a class="btn btn-success btn-xs" href="<?php echo e(URL::to('/masterfacility/'.$info->manufact_id.'/edit')); ?>"  > <i class="fa fa-pencil"> </i></a>      
                                    <a class="btn btn-xs btn-danger" href="javascript:;" onclick="meanufacetDelete('<?php echo e($info->manufact_id); ?>','<?php echo e($info->manufact_name); ?>')"><i class="fa fa-trash"></i></a>
                                </td>
					    	</tr>
				    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				    </tbody>
			    </table>
	    	</div>
		</div>
	</div>
</div>
<?php echo $__env->make('modules.inventory.master.delete', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make( 'backend.index' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>