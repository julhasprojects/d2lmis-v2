<div class="left-sidebar print-none">
    <?php if(Gate::check('complain.notification') || Gate::check('nlrequest.notification') || Gate::check('mikrotiknotactive.notification') || Gate::check('houseshift.notification') || Gate::check('packagechange.notification') || Gate::check('linedisconnectrequest.notification') || Gate::check('reconnect.notification') || Gate::check('duebill.notification')): ?>
        <span class="left-sidbar-icon notification" onclick="notifecationLoadPage()"  data-toggle="modal" data-target="#notifection"> <img src="<?php echo e(URL::asset( 'assets/images/notification.png' )); ?>" class="img-responsive"> </span>
    <?php endif; ?>
    <?php if(Gate::check('sms.notification') && Gate::check('admin.access')): ?><a class="left-sidbar-icon notification" href="<?php echo e(URL::to( '/send-client-sms' )); ?>"> <img src="<?php echo e(URL::asset( 'assets/images/sms.ico' )); ?>" class="img-responsive"> </a><?php endif; ?>
    <?php if(Gate::check('sms.notification') && Gate::check('admin.access')): ?><a class="left-sidbar-icon notification" href="#"> <img src="<?php echo e(URL::asset( 'assets/images/email-notification.png' )); ?>" class="img-responsive"> </a><?php endif; ?>
</div>
<div id="notifection" class="modal fade" role="dialog">
    <div class="modal-dialog email-notifection">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> ISPERP Notification Center </h4>
            </div>
            <div class="modal-body" id="isperpNotify">
                <div id="loaderNotefecation"> </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Single Bill Generate Model -->
<div class="modal fade individual-billgenerate-request-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myLargeModalLabel">Individual Bill Generate</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <div role="tabpanel">
                        <div class="row">
                            <div class="form-group">
                                <label for="clientname" class="col-sm-3 control-label"> Search Client Information<span class="required">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="IndividualClientBillGenerate" name="IndividualClientBillGenerate" placeholder="Search Client Information">
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div role="tabpanel" class="tab-pane active" id="clientInfo">
                            <div class="serach-replace-info" id="clientinfoSearchBillGenerateResult">
                            </div>                               
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Single Bill Generate Model -->

<!-- Package Change Request -->
<div class="modal fade package-change-request-modal-lg"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myLargeModalLabel">New Package Information.</h4>
            </div>
            <div class="modal-body" id="packageallForm">
                <div role="tabpanel">
                    <div class="row">
                        <div class="form-group">
                            <label for="clientname" class="col-sm-3 control-label"> Search Client Information<span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="scifpchange" name="scifpchange" placeholder="Search By Client ID / Username / Road">
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div role="tabpanel" class="tab-pane active" id="clientInfo">
                        <div class="serach-replace-info" id="scifpchange-result-replace"> </div>                               
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="packageResetCancel" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Package change request -->

<!-- House Change Request -->
<div class="modal fade newhouse-change-request-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myLargeModalLabel">Search Client Information for House Shifting</h4>
            </div>
            <div class="modal-body"  id="allForm">
                <div class="panel-body">
                    <div role="tabpanel">
                        <div class="row">
                            <div class="form-group">
                                <label for="clientname" class="col-sm-3 control-label"> Search Client Information<span class="required">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" data-minlength="3" id="scifhousechange" name="scifhousechange" placeholder="Search By Client ID / Username / Road" required data-default="">
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div role="tabpanel" class="tab-pane active" id="clientInfo">
                            <div class="serach-replace-info" id="scifhousechange-result-replace">
                            </div>                               
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" id="houseResetCancel" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>                        
            </div>
        </div>
    </div>
</div>
<!-- End House change request -->

<!-- Bill History Panel -->
<div id="billHistoryPanel" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" id="billAllForm">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> Bill History Panel</h4>
            </div>
            <div class="modal-body modal-body-design">
                <div class="client-history-search"> 
                    <div class="row searchingType  col-md-12">
                        <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12 padding-left-zero">
                            <span class="pattern-title">Searching Type</span>
                        </div>
                        <div class="col-md-9 col-sm-9 col-lg-9 col-xs-12">
                            <div class="col-md-2 col-lg-2 col-xs-12 search-history pull-left">                         
                                <div class="radio" id="searchbillHistory"><input class="pattern-block billSearchType" name="billSearchType" id="billSearchTypeAll" checked="" value="6" type="radio"></div> All
                            </div>
                            <div class="col-md-2 col-lg-2 col-xs-12 search-history pull-left">                         
                                <div class="radio" id="searchbillHistory"><input class="pattern-block billSearchType" name="billSearchType" id="billSearchType"  value="1" type="radio"></div> Client ID
                            </div>
                            <div class="col-md-2 col-lg-2 col-xs-12 search-history pull-left">                         
                                <div class="radio" id="searchbillHistory"><input class="pattern-block billSearchType" name="billSearchType" id="billSearchTypes" value="2" type="radio"></div>  User Name
                            </div>
                            <div class="col-md-2 col-lg-2 col-xs-12 search-history pull-left">                         
                                <div class="radio" id="searchbillHistory"><input class="pattern-block billSearchType" name="billSearchType" id="billSearchTyped" value="3" type="radio"></div>  Mobile Name
                            </div>
                            
                            <div class="col-md-2 col-lg-2 col-xs-12 search-history pull-left">                         
                                <div class="radio" id="searchclientHistory"><input class="pattern-block clientSearchType" name="billSearchType" id="clientSearchHouse" value="5" type="radio"></div> House
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="clientname" class="col-sm-3 control-label"> Search Client Bill Information<span class="required">*</span></label>
                        <div class="col-sm-8">
                            <input class="form-control" id="searchclientBill" name="sclientcomp" placeholder="Search bill history by client bill no / cilent id" required="" minlength="3" type="text" data-default="">
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <div id="bill-history-search-result" class="bill-history-search-result">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="billresetCancel" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
    
<!-- line disconect Request -->
<div class="modal fade line-disconect-request-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myLargeModalLabel">Search Client Information for Line Disconnect</h4>
            </div>
            <div class="modal-body">
                <div role="tabpanel" id="allForm">
                    <div class="row">
                        <div class="form-group">
                            <label for="clientname" class="col-sm-3 control-label"> Search Client Information<span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="slidlinedis" name="slidlinedis" placeholder="Search By Client Username / Client ID" required data-default=""> 
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <!--  Client Information Setup -->
                    <div role="tabpanel" class="tab-pane active" id="clientInfo">
                        <div class="slidlinedis-replace-info" id="slidlinedis-result-replace"> </div>                               
                    </div>
                    <!--   Information Setup -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="resetCancel" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>                        
    </div>
</div>
<!-- line disconnect request -->

<div class="modal fade client-complain-request-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="allForm">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myLargeModalLabel">Search Client Information for Client Complain</h4>
            </div>

            <div class="modal-body" >
                <div role="tabpanel">
                    <div class="row">
                        <div class="form-group">
                            <label for="clientname" class="col-sm-3 control-label"> Search Client Information<span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="sclientcomp" name="sclientcomp" placeholder="Search Client Information" required minlength="3" data-default="">
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <hr/>              
                    <div role="tabpanel" class="tab-pane active" id="clientInfo">
                        <div class="sclientcomp-replace-info" id="sclientcomp-result-replace"> </div>                               
                    </div>
                     
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="resetCancel" data-dismiss="modal">Cancel</button>
                    </div>
                </div>                      
            </div>
        </div>
    </div>
</div>

<!-- Begin of Line Connection History -->
<div class="modal fade client-history-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myLargeModalLabel">Product  Details</h4>
            </div>
            <div class="modal-body" id="allForm">
                <!-- <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#basic">Basic</a></li>
                    <li><a data-toggle="tab" href="#advanceSearch">Advanced Search</a></li>
                </ul> -->
                <div class="tab-content">
                   <!--  <div id="basic" class="tab-pane fade in active">
                       <div class="row">
                           <div class="form-group">
                               <label for="clientname" class="col-sm-4 control-label"> Search by username / ID / House / Firstname / Mobile Number<span class="required">*</span></label>
                               <div class="col-sm-8">
                                   <input type="text" class="form-control" id="searchLineBasicHistory" name="searchLineBasicHistory" placeholder="Search Client Information" required minlength="3" data-default="">
                                   <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                   <div class="help-block with-errors"></div>
                               </div>
                               <div id="loaderRunBasic"></div>
                               <div role="tabpanel" class="tab-pane active" id="clientInfo">
                                   <div class="sclientcomp-replace-info" id="basicSearchHistory">  </div>                           
                               </div>
                               
                           </div>
                       </div>
                   </div> -->
                    <!-- <div id="advanceSearch" class="tab-pane fade"> -->
                        <div class="client-history-search"> 
                            <div class="row searchingType  col-md-12">
                                <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12 padding-left-zero">
                                    <span class="pattern-title">Searching Type</span>
                                </div>
                                <div class="col-md-9 col-sm-9 col-lg-9 col-xs-12">
                                    <div class="col-md-2 col-lg-2 col-xs-12 search-history pull-left">                         
                                        <div class="radio" id="searchclientHistory"><input class="pattern-block clientSearchType" name="clientSearchType" id="clientSearchTypeAll" checked="" value="6" type="radio"></div> All
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-xs-12 search-history pull-left">                         
                                        <div class="radio" id="searchclientHistory"><input class="pattern-block clientSearchType" name="clientSearchType" id="clientSearchType"  value="1" type="radio"></div> Product ID
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel">
                            <div class="row">
                                <div class="form-group">
                                    <label for="clientname" class="col-sm-3 control-label"> Search <span class="required">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="searchLineHistory" name="sclientcomp" placeholder="Search Client Information" required minlength="3" data-default="">
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div id="loaderRun"></div>
                            <div role="tabpanel" class="tab-pane active" id="clientInfo">
                                <div class="sclientcomp-replace-info" id="linehistory-result-replace">  </div>                           
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" id="resetCancel" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                   
                </div>
            </div>                              
        </div>
    </div>
</div>
<!-- End line Connection history -->

<!--Start admin profile -->
<div class="modal fade admin-profile-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myLargeModalLabel">View Profile</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                  <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-pills" role="tablist">
                            <li role="presentation" class="active"><a href="#adminprofile" role="tab" data-toggle="tab">Admin Profile</a></li>
                            <li role="presentation"><a href="#changepass" role="tab" data-toggle="tab">Change Password </a></li>                              
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="adminprofile">
                               <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-white">  
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="clientname" class="col-sm-2 control-label">Employee Id<span class="required">*</span></label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control"  id="ademployeeid" name="ademployeeid" value="<?php echo e(Session::get('emp_id')); ?>" data-minlength="3" maxlength="40" required readonly>
                                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="clientname" class="col-sm-2 control-label">Full Name</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control"  id="adfullname" name="adfullname" value="<?php echo e(Session::get('name')); ?>" data-minlength="3" maxlength="40" required readonly>
                                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                               <!--  <div class="form-group">
                                                    <label for="clientname" class="col-sm-2 control-label">Date Of Join</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control flddisconmonth"  id="addateofjoin" name="addateofjoin" value="<?php echo e(Input::old('addateofjoin')); ?>" data-minlength="3" maxlength="40" required readonly>
                                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div> -->
                                                <div class="form-group">
                                                    <label for="clientname" class="col-sm-2 control-label">Mobile</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control"  id="admobile" name="admobile" value="<?php echo e(Session::get('mobile')); ?>"  maxlength="40" required readonly>
                                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="clientname" class="col-sm-2 control-label">Email</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control"  id="ademail" name="ademail" value="<?php echo e(Session::get('email')); ?>" placeholder="Email address"  maxlength="40" required readonly>
                                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>    
                                        </div>    
                                    </div>        
                                </div>            
                            </div> 
                            <!-- start change pass  -->
                            <div role="tabpanel" class="tab-pane" id="changepass"> 
                                 <div class="form-group">
                                    <label for="road" class="col-sm-2 control-label">Full Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="road" name="road" value="<?php echo e(Session::get('name')); ?>" placeholder="11" readonly>                                                          
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="road" class="col-sm-2 control-label">User Name(Email)</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="road" name="road" value="<?php echo e(Session::get('email')); ?>" placeholder="11" readonly>                                                          
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="house" class="col-sm-2 control-label">Old Password</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" id="adoldpassword" name="house" value="" placeholder="***">               
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="flat_no" class="col-sm-2 control-label">New Password</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" id="adnewpassword" name="flat_no" value="<?php echo e(Input::old('flat_no')); ?>" placeholder="***">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="sector" class="col-sm-2 control-label">Re-Type Pass</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" id="adretypassword" name="sector" value="<?php echo e(Input::old('sector')); ?>" placeholder="***">         
                                    </div>
                                </div> 
                                <button type="submit" id="adminPasswordChanged" class="btn btn-primary">Update</button>  
                            </div>   
                            <!-- end change pass  -->
                        </div> 
                    </div>  
                </div> 
            </div>
        </div> 
    </div>        
</div>
<!--End admin profile -->

