<div id="ramadanmenu" class="menu-menu-1-container bottom-menubar">
	<div id="menu-button">Menu</div>
	<ul id="menu-menu-1" class="menu">
		<li><a href="<?php echo e(URL::to( '/d2lmis-control-panel' )); ?>">Dashboard</a></li>
		<li class="hover hrm-main-menubar">
			<a href="#">
				<span>
					<i class="fa fa-users"></i>
				   	<span class="title">Receive<i class="fa fa-angle-down"></i> </span> 
			   	</span>
		   </a>
		   <ul class="sub-menu">
				<div class="toltip"></div>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( '/receive' )); ?>">
						<i class="fa fa-users"></i>
						Product Receive
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( '/receive/show' )); ?>">
						<i class="fa fa-briefcase"></i>
						Approve Received Product
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( '/receive/approved/products' )); ?>">
						<i class="fa fa-briefcase"></i>
						Product Receive Report
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( '/receive/disapproved/products' )); ?>">
						<i class="fa fa-briefcase"></i>
						All Disapproved Products
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( '/local/purchase' )); ?>">
						<i class="fa fa-briefcase"></i>
						Local Purchase
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( '/receive/show' )); ?>">
						<i class="fa fa-briefcase"></i>
						Approve Local Purchase
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( '/receive/approved/products' )); ?>">
						<i class="fa fa-briefcase"></i>
						Local Purchase Report
					</a>
				</li>
				
			</ul>
		</li>

		<li class="hover hrm-main-menubar">
			<a href="#" class="dropdown-hover">
				<span><i class="fa fa-money"></i>
				   <span class="title"> Issue <i class="fa fa-angle-down"></i></span> 
			   </span>
		   </a>
			 <ul class="sub-menu">
				<div class="toltip"></div>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( 'create-new-requisition' )); ?>">
					<i class="fa fa-money"></i>
						Create Issue Voucher
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( '/requisition/approval' )); ?>">
					<i class="fa fa-money"></i>
						Approve Issue Vourcher					 
					</a>
				</li>	
				<li class="sub-item">
					<a href="<?php echo e(URL::to( '/requisition/approval' )); ?>">
					<i class="fa fa-money"></i>
						Approved Issue Vourcher					 
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( '/requisition/approval' )); ?>">
					<i class="fa fa-money"></i>
						Disapproved Issue Vourcher					 
					</a>
				</li>		 
			</ul> 
		</li>
		<li class="hover hrm-main-menubar">
			<a href="#" class="dropdown-hover">
				<span><i class="fa fa-money"></i>
				   <span class="title"> Adjustment <i class="fa fa-angle-down"></i></span> 
			   </span>
		   </a>
			 <ul class="sub-menu">
				<div class="toltip"></div>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( 'adjustment' )); ?>">
						<i class="fa fa-money"></i>
						New Adjustment
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( 'adjustment' )); ?>">
						<i class="fa fa-money"></i>
						Approve Adjustment Qty
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( '/requisition/approval' )); ?>">
						<i class="fa fa-money"></i>
						Adjustment Reports					 
					</a>
				</li>			 
			</ul> 
		</li>
		<li class="hover hrm-main-menubar">
		   <a href="#" class="dropdown-hover">
				<span>
				 <i class="fa fa-trophy"></i> &nbsp;Stock <i class="fa fa-angle-down"></i>
				</span>
			</a>
			<ul class="sub-menu">
				<div class="toltip"></div>
				
				<li class="sub-item">
					<a href="<?php echo e(URL::to( 'current-stock' )); ?>">
						<i class="fa fa-gift"></i>
						Current Stock 
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( 'current-stock' )); ?>">
						<i class="fa fa-gift"></i>
						Slow Moving Item Stock
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( 'stock/return' )); ?>">
						<i class="fa fa-gift"></i>
						Stock Return
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( 'stock/return/show' )); ?>">
						<i class="fa fa-gift"></i>
						Approval Return Stocks
					</a>
				</li>

				<li class="sub-item">
					<a href="<?php echo e(URL::to( 'stocks/return/approved' )); ?>">
						<i class="fa fa-gift"></i>
						Approved  Return Stocks
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( 'stocks/return/disapproved' )); ?>">
						<i class="fa fa-gift"></i>
						Disapproved  Return Stocks
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( 'damage/stock' )); ?>">
						<i class="fa fa-gift"></i>
						Damage Stock
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( 'damage/stock/show' )); ?>">
						<i class="fa fa-gift"></i>
						Approve Damage Stock
					</a>
				</li>
			</ul> 
			<li class="hover hrm-main-menubar">
			<a href="#" class="dropdown-hover">
				<span><i class="fa fa-money"></i>
				   <span class="title"> Indent <i class="fa fa-angle-down"></i></span> 
			   </span>
		   </a>
			 <ul class="sub-menu">
				<div class="toltip"></div>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( 'create/indent' )); ?>">
						<i class="fa fa-money"></i>
						Submit New Indent
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( '/requisition/approval' )); ?>">
						<i class="fa fa-money"></i>
						All Indents				 
					</a>
				</li>			 
			</ul> 
		</li>
		<li class="hover hrm-main-menubar">
			<a href="#" class="dropdown-hover">
				<span><i class="fa fa-money"></i>
				   <span class="title"> Supply Plan <i class="fa fa-angle-down"></i></span> 
			   </span>
		   </a>
			 <ul class="sub-menu">
				<div class="toltip"></div>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( 'create-new-requisition' )); ?>">
						<i class="fa fa-money"></i>
						Approve Indent
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( '/requisition/approval' )); ?>">
						<i class="fa fa-money"></i>
						Confirm Supply				 
					</a>
				</li>			 
			</ul> 
		</li>
		<li class="hover hrm-main-menubar">
				<a href="#" class="dropdown-hover">
					<span><i class="fa fa-cogs"></i>
					   <span class="title">Master Data<i class="fa fa-angle-down"></i>
					</span> 

				   </span>
			   </a>
				 <ul class="sub-menu">
					<div class="toltip"></div>
					<li class="sub-item">
						<a href="<?php echo e(URL::to( '/new/product/setup' )); ?>">
							Product Setup
						</a>
					</li>
					<li class="sub-item">
						<a href="<?php echo e(URL::to( '/product/category' )); ?>">
							Product Category Setup
						</a>
					</li>
					<li class="sub-item">
						<a href="<?php echo e(URL::to( '/product/unit' )); ?>">
							Product Unit Setup
						</a>
					</li>
					<li class="sub-item">
						<a href="<?php echo e(URL::to( '/manufacturer' )); ?>">
							Manufacturer Setup
						</a>
					</li>
					<li class="sub-item">
						<a href="<?php echo e(URL::to( '/transaction/type' )); ?>">
							Transaction Type Setup
						</a>
					</li>
					<li class="sub-item">
						<a href="<?php echo e(URL::to( '/supplier' )); ?>">
							Supplier Add
						</a>
					</li>
					<li class="sub-item">
						<a href="<?php echo e(URL::to( '/adjustment/type' )); ?>">
							Adjustment Type Setup
						</a>
					</li>
				 
				</ul> 
		</li>
		<li class="hover hrm-main-menubar">
			<a href="#" class="dropdown-hover">
				<span><i class="fa fa-money"></i>
				   <span class="title"> Facility Info Settings <i class="fa fa-angle-down"></i></span> 
			   </span>
		   </a>
			 <ul class="sub-menu">
				<div class="toltip"></div>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( 'create-new-requisition' )); ?>">
						<i class="fa fa-money"></i>
						New Facility Add
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( '/admin-information' )); ?>">
						<i class="fa fa-money"></i>
						Create New User		 
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( '/master/facility' )); ?>">
						<i class="fa fa-money"></i>
						List of All Facilities
					</a>
				</li>
				<li class="sub-item">
					<a href="<?php echo e(URL::to( '/union' )); ?>">
						<i class="fa fa-money"></i>
						Union List	 
					</a>
				</li>	
				<li class="sub-item">
					<a href="<?php echo e(URL::to( '/upazila' )); ?>">
						<i class="fa fa-money"></i>
						Upazila List	 
					</a>
				</li><li class="sub-item">
					<a href="<?php echo e(URL::to( '/district' )); ?>">
						<i class="fa fa-money"></i>
						District List	 
					</a>
				</li><li class="sub-item">
					<a href="<?php echo e(URL::to( '/division' )); ?>">
						<i class="fa fa-money"></i>
						Division List	 
					</a>
				</li>		 
			</ul> 
		</li>
		<li class="hover hrm-main-menubar">
			   	<a href="#" class="dropdown-hover">
					<span>
					 <i class="fa fa-bar-chart"></i> &nbsp;Reports <i class="fa fa-angle-down"></i>
					</span>
				</a>
				<ul class="sub-menu">
					<div class="toltip"></div>
					
					<li class="sub-item">
						<a href="<?php echo e(URL::to( 'master/transaction' )); ?>">
							<i class="fa fa-gift"></i>
							Master Transactions
						</a>
					</li>

					<li class="sub-item">
						<a href="<?php echo e(URL::to( 'master/summary/reports' )); ?>">
							<i class="fa fa-gift"></i>
							Master Summary Reports
						</a>
					</li>

					<li class="sub-item">
						<a href="<?php echo e(URL::to( 'new/product/setup/show' )); ?>">
							<i class="fa fa-gift"></i>
							List of all Products
						</a>
					</li>
					<li class="sub-item">
						<a href="<?php echo e(URL::to( 'product/category/shows' )); ?>">
							<i class="fa fa-gift"></i>
							List of Product Category
						</a>
					</li>
					<li class="sub-item">
						<a href="<?php echo e(URL::to( 'manufacturer/show' )); ?>">
							<i class="fa fa-gift"></i>
							List of Manufacturer's
						</a>
					</li>
					<li class="sub-item">
						<a href="<?php echo e(URL::to( 'supplier/show' )); ?>">
							<i class="fa fa-gift"></i>
							List of Supplier's
						</a>
					</li>
				</ul> 
		</li>
		
	</ul>
</div>	