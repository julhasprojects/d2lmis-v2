<?php $__env->startSection( 'content_area' ); ?>

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Submit Indent</a></li>
    </ul>
</div>
<?php echo $__env->make('modules.inventory.master.navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('modules.inventory.master.error', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('master.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div id="main-wrapper">

	<div class="col0-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
				<h2 class="title-house"> Submit Indent</h2>
				<?php echo Form::open(['url'=>'requisition', 'class' =>'form col-md-12', 'enctype'=>'multipart/form-data']); ?>

				<div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
					<label class="col-sm-3 control-label package-label" for="grandtotal">
						Transaction No
					</label>
	            	<div class="col-sm-9">
		            	<input type="text" name="requisition_no" id="requisition_no" class="form-control" value="<?php echo date('YmdHis');?>" readonly="">
		            </div>
          		</div>
          		<div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
					<label class="col-sm-3 control-label package-label" for="grandtotal">
						Invoice No
					</label>
	            	<div class="col-sm-9">
		            	<input type="text" name="remarks" id="remarks" class="form-control">
		            </div>
          		</div> 
          		 
          		<div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
					<label class="col-sm-3 control-label package-label" for="grandtotal">
						Indent Submitted By
					</label>
	            	<div class="col-sm-9">
		            	<select class="form-control" name="submitted_by" id="supplier_id" >
			                <option value="">Select Employee</option>
			                <?php $__currentLoopData = $empployeeList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<option value=" <?php echo e($emp->employeeID); ?>"><?php echo e($emp->fullName); ?></option>
			                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			            </select> 
		            </div>
          		</div>
          		<div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
					<label class="col-sm-3 control-label package-label" for="grandtotal">Indent Date</label>
               		<div class="col-sm-9">
                        <div class="input-group date" data-date-format="yyyy-mm-dd">
                            
                            <input type="text" name="requisition_date" id="requisition_date" class="form-control"  placeholder="yyyy-mm-dd">
                            <span class="input-group-btn">
                            	<button class="btn default" type="button">
                            		<i class="glyphicon glyphicon-th"></i>
                            	</button>
                            </span>
                        </div>
                    </div>
                </div> 
                <div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
					<label class="col-sm-3 control-label package-label" for="grandtotal">
						Prepared By
					</label>
	            	<div class="col-sm-9">
		            	<select class="form-control" name="submitted_by" id="supplier_id" >
			                <option value="">Select Employee</option>
			                <?php $__currentLoopData = $empployeeList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<option value=" <?php echo e($emp->employeeID); ?>"><?php echo e($emp->fullName); ?></option>
			                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			            </select> 
		            </div>
          		</div>
          		<div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
					<label class="col-sm-3 control-label package-label" for="grandtotal">Prepared Date</label>
               		<div class="col-sm-9">
                        <div class="input-group date" data-date-format="yyyy-mm-dd">
                            
                            <input type="text" name="requisition_date" id="requisition_date" class="form-control"  placeholder="yyyy-mm-dd">
                            <span class="input-group-btn">
                            	<button class="btn default" type="button">
                            		<i class="glyphicon glyphicon-th"></i>
                            	</button>
                            </span>
                        </div>
                    </div>
                </div> 
                <div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
					<label class="col-sm-3 control-label package-label" for="grandtotal">
						Approval Authority
					</label>
	            	<div class="col-sm-9">
		            	<select class="form-control" name="submitted_by" id="supplier_id" >
			                <option value="">Select Employee</option>
			                <?php $__currentLoopData = $empployeeList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<option value=" <?php echo e($emp->employeeID); ?>"><?php echo e($emp->fullName); ?></option>
			                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			            </select> 
		            </div>
          		</div>
          		<div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
					<label class="col-sm-3 control-label package-label" for="grandtotal">Approval Date</label>
               		<div class="col-sm-9">
                        <div class="input-group date" data-date-format="yyyy-mm-dd">
                            
                            <input type="text" name="requisition_date" id="requisition_date" class="form-control"  placeholder="yyyy-mm-dd">
                            <span class="input-group-btn">
                            	<button class="btn default" type="button">
                            		<i class="glyphicon glyphicon-th"></i>
                            	</button>
                            </span>
                        </div>
                    </div>
                </div>
          		<div class="froum-group col-md-6 col-xs-12 col-lg-6 col-sm-6 inventory-column">
					<label class="col-sm-3 control-label package-label" for="grandtotal">
						Remarks
					</label>
	            	<div class="col-sm-9">
		            	<input type="text" name="remarks" id="remarks" class="form-control">
		            </div>
          		</div> 

				<table id="table_gr" class="table table-bordered table-hover display dataTable table-header-bg">
	                <thead>
	                    <tr>
	                        <th>Category</th>
	                        <th>Product Name</th>
	                        <th>Available Stock</th>
	                        <th>Requested Qty</th>
	                        <th>Remarks</th>
	                        <th>Add </th>
	                    </tr>
	                </thead>
	                <tbody class="controls">
	                    <tr class="entry">
	                        <td class="col-md-2">
	                            <select class="form-control typeId" name="category_id[]" id="category_id">
	                               <option value="">Select Category</option>
					                <?php $__currentLoopData = $categoryList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<option value=" <?php echo e($category->category_id); ?>"><?php echo e($category->category_name); ?></option>
					                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	                            </select>
	                        </td>
	                        <td class="col-md-2">
	                            <select class="form-control product_id product_qty" onclick="product_availble('1','0')" name="product_id[]" id="product_id">

	                                <option value="">Select Product</option>
					                <?php $__currentLoopData = $productList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<option value="<?php echo e($product->product_id); ?>"><?php echo e($product->product_name); ?></option>
					                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	                            </select>
	                        </td>
	                        <td>

	                        	<input type="text" name="unit_price[]" placeholder="Stock Qty" id="available_products" readonly="" class="form-control">
	                        </td>
	                        <td>
	                        	<?php echo Form::number('quantity[]', null,['class'=>'form-control','placeholder'=>'Quantity', 'id'=>'quantity_calculate', 'value'=>'0']); ?>

	                        	<input type="hidden" id="hiddenqty">
	                        </td>
	                        <td><?php echo Form::text('comments[]', null,['class'=>'form-control','placeholder'=>'Remarks']); ?></td>
	                        <td class="col-md-1">
	                            <span class="input-group-btn">
	                                <button class="btn btn-primary btn-add" type="button">
	                                    Add <span class="glyphicon glyphicon-plus"></span>
	                                </button>
	                            </span>
	                        </td>
	                    </tr>
	                    <tr id="newPara"></tr>
	                </tbody>
	            </table>

          		<div class="col-md-12 text-center">
	                <br><br>
	                <?php echo Form::submit('Submit', ['class'=>'btn btn-primary']); ?>

	                <button class="btn btn-danger" type="reset">Reset</button>
	            </div>
            	<?php echo Form::close(); ?>	
			</div>
		</div>
	</div>
</div>
<script>
	$(function() {
	    $(document).on('click', '.btn-add', function(e) {
	    	var length = $(".typeId").length;
	        $( "#newPara" ).before( '<tr id="property_'+ length + '"><td class="col-md-1"> <select class="form-control typeId" id="category_id_'+ length + '" name="category_id[]"> <option value="">Select Category</option> <?php $__currentLoopData = $categoryList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($category->category_id); ?>"><?php echo e($category->category_name); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </select> </td><td class="col-md-2"> <select class="products-name form-control nameId" name="product_id[]" id="product_id_'+ length + '" > <option value="">Select Product</option> <?php $__currentLoopData = $productList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($product->product_id); ?>"><?php echo e($product->product_name); ?></option> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </select> </td><td> <input   id="unit_price_'+ length +'" class="form-control" placeholder="Stock Qty" name="unit_price[]" type="txt" id="available_products" readonly><input  id="hidden_unit_price_'+ length +'" class="form-control" placeholder="Stock Qty" name="unit_price[]" type="hidden" id="available_products_"'+ length +' readonly>  </td><td> <input class="form-control qty-calculateds" placeholder="Quantity" id="quantity_id_'+ length + '" name="quantity[]" type="number"> </td><td> <input class="form-control" step="1" placeholder="Client Id" name="total_price[]" type="number"> </td><td><input class="form-control" placeholder="Comments" name="comments[]" type="text"></td><td class="hidden">  </td><td class="col-md-1"> <span class="input-group-btn"> <button class="btn btn-danger btn-remove_'+ length+'" type="button"> Remove <span class="glyphicon glyphicon-minus"></span> </button> </span> </td></tr>' );

	        	

	        	$('#category_id_'+ length ).on('click',function() {
        
			        var categoryId = $( this).val();

			        var value = '?categoryId=' + categoryId;  
			       
			        $.ajax({
			            type: "GET",
			            url: basePath + 'product/category/show' + value
			        }).success(function ( result ) {

			            $('#product_id_'+ length ).html( result );
			            $('#loader').slideUp(200,function(){        
			                $('#loader').remove();
			            });
			            $(".loader").fadeOut("slow"); 

			        }).error(function ( result ) {

			            console.log('Information not found');

			        }); 
			    });

	        	$('.btn-remove_'+ length).on('click keyup',function() { 
	        		$('#property_'+ length).hide();
	        	})
	        	
	        })

	   /* $('#unit_price,#quantity').on('click keyup',function() {
	    	
    		var totalfirstUnit   = $('#unit_price').val();
    		var totalfirstQuenty = $('#quantity').val();

    		var totalPriceFirst  = totalfirstUnit * totalfirstQuenty;
    		$('#total_price').val(totalPriceFirst);
    	});*/

    	$('#category_id' ).on('click',function() {
        	
	        var categoryId = $( this).val();

	        var value = '?categoryId=' + categoryId;  
	       
	        $.ajax({
	            type: "GET",
	            url: basePath + 'product/category/show' + value
	        }).success(function ( result ) {

	            $('#product_id' ).html( result );
	            $('#loader').slideUp(200,function(){        
	                $('#loader').remove();
	            });
	            $(".loader").fadeOut("slow"); 

	        }).error(function ( result ) {

	            console.log('Information not found');

	        }); 
	    });

	    

	    

	});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make( 'backend.index' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>