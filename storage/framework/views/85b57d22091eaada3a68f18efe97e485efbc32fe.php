<!DOCTYPE html>
<html>
<head>

    <title>D2LMIS-CONTROL PANEL</title>
    
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">   
    <link href="<?php echo e(URL::asset( 'assets/plugins/pace-master/themes/blue/pace-theme-flash.css' )); ?>" rel="stylesheet"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="<?php echo e(URL::asset( 'assets/js/login.js' )); ?>"></script>    
    <link href="<?php echo e(URL::asset( 'assets/css/custom.css' )); ?>" rel="stylesheet" />
    <link href="<?php echo e(URL::asset( 'assets/css/dhis2-login.css' )); ?>" rel="stylesheet" />
    <link href="<?php echo e(URL::asset( 'assets/css/sweetalert/sweetalert.css' )); ?>" rel="stylesheet" />
    <script src="<?php echo e(URL::asset( 'assets/js/sweetalert/sweetalert.min.js' )); ?>"></script>

</head>

<body class="page-header-fixed front-backgroud small-sidebar">
 
