<?php $__env->startSection( 'content_area' ); ?>

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Product Setup </a></li>
    </ul>
</div>

<?php echo $__env->make('modules.inventory.master.navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('modules.inventory.master.error', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div id="deletenotification"></div>
<div id="main-wrapper">
	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
				<h2 class="title-house"> Product Setup</h2>
				 <?php echo e(Form::open(array('url'=>"new/product/setup",'class'=>'form-horizontal form-bordered','method'=>'post'))); ?>

					<div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product ID:</label>
		                <div class="col-md-8">
		                    <?php echo Form::text('product_id', $productID,['class'=>'form-control','placeholder'=>'Product ID','readonly']); ?>

		                </div>
		            </div>
		            <div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product Code:</label>
	                	<div class="col-md-8">
		                    <?php echo Form::text('product_code', null,['class'=>'form-control','placeholder'=>'Product Code']); ?>

		                </div>
		            </div>
		           <div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product Category(Type)</label>
		                <div class="col-md-8">
		                	<select class="form-control chosen-select-width" name="productcategory" id='productcategory'>
		                		<option value="">Select Product Category</option>
		                		<?php $__currentLoopData = $productCategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $productCategoryInfo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		                		<option value="<?php echo e($productCategoryInfo->category_id); ?>"> <?php echo e($productCategoryInfo->category_name); ?> </option>
		                		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		                	</select>
		                </div>
		            </div>
					
					<div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product Unit:</label>
		                <div class="col-md-8">
		                	<select class="form-control chosen-select-width" name="productunit" id='productunit'>
		                		<option value="">Select Product Unit</option>
		                		<?php $__currentLoopData = $productUnits; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pproductUnitsInfo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		                		<option value="<?php echo e($pproductUnitsInfo->unit_id); ?>"> <?php echo e($pproductUnitsInfo->unit_name); ?> </option>
		                		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		                	</select>
		                </div>
		            </div>
					<div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Manufacturer(Brand)</label>
		                <div class="col-md-8">
		                	<select class="form-control chosen-select-width" name="manufacturer" id='manufacturer'>
		                		<option value="">Select Manufacturer Unit</option>
		                		<?php $__currentLoopData = $manufacturerList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $manufacturerInfo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		                			<option value="<?php echo e($manufacturerInfo->manufact_id); ?>"> <?php echo e($manufacturerInfo->manufact_name); ?> </option>
		                		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		                	</select>
		                </div>
		            </div>
		            <div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product Name:</label>
		                <div class="col-md-8">
		                    <?php echo Form::text('product_name', null,['class'=>'form-control','placeholder'=>'Product Name']); ?>

		                    <br>
		                </div>
		            </div>
		            <div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product details:</label>
		                <div class="col-md-8">
		                    <?php echo Form::text('product_details', null,['class'=>'form-control','placeholder'=>'Product Details']); ?>

		                    <br>
		                </div>
		            </div>
					 <div class="col-md-12 product-setup-panel">
		                <label class="control-label col-md-2">Product Notes:</label>
		                <div class="col-md-8">
		                    <?php echo Form::textarea('product_notes', null,['class'=>'form-control','placeholder'=>'Product Notes']); ?>

		                    <br>
		                </div>
		            </div>
		            <div class="col-md-12 text-center">
		                <?php echo Form::submit('Submit', ['class'=>'btn btn-primary']); ?>

		                <button class="btn btn-danger" type="reset">Reset</button>
		            </div>

		            <?php echo e(Form::close()); ?>

			</div>
		</div>
	</div>
	<div class="col-md-12 col-xs-12 col-lg-12  left-box"> 
		<div class="panel-white">
			<div class="panel-body">
				<?php echo e(csrf_field()); ?>

				<h2 class="title-house"> Product   List </h2>
			 	<table class="table table-hover table-bordered display" id="tableresponsive" style="width: 100%; cellspacing: 0;">
				    <thead class="table-header-bg">
				        <tr>
				            <th>SN</th>
				            <th>Product ID</th>
				            <th>Category Name</th>
				            <th>Product Unit</th>
				            <th>Manufacturer</th>
				            <th>Product Code</th>
				            <th>Product Name</th>
				            <th>Product Detials</th>
				            <th>Product Notes</th>
				            
				            <th>Action</th>
				        </tr>
				    </thead>
				    <tbody>
				    	<?php $sn =1 ?>
				    	<?php $__currentLoopData = $productSetup; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					    	<tr id="product_Setup_<?php echo e($info->product_id); ?>">
					    		<td> <?php echo e($sn++); ?> </td>
					    		<td> <?php echo e($info->product_id); ?> </td>
					    		<td> <?php echo e($info->category_name); ?> </td>
					    		<td> <?php echo e($info->unit_name); ?> </td>
					    		<td> <?php echo e($info->manufact_name); ?> </td>
					    		<td> <?php echo e($info->product_code); ?> </td>
					    		<td> <?php echo e($info->product_name); ?> </td>
					    		<td> <?php echo e($info->product_details); ?> </td>
					    		<td> <?php echo e($info->product_notes); ?> </td>
					    		<td>  
					    			<a class="btn btn-success btn-xs" href="<?php echo e(URL::to('/new/product/setup/'.$info->product_id.'/edit')); ?>"  > <i class="fa fa-pencil"> </i></a>      
                                    <a class="btn btn-xs btn-danger" href="javascript:;" onclick="productSetupDeleted('<?php echo e($info->product_id); ?>','<?php echo e($info->product_name); ?>')"><i class="fa fa-trash"></i></a>
                                </td>
					    	</tr>
				    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				    </tbody>
			    </table>
	    	</div>
		</div>
	</div>
</div>
<?php echo $__env->make('modules.inventory.master.delete', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make( 'backend.index' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>