<?php $__env->startSection( 'content_area' ); ?>

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Master Transaction   </a></li>
    </ul>
</div>
<?php echo $__env->make('modules.inventory.master.navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div id="main-wrapper">
	<div class="col-md-12 left-box">

		<div class="panel panel-white">
			<div class="panel panel-body">
				<ul class="nav nav-tabs">
				  <li class="active"><a data-toggle="tab" href="#advancedSearch"> Inventory Master Transactions </a></li>
				</ul>
				<div class="tab-content">
			    	<div id="advancedSearch" class="tab-pane fade in active">
			    		<div class="col-md-3 col-xs-12 col-sm-12">
							<select  id="transactionType" class="chosen-select-width">
								<?php $__currentLoopData = $transactionType; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $typeInfo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($typeInfo->id); ?>"> <?php echo e($typeInfo->type_name); ?> </option>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>
						</div>
						<div class="col-md-2 padding-left-zero">
                            <div class="form-group">
                                <div class="input-group">
                                    <input class="form-control bbibillfrom" data-date-format="yyyy-mm-dd" name="datefrom" id="datefrom" placeholder="Transaction From" required="" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="input-group">
                                    <input class="form-control bbibillto" data-date-format="yyyy-mm-dd" name="dateto" id="dateto" placeholder="Transaction To" required="" type="text">
                                 </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <button class="btn btn-success btn-center button-inventory" id="masterTransactionSeatch"> Find </button>
                            </div>
                        </div>
                        <div class="row">
	                        <div class="col-md-12">
	                        	<div id="masterTransactionResult"></div>
	                        </div>
                        </div>
			    	</div>
			    </div>
			</div>
	    </div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make( 'backend.index' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>