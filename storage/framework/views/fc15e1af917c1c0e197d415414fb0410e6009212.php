<script>
	$(function() {
	    $(document).on('click', '.btn-add', function(e) {
	    	var length = $(".typeId").length;
	        $( "#newPara" ).before( '<tr id="property_'+ length + '"><td class="col-md-1"> <select class="form-control typeId" id="category_id_'+ length + '" name="category_id[]"> <option value="">Select Category</option> <?php $__currentLoopData = $categoryList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($category->category_id); ?>"><?php echo e($category->category_name); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </select> </td><td class="col-md-2"> <select class="form-control nameId" name="product_id[]" id="product_id_'+ length + '"> <option value="">Select Product</option> <?php $__currentLoopData = $productList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($product->product_id); ?>"><?php echo e($product->product_name); ?></option> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </select> </td><td> <?php echo Form::number("total_received[]", null,["class"=>"form-control","id"=>"total_received","placeholder"=>"Total Received"]); ?> </td><td> <input class="form-control" placeholder="Quantity"  id="quantity_'+ length + '" name="quantity[]" type="number"> </td><td> <input type="text" name="referance[]" id="referance" class="form-control" placeholder="Referance"></td><td><input class="form-control" placeholder="Comments" name="comments[]" type="text"></td><td class="hidden">  </td><td class="col-md-1"> <span class="input-group-btn"> <button class="btn btn-danger btn-remove_'+ length+'" type="button"> Remove <span class="glyphicon glyphicon-minus"></span> </button> </span> </td></tr>' );

	        	$('#warranty_period_'+ length + '').datepicker();

	        	$('#unit_price_'+ length + ',#quantity_'+ length +'').on('click keyup',function() {
	        		var totalUnit   = $('#unit_price_'+length).val();
	        		var totalQuenty = $('#quantity_'+length).val();
	        		var totalPrice  = totalUnit * totalQuenty;
	        		$('#total_price_'+ length).val(totalPrice);
	        	});

	        	$('#category_id_'+ length ).on('click',function() {
        
			        var categoryId = $( this).val();

			        var value = '?categoryId=' + categoryId;  
			       
			        $.ajax({
			            type: "GET",
			            url: basePath + 'product/category/show' + value
			        }).success(function ( result ) {

			            $('#product_id_'+ length ).html( result );
			            $('#loader').slideUp(200,function(){        
			                $('#loader').remove();
			            });
			            $(".loader").fadeOut("slow"); 

			        }).error(function ( result ) {

			            console.log('Information not found');

			        }); 
			    });

	        	$('.btn-remove_'+ length).on('click keyup',function() { 
	        		$('#property_'+ length).hide();
	        	})
	        	
	        })

	 

    	$('#category_id' ).on('click',function() {
        	
	        var categoryId = $( this).val();

	        var value = '?categoryId=' + categoryId;  
	       
	        $.ajax({
	            type: "GET",
	            url: basePath + 'product/category/show' + value
	        }).success(function ( result ) {

	            $('#product_id' ).html( result );
	            $('#loader').slideUp(200,function(){        
	                $('#loader').remove();
	            });
	            $(".loader").fadeOut("slow"); 

	        }).error(function ( result ) {

	            console.log('Information not found');

	        }); 
	    });
	});
</script>