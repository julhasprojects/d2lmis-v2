<?php $__env->startSection( 'content_area' ); ?>

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Current Stock  </a></li>
    </ul>
</div>
<?php echo $__env->make('modules.inventory.master.navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('modules.inventory.master.error', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div id="main-wrapper">

	<div class="col0-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
				<h2 class="title-house"> Current Stock</h2>
				 	<table class="table table-hover table-bordered display" id="tableresponsive" style="width: 100%; cellspacing: 0;">
		    			<thead class="table-header-bg">
	                        <tr>
	                            <th>S/N</th>
                                <th>Category</th>
                                <th>Product ID</th>
                                <th>Product Name</th>
                                <th>Product Details</th>
	                            <th>Stock Qty</th>
	                            <th>Last Update</th>
	                        </tr>
                        </thead>
                        <tbody>
                        <?php $sn=1; ?>
                        <?php $__currentLoopData = $currentStock; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $currentStock): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="odd gradeX">
                                <td><?php echo e($sn++); ?>

                                </td>
                                <td><?php echo e($currentStock->category_name); ?>

                                </td>
                                <td><?php echo e($currentStock->product_id); ?></td>
                                <td><?php echo e($currentStock->product_name); ?></td>
                                <td><?php echo e($currentStock->product_details); ?></td>
                                <td><?php echo e($currentStock->stock_qty); ?></td>
                                <td><?php echo e($currentStock->stockupdate_date); ?></td>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make( 'backend.index' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>