<script>
	$(function() {
	    $(document).on('click', '.btn-add', function(e) {
	    	var length = $(".typeId").length;
	        $( "#newPara" ).before( '<tr id="property_'+ length + '"><td class="col-md-1"> <select class="form-control typeId" id="category_id_'+ length + '" name="category_id[]"> <option value="">Select Category</option> <?php $__currentLoopData = $categoryList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($category->category_id); ?>"><?php echo e($category->category_name); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </select> </td><td class="col-md-2"> <select class="form-control nameId" name="product_id[]" id="product_id_'+ length + '"> <option value="">Select Product</option> <?php $__currentLoopData = $productList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($product->product_id); ?>"><?php echo e($product->product_name); ?></option> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </select> </td><td> <input   id="unit_price_'+ length +'" class="form-control" placeholder="Opening" name="unit_price[]" type="number"> </td><td> <input   id="unit_price_'+ length +'" class="form-control" placeholder="Unit Price" name="unit_price[]" type="number"> </td><td> <input   id="unit_price_sale'+ length +'" class="form-control" placeholder="Quantity" name="unit_price_sale[]" type="number"> </td><td> <input class="form-control" placeholder="Total Price"  id="quantity_'+ length + '" name="quantity[]" type="number"> </td><td> <input class="form-control" step="0.02" placeholder="Lot No" id="total_price_'+ length + '" name="total_price[]" type="number" readonly value="0"> </td><td> <div id="sandbox" class="sandbox span7 col-md-12"> <div class="input-group date" data-date-format="yyyy-mm-dd"> <input name="warranty_period[]" id="warranty_period_'+ length + '" class="form-control" placeholder="yyyy-mm-dd" type="text"> <span class="input-group-btn"><button class="btn default" type="button"><i class="glyphicon glyphicon-th"></i></button></span> </div></div></td><td> <div id="sandbox" class="sandbox span7 col-md-12"> <div class="input-group date" data-date-format="yyyy-mm-dd"> <input name="warranty_period[]" id="warranty_period_'+ length + '" class="form-control" placeholder="yyyy-mm-dd" type="text"> <span class="input-group-btn"><button class="btn default" type="button"><i class="glyphicon glyphicon-th"></i></button></span> </div></div></td><td><input class="form-control" placeholder="Comments" name="comments[]" type="text"></td><td class="hidden">  </td><td class="col-md-1"> <span class="input-group-btn"> <button class="btn btn-danger btn-remove_'+ length+'" type="button"> Remove <span class="glyphicon glyphicon-minus"></span> </button> </span> </td></tr>' );

	        	$('#warranty_period_'+ length + '').datepicker();

	        	$('#unit_price_'+ length + ',#quantity_'+ length +'').on('click keyup',function() {
	        		var totalUnit   = $('#unit_price_'+length).val();
	        		var totalQuenty = $('#quantity_'+length).val();
	        		var totalPrice  = totalUnit * totalQuenty;
	        		$('#total_price_'+ length).val(totalPrice);
	        	});

	        	$('#category_id_'+ length ).on('click',function() {
        
			        var categoryId = $( this).val();

			        var value = '?categoryId=' + categoryId;  
			       
			        $.ajax({
			            type: "GET",
			            url: basePath + 'product/category/show' + value
			        }).success(function ( result ) {

			            $('#product_id_'+ length ).html( result );
			            $('#loader').slideUp(200,function(){        
			                $('#loader').remove();
			            });
			            $(".loader").fadeOut("slow"); 

			        }).error(function ( result ) {

			            console.log('Information not found');

			        }); 
			    });

	        	$('.btn-remove_'+ length).on('click keyup',function() { 
	        		$('#property_'+ length).hide();
	        	})
	        	
	        })

	    $('#unit_price,#quantity').on('click keyup',function() {
	    	
    		var totalfirstUnit   = $('#unit_price').val();
    		var totalfirstQuenty = $('#quantity').val();

    		var totalPriceFirst  = totalfirstUnit * totalfirstQuenty;
    		$('#total_price').val(totalPriceFirst);
    	});

    	$('#category_id' ).on('click',function() {
        	
	        var categoryId = $( this).val();

	        var value = '?categoryId=' + categoryId;  
	       
	        $.ajax({
	            type: "GET",
	            url: basePath + 'product/category/show' + value
	        }).success(function ( result ) {

	            $('#product_id' ).html( result );
	            $('#loader').slideUp(200,function(){        
	                $('#loader').remove();
	            });
	            $(".loader").fadeOut("slow"); 

	        }).error(function ( result ) {

	            console.log('Information was not found');

	        }); 
	    });
	});
</script>