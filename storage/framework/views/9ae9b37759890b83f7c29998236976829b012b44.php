<?php $__env->startSection( 'content_area' ); ?>

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> CONTROL PANEL </a></li>
    </ul>
</div>
<?php echo $__env->make('master.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('modules.inventory.master.navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('modules.inventory.master.sale-reqution', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('master.error', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div id="main-wrapper" class="main-section">
	<div  class="pppoe-page inv-page">
		<ul class="pppoe-radius">
			<!-- <li class="bg-primary">
				<a button type="button" data-toggle="modal" data-target="#exampleModal">
					<span class="icon-radius"><i class="fa fa-folder-open  pppoe-icon"></i></span>
					<span class="pppoe-radius">New Sale</span>
				</a>
			</li> -->
			<li class="bg-primary">
				<a href="<?php echo e(URL::to( '/receive' )); ?>">
					<span class="icon-radius"><i class="fa fa-folder-open  pppoe-icon"></i></span>
					<span class="pppoe-radius">Product Receive</span>
				</a>
			</li>
			<!-- <li class="bg-success">
				<a href="<?php echo e(URL::to( '/due/sales/reports/' )); ?>">
					<span class="icon-radius"><i class="fa fa-folder-open  pppoe-icon"></i></span>
					<span class="pppoe-radius">View Due Sales</span>
				</a>
			</li> -->
			<li class="bg-success">
				<a href="<?php echo e(URL::to( '/create-new-requisition' )); ?>">
					<span class="icon-radius"><i class="fa fa-sign-out pppoe-icon"></i></span>
					<span class="pppoe-radius">Product Issues</span>
				</a>
			</li>
			<!-- <li class="bg-info">
				<a href="<?php echo e(URL::to( '/sales/reports/' )); ?>">
					<span class="icon-radius"><i class="fa fa-folder-open  pppoe-icon"></i></span>
					<span class="pppoe-radius">Sales Reports</span>
				</a>
			</li> -->
			<li class="bg-info">
				<a href="<?php echo e(URL::to( '/adjustment' )); ?>">
					<span class="icon-radius"><i class="fa fa-adjust  pppoe-icon"></i></span>
					<span class="pppoe-radius">Adjustments</span>
				</a>
			</li>
			<li class="bg-warning">
				<a href="<?php echo e(URL::to( '/current-stock' )); ?>">
					<span class="icon-radius"><i class="fa fa-archive  pppoe-icon"></i></span>
					<span class="pppoe-radius">Current Stock Report</span>
				</a>
			</li>
			<li class="bg-primary">
				<a href="#">
					<span class="icon-radius"><i class="fa fa-area-chart pppoe-icon"></i></span>
					<span class="pppoe-radius">Slow Moving Items/ Medicnes</span>
				</a>
			</li>
			<li class="bg-dark">
				<a href="<?php echo e(URL::to( '/stock/return' )); ?>">
					<span class="icon-radius"><i class="fa fa-crosshairs  pppoe-icon"></i></span>
					<span class="pppoe-radius">Stock Return</span>
				</a>
			</li>
			<li class="bg-success">
				<a href="<?php echo e(URL::to( '/create/indent' )); ?>">
					<span class="icon-radius"><i class="fa fa-plus-square-o  pppoe-icon"></i></span>
					<span class="pppoe-radius">Submit New Indent</span>
				</a>
			</li>
			<li class="bg-primary">
				<a href="#">
					<span class="icon-radius"><i class="fa fa-pencil-square  pppoe-icon"></i></span>
					<span class="pppoe-radius">Add Product Category</span>
				</a>
			</li>
			<li class="bg-info">
				<a href="#">
					<span class="icon-radius"><i class="fa fa-comments  pppoe-icon"></i></span>
					<span class="pppoe-radius">Send Custom SMS</span>
				</a>
			</li>
			<li class="bg-info">
				<a href="<?php echo e(URL::to( '/barcode-generator/' )); ?>">
					<span class="icon-radius"><i class="fa fa-barcode  pppoe-icon"></i></span>
					<span class="pppoe-radius">Barcode Generate</span>
				</a>
			</li>
			<li class="bg-dark">
				<a href="<?php echo e(URL::to( '/new/product/setup/' )); ?>">
					<span class="icon-radius"><i class="fa fa-product-hunt pppoe-icon"></i></span>
					<span class="pppoe-radius">Product Setup</span>
				</a>
			</li>
			<li class="bg-danger">
				<a href="#">
					<span class="icon-radius"><i class="fa fa-file-text-o  pppoe-icon"></i></span>
					<span class="pppoe-radius">Stock Out</span>
				</a>
			</li>
		</ul>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make( 'backend.index' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>