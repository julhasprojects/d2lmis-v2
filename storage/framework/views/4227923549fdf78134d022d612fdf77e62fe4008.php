<?php $__env->startSection( 'content_area' ); ?>

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);">  Stock Return  </a></li>
    </ul>
</div>
<?php echo $__env->make('modules.inventory.master.navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('modules.inventory.master.error', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('master.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div id="main-wrapper">
    <div class="col-md-12 colum-design-para">
        <div class="panel panel-white">
            <div class="panel panel-body">
                <h2 class="title-house"> Return Stock</h2>
                <?php echo Form::open(['url'=>'stock/return', 'class' =>'form col-md-12', 'enctype'=>'multipart/form-data']); ?>

                <div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 return-stock-column">
                    <label class="control-label col-md-3">Transaction ID:</label>
                    <div class="col-md-9">
                        <?php echo Form::text('transaction_id', null,['class'=>'form-control','placeholder'=>'Transaction ID']); ?>

                        <br>
                    </div>
                </div>

                <div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 return-stock-column">
                    <label class="control-label col-md-3"> Return  Date:</label>
                    <div class="col-md-9">
                        <?php echo Form::text('return_date', null,['class'=>'form-control bbibillto','placeholder'=>'Year-Month-Date']); ?>

                        <br>
                    </div>
                </div>

                <div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 return-stock-column">
                    <label class="control-label col-md-3"> Requention No:</label>
                    <div class="col-md-9">
                        <?php echo Form::text('requestion_no', null,['class'=>'form-control','placeholder'=>'Requention Number']); ?>

                        <br>
                    </div>
                </div>

                <div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 return-stock-column">
                    <label class="control-label col-md-3"> Return  By:</label>
                    <div class="col-md-9">
                        <select name="return_by" id="return_by" class="chosen-select-width">
                            <option value="">Select Retrun By</option>
                             <?php $__currentLoopData = $allEmployeer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $empInfo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($empInfo->employeeID); ?>"> <?php echo e($empInfo->fullName); ?></option>
                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>   
                        </select>
                    </div>
                </div>

                <div class="col-md-6 col-xs-12 col-lg-6 col-sm-6 return-stock-column">
                    <label class="control-label col-md-3"> Reason  of return:</label>
                    <div class="col-md-9">
                        <?php echo Form::textarea('reason_return', null,['class'=>'form-control','placeholder'=>'Reason Return','cols'=>'2','rows'=>'2']); ?>

                        <br>
                    </div>
                </div>
                
                <table id="table_gr" class="table table-bordered table-hover display dataTable table-header-bg">
                    <thead>
                        <tr>
                            <th>Category</th>
                            <th>Product Name</th>
                            <th>Total Recived</th>
                            <th>Retrun Qty</th>
                          
                            <th>Referance</th>
                            <th>Remarks</th>
                            <th>Add Row </th>
                        </tr>
                    </thead>
                    <tbody class="controls">
                        <tr class="entry" id="property">
                            <td class="col-md-1">
                                <select class="form-control typeId" name="category_id[]" id="category_id">
                                   <option value="">Select Category</option>
                                    <?php $__currentLoopData = $categoryList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($category->category_id); ?>"><?php echo e($category->category_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </td>
                            <td class="col-md-2">
                                <select class="form-control nameId" name="product_id[]" id="product_id">
                                    <option value="">Select Product</option>
                                    <?php $__currentLoopData = $productList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($product->product_id); ?>"><?php echo e($product->product_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </td>
                            <td>
                                <?php echo Form::number('total_received[]', null,['class'=>'form-control','id'=>'total_received','placeholder'=>'Total Received']); ?>

                            </td>
                            <td>
                                <?php echo Form::number('quantity[]', null,['class'=>'form-control', 'id'=>'quantity','placeholder'=>'Quantity']); ?>

                            </td>   
                                                 
                            <td>
                                <input type="text" name="referance[]" id="referance" class="form-control" placeholder="Referance">
                            </td>
                            <td><?php echo Form::text('comments[]', null,['class'=>'form-control','placeholder'=>'Comments']); ?></td>

                            <td class="col-md-1">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary btn-add" type="button">
                                        Add <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </span>
                            </td>
                        </tr>
                        <tr id="newPara"></tr>
                    </tbody>
                </table>
                <div class="return-submit-button">
                    <button class="btn btn-success inventory-submit-button" type="submit"> Create </button>
                </div>
                <?php echo Form::close(); ?>   
            </div>
        </div>
    </div>

</div>
<?php echo $__env->make('modules.inventory.return-stock.stock-return-script', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make( 'backend.index' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>