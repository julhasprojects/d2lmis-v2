<?php $__env->startSection( 'content_area' ); ?>

<div class="page-title reports-title">
    <ul class="breadcrumb  col-md-8 col-xs-12 col-lg-8 col-sm-8 pull-left">
        <li class="completed"><a href="javascript:void(0);"> Dashboard </a></li>
        <li class="completed"><a href="javascript:void(0);"> Modules </a></li>
        <li class="completed"><a href="javascript:void(0);"> Inventory </a></li>
        <li class="active"><a href="javascript:void(0);"> Product Approval  </a></li>
    </ul>
</div>
<?php echo $__env->make('modules.inventory.master.navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('modules.inventory.master.error', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div id="main-wrapper">

	<div class="col-md-12 left-box">
		<div class="panel panel-white">
			<div class="panel panel-body">
				<h2 class="title-house"> Approve Products</h2>
			 	<?php echo e(csrf_field()); ?>

                <div class="table-responsive">
                    <table class="table table-hover table-bordered display" id="tableresponsive" style="width: 100%; cellspacing: 0;">
    	    			<thead class="table-header-bg">
                            <tr>
                                <th>S/N</th>
                                <th>Product Name</th>
                                <th>Recived By</th>
                                <th>Unit Price(Tk)</th>
                                <th>Received Qty</th>
                                <th>Total Amount</th>
                                <th>Warranty Period</th>
                                <th>Comments</th>
                                <th>Transaction Date</th>	                         
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $sn=1; ?>
                        <?php $__currentLoopData = $transactions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $transaction): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="odd gradeX" id="product_<?php echo e($transaction->transactionID); ?>">
                                <td><?php echo e($sn++); ?> </td>
                                <td><?php echo e($transaction->product_name); ?> </td>
                                <td><?php echo e($transaction->fullName); ?> </td>
                                <td><?php echo e($transaction->unitPrice); ?></td>
                                <td><?php echo e($transaction->quantity); ?></td>
                                <td><?php echo e($transaction->total_price); ?></td>
                                <td><?php echo e($transaction->warranty_period); ?></td>
                                <td><?php echo e($transaction->comments); ?></td>
                                <td>
                                    <?php if($transaction->approval_status  == 1): ?>
                                        <?php echo e($transaction->updated_at); ?>

                                    <?php else: ?>
                                        
                                    <?php endif; ?>
                                </td>
                                <?php if($transaction->approval_status == 0): ?> 
                                    <td>
                                        <span class="btn btn-warning btn-xs">Not Approve Yet</span>
                                    </td>
                                    <td>
                                        <span class="button-apporved">
                                            <button class="btn btn-success btn-xs status <?php echo e($transaction->id); ?>approved" href="javascript:;" onclick="approvedDisapprovedProduct('<?php echo e($transaction->invPID); ?>','<?php echo e($transaction->product_id); ?>','<?php echo e($transaction->quantity); ?>','<?php echo e($transaction->product_name); ?>','1','<?php echo e($transaction->transactionID); ?>','1')">
                                                <i class="glyphicon glyphicon-check"></i>
                                                Approve Now
                                            </button>
                                           
                                            <a class="btn btn-info btn-xs status <?php echo e($transaction->id); ?>approved"   data-toggle="modal" data-target="#editModal_<?php echo e($transaction->invPID); ?>">
                                                Edit<i class="fa fa-edit"></i>
                                            </a>
                                            
                                            <button class="btn btn-danger btn-xs status <?php echo e($transaction->id); ?>approved" href="javascript:;" onclick="approvedDisapprovedProduct('<?php echo e($transaction->invPID); ?>','<?php echo e($transaction->product_id); ?>','<?php echo e($transaction->quantity); ?>','<?php echo e($transaction->product_name); ?>','2','<?php echo e($transaction->transactionID); ?>','0')">
                                                <i class="glyphicon glyphicon-check"></i>
                                                Disapprove
                                            </button>
                                        </span>
                                    </td>   
                                <?php elseif($transaction->approval_status == 1): ?>
                                    <td>
                                        <span class="btn btn-success btn-xs">Approved</span>
                                    </td>
                                    <td></td>
                                <?php elseif($transaction->approval_status == 2): ?>
                                    <td>
                                        <span class="btn btn-danger btn-xs">Disapproved</span>
                                    </td>  
                                    <td></td>                                       
                                <?php endif; ?>
                             
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
			</div>
		</div>
	</div>
</div>
<?php $__currentLoopData = $transactions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $transaction): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div id="editModal_<?php echo e($transaction->invPID); ?>" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-times" aria-hidden="true"></i> </button>
                <h4 class="modal-title">Updated Product Information </h4>
            </div>
            <div class="modal-body modal-inventory" id="info">
                <?php echo Form::open(['url'=>'update/storesdetails/products/'.$transaction->invPID, 'class' =>'form col-md-12', 'enctype'=>'multipart/form-data','method'=>'PUT']); ?>

                <div class="form-gorups listproducts" >
                    <select class="form-control nameId" name="product_id" id="product_id">
                        <option value="">Select Product</option>
                        <?php $__currentLoopData = $productList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($product->product_id); ?>" <?php if($transaction->product_id == $product->product_id ): ?> selected <?php endif; ?> >
                            <?php echo e($product->product_name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                    
                </div>
                <div class="form-gorups listproducts">
                    
                </div>
                <div class="form-gorups listproducts">
                    <input type="text" value="<?php echo e($transaction->unitPrice); ?>" class="form-control unitPriceEdit" id="unitPriceEdit_<?php echo e($transaction->storeid); ?>" placeholder='Unit Price' name="unit_price">
                   
                </div>
                 <div class="form-gorups listproducts">
                    <input type="text" value="<?php echo e($transaction->salePrice); ?>" class="form-control unitPriceEdit" id="unitPriceEdit_<?php echo e($transaction->storeid); ?>" placeholder='Sale Price' name="sale_price">
                </div>
                
                <div class="form-gorups listproducts">
                    <input type="text" value="<?php echo e($transaction->quantity); ?>" class="form-control quantityEdit" id="quantityEdit_<?php echo e($transaction->storeid); ?>" placeholder='Quantity' name="quantity">
                </div>
                <div class="form-gorups listproducts">
                    <input type="text" value="<?php echo e($transaction->total_price); ?>" class="form-control totalPriceEdit" id="totalPriceEdit_<?php echo e($transaction->storeid); ?>" placeholder='Total Price' name="total_price" readonly>
                </div>
          
                <button type="button" data-dismiss="modal" class="btn default">Cancel</button>
                <button type="submit"  class="btn btn-success" > Update</button>
            <?php echo Form::close(); ?>

            </div>
            
        </div>
   </div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


<?php echo $__env->make('modules.inventory.master.master-edit', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('modules.inventory.master.approved', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<script>

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make( 'backend.index' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>