/*$(document).ready(function () {
	'use strict';
    $('#tableresponsive').DataTable({
       'paging' : true  ,
       "pageLength": 100,
       'searching' : true,
       "language": {
          "emptyTable": "No result found"
        }
    });
});*/

$(document).ready(function() {
    $('#tablCollection,#tableresponsive').DataTable( {
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    
                }
            },

            'colvis',
            'print'
        ],

        retrieve: true,
        language: {
          "emptyTable": "No result found"
        },
        pageLength: 100,
        paging: true,
        // sDom: "Rlfrtip",
        dom: 'Bfrtip',
    } );
    $('#tableComplain').DataTable( {
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    
                }
            },

            'colvis',
            'print'
        ],

        retrieve: true,
        language: {
          "emptyTable": "No result found"
        },
        pageLength: 500,
        paging: true,
        // sDom: "Rlfrtip",
        dom: 'Bfrtip',
    } );
$('#tableresponsivethree').DataTable( {
    buttons: [
        {
            extend: 'copyHtml5',
            exportOptions: {
                columns: [ 0, ':visible' ]
            }
        },
        {
            extend: 'excelHtml5',
            exportOptions: {
                columns: ':visible'
            }
        },
        {
            extend: 'pdfHtml5',
            exportOptions: {
                
            }
        },

        'colvis',
        'print'
    ],
    retrieve: true,
    language: {
      "emptyTable": "No result found"
    },
    pageLength: 100,
    paging: true,
    // sDom: "Rlfrtip",
    dom: 'Bfrtip',
} );
   
     $('#tableresponsivetwo').DataTable( {
        dom: 'Bfrtip',
        // sDom: "Rlfrtip",
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    
                }
            },

            'colvis',
            'print'
        ],
        retrieve: true,
        language: {
          "emptyTable": "No result found"
        },
        pageLength: 2000,
        paging: true,
    } );
} );
