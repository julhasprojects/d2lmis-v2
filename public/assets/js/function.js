/**
 * [Reset Button and cancel Modal ]
 * @param  {[type]} ) { $('#newlineReaqestForm').find('input[type [description]
 * @return {[type]}   [description]
 */
$("#newlineRequestReset,#newlineRequestCancel").on("click",function() {
    $('#newlineReaqestForm').find('input[type=text]').each(function() {
        $(this).val($(this).attr("data-default"));
    });
});


$("#resetCancel").on("click",function() {
    $('#allForm').find('input[type=text]').each(function() {
        $(this).val($(this).attr("data-default"));
    });
    location.reload(); 
});
function getIntegerValue(data) {

    if (typeof data === 'undefiend' || data == null || data == '') {

        return 0;

    } else {

        return parseInt(data);

    }
}
$("#billresetCancel").on("click",function() {
    $('#billAllForm').find('input[type=text]').each(function() {
        $(this).val($(this).attr("data-default"));
    });
    location.reload(); 
});

$("#packageResetCancel").on("click",function() {
    $('#packageallForm').find('input[type=text]').each(function() {
        $(this).val($(this).attr("data-default"));
    });
    location.reload(); 
});
$(function() {
  $(document).on('keydown', '#numberFormat,#relipstep,#realip,#endrealip', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
})

$("#houseResetCancel").on("click",function() {
    $('#packageallForm').find('input[type=text]').each(function() {
        $(this).val($(this).attr("data-default"));
    });
    location.reload(); 
});

/**
 * [Active Mikrotik description]
 * @param  {[type]} status [description]
 * @param  {[type]} id     [description]
 * @return {[type]}        [description]
 */
function activeMikrotik(status,id) {

    $('#pluginConfirmModel').appendTo("body").modal('show');
    var mikortikId   = $("#mikrotikId").val();
    var mikortikName = $("#mikrotikId").val();

    $('#pluginConfirmModel #info').html('<h2 class="mikrotik-confirm">Are you sure want to active in Microtik?</h2>');

    $('.modal-footer').on('click', '#confirm', function() {

        $('#loaderUPdated').append('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');
        
        var package_costing = $("#package_costing").val();
        var username        = $("#username").val();
        var preusername     = $("#preusername").val();
        var password        = $("#password").val();
        var firstname       = $("#firstname").val();
        var lastname        = $("#lastname").val();
        var due_bill        = $("#due_bill").val();
        var mac             = $("#mac").val();
        var mobilenumber    = $("#mob_num").val();
        var connction_date  = $("#connction_date").val();

        if( $("#realip").val() ==='undefined'  || $("#realip").val() == null || $("#realip").val() == '' ) {

            var realip = 0;
             
        }   else {

            var realip = $("#realip").val() ;

        }
      
        var find_info  ='?mikortikId='+ mikortikId +'&status='+status + '&mac='+mac + '&due_bill='+due_bill + '&firstname='+firstname + '&lastname='+lastname  +'&newconid='+id + '&packageid='+ package_costing + '&username='+username + '&preusername='+preusername + '&password='+password + '&realip='+realip+'&mobilenumber='+mobilenumber+'&connction_date='+connction_date; 

        $.ajax({
            type: "GET",
            url: basePath + 'update-new-line-request' + find_info,


        }).success(function ( result ) {

            $('#loader').slideUp(200,function(){        
                $('#loader').remove();
            });

            $(".loader").fadeOut("slow"); 

            if(result == 400) {
                
                swal("Error!", "Sorry! You can not active microtik without paying your due bill.", "error");
               
            } else if(result == 304) {

                swal("Error!", "Sorry! Please add Mac address before active in microtik.", "error");
            
            } else{

                swal("Good job!", "Thank you! Successfully activated in Microtik.", "success");
                $('.mikrotik-setting').hide();

            }

        }).error(function ( result ) {

            $('#loader').slideUp(200,function(){        
                $('#loader').remove();
            });
            
            $(".loader").fadeOut("slow"); 

            swal("Error!", "Sorry! Could not active in Microtik. Please try again.", "error");


        }); 
    });
}

function notifecationLoadPage() {

    $('#loaderNotefecation').append('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');
    $.ajax({
            type: "GET",
            url: basePath + 'isperp/notifecation/center' ,

    }).success(function ( result ) {

        $('#isperpNotify').html(result);

    }).error(function ( result ) {

    });
}

// Mikrotik Disable And Enalbe
function mikrotikDisableAndEnable(id,username,mikid) {
    if($("#switch_on_status_"+id).is(':checked')==true) {
        status = 1;
    } else {
        status = 0;  
    }

    var find_info  ='?id='+ id +'&username='+username+'&mikid='+mikid+'&status='+status;

    $.ajax({
            type: "GET",
            url: basePath + 'mikroik/user/on/off' + find_info,

    }).success(function ( result ) {
        
        swal("Good job!", "Thank you! Successfully Mikrotik opration ", "success");
     

    }).error(function ( result ) {

    
        swal("Error!", "Sorry! Could not opration in Microtik. Please try again.", "error");

    }); 
}
/**
 * [Active Mikrotik description]
 * @param  {[type]} status [description]
 * @param  {[type]} id     [description]
 * @return {[type]}        [description]
 */
function enableMikrotik(status,id) {

    $('#pluginConfirmModel').appendTo("body").modal('show');
    var mikortikId  = $("#mikrotikId").val();
    var mikortikName = $("#mikrotikId").val();

    $('#pluginConfirmModel #info').html('<h2 class="mikrotik-confirm">Are you sure want to active in Microtik?</h2>');

    $('.modal-footer').on('click', '#confirm', function() {

        $('#loaderUPdated').append('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');
        
        var package_costing = $("#package_costing").val();
        var username        = $("#username").val();
        var preusername     = $("#preusername").val();
        var password        = $("#password").val();
        var packageid       = $("#package_costing").val();
        var firstname       = $("#firstname").val();
        var lastname        = $("#lastname").val();
        var due_bill        = $("#due_bill").val();
        var mac             = $("#mac").val();
        var mobilenumber    = $("#mob_num").val();
        var mobilenumber    = $("#mob_num").val();
        var clientmikrotikId= $("#clientmikrotikId").val();

        if( $("#realip").val() ==='undefined'  || $("#realip").val() == null || $("#realip").val() == '' ) {

            var realip = 0;
             
        }   else {

            var realip = $("#realip").val() ;

        }
      
        var find_info  ='?clientmikrotikId='+ clientmikrotikId +'&status='+ status +'&packageid='+packageid + '&mac='+mac + '&due_bill='+due_bill + '&firstname='+firstname + '&lastname='+lastname  +'&newconid='+id + '&packageid='+ package_costing + '&username='+username + '&preusername='+preusername + '&password='+password + '&realip='+realip+'&mobilenumber='+mobilenumber; 

        $.ajax({
            type: "GET",
            url: basePath + 'enable-shorttime-desconnection-in-mikrotik' + find_info,


        }).success(function ( result ) {

            $('#loader').slideUp(200,function(){        
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow"); 

            if(result == 400) {
                
                swal("Error!", "Sorry! You can not active microtik without paying your due bill.", "error");
               
            } else if(result == 304) {

                swal("Error!", "Sorry! Please add Mac address before active in microtik.", "error");
            
            } else{

                swal("Good job!", "Thank you! Successfully activated in Microtik.", "success");
                $('.mikrotik-setting').hide();

            }



        }).error(function ( result ) {

            $('#loader').slideUp(200,function(){        
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow"); 

            swal("Error!", "Sorry! Could not active in Microtik. Please try again.", "error");


        }); 
    });
}

/**
 * [Active Mikrotik description]
 * @param  {[type]} status [description]
 * @param  {[type]} id     [description]
 * @return {[type]}        [description]
 */
function packageChangeMikrotik(clientID,mikrotikId,requestID) {

    $('#pluginConfirmModel').appendTo("body").modal('show');
    

    $('#pluginConfirmModel #info').html('<h2 class="mikrotik-confirm">Are you sure want to package change  in Microtik?</h2>');

    $('.modal-footer').on('click', '#confirm', function() {

        $('#loaderUPdated').append('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

        var find_info  ='?clientID='+ clientID + '&mikrotikId='+ mikrotikId + '&requestID=' + requestID; 

        $('#packageChangeLoader').append('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

        $.ajax({
            type: "GET",
            url: basePath + 'package/change/mikrotik' + find_info,


        }).success(function ( result ) {

            $('#loader').slideUp(200,function(){        
                $('#loader').remove();
            });

            $(".loader").fadeOut("slow"); 
            
            swal("Good job!", "Thank you! Successfully package change in Microtik.", "success");
            $('.mikrotik-setting').hide();
            $('#packagechangemikrotik_'+clientID).hide();

        }).error(function ( result ) {

            $('#loader').slideUp(200,function(){        
                $('#loader').remove();
            });
          
            $(".loader").fadeOut("slow"); 

            swal("Error!", "Sorry! Could not active in Microtik. Please try again.", "error");

        }); 
    });
}

/**
 * [Active Mikrotik description]
 */
function mikrotikChaneInmikrotik(clientID,mikrotikId,requestID) {

    $('#pluginConfirmModel').appendTo("body").modal('show');
    

    $('#pluginConfirmModel #info').html('<h2 class="mikrotik-confirm">Are you sure want to inactive this client in Microtik?</h2>');

    $('.modal-footer').on('click', '#confirm', function() {

        $('#loaderUPdated').append('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

        var find_info  ='?clientID='+ clientID + '&mikrotikId='+ mikrotikId + '&requestID=' + requestID; 

        $('#lineDisconnectLoader').append('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

        $.ajax({
            type: "GET",
            url: basePath + 'inactive/client/in/mikrotik' + find_info,

        }).success(function ( result ) {

            $('#loader').slideUp(200,function(){        
                $('#loader').remove();
            });
            $('.mikrtik_remove_'+clientID).hide();
            
            $(".loader").fadeOut("slow"); 

            swal("Good job!", "Thank you! Successfully inactive in Microtik.", "success");
            $('.mikrotik-setting').hide();


        }).error(function ( result ) {

            $('#loader').slideUp(200,function(){        
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow"); 

            swal("Error!", "Sorry! Could not inactive in Microtik. Please try again.", "error");

        }); 
    });
}

/* Mikrotik Migration */
function mikrotikMigration(clientID) {
    $('#confirmModal').appendTo("body").modal('show');
    

    $('#info p').html('<h2 class="mikrotik-confirm">Do you want to migrate this client?</h2>');

    $('.modal-footer').on('click', '#confirm', function() {

        var mikrotikId = $("#mikrotik_"+clientID).val();

        var find_info  ='?clientID='+ clientID + '&mikrotikId='+ mikrotikId; 

        $.ajax({

            type: "GET",
            url: basePath + 'mikrotik/user/migration/pppoe' + find_info,

        }).success(function ( result ) {

            $('#loader').slideUp(200,function(){        
                $('#loader').remove();
            });

            $(".loader").fadeOut("slow"); 

            swal("Good job!", "Thank you! Successfully migration in Microtik.", "success");
            location.reload();
        }).error(function ( result ) {

            $('#loader').slideUp(200,function(){        
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow"); 

            swal("Error!", "Sorry! Could not change in Microtik. Please try again.", "error");

        }); 
    });
}
/**
 * [cable Plug Description]
 */
function cablePlug(clientId,status) {

    $('#pluginConfirmModel').appendTo("body").modal('show');

    if($("#cable_status_"+clientId).is(':checked')==true) {

        statusplug = 1;

    } else {

        statusplug = 0;
        
    }
  
    // alert(status);
    if(statusplug == 0 ) {

        var stauscable = 'Plug';

    } else {

        var stauscable = 'Unplug';

    }

    $('#info p').html('Are you sure ! You want to   <strong>'+ clientId +'</strong>  Cable' + stauscable+ '?<br>' +'</div>'); 
  
    
    $('.modal-footer').on('click', '#delete', function() {

        var find_info ='?cableStatus='+ statusplug +'&clientId='+clientId; 

        $.ajax({

            type: "GET",
            url: basePath + 'cable/status/plug/unplug' + find_info

        }).success(function ( result ) {

            location.reload();
           
        }).error(function ( result ) {
            console.log('Information not found');
        }); 
    });
}

/**
 * [cable Plug Description]
 */
function realIpCostSetting(clientId) {

    $('#pluginConfirmModel').appendTo("body").modal('show');

  

    $('#info p').html('Are you sure ! You want to   <strong>'+ clientId +'</strong>  Cable' + stauscable+ '?<br>' +'</div>'); 
  
    
    $('.modal-footer').on('click', '#delete', function() {

        var find_info ='?cableStatus='+ status +'&clientId='+clientId; 

        $.ajax({

            type: "GET",
            url: basePath + 'cable/status/plug/unplug' + find_info

        }).success(function ( result ) {

            location.reload();
           
        }).error(function ( result ) {
            console.log('Information not found');
        }); 
    });
}
/**
 * [cable Plug Description]
 */
function permennectDisconnection(clientId,status) {

    $('#pluginConfirmModel').appendTo("body").modal('show');
    $('#info p').html('Are you sure you went to permannent disconnect? '); 

    $('.modal-footer').on('click', '#delete', function() {

        var find_info ='?status='+ status +'&clientId='+clientId; 

        $.ajax({

            type: "GET",
            url: basePath + 'permannent/inactive/in/mikrotik' + find_info

        }).success(function ( result ) {

            location.reload();
           
        }).error(function ( result ) {
            console.log('Information not found');
        }); 
    });
}


function clientComplainHistory(id) {

        var find_info ='?id='+ id; 
        $('#complainHistory').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

        $.ajax({

            type: "GET",
            url: basePath + 'client/complain/history' + find_info

        }).success(function ( result ) {

            $('#complainHistory').html(result);
            $('#loader').slideUp(200,function(){        
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow"); 

        }).error(function ( result ) {
            console.log('Information not found');
             $('#loader').slideUp(200,function(){        
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow"); 
        }); 
    
}


function packageChangeHistory(id) {

        var find_info ='?id='+ id; 
        $('#packageCangeHistory').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

        $.ajax({

            type: "GET",
            url: basePath + 'client/packagechange/history' + find_info

        }).success(function ( result ) {

            $('#packageCangeHistory').html(result);
            $('#loader').slideUp(200,function(){        
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow"); 

        }).error(function ( result ) {
            console.log('Information not found');
             $('#loader').slideUp(200,function(){        
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow"); 
        }); 
    
}

function clienthouseChangeHistory(id) {

        var find_info ='?id='+ id; 
        $('#houseCangeHistory').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

        $.ajax({

            type: "GET",
            url: basePath + 'client/housechange/history' + find_info

        }).success(function ( result ) {

            $('#houseCangeHistory').html(result);
            $('#loader').slideUp(200,function(){        
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow"); 

        }).error(function ( result ) {
            console.log('Information not found');
             $('#loader').slideUp(200,function(){        
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow"); 
        }); 
    
}


function clientDisconnectionHistory(id) {

        var find_info ='?id='+ id; 
        $('#disconnectionHistory').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

        $.ajax({

            type: "GET",
            url: basePath + 'client/disconnection/history' + find_info

        }).success(function ( result ) {

            $('#disconnectionHistory').html(result);
            $('#loader').slideUp(200,function(){        
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow"); 

        }).error(function ( result ) {
            console.log('Information not found');
             $('#loader').slideUp(200,function(){        
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow"); 
        }); 
    
}
function newlineConnectionFeee(newlineID,newconid) {

    var find_info ='?newlineID='+ newlineID+'&newconid='+ newconid; 

    $.ajax({

        type: "GET",
        url: basePath + 'connectionfee/change/update' + find_info

    }).success(function ( result ) {

        $('#updateConnectionFeePiad').appendTo("body").modal('show');
        $('#connectionFeeResult').html(result);

        $("#connectionFeeUpdate").on('click',function(){
            var  editConnectionFee = $('#editConnectionFee').val();
            var  editConnectionFeePaid = $('#editConnectionFeePaid').val();
            var  editConnectionFeeDue   = $('#editConnectionFeeDue').val();
            var  editConnectionFeeAdjust = $('#editConnectionFeeAdjust').val();
            



            var values ='?newlineID='+ newlineID+'&newconid='+ newconid+'&editConnectionFee='+ editConnectionFee+'&editConnectionFeePaid='+ editConnectionFeePaid+'&editConnectionFeeDue='+ editConnectionFeeDue+'&editConnectionFeeAdjust='+ editConnectionFeeAdjust;
            // alert(values);
            $.ajax({
                type: "GET",
                url: basePath + 'connectionfee/collection/update' + values

            }).success(function ( result ) {

                window.location=basePath+'frontend-edit-new-line-request/'+result+'#packageInformation';
                 swal("Good job!", "Thank you! Successfully client Connection Charge.", "success");
            }).error(function ( result ) { }); 
        })
        
    }).error(function ( result ) {
       
    }); 
   
}
function clientBillPaymentHistory(id) {

    var find_info ='?id='+ id; 
    $('#clientBillPaymentHistoryResult').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

    $.ajax({

        type: "GET",
        url: basePath + 'client/billpayment/history' + find_info

    }).success(function ( result ) {

        $('#clientBillPaymentHistoryResult').html(result);
        $('#loader').slideUp(200,function(){        
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 

    }).error(function ( result ) {
        console.log('Information not found');
         $('#loader').slideUp(200,function(){        
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 
    }); 
    
}
function clientReconnectionHistory(id) {

    var find_info ='?id='+ id; 
    $('#clientReconnectionHistoryResult').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

    $.ajax({

        type: "GET",
        url: basePath + 'client/reconnection/history' + find_info

    }).success(function ( result ) {

        $('#clientReconnectionHistoryResult').html(result);
        $('#loader').slideUp(200,function(){        
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 

    }).error(function ( result ) {
        console.log('Information not found');
         $('#loader').slideUp(200,function(){        
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 
    }); 
    
}

/**
 * [cable Plug Description]
 */
function permennectDisconnection(clientId,status) {

    $('#pluginConfirmModel').appendTo("body").modal('show');
    $('#info p').html('Are you sure you went to permannent disconnect? '); 

    $('.modal-footer').on('click', '#delete', function() {

        var find_info ='?status='+ status +'&clientId='+clientId; 

        $.ajax({

            type: "GET",
            url: basePath + 'permannent/inactive/in/mikrotik' + find_info

        }).success(function ( result ) {

            location.reload();
           
        }).error(function ( result ) {
            console.log('Information not found');
        }); 
    });
}

function smsGlobalSetting(id) {

    $('#confirmModal').appendTo("body").modal('show');
    $('#info p').html('Are you sure you went to Confirm? '); 
    
    $('.modal-footer').on('click', '#delete', function() {

        var find_info ='?id='+ id +'&clientId='+clientId; 

        $.ajax({

            type: "GET",
            url: basePath + 'permannent/inactive/in/mikrotik' + find_info

        }).success(function ( result ) {

            location.reload();
           
        }).error(function ( result ) {
            console.log('Information not found');
        }); 
    });
}
/**
 * [Reconnection Option]
 */
function clientsReconnect(clientId,id) {

    $('#reconnetModel').appendTo("body").modal('show');
    $('.client-history-modal-lg').appendTo("body").modal('hide');
    $('.client-history-modal-lg').removeClass("in");
    $('.client-history-modal-lg').hide();
    // $('#info p').html('Are you sure you went to Reconnect? '); 

    function getIntegerValue(data) {

        if (typeof data === 'undefiend' || data == null || data == '') {

            return 0;

        } else {

            return parseInt(data);

        }
    }

    var reconnectInfo ='?id='+ id;

    $.ajax({

        type: "GET",
        url: basePath + 'client/reconnect/bill/generate' + reconnectInfo,
        data: {
            '_token': $('input[name=_token]').val(),
            'id': id
        },

    }).success(function ( result ) {

        $('#reconnectionPanel').html(result);
       
    })

    $('.modal-footer').on('click', '#confirm', function() {

       
        var reconnectDate = $('#reconnectDate').val();
        var status = $('#status').val();
        var remarks = $('#remarks').val();
        var RecconnectionConnectionFee = getIntegerValue($('#RecconnectionConnectionFee').val());

        var values ='?id='+ id +'&clientId='+clientId+'&remarks='+remarks +'&reconnectDate='+reconnectDate +'&RecconnectionConnectionFee='+RecconnectionConnectionFee+'&status='+status; 

        if(reconnectDate=='') {

            swal("Sorry!", "Reconnect date can not be empty. Plesae select Reconnect date.", "error");
        
        } else {

            $.ajax({

                type: "GET",
                url: basePath + 'client/reconnect/information' + values,
                data: {
                    '_token': $('input[name=_token]').val(),
                    // 'id': id
                },

            }).success(function ( result ) {

                swal("Good job!", "Thank you! Successfully reconnect clients.", "success");
                window.location=basePath+'current/month/active/connections/1';
                // location.reload();
               
            }).error(function ( result ) {

                console.log('Information not found');
            }); 
        };
    });
}


// Bill generate collectoin 
function billGenerateLoader() {
    $('#billGenerateModel').appendTo("body").modal('show');
   
    $('.modal-footer').on('click', '#billGeneratedSubmit', function() {
        
        $('#billloaderGenerated').append('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');
    
        var billmonth = $('#billmonth').val();
        var billyear  = $('#billyear').val();
        var issudate  = $('#issudate').val();
        var discount  = $('#discount').val();
        var lastdate  = $('#lastdate').val();
        var remarks   = $('#remarks').val();


        var values ='?billmonth='+ billmonth +'&billyear='+billyear +'&issudate='+issudate +'&discount='+discount+'&lastdate='+lastdate +'&remarks='+remarks; 

        if(issudate=='') {

            swal("Sorry!", "Issu date can not be empty. Plesae select Issu date.", "error");
        
        } else {

            $.ajax({

                type: "POST",
                url: basePath + 'add-bill-all' + values,
                data: {
                    '_token': $('input[name=_token]').val(),
                    // 'id': id
                },

            }).success(function ( result ) {

                swal("Good job!", "Thank you! Successfully bill generated.", "success");
                window.location=basePath+'bill-generate';

                $('#loader').slideUp(200,function() {        
                    $('#loader').remove();
                });
                
                $(".loader").fadeOut("slow"); 
               
            }).error(function ( result ) {

                swal("Sorry!", "Unsuccessfully bill generated.", "error");
                // window.location=basePath+'bill-generate';
                $('#loader').slideUp(200,function() {        
                    $('#loader').remove();
                });
                $(".loader").fadeOut("slow"); 
            }); 
        };
    });
}
/**
 * [description]
 * @param  {[type]} ) {               if($('#realIpChageSetup').is(':checked') [description]
 * @return {[type]}   [description]
 */
$(document).on('click', '#realIPStatus', function() {

    if($('#realIPStatus').is(':checked') == true) {

       $('.realipstatus').show(500); 

    } else {

        $('.realipstatus').hide(500);

    }

});

/**
 * [description]
 * @param  {[type]} ) {               if($('#realIpChageSetup').is(':checked') [description]
 * @return {[type]}   [description]
 */
$(document).on('click', '#passwordPattern', function() {
    var passowrd = $(this).val();
    if(passowrd==3) {
        $("#defualtpassword").html('<input type="text" class="form-control" name="defualtpasswords" placeholder="Defualt Password">');
    } else {
        $("#defualtpassword").html('');
    }
});
/**
 * [description]
 * @param  {[type]} ) {               if($('#realIpChageSetup').is(':checked') [description]
 * @return {[type]}   [description]
 */
$(document).on('click', '#realIpChageSetup', function() {

    if($('#realIpChageSetup').is(':checked') == true) {

        $('#realIPCharge').val(200);

    } else {

        $('#realIPCharge').val(0);

    }

});

/**
 * [description]
 * @param  {[type]} ) {               var form [description]
 * @return {[type]}   [description]
 */
$(document).on('click dp.change change', '#defualterDateform,#defualterDateTo', function() {

    var form = $('#defualterDateform').val();
    var to   = $('#defualterDateTo').val();

    if($('#duefualterDueSetup').is(':checked') == true) {
        var realip = 200;
    } else {
        var realip = 0;
    }

    var oneDay     = 24*60*60*1000;
    var firstDate  = new Date($("#defualterDateform").val());
    var secondDate = new Date($("#defualterDateTo").val());

    var diffDays   = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay))) + 1;
    
    $('#defaulterDay').val(diffDays);
 
    $('#defaulterDay').val(diffDays);

    var billno                 = $('#billno').val();
    var spDiscount             = $('#spDiscount').val();
    var defualterDateform      = $('#defualterDateform').val();
    var defualterDateTo        = $('#defualterDateTo').val();

    var values ='?billno='+ billno + '&spDiscount='+ spDiscount + '&defualterDateform='+ defualterDateform + '&defualterDateTo='+ defualterDateTo + '&diffDays='+ diffDays ; 

    $.ajax({

        type: "GET",
        url: basePath + 'defualter/bill/monthly/billgenerate' + values,
        data: {
            '_token': $('input[name=_token]').val(),
            'id': billno
        },

    }).success(function ( result ) {

        $('#reBillGenerateMonthly').val(result);

        var monthlyAmount = $('#reBillGenerateMonthly').val();
        var preDue   = $('#preDue').val()

        var totalReAmount = (+monthlyAmount+ +preDue+ +realip) ;
        $('#reTotalAmount').val(totalReAmount)

    }).error(function ( result ) {
        console.log('Information not found');
    }); 
});

$(document).on('change click', '#defualterClose', function() { 
    location.reload();

});

/**
 * [description]
 * @param  {[type]} ) {               var oneDay [description]
 * @return {[type]}   [description]
 */
$(document).on('change click', '#reBillGeneratedDefulater', function() {

    var oneDay     = 24*60*60*1000;
    var firstDate  = new Date($("#defualterDateform").val());
    var secondDate = new Date($("#defualterDateTo").val());

    var diffDays   = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay))) + 1;
    
    if($('#duefualterDueSetup').is(':checked') == true) {
        var realip = 200;
    } else {
        var realip = 0;
    }

    $('#defaulterDay').val(diffDays);

    var billno                 = $('#billno').val();
    var spDiscount             = $('#spDiscount').val();
    var defualterDateform      = $('#defualterDateform').val();
    var defualterDateTo        = $('#defualterDateTo').val();

    var values     ='?billno='+ billno + '&spDiscount='+ spDiscount + '&defualterDateform='+ defualterDateform + '&defualterDateTo='+ defualterDateTo + '&diffDays='+ diffDays ; 

    $.ajax({

        type: "GET",
        url: basePath + 'defualter/bill/monthly/billgenerate' + values,
        data: {
            '_token': $('input[name=_token]').val(),
            'id': billno
        },

    }).success(function ( result ) {

        $('#reBillGenerateMonthly').val(result);

        var monthlyAmount = $('#reBillGenerateMonthly').val();
        var preDue   = $('#preDue').val()

        var totalReAmount = (+monthlyAmount+ +preDue+ +realip) ;
        $('#reTotalAmount').val(totalReAmount)

    }).error(function ( result ) {
        console.log('Information not found');
    }); 
});

/**
 * [description]
 * @param  {[type]} )                    {                        var billno [description]
 * @param  {[type]} }).success(function (             result ) {                              if(result [description]
 * @return {[type]}                      [description]
 */
$(document).on('change click', '#billDefualterRegenrated', function() {

    var billno             = $('#billno').val();
    var monthly            = $('#reBillGenerateMonthly').val();
    var defualterDateform  = $('#defualterDateform').val();
    var totalAmount        = $('#reTotalAmount').val();
    var defualterDateTo    = $('#defualterDateTo').val();
    var deRemarks          = $('#deRemarks').val();

    var values ='?billno='+ billno + '&monthly='+ monthly +'&totalAmount='+ totalAmount  +'&deRemarks='+ deRemarks +'&defualterDateTo='+ defualterDateTo +'&defualterDateform='+ defualterDateform; 
    
    $.ajax({

        type: "POST",
        url: basePath + 'defualter/rebillgenerate/monthly' + values,
        data: {
            '_token': $('input[name=_token]').val(),
         
        },

    }).success(function ( result ) {
        if(result == 200) {
            swal("Good job!", "Thank you! Successfully Re-billgenerate.", "success"); 
        } else {
            swal("Sorry!", "Something is wrong.", "error");
        }

       
    }).error(function ( result ) {

        swal("Sorry!", "Something is wrong.", "error");
    }); 

});

/**
 * [description]
 * @param  {[type]} )                    {                        var billno [description]
 * @param  {[type]} }).success(function (             result ) {                              if(result [description]
 * @return {[type]}                      [description]
 */
$(document).on('change click', '#todayBillSummery', function() {

    var tocollectaionDate   = $('#tocollectaionDate').val();
    var fromcollectaionDate = $('#fromcollectaionDate').val();
      $('#todayCollectionSummeryLoader').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');
        
    var values ='?tocollectaionDate='+ tocollectaionDate + '&fromcollectaionDate='+ fromcollectaionDate ; 
    
    $.ajax({

        type: "get",
        url: basePath + 'collections/bill/summery/search' + values,
       

    }).success(function ( result ) {
         $('#loader').slideUp(200,function(){        
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 

        $('#todayBillSummeryResult').html(result);
       
    }).error(function ( result ) {

        swal("Sorry!", "Something is wrong.", "error");
    }); 

});
/**
 * [description]
 * @param  {[type]} )                    {                        var billno [description]
 * @param  {[type]} }).success(function (             result ) {                              if(result [description]
 * @return {[type]}                      [description]
 */
$(document).on('change click', '#billSummeryMonthSearch', function() {

    var dailySummeryMonth   = $('#dailySummeryMonth').val();
    var dailySummeryYear = $('#dailySummeryYear').val();
      $('#todayCollectionSummeryLoader').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');
        
    var values ='?dailySummeryMonth='+ dailySummeryMonth + '&dailySummeryYear='+ dailySummeryYear ; 
    
    $.ajax({

        type: "get",
        url: basePath + 'collections/bill/summery/monthly/search' + values,
       

    }).success(function ( result ) {
         $('#loader').slideUp(200,function(){        
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 

        $('#todayBillSummeryResult').html(result);
       
    }).error(function ( result ) {

        swal("Sorry!", "Something is wrong.", "error");
    }); 

});
/**
 * [description]
 * @param  {[type]} )                    {                        var billno [description]
 * @param  {[type]} }).success(function (             result ) {                              if(result [description]
 * @return {[type]}                      [description]
 */
$(document).on('click', '#defaulterMonthlySearch', function() {

    var monthly   = $('#monthly').val();
    var year      = $('#year').val();

    $('#defaulterSearch').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');
        

    var values ='?year='+ year + '&monthly='+ monthly; 
    
    $.ajax({

        type: "GET",
        url: basePath + 'defaulter/monthly/search' + values,
        data: {
            '_token': $('input[name=_token]').val(),
         
        },

    }).success(function ( result ) {
        // Remove search result
  
        $('#loader').slideUp(200,function(){        
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 

        $('#defaulterList').html(result);
       
    }).error(function ( result ) { }); 

});


/**
 * [description]
 * @param  {[type]} )                    {                        var billno [description]
 * @param  {[type]} }).success(function (             result ) {                              if(result [description]
 * @return {[type]}                      [description]
 */
$(document).on('keyup', '#mikrotikChangeSearch', function() {

    var mikrotikChangeSearch   = $('#mikrotikChangeSearch').val();

    $('#mikrotikChangeSearchLoader').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');
        

    var values ='?mikrotikChangeSearch='+ mikrotikChangeSearch; 
    
    $.ajax({

        type: "GET",
        url: basePath + 'mikrotik/user/migration/search' + values,
        data: {
            '_token': $('input[name=_token]').val(),
         
        },

    }).success(function ( result ) {
        // Remove search result
        
        $('#loader').slideUp(200,function(){        
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 

        $('#mikrotikChangeSearchResult').html(result);
       
    }).error(function ( result ) { }); 

});
/**
 * [description]
 * @param  {[type]} )                    {                        var billno [description]
 * @param  {[type]} }).success(function (             result ) {                              if(result [description]
 * @return {[type]}                      [description]
 */
$(document).on('click', '#defaulterRebackMonthlySearch', function() {

    var monthly   = $('#rebackMonthly').val();
    var year      = $('#rebackYear').val();

    $('#defaulterRebackSearch').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');
        

    var values ='?year='+ year + '&monthly='+ monthly; 
    
    $.ajax({

        type: "GET",
        url: basePath + 'defaulter/reback/monthly/search' + values,
        data: {
            '_token': $('input[name=_token]').val(),
         
        },

    }).success(function ( result ) {
        // Remove search result
        
        $('#loader').slideUp(200,function(){        
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 

        $('#defaulterRebackList').html(result);
       
    }).error(function ( result ) { }); 

});

function dueAdjustmentOption(specalDiscount,rfdiscount,pkdiscount,totalamount,prvPaid,totalamountpaid,adjustment) {
    var toalPaid = (+prvPaid+ +adjustment+ +totalamountpaid) -rfdiscount ;
    var totalDue = +totalamount - +toalPaid;
    $("#dueCollectiondue").val(totalDue);
}

$(document).on('keyup', '#dueCollectionPaid,#dueCollectiondueaddjustemnts', function() {

    var specialDiscount              = $('#specialDiscount').val();
    var rfdiscount                   = $('#referenceDiscount').val();
    var pkgdiscount                  = $('#dueCollectionpackageDisocunt').val();
    var totalamount                  = $('#dueCollectionTotalAmount').val();
    var prvPaid                      = $('#dprvPaid').val();
    var dueCollectedPaid             = $('#dueCollectionPaid').val();
    var adjustment                   = $('#dueCollectiondueaddjustemnts').val();

    dueAdjustmentOption(specialDiscount,rfdiscount,pkgdiscount,totalamount,prvPaid,dueCollectedPaid,adjustment) 

});
/**
 * [Reconnection Option]
 */
function clientsUnblock(clientId,id) {

  
    $('#reconnetModel').appendTo("body").modal('show');
    // $('#info p').html('Are you sure you went to Reconnect? '); 

    $('.modal-footer').on('click', '#confirm', function() {

        var reconnectDate = $('#reconnectDate').val();

        var values ='?id='+ id +'&clientId='+clientId +'&reconnectDate='+reconnectDate; 

        if(reconnectDate=='') {

            swal("Sorry!", "Reconnect date can not be empty. Plesae select Reconnect date.", "error");
        
        } else {

            $.ajax({

                type: "GET",
                url: basePath + 'client/unblock/information' + values,
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': id
                },

            }).success(function ( result ) {

                swal("Good job!", "Thank you! Successfully unblock clients.", "success");
                // window.location=basePath+'current/month/active/connections/1';
                location.reload();
               
            }).error(function ( result ) {

                console.log('Information not found');
            }); 
        };
    });
}

/**
 * [defualterFromSubmit description]
 * @return {[type]} [description]
 */
function defualterFromSubmit() {

    function getIntegerValue(data) {

        if (typeof data === 'undefiend' || data == null || data == '') {

            return 0;

        } else {

            return parseInt(data);

        }
    }


    var billno           = getIntegerValue($('#billno').val());
    var clientid         = getIntegerValue($('#clientid').val());
    var applicationDate  = $('#applicationDate').val();
    var paybleDate       = $('#paybleDate').val();

    var values ='?clientid='+ clientid +'&billno='+billno +'&applicationDate='+applicationDate +'&paybleDate='+paybleDate;
    $.ajax({

        type: "POST",
        url: basePath + 'defaulter/continuation/application/request' + values,
        data: {
            '_token': $('input[name=_token]').val(),
            'id': clientid
        },

    }).success(function ( result ) {
        if(result!=401) {

            swal("Good job!", "Thank you! Successfully Defaulter Continuation Application.", "success"); 

       } else {

            swal("Sorry!", "Your! Defaulter Continuation Application has already existing!.", "error");

       }
 
    }).error(function ( result ) {

        console.log('Information not found');
    }); 
  
}
/**
 * [ressellerBillGenerated description]
 * @return {[type]} [description]
 */
function realIPcollectionSubmit() {

    function getIntegerValue(data) {

        if (typeof data === 'undefiend' || data == null || data == '') {

            return 0;

        } else {

            return parseInt(data);

        }
    }

    if($('#realIpChageSetup').is(':checked') == true) {

        var realIPChargeStatus = 1;

    } else {

        var realIPChargeStatus = 0;

    }

    if($('#realIPStatus').is(':checked') == true) { 
        var realIPStatus = 1;
    } else {
        var realIPStatus = 0;
    }
    var billno        = getIntegerValue($('#billno').val());
    var clientid      = getIntegerValue($('#clientid').val());
    var realIPID      = getIntegerValue($('#realIPID').val());
    var collectedDate = $('#collectedDate').val();
    var collectedBy   = getIntegerValue($('#collectedBy').val());
    var realIPCharge  = getIntegerValue($('#realIPCharge').val());
    var changedDate   = $('#changedDate').val();
    
    if(realIPID != '0' || realIPStatus==0) {

        var values ='?clientid='+ clientid +'&changedDate='+ changedDate +'&realIPStatus='+ realIPStatus +'&realIPID='+ realIPID +'&realIPChargeStatus='+ realIPChargeStatus +'&collectedDate='+collectedDate+'&collectedBy='+collectedBy+'&billno='+billno+'&realIPCharge=' +realIPCharge; 
        $.ajax({

            type: "POST",
            url: basePath + 'real/ip/charge/request' + values,
            data: {
                '_token': $('input[name=_token]').val(),
                'id': clientid
            },

        }).success(function ( result ) {
            if(result!=401) {

                swal("Good job!", "Thank you! Successfully Real IP Request.", "success"); 

           } else {

                swal("Sorry!", "Your! Rel IP request has already existing!.", "error");

           }
            window.location=basePath+'real/ip/all/request'
        }).error(function ( result ) {

           swal("Sorry!", "Your! Rel IP request has wrong.", "error");
        }); 

       

    } else {

        swal("Sorry!", "Real IP  can not be empty. Plesae enter Real IP", "error");
    }

};

/**
 * [Reconnection Option]
 */
function ressellerBillGenerated(clientId,billno,preamount,currentMonth,totalAmount,paidAmount,dueAmount) {

    $('#confirmModal').appendTo("body").modal('show');
    $('#resellerPage').html('<div class="row"> <div class="col-md-4 col-xs-12 col-lg-4"> <label class="control-label" for=""> Pre Month Due: </label> </div><div class="col-md-8 col-xs-12 col-lg-8"> <div class="form-group"> <input id="rspreDue" value="'+preamount +'" class="form-control" type="text"> </div></div></div><div class="row"> <div class="col-md-4 col-xs-12 col-lg-4"> <label class="control-label" for=""> Monthly Amount: </label> </div><div class="col-md-8 col-xs-12 col-lg-8"> <div class="form-group"> <input id="rscurma" value="'+currentMonth +'" class="form-control" type="text"> </div></div></div><div class="row"> <div class="col-md-4 col-xs-12 col-lg-4"> <label class="control-label" for=""> Total Amount: </label> </div><div class="col-md-8 col-xs-12 col-lg-8"> <div class="form-group"> <input id="rstotalamount" value="'+totalAmount +'" class="form-control" type="text" readonly> </div></div></div><div class="row"> <div class="col-md-4 col-xs-12 col-lg-4"> <label class="control-label" for=""> Paid Amount: </label> </div><div class="col-md-8 col-xs-12 col-lg-8"> <div class="form-group"> <input id="rsPaidamount" value="'+paidAmount +'" class="form-control" type="text" readonly> </div></div></div><div class="row"> <div class="col-md-4 col-xs-12 col-lg-4"> <label class="control-label" for=""> Due Amount: </label> </div><div class="col-md-8 col-xs-12 col-lg-8"> <div class="form-group"> <input id="rsdueAmount" value="'+dueAmount +'" class="form-control" type="text" readonly> </div></div></div>'); 

    $('#rspreDue,#rscurma').keyup(function(){

       var reslllerDue   = getIntegerValue($("#rspreDue").val());
       var rsdueAmount   = getIntegerValue($("#rsdueAmount").val());
       var rsPaidamount  = getIntegerValue($("#rsPaidamount").val());
       var reslllercurma = $("#rscurma").val();
       var totalAmount   = +reslllerDue +  +reslllercurma; 

       $('#rstotalamount').val(totalAmount);
       $('#rsdueAmount').val(totalAmount-rsPaidamount);

    });


    $('.modal-footer').on('click', '#confirm', function() {
        var reslllerDue       = $("#rspreDue").val();
        var reslllercurma     = $("#rscurma").val();
        var reslllerDueAmount = $("#rsdueAmount").val();

        var rstotalAmount = $('#rstotalamount').val();
   
        var values ='?billno='+ billno +'&clientId='+clientId+'&reslllerDue='+reslllerDue+'&reslllercurma='+reslllercurma+'&rstotalAmount='+rstotalAmount+'&rsdueAmount='+rsdueAmount +'&reslllerDueAmount='+reslllerDueAmount ; 
            $.ajax({
                type: "GET",
                url: basePath + 'bill/generated/update/information' + values,
                data: {
                    // '_token': $('input[name=_token]').val(),
                    'id': billno
                },

            }).success(function ( result ) {
                swal("Good job!", "Thank you! Successfully update billis.", "success");
                // window.location=basePath+'reseller/generated/bills';
                location.reload();
               
            }).error(function ( result ) {

                console.log('Information not found');
            }); 
     
    });
}
/**
 * [cable Plug Description]
 */
function blockConfrim(clientId,id) {

    $('#confermationClients').appendTo("body").modal('show');
    $('#info p').html('Are you sure you want to Inactive From Mikrotik ? '); 


}

/* Global Setting Enable */
function globalSettingEnable(type,status,message) {

    $('#pluginConfirmModel').appendTo("body").modal('show');

    if($('#globalSetting_'+type).is(':checked') == true) {
        var statusone = '1';
    } else {
        var statusone = '0' ;
    }
    $('#info p').html(message); 
  
    
    $('.modal-footer').on('click', '#delete', function() {

        var find_info ='?status='+ statusone +'&type='+type; 

        $.ajax({
            type: "GET",
            url: basePath + 'global/system/all' + find_info,
            data: {
                '_token': $('input[name=_token]').val(),
                'id': status
            },

        }).success(function ( result ) {

            location.reload();

            if(result == 101 ) {

                swal("Good job!", "Congratulations! Your Mikrotik Setup successfully.", "success"); 

            } else if(result == 102) {

                swal("Error!", "Sorry! Can not take your request. Please check yourdata quality.", "error");

            } else if(result == 201) {

                swal("Good job!", "Congratulations! Your SMS Setup successfully.", "success");  

            } else if(result == 202) {

                swal("Error!", "Sorry! Can not take your request. Please check yourdata quality.", "error");

            } else if(result == 501) {

                swal("Good job!", "Congratulations! Your Bill Setup successfully.", "success");  

            } else if(result == 502) {

                swal("Error!", "Sorry! Can not take your request. Please check yourdata quality.", "error");

            } else if(result == 601) {

                swal("Good job!", "Congratulations! Your Package Charge Setup successfully.", "success"); 

            } else if(result == 602){

                swal("Error!", "Sorry! Can not take your request. Please check yourdata quality.", "error");
                
            } else if(result == 1001){

                swal("Error!", "Congratulations! Your Package Charge Setup successfully.");
                
            }
            
        }).error(function ( result ) {
            console.log('Su');
        }); 
    });
}

/**
 * Logout function 
 */
function logout(message) {

    $('#logOutModal').appendTo("body").modal('show');

    $('#info p').html(message); 
  
    
    $('.modal-footer').on('click', '#logoutSubmit', function() {

        $('#mobile-script').append('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');
        
        $.ajax({

            type: "GET",
            url: basePath + 'auth/logout' ,
         
        }).success(function ( result ) {

            $('#loader').slideUp(200,function() {       
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow");
            location.reload();

            // swal("Good job!", "Congratulations! Logout Successfully   .", "success"); 
            
        }).error(function ( result ) {

            $('#loader').slideUp(200,function() {       
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow");

             swal("Sorry!", "", "eroor"); 
        }); 
    });
}

/**
 * Mobile number Validation
 */
function mobileNumberFormat(status,message) {

    $('#pluginConfirmModel').appendTo("body").modal('show');

    $('#info p').html(message); 
  
    
    $('.modal-footer').on('click', '#delete', function() {
        $('#mobile-script').append('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');
        $.ajax({

            type: "GET",
            url: basePath + 'convert/mobilenumber' ,
         
        }).success(function ( result ) {

            $('#loader').slideUp(200,function() {       
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow");
            location.reload();

            swal("Good job!", "Congratulations! Your Mobile number format Successfully  .", "success"); 
            
        }).error(function ( result ) {
            $('#loader').slideUp(200,function() {       
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow");

             swal("Sorry!", "Your Mobile number format no Successfully  .", "eroor"); 
        }); 
    });
}

// Filed length limet condition
function checkNumberFieldLength(elem) {

	if (elem.value.length > 11) {

        elem.value = elem.value.slice(0,11); 

    }
}

/* Setting Delete */
function settingDelete(id,name) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure ! You want to delete <strong>'+ name +'</strong> ? <br>' + '</div>');

    $('.modal-footer').on('click', '#delete', function() {
       $('#deletenotification').html("<div class='alert alert-success text-center'><i class='fa fa-check'></i>"  + name + " Deleted successfully</div>");
        
        $('.setting_' + id ).remove();
        $.ajax({
            type: 'DELETE',
            url: basePath + 'sms/management/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function(data) { }
        });
    });
}

/* Complain Setting Delete */
function complainSettingDestroy(id,name) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure want to delete this data?');

    $('.modal-footer').on('click', '#delete', function() {
     
        $('.complain_' + id ).remove();
        $.ajax({
            type: "POST",
            url: basePath + 'complain/setting/delete/' +id,
           
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
        }).success(function ( result ) {

            swal("Thank you!", "Your complain type has deleted successfully.", "success");
        
        }).error(function ( result ) {

            swal("Sorry", "We can't delete complain type. Please try again!.", "error");

        }); 
       
    });
}

/* Complain Setting Delete */
function complainTypeDestroy(id,name) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure want to delete this data?');
var value ='?id='+ id;
    $('.modal-footer').on('click', '#delete', function() {
     
        $('.complain_stting_' + id ).remove();
        $.ajax({
            type: "GET",
            url: basePath + 'complain/table/setting/delete' +value,
           
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
        }).success(function ( result ) {

            swal("Thank you!", "Your complain table setting has deleted successfully.", "success");
        
        }).error(function ( result ) {

            swal("Sorry", "We can't delete complain table setting. Please try again!.", "error");

        }); 
       
    });
}
/* Complain Setting Delete */
function defualterApplicationApproved(id,status) {

    $('#confirmModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure want to approved application?');

    $('.modal-footer').on('click', '#confirm', function() {

        var value ='?id='+ id +'&status='+ status;

        $('.complain_' + id ).remove();
        
        $.ajax({

            type: "GET",
            url: basePath + 'defaulter/continuation/application/approved' +value,
           
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },

        }).success(function ( result ) {
            if(status==1) {
                 swal("Thank you!", "Defaulter Application  has approved successfully.", "success");  
            } else {
                swal("Thank you!", "Defaulter Application  has disapproved successfully.", "success");  
            }
           
              // location.reload();
        }).error(function ( result ) {

            swal("Sorry", "We can't  Defaulter Application. Please try again!.", "error");

        }); 
     
    });
}

/* Defualter Appliation disapproved */
function defualterApplicationdisApproved(id,status) {
    $('#confirmModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure want to approved application?');

    $('.modal-footer').on('click', '#confirm', function() {

        var value ='?id='+ id +'&status='+ status;

        $('.complain_' + id ).remove();
        $.ajax({
            type: "GET",
            url: basePath + 'defaulter/continuation/application/disapproved' +value,
           
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
        }).success(function ( result ) {
            if(status==1) {
                 swal("Thank you!", "Defaulter Application  has approved successfully.", "success");  
            } else {
                swal("Thank you!", "Defaulter Application  has disapproved successfully.", "success");  
            }
           
              // location.reload();
        }).error(function ( result ) {

            swal("Sorry", "We can't  Defaulter Application. Please try again!.", "error");

        }); 
     
    });
}
// Pay Bill Redirect
function payBillRedirect(id){
    var value ='?id='+ id ;
    $.ajax({
        type: "GET",
        url: basePath + 'pay/bill/redirect' +value,
       
        data: {
            // '_token': $('input[name=_token]').val(),
            'id': id
        },
    }).success(function ( result ) {
        
        if(result==1) {

            window.location = basePath+'user-bill-collection-by-collector/'+id;//alert('1');

        } 
        if (result==2) {

            window.location = basePath+'user-due-bill-collection-by-collector/'+id;//alert('1');

        } 
         if(result==3) {

            window.location = basePath+'user-due-bill-collection-by-collector/'+id+'#addvancedBill';//alert('1');

        } 
         if(result==4) {

            
            
        }
        // swal("Thank you!", "Defaulter Application  has delete successfully.", "success");
          
    }).error(function ( result ) {

        // swal("Sorry", "We can't delete Defaulter Application. Please try again!.", "error");

    }); 
}
/* Complain Setting Delete */
function defualterApplicationDelete(id) {

    $('#confirmModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure want to approved application?');

    $('.modal-footer').on('click', '#confirm', function() {

        var value ='?id='+ id ;

        $('.complain_' + id ).remove();
        $.ajax({
            type: "POST",
            url: basePath + 'defaulter/continuation/application/delete' +value,
           
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
        }).success(function ( result ) {

            swal("Thank you!", "Defaulter Application  has delete successfully.", "success");
              location.reload();
        }).error(function ( result ) {

            swal("Sorry", "We can't delete Defaulter Application. Please try again!.", "error");

        }); 
     
    });
}


/* Complain Setting Delete */
function realIPDestroy(id) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure want to delete this real IP Information?');
     var find_info ='?id='+ id ;
     
    $('.modal-footer').on('click', '#delete', function() {
        
        $('.complain_' + id ).remove();
        $.ajax({
            type: "POST",
            url: basePath + 'real/ip/delete' +find_info,
           
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
        }).success(function ( result ) {

            swal("Thank you!", "Your Real IP charge information delete successfully.", "success");
            location.reload();
            
        }).error(function ( result ) {

            swal("Sorry", "We can't delete Real IP charge informatio. Please try again!.", "error");

        }); 
       
    });
}

/* Mikrotik Setting Delete */
function mikrotikDestroy(id,status,message) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html(message);

    $('.modal-footer').on('click', '#delete', function() {
     
        $('.mikrotik_' + id +'_'+ status).remove();

        var find_info ='?id='+ id +'&status='+status;
        $.ajax({
            type: "GET",
            url: basePath + 'mikrotik/integrations/delete' +find_info,
           
        }).success(function ( result ) {

            swal("Thank you!", "Mikrotik information has deleted successfully", "success");
        
        }).error(function ( result ) {

            swal("Sorry", "We are unable to delete mikrotik information.", "error");

        }); 
       
    });
}

/* Complain Setting Delete */
function advanceBillDelete(id,name,month,year) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure ! You want to delete <strong>'+ name +'</strong> ? <br>' + '</div>');

    $('.modal-footer').on('click', '#delete', function() {
        var find_info ='?month='+ month +'&id='+id +'&year='+year;
        
        $.ajax({
            type: "POST",
            url: basePath + 'advance/paid/collection/delete' +find_info,
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
        }).success(function ( result ) {

            swal("Thank you!", "Your advacne bill collection has deleted successfully.", "success");
            $('.advancebill_' + id ).remove();
        
        }).error(function ( result ) {

            swal("Sorry", "We can't deleted  advacne bill collection. Please try again!.", "error");

        }); 
       
    });
}

/* Complain Setting Delete */
function newlineSettingDestroy(id,name) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info P').html('Are you sure ! You want to delete <strong>'+ name +'</strong> ? <br>' + '</div>');

    $('.modal-footer').on('click', '#delete', function() {

        var find_info ='?id='+ id;
        
        $.ajax({
            type: "POST",
            url: basePath + 'newline/setting/setup/delete' +find_info,
           
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
        }).success(function ( result ) {
            $('.setting_' + id ).remove();
            swal("Thank you!", "Your newline setting has deleted successfully.", "success");
        
        }).error(function ( result ) {
            $('.setting_' + id ).remove();
            swal("Sorry", "We can't create newline setting. Please try again!.", "error");

        }); 
       
    });
}

/* Branch Setup deleted */
function branchSetupDeleted(id,name) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info').html('Are you sure want to delete this branch? ' + '</div>');

    $('.modal-footer').on('click', '#delete', function() {
       $('#deletenotification').html("<div class='alert alert-success text-center'> Your branch infmration has deleted successfully.</div>");
       

        $('.branch_' + id ).remove();
        $.ajax({
            type: 'DELETE',
            url: basePath + 'branch/setup/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            }, success: function(data) { 
                swal("Thank you!", " Your branch infmration has deleted successfully.", "success");
            }
            
        });
    });
}
/* Branch Setup deleted */
function dueBillDelete(billno) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure want to delete this bill? ' + '</div>');
    var find_info ='?billno='+ billno; 

    $('.modal-footer').on('click', '#delete', function() {
        $('#deletenotification').html("<div class='alert alert-success text-center'> Your bill infmration has deleted successfully.</div>");

        $.ajax({
            type: 'POST',
            url: basePath + 'due/bill/collections/delete'+find_info ,
            data: {
                '_token': $('input[name=_token]').val(),
                'id': billno
            }, success: function(data) { 
                swal("Thank you!", "Your bill infmration has deleted successfully.", "success");
                location.reload(); 
            }
            
        });
    });
}
/* Branch Setup deleted */
function companySetupDeleted(id,name) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure want to delete this Company Information? ' + '</div>');

    $('.modal-footer').on('click', '#delete', function() {
       $('#deletenotification').html("<div class='alert alert-success text-center'> Your Company Information has deleted successfully.</div>");
       

        $('.company_' + id ).remove();
        $.ajax({
            type: 'DELETE',
            url: basePath + 'company-profile/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            }, success: function(data) { 
                swal("Thank you!", " Your company infmration has deleted successfully.", "success");
            }
            
        });
    });
}

/* Branch Setup deleted */
function expenseSetupDelete(id) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure want to delete this Expense ? ');

    $('.modal-footer').on('click', '#delete', function() {

        var find_info ='?status='+ status; 
        
        $.ajax({
            type: 'POST',
            url: basePath + 'accounting/expenses/delete'+find_info,
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            }, success: function(data) { 
                $('.expenss_' + id ).remove();
                swal("Thank you!", " Your  infmration has deleted successfully.", "success");
            }
            
        });
    });
}

/* Branch Setup deleted */
function itemSetupDelete(id) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure want to delete this item ? ' + '');

    $('.modal-footer').on('click', '#delete', function() {

        var find_info ='?id='+ id; 
        
        $.ajax({
            type: 'POST',
            url: basePath + 'accounting/item/delete'+find_info,
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            }, success: function(data) { 
                $('.item_' + id ).remove();
                swal("Thank you!", " Your  infmration has deleted successfully.", "success");
            }
            
        });
    });
}

/* Transection Type Setup deleted */
function transectionmodeSetupDelete(id) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure want to delete this Transaction Mode ? ' + '');

    $('.modal-footer').on('click', '#delete', function() {

        var find_info ='?id='+ id; 
        
        $.ajax({
            type: 'POST',
            url: basePath + 'accounting/transection/mode/delete'+find_info,
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            }, success: function(data) { 
                $('.transectionmode_' + id ).remove();
                swal("Thank you!", " Your  information has deleted successfully.", "success");
            }
            
        });
    });
}

/*Real IP Setup Deleted */
function realIPSetupDeleted(id,name) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info').html('Are you sure want to delete this Real IP? ' + '</div>');

    $('.modal-footer').on('click', '#delete', function() {

        $('#deletenotification').html("<div class='alert alert-success text-center'>  Your real IP information has deleted successfully.</div>");
       

        $('.real_ip_' + name ).remove();
        $.ajax({
            type: 'DELETE',
            url: basePath + 'real/ip/setup/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            }, success: function(data) { 
                swal("Thank you!", " Your branch infmration has deleted successfully.", "success");
            }
            
        });
    });
}

/*Region And Road Setup Deleted */
function roadRegionSetupDeleted(id,status) {

    $('#deleteModal').appendTo("body").modal('show');

    if(status == 1) {

        $('#info').html('Are you sure want to delete this  Road ? ' + '</div>'); 

    } else {

        $('#info').html('Are you sure want to delete this Region & Road ? ' + '</div>');  

    }
    

    $('.modal-footer').on('click', '#delete', function() {
        if(status == 1) {
            $('#deletenotification').html("<div class='alert alert-success text-center'>   Road has deleted successfully.</div>");
        } else {
            $('#deletenotification').html("<div class='alert alert-success text-center'>  Region & Road has deleted successfully.</div>");
        }
        
        var find_info ='?status='+ status; 
        $('.region_' + id ).remove();
        $.ajax({
            type: 'DELETE',
            url: basePath + 'region/setup/destroy' + find_info,
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            }, success: function(data) { 
                swal("Thank you!", " Your road and region  infmration delete has successfully.", "success");
            }
            
        });
    });
}

/**
 * [adminDestroy description]
 */
function adminDestroy(id,name) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info').html('Are you sure want to delete this  user ? ' + '</div>'); 


    $('.modal-footer').on('click', '#delete', function() {

        $('.user_list_' + id ).remove();
        swal("Thank you!", "Your user  infmration deleted has successfully.", "success");
        $.ajax({
            type: 'DELETE',
            url: basePath + 'admin-information/destroy' ,
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            }, success: function(data) { 
                swal("Thank you!", " Your admin  infmration deleted has successfully.", "success");
            }
            
        });
    });
}

/**
 * [adminDestroy description]
 */
function invalidClients(id,name) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure want to delete this client ? ' + '</div>'); 


    $('.modal-footer').on('click', '#delete', function() {
        
        var find_info ='?id='+ id; 
        
        $.ajax({
            type: 'POST',
            url: basePath + 'data/validations/delete' +find_info ,
            data: {

                '_token': $('input[name=_token]').val(),
                'id': id

            }, success: function(data) { 
                $('.client_list_' + id ).remove();
                swal("Thank you!", " Your client  infmration deleted has successfully.", "success");
            
            }
            
        });
    });
}

/*district Setup Deleted */
function districtDeleted(id,status) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info').html('Are you sure want to delete this district? ' + '</div>');  

    
    $('.modal-footer').on('click', '#delete', function() {

       $('#deletenotification').html("<div class='alert alert-success text-center'>  Your district infmration has deleted successfully </div>");
       
        $('.district_' + id ).remove();
        $.ajax({
            type: 'DELETE',
            url: basePath + 'district/setup/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            }, success: function(data) { 
                swal("Thank you!", " Your district infmration has deleted successfully.", "success");
            }
            
        });
    });
}

/*Region And Road Setup Deleted */
function divisionDeleted(id,status) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info').html('Are you sure want to delete this division? ' + '</div>');  

    
    $('.modal-footer').on('click', '#delete', function() {

       $('#deletenotification').html("<div class='alert alert-success text-center'>  Your division infmration has deleted successfully </div>");
       
        $('.division_' + id ).remove();
        $.ajax({
            type: 'DELETE',
            url: basePath + 'division/setup/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            }, success: function(data) { 
                swal("Thank you!", " Your division infmration has deleted successfully.", "success");
            }
            
        });
    });
}

/*Region And Road Setup Deleted */
function upazilaDeleted(id,status) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info').html('Are you sure want to delete this upazila? ' + '</div>');  

    
    $('.modal-footer').on('click', '#delete', function() {

       $('#deletenotification').html("<div class='alert alert-success text-center'>  Your upazila infmration has deleted successfully </div>");
       
        $('.upazila_' + id ).remove();
        $.ajax({
            type: 'DELETE',
            url: basePath + 'upazila/setup/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            }, success: function(data) { 
                swal("Thank you!", " Your upazila infmration has deleted successfully.", "success");
            }
            
        });
    });
}

/* SMS Type Setup */
function smsTypeSetup(id,name) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure ! You want to delete ?');

    $('.modal-footer').on('click', '#delete', function() {
       // $('#deletenotification').html("<div class='alert alert-success text-center'><i class='fa fa-check'></i>"  + name + " Deleted successfully</div>");
        swal("Good job!", "SMS Type deleted successfully.", "success");  
        $('.setting_' + id ).remove();
        $.ajax({
            type: 'DELETE',
            url: basePath + 'sms/type/setup/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function(data) { }
        });
    });
}

/* Global Setting Deleted */
function globalSettingDelete(id,name) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure ! You want to delete' + '</div>');
     var find_info ='?id='+ id; 
    $('.modal-footer').on('click', '#delete', function() {
        
        
        $.ajax({
            type: 'POST',
            url: basePath + 'sms/global/settings/delete'+find_info,
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function(data) {
                $('.global_' + id ).remove();
                swal("Good job!", "SMS Setting deleted successfully.", "success");

            } 
        });
    });
}

/* SMS Respones Setup */
function responseDelete(id,name) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info').html('Are you sure ! You want to delete <strong>'+ name +'</strong> ? <br>' + '</div>');

    $('.modal-footer').on('click', '#delete', function() {
       $('#deletenotification').html("<div class='alert alert-success text-center'><i class='fa fa-check'></i>"  + name + " Deleted successfully</div>");
        
        $('.respones_' + id ).remove();
        $.ajax({
            type: 'DELETE',
            url: basePath + 'sms/response/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function(data) {
                swal("Good job!", "SMS Setting deleted successfully.", "success");
            }
        });
    });
}

/* SMS Respones Setup */
function smsSendCategory(module_name,codes,region_id) {

    $('#confirmModal').appendTo("body").modal('show');
    $('body').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');
    $('#info p').html('Are you sure ! You want to send SMS ?');

    $('.modal-footer').on('click', '#confirm', function() {

        var message   = $('#smsMessage').val();
        var smsRegion = $('#sms_region').val();
        var code      = $('#sms_category').val();

        var find_info ='?code='+code +'&region_id='+smsRegion + '&message=' + message; 

        $.ajax({
            type: 'POST',
            url: basePath + 'category/sms/send/client' + find_info,
            data: {
                '_token': $('input[name=_token]').val(),
                // 'id': id
            },
            success: function(data) {
                swal("Good job!", "SMS has  send successfully.", "success");
                location.reload();
            }
        });
    });
}

/* New line Collection wrong data edit */
function newlineRequestCollectionEdit(id,statusedit) {

    if($('#newlinePaidEdit').is(':checked') == true){

        $('#updateNewlinePaid').appendTo("body").modal('show');

        // Calculation for total amount if wrong entry
        var total             = getIntegerValue($('#grand_toal').val());
        var newlinePaidEdit   = getIntegerValue($('#editnewlinepaid').val());
        var editdueAdjustment = getIntegerValue($('#editdueAdjustment').val());
        var currentDueBill    = +total - (newlinePaidEdit+ +editdueAdjustment  );

        var dueBil = $('#editDueBill').val(currentDueBill);


        $('#editnewlinepaid').on('keyup',function() {

            var editdueAdjustment  = getIntegerValue($('#editdueAdjustment').val());
            var current            = getIntegerValue($('#grand_toal').val());
            var newlineCurrentPaid = getIntegerValue($('#editnewlinepaid').val());
            var totalPaidAdjustment= newlineCurrentPaid+ +editdueAdjustment;
            var calculateNewline   = +current -totalPaidAdjustment ;

            if(calculateNewline >= 0) {

                $('#editDueBill').val(calculateNewline); 

            } else {

                $('#editnewlinepaid').val(newlinePaidEdit); 
                $('#editDueBill').val(currentDueBill); 

            }
            
        });
            
        $('#info').html('Are you sure ! You want to delete <strong>'+ name +'</strong> ? <br>' + '</div>');

        $('.modal-footer').on('click', '#delete', function() {


            var editnewlinepaid    = $("#editnewlinepaid").val();
            var editDueBill        = $("#editDueBill").val();

            var find_info ='?statusedit='+ statusedit +'&id='+id +'&editnewlinepaid='+editnewlinepaid +'&editDueBill='+editDueBill+'&editdueAdjustment=' +editdueAdjustment; 

            $.ajax({
                type: 'POST',
                url: basePath + 'update-new-line-request' + find_info,
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': id
                },
                success: function(data) { 
                    swal("Good job!", "Congratulations! Newline Collection Update Sucessfully.", "success");  
                    location.reload(); 
                }
            });
        });
    }
}

/* Complain Contact person live update */
/*function contactPerson(id) {
        
    var empid = $("#contactPerson_"+id).val();
  
    var values = '?id=' + id ;
        values += '&empid=' + empid;
        
    $.ajax({
        type: "GET",
        url: basePath +'changed-contactperson-live' + values , 
        success: function(html) {
        
            $("#status_"+id).html('Progress');
            $("#assignby_"+id).html(empid);
        }
    });          
}*/

/**
 * [description]
 * @param  {[type]} )        {                  $.ajax({        type:                                      'GET',          url: basePath + 'newline/request/page/load',                       data: {            '_token': $('input[name [description]
 * @param  {[type]} success: function(data) {                    $('#newConnectionRequestBody').html(data);              }                 });                            } [description]
 * @return {[type]}          [description]
 */
$(document).on('change click', '#newlineRequestLoad', function() {
    
    $.ajax({
        type: 'GET',
        url: basePath + 'newline/request/page/load',
        data: {
            '_token': $('input[name=_token]').val(),
            // 'id': id
        },
        success: function(data) {
           $('#newConnectionRequestBody').html(data);
        }
    });
    
});

/**
 * [description]
 * @param  {String} ) {               $('#dailyConnectionBiResult').after('<div class [description]
 * @return {[type]}   [description]
 */
$(document).on('change click', '#sms_category', function() {

    // $('#dailyYearlyBiResult').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

    var code  = $( '#sms_category' ).val();
    var find_info  ='?code='+code;  

    $.ajax({
        type: "GET",
        url: basePath + 'sms/module/message/search' + find_info
        
    }).success(function ( result ) {

        $('#loader').slideUp(200,function() {       
            $('#loader').remove();
        });

        $(".loader").fadeOut("slow"); 

        $('#smsMessage').html( result );
           
    }).error(function ( result ) {
        console.log('Information was not found.');
    });
}); 

/**
 * [description]
 * @param  {String} ) {               $('#dailyConnectionBiResult').after('<div class [description]
 * @return {[type]}   [description]
 */
$(document).on('change', '#yearlyGenerated', function() {

    $('#dailyYearlyBiResult').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

    var yearlyGenerated  = $( '#yearlyGenerated' ).val();
    var find_info  ='?yearlyGenerated='+yearlyGenerated;  

    $.ajax({
        type: "GET",
        url: basePath + 'isperp/dashboard/yearly/search' + find_info
        
    }).success(function ( result ) {

        $('#loader').slideUp(200,function() {       
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 

        $('#monthlyGeneratedAmount').html( result );
           
    }).error(function ( result ) {

        console.log('Information was not found.');

    });
}); 


/**
 * [description]
 * @param  {[type]} ) {               var dailyformbi [description]

 */
$(document).on('click', '#collectionByReportsBi', function() {

    $('#dailyCollectionBiResult').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

    var dailyformbi  = $( '#dailyCollectionBiForm' ).val();
    var dailytobi    = $( '#dailyCollectionBiTo' ).val();

    var find_info  ='?dailyformbi='+ dailyformbi +'&dailytobi=' +dailytobi;  

    $.ajax({
        type: "GET",
        url: basePath + 'isperp/dashboard/collection/date/search' + find_info
        
    }).success(function ( result ) {
        $('#loader').slideUp(200,function() {       
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 
        $('#collectorWiseDailyCols').html( result );
        // $('#dailyConnection').html( result );
           
    }).error(function ( result ) {

        console.log('Information was not found.');

    });
}); 

/**
 * [description]
 * @param  {[type]} ) {               var dailyformbi [description]

 */
$(document).on('click', '#dailyNewConeectionsBi', function() {

    $('#dailyConnectionBiResult').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

    var dailyformbi  = $( '#dailyformbi' ).val();
    var dailytobi    = $( '#dailytobi' ).val();
    var nl_type      = $( '#nl_type' ).val();

    var find_info  ='?dailyformbi='+ dailyformbi +'&dailytobi=' +dailytobi+'&nl_type=' +nl_type;  

    $.ajax({
        type: "GET",
        url: basePath + 'isperp/dashboard/connection/date/search' + find_info
        
    }).success(function ( result ) {
        
        $('#loader').slideUp(200,function() {       
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 

        $('#dailyNewLine').html( result );
        $('#dailyConnection').html( result );

    }).error(function ( result ) {

        $('#dailyNewLine').html( '<span class="not-found"> Not Found</span>' );
        $('#dailyConnection').html( '<span class="not-found"> Not Found</span>' );

    });
}); 

/**
 * [description]
 * @param  {[type]} ) {               var dailyformbi [description]

 */
$(document).on('click', '#dailyNewConeectionsTypeBi', function() {

    $('#dailyConnectionBiResult').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

    var connectionTypeTo  = $( '#connectionTypeTo' ).val();
    var connectionTypeform  = $( '#connectionTypeform' ).val();
    var nl_type             = $( '#nl_type' ).val();

    var find_info  ='?connectionTypeTo='+ connectionTypeTo +'&connectionTypeform=' +connectionTypeform+'&nl_type=' +nl_type;  

    $.ajax({
        type: "GET",
        url: basePath + 'isperp/dashboard/connection/type/date/search' + find_info
        
    }).success(function ( result ) {
        
        $('#loader').slideUp(200,function() {       
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 

        $('#dailyNewLine').html( result );
        $('#dailyConnection').html( result );

    }).error(function ( result ) {

        $('#dailyNewLine').html( '<span class="not-found"> Not Found</span>' );
        $('#dailyConnection').html( '<span class="not-found"> Not Found</span>' );

    });
});
/**
 * [Daily complains bi reporsts]
 */
$(document).on('click', '#dailycomplainBi', function() {

    $('#dailyComplainBiResult').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

    var complaintobi  = $( '#complaintobi' ).val();

    var find_info  ='?complaintobi='+ complaintobi;  

    $.ajax({
        type: "GET",
        url: basePath + 'isperp/dashboard/complain/date/search' + find_info
        
    }).success(function ( result ) {

        $('#loader').slideUp(200,function() {       
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 

        $('#areaBasedDailyComplains').html( result );
        $('#DailyAreaBasedComplain').html( result );
           
    }).error(function ( result ) {

        console.log('Information was not found.');

    });
}); 

/**
 * [Cause base Search]
 */
$(document).on('click', '#causeBasedComplainBie', function() {

    $('#causeComplainBiResult').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

    var causebiTo    = $( '#causebiTo' ).val();
    var causebiForm  = $( '#causebiForm' ).val();

    var find_info  ='?causebiTo='+ causebiTo +'&causebiForm='+ causebiForm;  

    $.ajax({
        type: "GET",
        url: basePath + 'complain/bi/category/date/search' + find_info
        
    }).success(function ( result ) {

        $('#CategoryBasedComplain').html( result );
           
    }).error(function ( result ) {

        console.log('Information was not found.');

    });
}); 

/**
 * [Daily complains bi reporsts]
 */
$(document).on('click', '#areaBasedSlovedComplainBi', function() {

    $('#areaWiseComplainBiResult').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

    var slovedto    = $( '#slovedto' ).val();
    var slovedform  = $( '#slovedform' ).val();

    var find_info  ='?slovedto='+ slovedto +'&slovedform='+ slovedform;  

    $.ajax({
        type: "GET",
        url: basePath + 'area/based/sloved/complain/date/search' + find_info
        
    }).success(function ( result ) {
        $('#loader').slideUp(200,function() {       
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 
        $('#areaBasedSlovedComplain').html( result );
           
    }).error(function ( result ) {

        console.log('Information was not found.');

    });
}); 

/**
 * [Daily complains bi reporsts]
 */
$(document).on('click', '#areaWiseConnectionBi', function() {

    var areawishform  = $( '#areawishform' ).val();
    var areawishto    = $( '#areawishto' ).val();

    var find_info  ='?areawishform='+ areawishform + '&areawishto=' +areawishto;  
    $('#areaWiseConnectionBiResult').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

    $.ajax({
        type: "GET",
        url: basePath + 'area/based/connection/date/search' + find_info
        
    }).success(function ( result ) {
        $('#loader').slideUp(200,function() {       
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 
        $('#areaBasedConnection').html( result );
           
    }).error(function ( result ) {

        console.log('Information was not found.');

    });
}); 

/**
 * [Daily complains bi reporsts]
 */
$(document).on('click', '#CollectorBasedConnectionBi', function() {

    $('#collectorConnectionBiResult').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

    var collectorConnectionBiTo   = $( '#collectorConnectionBiTo' ).val();
    var collectorConnectionBiForm = $( '#collectorConnectionBiForm' ).val();

    var find_info  ='?collectorConnectionBiTo='+ collectorConnectionBiTo + '&collectorConnectionBiForm=' +collectorConnectionBiForm;  

    $.ajax({
        type: "GET",
        url: basePath + 'collector/based/connection/date/search' + find_info
        
    }).success(function ( result ) {

        $('#loader').slideUp(200,function() {       
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 
        $('#CollectorBasedConnection').html( result );
           
    }).error(function ( result ) {

        console.log('Information was not found.');

    });
}); 

/**
 * [Daily complains bi reporsts]
 */
$(document).on('click', '#regionBasedDailyCollectionBi', function() {

    $('#CollectionRegionBiResult').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

    var regiontoBi  = $( '#regiontoBi' ).val();
    var regionfromBi    = $( '#regionfromBi' ).val();

    var find_info  ='?regiontoBi='+ regiontoBi + '&regionfromBi=' +regionfromBi;  

    $.ajax({
        type: "GET",
        url: basePath + 'region/based/collection/date/search' + find_info
        
    }).success(function ( result ) {
        $('#loader').slideUp(200,function() {       
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 
        $('#regionBasedDailyCollection').html( result );
           
    }).error(function ( result ) {

        console.log('Information was not found.');

    });
}); 



/**
 * [Monthly previous comparision bi]
 */
$(document).on('click', '#monthlyPreviousDueComparisonBi', function() {

    $('#DueComposrBiResult').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

    var duecomporsorTo    = $( '#duecomporsorTo' ).val();
    var duecomporsorForm  = $( '#duecomporsorForm' ).val();

    var find_info  ='?duecomporsorTo='+ duecomporsorTo + '&duecomporsorForm=' +duecomporsorForm;  

    $.ajax({
        type: "GET",
        url: basePath + 'monthly/previous/due/date/search' + find_info
        
    }).success(function ( result ) {

        $('#loader').slideUp(200,function() {       
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 
        $('#monthlyPreviousDueComparison').html( result );
           
    }).error(function ( result ) {

        console.log('Information was not found.');

    });
}); 

/**
 * [Area based disconnection]
 */
$(document).on('click', '#areaBasedDisconnectionBi', function() {

    $('#areabasedDisconnectionBiResult').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

    var areadisconnectionTo    = $( '#areadisconnectionTo' ).val();
    var areadisconnectionForm  = $( '#areadisconnectionForm' ).val();

    var find_info  ='?areadisconnectionTo='+ areadisconnectionTo + '&areadisconnectionForm=' +areadisconnectionForm;  

    $.ajax({
        type: "GET",
        url: basePath + 'area/based/disconnection/date/search' + find_info
        
    }).success(function ( result ) {
        $('#loader').slideUp(200,function() {       
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 
        $('#areaBasedDisconnection').html( result );
           
    }).error(function ( result ) {

        console.log('Information was not found.');

    });
}); 
/**
 * [Area based reconnection
 */
$(document).on('click', '#areaBasedReconnectionBi', function() {

    var reconnectionToBi    = $( '#reconnectionToBi' ).val();
    var reconnectionFormBi  = $( '#reconnectionFormBi' ).val();

    $('#areabasedReconnectionBiResult').after('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

    var find_info  ='?reconnectionToBi='+ reconnectionToBi + '&reconnectionFormBi=' +reconnectionFormBi;  

    $.ajax({
        type: "GET",
        url: basePath + 'area/based/reconnection/date/search' + find_info
        
    }).success(function ( result ) {
        $('#loader').slideUp(200,function() {       
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 
        $('#areaBasedReconnection').html( result );
           
    }).error(function ( result ) {

        console.log('Information was not found.');

    });
});
/* Newline View More */
function newlineReqeustNotes(nlreaID) {

   
     $('.red').hide();

    $('#pluginConfirmModel').appendTo("body").modal('show');

    $('#info p').html('<form method="GET" action="'+basePath+'update-new-line-notes" accept-charset="UTF-8" enctype="multipart/form-data" role="form" data-toggle="validator" novalidate="true"><textarea class="form-control" name="newlinenotes" id="newlinenotes_'+nlreaID+'"></textarea><input type="hidden" name="newlineReqeustID"  name="nlreaID" value="'+nlreaID+'"><button class="btn btn-success" type="submit">Submit</button><div class="row newlineReqeustNotes" id="newlineReqeustNotes"></div>'); 

    var find_info  ='?nlreaID='+ nlreaID ;  

    $.ajax({
        type: "GET",
        url: basePath + 'new/line/notes/list' + find_info
        
    }).success(function ( result ) {
      
        $('#resultTable').html( result );
           
    }).error(function ( result ) {

        console.log('Information was not found.');

    });
} 

/* Newline View More */
function complainNotes(cmpID,clientID) {

   
     $('.red').hide();

    $('#pluginConfirmModel').appendTo("body").modal('show');

    $('#complainNotes').html('<form method="GET" action="'+basePath+'update/complain/notes" accept-charset="UTF-8" enctype="multipart/form-data" role="form" data-toggle="validator" novalidate="true"><textarea class="form-control" name="complainNotes" id="complanNotes_id'+cmpID+'"></textarea><input type="hidden"   value="'+cmpID+'" name="cmpid"><input type="hidden" name="cid"  value="'+clientID+'"><button class="btn btn-success" type="submit">Submit</button><div class="row newlineReqeustNotes" id="newlineReqeustNotes"></div>'); 

    var find_info  ='?cmpID='+ cmpID+'&clientID='+ clientID ;  

    $.ajax({
        type: "GET",
        url: basePath + 'complain/notes' + find_info
        
    }).success(function ( result ) {
      
        $('#newlineReqeustNotes').html( result );
           
    }).error(function ( result ) {

        console.log('Information was not found.');

    });
} 
/* Newline View More */
function newlineRequestViewMore(title,assingDate,feedback,id,referance) {

    var assingTechnican = $('#assignd_technchian_'+id).val();
    var phoneRecivedBy  = $('#phoneRecivedBy_'+id).val();
    var referances      = $('#refreanceBy_'+id).val();

    if(assingTechnican=='undefined') {
        assingBy = ' ';
    } else {
         assingBy = assingTechnican;
    }

    if(referance == 1) {

        var referanceType =  "Office";

    } else if(referance == 2) {
        var referanceType = "None"; 
    } else {
        var referanceType =  "Client";
    }
    

    $('#pluginConfirmModel').appendTo("body").modal('show');
    $('#viewMoreModalTitle').html(title);

    $('#info p').html('<div class="row"><div class="col-md-3 col-sm-3 more-details-referance">  Assigned Date :</div><div class="col-md-9 col-sm-9 more-details-referance">' +assingDate + '</div> </div> <hr><div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Feedback: </div><div class="col-md-9 col-sm-9 more-details-referance">' +feedback + '</div> </div> <hr><div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Assigned By </div><div class="col-md-9 col-sm-9 more-details-referance">' +assingBy + '</div> </div> <hr><div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Phone Received By </div><div class="col-md-9 col-sm-9 more-details-referance">' +phoneRecivedBy + '</div> </div> <hr><div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Referance Type </div><div class="col-md-9 col-sm-9 more-details-referance">' +referanceType + '</div> </div> <hr><div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Referance </div><div class="col-md-9 col-sm-9 more-details-referance">' +referances + '</div> </div> <hr><span class="btn btn-group button-center-group">  <a href="'+basePath+'newline-phonereq-reject/'+id+'" id="fe-newlinephonerequest" class="btn btn-danger  btn-xs" title="Reject">Reject</a>  </span>'); 
}

/* Newline View More */
function newlineViewMore(title,mikrotik,connectiondate,referance,feedback) {

    if(mikrotik == 1) {

        mikrotikStatus = 'Active';

    } else {

        mikrotikStatus = 'Not Active';

    }

    $('#viewMoreModal').appendTo("body").modal('show');
    $('#viewMoreModalTitle').html(title);

    $('#info p').html('<div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Micortik Account</div><div class="col-md-9 col-sm-9 more-details-referance">' +mikrotikStatus + '</div> </div> <hr><div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Connection Date </div><div class="col-md-9 col-sm-9 more-details-referance">' +connectiondate + '</div> </div> <hr><div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Referance </div><div class="col-md-9 col-sm-9 more-details-referance">' +referance + '</div> </div> <hr><div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Remarks </div><div class="col-md-9 col-sm-9 more-details-referance">' +feedback + '</div> </div> <hr>'); 
}

/**
 * [complainViewMore description]
 */
function billHistory(id,month) {
    $('.bill-collapse-'+id).slideToggle(500);
    $('.bill_history_icon_'+id+' i').toggleClass('fa-minus');
}
/**
 * [complainViewMore description]
 * @param  {[title]} title           [description]
 * @param  {[uid]} uid             [description]
 * @param  {[assignBytype]} assignBy        [description]
 * @param  {[assingTime]} assingTime      [description]
 * @param  {[complainDetails]} complainDetails [description]
 * @param  {[sloveddate]} sloveddate      [description]
 */
function complainViewMore(title,uid,assignBy,assingTime,complainDetails,sloveddate,clientId,technician,cause,solvedBy) {

  
    $('#viewMoreModal').appendTo("body").modal('show');
    $('#viewMoreModalTitle').html(title);

    $('#info p').html('<div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Solved Date :</div><div class="col-md-9 col-sm-9 more-details-referance">' +sloveddate + '</div> </div> <hr><div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Complain Details: </div><div class="col-md-9 col-sm-9 more-details-referance">' +complainDetails + '</div> </div> <hr><div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Assign By: </div><div class="col-md-9 col-sm-9 more-details-referance">' +assignBy + '</div> </div> <hr><div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Solved By </div><div class="col-md-9 col-sm-9 more-details-referance">' +solvedBy + '</div> </div> <hr><div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Assign Time: </div><div class="col-md-9 col-sm-9 more-details-referance">' +assingTime + '</div> </div> <hr><bt><a type="submit"  class="send-sms-tech btn btn-warning pull-left" href="'+basePath+'send/sms/technician/'+clientId+','+technician+','+cause+'"> Send SMS Tech.</a>'); 
}

function technicianAssign(newlineReqId) {

    var technician = $('#assignedto_'+newlineReqId).val();

     var find_info  ='?technician='+ technician+'&newlineReqId='+ newlineReqId ;  

    $.ajax({
        type: "GET",
        url: basePath + 'newline/request/assign' + find_info
        
    }).success(function ( result ) {
        if(result==1) {

             swal("Congratulations!", "New line request has successfully assigned.", "success"); 
            $('#newlineAssingButton_'+newlineReqId).html('Assigned');
            $('#newlineAssingButton_'+newlineReqId).removeClass('assing-button-deufalt');
            $('#newlineAssingButton_'+newlineReqId).addClass('button-assinged');
        } else {
            $('#newlineAssingButton_'+newlineReqId).html('Assignd');
            $('#newlineAssingButton_'+newlineReqId).addClass('assing-button-deufalt');
            $('#newlineAssingButton_'+newlineReqId).removeClass('button-assinged');
            swal("Sorry!", "New line request has unsuccessfully assigned. Please select technician..", "error"); 
                
        }
        // $('#resultTable').html( result );
           
    }).error(function ( result ) {

        console.log('Information was not found.');

    });
    
}
/* Line Disconnect View More*/
function linedisconnectViewMore(title,feedback,collectedDate,id,days,disto,disform,collecteddate) {

    $('#viewMoreModal').appendTo("body").modal('show');
    $('#viewMoreModalTitle').html(title);
    var phonerecivedBy = $('#phone_'+id).html();
    var collected      = $('#collected_'+id).html();

    $('#info p').html('<div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Phone Recived by </div><div class="col-md-9 col-sm-9 more-details-referance">' +phonerecivedBy + '</div> </div> <hr><div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Collected by </div><div class="col-md-9 col-sm-9 more-details-referance">' +collected + '</div> </div> <hr><div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Disconnect To </div><div class="col-md-9 col-sm-9 more-details-referance">' +disto + '</div> </div> <hr><div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Disconnect Form </div><div class="col-md-9 col-sm-9 more-details-referance">' +disform + '</div> </div> <hr><div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Block Days </div><div class="col-md-9 col-sm-9 more-details-referance">' +days + '</div> </div> <hr><div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Collected Date </div><div class="col-md-9 col-sm-9 more-details-referance">' +collectedDate + '</div> </div> <hr><div class="row"><div class="col-md-3 col-sm-3 more-details-referance"> Remarks </div><div class="col-md-9 col-sm-9 more-details-referance">' +feedback + '</div> </div> <hr>'); 
}

/**
 * Mac address validation and maskin
 */

var macAddress = document.getElementById("mac");
function formatMAC(e) {
    var r = /([a-f0-9]{2})([a-f0-9]{2})/i,
        str = e.target.value.replace(/[^a-f0-9]/ig, "");
    
    while (r.test(str)) {
        str = str.replace(r, '$1' + ':' + '$2');
    }

    e.target.value = str.slice(0, 17);
};

macAddress.addEventListener("keyup", formatMAC, false);

