$(document).ready(function(){
	$("#valueSubmit").on("submit",function() {

		var printpaidAmount  = $('#paidamount').val();
		var printadvancePaid = $('#advancePaid').val();
		var printBillno      = $('#billno').val();
		var print_clientid   = $('#print_clientid').val();
		var print_Name       = $('#print_Name').val();
		var printralIpChange = $('#printralIpChange').val();

		JsBarcode("#barcode",printBillno,{
            displayValue: false,
             width: 3,
             height:20,
        });
        
        JsBarcode("#barcode1",print_clientid,{
            displayValue: false,
             width: 3,
             height:20,
        });

		var today = $('#todayPrintDate').val();

		var totalPaid = +printpaidAmount+ +printadvancePaid;

		$('#print_billpaidAmount').html(': '+ totalPaid);
		$('#print_billpaidAmountTotal').html(': '+ totalPaid);
		$('#print_billno').html(': '+ printBillno);
		$('#print_cid').html(': '+ print_clientid);
		$('#print_Name_r').html(': '+ print_Name);
		$('#withIPCharge').html(': '+ printralIpChange);
		
		
	  	$("#click").printMe({
            "path" : [basePath + "/assets/css/custom.css"],
        });
	});

	$("#duebillSubmit").on("submit",function() {

		var printpaidAmount  = $('#paidamount').val();
		var printadvancePaid = $('#advance').val();
		var printBillno      = $('#billno').val();
		var print_clientid   = $('#print_clientid').val();
		var print_Name_due   = $('#due_print_Name').val();
			
		JsBarcode("#dueBillBarcode", printBillno, {
			displayValue: false,
			width: 3,
			height:20,
		});
		JsBarcode("#dueBillBarcode1", print_clientid, {
			displayValue: false,
			width: 3,
			height:20,
		});
		
		var today = $('#todayPrintDate').val();

		var totalPaid = +printpaidAmount+ +printadvancePaid;

		$('#print_billpaidAmount').html(': '+ totalPaid);
		$('#print_billpaidAmountTotal').html(': '+ totalPaid);
		$('#print_billno_due').html(': '+ printBillno);
		$('#print_cid_due').html(': '+ print_clientid);
		$('#print_Name_due').html(': '+ print_Name_due);
		
		
	  	$("#click").printMe({
            "path" : [basePath + "/assets/css/custom.css"],
        });
	});

	$("#advancedBillSubmit").on("submit",function() {

		
		var printadvancePaid = $('#advance').val();
		
		var advance_cid   = $('#advance_cid').val();
		var advance_print_Name   = $('#advance_print_Name').val();
			
		JsBarcode("#advance_barcode", advance_cid, {
			displayValue: false,
			width: 3,
			height:20,
		});
		
		

		

		$('#aprint_billpaidAmount').html(': '+ printadvancePaid);
		$('#aprint_billpaidAmountTotal').html(': '+ printadvancePaid);
		$('#aprint_billno_due').html(': '+ advance_cid);
		$('#aprint_cid_due').html(': '+ advance_cid);
		$('#aprint_Name_due').html(': '+ advance_print_Name);
		
		
	  	$("#printAdvance").printMe({
            "path" : [basePath + "/assets/css/custom.css"],
        });
	});
});