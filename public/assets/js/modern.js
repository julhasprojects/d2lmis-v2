

/**
 * jQuery PrintMe v.1.0
 * 
 * A jquery plugin that prints the given element
 *
 * Copyright 2014, Daniel Arlandis <daniarlandis@gmail.com> www.daniarlandis.es
 * Released under the WTFPL license
 * http://sam.zoy.org/wtfpl/
 *
 * Date: Mon Feb 10 19:23:00 2014
 * Last modified: Fri Oct 21 22:00:00 2016
 */
jQuery.fn.printMe = function(options){

	// Setup options
	var settings = $.extend({
		// Defaults options.
		path: [],
		title: "",
		head: false,
	}, options );
	
	// Set the properties and run the plugin
	return this.each(function(){
		
		// Store the object
		var $this = $(this);

		var w = window.open();
		w.document.write( "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" );
		w.document.write( "<html>" );
		w.document.write( "<head>" );
		w.document.write( "<meta charset='utf-8'>" );

		// Add the style sheets
		for(i in settings.path){
			w.document.write('<link rel="stylesheet" href="'+settings.path[i]+'">');
		}
		
		// Close the head
		w.document.write('</head><body>');

		// Add a header when the title not is empty
		if (settings.title != "")
			w.document.write( "<h1>" + settings.title + "<\/h1>" );

		// Add a content to print
		w.document.write( $this.html() );
		w.document.write('<style>  .barcodenewconid{display:none} </style><script type="text/javascript">function closeme(){window.close();}setTimeout(closeme,50);window.print();</script></body></html>');
		w.document.write('<style> .print-money-receipt{display:none;}  .barcodebillno,.invoice-tetrasoft,.office-print-area{display:none} .barcodenewconid{display:block !important}</style><script type="text/javascript">function closeme(){window.close();}setTimeout(closeme,50);window.print();</script></body></html>');
		w.document.close();
	});
}

/*custom scroll bar*/

/*! enscroll - v0.6.0 - 2014-02-27
* Copyright (c) 2014 ; Licensed  */
!function(a,b,c,d){var e={verticalScrolling:!0,horizontalScrolling:!1,verticalScrollerSide:"right",showOnHover:!1,scrollIncrement:20,minScrollbarLength:40,pollChanges:!0,drawCorner:!0,drawScrollButtons:!1,clickTrackToScroll:!0,easingDuration:500,propagateWheelEvent:!0,verticalTrackClass:"vertical-track",horizontalTrackClass:"horizontal-track",horizontalHandleClass:"horizontal-handle",verticalHandleClass:"vertical-handle",scrollUpButtonClass:"scroll-up-btn",scrollDownButtonClass:"scroll-down-btn",scrollLeftButtonClass:"scroll-left-btn",scrollRightButtonClass:"scroll-right-btn",cornerClass:"scrollbar-corner",zIndex:1,addPaddingToPane:!0,horizontalHandleHTML:'<div class="left"></div><div class="right"></div>',verticalHandleHTML:'<div class="top"></div><div class="bottom"></div>'},f=function(a){a.preventDefault?a.preventDefault():a.returnValue=!1,a.stopPropagation?a.stopPropagation():a.cancelBubble=!0},g=b.requestAnimationFrame||b.mozRequestAnimationFrame||b.webkitRequestAnimationFrame||b.oRequestAnimationFrame||b.msRequestAnimationFrame||function(a){setTimeout(a,17)},h=function(b,c){var d=a(b).css(c),e=/^-?\d+/.exec(d);return e?+e[0]:0},i=function(a){var b,c,d={width:"5px",height:"1px",overflow:"hidden",padding:"8px 0",visibility:"hidden",whiteSpace:"pre-line",font:"10px/1 serif"},e=document.createElement(a),f=document.createTextNode("a\na");for(c in d)e.style[c]=d[c];return e.appendChild(f),document.body.appendChild(e),b=e.scrollHeight<28,document.body.removeChild(e),b},j=.5*Math.PI,k=10*Math.log(2),l=function(a,b,c){var d=j/b,e=a*d;return Math.round(e*Math.cos(d*c))},m=function(a,b,c){return Math.round(a*k*Math.pow(2,-10*c/b+1)/b)},n=function(a,b,c,d){return 2*c/Math.PI*Math.asin((d-a)/b)},o=function(b){var c=a(this).data("enscroll"),d=this,e=c.settings,f=function(){var b=a(this).data("enscroll"),c=b.settings;b&&c.showOnHover&&(c.verticalScrolling&&a(b.verticalTrackWrapper).is(":visible")&&a(b.verticalTrackWrapper).stop().fadeTo(275,0),c.horizontalScrolling&&a(b.horizontalTrackWrapper).is(":visible")&&a(b.horizontalTrackWrapper).stop().fadeTo(275,0),b._fadeTimer=null)};c&&e.showOnHover&&(c._fadeTimer?clearTimeout(c._fadeTimer):(e.verticalScrolling&&a(c.verticalTrackWrapper).is(":visible")&&a(c.verticalTrackWrapper).stop().fadeTo(275,1),e.horizontalScrolling&&a(c.horizontalTrackWrapper).is(":visible")&&a(c.horizontalTrackWrapper).stop().fadeTo(275,1)),b!==!1&&(c._fadeTimer=setTimeout(function(){f.call(d)},1750)))},p=function(b,c){var d=a(b),e=d.data("enscroll"),f=d.scrollTop();e&&e.settings.verticalScrolling&&(d.scrollTop(f+c),e.settings.showOnHover&&o.call(b))},q=function(b,c){var d=a(b),e=d.data("enscroll"),f=d.scrollLeft();e&&e.settings.horizontalScrolling&&(d.scrollLeft(f+c),e.settings.showOnHover&&o.call(b))},r=function(b){if(1===b.which){var d,e,f,h,i,j,k,l,m,n=b.data.pane,p=a(n),q=p.data("enscroll"),r=!0,s=function(){r&&(f!==h&&(q._scrollingY||(q._scrollingY=!0,q._startY=p.scrollTop(),g(function(){t(p)})),e.style.top=f+"px",q._endY=f*m/l,h=f),g(s),q.settings.showOnHover&&o.call(n))},u=function(a){return r&&(f=a.clientY-j-i,f=Math.min(0>f?0:f,l)),!1},v=function(){return r=!1,c.body.style.cursor=k,this.style.cursor="",d.removeClass("dragging"),a(c.body).off("mousemove.enscroll.vertical").off("mouseup.enscroll.vertical"),a(c).off("mouseout.enscroll.vertical"),p.on("scroll.enscroll.pane",function(a){x.call(this,a)}),!1};return d=a(q.verticalTrackWrapper).find(".enscroll-track"),e=d.children().first()[0],f=parseInt(e.style.top,10),m=n.scrollHeight-(q._scrollHeightNoPadding?a(n).height():a(n).innerHeight()),i=b.clientY-a(e).offset().top,l=d.height()-a(e).outerHeight(),j=d.offset().top,p.off("scroll.enscroll.pane"),a(c.body).on({"mousemove.enscroll.vertical":u,"mouseup.enscroll.vertical":function(a){v.call(e,a)}}),a(c).on("mouseout.enscroll.vertical",function(a){a.target.nodeName&&"HTML"===a.target.nodeName.toUpperCase()&&v.call(e,a)}),d.hasClass("dragging")||(d.addClass("dragging"),k=a(c.body).css("cursor"),this.style.cursor=c.body.style.cursor="ns-resize"),g(s),!1}},s=function(b){if(1===b.which){var d,e,f,h,i,j,k,l,m,n=b.data.pane,p=a(n),q=a(n).data("enscroll"),r=!0,s=function(){r&&(f!==h&&(q._scrollingX||(q._scrollingX=!0,q._startX=p.scrollLeft(),g(function(){t(p)})),e.style.left=f+"px",q._endX=f*i/m,h=f),g(s),q.settings.showOnHover&&o.call(n))},u=function(a){return r&&(f=a.clientX-k-j,f=Math.min(0>f?0:f,m)),!1},v=function(){return r=!1,d.removeClass("dragging"),c.body.style.cursor=l,this.style.cursor="",d.removeClass("dragging"),a(c.body).off("mousemove.enscroll.horizontal").off("mouseup.enscroll.horizontal"),a(c).off("mouseout.enscroll.horizontal"),p.on("scroll.enscroll.pane",function(a){x.call(this,a)}),!1};return d=a(q.horizontalTrackWrapper).find(".enscroll-track"),e=d.children().first()[0],f=parseInt(e.style.left,10),i=n.scrollWidth-a(n).innerWidth(),j=b.clientX-a(e).offset().left,m=d.width()-a(e).outerWidth(),k=d.offset().left,p.off("scroll.enscroll.pane"),a(c.body).on({"mousemove.enscroll.horizontal":u,"mouseup.enscroll.horizontal":function(a){v.call(e,a)}}),a(c).on("mouseout.enscroll.horizontal",function(a){a.target.nodeName&&"HTML"===a.target.nodeName.toUpperCase()&&v.call(e,a)}),d.hasClass("dragging")||(d.addClass("dragging"),l=a("body").css("cursor"),this.style.cursor=c.body.style.cursor="ew-resize"),g(s),!1}},t=function(a){var b,c,d,e=a.data("enscroll"),f=e._duration;e._scrollingX===!0&&(b=e._endX-e._startX,0===b?e._scrollingX=!1:(c=a.scrollLeft(),d=n(e._startX,b,f,c),b>0?c>=e._endX||c<e._startX?e._scrollingX=!1:(q(a,Math.max(1,l(b,f,d))),g(function(){t(a)})):c<=e._endX||c>e._startX?e._scrollingX=!1:(q(a,Math.min(-1,l(b,f,d))),g(function(){t(a)})))),e._scrollingY===!0&&(b=e._endY-e._startY,0===b?e._scrollingY=!1:(c=a.scrollTop(),d=n(e._startY,b,f,c),b>0?c>=e._endY||c<e._startY?e._scrollingY=!1:(p(a,Math.max(1,l(b,f,d))),g(function(){t(a)})):c<=e._endY||c>e._startY?e._scrollingY=!1:(p(a,Math.min(-1,l(b,f,d))),g(function(){t(a)}))))},u=function(a,b){var c=a.data("enscroll"),d=a.scrollLeft(),e=a[0].scrollWidth-a.innerWidth();return!c.settings.horizontalScrolling||c._scrollingY?!1:(c._scrollingX||(c._scrollingX=!0,c._startX=d,c._endX=c._startX,g(function(){t(a)})),c._endX=b>0?Math.min(d+b,e):Math.max(0,d+b),0>b&&d>0||b>0&&e>d)},v=function(a,b){var c=a.data("enscroll"),d=a.scrollTop(),e=a[0].scrollHeight-(c._scrollHeightNoPadding?a.height():a.innerHeight());return!c.settings.verticalScrolling||c._scrollingX?!1:(c._scrollingY||(c._scrollingY=!0,c._startY=d,c._endY=c._startY,g(function(){t(a)})),c._endY=b>0?Math.min(d+b,e):Math.max(0,d+b),0>b&&d>0||b>0&&e>d)},w=function(b){var c,d=a(this),e=d.data("enscroll"),g=e.settings.scrollIncrement,h="deltaX"in b?-b.deltaX:"wheelDeltaX"in b?b.wheelDeltaX:0,i="deltaY"in b?-b.deltaY:"wheelDeltaY"in b?b.wheelDeltaY:"wheelDelta"in b?b.wheelDelta:0;Math.abs(h)>Math.abs(i)?(c=(h>0?-g:g)<<2,(u(d,c)||!e.settings.propagateWheelEvent)&&f(b)):(c=(i>0?-g:g)<<2,(v(d,c)||!e.settings.propagateWheelEvent)&&f(b))},x=function(){var b,c,d,e=a(this),f=e.data("enscroll");f&&(f.settings.verticalScrolling&&(c=a(f.verticalTrackWrapper).find(".enscroll-track")[0],b=c.firstChild,d=e.scrollTop()/(this.scrollHeight-(f._scrollHeightNoPadding?e.height():e.innerHeight())),d=isNaN(d)?0:d,b.style.top=d*(a(c).height()-a(b).outerHeight())+"px"),f.settings.horizontalScrolling&&(c=a(f.horizontalTrackWrapper).find(".enscroll-track")[0],b=c.firstChild,d=e.scrollLeft()/(this.scrollWidth-e.innerWidth()),d=isNaN(d)?0:d,b.style.left=d*(a(c).width()-a(b).innerWidth())+"px"))},y=function(b){var c,d=a(this),e=d.data("enscroll");if(!/(input)|(select)|(textarea)/i.test(this.nodeName)&&b.target===this&&e){switch(c=e.settings.scrollIncrement,b.keyCode){case 32:case 34:return v(d,d.height()),!1;case 33:return v(d,-d.height()),!1;case 35:return v(d,this.scrollHeight),!1;case 36:return v(d,-this.scrollHeight),!1;case 37:return u(d,-c),!1;case 38:return v(d,-c),!1;case 39:return u(d,c),!1;case 40:return v(d,c),!1}return!0}},z=function(){var b=this,d=a(b).data("enscroll").settings,e=!0,f=0,h=0,i=a(b).offset().top,j=i+a(b).outerHeight(),k=a(b).offset().left,l=k+a(b).outerWidth(),m=function(a){var b=a.pageX,c=a.pageY;f=k>b?b-k:b>l?b-l:0,h=i>c?c-i:c>j?c-j:0},n=function(){d.horizontalScrolling&&f&&q(b,parseInt(f/4,10)),d.verticalScrolling&&h&&p(b,parseInt(h/4,10)),e&&g(n)},o=function(){e=!1,a(c).off("mousemove.enscroll.pane").off("mouseup.enscroll.pane")};g(n),a(c).on({"mousemove.enscroll.pane":m,"mouseup.enscroll.pane":o})},A=function(a){var b,c,e,h,i,j,k,l=this,n=function(a){b=a.touches[0].clientX,c=a.touches[0].clientY,e||(e=c===i&&b===h?d:Math.abs(i-c)>Math.abs(h-b)?"y":"x"),f(a)},o=function(){j&&("y"===e?(p(l,i-c),k=i-c,i=c):"x"===e&&(q(l,h-b),k=h-b,h=b),g(o))},r=function(){var a=0,b=Math.abs(1.5*k);this.removeEventListener("touchmove",n,!1),this.removeEventListener("touchend",r,!1),j=!1,g(function c(){var d;a===b||j||(d=m(k,b,a),isNaN(d)||0===d||(a+=1,"y"===e?p(l,d):q(l,d),g(c)))})};1===a.touches.length&&(h=a.touches[0].clientX,i=a.touches[0].clientY,j=!0,this.addEventListener("touchmove",n,!1),this.addEventListener("touchend",r,!1),g(o))},B={reposition:function(){return this.each(function(){var b,c,d,e=a(this),f=e.data("enscroll"),g=function(a,b,c){a.style.left=b+"px",a.style.top=c+"px"};f&&(d=e.position(),b=f.corner,f.settings.verticalScrolling&&(c=f.verticalTrackWrapper,g(c,"right"===f.settings.verticalScrollerSide?d.left+e.outerWidth()-a(c).width()-h(this,"border-right-width"):d.left+h(this,"border-left-width"),d.top+h(this,"border-top-width"))),f.settings.horizontalScrolling&&(c=f.horizontalTrackWrapper,g(c,d.left+h(this,"border-left-width"),d.top+e.outerHeight()-a(c).height()-h(this,"border-bottom-width"))),b&&g(b,d.left+e.outerWidth()-a(b).outerWidth()-h(this,"border-right-width"),d.top+e.outerHeight()-a(b).outerHeight()-h(this,"border-bottom-width")))})},resize:function(){return this.each(function(){var b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r=a(this),s=r.data("enscroll");return s?(b=s.settings,r.is(":visible")?(b.verticalScrolling&&(e=s.verticalTrackWrapper,c=r.innerHeight(),f=c/this.scrollHeight,g=a(e).find(".enscroll-track")[0],j=a(e).find("."+b.scrollUpButtonClass),k=a(e).find("."+b.scrollDownButtonClass),i=b.horizontalScrolling?c-a(s.horizontalTrackWrapper).find(".enscroll-track").outerHeight():c,i-=a(g).outerHeight()-a(g).height()+j.outerHeight()+k.outerHeight(),n=g.firstChild,p=Math.max(f*i,b.minScrollbarLength),p-=a(n).outerHeight()-a(n).height(),e.style.display="none",g.style.height=i+"px",n.style.height=p+"px",1>f&&(f=r.scrollTop()/(this.scrollHeight-r.height()),n.style.top=f*(i-p)+"px",e.style.display="block")),b.horizontalScrolling&&(e=s.horizontalTrackWrapper,d=r.innerWidth(),f=d/this.scrollWidth,g=a(e).find(".enscroll-track")[0],l=a(e).find("."+b.scrollLeftButtonClass),m=a(e).find("."+b.scrollRightButtonClass),h=b.verticalScrolling?d-a(s.verticalTrackWrapper).find(".enscroll-track").outerWidth():d,h-=a(g).outerWidth()-a(g).width()+l.outerWidth()+m.outerWidth(),n=g.firstChild,o=Math.max(f*h,b.minScrollbarLength),o-=a(n).outerWidth()-a(n).width(),e.style.display="none",g.style.width=h+"px",n.style.width=o+"px",1>f&&(f=r.scrollLeft()/(this.scrollWidth-r.width()),n.style.left=f*(h-o)+"px",e.style.display="block"),s._prybar&&(q=s._prybar,this.removeChild(q),b.verticalScrolling&&(q.style.width=this.scrollWidth+a(s.verticalTrackWrapper).find(".enscroll-track").outerWidth()+"px",this.appendChild(q)))),s.corner&&(s.corner.style.display=s.verticalTrackWrapper&&s.horizontalTrackWrapper&&a(s.verticalTrackWrapper).is(":visible")&&a(s.horizontalTrackWrapper).is(":visible")?"":"none")):(b.verticalScrolling&&(s.verticalTrackWrapper.style.display="none"),b.horizontalScrolling&&(s.horizontalTrackWrapper.style.display="none"),s.corner&&(s.corner.style.display="none")),void 0):!0})},startPolling:function(){return this.each(function(){var b,c=a(this).data("enscroll"),d=this,e=a(d),f=-1,g=-1,h=-1,i=-1,j=function(){if(c.settings.pollChanges){var a=d.scrollWidth,k=d.scrollHeight,l=e.width(),m=e.height(),n=e.offset();(c.settings.verticalScrolling&&(m!==g||k!==i)||c.settings.horizontalScrolling&&(l!==f||a!==h))&&(h=a,i=k,B.resize.call(e)),(b.left!==n.left||b.top!==n.top||l!==f||m!==g)&&(b=n,f=l,g=m,B.reposition.call(e)),setTimeout(j,350)}};c&&(c.settings.pollChanges=!0,i=d.scrollHeight,h=d.scrollWidth,b=e.offset(),j())})},stopPolling:function(){return this.each(function(){var b=a(this).data("enscroll");b&&(b.settings.pollChanges=!1)})},destroy:function(){return this.each(function(){var c,d,e=a(this),f=e.data("enscroll");f&&(B.stopPolling.call(e),d=f._mouseScrollHandler,f.settings.verticalScrolling&&(c=f.verticalTrackWrapper,a(c).remove(),c=null),f.settings.horizontalScrolling&&(c=f.horizontalTrackWrapper,a(c).remove(),c=null),f._fadeTimer&&clearTimeout(f._fadeTimer),f.corner&&a(f.corner).remove(),f._prybar&&f._prybar.parentNode&&f._prybar.parentNode===this&&a(f._prybar).remove(),this.setAttribute("style",f._style||""),f._hadTabIndex||e.removeAttr("tabindex"),e.off("scroll.enscroll.pane").off("keydown.enscroll.pane").off("mouseenter.enscroll.pane").off("mousedown.enscroll.pane").data("enscroll",null),this.removeEventListener?(this.removeEventListener("wheel",d,!1),this.removeEventListener("mousewheel",d,!1),this.removeEventListener("touchstart",A,!1)):this.detachEvent&&this.detachEvent("onmousewheel",d),a(b).off("resize.enscroll.window"))})}};a.fn.enscroll=function(d){var f;return B[d]?B[d].call(this):(f=a.extend({},e,d),this.each(function(){if(f.verticalScrolling||f.horizontalScrolling){var d,e,g,j,k,l,m,n,t,C,D,E,F,G,H,I,J,K,L=a(this),M=this,N=L.attr("style"),O=!0,P={position:"absolute","z-index":f.zIndex,margin:0,padding:0},Q=function(a){w.call(M,a)},R=function(b,c){"string"==typeof c?a(b).html(c):b.appendChild(c)};if(f.verticalScrolling){e=c.createElement("div"),j=c.createElement("div"),l=c.createElement("a"),a(j).css("position","relative").addClass("enscroll-track").addClass(f.verticalTrackClass).appendTo(e),f.drawScrollButtons&&(m=c.createElement("a"),n=c.createElement("a"),a(m).css({display:"block","text-decoration":"none"}).attr("href","").html("&nbsp;").addClass(f.scrollUpButtonClass).on("click",function(){return p(M,-f.scrollIncrement),!1}).insertBefore(j),a(n).css({display:"block","text-decoration":"none"}).attr("href","").html("&nbsp;").on("click",function(){return p(M,f.scrollIncrement),!1}).addClass(f.scrollDownButtonClass).appendTo(e)),f.clickTrackToScroll&&a(j).on("click",function(b){b.target===this&&v(L,b.pageY>a(l).offset().top?L.height():-L.height())}),a(l).css({position:"absolute","z-index":1}).attr("href","").addClass(f.verticalHandleClass).mousedown({pane:this},r).click(function(){return!1}).appendTo(j),R(l,f.verticalHandleHTML),a(e).css(P).insertAfter(this),f.showOnHover&&a(e).css("opacity",0).on("mouseover.enscroll.vertical",function(){o.call(M,!1)}).on("mouseout.enscroll.vertical",function(){o.call(M)}),E=a(j).outerWidth(),f.addPaddingToPane&&(K="right"===f.verticalScrollerSide?{"padding-right":h(this,"padding-right")+E+"px"}:{"padding-left":h(this,"padding-left")+E+"px"},L.css(a.extend({width:L.width()-E+"px"},K)));try{I=parseInt(L.css("outline-width"),10),0!==I&&!isNaN(I)||"none"!==L.css("outline-style")||L.css("outline","none")}catch(S){L.css("outline","none")}}f.horizontalScrolling&&(d=c.createElement("div"),g=c.createElement("div"),k=c.createElement("a"),a(g).css({position:"relative","z-index":1}).addClass("enscroll-track").addClass(f.horizontalTrackClass).appendTo(d),f.drawScrollButtons&&(t=c.createElement("a"),C=c.createElement("a"),a(t).css("display","block").attr("href","").on("click",function(){return q(M,-f.scrollIncrement),!1}).addClass(f.scrollLeftButtonClass).insertBefore(g),a(C).css("display","block").attr("href","").on("click",function(){return q(M,f.scrollIncrement),!1}).addClass(f.scrollRightButtonClass).appendTo(d)),f.clickTrackToScroll&&a(g).on("click",function(b){b.target===this&&u(L,b.pageX>a(k).offset().left?L.width():-L.width())}),a(k).css({position:"absolute","z-index":1}).attr("href","").addClass(f.horizontalHandleClass).click(function(){return!1}).mousedown({pane:this},s).appendTo(g),R(k,f.horizontalHandleHTML),a(d).css(P).insertAfter(this),f.showOnHover&&a(d).css("opacity",0).on("mouseover.enscroll.horizontal",function(){o.call(M,!1)}).on("mouseout.enscroll.horizontal",function(){o.call(M)}),D=a(g).outerHeight(),f.addPaddingToPane&&L.css({height:L.height()-D+"px","padding-bottom":parseInt(L.css("padding-bottom"),10)+D+"px"}),J=document.createElement("div"),a(J).css({width:"1px",height:"1px",visibility:"hidden",padding:0,margin:"-1px"}).appendTo(this)),f.verticalScrolling&&f.horizontalScrolling&&f.drawCorner&&(F=c.createElement("div"),a(F).addClass(f.cornerClass).css(P).insertAfter(this)),H=L.attr("tabindex"),H||(L.attr("tabindex",0),O=!1);try{G=L.css("outline"),(!G||G.length<1)&&L.css("outline","none")}catch(S){L.css("outline","none")}L.on({"scroll.enscroll.pane":function(a){x.call(this,a)},"keydown.enscroll.pane":y,"mousedown.enscroll.pane":z}).css("overflow","hidden").data("enscroll",{settings:f,horizontalTrackWrapper:d,verticalTrackWrapper:e,corner:F,_prybar:J,_mouseScrollHandler:Q,_hadTabIndex:O,_style:N,_scrollingX:!1,_scrollingY:!1,_startX:0,_startY:0,_endX:0,_endY:0,_duration:parseInt(f.easingDuration/16.66666,10),_scrollHeightNoPadding:i(this.nodeName)}),a(b).on("resize.enscroll.window",function(){B.reposition.call(L)}),f.showOnHover&&L.on("mouseenter.enscroll.pane",function(){o.call(this)}),this.addEventListener?("onwheel"in this||"WheelEvent"in b&&navigator.userAgent.toLowerCase().indexOf("msie")>=0?this.addEventListener("wheel",Q,!1):"onmousewheel"in this&&this.addEventListener("mousewheel",Q,!1),this.addEventListener("touchstart",A,!1)):this.attachEvent&&this.attachEvent("onmousewheel",Q),f.pollChanges&&B.startPolling.call(L),B.resize.call(L),B.reposition.call(L)}}))}}(jQuery,window,document);
(function($) { 
	$('.tools a').click(function() {
	   // $('.monthlyGenaratedAmount').Toggle();
	    return false;
	});
})(jQuery);

$( document ).ready(function() {

	//$('.widget').hide();
    $('.scrollbox').enscroll();
    // Toggle Search
    $('.show-search').click(function(){
        $('.search-form').css('margin-top', '0');
    });
    
    $('.close-search').click(function(){
        $('.search-form').css('margin-top', '-60px');
    });
      $('#select_all').on('click',function(){
            if(this.checked){
                $('.checkbox').each(function(){
                    this.checked = true;
                });
            }else{
                 $('.checkbox').each(function(){
                    this.checked = false;
                });
            }
        });
        
        $('.checkbox').on('click',function(){
            if($('.checkbox:checked').length == $('.checkbox').length){
                $('#select_all').prop('checked',true);
            }else{
                $('#select_all').prop('checked',false);
            }
        });
    
    // Fullscreen
    function toggleFullScreen() {
        if ((document.fullScreenElement && document.fullScreenElement !== null) ||  
            (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (document.documentElement.requestFullScreen) {  
                document.documentElement.requestFullScreen();  
            } else if (document.documentElement.mozRequestFullScreen) {  
                document.documentElement.mozRequestFullScreen(); 
            } else if (document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
            }  
        } else {  
            if (document.cancelFullScreen) {  
                document.cancelFullScreen();  
            } else if (document.mozCancelFullScreen) {  
                document.mozCancelFullScreen();  
            } else if (document.webkitCancelFullScreen) {  
                document.webkitCancelFullScreen();  
            }  
        }  
    }
    
    $('.toggle-fullscreen').click(function(){
        toggleFullScreen();
    });
    
    
    // Waves
    Waves.displayEffect();
    
    // tooltips
    $( '[data-toggle~="tooltip"]' ).tooltip({
        container: 'body'
    });
    
    // Switchery
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    
    elems.forEach(function(html) {
        var switchery = new Switchery(html, { color: '#23B7E5' });
    });
    
    // Element Blocking
    function blockUI(item) {    
        $(item).block({
            message: '<img src="assets/images/reload.gif" width="20px" alt="">',
            css: {
                border: 'none',
                padding: '0px',
                width: '20px',
                height: '20px',
                backgroundColor: 'transparent'
            },
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.9,
                cursor: 'wait'
            }
        });
    }
    
    function unblockUI(item) {
        $(item).unblock();
    }  
    
    // Panel Control
    $('.panel-collapse').click(function(){
        $(this).closest(".panel").children('.panel-body').slideToggle('fast');
    });
    
    $('.panel-reload').click(function() { 
        var el = $(this).closest(".panel").children('.panel-body');
        blockUI(el);
        window.setTimeout(function () {
            unblockUI(el);
        }, 1000);
    
    }); 
    
    $('.panel-remove').click(function(){
        $(this).closest(".panel").hide();
    });
    
    // Push Menu
    $('.push-sidebar').click(function(){
        var hidden = $('.sidebar');
        
        if (hidden.hasClass('visible')){
            hidden.removeClass('visible');
            $('.page-inner').removeClass('sidebar-visible');
        } else {
            hidden.addClass('visible');
            $('.page-inner').addClass('sidebar-visible');
        }
    });
    
    // sortable
    $(".sortable").sortable({
        connectWith: '.sortable',
        items: '.panel',
        helper: 'original',
        revert: true,
        placeholder: 'panel-placeholder',
        forcePlaceholderSize: true,
        opacity: 0.95,
        cursor: 'move'
    });
    
  
    
  
    
    // Sidebar Menu
    var parent, ink, d, x, y;
    $('.sidebar .accordion-menu li .sub-menu').slideUp(0);
    $('.sidebar .accordion-menu li.open .sub-menu').slideDown(0);
    $('.small-sidebar .sidebar .accordion-menu li.open .sub-menu').hide(0);
    $('.sidebar .accordion-menu > li.droplink > a').click(function(){
        
        if($('body').hasClass('.small-sidebar')) {
            return;
        };
        
        if($('body').hasClass('.page-horizontal-bar')) {
            return;
        };
        
        if($('body').hasClass('.hover-menu')) {
            return;
        };
        
        var menu = $('.sidebar .menu'),
            sidebar = $('.page-sidebar-inner'),
            page = $('.page-content'),
            sub = $(this).next(),
            el = $(this);
        
        menu.find('li').removeClass('open');
        $('.sub-menu').slideUp(200, function() {
            sidebarAndContentHeight();
        });
        sidebarAndContentHeight();
        
        if (!sub.is(':visible')) {
            $(this).parent('li').addClass('open');
            $(this).next('.sub-menu').slideDown(200, function() {
                sidebarAndContentHeight();
            });
        } else {
            sub.slideUp(200, function() {
                sidebarAndContentHeight();
            });
        }
        return false;
    });
    
  /*  $('.sidebar .accordion-menu .sub-menu li.droplink > a').click(function() {
        
        var menu = $(this).parent().parent(),
            sidebar = $('.page-sidebar-inner'),
            page = $('.page-content'),
            sub = $(this).next(),
            el = $(this);
        
        menu.find('li').removeClass('open');
        sidebarAndContentHeight();
        
        if (!sub.is(':visible')) {
            $(this).parent('li').addClass('open');
            $(this).next('.sub-menu').slideDown(200, function() {
                sidebarAndContentHeight();
            });
        } else {
            sub.slideUp(200, function() {
                sidebarAndContentHeight();
            });
        }
        return false;
    });*/
    
   /* // Makes .page-inner height same as .page-sidebar height
    var sidebarAndContentHeight = function () {
        var content = $('.page-inner'),
            sidebar = $('.page-sidebar'),
            body = $('body'),
            height,
            footerHeight = $('.page-footer').outerHeight(),
            pageContentHeight = $('.page-content').height();
        
        content.attr('style', 'min-height:' + sidebar.height() + 'px !important');
        
        if (body.hasClass('page-sidebar-fixed')) {
            height = sidebar.height() + footerHeight;
        } else {
            height = sidebar.height() + footerHeight;
            if (height  < $(window).height()) {
                height = $(window).height();
            }
        }
        
        if (height >= content.height()) {
            content.attr('style', 'min-height:' + height + 'px !important');
        }
    };*/
    
    // sidebarAndContentHeight();
    
    // window.onresize = sidebarAndContentHeight;
    
    
    // Slimscroll
    $('.slimscroll').slimscroll({
        allowPageScroll: true
    });
    
    // Layout Settings
    var fixedHeaderCheck = document.querySelector('.fixed-header-check'),
        fixedSidebarCheck = document.querySelector('.fixed-sidebar-check'),
        horizontalBarCheck = document.querySelector('.horizontal-bar-check'),
        toggleSidebarCheck = document.querySelector('.toggle-sidebar-check'),
        boxedLayoutCheck = document.querySelector('.boxed-layout-check'),
        compactMenuCheck = document.querySelector('.compact-menu-check'),
        hoverMenuCheck = document.querySelector('.hover-menu-check'),
        defaultOptions = function() {
            
            if(($('body').hasClass('small-sidebar'))&&(toggleSidebarCheck.checked == 1)){
                toggleSidebarCheck.click();
            }
        
            if(!($('body').hasClass('page-header-fixed'))&&(fixedHeaderCheck.checked == 0)){
                fixedHeaderCheck.click();
            }
        
            if(($('body').hasClass('page-sidebar-fixed'))&&(fixedSidebarCheck.checked == 1)){
                fixedSidebarCheck.click();
            }
        
            if(($('body').hasClass('page-horizontal-bar'))&&(horizontalBarCheck.checked == 1)){
                horizontalBarCheck.click();
            }
        
            if(($('body').hasClass('compact-menu'))&&(compactMenuCheck.checked == 1)){
                compactMenuCheck.click();
            }
        
            if(($('body').hasClass('hover-menu'))&&(hoverMenuCheck.checked == 1)){
                hoverMenuCheck.click();
            }
        
            if(($('.page-content').hasClass('container'))&&(boxedLayoutCheck.checked == 1)){
                boxedLayoutCheck.click();
            }
        
            $(".theme-color").attr("href", 'assets/css/themes/white.css');
           
            sidebarAndContentHeight();
        },
       smTxt = str = $('.navbar .logo-box a span').text(),
        //smTxt = (str.slice(0,1)),
        collapseSidebar = function() {
            $('body').toggleClass("small-sidebar");
            $('.navbar .logo-box a span').html($('.navbar .logo-box a span').text() == smTxt ? str : smTxt);
            sidebarAndContentHeight();
        },
        fixedHeader = function() {
            if(($('body').hasClass('page-horizontal-bar'))&&($('body').hasClass('page-sidebar-fixed'))&&($('body').hasClass('page-header-fixed'))){
                fixedSidebarCheck.click();
                alert("Static header isn't compatible with fixed horizontal nav mode. Modern will set static mode on horizontal nav.");
            };
            $('body').toggleClass('page-header-fixed');
            sidebarAndContentHeight();
        },
        fixedSidebar = function() {
            if(($('body').hasClass('page-horizontal-bar'))&&(!$('body').hasClass('page-sidebar-fixed'))&&(!$('body').hasClass('page-header-fixed'))){
                fixedHeaderCheck.click();
                alert("Fixed horizontal nav isn't compatible with static header mode. Modern will set fixed mode on header.");
            };
            if(($('body').hasClass('hover-menu'))&&(!$('body').hasClass('page-sidebar-fixed'))){
                hoverMenuCheck.click();
                alert("Fixed sidebar isn't compatible with hover menu mode. Modern will set accordion mode on menu.");
            };
            $('body').toggleClass('page-sidebar-fixed');
            if ($('body').hasClass('.page-sidebar-fixed')) {
                $('.page-sidebar-inner').slimScroll({
                    destroy:true
                });
            };
            $('.page-sidebar-inner').slimScroll();
            sidebarAndContentHeight();
        },
        horizontalBar = function() {
            $('.sidebar').toggleClass('horizontal-bar');
            $('.sidebar').toggleClass('page-sidebar');
            $('body').toggleClass('page-horizontal-bar');
            if(($('body').hasClass('page-sidebar-fixed'))&&(!$('body').hasClass('page-header-fixed'))){
                fixedHeaderCheck.click();
                alert("Static header isn't compatible with fixed horizontal nav mode. Modern will set static mode on horizontal nav.");
            };
            sidebarAndContentHeight();
        },
        boxedLayout = function() {
            $('.page-content').toggleClass('container');
            sidebarAndContentHeight();
        },
        compactMenu = function() {
            $('body').toggleClass('compact-menu');
            sidebarAndContentHeight();
        },
        hoverMenu = function() {
            if((!$('body').hasClass('hover-menu'))&&($('body').hasClass('page-sidebar-fixed'))){
                fixedSidebarCheck.click();
                alert("Fixed sidebar isn't compatible with hover menu mode. Modern will set static mode on sidebar.");
            };
            $('body').toggleClass('hover-menu');
            sidebarAndContentHeight();
        };
    
    
    // Logo text on Collapsed Sidebar
    $('.small-sidebar .navbar .logo-box a span').html($('.navbar .logo-box a span').text() == smTxt ? str : smTxt);
    
    
    if( !$('.theme-settings').length ) {
        $('.sidebar-toggle').click(function() {
            collapseSidebar();
        });
    };
    
    if( $('.theme-settings').length ) {
    fixedHeaderCheck.onchange = function() {
        fixedHeader();
    };
    
    fixedSidebarCheck.onchange = function() {
        fixedSidebar();
    };
    
    horizontalBarCheck.onchange = function() {
        horizontalBar();
    };
    
    toggleSidebarCheck.onchange = function() {
        collapseSidebar();
    };
        
    compactMenuCheck.onchange = function() {
        compactMenu();
    };
        
    hoverMenuCheck.onchange = function() {
        hoverMenu();
    };
        
    boxedLayoutCheck.onchange = function() {
        boxedLayout();
    };
    
    
    // Sidebar Toggle
    $('.sidebar-toggle').click(function() {
        toggleSidebarCheck.click();
    });
    
    // Reset options
    $('.reset-options').click(function() {
        defaultOptions();
    });
    
    // Color changer
    $(".colorbox").click(function(){
        var color =  $(this).attr('data-css');
        $(".theme-color").attr('href', 'assets/css/themes/' + color + '.css');
        return false;
    });
    
    // Fixed Sidebar Bug
    if(!($('body').hasClass('page-sidebar-fixed'))&&(fixedSidebarCheck.checked == 1)){
        $('body').addClass('page-sidebar-fixed');
    }
    
    if(($('body').hasClass('page-sidebar-fixed'))&&(fixedSidebarCheck.checked == 0)){
        $('.fixed-sidebar-check').prop('checked', true);
    }
    
    // Fixed Header Bug
    if(!($('body').hasClass('page-header-fixed'))&&(fixedHeaderCheck.checked == 1)){
        $('body').addClass('page-header-fixed');
    }
    
    if(($('body').hasClass('page-header-fixed'))&&(fixedHeaderCheck.checked == 0)){
        $('.fixed-header-check').prop('checked', true);
    }
    /*
    // horizontal bar Bug
    if(!($('body').hasClass('page-horizontal-bar'))&&(horizontalBarCheck.checked == 1)){
        $('body').addClass('page-horizontal-bar');
        $('.sidebar').addClass('horizontal-bar');
        $('.sidebar').removeClass('page-sidebar');
    }
    */
    if(($('body').hasClass('page-horizontal-bar'))&&(horizontalBarCheck.checked == 0)){
        $('.horizontal-bar-check').prop('checked', true);
    }
    
    // Toggle Sidebar Bug
    if(!($('body').hasClass('small-sidebar'))&&(toggleSidebarCheck.checked == 1)){
        $('body').addClass('small-sidebar');
    }
    
    if(($('body').hasClass('small-sidebar'))&&(toggleSidebarCheck.checked == 0)){
        $('.horizontal-bar-check').prop('checked', true);
    }
    
    // Boxed Layout Bug
    if(!($('.page-content').hasClass('container'))&&(boxedLayoutCheck.checked == 1)){
        $('.toggle-sidebar-check').addClass('container');
    }
    
    if(($('.page-content').hasClass('container'))&&(boxedLayoutCheck.checked == 0)){
        $('.boxed-layout-check').prop('checked', true);
    }
        
    // Boxed Layout Bug
    if(!($('.page-content').hasClass('container'))&&(boxedLayoutCheck.checked == 1)){
        $('.toggle-sidebar-check').addClass('container');
    }
    
    if(($('.page-content').hasClass('container'))&&(boxedLayoutCheck.checked == 0)){
        $('.boxed-layout-check').prop('checked', true);
    }
        
    // Boxed Layout Bug
    if(!($('.page-content').hasClass('container'))&&(boxedLayoutCheck.checked == 1)){
        $('.toggle-sidebar-check').addClass('container');
    }
    
    if(($('.page-content').hasClass('container'))&&(boxedLayoutCheck.checked == 0)){
        $('.boxed-layout-check').prop('checked', true);
    }
    }
    
    
    /*// Chat Sidebar
    var menuRight = document.getElementById( 'cbp-spmenu-s1' ),
        showRight = document.getElementById( 'showRight' ),
        closeRight = document.getElementById( 'closeRight' ),
        menuRight2 = document.getElementById( 'cbp-spmenu-s2' ),
        closeRight2 = document.getElementById( 'closeRight2' ),
        body = document.body;
    */
   /* showRight.onclick = function() {
        classie.toggle( menuRight, 'cbp-spmenu-open' );
    };*/
    
    /*closeRight.onclick = function() {
        classie.toggle( menuRight, 'cbp-spmenu-open' );
    };
    */
    /*closeRight2.onclick = function() {
        classie.toggle( menuRight2, 'cbp-spmenu-open' );
    };
    
    $('.showRight2').click(function() {
        classie.toggle( menuRight2, 'cbp-spmenu-open' );
    });*/
    
 /*   $(".chat-write form input").keypress(function (e) {
        if ((e.which == 13)&&(!$(this).val().length == 0)) {
            $('<div class="chat-item chat-item-right"><div class="chat-message">' + $(this).val() + '</div></div>').insertAfter(".chat .chat-item:last-child");
            $(this).val('');
        } else if(e.which == 13) {
            return;
        }
        $('.chat').slimscroll({
            allowPageScroll: true
        });
    });*/
});



/*!
 * bootstrap-tokenfield
 * https://github.com/sliptree/bootstrap-tokenfield
 * Copyright 2013-2014 Sliptree and other contributors; Licensed MIT
 */

(function (factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery'], factory);
  } else if (typeof exports === 'object') {
    // For CommonJS and CommonJS-like environments where a window with jQuery
    // is present, execute the factory with the jQuery instance from the window object
    // For environments that do not inherently posses a window with a document
    // (such as Node.js), expose a Tokenfield-making factory as module.exports
    // This accentuates the need for the creation of a real window or passing in a jQuery instance
    // e.g. require("bootstrap-tokenfield")(window); or require("bootstrap-tokenfield")($);
    module.exports = global.window && global.window.$ ?
      factory( global.window.$ ) :
      function( input ) {
        if ( !input.$ && !input.fn ) {
          throw new Error( "Tokenfield requires a window object with jQuery or a jQuery instance" );
        }
        return factory( input.$ || input );
      };
  } else {
    // Browser globals
    factory(jQuery, window);
  }
}(function ($, window) {

  "use strict"; // jshint ;_;

 /* TOKENFIELD PUBLIC CLASS DEFINITION
  * ============================== */

  var Tokenfield = function (element, options) {
    var _self = this

    this.$element = $(element)
    this.textDirection = this.$element.css('direction');

    // Extend options
    this.options = $.extend(true, {}, $.fn.tokenfield.defaults, { tokens: this.$element.val() }, this.$element.data(), options)

    // Setup delimiters and trigger keys
    this._delimiters = (typeof this.options.delimiter === 'string') ? [this.options.delimiter] : this.options.delimiter
    this._triggerKeys = $.map(this._delimiters, function (delimiter) {
      return delimiter.charCodeAt(0);
    });
    this._firstDelimiter = this._delimiters[0];

    // Check for whitespace, dash and special characters
    var whitespace = $.inArray(' ', this._delimiters)
      , dash = $.inArray('-', this._delimiters)

    if (whitespace >= 0)
      this._delimiters[whitespace] = '\\s'

    if (dash >= 0) {
      delete this._delimiters[dash]
      this._delimiters.unshift('-')
    }

    var specialCharacters = ['\\', '$', '[', '{', '^', '.', '|', '?', '*', '+', '(', ')']
    $.each(this._delimiters, function (index, character) {
      var pos = $.inArray(character, specialCharacters)
      if (pos >= 0) _self._delimiters[index] = '\\' + character;
    });

    // Store original input width
    var elRules = (window && typeof window.getMatchedCSSRules === 'function') ? window.getMatchedCSSRules( element ) : null
      , elStyleWidth = element.style.width
      , elCSSWidth
      , elWidth = this.$element.width()

    if (elRules) {
      $.each( elRules, function (i, rule) {
        if (rule.style.width) {
          elCSSWidth = rule.style.width;
        }
      });
    }

    // Move original input out of the way
    var hidingPosition = $('body').css('direction') === 'rtl' ? 'right' : 'left',
        originalStyles = { position: this.$element.css('position') };
    originalStyles[hidingPosition] = this.$element.css(hidingPosition);

    this.$element
      .data('original-styles', originalStyles)
      .data('original-tabindex', this.$element.prop('tabindex'))
      .css('position', 'absolute')
      .css(hidingPosition, '-10000px')
      .prop('tabindex', -1)

    // Create a wrapper
    this.$wrapper = $('<div class="tokenfield form-control" />')
    if (this.$element.hasClass('input-lg')) this.$wrapper.addClass('input-lg')
    if (this.$element.hasClass('input-sm')) this.$wrapper.addClass('input-sm')
    if (this.textDirection === 'rtl') this.$wrapper.addClass('rtl')

    // Create a new input
    var id = this.$element.prop('id') || new Date().getTime() + '' + Math.floor((1 + Math.random()) * 100)
    this.$input = $('<input type="'+this.options.inputType+'" class="token-input" autocomplete="off" />')
                    .appendTo( this.$wrapper )
                    .prop( 'placeholder',  this.$element.prop('placeholder') )
                    .prop( 'id', id + '-tokenfield' )
                    .prop( 'tabindex', this.$element.data('original-tabindex') )

    // Re-route original input label to new input
    var $label = $( 'label[for="' + this.$element.prop('id') + '"]' )
    if ( $label.length ) {
      $label.prop( 'for', this.$input.prop('id') )
    }

    // Set up a copy helper to handle copy & paste
    this.$copyHelper = $('<input type="text" />').css('position', 'absolute').css(hidingPosition, '-10000px').prop('tabindex', -1).prependTo( this.$wrapper )

    // Set wrapper width
    if (elStyleWidth) {
      this.$wrapper.css('width', elStyleWidth);
    }
    else if (elCSSWidth) {
      this.$wrapper.css('width', elCSSWidth);
    }
    // If input is inside inline-form with no width set, set fixed width
    else if (this.$element.parents('.form-inline').length) {
      this.$wrapper.width( elWidth )
    }

    // Set tokenfield disabled, if original or fieldset input is disabled
    if (this.$element.prop('disabled') || this.$element.parents('fieldset[disabled]').length) {
      this.disable();
    }

    // Set tokenfield readonly, if original input is readonly
    if (this.$element.prop('readonly')) {
      this.readonly();
    }

    // Set up mirror for input auto-sizing
    this.$mirror = $('<span style="position:absolute; top:-999px; left:0; white-space:pre;"/>');
    this.$input.css('min-width', this.options.minWidth + 'px')
    $.each([
        'fontFamily',
        'fontSize',
        'fontWeight',
        'fontStyle',
        'letterSpacing',
        'textTransform',
        'wordSpacing',
        'textIndent'
    ], function (i, val) {
        _self.$mirror[0].style[val] = _self.$input.css(val);
    });
    this.$mirror.appendTo( 'body' )

    // Insert tokenfield to HTML
    this.$wrapper.insertBefore( this.$element )
    this.$element.prependTo( this.$wrapper )

    // Calculate inner input width
    this.update()

    // Create initial tokens, if any
    this.setTokens(this.options.tokens, false, ! this.$element.val() && this.options.tokens )

    // Start listening to events
    this.listen()

    // Initialize autocomplete, if necessary
    if ( ! $.isEmptyObject( this.options.autocomplete ) ) {
      var side = this.textDirection === 'rtl' ? 'right' : 'left'
       ,  autocompleteOptions = $.extend({
            minLength: this.options.showAutocompleteOnFocus ? 0 : null,
            position: { my: side + " top", at: side + " bottom", of: this.$wrapper }
          }, this.options.autocomplete )

      this.$input.autocomplete( autocompleteOptions )
    }

    // Initialize typeahead, if necessary
    if ( ! $.isEmptyObject( this.options.typeahead ) ) {

      var typeaheadOptions = this.options.typeahead
        , defaults = {
            minLength: this.options.showAutocompleteOnFocus ? 0 : null
          }
        , args = $.isArray( typeaheadOptions ) ? typeaheadOptions : [typeaheadOptions, typeaheadOptions]

      args[0] = $.extend( {}, defaults, args[0] )

      this.$input.typeahead.apply( this.$input, args )
      this.$hint = this.$input.prev('.tt-hint')
      this.typeahead = true
    }
  }

  Tokenfield.prototype = {

    constructor: Tokenfield

  , createToken: function (attrs, triggerChange) {
      var _self = this

      if (typeof attrs === 'string') {
        attrs = { value: attrs, label: attrs }
      } else {
        // Copy objects to prevent contamination of data sources.
        attrs = $.extend( {}, attrs )
      }

      if (typeof triggerChange === 'undefined') {
         triggerChange = true
      }

      // Normalize label and value
      attrs.value = $.trim(attrs.value.toString());
      attrs.label = attrs.label && attrs.label.length ? $.trim(attrs.label) : attrs.value

      // Bail out if has no value or label, or label is too short
      if (!attrs.value.length || !attrs.label.length || attrs.label.length <= this.options.minLength) return

      // Bail out if maximum number of tokens is reached
      if (this.options.limit && this.getTokens().length >= this.options.limit) return

      // Allow changing token data before creating it
      var createEvent = $.Event('tokenfield:createtoken', { attrs: attrs })
      this.$element.trigger(createEvent)

      // Bail out if there if attributes are empty or event was defaultPrevented
      if (!createEvent.attrs || createEvent.isDefaultPrevented()) return

      var $token = $('<div class="token" />')
            .append('<span class="token-label" />')
            .append('<a href="#" class="close" tabindex="-1">&times;</a>')
            .data('attrs', attrs)

      // Insert token into HTML
      if (this.$input.hasClass('tt-input')) {
        // If the input has typeahead enabled, insert token before it's parent
        this.$input.parent().before( $token )
      } else {
        this.$input.before( $token )
      }

      // Temporarily set input width to minimum
      this.$input.css('width', this.options.minWidth + 'px')

      var $tokenLabel = $token.find('.token-label')
        , $closeButton = $token.find('.close')

      // Determine maximum possible token label width
      if (!this.maxTokenWidth) {
        this.maxTokenWidth =
          this.$wrapper.width() - $closeButton.outerWidth() -
          parseInt($closeButton.css('margin-left'), 10) -
          parseInt($closeButton.css('margin-right'), 10) -
          parseInt($token.css('border-left-width'), 10) -
          parseInt($token.css('border-right-width'), 10) -
          parseInt($token.css('padding-left'), 10) -
          parseInt($token.css('padding-right'), 10)
          parseInt($tokenLabel.css('border-left-width'), 10) -
          parseInt($tokenLabel.css('border-right-width'), 10) -
          parseInt($tokenLabel.css('padding-left'), 10) -
          parseInt($tokenLabel.css('padding-right'), 10)
          parseInt($tokenLabel.css('margin-left'), 10) -
          parseInt($tokenLabel.css('margin-right'), 10)
      }

      $tokenLabel
        .text(attrs.label)
        .css('max-width', this.maxTokenWidth)

      // Listen to events on token
      $token
        .on('mousedown',  function (e) {
          if (_self._disabled || _self._readonly) return false
          _self.preventDeactivation = true
        })
        .on('click',    function (e) {
          if (_self._disabled || _self._readonly) return false
          _self.preventDeactivation = false

          if (e.ctrlKey || e.metaKey) {
            e.preventDefault()
            return _self.toggle( $token )
          }

          _self.activate( $token, e.shiftKey, e.shiftKey )
        })
        .on('dblclick', function (e) {
          if (_self._disabled || _self._readonly || !_self.options.allowEditing ) return false
          _self.edit( $token )
        })

      $closeButton
          .on('click',  $.proxy(this.remove, this))

      // Trigger createdtoken event on the original field
      // indicating that the token is now in the DOM
      this.$element.trigger($.Event('tokenfield:createdtoken', {
        attrs: attrs,
        relatedTarget: $token.get(0)
      }))

      // Trigger change event on the original field
      if (triggerChange) {
        this.$element.val( this.getTokensList() ).trigger( $.Event('change', { initiator: 'tokenfield' }) )
      }

      // Update tokenfield dimensions
      this.update()

      // Return original element
      return this.$element.get(0)
    }

  , setTokens: function (tokens, add, triggerChange) {
      if (!tokens) return

      if (!add) this.$wrapper.find('.token').remove()

      if (typeof triggerChange === 'undefined') {
          triggerChange = true
      }

      if (typeof tokens === 'string') {
        if (this._delimiters.length) {
          // Split based on delimiters
          tokens = tokens.split( new RegExp( '[' + this._delimiters.join('') + ']' ) )
        } else {
          tokens = [tokens];
        }
      }

      var _self = this
      $.each(tokens, function (i, attrs) {
        _self.createToken(attrs, triggerChange)
      })

      return this.$element.get(0)
    }

  , getTokenData: function($token) {
      var data = $token.map(function() {
        var $token = $(this);
        return $token.data('attrs')
      }).get();

      if (data.length == 1) {
        data = data[0];
      }

      return data;
    }

  , getTokens: function(active) {
      var self = this
        , tokens = []
        , activeClass = active ? '.active' : '' // get active tokens only
      this.$wrapper.find( '.token' + activeClass ).each( function() {
        tokens.push( self.getTokenData( $(this) ) )
      })
      return tokens
  }

  , getTokensList: function(delimiter, beautify, active) {
      delimiter = delimiter || this._firstDelimiter
      beautify = ( typeof beautify !== 'undefined' && beautify !== null ) ? beautify : this.options.beautify

      var separator = delimiter + ( beautify && delimiter !== ' ' ? ' ' : '')
      return $.map( this.getTokens(active), function (token) {
        return token.value
      }).join(separator)
  }

  , getInput: function() {
    return this.$input.val()
  }

  , listen: function () {
      var _self = this

      this.$element
        .on('change',   $.proxy(this.change, this))

      this.$wrapper
        .on('mousedown',$.proxy(this.focusInput, this))

      this.$input
        .on('focus',    $.proxy(this.focus, this))
        .on('blur',     $.proxy(this.blur, this))
        .on('paste',    $.proxy(this.paste, this))
        .on('keydown',  $.proxy(this.keydown, this))
        .on('keypress', $.proxy(this.keypress, this))
        .on('keyup',    $.proxy(this.keyup, this))

      this.$copyHelper
        .on('focus',    $.proxy(this.focus, this))
        .on('blur',     $.proxy(this.blur, this))
        .on('keydown',  $.proxy(this.keydown, this))
        .on('keyup',    $.proxy(this.keyup, this))

      // Secondary listeners for input width calculation
      this.$input
        .on('keypress', $.proxy(this.update, this))
        .on('keyup',    $.proxy(this.update, this))

      this.$input
        .on('autocompletecreate', function() {
          // Set minimum autocomplete menu width
          var $_menuElement = $(this).data('ui-autocomplete').menu.element

          var minWidth = _self.$wrapper.outerWidth() -
              parseInt( $_menuElement.css('border-left-width'), 10 ) -
              parseInt( $_menuElement.css('border-right-width'), 10 )

          $_menuElement.css( 'min-width', minWidth + 'px' )
        })
        .on('autocompleteselect', function (e, ui) {
          if (_self.createToken( ui.item )) {
            _self.$input.val('')
            if (_self.$input.data( 'edit' )) {
              _self.unedit(true)
            }
          }
          return false
        })
        .on('typeahead:selected typeahead:autocompleted', function (e, datum, dataset) {
          // Create token
          if (_self.createToken( datum )) {
            _self.$input.typeahead('val', '')
            if (_self.$input.data( 'edit' )) {
              _self.unedit(true)
            }
          }
        })

      // Listen to window resize
      $(window).on('resize', $.proxy(this.update, this ))

    }

  , keydown: function (e) {

      if (!this.focused) return

      var _self = this

      switch(e.keyCode) {
        case 8: // backspace
          if (!this.$input.is(document.activeElement)) break
          this.lastInputValue = this.$input.val()
          break

        case 37: // left arrow
          leftRight( this.textDirection === 'rtl' ? 'next': 'prev' )
          break

        case 38: // up arrow
          upDown('prev')
          break

        case 39: // right arrow
          leftRight( this.textDirection === 'rtl' ? 'prev': 'next' )
          break

        case 40: // down arrow
          upDown('next')
          break

        case 65: // a (to handle ctrl + a)
          if (this.$input.val().length > 0 || !(e.ctrlKey || e.metaKey)) break
          this.activateAll()
          e.preventDefault()
          break

        case 9: // tab
        case 13: // enter

          // We will handle creating tokens from autocomplete in autocomplete events
          if (this.$input.data('ui-autocomplete') && this.$input.data('ui-autocomplete').menu.element.find("li:has(a.ui-state-focus), li.ui-state-focus").length) break

          // We will handle creating tokens from typeahead in typeahead events
          if (this.$input.hasClass('tt-input') && this.$wrapper.find('.tt-cursor').length ) break
          if (this.$input.hasClass('tt-input') && this.$wrapper.find('.tt-hint').val() && this.$wrapper.find('.tt-hint').val().length) break

          // Create token
          if (this.$input.is(document.activeElement) && this.$input.val().length || this.$input.data('edit')) {
            return this.createTokensFromInput(e, this.$input.data('edit'));
          }

          // Edit token
          if (e.keyCode === 13) {
            if (!this.$copyHelper.is(document.activeElement) || this.$wrapper.find('.token.active').length !== 1) break
            if (!_self.options.allowEditing) break
            this.edit( this.$wrapper.find('.token.active') )
          }
      }

      function leftRight(direction) {
        if (_self.$input.is(document.activeElement)) {
          if (_self.$input.val().length > 0) return

          direction += 'All'
          var $token = _self.$input.hasClass('tt-input') ? _self.$input.parent()[direction]('.token:first') : _self.$input[direction]('.token:first')
          if (!$token.length) return

          _self.preventInputFocus = true
          _self.preventDeactivation = true

          _self.activate( $token )
          e.preventDefault()

        } else {
          _self[direction]( e.shiftKey )
          e.preventDefault()
        }
      }

      function upDown(direction) {
        if (!e.shiftKey) return

        if (_self.$input.is(document.activeElement)) {
          if (_self.$input.val().length > 0) return

          var $token = _self.$input.hasClass('tt-input') ? _self.$input.parent()[direction + 'All']('.token:first') : _self.$input[direction + 'All']('.token:first')
          if (!$token.length) return

          _self.activate( $token )
        }

        var opposite = direction === 'prev' ? 'next' : 'prev'
          , position = direction === 'prev' ? 'first' : 'last'

        _self.$firstActiveToken[opposite + 'All']('.token').each(function() {
          _self.deactivate( $(this) )
        })

        _self.activate( _self.$wrapper.find('.token:' + position), true, true )
        e.preventDefault()
      }

      this.lastKeyDown = e.keyCode
    }

  , keypress: function(e) {

      // Comma
      if ($.inArray( e.which, this._triggerKeys) !== -1 && this.$input.is(document.activeElement)) {
        if (this.$input.val()) {
          this.createTokensFromInput(e)
        }
        return false;
      }
    }

  , keyup: function (e) {
      this.preventInputFocus = false

      if (!this.focused) return

      switch(e.keyCode) {
        case 8: // backspace
          if (this.$input.is(document.activeElement)) {
            if (this.$input.val().length || this.lastInputValue.length && this.lastKeyDown === 8) break

            this.preventDeactivation = true
            var $prevToken = this.$input.hasClass('tt-input') ? this.$input.parent().prevAll('.token:first') : this.$input.prevAll('.token:first')

            if (!$prevToken.length) break

            this.activate( $prevToken )
          } else {
            this.remove(e)
          }
          break

        case 46: // delete
          this.remove(e, 'next')
          break
      }
      this.lastKeyUp = e.keyCode
    }

  , focus: function (e) {
      this.focused = true
      this.$wrapper.addClass('focus')

      if (this.$input.is(document.activeElement)) {
        this.$wrapper.find('.active').removeClass('active')
        this.$firstActiveToken = null

        if (this.options.showAutocompleteOnFocus) {
          this.search()
        }
      }
    }

  , blur: function (e) {

      this.focused = false
      this.$wrapper.removeClass('focus')

      if (!this.preventDeactivation && !this.$element.is(document.activeElement)) {
        this.$wrapper.find('.active').removeClass('active')
        this.$firstActiveToken = null
      }

      if (!this.preventCreateTokens && (this.$input.data('edit') && !this.$input.is(document.activeElement) || this.options.createTokensOnBlur )) {
        this.createTokensFromInput(e)
      }

      this.preventDeactivation = false
      this.preventCreateTokens = false
    }

  , paste: function (e) {
      var _self = this

      // Add tokens to existing ones
      if (_self.options.allowPasting) {
        setTimeout(function () {
          _self.createTokensFromInput(e)
        }, 1)
      }
    }

  , change: function (e) {
      if ( e.initiator === 'tokenfield' ) return // Prevent loops

      this.setTokens( this.$element.val() )
    }

  , createTokensFromInput: function (e, focus) {
      if (this.$input.val().length < this.options.minLength)
        return // No input, simply return

      var tokensBefore = this.getTokensList()
      this.setTokens( this.$input.val(), true )

      if (tokensBefore == this.getTokensList() && this.$input.val().length)
        return false // No tokens were added, do nothing (prevent form submit)

      if (this.$input.hasClass('tt-input')) {
        // Typeahead acts weird when simply setting input value to empty,
        // so we set the query to empty instead
        this.$input.typeahead('val', '')
      } else {
        this.$input.val('')
      }

      if (this.$input.data( 'edit' )) {
        this.unedit(focus)
      }

      return false // Prevent form being submitted
    }

  , next: function (add) {
      if (add) {
        var $firstActiveToken = this.$wrapper.find('.active:first')
          , deactivate = $firstActiveToken && this.$firstActiveToken ? $firstActiveToken.index() < this.$firstActiveToken.index() : false

        if (deactivate) return this.deactivate( $firstActiveToken )
      }

      var $lastActiveToken = this.$wrapper.find('.active:last')
        , $nextToken = $lastActiveToken.nextAll('.token:first')

      if (!$nextToken.length) {
        this.$input.focus()
        return
      }

      this.activate($nextToken, add)
    }

  , prev: function (add) {

      if (add) {
        var $lastActiveToken = this.$wrapper.find('.active:last')
          , deactivate = $lastActiveToken && this.$firstActiveToken ? $lastActiveToken.index() > this.$firstActiveToken.index() : false

        if (deactivate) return this.deactivate( $lastActiveToken )
      }

      var $firstActiveToken = this.$wrapper.find('.active:first')
        , $prevToken = $firstActiveToken.prevAll('.token:first')

      if (!$prevToken.length) {
        $prevToken = this.$wrapper.find('.token:first')
      }

      if (!$prevToken.length && !add) {
        this.$input.focus()
        return
      }

      this.activate( $prevToken, add )
    }

  , activate: function ($token, add, multi, remember) {

      if (!$token) return

      if (typeof remember === 'undefined') var remember = true

      if (multi) var add = true

      this.$copyHelper.focus()

      if (!add) {
        this.$wrapper.find('.active').removeClass('active')
        if (remember) {
          this.$firstActiveToken = $token
        } else {
          delete this.$firstActiveToken
        }
      }

      if (multi && this.$firstActiveToken) {
        // Determine first active token and the current tokens indicies
        // Account for the 1 hidden textarea by subtracting 1 from both
        var i = this.$firstActiveToken.index() - 2
          , a = $token.index() - 2
          , _self = this

        this.$wrapper.find('.token').slice( Math.min(i, a) + 1, Math.max(i, a) ).each( function() {
          _self.activate( $(this), true )
        })
      }

      $token.addClass('active')
      this.$copyHelper.val( this.getTokensList( null, null, true ) ).select()
    }

  , activateAll: function() {
      var _self = this

      this.$wrapper.find('.token').each( function (i) {
        _self.activate($(this), i !== 0, false, false)
      })
    }

  , deactivate: function($token) {
      if (!$token) return

      $token.removeClass('active')
      this.$copyHelper.val( this.getTokensList( null, null, true ) ).select()
    }

  , toggle: function($token) {
      if (!$token) return

      $token.toggleClass('active')
      this.$copyHelper.val( this.getTokensList( null, null, true ) ).select()
    }

  , edit: function ($token) {
      if (!$token) return

      var attrs = $token.data('attrs')

      // Allow changing input value before editing
      var options = { attrs: attrs, relatedTarget: $token.get(0) }
      var editEvent = $.Event('tokenfield:edittoken', options)
      this.$element.trigger( editEvent )

      // Edit event can be cancelled if default is prevented
      if (editEvent.isDefaultPrevented()) return

      $token.find('.token-label').text(attrs.value)
      var tokenWidth = $token.outerWidth()

      var $_input = this.$input.hasClass('tt-input') ? this.$input.parent() : this.$input

      $token.replaceWith( $_input )

      this.preventCreateTokens = true

      this.$input.val( attrs.value )
                .select()
                .data( 'edit', true )
                .width( tokenWidth )

      this.update();

      // Indicate that token is now being edited, and is replaced with an input field in the DOM
      this.$element.trigger($.Event('tokenfield:editedtoken', options ))
    }

  , unedit: function (focus) {
      var $_input = this.$input.hasClass('tt-input') ? this.$input.parent() : this.$input
      $_input.appendTo( this.$wrapper )

      this.$input.data('edit', false)
      this.$mirror.text('')

      this.update()

      // Because moving the input element around in DOM
      // will cause it to lose focus, we provide an option
      // to re-focus the input after appending it to the wrapper
      if (focus) {
        var _self = this
        setTimeout(function () {
          _self.$input.focus()
        }, 1)
      }
    }

  , remove: function (e, direction) {
      if (this.$input.is(document.activeElement) || this._disabled || this._readonly) return

      var $token = (e.type === 'click') ? $(e.target).closest('.token') : this.$wrapper.find('.token.active')

      if (e.type !== 'click') {
        if (!direction) var direction = 'prev'
        this[direction]()

        // Was it the first token?
        if (direction === 'prev') var firstToken = $token.first().prevAll('.token:first').length === 0
      }

      // Prepare events and their options
      var options = { attrs: this.getTokenData( $token ), relatedTarget: $token.get(0) }
        , removeEvent = $.Event('tokenfield:removetoken', options)

      this.$element.trigger(removeEvent);

      // Remove event can be intercepted and cancelled
      if (removeEvent.isDefaultPrevented()) return

      var removedEvent = $.Event('tokenfield:removedtoken', options)
        , changeEvent = $.Event('change', { initiator: 'tokenfield' })

      // Remove token from DOM
      $token.remove()

      // Trigger events
      this.$element.val( this.getTokensList() ).trigger( removedEvent ).trigger( changeEvent )

      // Focus, when necessary:
      // When there are no more tokens, or if this was the first token
      // and it was removed with backspace or it was clicked on
      if (!this.$wrapper.find('.token').length || e.type === 'click' || firstToken) this.$input.focus()

      // Adjust input width
      this.$input.css('width', this.options.minWidth + 'px')
      this.update()

      // Cancel original event handlers
      e.preventDefault()
      e.stopPropagation()
    }

    /**
     * Update tokenfield dimensions
     */
  , update: function (e) {
      var value = this.$input.val()
        , inputPaddingLeft = parseInt(this.$input.css('padding-left'), 10)
        , inputPaddingRight = parseInt(this.$input.css('padding-right'), 10)
        , inputPadding = inputPaddingLeft + inputPaddingRight

      if (this.$input.data('edit')) {

        if (!value) {
          value = this.$input.prop("placeholder")
        }
        if (value === this.$mirror.text()) return

        this.$mirror.text(value)

        var mirrorWidth = this.$mirror.width() + 10;
        if ( mirrorWidth > this.$wrapper.width() ) {
          return this.$input.width( this.$wrapper.width() )
        }

        this.$input.width( mirrorWidth )

        if (this.$hint) {
          this.$hint.width( mirrorWidth )
        }
      }
      else {
        var w = (this.textDirection === 'rtl')
              ? this.$input.offset().left + this.$input.outerWidth() - this.$wrapper.offset().left - parseInt(this.$wrapper.css('padding-left'), 10) - inputPadding - 1
              : this.$wrapper.offset().left + this.$wrapper.width() + parseInt(this.$wrapper.css('padding-left'), 10) - this.$input.offset().left - inputPadding;
        //
        // some usecases pre-render widget before attaching to DOM,
        // dimensions returned by jquery will be NaN -> we default to 100%
        // so placeholder won't be cut off.
        isNaN(w) ? this.$input.width('100%') : this.$input.width(w);

        if (this.$hint) {
          isNaN(w) ? this.$hint.width('100%') : this.$hint.width(w);
        }
      }
    }

  , focusInput: function (e) {
      if ( $(e.target).closest('.token').length || $(e.target).closest('.token-input').length || $(e.target).closest('.tt-dropdown-menu').length ) return
      // Focus only after the current call stack has cleared,
      // otherwise has no effect.
      // Reason: mousedown is too early - input will lose focus
      // after mousedown. However, since the input may be moved
      // in DOM, there may be no click or mouseup event triggered.
      var _self = this
      setTimeout(function() {
        _self.$input.focus()
      }, 0)
    }

  , search: function () {
      if ( this.$input.data('ui-autocomplete') ) {
        this.$input.autocomplete('search')
      }
    }

  , disable: function () {
      this.setProperty('disabled', true);
    }

  , enable: function () {
      this.setProperty('disabled', false);
    }

  , readonly: function () {
      this.setProperty('readonly', true);
    }

  , writeable: function () {
      this.setProperty('readonly', false);
    }

  , setProperty: function(property, value) {
      this['_' + property] = value;
      this.$input.prop(property, value);
      this.$element.prop(property, value);
      this.$wrapper[ value ? 'addClass' : 'removeClass' ](property);
  }

  , destroy: function() {
      // Set field value
      this.$element.val( this.getTokensList() );
      // Restore styles and properties
      this.$element.css( this.$element.data('original-styles') );
      this.$element.prop( 'tabindex', this.$element.data('original-tabindex') );

      // Re-route tokenfield label to original input
      var $label = $( 'label[for="' + this.$input.prop('id') + '"]' )
      if ( $label.length ) {
        $label.prop( 'for', this.$element.prop('id') )
      }

      // Move original element outside of tokenfield wrapper
      this.$element.insertBefore( this.$wrapper );

      // Remove tokenfield-related data
      this.$element.removeData('original-styles')
                   .removeData('original-tabindex')
                   .removeData('bs.tokenfield');

      // Remove tokenfield from DOM
      this.$wrapper.remove();
      this.$mirror.remove();

      var $_element = this.$element;

      return $_element;
  }

  }


 /* TOKENFIELD PLUGIN DEFINITION
  * ======================== */

  var old = $.fn.tokenfield

  $.fn.tokenfield = function (option, param) {
    var value
      , args = []

    Array.prototype.push.apply( args, arguments );

    var elements = this.each(function () {
      var $this = $(this)
        , data = $this.data('bs.tokenfield')
        , options = typeof option == 'object' && option

      if (typeof option === 'string' && data && data[option]) {
        args.shift()
        value = data[option].apply(data, args)
      } else {
        if (!data && typeof option !== 'string' && !param) {
          $this.data('bs.tokenfield', (data = new Tokenfield(this, options)))
          $this.trigger('tokenfield:initialize')
        }
      }
    })

    return typeof value !== 'undefined' ? value : elements;
  }

  $.fn.tokenfield.defaults = {
    minWidth: 60,
    minLength: 0,
    allowEditing: true,
    allowPasting: true,
    limit: 0,
    autocomplete: {},
    typeahead: {},
    showAutocompleteOnFocus: false,
    createTokensOnBlur: false,
    delimiter: ',',
    beautify: true,
    inputType: 'text'
  }

  $.fn.tokenfield.Constructor = Tokenfield


 /* TOKENFIELD NO CONFLICT
  * ================== */

  $.fn.tokenfield.noConflict = function () {
    $.fn.tokenfield = old
    return this
  }

  return Tokenfield;

}));

/* ========================================================================
 * Bootstrap: scrollspy.js v3.1.1
 * http://getbootstrap.com/javascript/#scrollspy
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // SCROLLSPY CLASS DEFINITION
  // ==========================

  function ScrollSpy(element, options) {
    var href
    var process  = $.proxy(this.process, this)

    this.$element       = $(element).is('body') ? $(window) : $(element)
    this.$body          = $('body')
    this.$scrollElement = this.$element.on('scroll.bs.scroll-spy.data-api', process)
    this.options        = $.extend({}, ScrollSpy.DEFAULTS, options)
    this.selector       = (this.options.target
      || ((href = $(element).attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) //strip for ie7
      || '') + ' .nav li > a'
    this.offsets        = $([])
    this.targets        = $([])
    this.activeTarget   = null

    this.refresh()
    this.process()
  }

  ScrollSpy.DEFAULTS = {
    offset: 10
  }

  ScrollSpy.prototype.refresh = function () {
    var offsetMethod = this.$element[0] == window ? 'offset' : 'position'

    this.offsets = $([])
    this.targets = $([])

    var self     = this
    var $targets = this.$body
      .find(this.selector)
      .map(function () {
        var $el   = $(this)
        var href  = $el.data('target') || $el.attr('href')
        var $href = /^#./.test(href) && $(href)

        return ($href
          && $href.length
          && $href.is(':visible')
          && [[ $href[offsetMethod]().top + (!$.isWindow(self.$scrollElement.get(0)) && self.$scrollElement.scrollTop()), href ]]) || null
      })
      .sort(function (a, b) { return a[0] - b[0] })
      .each(function () {
        self.offsets.push(this[0])
        self.targets.push(this[1])
      })
  }

  ScrollSpy.prototype.process = function () {
    var scrollTop    = this.$scrollElement.scrollTop() + this.options.offset
    var scrollHeight = this.$scrollElement[0].scrollHeight || this.$body[0].scrollHeight
    var maxScroll    = scrollHeight - this.$scrollElement.height()
    var offsets      = this.offsets
    var targets      = this.targets
    var activeTarget = this.activeTarget
    var i

    if (scrollTop >= maxScroll) {
      return activeTarget != (i = targets.last()[0]) && this.activate(i)
    }

    if (activeTarget && scrollTop <= offsets[0]) {
      return activeTarget != (i = targets[0]) && this.activate(i)
    }

    for (i = offsets.length; i--;) {
      activeTarget != targets[i]
        && scrollTop >= offsets[i]
        && (!offsets[i + 1] || scrollTop <= offsets[i + 1])
        && this.activate( targets[i] )
    }
  }

  ScrollSpy.prototype.activate = function (target) {
    this.activeTarget = target

    $(this.selector)
      .parentsUntil(this.options.target, '.active')
      .removeClass('active')

    var selector = this.selector +
        '[data-target="' + target + '"],' +
        this.selector + '[href="' + target + '"]'

    var active = $(selector)
      .parents('li')
      .addClass('active')

    if (active.parent('.dropdown-menu').length) {
      active = active
        .closest('li.dropdown')
        .addClass('active')
    }

    active.trigger('activate.bs.scrollspy')
  }


  // SCROLLSPY PLUGIN DEFINITION
  // ===========================

  var old = $.fn.scrollspy

  $.fn.scrollspy = function (option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.scrollspy')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.scrollspy', (data = new ScrollSpy(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.scrollspy.Constructor = ScrollSpy


  // SCROLLSPY NO CONFLICT
  // =====================

  $.fn.scrollspy.noConflict = function () {
    $.fn.scrollspy = old
    return this
  }


  // SCROLLSPY DATA-API
  // ==================

  $(window).on('load', function () {
    $('[data-spy="scroll"]').each(function () {
      var $spy = $(this)
      $spy.scrollspy($spy.data())
    })
  })

}(jQuery);
