$(document).ready(function () {
 /**
     * [Total Port Search]
     */
    $('#SearchProduct').keyup(function() {
      
        var SearchProduct = $( '#SearchProduct' ).val();
   
       
        var value = '?SearchProduct=' + SearchProduct;  
        
        $('#salesLoader').before('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

        $.ajax({
            type: "GET",
            url: basePath + 'sales/product/search' + value
        }).success(function ( result ) {

            $('#productSearchingResult').html(result);
            $('#loader').slideUp(200,function() {        
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow"); 

        }).error(function ( result ) {

            console.log('Information not found');

        }); 
    });
    $('#supplier_id').click(function() {
        var supplier_id = $( '#supplier_id' ).val();

        var value = '?supplier_id=' + supplier_id;  
        
        $.ajax({
            type: "GET",
            url: basePath + 'supplier/address' + value
        }).success(function ( result ) {

            $('#supplier_address').val( result );
            $('#loader').slideUp(200,function(){        
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow"); 

        }).error(function ( result ) {

            console.log('Information not found');

        }); 
    });

    $('#masterTransactionSeatch').click(function() {
      
        var transactionType = $( '#transactionType' ).val();
        var datefrom        = $( '#datefrom' ).val();
        var dateto          = $( '#dateto' ).val();

        var value = '?transactionType=' + transactionType + '&datefrom=' + datefrom + '&dateto=' + dateto;  
        
        $('#masterTransactionResult').before('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

        $.ajax({
            type: "GET",
            url: basePath + 'master/transection/search/type' + value
        }).success(function ( result ) {

            $('#masterTransactionResult').html(result);
            $('#loader').slideUp(200,function() {        
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow"); 

        }).error(function ( result ) {

            console.log('Information not found');

        }); 
    });

    /**
     * Switch Type Search
     * @param  {[type]} ) {                         var transactionType [description]
     * @return {[type]}   [description]
     */
    $('#switchTypeSearch').click(function() {
      
        var switchPort = $( '#switchPort' ).val();
     

        var value = '?switchPort=' + switchPort;  
        
        $('#masterBoxManagement').before('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

        $.ajax({
            type: "GET",
            url: basePath + 'master/box/management/switch/search' + value
        }).success(function ( result ) {

            $('#boxManagementReports').html(result);
            $('#loader').slideUp(200,function() {        
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow"); 

        }).error(function ( result ) {

            console.log('Information not found');

        }); 
    });

    /**
     * [Pop Adress Search]
     */
    $('#popAdressSearch').click(function() {
      
        var pop = $( '#pop' ).val();
       

        var value = '?pop=' + pop ;  
        
        $('#masterBoxManagement').before('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

        $.ajax({
            type: "GET",
            url: basePath + 'master/box/management/popaddress/search' + value
        }).success(function ( result ) {

            $('#boxManagementReports').html(result);
            $('#loader').slideUp(200,function() {        
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow"); 

        }).error(function ( result ) {

            console.log('Information not found');

        }); 
    });

    /**
     * [Mc Type Search]
     */
    $('#MCTypeSearch').click(function() {
      
        var mcType = $( '#mcType' ).val();
       

        var value = '?mcType=' + mcType ;  
        
        $('#masterBoxManagement').before('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

        $.ajax({
            type: "GET",
            url: basePath + 'master/box/management/mctype/search' + value
        }).success(function ( result ) {

            $('#boxManagementReports').html(result);
            $('#loader').slideUp(200,function() {        
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow"); 

        }).error(function ( result ) {

            console.log('Information not found');

        }); 
    });

    /**
     * [Mc Type Search]
     */
    $('#SFPTypeSearch').click(function() {
      
        var sfType = $( '#sfType' ).val();
       

        var value = '?sfType=' + sfType ;  
        
        $('#masterBoxManagement').before('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

        $.ajax({
            type: "GET",
            url: basePath + 'master/box/management/sfptype/search' + value
        }).success(function ( result ) {

            $('#boxManagementReports').html(result);
            $('#loader').slideUp(200,function() {        
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow"); 

        }).error(function ( result ) {

            console.log('Information not found');

        }); 
    });

    /**
     * [Total Port Search]
     */
    $('#TotalPortSearch').click(function() {
      
        var month = $( '#month' ).val();
        var year = $( '#year' ).val();
        var type = $( '#type' ).val();
       

        var value = '?month=' + month +  '&year=' + year + '&type=' + type  ;  
        
        $('#masterBoxManagement').before('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

        $.ajax({
            type: "GET",
            url: basePath + 'master/box/management/port/search' + value
        }).success(function ( result ) {

            $('#boxManagementReports').html(result);
            $('#loader').slideUp(200,function() {        
                $('#loader').remove();
            });
            $(".loader").fadeOut("slow"); 

        }).error(function ( result ) {

            console.log('Information not found');

        }); 
    });
    

});
 
$('#masterSummaryReportSearch').click(function() {
      
    var transactionType = $( '#transactionType' ).val();
    var datefrom        = $( '#datefrom' ).val();
    var dateto          = $( '#dateto' ).val();

    var value = '?transactionType=' + transactionType + '&datefrom=' + datefrom + '&dateto=' + dateto;  
    
    $('#masterSummaryLoader').before('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');

    $.ajax({
        type: "GET",
        url: basePath + 'master/summary/reports/search' + value

    }).success(function ( result ) {

        $('#masterSummaryReportSearchResult').html(result);
        $('#loader').slideUp(200,function() {        
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 

    }).error(function ( result ) {

        $('#loader').slideUp(200,function() {        
            $('#loader').remove();
        });
        $(".loader").fadeOut("slow"); 
        $('#masterSummaryReportSearchResult').html('<span class="not-found"> Not Found</span>');
        console.log('Information not found');

    }); 
});


function productSetupDeleted(id,name) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info').html('Are you sure ! You want to delete <strong>'+ name +'</strong>?<br>' +
        '<br><div class="note note-warning">' +
        ""+
    '</div>');
    
    $('.modal-footer').on('click', '#delete', function() {
        $('#deletenotification').html("<div class='alert alert-success text-center'><i class='fa fa-check'></i>"  + name + " Deleted successfully</div>");
        
        $('#product_Setup_' + id ).remove();
        $.ajax( {
            type: 'DELETE',
            url: basePath + 'new/product/setup/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function(data) { }
        });
    });
}

$(document).on('keyup', '.quantityEdit,.unitPriceEdit', function() {


    var  str         = $(this).attr('id');
    var ID           = str.split("_",3);
    var ids = ID[1];

    var quantity  =  $('#quantityEdit_'+ids).val();
    var unitprice = $('#unitPriceEdit_'+ids).val();
    var totalprice  = quantity*unitprice;
  
    $('#totalPriceEdit_'+ids).val(totalprice);
});

    /**
 * [editProductInformation description]
 */
/*function editProductInformation(id,quantity,product_name,status,transectionID,unitPrice,totalPrice,product_id) {

    $('#editModal').appendTo("body").modal('show');

    $('#quantityEdit').val(quantity);
    $('#unitPriceEdit').val(unitPrice)
    
    $('#product_id').append(new Option('Foo', 'foo', true, true));

    var quantity =  $('#quantityEdit').val();
    var unitprice = $('#unitPriceEdit').val();

    var totalprice = quantity*unitprice;


    $('#totalPriceEdit').val(totalPrice);

    var values = '?id=' + id + '&quantity=' + $('#quantityEdit').val() + '&unitprice=' + unitprice+ '&totalprice=' + totalprice + '&product_id=' + product_id;  

    $('.modal-footer').on('click', '#delete', function() {
               alert(values);
        $.ajax( {
            type: 'GET',
            url: basePath + 'update/storesdetails/products'+values,
            
            success: function(data) {
                swal("Good job!", "Your information has update successfully", "success");
                    // location.reload();
             }
        });
    });
}*/

/**
 * [meanufacetDelete description]
 */
function meanufacetDelete(id,name) {
    $('#deleteModal').appendTo("body").modal('show');

    $('#info').html('Are you sure ! You want to delete <strong>'+ name +'</strong>?<br>' +
        '<br><div class="note note-warning">' +
        ""+
    '</div>');
    
    $('.modal-footer').on('click', '#delete', function() {
        $('#deletenotification').html("<div class='alert alert-success text-center'><i class='fa fa-check'></i>"  + name + " Deleted successfully</div>");
        
        $('#manufacturer_delete_' + id ).remove();
        $.ajax( {
            type: 'DELETE',
            url: basePath + 'manufacturer/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function(data) { }
        });
    });
}


/**
 * [meanufacetDelete description]
 */
function productUnitDeleted(id,name) {
    $('#deleteModal').appendTo("body").modal('show');

    $('#info').html('Are you sure ! You want to delete <strong>'+ name +'</strong> ?<br>' +
        '<br><div class="note note-warning">' +
        ""+
    '</div>');
    
    $('.modal-footer').on('click', '#delete', function() {
        $('#deletenotification').html("<div class='alert alert-success text-center'><i class='fa fa-check'></i>"  + name + " Deleted successfully</div>");
        
        $('#product_unit_' + id ).remove();
        $.ajax( {
            type: 'DELETE',
            url: basePath + 'product/unit/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function(data) { }
        });
    });
}

/**
 * [productCategoryDeleted description]
 */
function productCategoryDeleted(id,name) {
    $('#deleteModal').appendTo("body").modal('show');

    $('#info').html('Are you sure ! You want to delete <strong>'+ name +'</strong> ?<br>' +
        '<br><div class="note note-warning">' +
        ""+
    '</div>');
    
    $('.modal-footer').on('click', '#delete', function() {
        $('#deletenotification').html("<div class='alert alert-success text-center'><i class='fa fa-check'></i>"  + name + " Deleted successfully</div>");
        
        $('#product_category_' + id ).remove();
        $.ajax( {
            type: 'DELETE',
            url: basePath + 'product/category/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function(data) { }
        });
    });
}

/**
 * [Transitiction Type Deleted description]
 */
function transitictionTypeDeleted(id,name) {
    
    $('#deleteModal').appendTo("body").modal('show');

    $('#info').html('Are you sure ! You want to delete <strong>'+ name +'</strong> ?<br>' +
        '<br><div class="note note-warning">' +
        ""+
    '</div>');
    
    $('.modal-footer').on('click', '#delete', function() {
        $('#deletenotification').html("<div class='alert alert-success text-center'><i class='fa fa-check'></i>"  + name + " Deleted successfully</div>");
        
        $('#transaction_' + id ).remove();
        $.ajax( {
            type: 'DELETE',
            url: basePath + 'transaction/type/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function(data) { }
        });
    });
}



$(document).on('change click keyup dp.change', '#pay_date', function() {
    $(".payamount").removeAttr( "readonly" );
});
// function product_availble(id,qty) { 
/**
 * [product_availble description]
 */
$(document).on('change click keyup', '#payamount', function() {
// function product_availble(id,qty) { 

    var totalAmount = $('#totalAmount').val();
    var payamount   = $('#payamount').val();
    var totalDue    = totalAmount-payamount;
    $("#dueAmount").val(totalDue);  
});

/**
 * [dueAmountSetupInventory description]
 * @return {[type]} [description]
 */
function dueAmountSetupInventory(id) {
    var totalAmount = $('#totalAmount_'+id).val();
    var payamount   = $('#payamount_'+id).val();

    var totalDue    = totalAmount-payamount;

    $("#dueAmount_"+id).val(totalDue);  
}

$(document).on('change click keyup', '#quantity_calculate,#product_id,#discount,#adj,#vat', function() {
// function product_availble(id,qty) { 

    var productID = $('#product_id').val();

    var find_info  ='?productID='+ productID; 
   
    $.ajax( {
        type: 'GET',
        url: basePath + 'product/availible' + find_info,
        success: function(data) { 
                 var price     = data.split(",");
                 var QTY       = $('#quantity_calculate').val();
                 var discount  = $('#discount').val();
                 var adj       = $('#adj').val();
                 var vat       = $('#vat').val();
                 var dueamount = $('#dueAmount').val();
                
                $("#available_products").val(price[0]-QTY);
                $("#sale_price").val(price[1]);
                $("#hiddenqty").val(price[0]);
                $("#totalAmount").val(price[1]*QTY);
                var totalPaymount = (price[1]*QTY - discount -adj-dueamount)+ +vat;
                $("#payamount").val(totalPaymount);

        }
    });

});

$(document).on('change click', '.products-name', function() {

    var productID    = $(this).val();
    var  str         = $(this).attr('id');
    var ID           = str.split("_",3);
    var unitprice_id = ID[2];

    var find_info    = '?productID='+ productID; 

    $.ajax( {
        type: 'GET',
        url: basePath + 'product/availible' + find_info,
        success: function(data) { 

            $("#unit_price_" + unitprice_id).val(data);
            $("#hidden_unit_price_" + unitprice_id).val(data);
                
        }
    });
   
});

$(document).on('keyup', '#quantity_calculate', function() {

    function getIntegerValue(data) {

        if (typeof data === 'undefiend' || data == null || data == '') {

            return 0;

        } else {

            return parseInt(data);

        }
    }
    var availibleProduct  = getIntegerValue($("#hiddenqty").val());
    var productQty        = getIntegerValue($(this).val());
    
    var totalFreeProduct  = 0;
    var totalFreeProduct  = (+availibleProduct) -  (+productQty);

    if(totalFreeProduct >= 0) {

        $("#available_products").val(totalFreeProduct);  

    } else {

        $("#available_products").val(availibleProduct); 
        $("#quantity_calculate").val(0);

    }

});


$(document).on('keyup', '.qty-calculateds', function() {

    function getIntegerValue(data) {

        if (typeof data === 'undefiend' || data == null || data == '') {

            return 0;

        } else {

            return parseInt(data);

        }
    }

    var str          = $(this).attr('id');
    var ID           = str.split("_",3);
    var unitprice_id = ID[2];
 
    var availibleProduct  = getIntegerValue($("#hidden_unit_price_"+unitprice_id).val());
    var productQty        = getIntegerValue($(this).val());
    
    var totalFreeProduct  = 0;
    var totalFreeProduct  = (+availibleProduct) -  (+productQty);
    if(totalFreeProduct >= 0) {

        $("#unit_price_"+unitprice_id).val(totalFreeProduct); 

    } else {
        
        $("#unit_price_"+unitprice_id).val(availibleProduct); 
        $("#quantity_id_"+unitprice_id).val(0);

    }

    

});
/**
 * [Transitiction Type Deleted description]
 */
function approvedDisapprovedProduct(pid,id,qty,name,status,transactionid,disapprovestatus) {
    
    $('#approvedModal').appendTo("body").modal('show');
    if(status == 1) {

       $('#info p').html('Are you want to approve this product  <strong>'+ name +'</strong> ?</div>'); 
    
    } else {

        $('#info p').html('Are you want to disapprove this product  <strong>'+ name +'</strong> ?</div>');
    
    }
        
    
    var find_info  ='?id='+ id +'&qty='+qty+'&pid='+pid +'&status=' + status +'&transactionid=' + transactionid + '&disapprovestatus=' + disapprovestatus; 

    $('.modal-footer').on('click', '#approve', function() {
        $('#deletenotification').html("<div class='alert alert-success text-center'><i class='fa fa-check'></i>"  + name + " Deleted successfully</div>");
        
        $('#transaction_' + id ).remove();
        $.ajax( {
            type: 'POST',
            url: basePath + 'product/approve/disapprove' + find_info,
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            }, success: function(data) {
                if(data==401) {

                    swal("Sorry!", "There is no sufficient stock.", "error");

                } else if(data == 402){

                    swal("Good job!", "Thank you! Successfully disapproved this product.", "success");
                    location.reload();

                } else {

                    swal("Good job!", "Thank you! Successfully approved this product.", "success");
                    location.reload();
                    
                }
                
            },error: function(data) {
                 swal("Error!", "Sorry! Sorry can not approve this product.", "error");
            },
        });
    });
}

/**
 * [Transitiction Type Deleted description]
 */
function editReantry(id,qty,name,status,transactionid,disapprovestatus) {
    
    $('#approvedModal').appendTo("body").modal('show');
    if(status == 1) {

       $('#info').html('Are you want to approve this product  <strong>'+ name +'</strong> ?</div>'); 
    
    } else {

        $('#info').html('Are you want to disapprove this product  <strong>'+ name +'</strong> ?</div>');
    
    }
        
    
    var find_info  ='?id='+ id +'&qty='+qty +'&status=' + status +'&transactionid=' + transactionid + '&disapprovestatus=' + disapprovestatus; 

    $('.modal-footer').on('click', '#approve', function() {
        $('#deletenotification').html("<div class='alert alert-success text-center'><i class='fa fa-check'></i>"  + name + " Deleted successfully</div>");
        
        $('#transaction_' + id ).remove();
        $.ajax( {
            type: 'POST',
            url: basePath + 'product/approve/disapprove' + find_info,
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            }, success: function(data) {
                if(data==401) {

                    swal("Sorry!", "There is no sufficient stock.", "error");

                } else if(data == 402){

                    swal("Good job!", "Thank you! Successfully disapproved this product.", "success");
                    location.reload();

                } else {

                    swal("Good job!", "Thank you! Successfully approved this product.", "success");
                    location.reload();
                    
                }
                
            },error: function(data) {
                 swal("Error!", "Sorry! Sorry can not approve this product.", "error");
            },
        });
    });
}

// Add product Sell
function addProductSell(id,product_name,current_stock,sale_price,product_id) {

            var length = id;
            var productCheck = $('#product_id_'+ length).val();
           
            if(productCheck===undefined) {
               
            } else {
               return swal("Error!", "Sorry! already submit please try to anaother product", "error");
                
            }
            $('.entry').html('');
            $( "#newPara" ).before( '<tr id="property_'+ length + '"><td></td><td class="col-md-1"><input   id="product_name_'+ length +'" class="form-control" placeholder="Stock Qty" name="product_name[]" type="hidden" value="'+product_name+'"  readonly><span style="color:#666"> '+product_name+'</span><input type="hidden" name="product_id[]" id="product_id_'+ length + '" value='+product_id+'></input><input type="hidden" name="product_price_id[]" class="inventory_price_id" id="product_price_id_'+ length + '" value='+id+'></input></td><td> <input   id="available_products_'+ length +'" class="form-control" placeholder="Stock Qty" name="unit_price[]" type="text" value="'+current_stock+'"  readonly><td> <input   id="sale_price_'+ length +'" class="form-control" placeholder="Sale.price" name="sale_price[]" readonly type="text" id="sale_price_'+ length +'" value="'+sale_price+'"><input  id="hidden_unit_price_'+ length +'" class="form-control" placeholder="Stock Qty" name="unit_price[]" type="hidden" id="available_products_"'+ length +' readonly>  </td><td> <input class="form-control qty-calculateds" placeholder="Qty" id="quantityId_'+ length + '" value="0" onkeyup="productListCalculate('+length+')" name="quantity[]" type="number"> </td><td> <input class="form-control qty-calculateds" placeholder="discount" value="0" id="discount_'+ length + '" name="discount[]" type="number" value="0" onkeyup="productListCalculate('+length+')"> </td><td> <input class="form-control qty-calculateds" placeholder="Adj" id="adjustment_'+ length + '" name="adj[]" value="0" type="number"  onkeyup="productListCalculate('+length+')"> </td><td> <input class="form-control qty-calculateds" placeholder="Vat" value="0" id="vat_'+ length + '" name="vat[]" type="number"  onkeyup="productListCalculate('+length+')"> </td>><td> <input class="form-control qty-calculateds" value="0" placeholder="T P." id="totalAmount_'+ length + '" name="totalamount[]"  type="number"  onkeyup="productListCalculate('+length+')" readonly> </td><input class="form-control" value="0" placeholder="t. Amount" id="totalAmount" name="totalamount[]" type="number"><td><input class="form-control payamount" placeholder="pamount" value="0" id="payamount_'+ length + '" readonly onkeyup="dueAmountSetupInventory('+length+')" name="payamount[]" type="number"></td><td> <input class="form-control" placeholder="c. pamount" id="charge_amount" name="chageamount[]" value="0" type="number"></td><td> <input class="form-control" placeholder="D. Amount"  readonly name="dueamount[]" type="number" id="dueAmount_'+ length + '" value="0" onkeyup="productListCalculate('+length+')">/td><td> <span class="input-group-btn"> <button class="btn btn-danger btn-remove_'+ length+'" type="button">  <span class="glyphicon glyphicon-minus"></span> </button> </span> </td></tr>' );
}

function productListCalculate(id) {
   

    var sale_price = $('#sale_price_'+id).val();
    var availibleP = $('#available_products_'+id).val();

    var QTY        = $('#quantityId_'+id).val();
     if(availibleP< QTY){
        
        return $('#quantityId_'+id).val(0);
    }
    var discount   = $('#discount_'+id).val();
    var adj        = $('#adjustment_'+id).val();
    var vat        = $('#vat_'+id).val();
    var dueAmount  = $('#dueAmount_'+id).val();
    
    // $('#available_products_'+id).val(sale_price-QTY);
    $('#sale_price_'+id).val(sale_price);
    // $('#hiddenqty_'+id).val(sale_price);
    $('#totalAmount_'+id).val(sale_price*QTY);
    var totalPaymount = (sale_price*QTY - discount -adj -dueAmount)+ +vat;
    $('#payamount_'+id).val(totalPaymount);
 
    

    var inputs = document.getElementsByClassName( 'inventory_price_id' ),
    names  = [].map.call(inputs, function( input ) {
        totalAmountID = input.value;
        var gandTotalS = $('#payamount_'+totalAmountID).val();
        return gandTotalS;
    }).join('+');

    var res = names.split("+");
    var i;
    var gandAmount=0;

    for( i=0; i<res.length;i++) {
        var gandAmount = +gandAmount+ +res[i];
    }
    
   
    $('#saleTotalAmount').val(gandAmount);
    $('#saleSubTotalAmount').val(gandAmount);
   
}

function productListAvailbleProduct(id) {
    var productID = $('#product_id_'+id).val();

    var find_info  ='?productID='+ productID; 
  
    $.ajax( {
        type: 'GET',
        url: basePath + 'product/availible' + find_info,
        success: function(data) { 
                 var price           = data.split(",");
                 var QTY      = $('#quantity_id_'+id).val();
                 var discount = $('#discount_'+id).val();
                 var adj      = $('#adjustment_'+id).val();
                 var vat      = $('#vat_'+id).val();
                 var dueAmount = $('#dueAmount_'+id).val();
              
                $('#available_products_'+id).val(price[0]-QTY);
                $('#sale_price_'+id).val(price[1]);
                $('#hiddenqty_'+id).val(price[0]);
                $('#totalAmount_'+id).val(price[1]*QTY);
                var totalPaymount = (price[1]*QTY - discount -adj -dueAmount)+ +vat;
                $('#payamount_'+id).val(totalPaymount);

        }
    });
}
function inventorySellPostPrint() {
    var sl = 1;
    var inputs = document.getElementsByClassName( 'inventory_price_id' ),
        names  = [].map.call(inputs, function( input ) {
            
            id = input.value;
            var sale_price = $('#sale_price_'+id).val();
            var availibleP = $('#available_products_'+id).val();
            var payamount  = $('#payamount_'+id).val();
            var QTY        = $('#quantityId_'+id).val();
            var productName = $('#product_name_'+id).val();

            var discount   = $('#discount_'+id).val();
            var adj        = $('#adjustment_'+id).val();
            var vat        = $('#vat_'+id).val();
            var dueAmount  = $('#dueAmount_'+id).val();
            var subTotal   = $('#saleSubTotalAmount').val();
            var totalAmt   = $('#saleTotalAmount').val();
            var serial =sl++;
            var html = '<tr style="border-bottom: 1px solid #eee;text-align: center;"><td>'+ serial +'</td><td>'+productName+'</td><td>'+QTY+'</td><td>'+sale_price+'</td><td>'+payamount+'</td></tr>';
            return html;
        });

    var totAmt = $('#saleTotalAmount').val();
    $("#pos_totalAmountPay").html(totAmt);
    $("#pos_table_result").html(names);
    // alert(names);
    // Date
    var date = $("#selectDatetime").val();
    $("#pos_sale_date_print").html(date);

    // Name
    var customer_name = $("#customer_name").val();
    $("#pos_sale_name_print").html(customer_name);

    // Date
    var salesPerson = $("#salesPerson option:selected").text();
    var product_id   = $("#product_id option:selected").text();
   
    $("#pos_sale_sold_print").html(salesPerson);

    // Phone Number
    var phone_number = $("#customer_phone_number").val();
    $("#pos_sale_phone_print").html(phone_number);

    

    // Phone Number
    var phone_number = $("#customer_phone_number").val();
    $("#pos_sale_phone_print").html(phone_number);
     // Phone Number
    var quantity_calculate = $("#quantity_calculate").val();
    var totalAmount        = $("#totalAmount").val();
    var salePrice          = $("#sale_price").val();
 
   
    // Bill Number
   
    $("#pos_sale_bill_print").html(totalAmount);

    $("#posPrint").printMe({
        "path" : ["{{ URL::asset( 'assets/css/custom.css' ) }}"],
       
        // "title" : "Document title"
    });
}
function inventorySellPrint() {
    var sl = 1;
    var inputs = document.getElementsByClassName( 'inventory_price_id' ),
        names  = [].map.call(inputs, function( input ) {
            
            id = input.value;
            var sale_price = $('#sale_price_'+id).val();
            var availibleP = $('#available_products_'+id).val();
            var payamount  = $('#payamount_'+id).val();
            var QTY        = $('#quantityId_'+id).val();
            var productName        = $('#product_name_'+id).val();

            var discount   = $('#discount_'+id).val();
            var adj        = $('#adjustment_'+id).val();
            var vat        = $('#vat_'+id).val();
            var dueAmount  = $('#dueAmount_'+id).val();
            var subTotal   = $('#saleSubTotalAmount').val();
            var totalAmt   = $('#saleTotalAmount').val();
            var serial =sl++;
            var html = '<tr style="border-bottom: 1px solid #eee;text-align: center;"><td>'+ serial +'</td><td>'+productName+'</td><td>'+QTY+'</td><td>'+sale_price+'</td><td>'+payamount+'</td></tr>';
            return html;
        });

    var totAmt = $('#saleTotalAmount').val();
    $("#totalAmountPay").html(totAmt);
    $("#table_result").html(names);
    // alert(names);
    // Date
    var date = $("#selectDatetime").val();
    $("#sale_date_print").html(date);

    // Name
    var customer_name = $("#customer_name").val();
    $("#sale_name_print").html(customer_name);

    // Date
    var salesPerson = $("#salesPerson option:selected").text();
    var product_id   = $("#product_id option:selected").text();
   
    $("#sale_sold_print").html(salesPerson);

    // Phone Number
    var phone_number = $("#customer_phone_number").val();
    $("#sale_phone_print").html(phone_number);

    

    // Phone Number
    var phone_number = $("#customer_phone_number").val();
    $("#sale_phone_print").html(phone_number);
     // Phone Number
    var quantity_calculate = $("#quantity_calculate").val();
    var totalAmount        = $("#totalAmount").val();
    var salePrice          = $("#sale_price").val();
 
   
    // Bill Number
   
    $("#sale_bill_print").html(totalAmount);

    $("#print").printMe({
        "path" : ["{{ URL::asset( 'assets/css/custom.css' ) }}"],
       
        // "title" : "Document title"
    });
}