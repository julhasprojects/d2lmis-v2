$(document).ready(function(){
$("#dhis2login").click(function(){
        var basePath = "http://localhost/demo.d2lmis.org/public/";
        var urlPath  = "https://play.dhis2.org/2.31.3/api/me";
       $('#loader').append('<div class="loader"><img src="'+basePath+'assets/images/loading.gif" alt="Searching......" /></div>');
    
        var username = $('#username').val();
        var password  = $('#password').val();

        var values ='?username='+ username +'&password='+password ; 

        if(username=='') {
            swal("Sorry!", "Enter your DHIS2 username", "error");        
        }else if(password=='') {
            swal("Sorry!", "Enter your DHIS2 password", "error");        
        } else {
            $.ajax({
                type: "POST",
                url: basePath + 'dhis2-login' + values,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function (result) { 
                    if(result==401){
                        swal("Sorry!", "Wrong credential!", "error");
                        $('#loader').slideUp(200,function() {        
                            $('#loader').remove();
                        });
                        $(".loader").fadeOut("slow");
                        window.location.assign(basePath+"d2lmis-dashboard");
                        

                    } else {
                        window.location.assign(basePath+"d2lmis-control-panel");
                    } 
                    /*swal("Great!", "You have logged in successfully!", "success");
                    $('#loader').slideUp(200,function() {        
                        $('#loader').remove();
                    });                    
                    $(".loader").fadeOut("slow"); */
                }, 
                error: function (err) { 
                    console.log(err);
                    swal("Sorry!", err, "error");
                    $('#loader').slideUp(200,function() {        
                        $('#loader').remove();
                    });
                    $(".loader").fadeOut("slow");
                }
            });
            
        };
    });
});