
$( document ).ready(function() {
	$('.widget .widget-title .tools a:nth-child(1)').click(function(){
		$(this).parent().parent().parent().toggleClass('hide-widget',500);
		return false;
	//alert("hi");
	});
	$('.widget .widget-title .tools a:nth-child(2)').click(function(){
		$(this).parent().parent().parent().toggleClass('hide',500);
		return false;
	//alert("hi");
	});

	$('#collection_dates').datepicker();
	//  setup-new-line   -->frontend new line 

   	$('#complaindate').datepicker({
	    format: 'YYYY-MM-DD hh:mm:ss',  
	});
   
   
   $('#rebackdate').datepicker();
   $('#connction_date').datepicker();
   $('#warranty_period').datepicker();
   $('#received_date').datepicker();
   $('#requisition_date').datepicker();
   $('#tocollectaionDate').datepicker();
   $('.selectDatetime').datepicker();
   $('#fromcollectaionDate').datepicker();
   $('#install_date').datepicker();
   $('.collecte_date').datepicker();
   $('.birthdate').datepicker({minDate: new Date()}); 
   $('.installdate').datepicker();
   $('.fnlbilldate').datepicker();
   $('.duedateform').datepicker();
   $('#received_date').datepicker();
   $('#challan_date').datepicker();
   $('.duedateto').datepicker();
   $('.fnlbirthdate').datepicker();
   $('#valid_to').datepicker();
   $('.fnldoingdate').datepicker();
   $('.newlineform').datepicker();
   $('.newlineto').datepicker();
   $('.connect_date').datepicker();
   $('.joiningdate').datepicker({
   		maxDate: new Date(),
   });

   $('.date_of_birth').datepicker({
   		startDate : new Date('1990-01-01'),
   		endDate : '2003-01-01',
   });
   $('#dis_date').datepicker();
   $('#packageChangeRequestFrom').datepicker();
   $('#packageChangeRequestTo').datepicker();
   $('.purchaseDate').datepicker();
   
   $('.dateofpayment').datepicker();
   $('.request_date_package').datepicker();


   // client-agreement -->frontend client agreement
   $('.fcabirthdate').datepicker(); 
   $('.fcabilldate').datepicker(); 

   	// line-disconnect -->frontend line-disconnect
	$('.flddisconmonth').datepicker(); 
	$('#lastdate').datepicker(); 
	$('#issudate').datepicker(); 

	$("#lastdate").click(function() {
	    $(".datepicker-days .day").click(function() {
	        $('.datepicker').hide();
	    });
	});

	$("#issudate").click(function() {
	    $(".datepicker-days .day").click(function() {
	        $('.datepicker').hide();
	    });
	});
	$('.flddisconfrom').datepicker(); 
	$('.flddisconto').datepicker(); 
   
   	// new-employee -->frontend new-employee
	$('.fnebirthdate').datepicker(); 
	$('.fnejoingdate').datepicker(); 

	// create-client-complain -->frontend create-client-complain
	$('.fcccompdate').datepicker(); 
	$('.fccsolvedate').datepicker(); 
	
	// bill-generate -->bill-generate
	$('.currma').datepicker(); 
	
	/*************************************************
	*				Backend Panel					 * 
	**************************************************/
	
	// admin-information -->backend admin-information
	$('.baijoindate').datepicker(); 
	$('.baivalidfrom').datepicker(); 
	$('.baivalidfrom').datepicker(); 
	$('.valid').datepicker(); 
	$('.complianfrom').datepicker(); 
	$('.complianto').datepicker(); 
	$('.hrm-datefrom').datepicker(); 
	$('.connection-datefrom').datepicker(); 
	$('.area-connection-datefrom').datepicker(); 
	$('.disconnection-datefrom').datepicker(); 
	$('.disconnection-datefrom').datepicker(); 
	$('.reconnection-datefrom').datepicker(); 
	$('.collector-datefrom').datepicker(); 
	$('.genarated-date').datepicker(); 
	$('.collected-date').datepicker(); 
	$('.region-date').datepicker(); 
	$('.due-date').datepicker(); 
	$('.due-datefrom').datepicker(); 
	$('.region-datefrom').datepicker(); 
	$('.inventory-cabledatefrom').datepicker(); 
	$('.inventory-cabledateto').datepicker(); 
	$('.inventory-boxto').datepicker(); 
	$('.inventory-boxfrom').datepicker(); 
	$('.area-complaindatefrom').datepicker(); 
	$('.area-complaindateto').datepicker(); 
	$('.category-dateto').datepicker(); 
	$('.category-datefrom').datepicker(); 
	$('.sloved-dateto').datepicker(); 
	$('.sloved-datefrom').datepicker(); 
	$('.type-dateto').datepicker(); 
	$('.type-datefrom').datepicker(); 

	$('.daily-datefrom').datepicker(); 
	// edit-admin -->backend edit-admin
	$('.beajoindate').datepicker(); 
	$('.beavalidfrom').datepicker(); 
	$('.beavalidto').datepicker(); 
	
	// edit-client-complain -->backend edit-client-complain
	$('.becccomplaindate').datepicker(); 
	$('.beccdeadlinetosolve').datepicker(); 

	// technician-employee -->backend technician-employee
	$('.btecomplaindate').datepicker(); 
	$('.btedeadlinetosolve').datepicker(); 

	// billing-information -->backend billing-information
	$('.bbibillfrom').datepicker(); 
	$('.bbibillto').datepicker(); 
	$('.dateto').datepicker(); 
	
	$('.area-datefrom').datepicker(); 
	$('.area-dateto').datepicker(); 
	$('.daily-col-date').datepicker(); 
	$('.area-dateto').datepicker(); 
	$('.datefrom').datepicker(); 
	$('.daily-dateto').datepicker(); 
	$('.area-datefrom').datepicker(); 
	

	var currentTime = new Date();
	// First Date Of the month 
	var startDateFrom = new Date(currentTime.getFullYear(),currentTime.getMonth(),1);
	// Last Date Of the Month 
	var startDateTo = new Date(currentTime.getFullYear(),currentTime.getMonth() +1,0);
	
	$('.currentmonthshow').datepicker({

	    minDate: startDateFrom,
	    maxDate: startDateTo,
	    orientation: "bottom auto"
		   
	}); 
	$(".currentmonthshow").click(function() {
	    $(".datepicker-days .day").click(function() {
	        $('.datepicker').hide();
	    });
	});

});