</div><!-- /.main-content -->
            </div><!-- /.main-container-inner -->

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="icon-double-angle-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

<!-- Add Footer -->
        <!-- basic scripts -->

      

        <script type="text/javascript">
            if("ontouchend" in document) document.write("<script src='<?php echo base_url(); ?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
        </script>
      
        <script src="<?php echo base_url(); ?>assets/js/typeahead-bs2.min.js"></script>

        <!-- page specific plugin scripts :: HOME PAGE -->

        <!--[if lte IE 8]>
          <script src="<?php echo base_url();?>assets/js/excanvas.min.js"></script>
        <![endif]-->

        <script src="<?php echo base_url(); ?>assets/js/jquery-ui-1.10.3.custom.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.ui.touch-punch.min.js"></script>
<!--        <script src="<?php echo base_url(); ?>assets/js/bootstrap-wysiwyg.min.js"></script>-->
        
        <script src="<?php echo base_url(); ?>assets/js/markdown/markdown.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/markdown/bootstrap-markdown.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.hotkeys.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap-wysiwyg.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootbox.min.js"></script>
        
        
<!--        <script src="<?php echo base_url(); ?>assets/js/wysihtml5-0.3.0.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap-wysihtml5.js"></script>-->
<!--        <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.easy-pie-chart.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.sparkline.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.pie.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.resize.min.js"></script>-->
        
        <!--PAGE SPECIFIC JS :: FORM ELEMENTS PAGE-->
        <script src="<?php echo base_url();?>assets/js/chosen.jquery.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/fuelux/fuelux.spinner.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/date-time/bootstrap-datepicker.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/date-time/bootstrap-timepicker.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/date-time/moment.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/date-time/daterangepicker.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap-colorpicker.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.knob.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.autosize.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.inputlimiter.1.3.1.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.maskedinput.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap-tag.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.gritter.min.js"></script>        
        
        <!-- page specific plugin scripts -->
      
       
        <!-- ace scripts -->

        <script src="<?php echo base_url();?>assets/js/ace-elements.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/ace.min.js"></script>
          <script src="<?php echo base_url();?>assets/js/custom.js"></script>
        
        
    </body>
</html>
