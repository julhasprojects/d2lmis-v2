$(document).ready(function () {
    $.fn.datepicker.defaults.format = "yyyy-mm-dd";
    $('.date-picker').datepicker();

	$('#departmentname').change(function() {

		var id = $(this).val();
		var values = '?id=' +id;
		
		$.ajax({
	        type: "GET",
	        url: basePath +'designation/search' + values, 
	        success: function(result)  {
	            $('#designation').html( result );
	        },

	        error: function (xhr, status, error) {
              swal("Error!", "Sorry! Can not take your request. Please check yourdata quality.", "error");
            }
	    });
	});

    Metronic.init();
    Layout.init();
    $('.demo-loading-btn,.demo-loading-btn-ajax,.demo-loading-update-btn-ajax') .click(function () {
        var btn = $(this)
        btn.button('loading')
        setTimeout(function () {  btn.button('reset') }, 8000)
    });
    $('.demo-loading-update-btn-ajax') .click(function () {
        var btn = $(this)
        btn.button('loading')
        setTimeout(function () {  btn.button('reset') }, 8000)
    });
});


/**
 *  Add more designation
 */
var $insertBefore = $('#insertBefore');

var $i = 0;

$('#plusButton').click(function() {

    $i = $i+1;
    $(' <div class="form-group"> <div class="col-md-12 padding-left-zero"><input class="form-control form-control-inline input-medium"  name="designation['+$i+']" type="text"  placeholder="Designation #'+($i+1)+'"/></div></div>').insertBefore($insertBefore);

});

$('#holidaysButton').click(function() {

    $i = $i + 1;
    $(' <div class="form-group"> ' +
   '<div class="col-md-6"><input class="form-control form-control-inline input-medium date-picker'+$i+'" name="date['+$i+']" type="text" value="" placeholder="Date"/></div>' +
   '<div class="col-md-6"><input class="form-control form-control-inline" name="occasion['+$i+']" type="text" value="" placeholder="Occasion"/></div>' +
    '</div>').insertBefore($insertBefore);

    $.fn.datepicker.defaults.format = "yyyy-mm-dd";
    $('.date-picker'+$i).datepicker();

});

var $insertBefore_edit = $('#insertBefore_edit');

var $j = 0;

$('#plus_edit_Button').click(function() {
	$j = $j+1;
    $(' <div class="form-group" id="edit_field"> <div class="col-md-12  padding-left-zero"><input class="form-control form-control-inline input-medium"  name="designation['+$j+']" type="text"  placeholder="Designation #'+($j+1)+'"/></div></div>').insertBefore($insertBefore_edit);
});


function showEdit(id,deptName) {
    $('div[id^="edit_field"]').remove();

    var url =  basePath + 'hrm/departments/'+id;

    url = url.replace(':id',id);
    $('#edit_form').attr('action',url );

    var get_url = basePath + 'hrm/departments/'+ id +'/edit';

    $("#edit_deptName").val(deptName);
    
    $.ajax({

        type: "GET",
        url : get_url,
        data: {"id":id}

    }).done(function(response) {
        $("#deptresponse").html(response);
        $j = $('input#designation').length - 1;
    });
}

/**
 * [Employeer Delete ]
 */
function empdel(id,name) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure ! You want to delete <strong>'+ name +'</strong> ?<br></div>');
    
    $('.modal-footer').on('click', '#delete', function() {
        $('#deletenotification').html("<div class='alert alert-success text-center'><i class='fa fa-check'></i>"  + name + " Deleted successfully</div>");
        
        $('.emp_' + id ).remove();
        $.ajax( {
            type: 'DELETE',
            url: basePath + 'hrm/employees/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function(data) { }
        });
    });

}

/**
 * [Hrm approved and Reject option ]
 */
function hrmapprovedrejectapproval(id,status,name) {

    $('#approvedModal').appendTo("body").modal('show');

    if(status == 1) {

        $('#info p').html('Are you sure ! You want to approved ?<br></div>');  

    }   else {

        $('#info p').html('Are you sure ! You want to reject ?<br></div>');   
    }
    
    
    $('.modal-footer').on('click', '#approve', function() {

        var values = '?status=' +status +'&id=' +id; 

        $.ajax( {
            type: 'GET',
            url: basePath + 'hrm/leave/confirm'+ values,
         
            success: function(data) {
                swal("Good job!", "Thank you! Successfully approved this application.", "success");
                location.reload();
            }
        });
    });

}

/**
 * [salary Edit Delete operation]
 */
function salaryEditDelete(id,dept) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure ! You want to delete <strong>'+ dept +'</strong> Salary?</div>');
    
    $('.modal-footer').on('click', '#delete', function() {
        $('#deletenotification').html("<div class='alert alert-success text-center'><i class='fa fa-check'></i>"  + dept + " Deleted successfully</div>");
        
        $('#salary_' + id ).remove();
        $.ajax( {
            type: 'POST',
            url: basePath + 'hrm/employees/salary/destory/'+id,
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function(data) { }
        });
    });

}


/**
 * [deparmentDelete description]
 */
function deparmentDelete(id,dept) {

	$('#deleteModal').appendTo("body").modal('show');

	$('#info p').html('Are you sure ! You want to delete <strong>'+ dept +'</strong> department ?</div>');

	$('.modal-footer').on('click', '#delete', function() {
       $('#deletenotification').html("<div class='alert alert-success text-center'><i class='fa fa-check'></i>"  + dept + " Deleted successfully</div>");
        
        var notice = $('.department_' + id ).remove();
        $.ajax({
            type: 'DELETE',
            url: basePath + 'hrm/departments/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function(data) { }
        });
    });

}

/**
 * [Employeer Delete ]
 */
function expensesdel(id,dept) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure ! You want to delete <strong>'+dept+'</strong> ?</div>');
    
    $('.modal-footer').on('click', '#delete', function() {
        $('#deletenotification').html("<div class='alert alert-success text-center'><i class='fa fa-check'></i>"  + dept + " Deleted successfully</div>");
        $('.expenses_' + id ).remove();
        $.ajax({
            type: 'DELETE',
            url: basePath + 'hrm/expenses/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function(data) { }
        });
    });

}


/**
 * [Employeer Delete ]
 */
function awardsdel(id,dept) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure ! You want to delete <strong>'+dept+'</strong> ?</div>');
    
    $('.modal-footer').on('click', '#delete', function() {
        $('#deletenotification').html("<div class='alert alert-success text-center'><i class='fa fa-check'></i>"  + dept + "Deleted successfully</div>");
    
        $('.awards_' + id ).remove();
        $.ajax({
            type: 'DELETE',
            url: basePath + 'hrm/awards/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function(data) { }
        });
    });

}

/**
 * [Holydays  Delete ]
 */
function holidaysdel(id,dept) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure ! You want to delete <strong>'+dept+' </div>');
    
    $('.modal-footer').on('click', '#delete', function() {
         $('#deletenotification').html("<div class='alert alert-success text-center'><i class='fa fa-check'></i>"  + dept + " Deleted successfully</div>");
       
        $('.holidays_' + id ).remove();
        $.ajax({
            type: 'DELETE',
            url: basePath + 'hrm/holidays/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function(data) { }
        });
    });

}

/**
 * [leaveDel description]

 */
function leaveDel(id,dept) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure ! You want to delete <strong>'+dept+'</strong> <br>' +
        '<br><div class="note note-warning">' +
        "<b>Note</b>: This will delete all the"  + dept + "  associated with it" +
    '</div>');
    
    $('.modal-footer').on('click', '#delete', function() {

        $('#deletenotification').html("<div class='alert alert-success text-center'><i class='fa fa-check'></i>"  + dept + "Deleted successfully</div>");
    
        var notice = $('.leave_' + id ).remove();
        $.ajax({
            type: 'DELETE',
            url: basePath + 'hrm/leavetypes/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
        },
        success: function(data) {

        }
        });
    });
}

/**
 * [noticeDeleted description]
 * @param  {[type]} id   [description]
 * @param  {[type]} dept [description]
 * @return {[type]}      [description]
 */
function noticeDeleted(id,notice) {

    $('#deleteModal').appendTo("body").modal('show');

    $('#info p').html('Are you sure ! You want to delete <strong>'+notice+' ?</div>');

    $('.modal-footer').on('click', '#delete', function() {
        $('#deletenotification').html("<div class='alert alert-success text-center'><i class='fa fa-check'></i>"  + dept + "Deleted successfully</div>");
    
        var notice = $('.notice_' + id ).remove();
        $.ajax({
            type: 'DELETE',
            url: basePath + 'hrm/noticeboards/destroy',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function(data) {
               
               
            }
        });
    });
}


function leaveTypeEdit(id,leaveType,leaveName, num) {

    var url = "leavetypes/"+id;
    url = url.replace(':id',id);
    $('#edit_form').attr('action',url );
    $("#edit_leaveType").val(leaveType);
    $("#edit_leaveName").val(leaveName);
    $("#edit_num_of_leave").val(num);

}




function showempedit(id) {

    if($('#checkbox_'+id+':checked').val() == '1') {
       
        $('#hiddenLeave_'+ id).html(' ');
        $('#leavetype_'+id).hide(1000);
        $('#absentResiont_'+id).hide(1000);

      

    } else {

        $('#hiddenLeave_'+ id).html('<input type="hidden"  name="abscent[]" value="0">');
        $('#leavetype_'+id).show(500);
        $('#absentResiont_'+id).show(1000); 
    }


}
/*
function employeeAssingFeedbackArea(id) {
     var values  = '?id=' + id;
     var regionListAssingemp = $('regionListAssingemp').val();
     var regionListAssingemp = $('regionListAssingemp').val();
    $.ajax({

        type: "Get",
        url: basePath +'hrm/employees/updated/region/list' + values, 
        data: {
            '_token': $('input[name=_token]').val(),
            'id': id
        },
        success: function(result) {

            swal("Good job!", "Congratulations! Your Region and Road has successfully added.", "success");
       
        },  error: function (xhr, status, error) {

           swal("Error!", "Sorry! Can not add new attendance. Please check yourdata quality.", "error");
        }
    });

}*/
function attendentManagement(id){

    if($('#checkbox_'+id+':checked').val() == '1') {

        $('#hiddenLeave_'+ id).html(' ');
        $('#leavetype_'+id).hide(1000);
        $('#absentResiont_'+id).hide(1000);
        $('#timeIn_'+id).show(1000);
        $('#timeOut_'+id).show(1000);
        

        var timeIn  = $('#timeIn_'+id).val();
        var timeOut = $('#timeOut_'+id ).val();

        var values  = '?timeIn=' + timeIn;
            values += '&timeOut=' + timeOut;
            values += '&emp_id=' + id;
            values += '&comments=' + $('#comments_'+id ).val();
            values += '&status=' + $('#checkbox_'+id+':checked').val();
            values += '&date=' + $('#date').val();

        $.ajax({

            type: "POST",
            url: basePath +'hrm/attendances' + values, 
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function(result) {

                // swal("Good job!", "Congratulations! Your attendance has successfully added.", "success");
           
            },  error: function (xhr, status, error) {

               swal("Error!", "Sorry! Can not add new attendance. Please check yourdata quality.", "error");
            }
        });

     } else {

        $('#hiddenLeave_'+ id).html('<input type="hidden"  name="abscent[]" value="0">');
        $('#leavetype_'+id).show(500);
        $('#absentResiont_'+id).show(1000); 
        $('#timeIn_'+id).hide(1000);
        $('#timeOut_'+id).hide(1000);  

        

        var timeIn    = null;
        var timeOut   = $('#timeOut_'+id ).val();
        var leavetype = $('#leavetype_'+id ).val();
        var resion    = $('#absentResiont_'+id ).val();

        var values  = '?timeIn=' + timeIn;
            values += '&timeOut=' + timeOut;
            values += '&leavetype=' + leavetype;
            values += '&resion=' + resion;
            values += '&emp_id=' + id;
            values += '&comments=' + $('#comments_'+id ).val();
            values += '&status=' + 0;
            values += '&date=' + $('#date').val();

        $.ajax({

            type: "POST",
            url: basePath +'hrm/attendances' + values, 
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function(result) {

                // swal("Good job!", "Congratulations! Your attendance has successfully added.", "success");
           
            },  error: function (xhr, status, error) {

               swal("Error!", "Sorry! Can not add new attendance. Please check yourdata quality.", "error");
            }
        });
     }
     

}

$(window).on('load', function () {
 // line disconnection request Calculation
    
    $('.timein,.timeout,.comments_attendances').on('dp.change keyup',function() {
        // alert('hi');
        /* this field id */
        var timeoutID = $(this).attr('id');
        var ID        = timeoutID.split("_");
        var thisID    = ID[1];

        /** Field Update Name */
        var timeIn    = $('#timeIn_'+thisID).val();
        var timeOut   = $('#timeOut_'+thisID).val();
        var comments  = $('#comments_'+thisID).val();
        
        /* time submisstion */
        var values = '?timeIn=' + timeIn;
            values += '&timeOut=' + timeOut;
            values += '&emp_id=' + thisID;
            values += '&date=' + $('#date' ).val();
            values += '&comments=' + comments;
            values += '&status=' + 1;

        $.ajax({

            type: "GET",
            url: basePath +'hrm/excuted/hours' + values, 
            success: function(result) {
                $('#excutedTime_'+thisID).val(result);
            }
        });
        
    });    
}); 

$('#regionListAssingemp').on('click change',function() {   
    var regionID = $(this).val();

    /* time submisstion */
    var values = '?regionID=' + regionID;

    $.ajax({

        type: "GET",
        url: basePath +'hrm/road/list' + values, 
        success: function(result) {
            $('#empassingRoad').html(result);
        }
    });
}); 
$('.leave-type,.reason').on('click keyup',function() {   
    var timeoutID = $(this).attr('id');
    var ID        = timeoutID.split("_");
    var thisID    = ID[1];
    /** Field Update Name */
    var leavetype = $('#leavetype_'+thisID).val();
    var absent    = $('#absentResiont_'+thisID).val();
    var comments  = $('#comments_'+thisID).val();
    
    /* time submisstion */
    var values = '?leavetype=' + leavetype;
        values += '&absent=' + absent;
        values += '&emp_id=' + thisID;
        values += '&date=' + $('#date' ).val();
        values += '&comments=' + comments;
        values += '&status=' + 0;

    $.ajax({

        type: "GET",
        url: basePath +'hrm/excuted/hours' + values, 
        success: function(result) {
            $('#excutedTime_'+thisID).val(result);
        }
    });
}); 
function UpdateDetails(id,type){

    var  form_id = '';
    var alert_div = '';

    if(type=='bank') {

        form_id     = '#bank_details_form';
        alert_div   =  '#alert_bank'

    }  else {

        form_id     = '#company_details_form';
        alert_div   =   '#alert_company';

    }

    $(alert_div).html('<div class="alert alert-info"><span class="fa fa-info"></span> Submitting..</div>');
    var url = "cr";
    url = url.replace(':id',id);
     $.ajax({
             type: "PATCH",
             url : url,
             dataType: 'json',
             data: $(form_id).serialize()

     }).done( function( response ) {
         $(alert_div).html('');
         if(response.status == "success")
         {
               $(alert_div).html('<div class="alert alert-success alert-dismissable"><button class="close" data-close="alert"></button><span class="fa fa-check"></span> '+response.msg+'</div>');

         }else if(response.status == "error")
         {
             var arr = response.msg;
             var alert ='';
             $.each(arr, function(index, value)
             {
                 if (value.length != 0)
                 {
                    alert += '<p><span class="fa fa-warning"></span> '+ value+ '</p>';

                 }
             });

             $(alert_div).append('<div class="alert alert-danger alert-dismissable"><button class="close" data-close="alert"></button> '+alert+'</div>');
         }
     });
}

$(document).ready(function () { 
    /**
     * [Day Search option ]
     * @param  {String} ) {                    var value [description]
     * @return {[type]}   [description]
     */
    $('#daySearch').on('click',function() { 
        var value = '?dateto=' + $("#dateto").val();   

        $.ajax({

            type: "GET",
            url: basePath + 'hrm/attendances/date/search' + value,

        }).success(function ( result ) {

            $('#attendanceResult').html( result );

        }).error(function ( result ) {
            console.log('Information was not found.');
        });
    });

    /**
     * [Mark attendace Search]
     */
    $('#markAttendanceSearch').on('click',function() { 

        var value = '?dateto=' + $("#dateto").val();  
        $('#loaderImages').append('<div class="loader"><img src="'+basePath+'assets/images/load.gif" alt="Searching......" /></div>');
        $.ajax({

            type: "GET",
            url: basePath + 'hrm/mark/attendances/date/search' + value,

        }).success(function ( result ) {
            $('#loader').slideUp(200,function() {       
                $('#loader').remove();
            });

            $(".loader").fadeOut("slow"); 
            $('#attendanceListResult').html( result );

        }).error(function ( result ) {
            console.log('Information was not found.');
        });
    });

    /**
     * [Mark attendace Search]
     */
    $('#leaveName').on('change',function() { 


        var value = '?leavename=' + $("#leaveName").val() + '&employee=' + $("#employee").val();   

        $.ajax({

            type: "GET",
            url: basePath + 'hrm/leave/available/search' + value,

        }).success(function ( result ) {

            $('#availableleave').val( result );

        }).error(function ( result ) {
            console.log('Information was not found.');
        });
    });

    $('#requestingday').on('keyup',function() { 


        var requestingday = $("#requestingday").val();   
        
        if(requestingday >= 2) {

            $('#singleday').hide();
            $('#moredays').show();

        } else {
            $('#singleday').show();
            $('#moredays').hide();
        }
       
    });
    /**
     * [Monthly attendace Search]
     */
    $('#monthlySearchAttendance').on('click',function() { 

        var value = '?dateto=' + $("#dateto").val();   
            value += '&dateform=' + $("#dateform").val();   
            value += '&employee=' + $("#employee").val();   

        $.ajax({

            type: "GET",
            url: basePath + 'hrm/monthly/attendance/search' + value,

        }).success(function ( result ) {

            $('#monthlyattendanceResult').html( result );

        }).error(function ( result ) {
            console.log('Information was not found.');
        });
    });

    /**
     * [Monthly attendace Search]
     */
    $('#onLeaveReports').on('click',function() { 

        var value = '?dateto=' + $("#dateto").val();  

        $.ajax({

            type: "GET",
            url: basePath + 'hrm/onleave/reports/search' + value,

        }).success(function ( result ) {

            $('#onLeaveResult').html( result );

        }).error(function ( result ) {
            console.log('Information was not found.');
        });
    });

    /**
     * [Monthly attendace Search]
     */
    $('#timeSheetApproval').on('click',function() { 
        
        var value = '?dateto=' + $("#dateto").val();   
            value += '&dateform=' + $("#dateform").val();   
            value += '&employee=' + $("#employee").val();   

        $.ajax({

            type: "GET",
            url: basePath + 'hrm/employee/timesheet/approval/search' + value,

        }).success(function ( result ) {

            $('#timesheetResult').html( result );

        }).error(function ( result ) {
            console.log('Information was not found.');
        });
    });

});