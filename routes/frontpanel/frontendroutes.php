<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

# login managemnt


# Default login page
Route::get( '/', 'Frontpanel\Login\userLoginController@index');
Route::get('login', ['as' => 'login', 'uses' => 'Frontpanel\Login\userLoginController@index']);

// DHIS2 Login
Route::post('dhis2-login','Frontpanel\Login\userLoginController@dhis2Login');





# Client management 
Route::get( '/client/logout', 'Client\ClientManagement@clientLogOut');
Route::post('/client/login/request', 'Client\ClientManagement@clientLoginReqeust');
Route::get( '/client/dashboard', 'Client\ClientManagement@clientDashboard');
Route::post( '/client/complain', 'Client\ClientManagement@clientComplain');
Route::post( '/client/packagechange/request', 'Client\ClientManagement@clientPackageChange');
Route::post( '/client/housechange/request', 'Client\ClientManagement@clientHouseChangeRequest');
Route::post( '/client/disconnect/request', 'Client\ClientManagement@clientDisconnectChangeRequest');
Route::get( '/client/bill/pay', 'Client\ClientManagement@clientBillPayRequest');
Route::get( '/client/bill/pay/bkash', 'Client\ClientManagement@clientBillPayBkash');
Route::get( '/client/road/info/{find_info}', 'Frontpanel\Newline\NewlineController@getEmployeeRegionRoads');
Route::get( '/cach', 'Frontpanel\Newline\NewlineController@cachPanel');
// Route::get( '/get/road/info-search/{find_info}', 'Frontpanel\Newline\NewlineController@getEmployeeRegionRoads' );

# My profile login
Route::get( '/my/profile', 'Client\ClientManagement@clientLogin');

$router->group( ['middleware' => 'auth'] , function($router) {


Route::get('auth/logout', 'Frontpanel\Login\userLoginController@logout');
Route::post( '/check-login', 'Frontpanel\Login\userLoginController@checkLogin');


# customar care dashboard
Route::get( '/customercare-dashboard', 'Frontpanel\CustomerCareDashboard@customerCareDashboard');
Route::get( '/newline/request/page/load', 'Frontpanel\CustomerCareDashboard@newlineRequestPageLoad');
Route::get( '/update-new-line-notes', 'Frontpanel\Newline\NewlineController@updateNewlineNotes');
Route::get( '/new/line/notes/list', 'Frontpanel\Newline\NewlineController@newlineNotesList');
Route::get( '/newlineline/referance/update', 'Frontpanel\Newline\NewlineController@newlinelineReferanceUpdate');
Route::get( '/master-line-connections', 'Frontpanel\Newline\NewlineController@masterLineConnections' );
Route::get( '/master-line-connections/{id}', 'Frontpanel\Newline\NewlineController@masterLineConnectionsRegion' );
Route::get( '/master/line/reconnections', 'Frontpanel\Newline\NewlineController@masterLineReConnections' );
Route::get( '/today/newline/connection', 'Frontpanel\Newline\NewlineController@todayNewlineconnection' );
Route::get( '/user/newline/request/accept/date/search', 'Frontpanel\Newline\NewlineController@newlineRequestDateSearch' );
Route::get( '/yesterday/newline/connection', 'Frontpanel\Newline\NewlineController@yesterdayLineconnection' );
Route::get( '/current/month/reconnections/{date}', 'Frontpanel\Newline\NewlineController@todaysReconnection' );
Route::get( '/yesterday-reconnection', 'Frontpanel\Newline\NewlineController@yesterdaysReconnection' );
Route::get( '/mikrotik/user/list', 'Mikrotik\MikrotikController@mikrotikList' );
Route::get( '/mikroik/user/on/off', 'Frontpanel\Newline\NewlineController@mikrtikUserOnOff' );

# Newline Request  management
Route::get( '/newline-phone-request/{date}', 'Frontpanel\Newline\NewlineController@newlinePhoneRequest' );
Route::get( '/all-newline-request/{date}', 'Frontpanel\Newline\NewlineController@newlinePhoneRequest' );
Route::get( '/newline/request/assign', 'Frontpanel\Newline\NewlineController@newlineAssingTechnican' );
Route::post( '/newline-phonereq-accept', 'Frontpanel\Newline\NewlineController@newlinePhoneAccept' );
Route::get( '/edit/newline/phonereq/info/{id}', 'Frontpanel\Newline\NewlineController@editNewlinePhoneRequest' );
Route::post( '/update/newline/phonereq/info', 'Frontpanel\Newline\NewlineController@updateNewlinePhoneRequest' );
Route::get( '/newline-phonereq-reject/{id}', 'Frontpanel\Newline\NewlineController@newlinPhoneReject' );
Route::get( '/newline/phone/request/date/search', 'Frontpanel\Newline\NewlineController@newlinePhoneRequestDate');
Route::get( '/newline/phone/request/year/search', 'Frontpanel\Newline\NewlineController@newlineRequestYearSearch');
Route::get( '/new-line-request', 'Linemgt\LinemanagementController@index' );
Route::get( '/newline/request', 'Frontpanel\Newline\NewlineController@newlineRequest' );

# Newline rejected request management Route
Route::get( '/all-rejected-nl-request', 'Frontpanel\Newline\NewlineController@allNewlineRejectedRequest' );
Route::get( '/destroy/newline/phonereq/info/{id}', 'Frontpanel\Newline\NewlineController@destroyNewlinePhoneRequest' );
Route::get( '/newlineline-reentry/{id}', 'Frontpanel\Newline\NewlineController@newlineReentrybyAjax' );
Route::get( '/newline/rejected/request/search', 'Frontpanel\Newline\NewlineController@newlineRejectedRequestDate');

# Newline Management
Route::get('/current/month/active/connections/{date}', 'Frontpanel\Newline\NewlineController@currentMonthConnection' );
Route::get('/current/month/active/connections/search', 'Frontpanel\Newline\NewlineController@frontNewLineRequestSearch' );
Route::get('/current/month/active/connections/date/search', 'Frontpanel\Newline\NewlineController@frontNewLineRequestaDateSearch' );
Route::get('/master/newline/connections/date/search', 'Frontpanel\Newline\NewlineController@masterNewlineConnectionDateSearch' );
Route::get('/master/newline/connections/status/search', 'Frontpanel\Newline\NewlineController@masterNewlineConnectionStatusSearch' );
Route::get('/master/newline/connections/type/search', 'Frontpanel\Newline\NewlineController@masterNewlineConnectionTypeSearch' );
Route::get( '/frontend-edit-new-line-request/{id}', 'Frontpanel\Newline\NewlineController@fronNewLineRequestEdit' );
Route::get( '/client-info-update/{id}', 'Frontpanel\Newline\NewlineController@fronNewLineRequestEdit' );
Route::get('/newline-collections', 'Frontpanel\Newline\NewlineController@currentMonthCollection');
Route::get('/connectionfee/change/update', 'Frontpanel\Newline\NewlineController@connectionFeeChangeUpdate');
Route::get('/connectionfee/collection/update', 'Frontpanel\Newline\NewlineController@connectionFeecollectionUpdate');
Route::get('/newline/collection/date-search', 'Frontpanel\Newline\NewlineController@currentMonthCollectionDateSearch');
Route::get('/alertbox', 'Pusher\ComplainLiveNotify@alertBox');
Route::get('/fireevent', 'Pusher\ComplainLiveNotify@fireEvent');

// Account Module route
Route::get('/accounting', 'Accounting\AccountingController@accountingDashboard' );
Route::resource('/accounting/income', 'Accounting\IncomeController' );

// Expense 
Route::resource('/accounting/expense', 'Accounting\ExpenseController' );
Route::post('/accounting/expenses/delete', 'Accounting\ExpenseController@expensesDelete' );

// Accounts Item 
Route::resource('/accounting/item', 'Accounting\ITEMController' );
Route::post('/accounting/item/delete', 'Accounting\ITEMController@itemDelete' );

// Accounts transection 
Route::resource('/accounting/transectiontype', 'Accounting\TransectionTypeController' );
Route::post('/accounting/transectiontype/delete', 'Accounting\TransectionTypeController@transectionTypeDelete' );

// Account Transection Mode  Declared
Route::resource('/accounting/transection/mode', 'Accounting\TransectionModeController' );
Route::post('/accounting/transection/mode/delete', 'Accounting\TransectionModeController@transectionModeDelete' );

// Account Tax Declared
Route::resource('/accounting/tax', 'Accounting\TaxController' );
Route::post('/accounting/tax/delete', 'Accounting\TaxController@taxDelete' );

// Account Route Declared
Route::resource('/accounting/accounts', 'Accounting\AccountsController' );
Route::post('/accounting/acounts/delete', 'Accounting\AccountsController@AccountsDelete' );

// Account Route Declared
Route::resource('/accounting/recuring/transection', 'Accounting\RecurringTransactionController' );
Route::post('/accounting/recuring/transection/delete', 'Accounting\RecurringTransactionController@recuringTransectionDelete' );



# Front panel report newline collection
Route::get( '/master-newline-collections', 'Frontpanel\Newline\NewlineController@newlineBillCollections');
Route::get( '/master-newline-collections/date-search', 'Frontpanel\Newline\NewlineController@newlineBillCollectionsDateSearch');
Route::get( '/master/cc/collections', 'Frontpanel\Newline\NewlineController@masterccCollection');
Route::get( '/master/cc/collector/collection', 'Frontpanel\Newline\NewlineController@masterccCollectorCollection');
Route::get( '/daily/cc/collections/summary', 'Frontpanel\Newline\NewlineController@dailyccCollectionsSummery');
Route::get( '/daily/cc/collection/summery/search', 'Frontpanel\Newline\NewlineController@dailyccCollectionsSummerySearch');
Route::get( '/yearly/all/collections/summary', 'Frontpanel\Newline\NewlineController@yearlyallCollectionsSummery');

# Housechange Management
Route::get( '/house-change-requst-livesearch/{find_info}', 'Frontpanel\Newline\NewlineController@houseChangeClientInfo');
Route::get( '/add-housechange-req/{find_info}', 'Linemgt\LinemanagementController@addNewHouseRequestInfo');
Route::get( '/house-change-request-live', 'Frontpanel\Newline\NewlineController@houseChangeRequest' );
Route::get( '/house-change-request/{date}', 'Linemgt\LinemanagementController@houseChangeRequestAssignAccept' );
Route::get( '/houseChange/client/request/search', 'Linemgt\LinemanagementController@houseChangeRequestSearch' );
Route::post( '/houseChange/request/assign', 'Linemgt\LinemanagementController@houseChangeRequestAssign' );
Route::get( '/new/house/request/accept/{id}', 'Linemgt\LinemanagementController@newHouseAccept' );
Route::get( '/new/house/change/request-info/edit/{id}', 'Linemgt\LinemanagementController@editHouseChangeRequestInfo' );
Route::post( '/update/house/change-request/info', 'Frontpanel\Newline\NewlineController@updateHouseChangeRequestInfo' );
Route::get( '/new/house/request/reject/{id}', 'Linemgt\LinemanagementController@newHouseReject' );
Route::get( '/houseshift/charge/allow', 'Frontpanel\Newline\NewlineController@newlineChargeAllow' );

# House change Master Reports
Route::get( '/master-houseshift-report', 'Report\HouseShift\HouseShiftController@masterHouseShift' );
Route::get( '/house-change/search-region', 'Report\HouseShift\HouseShiftController@houseShiftRegionsearchData' );
Route::get( '/house/shift/keyword-search/{find_info}', 'Report\HouseShift\HouseShiftController@houseShiftKeywordsearchData' );
Route::get( '/house-change/search-date', 'Report\HouseShift\HouseShiftController@houseShiftDatesSarchData' );
Route::get( '/house/shift/changed-technican-search/{find_info}', 'Report\HouseShift\HouseShiftController@houseShiftChangedTechnicianhData' );
Route::get( '/house-shift/months-based-search/{find_info}', 'Report\HouseShift\HouseShiftController@houseShiftMonthbasedSearch' );

# Packagechange Management
Route::get( '/package-change-livesearch/{find_info}', 'Frontpanel\Newline\NewlineController@pkgChangeClientInfo');
Route::get( '/add-packagechange-req/{find_info}', 'Frontpanel\Newline\NewlineController@addPackageChangeClientInfo');
Route::get( '/package/calculation/day/monthly', 'Frontpanel\Newline\NewlineController@packageCalculationDayMonthly');
Route::get( '/packagechange/corporate/client', 'Frontpanel\Newline\NewlineController@packageChangeCorporatClient');
Route::get( '/package-change-request-live', 'Frontpanel\Newline\NewlineController@packageChangeRequest' );
Route::get( '/package-change-request/{date}', 'Linemgt\LinemanagementController@packageChangeRequest' );
Route::get( '/packageChange/client/request/date', 'Linemgt\LinemanagementController@packageChangeRequestSearch' );
Route::get( '/new/package/request/accept/{id}', 'Frontpanel\Bill\BillController@newPackageAccept' );
Route::get( '/package/change/mikrotik', 'Frontpanel\Bill\BillController@packageChangeMikrotik' );
Route::get( '/inactive/client/in/mikrotik', 'Linemgt\LinemanagementController@inactiveClientsInMikrotik' );
Route::get( '/permannent/inactive/in/mikrotik', 'Linemgt\LinemanagementController@permannentinactiveClientsInMikrotik' );
Route::get( '/new/package/request/edit/{id}', 'Frontpanel\Newline\NewlineController@packageChangeRequestEdit' );
Route::post( '/update-package-change', 'Frontpanel\Newline\NewlineController@updatePackageChangeRequest' );
Route::get( '/realip/search', 'Frontpanel\Newline\NewlineController@pkgChangeRealIpCharge');
Route::get( '/package/realip/setup', 'Frontpanel\Newline\NewlineController@packageRealIpSetup');
Route::get( '/base/search', 'Frontpanel\Newline\NewlineController@hoseshiftBaseSearch');
Route::get( '/package/setup/charge', 'Frontpanel\Newline\NewlineController@packageSetupCharge');
Route::get( '/new/package/request/reject/{id}', 'Linemgt\LinemanagementController@newPackageReject' );
Route::get( '/master-packagechange-report', 'Report\PackChange\PackChangeController@masterPackageChange' );
Route::get( '/package/changed/keyword-search/{find_info}', 'Report\PackChange\PackChangeController@packageChangekeywordsearchData' );
Route::get( '/package-change/search-package', 'Report\PackChange\PackChangeController@packageChangesearchData' );
Route::get( '/package-change/search-date', 'Report\PackChange\PackChangeController@packageChangeSearchDateRangeData' );
Route::get( '/package/changed-technican-search/{find_info}', 'Report\PackChange\PackChangeController@packageChangeTechniciandsearchData' );


# Client Complian management
Route::get( '/client-complain-livesearch/{find_info}', 'Frontpanel\Complain\ComplainController@clientComplainSearch');
Route::get( '/create-client-complain/{find_info}', 'Frontpanel\Complain\ComplainController@index');
Route::get( '/add-complain', 'Frontpanel\Complain\ComplainController@addComplain' );
Route::get( '/todays/and/pending/complains', 'Frontpanel\Complain\ComplainController@todaysComplains' );
Route::get( '/my/complain', 'Frontpanel\Complain\ComplainController@empWiseComplain' );
Route::get( '/complain/notes', 'Frontpanel\Complain\ComplainController@complainNotes' );

Route::match(['get', 'post'], '/enggineer/wise/complain/summary', 'Frontpanel\Complain\ComplainController@enggineerWiseComplainSummery' );
Route::get( '/enggineer/wise/complain/summary/{id}', 'Frontpanel\Complain\ComplainController@enggineerWiseComplainSummerySummery' );
Route::get( '/view-client-complain', 'Frontpanel\Complain\ComplainController@viewComplain');
Route::get( '/edit-client-complain-request/{find_info}', 'Frontpanel\Complain\ComplainController@editClientComplain' );
Route::get( '/search-clcomplain-info/{find_info}', 'Frontpanel\Complain\ComplainController@show' );
Route::get( '/complain-edit-phonenumber', 'Frontpanel\Complain\ComplainController@todaysComplainMobileLiveEdit');
Route::get( '/ccomplain-search/{find_info}', 'Frontpanel\Complain\ComplainController@clientSearchByID');
Route::get( '/complain/live-search/{find_info}', 'Frontpanel\Complain\ComplainController@complainSearch');
Route::post( '/update-client-complain', 'Frontpanel\Complain\ComplainController@updateClientComplain');
Route::get( '/view-complain/search-date', 'Frontpanel\Complain\ComplainController@complainDateSearchInfo');
Route::POST( '/view-complain/search-date-region', 'Frontpanel\Complain\ComplainController@complainDateSearchRigon');
Route::get( '/sclient-complain-username/{find_info}', 'Frontpanel\Complain\ComplainController@findComplainThrough');
Route::get( '/client/complain/technican', 'Frontpanel\Complain\ComplainController@findempComplainTechnican');
Route::get( '/sclient-complain-keyword/{find_info}', 'Frontpanel\Complain\ComplainController@findComplainThroughByKeyword');
Route::get( '/sclient-complain-area/{find_info}', 'Frontpanel\Complain\ComplainController@findCompalinByArea');
Route::get( '/sclient-complain-comptype/{find_info}', 'Frontpanel\Complain\ComplainController@searchCompType');
Route::get( '/line-disconnect-search-technician/{find_info}', 'Frontpanel\Complain\ComplainController@LineDisconnectSearchtechnician');
Route::get( '/insert-start', 'Frontpanel\Newline\NewlineController@dummydataupload' );
Route::post( '/complain-status-changed', 'Frontpanel\Complain\ComplainController@updateComplainStatus');
Route::get( '/table-edit-ajax', 'Frontpanel\Complain\ComplainController@editFeedbackAjaxUpdate');
Route::get( '/status-changed-ajax', 'Frontpanel\Complain\ComplainController@statusChangedByAjax');
Route::get( '/find-corporte-customer-type', 'Frontpanel\DashboardController@corporateCustomerType' );
Route::get( '/changed-contactperson-live', 'Frontpanel\Complain\ComplainController@contactPersonChangedByAjax');
Route::get( '/changed-priority-live', 'Frontpanel\Complain\ComplainController@complainPriorityChangedByAjax');
Route::get( '/destroy-complain/{id}', 'Cmpmgt\ComplainController@destroy' );
Route::get( '/edit-client-complain/{id}', 'Cmpmgt\ComplainController@edit' );
Route::post( '/update-client-complain-information', 'Cmpmgt\ComplainController@update' );
Route::get( '/pending/complain/{date}', 'Frontpanel\Complain\ComplainController@pendingComplainBox' );
Route::get( '/update/complain/notes', 'Frontpanel\Complain\ComplainController@updateComplainNotes' );

# Line disconnect al Route
Route::get( '/line-disconnect-livesearch/{find_info}', 'Frontpanel\Lindis\LinedisController@lineDisSearch');
Route::get( '/line-disconnect-editinfo/{find_info}', 'Frontpanel\Lindis\LinedisController@lineDisconnect');
Route::get( '/addline-disconnect','Frontpanel\Lindis\LinedisController@addlinedisconnect' );
Route::get( '/line/disconnect/request/date/search', 'Frontpanel\Lindis\LinedisController@lineDisconnectDateSearch' );
Route::match(['get', 'post'],'/line-disconnect-request/{date}', 'Linemgt\LinemanagementController@lineDisconnect' );
Route::get( '/client-reconnect-info/{find}', 'Frontpanel\Lindis\LinedisController@clientReconnectInfo');
Route::get( '/client/reconnect/information', 'Frontpanel\Lindis\LinedisController@clientReconnectInformation');
Route::get( '/client/reconnect/bill/generate', 'Frontpanel\Lindis\LinedisController@clientReconnectBillGenerate');
Route::get( '/client/unblock/information', 'Frontpanel\Lindis\LinedisController@clientUnblockInformation');
Route::post( '/update-client-reconnect-info', 'Frontpanel\Lindis\LinedisController@updateClientReconnectInfo' );
Route::get( '/frontend-line-reconnect-info/{find}', 'Frontpanel\Lindis\LinedisController@frontendClientReconnectInfo' );
Route::get( '/disconnect/request/accept/{id}', 'Linemgt\LinemanagementController@disconnectAccept' );
Route::get( '/disconnect/request/reject/{id}', 'Linemgt\LinemanagementController@disconnectReject' );
Route::get( '/disconnect/request/reject/{id}', 'Linemgt\LinemanagementController@disconnectReject' );
Route::get( '/edit-line-disconnect-request/{id}', 'Linemgt\LinemanagementController@linedisconnectedit' );
Route::post( '/line/disconnect/notes/update/', 'Linemgt\LinemanagementController@lineDisconnectNotesUpdate' );

# Search Client History
Route::get( '/search-client-history-details', 'Frontpanel\DashboardController@clienthistoryInformation');
Route::get( '/search/client/history/basic/details', 'Frontpanel\DashboardController@clienthistoryInformationBasic');

# Client Bill Collector
Route::get( '/user-bill-collection-by-collector', 'Frontpanel\Bill\BillController@billcollectByCollector' );
Route::get( '/user-bill-collection-by-collector/{id}', 'Frontpanel\Bill\BillController@billcollectByCollectorSubmit' );
Route::get( '/defualter/bill/monthly/billgenerate', 'Frontpanel\Bill\BillController@defualterMonthlyBillGenerated' );
Route::post( '/defualter/rebillgenerate/monthly', 'Frontpanel\Bill\BillController@defualterMonthlyBillUpdate' );
Route::get( '/export/due/bill/collection/by-collector', 'Report\Csv\ExportcsvController@exportDueBillCollectorData'  );
Route::get( '/export/bill/collection/by-collector', 'Report\Csv\ExportcsvController@exportBillCollectorData'  );
Route::get( '/export/advance/bill/collection/by-collector', 'Report\Csv\ExportcsvController@exportadvancedBillCollectorData'  );
Route::get( '/user-due-bill-collection-by-collector', 'Frontpanel\Bill\BillController@dueBillcollectByCollector' );
Route::get( '/user-due-bill-collection-by-collector/{newconid}', 'Frontpanel\Bill\BillController@dueBillcollectByCollectorSingle' );
Route::resource( '/due/bill/collection', 'Frontpanel\Bill\DueCollectionController' );
Route::post( '/due/bill/collections/delete', 'Frontpanel\Bill\DueCollectionController@dueBillCollectionDelete' );
Route::post( '/due/bill/collection/collector/submit', 'Frontpanel\Bill\DueCollectionController@duePaidCollectionConfirm' );
Route::get( '/client/bill/collection/info/search', 'Frontpanel\Bill\BillController@billCollectionInfo');
Route::get( '/newline-recollection/{find_info}', 'Frontpanel\Newline\NewlineController@newlineRecollection');
Route::get( '/client/bill/collection/info/search/address/wise', 'Frontpanel\Bill\BillController@billCollectionInfoByAddress');
Route::get( '/client/due/bill/collection/info/search', 'Frontpanel\Bill\BillController@dueBillCollectionInfo');
Route::get( '/client/update/collected/bill/{billno}', 'Frontpanel\Bill\BillController@updateBillCollectionInfo' );
Route::get( '/details/billing/report/{billno}', 'Billing\BillReportController@billingDetails' );
Route::get( '/employee-profile-data', 'Frontpanel\DashboardController@employeeProfileData');

# support route
Route::get( '/support', 'Frontpanel\Support\SupportController@support' );
Route::get( '/support/view', 'Frontpanel\Support\SupportController@SupportView' );
Route::post( '/support/request', 'Frontpanel\Support\SupportController@supportRequest' );
Route::get( '/support/check', 'Frontpanel\Support\SupportController@billSupportCheck' );
Route::get( '/support/edit/{newconid}', 'Frontpanel\Support\SupportController@supportEditRequest' );
Route::post( '/support/update/{id}', 'Frontpanel\Support\SupportController@supportUpdate');
Route::get( '/support/destroy/{newconid}', 'Frontpanel\Support\SupportController@billSupportDestroy');



Route::get( '/client/bill/collection/info/advanced/search', 'Frontpanel\Bill\advancePaidController@billCollectionAdvancedInfo');
Route::resource( '/advance/paid/collection', 'Frontpanel\Bill\advancePaidController');
Route::post( '/confirm/advance/collection', 'Frontpanel\Bill\advancePaidController@confirmAdvanceCollection');
Route::post( '/advance/paid/collection/delete', 'Frontpanel\Bill\advancePaidController@deleteAdvance');
Route::PUT( '/advance/paid/collection/update/{id}', 'Frontpanel\Bill\advancePaidController@updateAdvanceBill');

# Front panel Newline Setup
Route::get( '/client/newline/request', 'Client\ClientManagement@clientNewlineManagement');
Route::get( '/client/newline/create', 'Client\ClientManagement@clientNewlineCreate');
Route::post( '/new-linesetup', 'Frontpanel\Newline\NewlineController@create' );
Route::post('/add-employee','Frontpanel\Employee\EmployeeController@create');

# Line disconnect Billing info
Route::get( '/disconnection/request/billing/info', 'Frontpanel\Newline\NewlineController@disconClientBillInfo');
Route::post( '/add-image', 'Frontpanel\subadmin\SubadminController@store' );
Route::get( '/search-empinfo-technician/{emp_id}', 'Frontpanel\Employee\EmployeeController@showEmployeeInfo');
Route::get( '/search-bgbill-info/{find_info}', 'Frontpanel\Bill\BillController@showBillInfo' );
Route::get( '/search-client-info/{find_info}', 'Frontpanel\Bill\BillController@showClientInfo' );
Route::get( '/get/road/info-search/{find_info}', 'Frontpanel\Newline\NewlineController@getEmployeeRegionRoads' );
Route::match(['get', 'post'], '/update-new-line-request', 'Linemgt\LinemanagementController@updateNewLineRequest' );
Route::match(['get', 'post'], '/enable-shorttime-desconnection-in-mikrotik', 'Linemgt\LinemanagementController@enableShortTimeInMikrotik' );
Route::post( '/update-new-line-due-request', 'Linemgt\LinemanagementController@updateNewLineDueRequest' );
Route::post( '/packagechange/request/assign', 'Frontpanel\Newline\NewlineController@packageChangeRequestAssign' );

/*--------------------------------------------------------
 *              Line Management                          *
 *-------------------------------------------------------*/
Route::get( '/edit-new-line-request/{id}', 'Linemgt\LinemanagementController@edit' );
Route::get( '/new-line-request-details/{id}', 'Linemgt\LinemanagementController@newlineDetails' );
Route::get( '/destroy-new-line-request/{id}', 'Linemgt\LinemanagementController@destroy' );
Route::get( '/destroy-line-disconnect-request/{id}', 'Linemgt\LinemanagementController@linedisconnectDestroy' );
Route::get( '/area-wise-connections', 'Linemgt\LinemanagementController@areaWiseConnectionsReports' );
Route::get( '/road-wise-connections', 'Linemgt\LinemanagementController@roadWiseConnectionsReports' );
Route::get( '/newline/phone/request/accept-reject/search/{find_info}', 'Linemgt\LinemanagementController@newlinePhoneRequesAcceptReject' );

Route::get( '/newline/request/accept/{id}', 'Linemgt\LinemanagementController@newlineAccept' );
Route::get( '/newline/request/reject/{id}', 'Linemgt\LinemanagementController@newlineReject' );

Route::get( '/newline/request/accept-reject/list/{id}', 'Linemgt\LinemanagementController@newlineAcceptRejectList' );
Route::get( '/cable/status/plug/unplug', 'Frontpanel\Newline\NewlineController@cableStatusPlugUnplug');
Route::post('today/newline/bill/collection/submit', 'Frontpanel\Newline\NewlineController@todayBillCollectionSubmit');

// Ajax live search 
Route::get( '/search-live-new-line-request/{find_info}', 'Linemgt\LinemanagementController@show' );
Route::get( '/master/linedisconnect-technican-search/{find_info}', 'Linemgt\LinemanagementController@searhTechnicinaLineDisconnect' );
Route::get( '/newline/request/info/search/{find_info}', 'Linemgt\LinemanagementController@newlineRequestSearch' );


Route::get( '/disconnect/request/accept-reject/list/{id}', 'Linemgt\LinemanagementController@disconnectAcceptRejectList' );
Route::post( '/select-technician-accept-linedisconnect', 'Frontpanel\Lindis\LinedisController@lineDissconnectTechnicianAssign' );
Route::get( '/client-houstshift-history/{find_info}', 'Frontpanel\DashboardController@clientHouseShiftHistory');
Route::get( '/client-instrument-history/{find_info}', 'Frontpanel\DashboardController@clientInstrumentHistory');
Route::get( '/client-complains-history/{find_info}', 'Frontpanel\DashboardController@clientComplainsHistory');
Route::get( '/pay/bill/redirect', 'Frontpanel\DashboardController@payBillRedirect');
Route::get( '/package/select/list', 'Frontpanel\DashboardController@packageSelectList');
Route::get( '/client/complain/history', 'Report\ClientProfile\ClientProfileDetails@clientComplainHistory');
Route::get( '/client/packagechange/history', 'Report\ClientProfile\ClientProfileDetails@clientPackagechangeHistory');
Route::get( '/client/housechange/history', 'Report\ClientProfile\ClientProfileDetails@clientHouseChangeHistory');
Route::get( '/client/disconnection/history', 'Report\ClientProfile\ClientProfileDetails@clientDisconnectionHistory');
Route::get( '/client/billpayment/history', 'Report\ClientProfile\ClientProfileDetails@clientBillpaymentHistory');
Route::get( '/client/reconnection/history', 'Report\ClientProfile\ClientProfileDetails@clientReconnectionHistory');


// line disconnect
Route::post( '/update-client-line-disconnect-request', 'Linemgt\LinemanagementController@updatelineDisconnect' );
Route::post('/update-reject-button/{disconid}', 'Linemgt\LinemanagementController@updateRejectButton' );
Route::get( '/inventory-analysis', 'Linemgt\LinemanagementController@inventoryAnalysis' );
//Route::get( '/newline/phone/request/live/search/{find_info}', 'Frontpanel\Newline\NewlineController@newlinePhoneRequestLiveSearch' );
Route::get( '/newline/phone/request/live/search/technician/{find_info}', 'Frontpanel\Newline\NewlineController@newlinePhoneRequestTechnicianLiveSearch' );
/* Dashboard Box Reports*/

/******************** Today's Re Connection **********************************/

Route::get( '/newline/requst/liveSearch/{find_info}', 'Frontpanel\Newline\NewlineController@newlineRequestClientInfo');



/* Bill Collection client information live search information*/

//Route::get( '/corporate/client/bill/collection/info/search/{find_info}', 'Frontpanel\Bill\BillController@billCollectionCorporateClientInfo');
Route::get( '/client/collected/bill/delete/{billno}', 'Frontpanel\Bill\BillController@destroyBillCollectionInfo' );
Route::get( '/edit/client/bill/{billno}', 'Frontpanel\Bill\BillController@editClientBillInfo' );
Route::post( '/update/client/bill', 'Frontpanel\Bill\BillController@updateClientBillInfo' );
Route::post( '/client/bill/collection/confirm', 'Frontpanel\Bill\BillController@updateClientBillCollectionInfo' );
Route::post( '/user/bill/collected/by/collector', 'Frontpanel\Bill\BillController@billCollectionByBillcollector' );
Route::post( '/user/bill/collected/by/collector/submit', 'Frontpanel\Bill\BillController@billCollectionByBillcollectorConfirm' );
Route::post( '/user/due/bill/collected/by/collector', 'Frontpanel\Bill\BillController@duebillCollectionByBillcollector' );
Route::post( '/user/advanced/bill/collected/by/collector', 'Frontpanel\Bill\BillController@advancedbillCollectionByBillcollector' );
Route::post( '/line-disconnect/search-date', 'Linemgt\LinemanagementController@searchDateLineDisconnect' );


/*--------------------------------------------------------
 *              Client Bill Management                   *
 *-------------------------------------------------------*/
Route::get( '/line-disconnect/{find_info}', 'Frontpanel\Newline\NewlineController@ajaxshow');
Route::get('/search-newemployee/{find_info}','Frontpanel\Employee\EmployeeController@show');
Route::get('/search-all-employee-info/{find_info}','Frontpanel\Complain\ComplainController@ajaxClientSearch');

Route::get( '/register-data', 'Frontpanel\Login\userLoginController@create');
Route::post( '/add-register', 'Frontpanel\Login\userLoginController@store');

 /*For Package create*/
Route::get( '/search-package/{packid}', 'Package\PackageController@ajaxPackage');
Route::get( '/technician-search/{find_info}', 'Frontpanel\Newline\NewlineController@technicianshow');

Route::get( '/clbgname-search/{find_info}', 'Frontpanel\Bill\BillController@clShow');
Route::get( '/lindisname-search/{find_info}', 'Frontpanel\Lindis\LinedisController@show');
Route::get( '/client-profile-details/{newconid}', 'Report\ClientProfile\ClientProfileDetails@clientProfileDetails' );


# Newline Exprt management
Route::get( '/export/all-newlinephone-request', 'Report\GlobalExport\GlobalExportController@allNewlinePhoneRequestExportData' );
Route::get( '/export/all-newlinerejected-request', 'Report\GlobalExport\GlobalExportController@allNewlineRejectedRequestExportData' );
Route::get( '/export/all-newline-connection', 'Report\GlobalExport\GlobalExportController@expoerAllNewlineConnection' );
Route::get( '/export/today-newline-bill-collection', 'Report\GlobalExport\GlobalExportController@downloadCurmonthNewlineCollection' );
Route::get( '/export/today-newline-bill-collection-confirm', 'Report\GlobalExport\GlobalExportController@todayNewlineBillCollectionConfirm' );
Route::get( '/export/master/newline/collection', 'Report\GlobalExport\GlobalExportController@masterNewlineCollection' );
Route::get( '/export/master/connections', 'Report\GlobalExport\GlobalExportController@exportMasterConnection' );

# House Change export
Route::get( '/export/all-house-change-request', 'Report\GlobalExport\GlobalExportController@allHouseChangeExportData' );
Route::get( '/export-master-house-shift', 'Report\GlobalExport\GlobalExportController@allMasterHouseShiftBoxData' );

# Package Change export
Route::get( '/export/all-package-change-request', 'Report\GlobalExport\GlobalExportController@allPackageChangeExportData' );
Route::get( '/export-master-packagechanged', 'Report\GlobalExport\GlobalExportController@allMasterPackagechangedData' );

# Al Complain export
Route::get( '/export/all-client-complains', 'Report\GlobalExport\GlobalExportController@allComplainsExportData' );
Route::get( '/export/complain/setting', 'Report\GlobalExport\GlobalExportController@exportComplainSetting' );
Route::get( '/export/all-todays-allcomplains', 'Report\GlobalExport\GlobalExportController@todayAllComplainBoxData' );

#line disconnect export request
Route::get( '/export/all-client-disconnect', 'Report\GlobalExport\GlobalExportController@allDisconnectRequestExportData' );
Route::get( '/export/all-collection-boxresult', 'Report\GlobalExport\GlobalExportController@allCollectionBoxData' );
Route::get( '/export/all-todays-newlineconnections', 'Report\GlobalExport\GlobalExportController@todayNewLineBoxData' );
Route::get( '/export/all-yesterday-newlineconnections', 'Report\GlobalExport\GlobalExportController@yesterdayNewLineBoxData' );
Route::get( '/export/all-employee', 'Report\GlobalExport\GlobalExportController@allEmployeeBoxData' );
Route::get( '/export/all-package', 'Report\GlobalExport\GlobalExportController@allPackageBoxData' );
Route::get( '/export/today-newline-bill-collected', 'Report\GlobalExport\GlobalExportController@downloadCurmonthNewlinecollected' );
Route::get( '/export/newline/bill/due/collection', 'Report\GlobalExport\GlobalExportController@downloadCurmonthNewlineDueCollection' );
Route::get( '/export/area/wise/connections', 'Report\GlobalExport\GlobalExportController@exportAreaWiseConnection' );
Route::get( '/export/road/wise/connections', 'Report\GlobalExport\GlobalExportController@exportRoadWiseConnection' );
Route::get( '/export/newline/dis/connections', 'Report\GlobalExport\GlobalExportController@exportNewDisConnectionExportData' );

# master cc collection and dalily collection export
Route::get( '/export/master/cc/collection/{id}', 'Report\GlobalExport\GlobalExportController@exportMasterCollection' );
Route::get( '/export/daily/cc/collections/summery', 'Report\GlobalExport\GlobalExportController@exportDailyCollectionSummery' );


// Location wise box reports
Route::get( '/check-existing-client-live-search/{find_info}', 'Frontpanel\Newline\NewlineController@checkExistingClientLive' );
Route::get( '/old-client-information-checking/{clientInfo}', 'Frontpanel\Newline\NewlineController@oldClientInforChecking' );
Route::get( '/frontend-box-search', 'Inventory\InventoryManagementController@frontBoxSearch' );
Route::get( '/inventry-address-search', 'Inventory\InventoryManagementController@inventoryBoxSearch' );
Route::get( '/location/wise/box/reports', 'Inventory\InventoryManagementController@locationWiseBoxReports' );




});