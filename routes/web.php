<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
#Avoid CORS errors

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods:  POST, GET, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');
Route::get('key', 'Key\Base32Controller@make_license');
Route::get('url', 'Key\Base32Controller@urlGenerate');

Route::get('verification', 'Key\VerificationController@index');

Route::get('sendEmail', 'Notification\EmailController@sendEmail');

Route::get('sendSms', function() {
	// echo phpinfo();
	// die();
	//pjgjkwbyuegjqcna
	$user 	= "tetrasoft";
	$pass 	= "159753";
	$sid 	= "tetrasoft1";
	$url 	= "http://sms.sslwireless.com/pushapi/dynamic/server.php";
	$param  = "user=$user&pass=$pass&sms[0][0]= 8801763293227 &sms[0][1]=".urlencode("Test SMS from ISPERP")."&sms[0][2]=123456789&sms[1][0]= 8801989442856 &sms[1][1]=".urlencode("Test SMS from ISPERP")."&sms[1][2]=123456790&sid=$sid";
	
	$crl = curl_init(); 
	curl_setopt($crl,CURLOPT_SSL_VERIFYPEER,FALSE); 
	curl_setopt($crl,CURLOPT_SSL_VERIFYHOST,2); 
	curl_setopt($crl,CURLOPT_URL,$url); 
	curl_setopt($crl,CURLOPT_HEADER,0); 
	curl_setopt($crl,CURLOPT_RETURNTRANSFER,1); 
	curl_setopt($crl,CURLOPT_POST,1); 
	curl_setopt($crl,CURLOPT_POSTFIELDS,$param);

	$response = curl_exec($crl); 
	curl_close($crl);

	echo $response;

});


// Route::get('/', function () {
//     return view('welcome');
// });
use Illuminate\Support\Facades\Mail ;

/*Route::get('/mail', function(){
	

	Mail::send('mail', ['name' => 'value'], function($message)
		{
			$address = 'roton706@gmail.com';
		    $message->to($address, 'some guy')->subject('Welcome!');
		});
});*/
Route::group(['middleware' => 'seller_guest'], function() {

Route::get('seller_register', 'SellerAuth\RegisterController@showRegistrationForm');
Route::post('seller_register', 'SellerAuth\RegisterController@register');
Route::get('seller_login', 'SellerAuth\LoginController@showLoginForm');
Route::post('seller_login', 'SellerAuth\LoginController@login');

});

Route::get('/hash', function(){
	return Hash::make('123456');
});

Route::get('/info', function() {
	echo date('Y-m-d H:i:s');
	echo phpinfo();
});

Route::get( '/', 'Frontpanel\Login\userLoginController@index');
Route::get( '/login', 'Frontpanel\Login\userLoginController@index');
// Route::get( '/database', 'Frontpanel\DashboardController@databaseBackup');

$router->group( ['middleware' => 'auth'] , function($router) {

Route::get( '/database', 'Frontpanel\DashboardController@databaseBackup');
/*********************************************************
 *				Front End 								 *
 *********************************************************/


/*--------------------------------------------------------
 *				Front Admin Management					 *
 *-------------------------------------------------------*/
Route::get( '/admin-profile', 'Frontpanel\Profile\ProfileController@index');

/*--------------------------------------------------------
 *				New Line Management						 *
 *-------------------------------------------------------*/
Route::get( '/setup-new-line', 'Frontpanel\Newline\NewlineController@index');
Route::get( '/client-agreement', 'Frontpanel\Newline\NewlineController@clientAgreement');
/*--------------------------------------------------------
 *				Employee Management						 *
 *-------------------------------------------------------*/
Route::get( '/new-employee', 'Frontpanel\Employee\EmployeeController@newEmployee');
Route::get( '/all-employee', 'Frontpanel\Employee\EmployeeController@allEmployee');

/*********************************************************
 *				Admin Dashboard							 *
 *********************************************************/



Route::get( '/all-admin', 'Admin\AdminController@all_admin' );
Route::get( '/admin-password-changed', 'Admin\AdminController@adminPasswordChanged' );
//Route::get( '/user/newpackage/change/request/search/{find}', 'Linemgt\LinemanagementController@packageChangeInfoSearch' );


/*--------------------------------------------------------
 *				Complain Management						 *
 *-------------------------------------------------------*/

Route::get( '/client-complain', 'Cmpmgt\ComplainController@index' );
Route::get( '/technician-employee', 'Cmpmgt\ComplainController@techEmployee' );

/*--------------------------------------------------------
 *				Billing Management						 * 
 *-------------------------------------------------------*/

Route::get( '/billing-information', 'Billing\BillingController@index' );


/*--------------------------------------------------------
 *				Navigation Management					 *
 *-------------------------------------------------------*/

Route::get( '/create-navigation', 'Navigation\NavigationMgtController@index');
Route::get( '/manage-navigation', 'Navigation\NavigationMgtController@navManagement');

/*--------------------------------------------------------
 *				Settings Section						 *
 *-------------------------------------------------------*/

Route::get( '/header-setup', 'Settings\SettingsController@index' );
Route::get( '/footer-setup', 'Settings\FooterController@index' );

/**
 * 	Conver all User Mobile Number different format to same format
 */
Route::get('/convert/mobilenumber', 'Settings\GlobalSystemController@mobileNumberFormation');

/**
 * 	Dashboard Notification Center
 */

Route::get('/dashboard/notification/mark-as-read', function() {

	Auth::user()->unreadNotifications->markAsRead();

});


});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/login/custom',[
	'uses' =>'Frontpanel\Login\userLoginController@authenticate',
	'as'  =>'login.custom'
]);
