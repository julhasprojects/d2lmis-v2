<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$router->group( ['middleware' => 'auth'] , function($router) {

# Billing Reports

# Newline Billing Reports
Route::match(['get', 'post'],'/today/newline/bill/collections', 'Report\Bill\BillReportGenerateController@currentMonthCollection');
Route::match(['get', 'post'],'/confirm/newline/bill/collections', 'Report\Bill\BillReportGenerateController@currentMonthCollection');
Route::match( ['get', 'post'], '/current/month/newline/bill/collections', 'Report\Bill\BillReportGenerateController@allNewlineBillCollection');

# Generate Bill Reports
Route::match(['get', 'post'], '/bill/generated-bills-all', 'Report\Bill\BillReportGenerateController@allGeneratedBills' );
Route::get( '/bill/bill-collection-statement', 'Report\Bill\BillReportGenerateController@billCollectionStatement' );
Route::get( '/bill/corporate/bill-collection-statement', 'Report\Bill\BillReportGenerateController@billCorporateCollectionStatement' );
Route::get( '/corporate/generated/bills', 'Report\Dash\DashComplainReportController@corporateGeneratedBillsList' );
Route::get( '/reseller/generated/bills', 'Report\Dash\DashComplainReportController@resellerGeneratedBillsList' );
Route::get( '/bill/generated/update/information', 'Report\Dash\DashComplainReportController@resellerGeneratedBillsUpdate' );
Route::match(['get', 'post'],'/bill/generated/user/reconnected', 'Report\Bill\BillReportGenerateController@billGenerateUserReconnected' );
Route::match(['get', 'post'], '/old/user/reconnected', 'Report\Bill\BillReportGenerateController@oldUserBillReconnected' );
Route::get( '/bill/all/bill/collected/list/date', 'Report\Bill\BillReportGenerateController@allCollectedBillListDate' );
Route::get( '/bill/all/bill/area/wise/collected', 'Report\Bill\BillReportGenerateController@areaWiseallCollectedBills' );
Route::get( '/bill/all/bill/collection/by/collector', 'Report\Bill\BillReportGenerateController@collectorWiseBillCollected');
Route::get( '/foster/bill/sheet/download', 'Report\Bill\BillReportGenerateController@fosterBillSheetDownload');

#collected Bill reports
Route::get( '/bill/collection-sheet', 'Report\Bill\BillReportGenerateController@billCollectionReport' );
Route::get( '/admin/bill/collection/by/collector/info', 'Report\Bill\BillReportGenerateController@billCollectionByCollectorInfo' );
Route::get( '/admin/bill/region/by/region/info', 'Report\Bill\BillReportGenerateController@billCollectionByRegionByRegion' );
Route::get( '/admin/bill/collection/by/collector', 'Report\Bill\BillReportGenerateController@billCollectionByCollector' );
Route::get( '/bill/collection/sheet/date', 'Report\Bill\BillReportGenerateController@collectionDate' );
Route::get( '/corporate/collected/bills', 'Report\Bill\BillReportGenerateController@corporateCollectedBillsList' );
Route::get( '/reseller/collected/bills', 'Report\Bill\BillReportGenerateController@resellerCollectedBillsList' );
Route::get( '/todays-collections-bill-summery', 'Report\Bill\BillReportGenerateController@todaysCollectonBillSummery' );
Route::get( '/collections/bill/summery/search', 'Report\Bill\BillReportGenerateController@collectonBillSummerySearch' );
Route::get( '/collections/bill/summery/monthly/search', 'Report\Bill\BillReportGenerateController@collectonBillSummeryMonthlySearch' );
Route::get( '/yesterdays-collections-bill-summery', 'Report\Bill\BillReportGenerateController@yesterdayCollectonBillSummery' );

#Bill adjust
Route::get( '/bills/adjustment', 'Report\Bill\BillReportGenerateController@allBillsAdjustment' );
Route::get( '/corporate/bills/adjustment', 'Report\Bill\BillReportGenerateController@corporateBillsAdjustment' );
Route::post( '/corporate/bills/adjustment/regionalemp', 'Report\Bill\BillReportGenerateController@corporateBillsAdjustmentByRegionalEmp' );
Route::get( '/bill/all-bill-collected-list', 'Report\Bill\BillReportGenerateController@allCollectedBillList' );
Route::get( '/bill/collections/box/report', 'Report\Bill\BillReportGenerateController@billCollectionBoxReports' );
Route::get( '/bill/update-collected-bill/{billno}', 'Report\Bill\BillReportGenerateController@updateColeectedBill' );
Route::get( '/bill/delete-collected-bill/{billno}', 'Report\Bill\BillReportGenerateController@destroyBillCollectionInfo' );
Route::get( '/bill/due-collected-bill/{id}', 'Report\Bill\BillReportGenerateController@destroydueBillCollectionInfo' );
Route::post( '/bill/update-confirm-collected-bill', 'Report\Bill\BillReportGenerateController@updateCollectionConfirm' );
Route::post( '/bill/update-all-collected-bill', 'Report\Bill\BillReportGenerateController@updateAllBillCollection' );

# Bill  Summery
Route::match(['get', 'post'], '/genarated-bill-summery', 'Report\Bill\BillReportGenerateController@getBillSummeryReport' );
Route::get('genarated-bill-summery-search', 'Report\Bill\BillReportGenerateController@getBillSummeryReportSearch' );
Route::match(['get', 'post'],'/home-bill-summery', 'Report\Bill\BillReportGenerateController@homeBillSummery' );
Route::match(['get', 'post'],'/corporate-bill-summery', 'Report\Bill\BillReportGenerateController@corporateBillSummery' );
Route::match(['get', 'post'],'/reseller-bill-summery', 'Report\Bill\BillReportGenerateController@resellerBillSummery' );
Route::match(['get', 'post'],'/newline-bill-summery', 'Report\Bill\BillReportGenerateController@newLineBillSummery' );
Route::match(['get', 'post'],'/newline-bill-summery-search', 'Report\Bill\BillReportGenerateController@newLineBillSummerySearch' );
Route::match(['get', 'post'],'/reconnection-bill-summery', 'Report\Bill\BillReportGenerateController@reconnectionBillSummery' );
Route::match(['get', 'post'],'/disconnect-bill-summery', 'Report\Bill\BillReportGenerateController@disconnectSummeryBill' );
Route::match(['get', 'post'],'/newline-and-disconnection-bill-summery', 'Report\Bill\BillReportGenerateController@newlineDisconnectionBillsummery' );
Route::get( '/due/bill/summary', 'Report\Bill\BillReportGenerateController@dueBillSummery' );
Route::get( '/due/bill/user', 'Report\Bill\BillReportGenerateController@dueBillUser' );
Route::get( '/due/bill/collection/{date}', 'Report\Bill\BillReportGenerateController@dueBillCollection' );
Route::get( '/bill/generated/history/', 'Report\Bill\BillReportGenerateController@billGeneratedHistory' );
Route::get( '/before/generated/bill/client/list/', 'Report\Bill\BillReportGenerateController@beforeGeneratedBillClientList' );


Route::get('/isperp/newline/and/disconnect/bill/summery/search', 'Report\Bill\BillReportGenerateController@newlineAndDisconnectBillSummerySearch' );


Route::get( '/special-discounts', 'Report\Bill\BillReportGenerateController@speecialDiscount' );
Route::get( '/export/legacy/due/bill/statement', 'Report\Bill\BillReportGenerateController@exportLegacyMonthDueBills' );
Route::post( '/bill/due/payments/search/date', 'Report\Bill\BillReportGenerateController@duePaymentsResultDate' );
Route::get( '/search/employee/billing/summery/result/{find}', 'Report\Bill\BillReportGenerateController@duePaymentSummeryByEmployee' );
Route::get( '/search/client/billing/summery/result/{find}', 'Report\Bill\BillReportGenerateController@duePaymentSummeryByClient' );
Route::get( '/due-payment/bill/live-report',  'Report\Bill\BillReportGenerateController@liveDueBillReport' );


Route::get('/set/client/distributor', 'Linemgt\LinemanagementController@setDistributor');
 /*-----------------------------------------------------*
 *			Client Profile Details					*
 *------------------------------------------------------*/

 /*-----------------------------------------------------*
 *			Employee Management Reports					*
 *------------------------------------------------------*/

Route::get( '/individual-employee-report', 'Report\Emp\EmpController@allEmpList' );
Route::get( '/individual-employee/{employeeID}', 'Report\Emp\EmpController@allEmpListEdit' );
Route::get( '/individual-employee/destroy/{employeeid}', 'Report\Emp\EmpController@allEmpListDestroy' );
Route::get( '/individual/employee/details-report/{id}', 'Report\Emp\EmpController@empDetails' );
Route::get( '/designation/list/search', 'Report\Emp\EmpController@dateWishSearch' );
Route::get( '/master-employee-report', 'Report\Emp\EmpController@masterEmployeeAll' );
Route::get( '/individual/employee/info-search/bar', 'Report\Emp\EmpController@individualEmpInfoBar' );
Route::get( '/individual/employee/gender/search/{find_info}', 'Report\Emp\EmpController@EmpInfoByGender' );
Route::get( '/individual/employee/department/search/{find_info}', 'Report\Emp\EmpController@EmpInfoByDepartment' );
Route::get( '/individual/employee/designation/search/{find_info}', 'Report\Emp\EmpController@EmpInfoByDesignation' );
Route::post( '/individual/employee/date/search', 'Report\Emp\EmpController@EmpInfoByDate' );

 /*--------------------------------------------------------
 *			Inventory Management Reports             	 *
 *-------------------------------------------------------*/

Route::get( '/inventory-boxno-based-search/{find}', 'Report\Nline\NewLineReportController@inventoryBoxNoSearchResult' );
Route::get( '/individual-inventory-box-report', 'Report\Nline\NewLineReportController@inventoryBoxDetails' );
Route::post( '/export-inventory-box-details', 'Report\Nline\NewLineReportController@exportInventoryBoxNoSearchResult'  );
Route::get( '/export-master-box-report', 'Report\Nline\NewLineReportController@exportInventoryBoxMasterResult'  );


// Route::post( '/inventory/box/details/export', 'Report\Nline\NewLineReportController@inverntoryBoxDetailsExport' );

Route::get( '/inventory-box-details-boxno-edit', 'Report\Nline\NewLineReportController@inventoryBoxDetailsBoxEdit');
Route::get( '/inventory-box-details-address-edit', 'Report\Nline\NewLineReportController@inventoryBoxDetailsAddressEdit');
Route::get( '/inventory-box-details-switchname-edit', 'Report\Nline\NewLineReportController@inventoryBoxDetailsSwitchNameEdit');
Route::get( '/inventory-box-details-mcname-edit', 'Report\Nline\NewLineReportController@inventoryBoxDetailsMCNameEdit');
Route::get( '/inventory-box-details-popaddress-edit', 'Report\Nline\NewLineReportController@inventoryBoxDetailsPopAddressEdit');
Route::get( '/inventory-box-details-cable-edit', 'Report\Nline\NewLineReportController@inventoryBoxDetailsCableEdit');

/*Route::get( '/inventory-box-details-edit', 'Report\Nline\NewLineReportController@inventoryBoxDetailsPortEdit');*/
 /*--------------------------------------------------------
 *			Line Management Reports					 	 *
 *-------------------------------------------------------*/
Route::get( '/all-newline-request-report', 'Report\Nline\NewLineReportController@newlineAll' );
Route::get( '/newline-connection/report/info-search/{find_info}', 'Report\Nline\NewLineReportController@newconReport' );
Route::get( '/details-newConnection-report/{newconid}', 'Report\Nline\NewLineReportController@allConnectionDetails' );
Route::get( '/master-newline-report', 'Report\Nline\NewLineReportController@masterConnectionDetails' );
Route::get( '/master/newline/connection/report/info-search/{find_info}', 'Report\Nline\NewLineReportController@masterNewconReport' );
Route::get( '/technician/region/road/info-search/{find_info}', 'Report\Nline\NewLineReportController@getTechnicianRegionRoads' );

Route::get( '/newline/status/search/{find_info}', 'Report\Nline\NewLineReportController@lineApprovedBy' );
Route::get( '/search/new-line/area/request/{find_info}', 'Report\Nline\NewLineReportController@newconReportArea' );
Route::get( '/search/new-line/tech/request/{find_info}', 'Report\Nline\NewLineReportController@newconReportTech' );
Route::post( '/search/newline/date/request', 'Report\Nline\NewLineReportController@newlineByDate' );
Route::post( '/search/newline/region/request', 'Report\Nline\NewLineReportController@newlineByRegion' );

Route::get( '/all-client-contact-list-report', 'Report\Nline\NewLineReportController@newlineClientContact' );
Route::get( '/connection-date-wise-report', 'Report\Nline\NewLineReportController@newlineConnectDate' );
Route::get( '/technician-wise-report', 'Report\Nline\NewLineReportController@newlineTechnician' );
Route::get( '/connection-area-wise-report', 'Report\Nline\NewLineReportController@newlineConnectArea' );
Route::get( '/client-gender-based-report', 'Report\Nline\NewLineReportController@newlineClientGender' );
Route::get( '/line-setup-summery-report', 'Report\Nline\NewLineReportController@newlineSummery' );

/*--------------------------------------------------------
 *			Line Disconnect Reports					 	 *
 *-------------------------------------------------------*/
 
 Route::get( '/all-line-disconnect-report', 'Report\Ldis\LineDisconnectController@linedisAll' );
 Route::get( '/disconnection/request/rejection', 'Report\Ldis\LineDisconnectController@disconnectionRequestRejection' );
 Route::get( '/disconnection/request/rejection/search', 'Report\Ldis\LineDisconnectController@disconnectionRequestRejectionSearch' );
 Route::get( '/disconnect/report/info-search/{find_info}', 'Report\Ldis\LineDisconnectController@disconnectReport' );
 Route::get( '/disconnect/cause/report/info-search/{find_info}', 'Report\Ldis\LineDisconnectController@disconnectCauseReport' );
 Route::get( '/master-disconnect-report', 'Report\Ldis\LineDisconnectController@masterLineDisDetails' );
 Route::get( '/master/disconnect/edit/{newconid}', 'Report\Ldis\LineDisconnectController@masterLineDisEdit' );
 Route::post( '/master/disconnect/update', 'Report\Ldis\LineDisconnectController@masterDisconnectUpdate' );
 Route::get( '/update-live-disconnect-cause', 'Report\Ldis\LineDisconnectController@updateLiveDisCause' );
 Route::get( '/current-month-disconnect-report', 'Report\Ldis\LineDisconnectController@curentMonthLineDisDetails' );
 Route::get( '/disconnect/report/client/info/search/{find_info}', 'Report\Ldis\LineDisconnectController@disClientReport' );
 Route::get( '/line-reconnect/{find_info}', 'Report\Ldis\LineDisconnectController@lineReConnect' );
 Route::get( '/disconnect/report/area-search', 'Report\Ldis\LineDisconnectController@disconnectAreaReport' );
 Route::get( '/disconnect/date/search', 'Report\Ldis\LineDisconnectController@disconnectDateReport' );
  Route::get( '/disconnect/monthly/yearly/search', 'Report\Ldis\LineDisconnectController@disconnectMonthlyYearly' );
 Route::get( '/disconnect/region/house/search', 'Report\Ldis\LineDisconnectController@disconnectRegionHouse' );
 Route::get( '/disconnect/id/search', 'Report\Ldis\LineDisconnectController@discnnnectIDSearh' );
 Route::get( '/disconnect/second/cause/search', 'Report\Ldis\LineDisconnectController@disconnectSecondCauseSearch' );

 Route::get( '/period-based-line-disconnect-report', 'Report\Ldis\LineDisconnectController@linedisPeriod' );
 Route::get( '/area-based-line-disconnect-report', 'Report\Ldis\LineDisconnectController@linedisArea' );
 Route::get( '/cause-based-line-disconnect-report', 'Report\Ldis\LineDisconnectController@linedisCause' );
 Route::get( '/line-disconnect-summery-report', 'Report\Ldis\LineDisconnectController@linedisSummery' );

//SMS Reports 
 Route::get( '/sms/consumption/repots', 'Report\SMS\SMSReportController@smsConsumptionReports' );
 Route::get( '/sms/monthly/search', 'Report\SMS\SMSReportController@smsMonthlySearch' );
 Route::get( '/sms/date/search', 'Report\SMS\SMSReportController@smsDateSearch' );
 Route::get( '/sms/region/search', 'Report\SMS\SMSReportController@smsRgionSearch' );

/*--------------------------------------------------------
 *			Billing Reports					 		 *
 *-------------------------------------------------------*/
Route::get( '/disconnected/due/bills', 'Report\Bill\BillReportGenerateController@disconnectDueBills');

Route::get( '/bill/bill-receipents-statement', 'Report\Bill\BillReportGenerateController@allReceipentStatements' );
Route::get( '/bill/update-receipents-statement', 'Report\Bill\BillReportGenerateController@updateAllReceipentStatements' );
Route::get( '/export/bill/disconnected/due/bill', 'Report\Csv\ExportcsvController@exportDisConnectDueBill' );
Route::get( '/export/foster/billsheet/download', 'Report\Csv\ExportcsvController@exportFosterBillsheetDownload');
Route::post( '/bill/update/receipents/statement/region/road', 'Report\Bill\BillReportGenerateController@updateAllReceipentStatementsRegion' );
Route::post( '/bill/bill-collection-statement-collector', 'Report\Bill\BillReportGenerateController@billCollectionStatementCollector' );
Route::get( '/billgenerate/get/road/by/regionID/{region_id}', 'Report\Bill\BillReportGenerateController@getEmployeeRegionRoads' );
Route::resource( '/bill-generate/report/region-road/search', 'Billing\BillingController@regionroadWiseGeneratedbill' );
Route::post( '/print/bill-generate/report/region-road/search', 'Report\Csv\ExportcsvController@printAreawiseBill' );

// PPPOE Radius
Route::resource( '/pppoe-radius', 'Mikrotik\PPPOERadiusController' );

// delete this route after test
// Route::get( '/test', 'Report\Bill\BillReportGenerateController@updateAllBillCollectionTest' );

Route::get( '/bill/partial/due/lists', 'Report\Bill\BillReportGenerateController@partialDueBillLists' );


Route::get( '/bill/summary', 'Report\Bill\BillReportGenerateController@billSummery' );
Route::get( '/bill/adjustment', 'Report\Bill\BillReportGenerateController@billAdjustment' );
Route::get( '/bill/collection', 'Report\Bill\BillReportGenerateController@billColStatement' );
Route::get( '/bill/due/payments', 'Report\Bill\BillReportGenerateController@duePayments' );
Route::get( '/bill/register', 'Report\Bill\BillReportGenerateController@index' );
Route::get( '/bill/register/details/{id}', 'Report\Bill\BillReportGenerateController@billRegister' );

Route::get( '/legacy/due/bill/list', 'Report\Bill\BillReportGenerateController@currentMonthDueBills' );
Route::match(['get', 'post'], '/unpaid/collection', 'Report\Bill\BillReportGenerateController@unPaidCollection' );
Route::post( '/legacy/due/bill/collector/list', 'Report\Bill\BillReportGenerateController@dueBillsByCollectorWise' );
Route::get( '/bill/summery/calendar', 'Report\Bill\BillReportGenerateController@billSummeryCalendar' );

/********************** sepecial discounts ******************************************/



Route::resource( '/due/bill/collection/statement/by/collector', 'Report\Bill\BillReportGenerateController@dueBillCollectionStatementCollector' );
Route::get( '/search/live/billing/summery/result/{find}', 'Report\Bill\BillReportGenerateController@liveBillSummery' );
Route::post( '/search/live/billing/summery/date', 'Report\Bill\BillReportGenerateController@billSummeryDate' );
Route::get( '/destroy-billing-report/{id}', 'Billing\BillReportController@destroy' );
Route::get( '/edit-billing-report/{id}', 'Billing\BillReportController@edit' );
Route::post( '/update-client-billing-report', 'Billing\BillReportController@update' );


Route::get( '/edit/generated/bill/report/{billno}', 'Billing\BillingController@editGeneratedBill' );
Route::post( '/update/generated/bill/report', 'Billing\BillingController@updateGeneratedBill' );
Route::get( '/destroy/generated/bill/report/{billno}', 'Billing\BillingController@destroyGeneratebill' );
Route::get( '/details/generated/bill/report/{billno}', 'Billing\BillingController@billingDetails' );
Route::post( '/update-generated-bill-recipent-statement', 'Billing\BillingController@confirmBillReceipent' );
Route::resource( '/bill/report/generated-bill/region', 'Report\Csv\ExportcsvController@generatedBillsDate' );
Route::post( '/bill/receipents/statement/date', 'Report\Bill\BillReportGenerateController@allReceipentStatementsDate' );
Route::post( '/bill/receipents/statement/region/road', 'Report\Bill\BillReportGenerateController@allReceipentStatementsRegion' );
Route::post( '/bill/update/receipents/statement/date', 'Report\Bill\BillReportGenerateController@updateStatementDate' );
Route::resource( '/bill/bill/collection/statement/date', 'Report\Bill\BillReportGenerateController@collectionStatementDate' );
Route::get( '/bill/collection/statement/region/road', 'Report\Bill\BillReportGenerateController@collectionStatementRegion' );
Route::get( '/corporate/clients/bill/adjustment/{billno}', 'Report\Bill\BillReportGenerateController@updateCorporateClientsBill');
Route::post( '/corporate/clients/bill/adjustment', 'Report\Bill\BillReportGenerateController@updateCorporateClientsBillConfirm' );
Route::get( '/special/discount/edit/{newconid}', 'Report\Bill\BillReportGenerateController@specialDiscountuUpdate');
Route::post( '/special/discount/bill', 'Report\Bill\BillReportGenerateController@specialDiscountUpdateBill');
Route::get( '/bill-history-livesearch', 'Report\Bill\BillReportGenerateController@billHistoryPanelLivesearch');
Route::get( '/master/due/bills', 'Report\Bill\BillReportGenerateController@masterDueBills' );
Route::get( '/master/due/bills/customer/type/search', 'Report\Bill\BillReportGenerateController@masterDueBillsCustomerTypeSearch' );
Route::get( '/master/due/bills/monthly/search', 'Report\Bill\BillReportGenerateController@masterDueBillsMonthlySearch' );
Route::get( '/master/due/bills/monthly/search/list', 'Report\Bill\BillReportGenerateController@masterDueBillsMonthlySearchList' );
Route::post('confirm/newline/bill/collection/submit', 'Report\Bill\BillReportGenerateController@todayBillCollectionSubmit');
// Route::post('newline/bill/collection/date-search', 'Report\Bill\BillReportGenerateController@currentMonthCollectionDateSearch');
// Route::post('final/newline/bill/collection/date-search', 'Report\Bill\BillReportGenerateController@currentMonthNewlineCollection');


/*
* 	Ajax Live serarch bill/receipents/statement/region/road
*/

Route::get( '/individual-client-billgen/{find}', 'Billing\BillGenerate@individualClientBillGenInfoShow');
Route::get( '/bill-generate/report/info-search/{find_info}', 'Billing\BillingController@showGeneratedbill' );
Route::get( '/bill-generate/report/limit/search/{find_info}', 'Billing\BillingController@limitGeneratedbill' );
Route::get( '/bill-recipent/region/road/info-search/{find_info}', 'Billing\BillReportController@getEmployeeRegionRoads' );
Route::get( '/bill-recipent/report/search/limit/{find_info}', 'Billing\BillReportController@showRecipentBillLimit' );
Route::get( '/bill-update/recipent/report/info-search/{find_info}', 'Billing\BillReportController@showUpdateRecipentBill' );
Route::get( '/bill-update/recipent/report/search/limit/{find_info}', 'Billing\BillReportController@updateRecipentBillLimit' );
Route::get( '/bill-recipent/report/keyword-search/{find_info}', 'Billing\BillReportController@showRecipentBillLocation' );
Route::get( '/bill-recipent/report/month-search/{find_info}', 'Billing\BillReportController@showRecipentBillMonth' );
//Route::get( '/bill-update/recipent/report/search/tech/{find_info}', 'Billing\BillReportController@updateRecipentBillTech' );

Route::get( '/bill/bill/collection/statement/search/road/{find_info}', 'Report\Bill\BillReportGenerateController@billCollectionStatementRoad' );
Route::get( '/bill/bill/collection/statement/search/limit/{find_info}', 'Report\Bill\BillReportGenerateController@billCollectionStatementLimit' );
Route::get( '/bill/all/bill/collected/list/search/info', 'Report\Bill\BillReportGenerateController@allCollectedBillListInfo' );
Route::get( '/bill/collected/customer/type/search', 'Report\Bill\BillReportGenerateController@billCollectedCustomerTypeSearch' );
Route::get( '/bill/confirm/customer/type/search', 'Report\Bill\BillReportGenerateController@billConfirmCustomerTypeSearch' );
Route::get( '/bill/all/bill/collected/list/search/road/{find_info}', 'Report\Bill\BillReportGenerateController@allCollectedBillListRoad' );
Route::get( '/bill/all/bill/collected/list/search/limit/{find_info}', 'Report\Bill\BillReportGenerateController@allCollectedBillListLimit' );
Route::get( '/bill-update/recipent/report/search/tech/{find_info}', 'Report\Bill\BillReportGenerateController@updateRecipentBillTech' );
Route::get( '/bill/collection/sheet/liveinfo', 'Report\Bill\BillReportGenerateController@allCollectedBillInfoSearch' );
Route::get( '/bill/collection/sheet/search/road/{find_info}', 'Report\Bill\BillReportGenerateController@allCollectedBillRoad' );
Route::get( '/bill/collection/sheet/search/limit/{find_info}', 'Report\Bill\BillReportGenerateController@allCollectedBillLimit' );
Route::post( '/export/bill/due/statement/areabased', 'Report\Csv\ExportcsvController@exportBillDueStatements' );
Route::get( '/bill/due/payments/area/wise/search', 'Report\Bill\BillReportGenerateController@areabasedBillDuePayments' );

/*--------------------------------------------------------
 *			Complain Reports					 		 *
 *-------------------------------------------------------*/
Route::get( '/type-wise-complain-report', 'Report\Cmp\ComplainReport@cmpType' );
Route::get( '/master-complain-report', 'Report\Cmp\ComplainReportController@masterComplainDetails' );
Route::get( '/client-wise-complain-report', 'Report\Cmp\ComplainReport@cmpClient' );
Route::get( '/details-complain-report/{newconid}', 'Report\Cmp\ComplainReportController@complainDetails' );
Route::get( '/pending-complain-report', 'Report\Cmp\ComplainReport@cmPending' );
Route::get( '/solved-complain-report', 'Report\Cmp\ComplainReport@cmpSolved' );
Route::get( '/complain-summery-report', 'Report\Cmp\ComplainReportController@cmpSummery' );

Route::get( '/complain/report/info-search/{newconid}', 'Report\Cmp\ComplainReportController@complainReport' );
Route::get( '/corporate/clients', 'Report\Dash\DashComplainReportController@corporateClients' );
Route::get( '/reseller/clients', 'Report\Dash\DashComplainReportController@resellerClients' );


Route::get( '/corporate/due/bills', 'Report\Dash\DashComplainReportController@corporateDueBillsList' );
Route::get( '/reseller/due/bills', 'Report\Dash\DashComplainReportController@resellerDueBillsList' );
/*--------------------------------------------------------
 *			Backend Dashboard Reports					 *
 *-------------------------------------------------------*/
Route::post( 'due/all/bill/area/wise/collectetion', 'Report\Bill\BillReportGenerateController@areawisePartialDueBillLists' );
Route::post( '/export/partial/due/payment/areabased', 'Report\Csv\ExportcsvController@areaBasedExportPartialDueBills' );
Route::get( '/admin-panel', 'Report\Dash\DashComplainReportController@activeComplains' );

Route::get( 'admin-panel/live-search/{yId}', 'Report\Dash\DashComplainReportController@yearChangeBar' );
Route::any( 'admin-panel/lpie-live-search', 'Report\Dash\DashComplainReportController@yearMonthlyChangeLPie' );
Route::any( 'admin-panel/billdistributor-chart', 'Report\Dash\DashComplainReportController@yearMonthlyBillDistibutorChart' );

/**
 * 	Black listed users
 */

Route::get( '/short/time/blocked/users', 'Report\Bill\DefaulterController@blockedUsers');
Route::resource( '/defaulter/users', 'Report\Bill\DefaulterController');

Route::get( '/current/defaulter/users', 'Report\Bill\DefaulterController@currentDefaulter');
Route::get( '/defaulter/continuation/application', 'Report\Bill\DefaulterController@defaulterContinuationApplication');
Route::get( '/defaulter/continuation/application/approved', 'Report\Bill\DefaulterController@defaulterContinuationApplicationApproved');
Route::get( '/defaulter/continuation/application/disapproved', 'Report\Bill\DefaulterController@defaulterContinuationApplicationdisApproved');
Route::post( '/defaulter/continuation/application/delete', 'Report\Bill\DefaulterController@defaulterContinuationApplicationDelete');
Route::post( '/defaulter/continuation/application/request', 'Report\Bill\DefaulterController@defaulterContinuationApplicationRequest');
Route::get( '/defaulter/application/user/search', 'Report\Bill\DefaulterController@defualterApplicationUserSearch');
Route::get( '/defaulter/monthly/search', 'Report\Bill\DefaulterController@defualterMonthlyBillSearch' );
Route::get( '/defaulter/reback/monthly/search', 'Report\Bill\DefaulterController@defualterrebackMonthlyBillSearch' );
Route::get( '/export/bill/all/bill/inactive/list', 'Report\Bill\DefaulterController@defualterExportInativeClient' );

// Real IP Management 
Route::get( '/realip/reports', 'RealIP\RealIPManagementController@realIPReports');
Route::get( '/realip/mgt', 'RealIP\RealIPManagementController@realIPMgt');
Route::get( '/real/ip/charge/setup', 'RealIP\RealIPManagementController@realIPChargeSetup');
Route::post( '/real/ip/charge/request', 'RealIP\RealIPManagementController@realIPChargeRequest');
Route::get( '/realip/request/accept/{id}', 'RealIP\RealIPManagementController@realIPChargeRequestAccept');
Route::get( '/realip/request/edit/{id}', 'RealIP\RealIPManagementController@realIPChargeRequestEdit');
Route::put( '/realip/request/update/{id}', 'RealIP\RealIPManagementController@realIPChargeRequestUpdate');
Route::get( '/real/ip/all/request', 'RealIP\RealIPManagementController@realIPAllRequest');
Route::post( '/real/ip/delete', 'RealIP\RealIPManagementController@realIPDelete');
Route::get( '/realip/information/search', 'RealIP\RealIPManagementController@realIPInfomrationSearch');


Route::get( '/export/realip/userlist', 'RealIP\RealIPManagementController@exportRealIPReports');

// All bill summery export 
Route::get( '/export/bill/allCollector', 'Report\Csv\ExportcsvController@exportAllCollector' );
Route::get( '/export/bill/corporateCollector', 'Report\Csv\ExportcsvController@exportCorporateCollector' );
Route::get( '/export/bill/home/collector', 'Report\Csv\ExportcsvController@exportHomeSummeryExport' );
Route::get( '/export/reseller/bill/summery', 'Report\Csv\ExportcsvController@exportResellerBillSummery' );
Route::get( '/export/newline/bill/summery', 'Report\Csv\ExportcsvController@exportNewlineBillSummery' );
Route::get( '/export/newline/disconnect/bill/summery', 'Report\Csv\ExportcsvController@exportNewlineDisconnectBillSummery' );

Route::get( '/export/bill/update/receipents/statement', 'Report\Csv\ExportcsvController@index' );
Route::post( '/export/bill/update/receipents/statement/region', 'Report\Csv\ExportcsvController@downloadRoadWiseStatement' );
Route::get( '/export/bill/collection/sheet', 'Report\Csv\ExportcsvController@exportCollectedBill' );
Route::get( '/exort/before/generated/bill/client/list/', 'Report\Csv\ExportcsvController@exportGeneratedBillClientList' );

Route::get( '/export/bill/all/bill/collected/list', 'Report\Csv\ExportcsvController@allBillCollectionList' );
Route::get( '/export/bill/receipents/statement', 'Report\Csv\ExportcsvController@exportAllReceipentStatements' );
Route::get( '/export/bill/collection/statement', 'Report\Csv\ExportcsvController@exportBillCollectionReport' );
Route::get( '/export/due/bill/collection/statement/{collectorid}/{type}', 'Report\Csv\ExportcsvController@exportDueBillCollectionReport' );
Route::post( '/export/bill/collection/statement/region', 'Report\Csv\ExportcsvController@exportBillCollectionStatement' );
Route::get( '/export/due/bill/statement', 'Report\Csv\ExportcsvController@exportDueBill' );
Route::get( '/export/unpaid/collection', 'Report\Csv\ExportcsvController@exportUnpaidCollection' );
Route::get( '/export/due/bill/statement/collector/{id}', 'Report\Csv\ExportcsvController@exportCollactorDueBill' );
Route::get( '/export/partial/due/bill/statement', 'Report\Csv\ExportcsvController@exportPartialDueBills' );


Route::get( '/export/corporate/client/bills/adjustment',  'Report\Csv\ExportcsvController@exportCorporateClientBillAdjustment' );
Route::get( '/export/corporate/client/due/bills/adjustment',  'Report\Csv\ExportcsvController@exportCorporateClientDueBillAdjustment' );
Route::get( '/export/bill/summary/report', 'Report\Csv\ExportcsvController@exportBillSummeryReport' );
Route::get( '/export/bill/generated', 'Report\Csv\ExportcsvController@exportAllGeneratedBill' );
Route::get( '/export/corporate/bill/generated/bills', 'Report\Csv\ExportcsvController@exportCorporateGeneratedBill' );
Route::get( '/export/reseller/generated/bills', 'Report\Csv\ExportcsvController@exportResellerGeneratedBill' );
Route::get( '/export/corporate/clients/list', 'Report\Csv\ExportcsvController@exportCorporateClientList' );
Route::get( '/export/reseller/due/bill', 'Report\Csv\ExportcsvController@exportReselerDueBill' );
Route::get( '/export/corporate/due/bill', 'Report\Csv\ExportcsvController@exportCorporateDueBill' );

Route::get( '/export/last30days/newline/connection', 'Report\Csv\ExportcsvController@exportLast30DayConnections' );
Route::get( '/export/last30days/disconnect/connection', 'Report\Csv\ExportcsvController@exportLast30DayDisconnections' );
Route::match(['get', 'post'], '/export/line/diconnect/data', 'Report\Csv\ExportcsvController@exportLineDiconnectData' );
Route::post('/export/customer/type/disconnect/data', 'Report\Csv\ExportcsvController@exportCustomerDisconnectData' );
Route::get( '/export/newline/connection/data', 'Report\Csv\ExportcsvController@exportNewlineData' );
Route::get( '/export/disconnection/request/mgt', 'Report\Csv\ExportcsvController@exportDisconnectionRequestMgt' );
Route::get( '/export/bill/generated/user/reconnected', 'Report\Csv\ExportcsvController@exportBillGeneratedReconnected' );
Route::get( '/export/bill/generated/old/user/reconnected', 'Report\Csv\ExportcsvController@exportBillGeneratedOldReconnected' );
Route::get( '/export/todays/collections/bill/summery', 'Report\Csv\ExportcsvController@exportTodaysCollectionBillSummery' );
Route::get( '/export/yesterdays/collections/bill/summery', 'Report\Csv\ExportcsvController@exportYesterdaysCollectionBillSummery' );


# print reprots
Route::get( '/print/bill/generated', 'Report\Csv\ExportcsvController@printAllGeneratedBill' );
Route::resource('bill/generated/region-road/print', 'Report\Csv\ExportcsvController@printRegionWiseBill');

#Connection Management
Route::match(['get', 'post'], '/connection-mgt-reports', 'Report\ConnectionMgt\ConnectionMgtBiReports@connectionMgtReports' );// Take it from dashboard daily connections method
Route::get('/area/based/connection/date/search', 'Report\ConnectionMgt\ConnectionMgtBiReports@areBasedConnectionBiReports' );// Take it from dashboard daily connections method
Route::get('/collector/based/connection/date/search', 'Report\ConnectionMgt\ConnectionMgtBiReports@collectorBasedConnectionBiReports' );// Take it from dashboard daily connections method
Route::get('/area/based/disconnection/date/search', 'Report\ConnectionMgt\ConnectionMgtBiReports@areaBasedDisconnectionDateSearch' );// Take it from dashboard daily connections method
Route::get('/area/based/reconnection/date/search', 'Report\ConnectionMgt\ConnectionMgtBiReports@areaBasedReconnectionDateSearch' );// Take it from dashboard daily connections method


// Billing data management

/*1. Monthly generated and collected; ei report ei dashboard theke load hobe
  2. Collector Wise Daily Collections etio dashboard theke load hobe	
*/
Route::match(['get', 'post'],  '/billing-all-reports', 'Report\BillingMgt\BillingMgtController@billingAllReports' );
Route::get('/region/based/collection/date/search', 'Report\BillingMgt\BillingMgtController@regionWiseDailyCollectionBi' );
Route::get('/monthly/previous/due/date/search', 'Report\BillingMgt\BillingMgtController@monthlyPreviousDueComparisonBi' );

// Inventory Management
Route::match(['get', 'post'], '/inventory-box-management', 'Report\InventoryMgt\InventoryBiReports@inventoryAllReports' );
# Complain management
Route::match(['get', 'post'],  '/complain-all-reports', 'Report\ComplainMgt\ComplainMgtBiReports@complainAllReports' );
Route::get(  '/complain/bi/category/date/search', 'Report\ComplainMgt\ComplainMgtBiReports@complainBiCategorySearch' );
Route::get(  '/area/based/sloved/complain/date/search', 'Report\ComplainMgt\ComplainMgtBiReports@slovedComplainBiSearch' );

# all admintration reports
Route::match(['get', 'post'],  '/all-administration-reports', 'Report\Administrative\AdminLogController@administrationReports' );
Route::match(['get', 'post'],  '/others-report', 'Report\Dash\DashComplainReportController@othersReport' );

# Inventory System pages

Route::match(['get', 'post'],  '/master-box-report', 'Report\Dash\DashComplainReportController@masterBoxReport' );
Route::resource( '/box-info', 'Inventory\InventoryManagementController' );
Route::post( '/box-info/reback/{id}', 'Inventory\InventoryManagementController@boxInfoRebackId' );
Route::get( '/inventory/master/cable/report', 'Inventory\InventoryManagementController@inventoryMasterCableReport' );
Route::get( '/inventory/road/wise/cable/report', 'Inventory\InventoryManagementController@inventoryRoadWishCableReport' );
Route::get( '/inventory/area/wise/cable/report', 'Inventory\InventoryManagementController@inventoryAreaWishCableReport' );
Route::get( '/box/wise/cable/report', 'Inventory\InventoryManagementController@boxWishCableReport' );
Route::get('/master/box/management', 'Inventory\InventoryManagementController@masterBoxManagement');
Route::get('/master/box/management/switch/search', 'Inventory\InventoryManagementController@masterBoxManagementswitchSearch');
Route::get('/master/box/management/popaddress/search', 'Inventory\InventoryManagementController@masterBoxManagementPopAddressSearch');
Route::get('/master/box/management/mctype/search', 'Inventory\InventoryManagementController@masterMcTypeSearch');
Route::get('/master/box/management/sfptype/search', 'Inventory\InventoryManagementController@masterSFPTypeSearch');
Route::get('/master/box/management/port/search', 'Inventory\InventoryManagementController@masterPortTypeSearch');

Route::get( '/box/wise/cable/monthly/search', 'Inventory\InventoryManagementController@boxWishCableMonthly' );
Route::get( '/inventory/box/wise/complain', 'Inventory\InventoryManagementController@boxWiseComplain' );
Route::get( '/inventory/box/wise/complain/monthly/search', 'Inventory\InventoryManagementController@boxWiseComplainMonthlySearch' );
Route::get( '/inventory/box/wise/complain/date/search', 'Inventory\InventoryManagementController@boxWiseComplainDateSearch' );
Route::get( '/inventory/free/port/report', 'Inventory\InventoryManagementController@inventoryFreeportReport' );
Route::get( '/inventory/free/port/search', 'Inventory\InventoryManagementController@inventoryFreePortSearch' );
Route::get( '/inventory/monthly/box/search', 'Inventory\InventoryManagementController@inventoryMonthlyBoxSearch' );
Route::get( '/inventory/region/road/box/search', 'Inventory\InventoryManagementController@inventoryRegionRoadBoxSearch' );
Route::get( '/inventory/region/road/wise/box/report', 'Inventory\InventoryManagementController@inventoryRegionRoadWiseBoxReport' );
Route::get( '/inventory/technician/box/search', 'Inventory\InventoryManagementController@inventoryTechnchainBoxSearch' );
Route::get( '/inventory/box/number/check', 'Inventory\InventoryManagementController@inventoryBoxNumberCheck' );
Route::get( '/update-box-number', 'Inventory\InventoryManagementController@updateBoxNumber' );
Route::get( '/inventory/box/number/count', 'Inventory\InventoryManagementController@inventoryNumberCount' );
Route::get( '/inventory/update/box/number/count', 'Inventory\InventoryManagementController@inventoryUpdatedNumberCount' );
Route::match(['get', 'post'], '/inventory-box-search', 'Frontpanel\Newline\NewlineController@inventoryBoxSearch' );
Route::get( '/inventory/box/wise/complain', 'Inventory\InventoryManagementController@boxWiseComplain' );
Route::get( '/export/inventory/box/wise/complain', 'Inventory\InventoryManagementController@exportInventoryBoxWiseComplain'  );
Route::get( '/export/free/port/report', 'Inventory\InventoryManagementController@exportInventoryBoxWiseFreeport'  );
Route::get( '/export/inventory/master/cable/report', 'Inventory\InventoryManagementController@exportInventoryBoxWiseCable'  );
Route::get( '/export/inventory/master/box/report', 'Inventory\InventoryManagementController@exportInventoryMasterBoxReport'  );
Route::get( '/inventory-box-reports', 'Inventory\InventoryManagementController@inventoryBoxDetails' );
Route::get( '/inventory-box-by-clients/{id}', 'Inventory\InventoryManagementController@inventoryBoxClientDetails' );
Route::post( '/inventory-box-details-region-wise', 'Inventory\InventoryManagementController@inventoryBoxDetailsRegionWise' );


# collection summery accept
Route::post( '/newline/request/collection/approved', 'Report\Bill\BillReportGenerateController@newlineRequestCollectionApproved' );
Route::post( '/housechange/request/collection/approved', 'Report\Bill\BillReportGenerateController@housechangeCollectionApproved' );


/**
 * 	Reail IP
 */

Route::post('/confirm/realip/collection', 'Frontpanel\Bill\BillController@confirmRealIpCollection');



/**
 * Reseller Mgt
 */
Route::get( '/reseller/mgt', 'Reseller\ResellerMgtController@index' );
// Route::get( '/reseller/mgt/summery/reprots/{id}', 'Reseller\ResellerMgtController@resellerMgtSummeryReports' );
});