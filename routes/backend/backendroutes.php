<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
$router->group( ['middleware' => 'auth'] , function($router) {

// dashboard setup
Route::match(['get', 'post'],'/d2lmis-dashboard', 'Report\Dash\IsperpDashboardReports@isperpDashboard' );
Route::match(['get', 'post'],'/isperp-dashboard/{id}', 'Report\Dash\IsperpDashboardReports@isperpDashboardBranch' );
Route::match(['get', 'post'],'/notifecation/send', 'Report\Dash\IsperpDashboardReports@notifecationSend' );
Route::get('/isperp/dashboard/yearly/search', 'Report\Dash\IsperpDashboardReports@isperpDashboardYearlySearch' );
Route::get('/isperp/dashboard/collection/date/search', 'Report\Dash\IsperpDashboardReports@isperpDashboardCollectionDateSearch' );
Route::get('/isperp/dashfboard/connection/date/search', 'Report\Dash\IsperpDashboardReports@isperpDashboardConnectionDateSearch' );
Route::get('/isperp/dashboard/connection/type/date/search', 'Report\Dash\IsperpDashboardReports@isperpDashboardConnectionTypeDateSearch' );
Route::get('/isperp/notifecation/center', 'Report\Dash\IsperpDashboardReports@isperpNotifecationCenter' );
Route::get('/isperp/generated/amount', 'Report\Dash\IsperpDashboardReports@isperpGeneratedAmount' );
Route::get('/isperp/generated/user', 'Report\Dash\IsperpDashboardReports@isperpGeneratedUser' );
Route::get('/isperp/collected/amount', 'Report\Dash\IsperpDashboardReports@isperpCollectedAmount' );
Route::get('/isperp/collected/user', 'Report\Dash\IsperpDashboardReports@isperpCollectedUser' );
Route::get('/isperp/generated/collected/pie/amount', 'Report\Dash\IsperpDashboardReports@isperpGeneratedCollectedpieAmount' );
Route::get('/isperp/newline/and/disconnect/bill/summery', 'Report\Dash\IsperpDashboardReports@newlineAndDisconnectBillSummery' );

// Account Module route
Route::get('/accounting', 'Accounting\AccountingController@accountingDashboard' );
Route::resource('/accounting/income', 'Accounting\IncomeController' );

// Expense 
Route::resource('/accounting/expense', 'Accounting\ExpenseController' );
Route::post('/accounting/expenses/delete', 'Accounting\ExpenseController@expensesDelete' );

// Accounts Item 
Route::resource('/accounting/item', 'Accounting\ITEMController' );
Route::post('/accounting/item/delete', 'Accounting\ITEMController@itemDelete' );

// Accounts transection 
Route::resource('/accounting/transectiontype', 'Accounting\TransectionTypeController' );
Route::post('/accounting/transectiontype/delete', 'Accounting\TransectionTypeController@transectionTypeDelete' );

// Account Transection Mode  Declared
Route::resource('/accounting/transection/mode', 'Accounting\TransectionModeController' );
Route::post('/accounting/transection/mode/delete', 'Accounting\TransectionModeController@transectionModeDelete' );

// Account Tax Declared
Route::resource('/accounting/tax', 'Accounting\TaxController' );
Route::post('/accounting/tax/delete', 'Accounting\TaxController@taxDelete' );

// Account Route Declared
Route::resource('/accounting/accounts', 'Accounting\AccountsController' );
Route::post('/accounting/acounts/delete', 'Accounting\AccountsController@AccountsDelete' );

// Account Route Declared
Route::resource('/accounting/recuring/transection', 'Accounting\RecurringTransactionController' );
Route::post('/accounting/recuring/transection/delete', 'Accounting\RecurringTransactionController@recuringTransectionDelete' );

// data import export controller
Route::get('/accounting/import/export', 'Accounting\ImportExportController@dataImport' );
Route::get('/import/expenese/data', 'Accounting\ImportExportController@dataExpensImport' );
Route::get('/import/item/data', 'Accounting\ImportExportController@dataItemImport' );
Route::get('/import/transection/type/data', 'Accounting\ImportExportController@importTrasectionTypeData' );
Route::get('/import/transection/mode/data', 'Accounting\ImportExportController@importTrasectionMode' );
Route::get('/import/accounts/data', 'Accounting\ImportExportController@importTrasectionAccountsMode' );
Route::get('/import/tax/data', 'Accounting\ImportExportController@importTaxMode' );

Route::get('/isperp/dashboard/complain/date/search', 'Report\Dash\IsperpDashboardReports@isperpDashboardComplainDateSearch' );

// employee region assing
Route::get( '/employee-regions-assign', 'Settings\EmployeeRegionController@newEmployeeRegionSetup' );

// package setup route
Route::resource( '/package-create',  'Package\PackageController' );

// Confirm package change collection by admin
Route::post('master/package-change/collection/confirm', 'Frontpanel\Bill\BillController@confirmPackageChangeCollection');

// Confirm linedisconnect change collection by admin
Route::post('master/lindisconnect/collection/confirm', 'Frontpanel\Bill\BillController@confirmLineDisconnectCollection');

// Bill Date Setup
Route::resource( '/bill/date/and/system/setup', 'Settings\DateController' );
Route::resource( '/bill/system/setup', 'Settings\BillSetupController' );

// Download excel bill sheet route
Route::get('/download/excel', 'Import\DataImportController@showExcelData');

// Complain and disconnect cause
Route::resource( '/complain/setting', 'Settings\ComplainSettings');
Route::post( '/complain/table/setting', 'Settings\ComplainSettings@complainTableSetting');
Route::get( '/complain/table/setting/delete', 'Settings\ComplainSettings@complainTableSettingDelete');

Route::get( '/complain/settings/search', 'Settings\ComplainSettings@comlainSettingSearch');
Route::post( '/complain/setting/delete/{id}', 'Settings\ComplainSettings@complainDelete');

// company Setup route
Route::resource( '/company-profile', 'Company\CompanyController' );
Route::resource( '/branch/setup', 'Company\Branch\BranchSetupController' );
Route::get( '/search-admin-info/{find_info}', 'Admin\AdminController@show' );
Route::get( '/branch/region/search', 'Admin\AdminController@branchRegionSearch' );
Route::get( '/online/collection/report', 'Report\Online\OnlineTransactionController@index' );
Route::get( '/checkout/master/date/search', 'Report\Online\OnlineTransactionController@checkoutMasterDateSearch' );
Route::get( '/online/checkout/reports', 'Report\Online\OnlineTransactionController@onlineCheckoutReports' );
Route::get( '/checkout/master/monthly/search', 'Report\Online\OnlineTransactionController@checkOutMasterMonthlyeSearch' );
Route::get( '/online/collection/master/reports', 'Report\Online\OnlineTransactionController@onlineCollectionMasterReports' );
Route::get( '/online/collection/master/monthly/search', 'Report\Online\OnlineTransactionController@onlineCollectionMasterMonthlySearch' );
Route::get( '/online/collection/master/date/search', 'Report\Online\OnlineTransactionController@onlineCollectionMasterdateSearch' );
Route::get( '/online/collection/master/payment/method/search', 'Report\Online\OnlineTransactionController@onlineCollectionMasterPaymentMethodSearch' );
Route::get( '/online/collection/master/region/search', 'Report\Online\OnlineTransactionController@onlineCollectionMasterRegionSearch' );
Route::get( '/online/collection/master/customertype/search', 'Report\Online\OnlineTransactionController@onlineCustomerTypeSearch' );
Route::get( '/online/transection/due/bill/summery', 'Report\Online\OnlineTransactionController@onlineTransactionDueBillSummery' );

// user setup 
Route::resource( '/admin-information', 'Admin\AdminController' );
Route::get( '/newadmin/employee/employee-info/{find_info}', 'Admin\AdminController@employeeInfo' );

// upload excel sheet
Route::get('/upload-excel-file', 'Import\DataImportController@index');
Route::post('/upload/unplug/status', 'Import\DataImportController@importCableData');
Route::post('/import/client/data', 'Import\DataImportController@importUserList');
Route::post('/import/disconnect/data', 'Import\DataImportController@importDisconnectData');

// region setup excel
Route::resource( '/region/setup', 'Region\RegionSetupController');
Route::put( '/road/update/{id}', 'Region\RegionSetupController@roadUpdate');
Route::get( '/road/setup/{id}/edit', 'Region\RegionSetupController@roadEdit');
Route::get('/master/region/reports', 'Region\RegionSetupController@masterRegionReports');
Route::get('/master-search/region-regionbased-report', 'Region\RegionSetupController@masterRegionSearchData');
Route::get('/master-search/region-roadbased-report', 'Region\RegionSetupController@masterRoadSearchData');
Route::get('/master-search/roadbased-report', 'Region\RegionSetupController@masterRoadSearch');
Route::get('/master-search/region-upazilabased-report/{find}', 'Region\RegionSetupController@masterupazilaSearchData');

// region setup excel
Route::resource( '/division/setup', 'Region\DivisionSetupController');
Route::resource( '/district/setup', 'Region\DistrictSetupController');
Route::resource( '/upazila/setup', 'Region\UpazilaSetupController');
Route::get( '/division/search', 'Region\DistrictSetupController@divisionSearch');
Route::get( '/upazila/search', 'Region\UpazilaSetupController@upazilaSearch');
Route::get( '/region/search', 'Region\RegionSetupController@regionSearch');

// System permission panel
Route::resource( '/system/permission/panel', 'Admin\AdminAllowController' );
Route::resource( '/newline/setting/setup',  'Settings\NewlineSettingsController' );
Route::post( '/newline/setting/setup/delete',  'Settings\NewlineSettingsController@newlineSettingDeleted' );
Route::post( '/newline/setting/setup/charge/update/{id}',  'Settings\NewlineSettingsController@newlineSettingChargeUpdate' );
Route::resource( '/customer/type/setup',  'Settings\NewlineSetup\CustomerTypeSetupController' );
Route::resource( '/setup/new/package',  'Settings\PackageCostSetupController' );
Route::resource( '/real/ip/setup',  'Settings\realIpSetupController' );
Route::resource( '/hs/settings',  'Settings\HsSettingController' );
Route::resource( '/hs/settings',  'Settings\HsSettingController' );
Route::resource( '/interface/setup',  'Settings\InterfaceSetupController' );
Route::resource( '/base/setup',  'Settings\Base\BaseSetupController' );
Route::resource( '/mikrotik/integration',  'Settings\Mikrotik\MikrotikSetupController' );
Route::get( '/mikrotik/integrations/delete',  'Settings\Mikrotik\MikrotikSetupController@mikrotikIntegrationDelete' );
Route::get( '/mikrotik/integrations/hotspot',  'Mikrotik\HotspotController@add' );
Route::get( '/mikrotik/integration/list/{id}/edit',  'Settings\Mikrotik\MikrotikSetupController@mikrotikListEdit' );
Route::PUT( '/mikrotik/integration/list/{id}',  'Settings\Mikrotik\MikrotikSetupController@mikrotikListUpdate' );

// user role Management
Route::resource( '/super/admin', 'Settings\SupperAdminController' );
Route::resource( '/role/permission', 'Settings\Permission\PermissionManagementController' );
Route::resource( '/user/role', 'Settings\Permission\UserRoleController' );
Route::resource( '/new/role/permission', 'Settings\Permission\RolePermissionController' );
Route::resource( '/permission/menu/category', 'Settings\Permission\PermissionCategoryController' );

// Global Setiing
Route::resource( '/global/system/settings', 'Settings\GlobalSystemController' );
Route::get( '/global/system/all', 'Settings\GlobalSystemController@globalSystemAll' );

// Admin log 
Route::get('/admin/log', 'Report\Administrative\AdminLogController@adminLog');

// BTRC CLIENT DATA
Route::get( '/all/download/excel', 'Report\GlobalExport\GlobalExportController@exportBTRCClientData' );
Route::get( '/all/download/btrc/report/excel', 'Report\GlobalExport\GlobalExportController@exportBTRCClientDataFormatTwo' );

// Admin Summery Reports
Route::get('/admin/log/summary/report', 'Report\Administrative\AdminLogController@adminLogSummeryReports');

// client Sms Sending
Route::get( '/send-client-sms', 'Settings\SMS\SMSApiConfiguration@smsMgt' );
Route::get( '/send/sms/technician/{id}', 'Settings\SMS\SMSApiConfiguration@sendSMSTechnician' );
Route::post( '/send-client-sms', 'Settings\SMS\SMSApiConfiguration@smsApiConfigure' );
Route::post( '/category/sms/send/client', 'Settings\SMS\SMSApiConfiguration@categorySmsCategory' );
Route::resource( '/sms/management', 'Settings\SMS\SMSManagementController' );
Route::get( '/sms/module/message/search', 'Settings\SMS\SMSApiConfiguration@smsMessageSearch' );

// Sms Global Setting
Route::post( '/sms/global/setting', 'Settings\SMS\SMSManagementController@smsManagmentGlobal' );
Route::get( '/sms/global/setting/{id}/edit', 'Settings\SMS\SMSManagementController@smsManagmentGlobalEdit' );
Route::post( '/sms/global/setting/{id}', 'Settings\SMS\SMSManagementController@smsManagmentGlobalUpdate' );
Route::post( '/sms/global/settings/delete', 'Settings\SMS\SMSManagementController@smsManagmentGlobalDestroy' );
Route::resource( '/sms/type/setup', 'Settings\SMS\SMSTypeController' );
Route::resource( '/sms/response', 'Settings\SMS\SMSResponseController' );
Route::resource( '/sms/sending/history', 'Settings\SMS\SMSHistoryController' );

// company information update and delete and search
Route::get( '/destroy-company-info/{id}', 'Company\CompanyController@destroy' );
Route::post( '/update-company-info', 'Company\CompanyController@update' );
Route::get( '/search-company-info/{find_info}', 'Company\CompanyController@show' );

// Newline Setting
Route::get( '/search/package-info/{find_info}', 'Package\PackageController@packageSearch');
Route::get( '/search/all-package/{find_info}', 'Package\PackageController@allPackageSearch');

// Bill Generated option
Route::get( '/bill-generate', 'Billing\BillGenerate@generatedBillInfoShow');
Route::get( '/billinfo-clientdata-search/{find_info}', 'Billing\BillingController@clientInfoShow' );
Route::get( '/individual-bill-generate/{find_info}', 'Billing\BillGenerate@indiBillInfoShow' );
Route::get( '/individual-client-bill-generate/{find_info}', 'Billing\BillGenerate@individualClientBill' );
Route::post( '/add-bill', 'Frontpanel\Bill\BillController@store' );
Route::post( '/add-bill-all', 'Frontpanel\Bill\BillController@generateAllBill' );

// new admin create
Route::post( '/add-new-admin', 'Admin\AdminController@create' );

// Ajax live search 
Route::get( '/newadmin/employee/all-employee/{find_info}', 'Admin\AdminController@employeeShow' );
Route::get( '/destroy-employee-info/{id}', 'Cmpmgt\ComplainController@destroyEmployee' );
Route::get( '/update-employee-info/{id}', 'Cmpmgt\ComplainController@editEmployee' );
Route::post( '/bakend-update-employee-info', 'Cmpmgt\ComplainController@updateEmployee' );

// Ajax live search 
Route::get( '/search-client-complain-info/{find_info}', 'Cmpmgt\ComplainController@show' );
Route::get( '/search-techician-info/{find_info}', 'Cmpmgt\ComplainController@liveEmpinfoShow' );

// Billing Management  
Route::get( '/search-live-billing-result/{find_info}', 'Billing\BillingController@show' );
Route::get( '/destroy-billing-request/{id}', 'Billing\BillingController@destroy' );
Route::get( '/edit-billing-request/{id}', 'Billing\BillingController@edit' );
Route::post( '/update-client-billing-info', 'Billing\BillingController@update' );

// Billing Report Management
Route::get( '/billing-sheet', 'Billing\BillReportController@index' );
Route::get( '/destroy-billing-report/{id}', 'Billing\BillReportController@destroy' );
Route::get( '/edit-billing-report/{id}', 'Billing\BillReportController@edit' );
Route::post( '/update-client-billing-report', 'Billing\BillReportController@update' );
Route::get( '/details-billing-report/{id}', 'Billing\BillReportController@billingDetails' );

// User Manual 
Route::resource( '/user-manual', 'Settings\UserManual\UserManualController' );


// Mikrotik

Route::get( '/inactive/in-mikrotik', 'Mikrotik\MikrotikController@inactiveForDue');
Route::get( '/mikrotik/package/configure', 'Mikrotik\MikrotikPPPOERadiusController@mikrotikPackageConfigure');
Route::get( '/mikrotik/user/migration', 'Mikrotik\MikrotikPPPOERadiusController@mikrotikUserMigration');
Route::get( '/mikrotik/user/migration/search', 'Mikrotik\MikrotikPPPOERadiusController@mikrotikUserMigrationSearch');
Route::get( '/mikrotik/user/migration/pppoe', 'Mikrotik\MikrotikPPPOERadiusController@mikrotikUserMigrationpppoe');
Route::get( '/prepaid/card/generator', 'Settings\PrepaidCardGenerator@index');

// Collection Type Reports
Route::resource( '/collection/type/report', 'Settings\CollectionType\CollectionTypeController');
Route::resource( '/service/charge/setup', 'Settings\ServiceCharge\ServiceChargeSetupController');
Route::resource( '/realip/charge/setup', 'Settings\RealIP\RealIPChargeSetupController');

// CRM Management
Route::get( '/send-email', 'CRM\CRMController@sendEmail' );
// Email Setting
Route::resource( '/email/setting', 'Settings\Email\EmailSettingController' );
// 20 box report routes
Route::get( '/total-dues', 'Report\Dash\DashComplainReportController@totalDueBox' );
Route::get( '/lastmonth/cllection', 'Report\Dash\DashComplainReportController@lastmonthCollectionBox' );
Route::get( '/lastmonth/due', 'Report\Dash\DashComplainReportController@lastmonthDueBox' );
Route::get( '/currmonth/genbill', 'Report\Dash\DashComplainReportController@currMonGenBillBox' );
Route::get( '/sloved/complain', 'Report\Dash\DashComplainReportController@slovedComplainBox' );
Route::get( '/currmonth/connection', 'Report\Dash\DashComplainReportController@currMonthConBox' );
Route::get( '/active/employes', 'Report\Dash\DashComplainReportController@activeemployeeConBox' );
Route::get( '/active/client', 'Report\Dash\DashComplainReportController@activclientBox' );
Route::get( '/total/package', 'Report\Dash\DashComplainReportController@totalpackageBox' );
Route::post( '/assign/technician/region', 'Settings\EmployeeRegionController@assignEmployeeRegion' );
Route::get( '/get/road/by/regionID', 'Settings\EmployeeRegionController@getEmployeeRegionRoads' );
Route::get('/line/connection/history', 'Report\Administrative\AdminLogController@lineConnectionHistory');
Route::get('/line/history/keyword-search/{find}', 'Report\Administrative\AdminLogController@lineConnectionHistoryKeywordSearch');
Route::get('/line-connection/changed-technican-search/{find}', 'Report\Administrative\AdminLogController@lineConnectionHistoryTechnicianSearch');
Route::match(['get', 'post'],'/all/download/billsheetexcel', 'Report\GlobalExport\GlobalExportController@exportBillSheetData' );

// Payment Method Integration
Route::get('api/v1/user/check-by-transaction-id' , 'PaymentApi\BkashController@index');
Route::get('data/validations' , 'Settings\DataValidationController@index');
Route::get('data/validations/clintinfo' , 'Settings\DataValidationController@getInvalidClientInfo');
Route::post('data/validations/delete' , 'Settings\DataValidationController@getInvalidClientInfoDelete');
Route::get('data/validations/{id}/edit' , 'Settings\DataValidationController@getInvalidClientInfoEdit');
Route::post('data/validations/update' , 'Settings\DataValidationController@getInvalidClientInfoUpdate');


});