<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
$router->group( ['middleware' => 'auth'] , function($router) {
// HRMS dashboard
Route::match(['get', 'post'], '/hrm/dashboard', 'Report\HRMS\HrmsBiReports@hrmDashboard' );

#Employee Setup
Route::resource('/hrm/employees', 'HRM\EmployeesController');
Route::post('/hrm/employees/salarycrate', 'HRM\EmployeesController@salarycrate');
Route::PUT('/hrm/employees/company/details/{id}', 'HRM\EmployeesController@companyDetails');
Route::post('/hrm/employees/salary/destory/{id}', 'HRM\EmployeesController@salaryDestory');
Route::PUT('/hrm/employees/updated/bank/details/{id}', 'HRM\EmployeesController@bankDetails');
Route::get( '/export/hrm/employees', 'HRM\EmployeesController@exportHrmEmployees' );
Route::get( '/export/hrm/departments', 'HRM\DepartmentController@exportDepartments' );
Route::get( '/hrm/road/list', 'HRM\EmployeesController@hrmRoadList' );
Route::PUT( '/hrm/employees/updated/region/list/{id}', 'HRM\EmployeesController@hrmEmployeeRegionList' );

# Department Setup
Route::resource('/hrm/departments', 'HRM\DepartmentController');

# Expenses Setup
Route::resource('/hrm/expenses', 'HRM\ExpensesController');

# Awards Setup
Route::resource('/hrm/awards', 'HRM\AwardsController');

# Holidays Setup
Route::resource('/hrm/holidays', 'HRM\HolidaysController');

# Attendece Controller
Route::resource('/hrm/attendances', 'HRM\AttendancesController');
Route::get('/hrm/excuted/hours', 'HRM\AttendancesController@hrmExcutedHours');

Route::get( '/hrm/attendances/date/search', 'HRM\AttendancesController@hrmAttendanceDateSearch' );
Route::get( '/hrm/mark/attendances/date/search', 'HRM\AttendancesController@hrmAttendanceMarkDateSearch' );
Route::get( '/hrm/attendances/show/{id}', 'HRM\AttendancesController@hrmMarkAttendanceIdshow' );
Route::get('/hrm/monthly/attendance','HRM\AttendancesController@hrmMonthlyAttendance');
Route::get('/hrm/monthly/attendance/search','HRM\AttendancesController@hrmMonthlyAttendanceSearch');
Route::get('/hrm/employee/timesheet/approval','HRM\AttendancesController@hrmEmployeeTimeSheetApporcal');
Route::get('/hrm/employee/timesheet/approval/search','HRM\AttendancesController@hrmEmployeeTimeSheetApporcalSearch');
Route::get( '/hrm/daily/attendances', 'HRM\AttendancesController@hrmDailyAttndances' );
Route::get( '/hrm/over/time/management', 'HRM\OverTimeController@index' );

# Notice Board Setup
Route::resource('/hrm/leavetypes', 'HRM\LeavetypesController');

# Notice Board Setup
Route::resource('/hrm/noticeboards', 'HRM\NoticeBoardsController');

# Designtion Search 
Route::get('/designation/search', 'HRM\HrmController@designationSearch');

# Leave Aplliction 
Route::resource('/leave/application', 'HRM\HrmLeaveApplicationController');
Route::get('/hrm/leave/approval','HRM\HrmLeaveApplicationController@show');
Route::get('/hrm/leave/confirm','HRM\HrmLeaveApplicationController@hrmLeaveConfirm');
Route::get('/hrm/leave/available/search','HRM\HrmLeaveApplicationController@hrmLeaveAvailableSearch');
Route::get('/hrm/onleave/reports','HRM\HrmLeaveApplicationController@hrmOnLeaveReports');
Route::get('/hrm/onleave/reports/search','HRM\HrmLeaveApplicationController@hrmOnLeaveReportsSearch');

# Call center application
Route::resource('/slot/assing/employee','HRM\CallCenter\SlotAssignEmployeeController');

});