<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// RestAPI Routes

// Route::get( '/get-current-stock', 'Inventory\RestAPIController@getCurrentStock');
Route::get('get-current-stock', array('uses' => 'Inventory\RestAPIController@getCurrentStock'));

$router->group( ['middleware' => 'auth'] , function($router) {
// Inventory Dashboard Setup
Route::resource( '/d2lmis-control-panel', 'Inventory\InventoryDashboardController' );

// Supplier adddress
Route::get( '/supplier/address', 'Inventory\Settings\ProductSetupController@supplierAddress');

// Supplier adddress
Route::get( '/product/category/show', 'Inventory\Settings\ProductSetupController@productCategoryShow');

// Inventory Receive  Route
Route::post( '/product-receive', 'Inventory\ProductReceiveController@index' );
Route::resource( '/receive', 'Inventory\ProductReceiveController' );

Route::post( '/product/approve/disapprove', 'Inventory\ProductReceiveController@productAppoveDisapprove' );
Route::get( '/receive/approved/products', 'Inventory\ProductReceiveController@approvedProducts' );
Route::get( '/receive/disapproved/products', 'Inventory\ProductReceiveController@disApprovedProducts' );
Route::PUT( '/update/storesdetails/products/{id}', 'Inventory\ProductReceiveController@storeDetailsProducts' );
Route::get( '/local/purchase', 'Inventory\ProductReceiveController@localPurchase');
// Inventory New Requisition
Route::get( '/create-new-requisition', 'Inventory\ProductRequisitionMgt@index');
Route::post( '/requisition', 'Inventory\ProductRequisitionMgt@store');
Route::post( '/sale/requisition', 'Inventory\ProductRequisitionMgt@saleInsert');
Route::get( '/requisition/approval', 'Inventory\ProductRequisitionMgt@show');
Route::get('/requisition/approval/{id}',['as'=>'requisition.approval', 'uses'=>'Inventory\ProductRequisitionMgt@update']);
Route::get( '/requisition/approved/all', 'Inventory\ProductRequisitionMgt@approvedRequisition');
Route::get( '/requisition/disapproved/all', 'Inventory\ProductRequisitionMgt@disApprovedRequisition');


Route::resource('/supplier', 'Inventory\SupplierController');

# Adjustment 
Route::get( '/adjustment', 'Inventory\AdjustmentManagementController@index');
# Indent 
Route::get( '/create/indent', 'Inventory\IndentManagementController@index');
# Inventory Logistics Settings
Route::resource('/logistics/setting', 'Inventory\LogisticsSettingController');

# Inventory Current Stock
Route::get( '/current-stock', 'Inventory\ProductReceiveController@currentStock');

# Inventory Stock Return 
Route::resource( '/stock/return', 'Inventory\StockReturnController');
Route::get( '/stocks/return/disapproved', 'Inventory\StockReturnController@stockDisapproved');
Route::get( '/stocks/return/approved', 'Inventory\StockReturnController@stockApproved');

# Inventory Damage Stock Return 
Route::resource( '/damage/stock', 'Inventory\DamageStockReturnController');

// Transaction Type
Route::resource( '/transaction/type', 'Inventory\Settings\TransactionTypeController');
Route::get( '/master/transaction', 'Inventory\Settings\transactionTypeController@masterTransection');
Route::get( '/master/transection/search/type', 'Inventory\Settings\transactionTypeController@masterTransectionSearchType');
Route::get( '/master/summary/reports', 'Inventory\Settings\transactionTypeController@masterSummaryReports');
Route::get( '/master/summary/reports/search', 'Inventory\Settings\transactionTypeController@masterSummaryReportsSearch');
#All requisition
Route::get( '/all/requisitions/', 'Inventory\ProductRequisitionMgt@allRequisitions');
// setting Options
// product Category
Route::resource( '/new/product/setup', 'Inventory\Settings\ProductSetupController');

// Product Category
Route::resource( '/product/category', 'Inventory\Settings\productCategoryController');

// Product Category
Route::resource( '/product/unit', 'Inventory\Settings\ProductsUnitsController');

// Manufacturer
Route::resource( '/manufacturer', 'Inventory\Settings\ManufacturerController');

// Product Availible
Route::get( '/product/availible', 'Inventory\InventoryDashboardController@productAvaiblibleSearch');
// Barcode Generator
Route::get( '/barcode-generator', 'Inventory\InventoryDashboardController@barcodeGenerator');


// Barcode Generator
Route::get( '/sales/reports', 'Inventory\ProductRequisitionMgt@salesReports');
Route::get( '/due/sales/reports', 'Inventory\ProductRequisitionMgt@dueSalesReports');

// Master Facility
Route::get( '/master/facility', 'Inventory\FacilityInformationManagementController@masterFacilityList');
Route::get( '/union', 'Inventory\FacilityInformationManagementController@unionList');
Route::get( '/upazila', 'Inventory\FacilityInformationManagementController@upazilaList');
Route::get( '/district', 'Inventory\FacilityInformationManagementController@districtList');
Route::get( '/division', 'Inventory\FacilityInformationManagementController@divisionList');

});