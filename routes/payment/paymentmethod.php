<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/info', function(){
	return Hash::make('123456');
});

Route::get('/billsheet/{token}' , 'PaymentApi\FosterController@currentMonthBill');
Route::post('/billcollection' , 'PaymentApi\FosterController@currentMonthBillCollection');
Route::post('/bill/received' , 'PaymentApi\FosterController@billRecevied');