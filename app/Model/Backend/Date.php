<?php

namespace App\Model\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;

class Date extends Model
{
    protected $table      = 'date_setup';
    public    $timestamp  = true;

    /**
     *  Get current month date range
     * @return [type] [description]
     */
    public static function monthYear() {

    	$date       = Date::whereNull('deleted_at')->orderBy('id', 'desc')->first();
        if(isset($date->start_day)) {

            $startDay   = $date->start_day;
            $endDay     = $date->end_day; 

        } else {

            $startDay   = Date('d');
            $endDay     = Date('d');  
        }
      	
      // var_dump($startDay, $endDay); die();

        $today      = date('d');
        $preMonth   = date('F', strtotime('-1 month')); //  January
        $nextMonth 	= date("F", mktime(0, 0, 0, date("n") + 1, 1) ); //  February
        $month      = $currentMonth = date('F'); //January
        // var_dump($month); die();
        // if ( $today >= $startDay  ) {
        // if ( $today >= $endDay ) {
        if ( $today > $endDay ) {

            $currentMonth = $nextMonth;
            $preMonth 	  = $month;  
        } 
        // var_dump($today,$currentMonth, $startDay, $endDay); echo '<br>';

        $preYear     = date('Y', strtotime('-1 year'));//  2015
        $nextYear    = date('Y', strtotime('+1 year'));//  2017
        $currentYear = date('Y'); //2016

        if ( ($today <= $startDay) && ( $month == 'December' ) ) {
            
            $preYear     = $currentYear;
            $currentYear = $nextYear;
            $preMonth     =  $currentMonth;
            $currentMonth =  $nextMonth;

        } else if ( ($today > $startDay && $today <= $endDay) && ( $month == 'December' ) ) {

            $preYear    = $currentYear;
            $currentYear = $nextYear;
        
        } else if( ($today > $endDay) && ($month == "December") ) {
            
            $billYear = $preYear    = $currentYear;
            $currentYear = $nextYear;

        }

        #Don't delete this , it's only for bill generate
        
        if ($month == 'December') {
            $billYear = $preYear;
        } else {
            $billYear = $currentYear;
        }

        # Don't delete this, it's only for newline bill collection
        if ($month == 'January') {
            $nlbillYear = $preYear;
        } else {
            $nlbillYear = $currentYear;
        }
        
        $monthNumber = date("m",strtotime($currentMonth));

        // var_dump( $currentMonth .','.$currentYear.','.$preMonth.','.$preYear .','.$startDay .','.$endDay.','.$billYear.','.$nlbillYear.','.$monthNumber ); die();
        return $currentMonth .','.$currentYear.','.$preMonth.','.$preYear .','.$startDay .','.$endDay.','.$billYear.','.$nlbillYear.','.$monthNumber;
    }

    public static function monthToNumber($monthName) {

        return date("m",strtotime($monthName));
        
    }

    /**
     * [This Month Start day End day ]
     * @param [get] $status [List for this month conntion list]
     */
    public static function StartdayEndday($status) {

        $monthYear = explode(',', Date::monthYear() );
        $endMonth  = $month = date('m',strtotime("1 $monthYear[0] ")); 
        $month     = date("m", mktime(0, 0, 0, date("m")  ));

        $year      = $monthYear[1];

        if ($month == 0) {

            $currentMonth = 12;
            $currentYear  = $year - 1;

        } else {

            if($monthYear[0] == Date('F')) {

                if ($month != 1) {
                    
                    $currentMonth = $month;  # For March 01 to March 31th
                    // $currentMonth = $month -1; # for march 25 to April 25 monthly cycle

                    $currentYear  = $year;

                    // var_dump($month, $currentMonth); die();

                } else {

                    $currentMonth =12;
                    // $currentMonth = $month;

                    $currentYear  = $year - 1;
                }

            } else {
            
                $currentMonth = $month;
                $currentYear  = $year;
                // $currentYear  = $year - 1;
                 // var_dump($month, $year);

            }

            
        }

        $startDate = ($monthYear[4] < 10 ) ? 0 . $monthYear[4] : $monthYear[4];

        /** 
         *  For March 25 to April 25 billing cycle
         * 
         */

        // $startDay   =  $currentYear . '-'. $currentMonth . '-'. $startDate;      
        // $endDay     =  $monthYear[1] . '-'. $endMonth . '-'. $monthYear[4]; 

        /** 
         *  For March 01 to March 31 billing cycle
         * 
         */

        $currentMonth    = date('m', strtotime($monthYear[0]));
        $lastDayOfMonth  = cal_days_in_month(CAL_GREGORIAN, $currentMonth, $monthYear[1]);


        $startDay   =  $currentYear . '-'. $currentMonth . '-'. $startDate;      
        $endDay     =  $monthYear[1] . '-'. $endMonth . '-'. $lastDayOfMonth; 


        // var_dump($startDay, $endDay ); die();
       
        if($status == 0) {

            return $startDay;

        } else  {

            return $endDay;

        }
        
        
    }

    public static function thisMonthDays($thisYear) {

        $firstDay = "01";
        $endDay   = $totalDays = cal_days_in_month(CAL_GREGORIAN, date('m'), $thisYear);

        $days =  $firstDay .','.$endDay.','.$totalDays;
        return explode(',', $days);

    }

    /**
     *  Last date of payment
     */
    public static function LastDateOfPayment() {

        $date  = Date::select('block_for_payment')->whereNull('deleted_at')->orderBy('id', 'desc')->first();
        
        if (!empty($date)) {

            return $date->block_for_payment;
        }

        return 0;

    }


    public static function checkDateValidity() {

        $string = "http://validity.isperp.org";

        // $data = (string)"104 116 116 112 58 47 47 118 97 108 105 100 105 116 121 46 105 115 112 101 114 112 46 111 114 103";
        // $result = Date::asciiToAlphabet($data);
        // var_dump($result); die();

        $entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
        $replacements = array('!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]");
        
        $result= str_replace($entities, $replacements, urlencode($string));

var_dump($result); die();
    }


    private static function asciiToAlphabet($input) {

        if(!isset($input)) {
            return false;
        } else {
            return chr($input);
        }
    }


}

