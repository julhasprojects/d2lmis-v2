<?php

namespace App\Model\Backend;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'company_info';
    public $timestamps = true;
}
