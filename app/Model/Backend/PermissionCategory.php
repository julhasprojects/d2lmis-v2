<?php

namespace App\Model\Backend;

use Illuminate\Database\Eloquent\Model;

class PermissionCategory extends Model
{
     protected $table = 'permission_category';
}
