<?php

namespace App\Model\Backend;

use Illuminate\Database\Eloquent\Model;

class PermissionRole extends Model
{
    protected $table = 'permission_role';
    public $timestamp = true;
    
    protected $rolePermissions = ['permission_id' => 'array'];

    public function role()
    {
        return $this->hasMany(Role::class);
    }

    public function permission()
    {
        return $this->hasMany(Permission::class);
    }
}
