<?php

namespace App\Model\Hrm;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    protected $table = 'employees';
    public $timestamps = true;

    public function workDuration($employeeID) {
    	
		$employee = Employees::select('joiningDate','exit_date')->where('employeeID','=',$employeeID)->first();
		$lastDate   =   ($employee->exit_date==NULL)?date('Y-m-d'):$employee->exit_date;

		$diff = date_diff(date_create($employee->joiningDate),date_create($lastDate));

		$difference = ($diff->y==0)?null:$diff->y.' year ';
		$difference .= ($diff->m==0)?null:$diff->m.' month ';
		$difference .= ($diff->d==0)?null:$diff->d.' day ';

		return $difference;

	}

	public static function employeesInfo() {
		
		$employeeInfo = [];

		$employees  = Employees::select('employeeID','fullName')->get()->toArray();
		foreach ($employees as $key => $info) {
			$employeeInfo[$info['employeeID']] = $info['fullName'];
		}
		
		return $employeeInfo;
	}
}
