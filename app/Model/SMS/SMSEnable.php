<?php

namespace App\Model\SMS;

use Illuminate\Database\Eloquent\Model;

class SMSEnable extends Model
{
    protected $table = 'sms_enable';
    public $timestamp = true;
}
