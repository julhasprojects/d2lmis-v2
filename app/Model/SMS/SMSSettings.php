<?php

namespace App\Model\SMS;

use Illuminate\Database\Eloquent\Model;

class SMSSettings extends Model
{
    protected $table = 'sms_settings';
    public $timestamp = true;
}
