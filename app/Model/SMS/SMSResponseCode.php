<?php

namespace App\Model\SMS;

use Illuminate\Database\Eloquent\Model;

class SMSResponseCode extends Model
{
    protected $table = 'sms_response';
    public $timestamp = true;
}
