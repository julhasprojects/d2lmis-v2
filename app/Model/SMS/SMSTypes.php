<?php

namespace App\Model\SMS;

use Illuminate\Database\Eloquent\Model;

class SMSTypes extends Model
{
    protected $table = 'sms_type';
    public $timestamp = true;
}
