<?php

namespace App\Model\SMS;

use Illuminate\Database\Eloquent\Model;

class SMSHistory extends Model
{
    protected $table = 'sms_history';
    public $timestamp = true;
}
