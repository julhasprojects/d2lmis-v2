<?php

namespace App\Model\Frontpanel;

use Illuminate\Database\Eloquent\Model;
use App\Model\Frontpanel\Region;
use DB;
use Session;
use Illuminate\Support\Facades\Cache;

class Empoffice extends Model
{

    # Branch Region List
    public static function branchRegion() {
        
        $branchId   = Session::get('branchId');

        $regionList = Region::where('region.branch_id', $branchId )
            ->get();

        return $regionList;
    
    }

    #employeeCategoryList
    public static function employeeCatgoryList($category) {

        $branchId = Session::get('branchId');

        # 1. Technichan Field and assign by   4. Technicain 5. Assing By
        if($category == 1 ) {

            Cache::pull('employee');
            $employee = Cache::remember('employee','1440',  function () {
                return DB::table('employees')
                ->leftjoin('designation','employees.designation','=','designation.id')
                ->leftjoin('branch','employees.branch_id','=','branch.id')
                ->leftjoin('department','designation.deptID','=','department.id')
                ->where('employees.branch_id', Session::get('branchId') )
                ->whereIn('department.id',[10,11,8,5])
                ->whereNotIn('employees.employeeID',[999])
                ->where('employees.status',1)
                ->orderBy('employees.fullName','asc')
                ->get();
            });
        // return view('backend.cach.cach-employee',compact('employees'));
            /*$employee = DB::table('employees')
                ->leftjoin('designation','employees.designation','=','designation.id')
                ->leftjoin('branch','employees.branch_id','=','branch.id')
                ->leftjoin('department','designation.deptID','=','department.id')
                ->where('employees.branch_id', $branchId )
                ->whereIn('department.id',[10,11,8,5])
                ->whereNotIn('employees.employeeID',[999])
                ->where('employees.status',1)
                ->orderBy('employees.fullName','asc')
                ->get();*/

        # 2. Billing
        } elseif( $category == 2 ) {

            Cache::pull('employee');
            $employee = Cache::remember('employee','1440',  function () {
            return DB::table('employees')
                ->leftjoin('designation','employees.designation','=','designation.id')
                ->leftjoin('branch','employees.branch_id','=','branch.id')
                ->leftjoin('department','designation.deptID','=','department.id')
                ->where('employees.branch_id', Session::get('branchId'))
                ->whereIn('department.id',[4])
                ->whereNotIn('employees.employeeID',[999])
                ->where('employees.status',1)
                ->orderBy('employees.fullName','asc')
                ->get();
            });

        # 3. Request Recived By
        } elseif ( $category == 3 ) {
            Cache::pull('employee');
            $employee = Cache::remember('employee','1440',  function () {
                return DB::table('employees')
                    ->leftjoin('designation','employees.designation','=','designation.id')
                    ->leftjoin('branch','employees.branch_id','=','branch.id')
                    ->leftjoin('department','designation.deptID','=','department.id')
                    ->where('employees.branch_id',  Session::get('branchId') )
                    ->whereIn('department.id',[6,8])
                    ->whereNotIn('employees.employeeID',[999])
                    ->where('employees.status',1)
                    ->orderBy('employees.fullName','asc')
                    ->get();
            });
        # 4. Technicain
        } elseif( $category == 4 ) {
            Cache::pull('employee');
            $employee = Cache::remember('employee','1440',  function () {
               return DB::table('employees')
                    ->leftjoin('designation','employees.designation','=','designation.id')
                    ->leftjoin('branch','employees.branch_id','=','branch.id')
                    ->leftjoin('department','designation.deptID','=','department.id')
                    ->where('employees.branch_id', Session::get('branchId') )
                    ->whereIn('department.id',[5,8,9,10,11])
                    ->whereNotIn('employees.employeeID',[999])
                    ->where('employees.status',1)
                    ->orderBy('employees.fullName','asc')
                    ->get();
                });
        # 5. Assign By
        } elseif( $category == 5) {
            Cache::pull('employee');
            $employee = Cache::remember('employee','1440',  function () {
            return DB::table('employees')
                ->leftjoin('designation','employees.designation','=','designation.id')
                ->leftjoin('branch','employees.branch_id','=','branch.id')
                ->leftjoin('department','designation.deptID','=','department.id')
                ->where('employees.branch_id',  Session::get('branchId') )
                ->whereIn('department.id',[10,8,9])
                ->whereNotIn('employees.employeeID',[999])
                ->where('employees.status',1)
                ->orderBy('employees.fullName','asc')
                ->get();
            });
        }
        
       return $employee;
    }


    public static function designationCategoryEmployer($dgid) {
        
        $branchId = Session::get('branchId');
        $value    = explode(',',$dgid);
       
        $employee = DB::table('employees')
            ->leftjoin('designation','employees.designation','=','designation.id')
            ->leftjoin('branch','employees.branch_id','=','branch.id')
            ->leftjoin('department','designation.deptID','=','department.id')
            ->where('employees.branch_id', $branchId )
            ->whereIn('department.id',$value)
            ->whereNotIn('employees.employeeID',[999])
            ->where('employees.status',1)
            ->get();
       
       return $employee;
    
    }

    public static function allEmployer() {
        
        $branchId = Session::get('branchId');

        $employee =  DB::table('employees')
            ->leftjoin('designation','employees.designation','=','designation.id')
            ->leftjoin('branch','employees.branch_id','=','branch.id')
            ->leftjoin('department','designation.deptID','=','department.id')
            ->where('employees.branch_id', $branchId )
            ->whereNotIn('employees.employeeID',[999])
            ->where('employees.status',1)
            ->get();

        return $employee;
      
    }

    public static function empBranch($id) {
       
        $employee =  DB::table('employees')
            ->leftjoin('designation','employees.designation','=','designation.id')
            ->leftjoin('branch','employees.branch_id','=','branch.id')
            ->leftjoin('department','designation.deptID','=','department.id')
            ->where('employees.employeeID', $id )
            ->first();

    	return $employee;
    
    }

    public static function branchAllEmpList() {

    	$branchId     = Session::get('branchId');
    	$branchAllEmp =  DB::table('employees')
            ->leftjoin('designation','employees.designation','=','designation.id')
            ->leftjoin('branch','employees.branch_id','=','branch.id')
            ->leftjoin('department','designation.deptID','=','department.id')
            ->where('employees.branch_id', $branchId )
            ->whereNotIn('employees.employeeID',[999])
            ->get();
        
        $allEmp = [];

    	foreach ($branchAllEmp as $key => $value) {

            $allEmp[] = $value->employeeID;

        }

        return $allEmp;

    }

    public static function branchAllEmployer() {

        $branchId     = Session::get('branchId');

        $branchAllEmp =  DB::table('employees')
            ->leftjoin('designation','employees.designation','=','designation.id')
            ->leftjoin('branch','employees.branch_id','=','branch.id')
            ->leftjoin('department','designation.deptID','=','department.id')
            ->where('employees.branch_id', $branchId )
            ->get();

        foreach ($branchAllEmp as $key => $value) {

            $allEmp[] = $value->employeeID;

        }
        
        return  implode(",", $allEmp);
    
    }
}
