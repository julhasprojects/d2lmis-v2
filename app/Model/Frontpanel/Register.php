<?php

namespace App\Model\Frontpanel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Register extends Model
{

    use SoftDeletes;
    protected $table='register_info';

    public $timestamps=true;
    protected $dates = ['deleted_at'];
}