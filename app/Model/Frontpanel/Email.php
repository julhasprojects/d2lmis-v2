<?php

namespace App\Model\Frontpanel;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $table = 'email_enable';
    public $timestamp = true;
}
