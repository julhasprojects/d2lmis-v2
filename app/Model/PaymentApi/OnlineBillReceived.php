<?php

namespace App\Model\PaymentApi;

use Illuminate\Database\Eloquent\Model;

class OnlineBillReceived extends Model
{
    protected $table = 'online_bill_received';
    public $timestamps = true;

    public static function existingBill($month, $year) {
		
        $existing  = [];
    	
        $clientlist = OnlineBillReceived::where('month', $month)->where('year', $year)->whereNull('deleted_at')->get();

    	foreach ($clientlist as $key => $value) {
    		$existing[] = $value->newconid; 
    	}

    	return $existing;
    }

}
