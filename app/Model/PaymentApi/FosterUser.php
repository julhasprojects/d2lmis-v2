<?php

namespace App\Model\PaymentApi;

use Illuminate\Database\Eloquent\Model;

class FosterUser extends Model
{
    protected $table = 'users';
    public $timestamps = true;
}
