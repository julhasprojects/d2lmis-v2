<?php

namespace App\Model\Inventory;

use Illuminate\Database\Eloquent\Model;

class UpazilaList extends Model
{
    protected $table = 'upazila';
    public $timestamps = true;
}
