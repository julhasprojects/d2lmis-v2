<?php

namespace App\Model\Inventory;

use Illuminate\Database\Eloquent\Model;

class InventoryEnable extends Model
{
    protected $table = 'inv_settings';
    public $timestamps = true;
}
