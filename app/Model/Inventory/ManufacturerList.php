<?php

namespace App\Model\Inventory;

use Illuminate\Database\Eloquent\Model;

class ManufacturerList extends Model
{
   protected $table = 'inv_manufacturer';
    public $timestamps = true;
}
