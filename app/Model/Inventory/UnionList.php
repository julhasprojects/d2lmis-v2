<?php

namespace App\Model\Inventory;

use Illuminate\Database\Eloquent\Model;

class UnionList extends Model
{
    protected $table = 'union';
    public $timestamps = true;
}
