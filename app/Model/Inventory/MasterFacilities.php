<?php

namespace App\Model\Inventory;

use Illuminate\Database\Eloquent\Model;

class MasterFacilities extends Model
{
    protected $table = 'master_facilities';
    public $timestamps = true;
}
