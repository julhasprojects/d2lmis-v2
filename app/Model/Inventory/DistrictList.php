<?php

namespace App\Model\Inventory;

use Illuminate\Database\Eloquent\Model;

class DistrictList extends Model
{
    protected $table = 'district';
    public $timestamps = true;
}
