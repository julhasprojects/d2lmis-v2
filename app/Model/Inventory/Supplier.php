<?php

namespace App\Model\Inventory;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $table = 'inv_supplier';
    public $timestamps = true;
}
