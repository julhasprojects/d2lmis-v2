<?php

namespace App\Model\Inventory;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'inv_productcategory';
    public $timestamps = true;
}
