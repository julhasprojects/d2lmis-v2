<?php

namespace App\Model\Inventory;

use Illuminate\Database\Eloquent\Model;

class DivisionList extends Model
{
    protected $table = 'division';
    public $timestamps = true;
}
