<?php

namespace App\Model\Inventory;

use Illuminate\Database\Eloquent\Model;

class StoreDetails extends Model
{
    protected $table = 'inv_storesdetails';
    public $timestamps = true;
}
