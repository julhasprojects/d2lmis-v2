<?php

namespace App\Model\Inventory;

use Illuminate\Database\Eloquent\Model;

class Store extends Model {
    protected $table = 'stores';
    public $timestamps = true;
}
