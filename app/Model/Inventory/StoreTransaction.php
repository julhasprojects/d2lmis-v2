<?php

namespace App\Model\Inventory;

use Illuminate\Database\Eloquent\Model;

class StoreTransaction extends Model
{
    protected $table = 'inv_storetransaction';
    public $timestamps = true;
}
