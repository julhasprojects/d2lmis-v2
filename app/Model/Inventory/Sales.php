<?php

namespace App\Model\Inventory;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    protected $table = 'inv_sales';
    public $timestamps = true;
}
