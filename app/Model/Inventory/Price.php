<?php

namespace App\Model\Inventory;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $table = 'inv_price';
    public $timestamps = true;
}
