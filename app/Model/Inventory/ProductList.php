<?php

namespace App\Model\Inventory;

use Illuminate\Database\Eloquent\Model;

class ProductList extends Model
{
    protected $table = 'inv_productlist';
    public $timestamps = true;
}
