<?php

namespace App\Model\Inventory;

use Illuminate\Database\Eloquent\Model;

class CustomerInfo extends Model
{
    protected $table = 'inv_customerinfo';
    public $timestamps = true;
}
