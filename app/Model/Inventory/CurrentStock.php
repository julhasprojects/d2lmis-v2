<?php

namespace App\Model\Inventory;

use Illuminate\Database\Eloquent\Model;

class CurrentStock extends Model
{
    protected $table = 'inv_currentstock';
    public $timestamps = true;
}
