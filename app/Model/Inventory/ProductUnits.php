<?php

namespace App\Model\Inventory;

use Illuminate\Database\Eloquent\Model;

class ProductUnits extends Model
{
    protected $table = 'inv_productunits';
    public $timestamps = true;
}
