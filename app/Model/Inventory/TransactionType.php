<?php

namespace App\Model\Inventory;

use Illuminate\Database\Eloquent\Model;

class TransactionType extends Model
{
    protected $table = 'inv_transactiontype';
    public $timestamps = true;
}
