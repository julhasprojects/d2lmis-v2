<?php

namespace App\Http\Controllers\Region;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Frontpanel\Region;
use App\Model\Frontpanel\Road;
use App\Model\Frontpanel\City;
use App\Model\Frontpanel\Empoffice;
use App\Model\Backend\Branch;
use Session;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class RegionSetupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {          

        $upazila_name = DB::table( 'upazila' ) 
            ->select(  'upazila_id', 'upazila_name' )
            ->get();                   

        $regionInfo = DB::table( 'region' ) 
            ->leftjoin('division', 'region.division_id', '=', 'division.division_id')
            ->leftjoin('district', 'region.district_id', '=', 'district.district_id')
            ->leftjoin('upazila', 'region.upazila_id', '=', 'upazila.upazila_id')
            ->leftjoin('employee_regions', 'region.region_id', '=', 'employee_regions.region_id')
            ->select(  'region.*' ,'division.division_name','district.district_name','upazila.upazila_name','employee_regions.emp_id')
            ->groupBy('region.region_id')
            ->orderBy('region.id','desc')
            ->paginate(300);
        
        $roadInfo = DB::table( 'road' ) 
            ->leftjoin('division', 'road.division_id', '=', 'division.division_id')
            ->leftjoin('district', 'road.district_id', '=', 'district.district_id')
            ->leftjoin('upazila', 'road.upazila_id', '=', 'upazila.upazila_id')
            ->leftjoin('region', 'road.region_id', '=', 'region.region_id')
            ->leftjoin('employee_regions', 'road.region_id', '=', 'employee_regions.region_id')
            ->select( 'road.*','region.region_name','road.id as road_id','division.division_name','district.district_name','upazila.upazila_name','employee_regions.emp_id')
            ->groupBy('road.id')
            ->get();

        $districtInfo = DB::table( 'district' )->get();
        $upazillInfo  = DB::table( 'upazila' )->get();

        return  $content = view( 'backend.region.region-all', [
            'region'       => Region::all(),
            'regionInfo'   => $regionInfo,
            'roadsInfo'    => $roadInfo,
            'road_name'    => Road::all(),
            'upazila_name' => $upazila_name,
            'employee'     => Empoffice::allEmployer(),
            'branchInfo'   => Branch::all(),
            'cityList'     => City::all(),
            'districtInfo' => $districtInfo,
            'upazillInfo'  => $upazillInfo,
            
        ]);
    }

   
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        if(!empty($request->input('road'))) {
            
            $userData = [
                'region'  => $request->input('region'),
                'road'    => $request->input('road')
            ];

            $rules   =  [      
                'region' =>'required|min:1|numeric',
                'road'   =>'required|min:2',
            ];

            $validation = Validator::make($userData,$rules); 

            if($validation->fails()) {

                return  Redirect::to('region/setup')->withInput()->withErrors($validation);

            } else {
                
                // Region existing check
                $regionExist = DB::table('road')->select('id')
                    ->where('region_id', $request->input('region') )
                    ->where('road', $request->input('road') )
                    ->count();
    
                if( $regionExist > 0 ) {

                    Session::put( 'invalid', 2 );
                    Session::flash('message','Sorry! This road name existed.');

                } else {
                    #validation section
                    $this->validate($request, [
                        'city'          => 'required',
                        'upzilla'       => 'required',
                        'districtInfo'  => 'required',
                        'region'        => 'required',
                        'road'          => 'required',
                    ]);

                    $roadSetup = new Road();
                    $roadSetup->country_id   = 1; 
                    $roadSetup->division_id  = $request->input('city'); 
                    $roadSetup->district_id  = $request->input('districtInfo'); 
                    $roadSetup->upazila_id   = $request->input('upzilla'); 
                    $roadSetup->region_id    = $request->input('region'); 
                    $roadSetup->road         = $request->input('road');  

                    if($roadSetup->save()) {

                        Session::put( 'valid', 1 );
                        Session::flash('message','New road has added successfully');

                    } else {

                        Session::put( 'invalid', 2 );
                        Session::flash('message','We can not take your request. Try again!.');
                        
                    }
                    return Redirect('region/setup')->with('status', '2');
                }
            }

        } else {

            $userData =  [
                'region'    => $request->input('region')
            ];
            $rules  =[  
                'region'  =>'required',
            ];
            $validation = Validator::make($userData,$rules); 

            if($validation->fails()) {

                return  Redirect::to('region/setup')->withInput()->withErrors($validation);

            } else {

                $regionExist = DB::table('region')->select('region_id')->where('region_name', $request->input('region'))->count();
                
                if( $regionExist > 0 ) {

                    Session::put( 'invalid', 2 );
                    Session::flash('message','Sorry! This region name has already exist.');

                    return back();

                } else {

                    $regionInfo = Region::orderBy('id', 'desc')->first();

                    if(!empty( $regionInfo)) {

                        $regionid = $regionInfo->region_id;
                        $regionid += 1;

                    } else {
                        $regionid = 1;
                    }
                    
                     #validation section
                    $this->validate($request, [
                        'city'           => 'required',
                        'upzilla'        => 'required',
                        'districtInfo'   => 'required',
                        'branch'         => 'required',
                    ]);

                    $regionSetup = new Region();
                    $regionSetup->country_id  = 1; 
                    $regionSetup->division_id   = $request->input('city'); 
                    $regionSetup->district_id   = $request->input('districtInfo'); 
                    $regionSetup->upazila_id    = $request->input('upzilla'); 
                    $regionSetup->region_id   = $regionid; 
                    $regionSetup->region_name = $request->input('region'); 
                    $regionSetup->branch_id   = \Input::get('branch'); 

                    if($regionSetup->save()) {

                        Session::put( 'valid', 1 );
                        Session::flash('message','New region has created successfully.');

                    } else {

                        Session::put( 'invalid', 2 );
                        Session::flash('message','We can not take your request. Try again!.');

                    }
                }
                
            }

        }
      
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $regionInfo = DB::table( 'region' ) 
            ->leftjoin('division', 'region.division_id', '=', 'division.division_id')
            ->leftjoin('district', 'region.district_id', '=', 'district.district_id')
            ->leftjoin('upazila', 'region.upazila_id', '=', 'upazila.upazila_id')
            ->leftjoin('employee_regions', 'region.region_id', '=', 'employee_regions.region_id')
            ->select(  'region.*' ,'division.division_name','district.district_name','upazila.upazila_name','employee_regions.emp_id')
            ->where('region.region_id',$id)
            ->groupBy('region.region_id')
            ->orderBy('region.id','desc')
            ->first();

        $districtInfo = DB::table( 'district' )->get();
        $upazillInfo  = DB::table( 'upazila' )->get();


        $content = view( 'backend.region.edit-region',[
            'regionInfo' => $regionInfo,
            'road_name'  => Road::all(),
            'region'     => Region::all(),
            'cityList'     => City::all(),
            'districtInfo' => $districtInfo,
            'upazillInfo'  => $upazillInfo,
            'branchInfo' => Branch::all(),
           
        ]);

        return view( 'backend.index')->with( 'content_area', $content );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id) {

      
        #validation section
        $this->validate($request, [
            'city'          => 'required',
            'upzilla'       => 'required',
            'districtInfo'  => 'required',
            'region'         => 'required',
        ]);

        $regionUpdate = Region::where('region_id',$id)->first(); 
     
        $regionUpdate->division_id  = $request->input('city'); 
        $regionUpdate->district_id  = $request->input('districtInfo'); 
        $regionUpdate->upazila_id   = $request->input('upzilla');     
        $regionUpdate->region_name  = $request->input( 'region' );

        if($regionUpdate->save()) {

            Session::put( 'valid', 1 );
            Session::flash( 'message', 'Your information has update successfully' );

        } else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to update your  information.');

        }

        return Redirect('region/setup');
    }

    public function roadEdit($id) {

       $roadInfo = DB::table( 'road' ) 
            ->leftjoin('division', 'road.division_id', '=', 'division.division_id')
            ->leftjoin('district', 'road.district_id', '=', 'district.district_id')
            ->leftjoin('upazila', 'road.upazila_id', '=', 'upazila.upazila_id')
            ->leftjoin('employee_regions', 'road.region_id', '=', 'employee_regions.region_id')
            ->select( 'road.*','road.id as road_id','division.division_name','district.district_name','upazila.upazila_name','employee_regions.emp_id')
            ->where('road.id',$id)
            ->first();

        $districtInfo = DB::table( 'district' )->get();
        $upazillInfo  = DB::table( 'upazila' )->get();

        $content = view( 'backend.region.edit-road',[
            'roadInfo'    => $roadInfo,
            'road_name'    => Road::all(),
            'region'       => Region::all(),
            'branchInfo'   => Branch::all(),
            'cityList'     => City::all(),
            'districtInfo' => $districtInfo,
            'upazillInfo'  => $upazillInfo,
           
        ]);

        return view( 'backend.index')->with( 'content_area', $content );
    }

    public function roadUpdate(Request $request,$id) {

        #validation section
        $this->validate($request, [
            'city'          => 'required',
            'upzilla'       => 'required',
            'districtInfo'  => 'required',
            'regionupdate'  => 'required',
            'road'          => 'required',
        ]);

        $roadUpdates = Road::find($id);     
      
        $roadUpdates->division_id  = $request->input('city'); 
        $roadUpdates->district_id  = $request->input('districtInfo'); 
        $roadUpdates->upazila_id   = $request->input('upzilla');   
        $roadUpdates->region_id    = $request->input( 'regionupdate' );
        $roadUpdates->road         = $request->input( 'road' );

        if( $roadUpdates->save()) {

            Session::put( 'valid', 1 );
            Session::flash( 'message', 'Your information has update successfully' );

        } else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to update your information.');

        }

        return Redirect('region/setup')->with('status', '2');
    }

    
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req) {

        $status = \Input::get('status');
        
        if($status == 1) {
            Road::where('id', $req->id)->delete();   
        } else {
            Region::where('region_id', $req->id)->delete();
            Road::where('region_id', $req->id)->delete();   
        }
        
        return response()->json();    
        
    }

    public function masterRegionReports() {   

        
        $region_name = DB::table( 'region' ) 
            ->select(  'region_id', 'region_name' )
            ->get();

        $road_name = DB::table( 'road' ) 
             ->select(  'id', 'road' )
             ->get();                   

        $upazila_name = DB::table( 'upazila' ) 
            ->select(  'upazila_id', 'upazila_name' )
            ->get();                   

        $region = DB::table( 'region' ) 
            ->join('division', 'region.division_id', '=', 'division.division_id')
            ->join('district', 'region.district_id', '=', 'district.district_id')
            ->join('upazila', 'region.upazila_id', '=', 'upazila.upazila_id')
            ->join('road', 'region.region_id', '=', 'road.region_id')
            ->join('employee_regions', 'region.region_id', '=', 'employee_regions.region_id')
            ->select(  'region.*' ,'division.division_name','district.district_name','upazila.upazila_name','road.road','employee_regions.emp_id')
            ->orderBy('region.id','desc')
            ->paginate(100);

        $content = view( 'backend.region.master-region-all' ,[
            'employee'     => Empoffice::allEmployer(),
            'region_name'  => $region_name,
            'road_name'    => $road_name,
            'upazila_name' => $upazila_name,
            'region'       => $region
        ]);

        return view( 'backend.index')->with( 'content_area', $content );
    }

    public function masterRegionSearchData(  ) {

        $regionId = \Input::get('mastersearchregion');

        $road     = Road::where('region_id', $regionId)->get();

        return view( 'backend.region.region-wise-road',[
           
            'road'   => $road,
        ]) ;

    }    

    public function masterRoadSearchData() {

        $mastersearchregion  = \Input::get('mastersearchregion');
        $mastersearchroad    = \Input::get('mastersearchroad');
        $mastersearchupazila = \Input::get('mastersearchupazila');

        if(!empty($mastersearchroad)) {

            $region = DB::table( 'region' ) 
                ->join('division', 'region.division_id', '=', 'division.division_id')
                ->join('district', 'region.district_id', '=', 'district.district_id')
                ->join('upazila', 'region.upazila_id', '=', 'upazila.upazila_id')
                ->join('road', 'region.region_id', '=', 'road.region_id')
                ->join('employee_regions', 'region.region_id', '=', 'employee_regions.region_id')
                ->select(  'region.*' ,'division.division_name','district.district_name','upazila.upazila_name','road.road','employee_regions.emp_id')
                ->orderBy('region.id','desc')
                ->where('road.id', $mastersearchroad) 
                ->get();

        }  else {

            $region = DB::table( 'region' ) 
                ->join('division', 'region.division_id', '=', 'division.division_id')
                ->join('district', 'region.district_id', '=', 'district.district_id')
                ->join('upazila', 'region.upazila_id', '=', 'upazila.upazila_id')
                ->join('road', 'region.region_id', '=', 'road.region_id')
                ->join('employee_regions', 'region.region_id', '=', 'employee_regions.region_id')
                ->select(  'region.*' ,'division.division_name','district.district_name','upazila.upazila_name','road.road','employee_regions.emp_id')
                ->orderBy('region.id','desc')
                ->where('region.id',$mastersearchregion) 
                ->get();
        }
        return view( 'backend.region.ajax-search-region-all',[
            'employee' => Empoffice::allEmployer(),
            'region'   => $region,
        ]) ;

    } 

    public function masterRoadSearch() {

        $mastersearchregion  = \Input::get('regionWiseSearch');
       

   
        $region = DB::table( 'region' ) 
            ->leftjoin('division', 'region.division_id', '=', 'division.division_id')
            ->leftjoin('district', 'region.district_id', '=', 'district.district_id')
            ->leftjoin('upazila', 'region.upazila_id', '=', 'upazila.upazila_id')
            ->leftjoin('road', 'region.region_id', '=', 'road.region_id')
            ->leftjoin('employee_regions', 'region.region_id', '=', 'employee_regions.region_id')
            ->select(  'region.*' ,'road.id as road_id','division.division_name','district.district_name','upazila.upazila_name','road.road','employee_regions.emp_id')
            ->orderBy('region.id','desc')
            ->where('region.id',$mastersearchregion) 
            ->get();
        
        return view( 'backend.region.ajax-search-road-all',[
            'employee' => Empoffice::allEmployer(),
            'roadInfo'   => $region,
        ]) ;

    } 

    public function masterupazilaSearchData( $find ) {
   
        $region = DB::table( 'region' ) 
            ->join('division', 'region.division_id', '=', 'division.division_id')
            ->join('district', 'region.district_id', '=', 'district.district_id')
            ->join('upazila', 'region.upazila_id', '=', 'upazila.upazila_id')
            ->join('road', 'region.region_id', '=', 'road.region_id')
            ->join('employee_regions', 'region.region_id', '=', 'employee_regions.region_id')
            ->select(  'region.*' ,'division.division_name','district.district_name','upazila.upazila_name','road.road','employee_regions.emp_id')
            ->orderBy('region.id','desc')
            ->where('upazila.upazila_id', $find) 
            ->get();

        return view( 'backend.region.ajax-search-region-all',[
            'employee' => Empoffice::allEmployer(),
            'region'   => $region,
        ]) ;

    }

     public function regionSearch() {

        $ID = \Input::get('upazilasearch');

        $districtInfo = Region::where('upazila_id',$ID)->get();
        echo '<option value=""> Select Region</option>';
        foreach ($districtInfo as $value) {
            echo '<option value= '. $value->region_id.'> '.$value->region_name.' </option>' ;
        }
    }
}
