<?php

namespace App\Http\Controllers\Region;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Frontpanel\Region;
use App\Model\Frontpanel\City;
use App\Model\Frontpanel\District;
use App\Model\Frontpanel\Country;
use Session;
class DistrictSetupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $districtList = District::leftjoin('division','district.division_id','=','division.division_id')
            ->leftjoin('country','district.country_id','=','country.country_id')
            ->get();

        return view( 'backend.region.district.index', [
            'region'       => Region::all(),
            'cityList'     => City::all(),
            'countryList'  => Country::all(),
            'districtList' => $districtList,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        #validation section
        $this->validate($request, [
            'city'     => 'required',
            'district' => 'required',
        ]);

        $district = District::orderBy('district_id','desc')->first();
        
        if(!empty($district)) {

            $districtID = $district->district_id + 1;

        } else {

           $districtID  = 1;

        }
      

        $districtInfo = New District();
        $districtInfo->country_id    = \Input::get('country');
        $districtInfo->district_id   = $districtID;
        $districtInfo->division_id   = \Input::get('city');
        $districtInfo->district_name = \Input::get('district');

        if($districtInfo->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','New district has added successfully');

        }  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We can not take your request. Try again!.');

        }
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $districtList = District::leftjoin('division','district.division_id','=','division.division_id')->first();

        return view( 'backend.region.district.edit', [
            'region'       => Region::all(),
            'cityList'     => City::all(),
            'districtList' => $districtList,
            'countryList'  => Country::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        # validation section
        $this->validate($request, [
            'city'     => 'required',
            'district' => 'required',
        ]);

        $districtInfo =  District::where('district_id',$id)->first();
        $districtInfo->country_id    = \Input::get('country');
        $districtInfo->district_id   = $id;
        $districtInfo->division_id   = \Input::get('city');
        $districtInfo->district_name = \Input::get('district');

        if($districtInfo->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','Your information has update successfully');

        }  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We can not take your request. Try again!.');

        }
        return redirect('district/setup');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req) {

        District::where('district_id', $req->id)->delete();
        return response()->json();    
    }
    
    public function divisionSearch() {

        $ID = \Input::get('citySearch');

        $districtInfo = District::where('division_id',$ID)->get();
        echo '<option value=""> Select District</option>';
        foreach ($districtInfo as $value) {
            echo '<option value= '. $value->district_id.'> '.$value->district_name.' </option>' ;
        }
    }

}
