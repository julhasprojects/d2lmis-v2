<?php

namespace App\Http\Controllers\Region;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Frontpanel\Region;
use App\Model\Frontpanel\City;
use App\Model\Frontpanel\District;
use App\Model\Frontpanel\Upazila;
use App\Model\Frontpanel\Country;
use Session;
use Input;

class UpazilaSetupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $upazilaList = Upazila::leftjoin('district','upazila.district_id','=','district.district_id')
            ->leftjoin('division','upazila.division_id','=','division.division_id')
            ->leftjoin('country','upazila.country_id','=','country.country_id')
            ->get();

        return view( 'backend.region.upzila.index', [
            'region'       => Region::all(),
            'cityList'     => City::all(),
            'districtList' => District::all(),
            'countryList'  => Country::all(),
            'upazilaList'  => $upazilaList,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

         #validation section
        $this->validate($request, [
            'city'     => 'required',
            'district' => 'required',
            'country' => 'required',
            'upazila' => 'required',
        ]);

        $upazila = Upazila::orderBy('upazila_id','desc')->first();
        
        if(!empty($upazila)) {

            $upazilaID = $upazila->upazila_id + 1;

        } else {

           $upazilaID  = 1;

        }
      
        $upazilaSetup = New Upazila();
        $upazilaSetup->country_id    = \Input::get('country');
        $upazilaSetup->division_id   = \Input::get('city');
        $upazilaSetup->district_id   = \Input::get('district');
        $upazilaSetup->upazila_id    = $upazilaID;
        $upazilaSetup->upazila_name = \Input::get('upazila');

        if($upazilaSetup->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','New upazila has added successfully');

        }  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We can not take your request. Try again!.');

        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)  {

        $upazilaList = Upazila::leftjoin('district','upazila.district_id','=','district.district_id')
            ->leftjoin('division','upazila.division_id','=','division.division_id')
            ->leftjoin('country','upazila.country_id','=','country.country_id')
            ->where('upazila.upazila_id',$id)
            ->first();

        return view( 'backend.region.upzila.edit', [
            'region'       => Region::all(),
            'cityList'     => City::all(),
            'districtList' => District::all(),
            'countryList'  => Country::all(),
            'upazilaList'  => $upazilaList,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

         #validation section
        $this->validate($request, [
            'city'     => 'required',
            'district' => 'required',
            'country'  => 'required',
            'upazila'  => 'required',
        ]);
        
      
        $upazilaUpdate =  Upazila::where('upazila_id',$id)->first();
        $upazilaUpdate->country_id    = \Input::get('country');
        $upazilaUpdate->division_id   = \Input::get('city');
        $upazilaUpdate->district_id   = \Input::get('district');
        $upazilaUpdate->upazila_id    = $id;
        $upazilaUpdate->upazila_name = \Input::get('upazila');

        if($upazilaUpdate->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','Your information has update successfully');

        }  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We can not take your request. Try again!.');

        }
        return redirect('upazila/setup');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req) {

        Upazila::where('upazila_id', $req->id)->delete();
        return response()->json();    
    }

    public function upazilaSearch() {

        $ID = \Input::get('districtInfo');

        $upazillInfo = Upazila::where('district_id',$ID)->get();
        echo '<option value=""> Select Upazila</option>';
        foreach ($upazillInfo as $value) {
            echo '<option value= '. $value->upazila_id.'> '.$value->upazila_name.' </option>' ;
        }
    }
}
