<?php

namespace App\Http\Controllers\Region;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Frontpanel\City;
use App\Model\Frontpanel\District;
use App\Model\Frontpanel\Country;
use Session;
class DivisionSetupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $ciltyList = City::leftjoin('country','division.country_id','=','country.country_id')->get();
        return view( 'backend.region.division.index', [
            'cityList'     => $ciltyList,
            'countryList'  => Country::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)  {
         #validation section
        $this->validate($request, [
            'division' => 'required',
        ]);

        $division = City::orderBy('division_id','desc')->first();

        if(!empty($division)) {

            $divisionID = $division->division_id + 1;

        } else {

           $divisionID  = 1;

        }
      

        $divisionSetup = New City();
        $divisionSetup->country_id    = \Input::get('country');
        $divisionSetup->division_id   = $divisionID;
        $divisionSetup->division_name = \Input::get('division');

        if($divisionSetup->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','New division has added successfully');

        }  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We can not take your request. Try again!.');

        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
         return view( 'backend.region.division.edit', [
            'cityList'     => City::where('division_id',$id)->first(),
            'countryList'  => Country::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
         #validation section
        $this->validate($request, [
            'division' => 'required',
        ]);

        $divisionUpdate = City::where('division_id',$id)->first();
        $divisionUpdate->country_id    = \Input::get('country');
        $divisionUpdate->division_id   = $id;
        $divisionUpdate->division_name = \Input::get('division');

        if($divisionUpdate->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','Your information has update successfully');

        }  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We can not take your request. Try again!.');

        }
        return redirect('division/setup');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req) {

        City::where('division_id', $req->id)->delete();
        return response()->json(); 
           
    }



}
