<?php

namespace App\Http\Controllers\Frontpanel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Session;
class FrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if( Auth::check() ) {
           
              
        return view( 'frontend.home' );
        }
        else {
            return view('frontend.login.front_login');
         }
    }

    
}
