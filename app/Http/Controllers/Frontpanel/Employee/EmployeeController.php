<?php

namespace App\Http\Controllers\Frontpanel\Employee;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Frontpanel\Employee; 
use App\Model\Frontpanel\Empoffice; 
use App\Model\Frontpanel\Empemergency; 
use Session;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use DB;
use Input;
use Illuminate\Pagination\Paginator;


class EmployeeController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}
	/**
	 * [newEmployee description]
	 * @return [type] [description]
	 */
	public function newEmployee()
	{
	    $empid = DB::table('emp_general_info')->select('emp_id')
				->orderBy('id', 'desc')
				->first();
        $empid = $empid->emp_id + 1;

        $employee_info = Employee::paginate(5);

		return view( 'frontend.employee.new-employee' )
			->with( 'emp_id', $empid )
			->with( 'employee_info', $employee_info );
	}

	public function allEmployee() {
		$employee  = DB::table('emp_general_info')
          ->select('emp_general_info.*')
          ->orderBy('id', 'desc')
          ->paginate(10);
		$content =	view( 'frontend.employee.all-employee-list' )
						->with( 'employee', $employee );
		return view( 'frontend.index' )
					->with('content_area', $content );
	}

	/**
	 * new Line setup technician information setup.
	 *
	 * @return Response
	 */
	public function showEmployeeInfo( $emp_id )
	{

		$techInfo = DB::table('emp_general_info')
	 		->where( 'emp_id', $emp_id )
	 			->get();
		return view( 'frontend.employee.newLineSetupShowEmpInfo' )
			->with( 'technician', $techInfo );
   	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$userData = array('firstname'	=>$request->input('firstname'),
					  'lastname'		=>$request->input('lastname'),
					  'bitrthdate'		=>$request->input('bitrthdate'),
					  'fax'				=>$request->input('fax'),
					  'address'			=>$request->input('address'),
					  'emailaddress'	=>$request->input('emailaddress'),
					  'city'			=>$request->input('city'),
					  'website'			=>$request->input('website'),
					  'zip'				=>$request->input('zip'),
					  'materialstatus'	=>$request->input('materialstatus'),
					  'country'			=>$request->input('country'),
					  'gender'			=>$request->input('gender'),
					  'homephone'		=>$request->input('homephone'),
					  'age'				=>$request->input('age'),
					  'workphone'		=>$request->input('workphone'),
					  'nationalid'		=>$request->input('nationalid'),
					  'emp_id'			=>$request->input('emp_id'),
					  'datejoin'		=>$request->input('datejoin'),
					  'empstatus'		=>$request->input('empstatus'),
					  'location'		=>$request->input('location'),
					  'position'		=>$request->input('position'),
					  'department'		=>$request->input('department'),
					  'joinsalary'		=>$request->input('joinsalary'),
					  'asignarea'		=>$request->input('asignarea'),
					  'pr_contact'		=>$request->input('pr_contact'),
					  'rel_shn'			=>$request->input('rel_shn'),
					  'address'			=>$request->input('address'),
					  'city'			=>$request->input('city'),
					  'zip'				=>$request->input('zip'),
					  'homephone'		=>$request->input('homephone'),
					  'workphponeemp'	=>$request->input('workphponeemp')
		   );   
	  
	   	$rules 	= array('firstname'	=>'required',					
					'address'	=>'required',
					'city'		=>'required',
					'country'	=>'required',
					'gender'	=>'required',
					'workphone'	=>'required|max:15',
					'age'		=>'required|integer',
					'emp_id'	=>'required|integer',
					'datejoin'	=>'required|date',
					'position'	=>'required',
					'department'=>'required'
				);
	   	$validation = Validator::make($userData,$rules); 
		
		if($validation->fails())  
		{
			return  Redirect::to('new-employee')->withInput()->withErrors($validation);
		} else { 

			$employee 				= new Employee();
			$employee->emp_id 		= $request->input('emp_id');
			$employee->firstname	=$request->Input('firstname');
			$employee->lastname		=$request->Input('lastname');
			$employee->bitrthdate	=$request->Input('bitrthdate');
			$employee->fax 			=$request->Input('fax');
			$employee->address 		=$request->Input('address');
			$employee->emailaddress	=$request->Input('emailaddress');
			$employee->city 		=$request->Input('city');
			$employee->website		=$request->Input('website');
			$employee->zip 			=$request->Input('zip');
			$employee->materialstatus=$request->Input('materialstatus');
			$employee->country 		=$request->Input('country');
			$employee->gender 		=$request->Input('gender');
			$employee->homephone 	=$request->Input('homephone');
			$employee->age 			=$request->Input('age');
			$employee->workphone 	=$request->Input('workphone');
			$employee->nationalid 	=$request->Input('nationalid');
			$employee->save(); 

		 /* official information fillup*/
          
		$empofc 	= DB::table('emp_official_info')->select('genid')
							->orderBy('genid', 'desc')
							->first();
		$empofc 		= $empofc->genid + 1;
		$empoffice 		=	new Empoffice();

		$imagePath = $proimg = $bgImage = $backimg = '';
		// Profile Image
		if ( !empty( $request->file('primage') ) ) {
			$proimg 		= 'proimg' . rand( 0,100 ) . '.' . $request->file('primage')->getClientOriginalExtension();
			//$imagePath		= '/public/assets/images/profile/'; // Profile Image Path  
			$imagePath		= 'assets/images/profile/'; // Profile Image Path  
			$request->file('primage')->move( base_path() . $imagePath , $proimg );

		} else if( !empty( $request->file('baimage') ) ){
			$backimg	= 'backimg' . rand( 100,300 ) . '.' . $request->file('baimage')->getClientOriginalExtension();
			//$bgImage 	= '/public/assets/images/Background/';  // Background Image Path        
			$bgImage 	= 'assets/images/Background/';  // Background Image Path        
			$request->file('baimage')->move( base_path() . $bgImage , $backimg );
		}
    	
         
		$empoffice->genid 		= $empofc;
		$empoffice->emp_id 		= $request->input('emp_id');
		$empoffice->datejoin	=$request->Input('datejoin');
		$empoffice->empstatus	=$request->Input('empstatus');
		$empoffice->location 	=$request->Input('location');
		$empoffice->position 	=$request->Input('position');
		$empoffice->department	=$request->Input('department');
		$empoffice->joinsalary	=$request->Input('joinsalary');
		$empoffice->asignarea	=$request->Input('asignarea');
		$empoffice->profimg 	= $imagePath . $proimg;
		$empoffice->backimg 	= $bgImage . $backimg;
		$empoffice->save();
       
		  /*emergency info*/
		$empemergency 				=new Empemergency();
		$empemergency->emofid 		=$empofc;
		$empemergency->emp_id		=$request->input('emp_id');
		$empemergency->primcontact 	=$request->input('pr_contact');
		$empemergency->relation 	=$request->Input('rel_shn');
		$empemergency->address 		=$request->Input('address');
		$empemergency->city 		=$request->Input('city');
		$empemergency->zip 			=$request->Input('zip');
		$empemergency->homephone 	=$request->Input('homephone');
		$empemergency->workphponeemp=$request->Input('workphponeemp');
		$empemergency->save();
		
		Session::flash('message','Data save suceefully');
		return redirect('new-employee');
	  
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show( $info )
	{
		$client_info = DB::table('client_info')
			->where('firstname', 		'LIKE',  '%'.$info.'%' )
			->orWhere('lastname', 		'LIKE',  '%'.$info.'%' )
			->orWhere('newconid', 		'LIKE',  '%'.$info.'%' )
			->orWhere('emp_id', 		'LIKE',  '%'.$info.'%' )
			->orWhere('address', 		'LIKE',  '%'.$info.'%' )
			->orWhere('emailaddress', 	'LIKE',  '%'.$info.'%' )
			->orWhere('homephone', 		'LIKE',  '%'.$info.'%' ) 
			->orderBy('id', 'desc')
			->get();
   
	  	return view( 'frontend.employee.search_client_info' )->with( 'client_info', $client_info );
	}
	/**
	 * [showallEmployee description]
	 * @param  [type] $info [description]
	 * @return [type]       [description]
	 */
	public function showallEmployee( $info )
	{
		$employee_info = DB::table('emp_general_info')
			->where('firstname', 		'LIKE',  '%'.$info.'%' )
			->orWhere('lastname', 		'LIKE',  '%'.$info.'%' )
			->orWhere('emp_id', 		'LIKE',  '%'.$info.'%' )
			->orWhere('address', 		'LIKE',  '%'.$info.'%' )
			->orWhere('emailaddress', 	'LIKE',  '%'.$info.'%' )
			->orWhere('city', 			'LIKE',  '%'.$info.'%' )
			->orWhere('workphone', 		'LIKE',  '%'.$info.'%' )
			->orWhere('nationalid', 	'LIKE',  '%'.$info.'%' )
			->orderBy('id', 'desc')
			->get();
   
	  	return view( 'frontend.employee.all-employee-search-list' )->with( 'employee', $employee_info );
	}

}
