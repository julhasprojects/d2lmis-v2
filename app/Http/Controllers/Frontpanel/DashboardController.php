<?php

namespace App\Http\Controllers\Frontpanel;

use Illuminate\Http\Request;
use App\Model\Frontpanel\Clientcomplain;
use App\Model\Frontpanel\Newlines; 
use App\Model\Frontpanel\Employee; 
use App\Model\Frontpanel\Region; 
use App\Model\Frontpanel\Road; 
use App\Model\Backend\Package;  
use App\Model\Backend\BoxInfo;  
use App\Model\Backend\Date;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Frontpanel\Packages; 
use App\Model\Frontpanel\Billgenerate; 
use App\Model\Frontpanel\Techinfo; 
use App\Model\Frontpanel\Empoffice; 
use App\Model\Backend\Company; 
use App\Model\Backend\DefaulterList; 
use App\Http\Model\Frontpanel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
use Config;
use ZipArchive;
use Input;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function databaseBackup() {

        $database_name =  DB::connection()->getDatabaseName();

        $database_host     = \Config::get('database.connections.mysql.host');
        $database_user     = \Config::get('database.connections.mysql.username');
        $database_password = \Config::get('database.connections.mysql.password');

        //var_dump( $database_name, $database_host, $database_user,$database_password );

        $bkpDone = $this->dbBackup( $database_host, $database_user, $database_password, $database_name, $tables = '*');
        
        if ($bkpDone) {

            Session::put( 'valid', 1 );
            Session::flash('message','Database backup has sucessfully completed.');

        } else {

            Session::put( 'invalid', 1 );
            Session::flash('message','Whoops! Something went wrong.');
            
        }

        return redirect('isperp-dashboard');

    }

    public function validity() {

        $database_name =  DB::connection()->getDatabaseName();

        $database_host     = \Config::get('database.connections.mysql.host');
        $database_user     = \Config::get('database.connections.mysql.username');
        $database_password = \Config::get('database.connections.mysql.password');

        //var_dump( $database_name, $database_host, $database_user,$database_password );

    }

    /**
     * [dbBackup description]
     * @param  [type] $database_host     [description]
     * @param  [type] $database_user     [description]
     * @param  [type] $database_password [description]
     * @param  [type] $database_name     [description]
     * @return [type]                    [description]
     */
    private function dbBackup($database_host, $database_user, $database_password, $database_name ) {
        
        ini_set('display_errors', '1');    // 0 or 1 set 1 if unable to download database it will show all possible errors
        ini_set('max_execution_time', 0); 
  
        $directory = "DatabaseBackup";

        $databaseBackup = public_path();
        /*
        if (!is_dir(public_path($directory))) {
            
            mkdir($directory, 0777, true );  
            $databaseBackup =  public_path() . '/' . $directory . '/';   
        }
        */
        $fileName = $database_name . '_' . date('d-m-Y') . '_'.date('h_i_s').'.sql' ; 
        
        // Set execution time limit
        if(function_exists('max_execution_time')) {
            if( ini_get('max_execution_time') > 0 )  {
                set_time_limit(0) ;
            }   
                
        }

        // Introduction information
        $return='';
        $return .= "--\n";
        $return .= "-- A Mysql Backup System \n";
        $return .= "--\n";
        $return .= '-- Export created: ' . date("Y/m/d") . ' on ' . date("h:i") . "\n\n\n";
        $return = "--\n";
        $return .= "-- Database : " . $database_name . "\n";
        $return .= "--\n";
        $return .= "-- --------------------------------------------------\n";
        $return .= "-- ---------------------------------------------------\n";
        $return .= 'SET AUTOCOMMIT = 0 ;' ."\n" ;
        $return .= 'SET FOREIGN_KEY_CHECKS=0 ;' ."\n" ;
        $tables = array() ; 
       
        $mysqli   = mysqli_connect( $database_host, $database_user, $database_password, $database_name );
        
        // Exploring what tables this database has
        $result = $mysqli->query('SHOW TABLES' ) ; 
        // $result = $mysqli->query("DROP DATABASE $database_name" ) ; 
        // var_dump('garg');
        // dd($result);

        while ($row = $result->fetch_row() ) {
            
            $tables[] = $row[0] ;
            
        }
        
        // Cycle through each  table
        foreach($tables as $table)  { 

            // Get content of each table
            $result = $mysqli->query('SELECT * FROM '. $table) ; 
            // Get number of fields (columns) of each table
            $num_fields = $mysqli->field_count  ;
            // Add table information
            $return .= "--\n" ;
            $return .= '-- Tabel structure for table `' . $table . '`' . "\n" ;
            $return .= "--\n" ;
            $return.= 'DROP TABLE  IF EXISTS `'.$table.'`;' . "\n" ; 
            // Get the table-schema
            $schema = $mysqli->query('SHOW CREATE TABLE '.$table) ;
            // Extract table schema 
            $tableshema = $schema->fetch_row() ; 
            // Append table-schema into code
            $return.= $tableshema[1].";" . "\n\n" ; 
            // Cycle through each table-row
            while($rowdata = $result->fetch_row()) { 
                // Prepare code that will insert data into table 
                $return .= 'INSERT INTO `'.$table .'`  VALUES ( '  ;
                // Extract data of each row 
                for($i=0; $i<$num_fields; $i++)  {   
                    $return .= '"'.$mysqli->real_escape_string($rowdata[$i]) . "\"," ;
                }
                // Let's remove the last comma 
                $return = substr("$return", 0, -1) ; 
                $return .= ");" ."\n" ;
            } 
            
            $return .= "\n\n" ; 
        }
        // Close the connection
        $mysqli->close() ;
        $return .= 'SET FOREIGN_KEY_CHECKS = 1 ; '  . "\n" ; 
        $return .= 'COMMIT ; '  . "\n" ;
        $return .= 'SET AUTOCOMMIT = 1 ; ' . "\n"  ; 

        header('Content-Type: application/octet-stream');   
        header("Content-Transfer-Encoding: Binary"); 
        header("Content-disposition: attachment; filename=\"".$fileName."\"");  
        echo $return; exit;

        // return file_put_contents($fileName , $return); 
                
        // $handle = fopen($fileName, 'w+');
        // fwrite($handle, $return);
        // fclose($handle);
        // echo "success";

    }

    public function corporateCustomerType() {

        return view('frontend.request.addfieldcorporatetype');
    }    
    
    public function clienthistoryInformation() {

        $serchingtype   = \Input::get('serchingtype');
        $newconidSearch = \Input::get('searchLineHistory');

        if( $serchingtype == 1) {

            $find       = \Input::get('searchLineHistory');

        } else if($serchingtype == 2) {

           /* $clientInfo = Newlines::where('username',$newconidSearch)->first();
            $find       = $clientInfo->newconid;   */

            $clientInfo = Newlines::leftjoin('region','client_info.region','=','region.region_id')
            ->leftjoin('road','client_info.road','=','road.id')
            ->where(function ($query) use ($newconidSearch) {
                $query->where('client_info.username', 'like', '%'.$newconidSearch.'%') ;
            })
            ->select('client_info.*','region.*','road.road as roadName')
            ->take(50)
            ->get();

            return view( 'frontend.dashboard.client-basic-history', [
                'clientInfo'=> $clientInfo,
            ]);

        } else if($serchingtype == 3) {

            $clientInfo = Newlines::leftjoin('region','client_info.region','=','region.region_id')
            ->leftjoin('road','client_info.road','=','road.id')
            ->where(function ($query) use ($newconidSearch) {
                $query->where('client_info.mobilenumber', 'like', '%'.$newconidSearch.'%') ;
            })
            ->select('client_info.*','region.*','road.road as roadName')
            ->take(50)
            ->get();

            return view( 'frontend.dashboard.client-basic-history', [
                'clientInfo'=> $clientInfo,
            ]);

           /* $clientInfo = Newlines::where('mobilenumber',$newconidSearch)->first();
            $find       = $clientInfo->newconid;*/

        } else if($serchingtype == 4) {

            $clientInfo = Newlines::leftjoin('region','client_info.region','=','region.region_id')
                ->where('client_info.firstname',$newconidSearch)
                ->first();
            $find       = $clientInfo->newconid;

        }  else if($serchingtype == 5) {

            $clientInfo = Newlines::leftjoin('region','client_info.region','=','region.region_id')
            ->leftjoin('road','client_info.road','=','road.id')
            ->where(function ($query) use ($newconidSearch) {
                $query->where('client_info.house', 'like', '%'.$newconidSearch.'%') ;
            })
            ->select('client_info.*','region.*','road.road as roadName')   
            ->take(50)
            ->get();

            return view( 'frontend.dashboard.client-basic-history', [
                'clientInfo'=> $clientInfo,
            ]);

        } else if($serchingtype == 6 && !empty($newconidSearch)) {
          
            /*Newlines:: where(function($query) use($newconidSearch) {
                $query->orWhere('client_info.newconid', 'LIKE',  '%'.$newconidSearch.'%' );
                $query->orWhere('client_info.firstname', 'LIKE',  '%'.$newconidSearch.'%');
                $query->orWhere('client_info.lastname', 'LIKE',  '%'.$newconidSearch.'%');
                $query->orWhere('client_info.username', 'LIKE',  '%'.$newconidSearch.'%');
                $query->orWhere('client_info.house', 'LIKE',  '%'.$newconidSearch.'%');
                $query->orWhere('client_info.mobilenumber', 'LIKE',  '%'.$newconidSearch.'%');
            })*/

            $monthYear = explode(',', Date::monthYear() );

            $clientInfo = Newlines::leftjoin('region','client_info.region','=','region.region_id')
                ->leftjoin('road','client_info.road','=','road.id')
                ->where(function ($query) use ($newconidSearch) {
                    $query->where('client_info.username', 'like', '%'.$newconidSearch.'%')
                        ->orWhere('client_info.mobilenumber', 'like','%'.$newconidSearch.'%')
                        ->orWhere('client_info.firstname', 'like',"$newconidSearch%")
                        ->orWhere('client_info.newconid', 'like',"$newconidSearch%");
                })
                ->leftjoin('instruments','client_info.newconid','=','instruments.newconid')
                ->select('client_info.*','region.*','road.road as roadName','instruments.nlbillcollect_status')
                // ->rightjoin('bill_generate','client_info.newconid','=','bill_generate.newconid')
                ->take(50)
                ->get();

            

           /* $billInfo = Newlines::leftjoin('region','client_info.region','=','region.region_id')
                ->leftjoin('road','client_info.road','=','road.id')
                ->where(function ($query) use ($newconidSearch) {
                    $query->where('client_info.username', 'like', '%'.$newconidSearch.'%')
                        ->orWhere('client_info.mobilenumber', 'like','%'.$newconidSearch.'%')
                        ->orWhere('client_info.firstname', 'like',"$newconidSearch%")
                        ->orWhere('client_info.newconid', 'like',"$newconidSearch%");
                })
                ->leftjoin('bill_generate','client_info.newconid','=','bill_generate.newconid')
                ->select('bill_generate.status','bill_generate.duestatus','client_info.*','region.*','road.road as roadName')
                ->where('bill_generate.bilmnth',$monthYear[0])
                ->where('bill_generate.billyear',$monthYear[1])
                ->take(50)
                ->get();*/


           /* // Old clients
            $billOldLine = [];
            $billnewLine = [];
            foreach ($billInfo as  $value) {

                if($value->nl_status==2){
                    $billOldLine[] = $value;
                }
                  
            }   

            // New Clients
            foreach ($clientInfo as  $value) {
                if($value->nl_status!=2){
                    $billnewLine[] = $value;
                }
            }

           


            // echo"<pre>";
            // print_r($clientInfo);
            // die();*/

            return view( 'frontend.dashboard.client-basic-history', [
                'clientInfo'=> $clientInfo,
            ]);

        }

        $monthYear = explode(',', Date::monthYear() );
        $branchId  = Session::get('branchId');
        
        $duefualter = Billgenerate::where('newconid',$newconidSearch)
            ->where('bilmnth',$monthYear[0])
            ->where('billyear',$monthYear[1])
            ->first();

        if(!empty($duefualter)) {

            $duefualterList = $duefualter->is_defaulter;

        } else {

            $duefualterList = 0;

        }

        # client details information
        $empDetails  = DB::table('client_info')
            ->leftjoin('line_disconnect', 'line_disconnect.clientid', '=', 'client_info.newconid')
            ->leftjoin('region', 'client_info.region', '=', 'region.region_id')  
            ->leftjoin('road', 'client_info.road', '=', 'road.id')         
            ->leftjoin('instruments', 'client_info.newconid', '=', 'instruments.newconid')
            ->leftjoin('box_info', 'instruments.box_no', '=', 'box_info.box_no')
            ->leftjoin('real_ip_setup', 'instruments.real_ip', '=', 'real_ip_setup.id')
            ->leftjoin('packages_info', 'packages_info.package_id', '=', 'instruments.packageid')
            // ->leftjoin('bill_collection', 'client_info.newconid', '=', 'bill_collection.newconid')
            ->leftjoin('newlinerequest', 'client_info.newline_reqid', '=', 'newlinerequest.newline_reqid')
            // ->leftjoin('bill_generate', 'client_info.newconid', '=', 'bill_generate.newconid')
            ->select('instruments.mikortik_id','instruments.line_on_off','instruments.bill_collector','instruments.bill_collection','instruments.mac_address','instruments.due_adjustement','newlinerequest.nlr_paid','newlinerequest.assigned_date as requestAssignedDate','newlinerequest.nlr_due','newlinerequest.nlr_due_adjust','newlinerequest.connection_cost as connectionPaid','instruments.switch_port_no','instruments.nl_grand_total','instruments.cable_meter','instruments.connection_date as insconnectionDate','box_info.*','line_disconnect.id as reconnectId','line_disconnect.dis_from','line_disconnect.dis_type','real_ip_setup.real_ip','client_info.branch_id','client_info.username','packages_info.packagename','line_disconnect.assigned_date','packages_info.packagecosting','instruments.box_no','instruments.connection_fee','instruments.special_discount','instruments.previous_due','instruments.newline_paid','instruments.due_bill','instruments.advancepaid','client_info.newconid', 'client_info.emp_id as technician','client_info.*','newlinerequest.request_date','newlinerequest.created_at as nlcreated_at','newlinerequest.connection_date','newlinerequest.accepted_by',  'region.region_name','road.road')
            ->where('client_info.newconid', $newconidSearch)
            ->where( 'client_info.branch_id',$branchId )
            ->orderBy( 'line_disconnect.id','desc')
            ->first(); 
    
   
            
      
        # Company information
        $company = Company::first();

        
        return view( 'frontend.dashboard.client-connection-history', [
            'emp_detail'           => $empDetails,
            // 'packagechangeRequest' => $packagechangeRequest,
            'employee'             => Empoffice::allEmployer(),
            'employeeList'         => Empoffice::allEmployer(),
            // 'complain'             => $complain,
            // 'houseChangeData'      => $houseChangeData,
            // 'line_disconnect'      => $line_disconnect,
            'company'              => $company,
            // 'billInfo'             => $billInfo,
            // 'reconnection'         => $reconnection,
            'contactperson'        => Empoffice::allEmployer(),
            'currentYear'          => $monthYear[1],
            'currentMonth'         => $monthYear[0],
            // 'basicInfo'            => $basicInfo,
            'duefualterList'       => $duefualterList,
            'companyinfo'          => $company,
        ]);
                        
    }

    /*Client information basic search*/
    public function clienthistoryInformationBasic() { 

        $searchHistory = \Input::get('searchLineHistory');

        $clientInfo = Newlines:: where(function($query) use($searchHistory) {
                $query->orWhere('client_info.newconid', 'LIKE',  '%'.$searchHistory.'%' );
                $query->orWhere('client_info.firstname', 'LIKE',  '%'.$searchHistory.'%');
                $query->orWhere('client_info.lastname', 'LIKE',  '%'.$searchHistory.'%');
                $query->orWhere('client_info.username', 'LIKE',  '%'.$searchHistory.'%');
                $query->orWhere('client_info.house', 'LIKE',  '%'.$searchHistory.'%');
                $query->orWhere('client_info.mobilenumber', 'LIKE',  '%'.$searchHistory.'%');
            })
            ->take(10)
            ->get();

        return view( 'frontend.dashboard.client-basic-history', [
            'clientInfo'=> $clientInfo,
        ]);
    }


    /**
     * [clientHouseShiftHistory description]
     * @param  [type] $find [description]
     * @return [type]       [description]
     */
    public function clientHouseShiftHistory($find) {


        $HouseChangeData = DB::table('housechange')
            ->join('client_info', 'client_info.newconid', '=', 'housechange.newconid')
            ->join('region', 'client_info.region', '=', 'region.region_id')     
            ->select('client_info.*', 'housechange.road as shift_road','housechange.house as shift_house','housechange.flat_no as shift_flat','housechange.area as area','housechange.updated_at as shifted_date', 'region.region_name' ) 
            ->Where('housechange.status', 1)
            ->Where('client_info.linestatus',1 )
            ->Where('client_info.newconid', $find )
            ->orderby('client_info.newconid', 'desc')
            ->take(20)
            ->get();

        return view( 'frontend.dashboard.client-houseshift-history' )  
                    ->with('HouseChangeData',$HouseChangeData);                      
    }   

    /**
     * [clientInstrumentHistory description]
     * @param  [type] $find [description]
     * @return [type]       [description]
     */
    public function clientInstrumentHistory($find) {

        $employee_tech = DB::select( DB::raw("SELECT distinct emp.employeeID,emp.fullName  FROM `designation` dg inner join department dt on dg.deptID=dt.id inner join employees emp on emp.designation=dg.id where dt.id=13 or dt.id=16 or dt.id=17 or dt.id=18 or dt.id=19") );
        
        $instruments_info =  DB::table('client_info')    
            ->join('instruments', 'instruments.newconid', '=', 'client_info.newconid')
            ->select('client_info.firstname','client_info.lastname','client_info.username','client_info.newconid','instruments.box_no','instruments.box_address','instruments.switch_name','instruments.port','instruments.switch_no','instruments.switch_assignby','instruments.mc_name','instruments.mc_number')
            ->Where('client_info.linestatus',   '=',  '1' )
            ->Where('client_info.newconid', $find )
            ->orderby('instruments.newconid', 'desc')
            ->take(5)
            ->get();

        return view( 'frontend.dashboard.client-instrument-history' )  
                    ->with('employee_tech',$employee_tech)                     
                    ->with('instruments_info',$instruments_info);                     
                            
    } 

    /**
     * [clientComplainsHistory description]
     * @param  [type] $find [description]
     * @return [type]       [description]
     */
    public function clientComplainsHistory($find) {

      $employee = DB::select( DB::raw("SELECT id,employeeID,fullName FROM employees") );
      $contactperson = DB::select( DB::raw("SELECT distinct emp.employeeID,emp.fullName FROM `designation` dg inner join department dt on dg.deptID=dt.id inner join employees emp on emp.designation=dg.id where dt.id=13 or dt.id=16 or dt.id=17 or dt.id=18 or dt.id=19") ); 
       

        $complain = DB::table('client_info')
            ->join('region', 'client_info.region', '=', 'region.region_id')
            ->join('road', 'client_info.road', '=', 'road.id')
            ->join('instruments', 'client_info.newconid', '=', 'instruments.newconid')
            ->join('packages_info', 'instruments.packageid', '=', 'packages_info.package_id')
            ->join('client_complain', 'client_info.newconid', '=', 'client_complain.newconid')
            ->select('client_info.*', 'client_complain.*','client_complain.id as uid','region.region_name', 'road.road','packages_info.packagename','packages_info.packagecosting')
            ->Where('client_info.newconid', 'like', '%'.$find.'%' )
            ->orderby('client_complain.created_at', 'desc')        
            ->orderby('client_complain.asgemploy', 'asc')        
            ->get();                  

        return view( 'frontend.dashboard.client-complain-history' )  
                ->with( 'contactperson', $contactperson )
                ->with( 'employee', $employee )                
                ->with('complain_info',$complain);    

    } 
    
    public function employeeProfileData() {

        return view( 'backend.left_sidebar', 
            [
                'admin_info' => 'Hi Roton'
            ]);

    }    

    public function packageSelectList() {

        // Package select list
        $packageInfo = Packages::all();
        $package     = \Input::get('new_package');

        foreach($packageInfo as $packageList) {
            if($package == $packageList->package_id) {
                echo  "<span class='packag-col-3'>  <input type='checkbox'  checked >  ".$packageList->packagename."   </span>";
            } else {
                echo  "<span class='packag-col-3'>  <input type='checkbox'  >  ".$packageList->packagename."   </span>";
            }
            

        } 
      
    }


    public function payBillRedirect() {

        $id         = Input::get('id');
        // $linestatus = Input::get('linestatus');
        $newlineInfo = Newlines::where('newconid',$id)->first();
        $monthYear = explode(',', Date::monthYear() );
        if($newlineInfo->nl_status==2) {

            $billGenerateInfo = Billgenerate::where('newconid',$id)
                ->Where('bilmnth',$monthYear[0])
                ->Where('billyear',$monthYear[1])
                ->first(); 

            if($billGenerateInfo->status==0 && $billGenerateInfo->duestatus==1) {

                return 1;

            }
            if ($billGenerateInfo->status==1 && $billGenerateInfo->duestatus==1) {

                return 2;

            } 
            if ($billGenerateInfo->status==1 && $billGenerateInfo->duestatus==0) {

                return 3;

            }

        } else {

            return 4;

        }

        
    }
}