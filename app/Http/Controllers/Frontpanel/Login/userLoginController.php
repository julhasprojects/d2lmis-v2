<?php

namespace App\Http\Controllers\Frontpanel\Login;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use DB;
use Input;
use App\User;
use Illuminate\Pagination\Paginator;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Model\Frontpanel\Register; 
use App\Model\Frontpanel\Empoffice; 
use App\Model\Backend\Date; 
use App\Model\Backend\Role; 
use App\Model\Backend\Company; 
use App\Model\Hrm\Employees; 
use App\Notifications\NotificationCenter;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use Session;
use Notification;


class userLoginController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request) {
		
		

		$companyInfo = Company::select('company_name', 'company_shortname')->orderBy('id', 'desc')->first();

		if( !Auth::check() ) {

			return view('frontend.login.front_login');
		
		} else {  

			$request->session()->flush();

			$message = 'Sorry! Authenication Failed';
			return Redirect::to('/')->withErrors($message);
		}
	}
		
	public function dhis2Login(Request $request) {	
		$username = trim($request->input('username'));
		$password = trim($request->input('password'));
		$ch = curl_init("https://play.dhis2.org/2.30/api/me");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_USERPWD, "admin:district1");
		$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
		$result=curl_exec ($ch);
		curl_close ($ch);
		$data['resultData']= json_decode($result, true);
		if($status_code == 0){
			
			return 401;
		}else {
			$monthYear = explode(',', Date::monthYear());

			# Authcation user data
			$user   = Auth::User();
			$branch = Empoffice::empBranch($user->emp_id);

			# Login employeer name
			$employeeName = Employees::where('employeeID',$user->emp_id)->first();
			$userNames    = explode(" ", $employeeName->fullName);

			if(count($userNames)>1) {

				$shortname = $userNames[1];

			} else {

				$shortname = $userNames[0];
				
			}

			# Session stord data
			Session::put('branchName',$branch->branch_name);
			Session::put('fullname',$shortname);
			Session::put('empId',$branch->employeeID);
			Session::put('branchId',$branch->branch_id);
			Session::put('name',$user->name);
			Session::put('id',$user->id);
			Session::put('user_id',$user->emp_id);
			Session::put('emp_id',$user->emp_id);
			Session::put('mobile',$user->mobile);
			Session::put('email',$user->email);
			Session::put('current_month',$monthYear[0]);

			return Redirect::to('d2lmis-dashboard' );
		}
	//return $data;
		// return Redirect::to('https://play.dhis2.org/2.31.3' );
	}


    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate(Request $request) {

      	$userData = [ 'username' => $request->input('email'),'password' => $request->input('password'), 'deleted_at' => null ]; 
	  	$rules    = [ 'username' => 'required|max:100', 'password' => 'required' ];
	  
	  	$validation = Validator::make( $userData, $rules ); 

        if ( $validation->fails() ) {

			return  Redirect::to('/')->withErrors($validation);

		} else {
			 	
			if( Auth::attempt( $userData ) ) {


				$monthYear = explode(',', Date::monthYear());

				# Authcation user data
				$user   = Auth::User();
				$branch = Empoffice::empBranch($user->emp_id);

				# Login employeer name
				$employeeName = Employees::where('employeeID',$user->emp_id)->first();
				$userNames    = explode(" ", $employeeName->fullName);

				if(count($userNames)>1) {

					$shortname = $userNames[1];

				} else {

					$shortname = $userNames[0];
					
				}

				# Session stord data
				Session::put('branchName',$branch->branch_name);
				Session::put('fullname',$shortname);
				Session::put('empId',$branch->employeeID);
				Session::put('branchId',$branch->branch_id);
				Session::put('name',$user->name);
				Session::put('id',$user->id);
				Session::put('user_id',$user->emp_id);
				Session::put('emp_id',$user->emp_id);
				Session::put('mobile',$user->mobile);
				Session::put('email',$user->email);
				Session::put('current_month',$monthYear[0]);

				$userPermission = Session::put( 'permission',$user->permission );

				$roleInfo = Role::leftjoin('role_user','roles.id','=','role_user.role_id')
					->where('role_user.user_id',$user->id)
					->first();

				if( $roleInfo->slug == 'super.admin' || $roleInfo->slug == 'admin') {

					// return redirect('isperp-dashboard')->with('notifection', 'hi');
					return Redirect::to('d2lmis-control-panel' );

				} else {   

					$request->session()->flush();
					$message = 'Sorry! Authenication Failed';
					return Redirect::to('/')->withErrors($message);

				}
			} else {

				$message = 'Authenication Failed';
				return Redirect::to('/')->withErrors($message);

			}
		} 
    }

/*    private function notifectionArea() {
    	$notifection = '15';

    	return $notifection;
    	// return 200;
    }*/
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	   return view('frontend.register.register_data'); 
	}

	public function checkLogin(Request $request) {
	
		$userData = array(
			'email'    => $request->input('email'),
			'password' => $request->input('password') 
		); 
	  		
	  	$rules   = array( 
	  		'email'    => 'required|email|max:100', 
	  		'password' => 'required' 
	  	);

	  	$validation = $this->validate(request(),[
	  		'email'    => 'required|email|max:100', 
	  		'password' => 'required', 
	  	]); 
	  
		if ( $validation->fails() ) {

			return  Redirect::to('/')->withErrors($validation);
		
		} else {
			 	
			if( Auth::attempt( $userData ) ) {
				
				$monthYear = explode(',', Date::monthYear());

				$user = Auth::User();

				Session::put('name',$user->name);
				Session::put('id',  $user->id);
				Session::put('user_id',$user->emp_id);
				Session::put('emp_id',$user->emp_id);
				Session::put('mobile',$user->mobile);
				Session::put('email',$user->email);
				Session::put('current_month',$monthYear[0]);

				$userPermission = Session::put( 'permission',$user->permission );

				if( $user->permission == '1' || $user->permission == '2' ) {

					return Redirect::to('d2lmis-control-panel');

				} else if ( $user->permission == '3') {
			
					return Redirect::to('customercare-dashboard');

				} else if ( $user->permission == '4') {
			
					return Redirect::to('user-bill-collection-by-collector');

				} else if ( $user->permission == '5'  ) {
			
					return Redirect::to('inventory-all-reports');

				} else if($user->permission == '6') {

					return Redirect::to('master-disconnect-report');

				} else {   

					$request->session()->flush();
					$message = 'Sorry! Authenication Failed';
					return Redirect::to('/')->withErrors($message);

				}
			} else {

				$message = 'Authenication Failed';
				return Redirect::to('/')->withErrors($message);

			}
		} 

	 }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request ) {
	
	   	$exist = 0;
		$exist = Register::where('email', $request->input('email'))->count();
		               
	   	if( !exist ) {

			$register = new Register();
			$register->name = $request->input('name');
			$register->email = $request->input('email');
			$register->password = Hash::make( $request->input('password') );
			$register->save();

			Session::flash('message','Registration has successfully completed!');

	   	} else {

		  Session::flash('message','Email has already exist!');  

	   	}
	 
	  	return redirect('register-data');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	  
	public function logout() {

	  	Auth::logout();
	  	return Redirect::to('/');

	}
	
}