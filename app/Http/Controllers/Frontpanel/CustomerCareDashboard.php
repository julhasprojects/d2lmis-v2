<?php

namespace App\Http\Controllers\Frontpanel;
use Illuminate\Http\Request;
use App\Model\Frontpanel\Clientcomplain;
use App\Model\Frontpanel\Newlines; 
use App\Model\Frontpanel\Employee; 
use App\Model\Frontpanel\Region; 
use App\Model\Frontpanel\Road; 
use App\Model\Frontpanel\Empoffice; 
use App\Model\Frontpanel\ComplainHistory; 
use App\Model\Backend\Package;  
use App\Model\Backend\NewlineSetup; 
use App\Model\Backend\NotificationCenter; 
use App\Model\Backend\UnamepasswordCombination;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Frontpanel\packages; 
use App\Model\Frontpanel\Techinfo; 
use App\Model\Frontpanel\NewlineRequest; 
use App\Http\Model\Frontpanel;
use App\Model\Backend\Company;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Model\Backend\Date;
use Session;
use DB;
use Config;

class CustomerCareDashboard extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function customerCareDashboard() {  

        /**
         * This part is for system loging tracking
        */  
        $user_id   = Session::get('user_id');
        $emp_id    = Session::get('emp_id');
        $email     = Session::get('email');
        $empId     = Session::get('empId');
        $monthYear = explode(',', Date::monthYear() ); 
        $today        = date('Y-m-d'); 
        $year         = date("Y");
        $currentTime  = strtotime($year);
        
        $yesterDay    = date('Y-m-d', strtotime('-1 day', strtotime($year))); 
        $tommorrow    = date('Y-m-d', strtotime('+1 day', strtotime($year))); 
        
        $startDay = Date::StartdayEndday('0');
        $endDay   = Date::StartdayEndday('1');

        /*$notifectionAll       = explode(',', NotificationCenter::NoticationCount($startDay,$endDay));
        $notifectionYesterday = explode(',', NotificationCenter::NoticationCount($yesterDay,$yesterDay));
        $notifectionToday     = explode(',', NotificationCenter::NoticationCount($today,$today));
        $notifectionTommorow  = explode(',', NotificationCenter::NoticationCount($tommorrow,$tommorrow));
        
*/
        $branchId = Session::get('branchId');

        $system_ip  = $_SERVER['REMOTE_ADDR'];
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        $login_date = date('Y-m-d H:i:s');

        if ( $emp_id != null ) {

            DB::insert("insert into tbl_admin_log (user_id,email,system_ip,login_date,user_agent) values ( '$emp_id','$email','$system_ip','$login_date','$user_agent')") ;
        }

        $region      = DB::table('region')->select('region_id','region_name')->get();
        $road        = DB::table('road')->select('id','road')->get();
        $cpackage    = Package::where('status',1)->get();
        $allEmp      = Empoffice::branchAllEmpList();
        $today       = date('Y-m-d');
        $previousDay = date('Y-m-d', strtotime(' -1 day'));

        $recentConnections = Newlines::limit(15)
            ->where('branch_id',$branchId)
            ->orderBy('created_at','desc')
            ->get();

        $complainCount   = ComplainHistory::complainHistoryCount();
        $todaycomplain   = Clientcomplain::dayComplain($today,$branchId);
        $totalcomplain   =  $complainCount->total; 
        $pendingComplain =  $complainCount->pending; 

       

        $clientList = Newlines::where('branch_id',$branchId)->get();

        $lineDisDetails = DB::table('client_info')
            ->join('line_disconnect', 'line_disconnect.clientid', '=', 'client_info.newconid')                
            ->select('line_disconnect.dis_month','client_info.newconid') 
            ->where('client_info.linestatus', 2) // only deactive connection 
            ->where('line_disconnect.disconstatus', 2) // only deactive connection 
            ->orderBy('line_disconnect.created_at', 'desc')
            ->where('line_disconnect.dis_month',$monthYear[0])
            ->where('line_disconnect.year',$monthYear[1])
            ->count('line_disconnect.clientid');
            
        $totaldisconect = DB::table( 'line_disconnect' )
            ->where('line_disconnect.dis_month', $monthYear[0])
            ->where('line_disconnect.year', $monthYear[1])
            ->where('line_disconnect.disconstatus',4)
            ->count(); 

         $clineComplain = Clientcomplain::countComplain(1);

        $content = view('frontend.customer-care-dashboard', [
            'roadList'             => $road,
            'regionList'           => Empoffice::branchRegion(),
            'cpackage'             => $cpackage,
            'employee'             => Empoffice::allEmployer(),
            'complain_emp'         => Empoffice::employeeCatgoryList('3'),
            'collectedBy'          => Empoffice::allEmployer(),
            'recentConnections'    => $recentConnections,
            'totalcomplain'        => $totalcomplain,
            'todayconection'       => Newlines::dateWiseConnection($today,1,3,$branchId),
            'todaysreconnections'  => Newlines::dateWiseConnection($today,3,3,$branchId),
            'lastdayconection'     => Newlines::dateWiseConnection($previousDay,1,3,$branchId),
            'todaycomplain'        => $todaycomplain,
            'pendingComplain'      => $clineComplain,
            'clientList'           => $clientList,
            'lineDisDetails'       => $lineDisDetails,
            'customerType'         => newlineSetup::all(),
            // 'notifectionYesterday' => $notifectionYesterday,
            // 'notifectionToday'     => $notifectionToday,
            // 'notifectionTommorow'  => $notifectionTommorow,
            'currentMonth'         => $monthYear[0],
            'currentYear'          => $monthYear[1],
            'today'                => $today,
            'emp_id'               => $emp_id,
            'totaldisconect'       => $totaldisconect,
            // 'notifectionAll'       => $notifectionAll,
            'companyinfo'          => Company::first(), 
            'packageInfo'          => Package::all(), 
            'status'               => 15
        ]);

        return view( 'frontend.index' )->with('content_area',$content);
    }

    /**
     * [Newline Request Page Load description]
     */
    public function newlineRequestPageLoad() {

        # branch id 
        $branchId   = Session::get('branchId');  
        $road       = DB::table('road')
            ->select('id','road')->get();

        // package information
        $cpackage   = Package::where('status',1)->get();
        $clientList = Newlines::where('branch_id',$branchId)
            ->get();

        $newlineCharge    = UnamepasswordCombination::first();

        $newlineRequest   = NewlineRequest::orderBy('id','desc')->first();
        $newlineRequestID = !empty($newlineRequest->id) ? $newlineRequest->id:0;

        return view('frontend.newline-request-page', [
            'roadList'         => $road,
            'regionList'       => Empoffice::branchRegion(),
            'cpackage'         => $cpackage,
            'customerType'     => newlineSetup::all(),
            'clientList'       => $clientList,
            'employee'         => Empoffice::allEmployer(),
            'collectedBy'      => Empoffice::allEmployer(),
            'emp_id'           => Session::get('emp_id'),
            'companyinfo'      => Company::first(), 
            'packageInfo'      => Package::all(), 
            'newlineRequestID' => $newlineRequestID, 
            'newlineCharge'    => $newlineCharge, 
        ]);
    }

}