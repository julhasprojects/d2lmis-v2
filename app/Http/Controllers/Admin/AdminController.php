<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Backend\Admin;
use App\Model\Backend\Branch;
use App\Model\Frontpanel\Employee;
use App\Model\Hrm\Employees;
use App\Model\Frontpanel\Clientcomplain;
use App\Model\Frontpanel\Empoffice;
use App\Model\Frontpanel\Region;
use DB;
use Session;
use Illuminate\Support\Facades\Hash;
use Excel;
use App\Model\Frontpanel\Register; 
use Auth;

class AdminController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function index() {

        $admin_list = DB::table( 'register_info' )
            ->select( 'register_info.*' )
            ->whereNotNull('emp_id')
            ->whereNotIn('emp_id',[999])
            ->whereNull('deleted_at')
            ->orderby('id','desc')
            ->paginate(100);

        $deactiveClient = DB::table( 'register_info' )
            ->select( 'register_info.*' )
            ->whereNotNull('emp_id')
            ->whereNotNUll('deleted_at')
            ->whereNotIn('emp_id',[999])
            ->orderby('id','desc')
            ->paginate(100);

        $allEmployer = DB::table( 'employees' )
            ->leftjoin('register_info','employees.employeeID','=','register_info.emp_id')
            ->where('employees.status',1)
            ->whereNotIn('employees.employeeID',[999])
            ->whereNull('register_info.deleted_at')
            ->get();
           
        return view( 'backend.admin.add_admin' ,[
            'employee'       => $allEmployer ,
            'deactiveClient' => $deactiveClient,
            'admin_list'     => $admin_list,
        ]);
    }

    /**
     * [Store description]
     * @param  Request $request [Ajax posting submit admin ]
     * @return [post]           [description]
     */
    public function store(Request $request) {


        $register = new Register(); 

        if( Session::get('user_id') ) {

            $currentUser = Session::get('user_id');

        } else {

            $currentUser = 0;

        }

        $existing_user = DB::table('register_info')
           ->select('register_info.email')
           ->where('email', $request->input('email'))
           ->count();

        # Chack alrady existing return
        if( $existing_user > 0 ) {
            
            return 200;

        } else {


            if ( ( \Input::get('password') == \Input::get('repassword') ) ) {

                $register->name        = \Input::get( 'fullName' );
                $register->emp_id      = $request->id;
                $register->mobile      = \Input::get( 'mobileNumber' );
                $register->email       = \Input::get('email');
                $register->username    = \Input::get( 'username' );
                $register->password    = Hash::make( \Input::get("password"));
                $register->re_password = Hash::make( \Input::get("repassword"));
                $register->valid_form  = \Input::get( 'valid_from' );
                $register->valid_to    = \Input::get( 'valid_to' );
                $register->status      = \Input::get( 'status' );
                $register->entry_by    = $currentUser;
                $register->save();

            } else {

                return 201;
                
            }

            return redirect( 'admin-information' )->with('removeajax', 'active');
        }
                 
    }

    public function employeeShow($find_info) {

        $employee = DB::select( DB::raw("SELECT * FROM tbl_employees where fullName like '%$find_info%' or employeeID like '%$find_info%' ") );
            
        return view( 'backend.admin.employee-live-search-result' )->with( 'employee', $employee );
       
    }

    public function employeeInfo($find_info) {

        $today = Date('Y-m-d');
   
        $employee = Employees::where('employeeID',$find_info)->first();

        $existing_user = DB::table('register_info')
           ->where('emp_id', $employee->employeeID)
           ->first();

    

        if(isset($existing_user)) {

            return 200;   

        }
     
        return view( 'backend.admin.ajax-search-user-info', [
            'info'     => $employee,
            'branch'   => Branch::all(),
            'today'    => $today,
        ]);

       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function all_admin() {
        
        $admin_list = DB::table( 'register_info' )
            ->select( 'register_info.*' )
            ->whereNotNull('emp_id')
            ->whereNull('deleted_at')
            ->orderby('id','desc')
            ->paginate(100);
                          
        $content =  view( 'backend.admin.admin_list' )->with( 'admin_list',$admin_list );
        return view( 'backend.index')->with( 'content_area', $content );
    }
        /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show( $find_info ) {

        $admin_list = Register::whereNull('deleted_at')
            ->where(function($query) use($find_info) {
                $query->orWhere('emp_id', 'LIKE',  '%'.$find_info.'%' );
                $query->orWhere('username', 'LIKE',  '%'.$find_info.'%' );
                $query->orWhere('email', 'LIKE',  '%'.$find_info.'%' );
                $query->orWhere('mobile', 'LIKE',  '%'.$find_info.'%' );
                $query->where('name', 'LIKE',  '%'.$find_info.'%' );
            })
            ->whereNotIn('emp_id',[999])
            ->get();
        
        return view( 'backend.admin.admin_live_search_result' )->with( 'admin_list', $admin_list );
             
    }

    /**
     * 
     */
     public function branchRegionSearch(  ) {

        $branchId = \Input::get('branchId');

        $region = Region::where('branch_id',$branchId)->get();

        echo "<option value=''> Select </option>";
        
        foreach ($region as $regionInfo) {
            echo '<option value='.$regionInfo->region_id.'>' . $regionInfo->region_name . '</option>';
        }
    
             
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {

        $admin_list = DB::table( 'register_info' )
           ->select( 'register_info.*' )
           ->where ('emp_id', $id)
           ->first();
                   
        $content =  view( 'backend.admin.update_admin' )->with( 'admin_list',$admin_list );
        return view( 'backend.index')->with( 'content_area', $content ); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request) {

        #validation section
        $this->validate($request, [
            'emp_name'    => 'required',
            'username'    => 'required',
            'password'    => 'required',
            're_password' => 'required',
        ]);

        $id          = $request->input( 'id' );  
        $status      = $request->input( 'status' );
        $newPassword = $request->input( 'password' );  
        // $mobile       = $request->input( 'mobile' );  

        if ( !empty($newPassword) ) {

            $password   = Hash::make($newPassword);

        } else {

            $password = $request->input( 're_password' );

        }
        if( $status == 1) {

            $deleted_at = null;

        } else {

            $deleted_at = date('Y-m-d h:i:s');

        }
      
        if ( ( $request->input('password') == $request->input('re_password') ) ) {

            if( isset($id) ) {  
                DB::table( 'register_info' )
                ->where( 'id', $id )
                ->update([  
                    'name'        => $request->input( 'emp_name' ),
                    'mobile'      => $request->input( 'mobile' ),
                    'password'    => $password,
                    're_password' => $password,
                    'username'    => $request->input( 'username' ),
                    'valid_form'  => $request->input( 'valid_form' ),
                    'valid_to'    => $request->input( 'valid_to' ),
                    'status'      => $status,                                           
                    'updated_by'  => Session::get('emp_id'),                                   
                    'deleted_at'  => $deleted_at,                                           
                ]);

                Session::put( 'valid', 1 );
                Session::flash( 'message', 'Profile has successfully updated.' ); 

            } else {

                Session::put( 'invalid', 2 );
                Session::flash( 'message', 'We can not update your profile. Try again.' );    
                
            }

        }  else {
                Session::put( 'valid', 1 );
                Session::flash( 'message', 'Password do not match.' );
                return back();
            }

        return redirect( 'admin-information' )->with('removeajax', 'active');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $req) {
  
        if( Session::get('emp_id') == $req->id ) {

            Session::put( 'valid', 1 );
            Session::flash( 'message', 'Unable to delete your account' );

        } else {
           
            Register::where( 'emp_id', $req->id )
                ->update([                                          
                    'deleted_by' => Session::get('emp_id'),                                          
                    'status'     => 0                                           
                ]);

            $affectedRows = Register::where( 'emp_id', $req->id )->delete();
            return 300;
            /*Session::put( 'valid', 1 );
            Session::flash( 'message', 'User has deleted successfully' );*/
         
        }
      
        // return back()->with('removeajax', 'active'); 
    }

    public function adminPasswordChanged() {

        
        $currentUser = Session::get('emp_id');
        
        $newpassword  = \Input::get('newpassword') ;
        $retypassword = \Input::get('retypassword') ;

        DB::table( 'register_info' )
            ->where( 'emp_id', $currentUser )
            ->update([
                'password'      => Hash::make($newpassword),
                're_password'   => Hash::make($retypassword)     
            ]);

     }
    
}