<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Model\Frontpanel\Clientcomplain; 
use App\Model\Frontpanel\ButtonPermission; 
use App\Model\Frontpanel\Employee; 
use App\Model\Frontpanel\Region; 
use App\Model\Frontpanel\Road; 
use App\Model\Backend\Package;  
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Frontpanel\packages; 
use App\Model\Frontpanel\Techinfo; 
use App\Http\Model\Frontpanel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail ;
use Session;
use DB;


class AdminAllowController extends Controller
{
    /**
     * Display admin Dashboard.
     *
     * @return Response
     */

    public function index() {

        $employee =  DB::table('register_info')
            ->select('name','emp_id')
            ->get();

        $permissionInfo = ButtonPermission::get();

        $content =  view('frontend.request.accept-button',   
            [
                'employee'       => $employee,  
                'permissionInfo' => $permissionInfo,  
            ]);

        return view( 'backend.index')->with( 'content_area', $content);
    }

    public function store() {
        
        $buttonPermission= new ButtonPermission();
        $buttonPermission->report_name       = \Input::get('reportName');
        $buttonPermission->report_permission = \Input::get('employee');
        $buttonPermission->report_remarks    = \Input::get('remarks');

        if($buttonPermission->save()) {

            Session::put('valid', '1');
            Session::flash( 'message', 'Your information was saved successfully!' );

        } else {

            Session::put('invalid', '1');
            Session::flash( 'message', 'Sorry! we cannot save your information.' );
            
        }
        return redirect( 'accept-button-control-panel' );
       
    }
   
}