<?php

namespace App\Http\Controllers\Key;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Backend\Company;

class VerificationController extends Controller
{
    private $private_key;
    private $public_key;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->private_key = @file_get_contents(asset('keys/dsa_priv.pem'), FILE_USE_INCLUDE_PATH);
        //read the public Key from disk
        // $this->public_key = file_get_contents(asset('keys/dsa_pub.pem'), FILE_USE_INCLUDE_PATH);
        $this->public_key = @file_get_contents(asset('keys/dsa_pub.txt'), FILE_USE_INCLUDE_PATH);

        if(!$this->private_key) {
            $this->private_key = "";
        }

        if(!$this->public_key) {
            $this->public_key = "";
        }

        $info = Company::first();

        $product_code = $name = $email = $phone = "";

        if(!empty($info)) {

            $product_code   = $info->phone; 
            $name           = $info->company_name;
            $email          = $info->email;
            $phone          = $info->phone;
        }
        

        // var_dump($this->public_key, $this->private_key);

        // $database_name =  DB::connection()->getDatabaseName();

        // $database_host     = \Config::get('database.connections.mysql.host');
        // $database_user     = \Config::get('database.connections.mysql.username');
        // $database_password = \Config::get('database.connections.mysql.password');

        $data = [];
        $data['db_host']        = \Config::get('database.connections.mysql.host');;
        $data['db_name']        = DB::connection()->getDatabaseName();
        $data['db_user']        = \Config::get('database.connections.mysql.username');
        $data['db_password']    = \Config::get('database.connections.mysql.password');
        $data['private_key']    = \Config::get('database.connections.mysql.password');
        $data['public_key']     = \Config::get('database.connections.mysql.password');

        $data['product_code']     = $product_code;
        $data['company_name']     = $name;
        $data['company_email']    = $email;
        $data['company_phone']    = $phone;

        $direct_api_url = "";

        dd($data);

        # REQUEST SEND TO SSLCOMMERZ
    
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $direct_api_url );
        curl_setopt($handle, CURLOPT_TIMEOUT, 30);
        curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($handle, CURLOPT_POST, 1 );
        curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, FALSE); # KEEP IT FALSE IF YOU RUN FROM LOCAL PC


        $content = curl_exec($handle);

        $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

        if($code == 200 && !( curl_errno($handle))) {
            curl_close( $handle);
            $response = $content;
        } else {
            curl_close( $handle);
            echo "FAILED TO CONNECT WITH API";
            // exit;
        }

        # PARSE THE JSON RESPONSE
        $result = json_decode($response, true );
        var_dump($content);
        dd($result);
        // $product_code = "1001";
        // $name = "ISPERP";
        // $email = "raselcse10@gmail.com";

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
