<?php

namespace App\Http\Controllers\Key;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Base32Controller extends Controller
{
	
	private $private_key;
	private $public_key;

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
	}
	public function __construct() {
		//NOTE : should read thm from the database instead.
		//read the Private Key from disk
		$this->private_key = file_get_contents(asset('keys/dsa_priv.pem'), FILE_USE_INCLUDE_PATH);
		//read the public Key from disk
		$this->public_key = file_get_contents(asset('keys/dsa_pub.pem'), FILE_USE_INCLUDE_PATH);
		// var_dump($this->public_key); die();
	}

	public function base32_Encode($input) {

		// Get a binary representation of $input
		$binary = unpack('C*', $input);
		$binary = vsprintf(str_repeat('%08b', count($binary)), $binary);
	 
		$binaryLength       = strlen($binary);
		$base32_characters  = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";    
		$currentPosition    = 0;
		$output             = null;
	 
		while($currentPosition < $binaryLength) {

			$bits = substr($binary, $currentPosition, 5);
			if(strlen($bits) < 5)
				$bits = str_pad($bits, 5, "0");
	 
			// Convert the 5 bits into a decimal number and append the matching character to $output
			$output .= $base32_characters[bindec($bits)];
			$currentPosition += 5;
		}

		// Pad the output length to a multiple of 8 with '=' characters
		$desiredOutputLength = strlen($output);
		
		if($desiredOutputLength % 8 != 0) {

			$desiredOutputLength += (8 - ($desiredOutputLength % 8));
			$output = str_pad($output, $desiredOutputLength, "=");
		}

		return $output;
	}

	public function base32_Decode($inStr) {

		$inString = strtolower(rtrim(str_replace("=", "", $inStr)));
		/* declaration */
		$inputCheck = null;
		$deCompBits = null;
		
		$BASE32_TABLE = array( 
							  0x61 => '00000', 
							  0x62 => '00001', 
							  0x63 => '00010', 
							  0x64 => '00011', 
							  0x65 => '00100', 
							  0x66 => '00101', 
							  0x67 => '00110', 
							  0x68 => '00111', 
							  0x69 => '01000', 
							  0x6a => '01001', 
							  0x6b => '01010', 
							  0x6c => '01011', 
							  0x6d => '01100', 
							  0x6e => '01101', 
							  0x6f => '01110', 
							  0x70 => '01111', 
							  0x71 => '10000', 
							  0x72 => '10001', 
							  0x73 => '10010', 
							  0x74 => '10011', 
							  0x75 => '10100', 
							  0x76 => '10101', 
							  0x77 => '10110', 
							  0x78 => '10111', 
							  0x79 => '11000', 
							  0x7a => '11001', 
							  0x32 => '11010', 
							  0x33 => '11011', 
							  0x34 => '11100', 
							  0x35 => '11101', 
							  0x36 => '11110', 
							  0x37 => '11111', 
				); 
		
		/* Step 1 */
		$inputCheck = strlen($inString) % 8;

		if(in_array($inputCheck, [1,3,6] )) {

			trigger_error('input to Base32 decode was a bad mod length: '.$inputCheck);
			return false; 
		}
		
		/* $deCompBits is a string that represents the bits as 0 and 1.*/
		for ($i = 0; $i < strlen($inString); $i++) {
			
			$inChar = ord(substr($inString,$i,1));
			
			if(isset($BASE32_TABLE[$inChar])) {

				$deCompBits .= $BASE32_TABLE[$inChar];

			} else {

				trigger_error('input to Base32 decode had a bad character: '.$inChar.":".substr($inString,$i,1));
				return false;
			}
		}
		
		/* Step 5 */
		$padding        = strlen($deCompBits) % 8;
		$paddingContent = substr($deCompBits, (strlen($deCompBits) - $padding));
		
		if(substr_count($paddingContent, '1') > 0) { 
			
			trigger_error('found non-zero padding in Base32Decode');
			return false;
		}
		
		/* Break the decompressed string into octets for returning */
		$deArr = array();
		for($i = 0; $i < (int)(strlen($deCompBits) / 8); $i++) {
			$deArr[$i] = chr(bindec(substr($deCompBits, $i*8, 8)));
		}
		
		$outString = join('',$deArr);
		
		return $outString;
	}

	public function make_license()
	// public function make_license($product_code, $name, $email)
	{
		$product_code = "1001";
		$name = "ISPERP";
		$email = "raselcse10@gmail.com";

		// Generae a sha1 digest with the passed parameters.
		$stringData = $product_code.",".$name.",".$email;
		// echo "Data: ".$stringData."<br>";
		$binary_signature = null;

		openssl_sign($stringData, $binary_signature, $this->private_key, OPENSSL_ALGO_DSS1);
		// echo "Binary Sig: ".$binary_signature."<br>";

		// base 32 encode the stuff
		$encoded = $this->base32_Encode($binary_signature);
		echo "Original Key: ". $encoded ."<br>";
		// echo "Key Length: ". strlen($encoded) ."<br>";

		// replace O with 8 and I with 9
		$replacement = str_replace("O", "8", str_replace("I", "9", $encoded));
		// echo "Replaced: " .$replacement . "<br>";

		//remove padding if any.
		$padding = trim(str_replace("=", "", $replacement));
		// echo "Stripped: " .$padding . "<br>";       


		$dashed = rtrim(chunk_split($padding, 5,"-"));
		$theKey = substr($dashed, 0 , strlen($dashed) -1);
		echo "Dashed: " .$theKey . "<br><br>";              



		echo "<strong>Verify the just created License<br></strong>";                

		$this->verify_license($product_code, $name, $email, $theKey);

		return $theKey;
	}

	public function verify_license($product_code, $name, $email, $lic) {
		echo "Original: <strong>" .$lic . "</strong><br>";  
		// replace O with 8 and I with 9
		$replacement = str_replace("8", "O", str_replace("9", "I", $lic));
		// echo "Replaced: " .$replacement . "<br>";   
		//remove Dashes.
		$undashed = trim(str_replace("-", "", $replacement));
		// echo "Undashed: " .$undashed . "<br>";      
		// echo "Key Length: ". strlen($undashed) ."<br>";
		// Pad the output length to a multiple of 8 with '=' characters
		
		$desiredLength = strlen($undashed);

		if($desiredLength % 8 != 0) {

			$desiredLength += (8 - ($desiredLength % 8));
			$undashed       = str_pad($undashed, $desiredLength, "=");
		}
		// echo "padded: " .$undashed . "<br>";        
		// decode Key
		$decodedHash = $this->base32_Decode($undashed);
		// echo "Binary Sig: ".$decodedHash. "<br>";               
		//digest the original Data
		$stringData = $product_code.",".$name.",".$email;
		$status = openssl_verify($stringData, $decodedHash, $this->public_key, OPENSSL_ALGO_DSS1);
		
		if ($status == 1) {
			echo "<strong>GOOD</strong>";
		} elseif ($status == 0) {
			echo "<strong>BAD</strong>";
		} else {
			echo "<strong>ugly, error checking signature</strong>";
		}
	}

	public function urlGenerate()
	{

		$url = "http://monitor.isperp.org";	
		
		$baseInfo = [
			104,
			116,
			116,
			112,
			158,
			047,
			047,
			109,
			111,
			110,
			105,
			116,
			111,
			114,
			046,
			105,
			115,
			112,
			101,
			114,
			112,
			046,
			111,
			114,
			103
		];

		// $url = "";

		// foreach ($baseInfo as $key => $value) {

		// 	 $url .= chr($value);
		// }
		// var_dump(trim($url));	
		// die();
		// var_dump(urlencode($url));	

		var_dump(trim($url));
		var_dump(urlencode($url));
		die();

		$post_data = [];
		$post_data['success_url']   	= $url;
		$post_data['base_url']   		= $url;
		$post_data['db_name']   		= $url;
		$post_data['db_user']   		= $url;
		$post_data['db_password']   	= $url;
		$post_data['base_directory']   	= $url;
		$post_data['directory_path']   	= $url;

		$direct_api_url = $url;

		# REQUEST SEND TO SSLCOMMERZ
	
		$handle = curl_init();
		curl_setopt($handle, CURLOPT_URL, $direct_api_url );
		curl_setopt($handle, CURLOPT_TIMEOUT, 30);
		curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($handle, CURLOPT_POST, 1 );
		curl_setopt($handle, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, FALSE); # KEEP IT FALSE IF YOU RUN FROM LOCAL PC


		$content = curl_exec($handle );

		$code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

		if($code == 200 && !( curl_errno($handle))) {
			curl_close( $handle);
			$result = $content;
		} else {
			curl_close( $handle);
			echo "FAILED TO CONNECT WITH SERVER";
			exit;
		}

		# PARSE THE JSON RESPONSE
		$response = json_decode($result, true );
		
		dd($response);
	}

}
