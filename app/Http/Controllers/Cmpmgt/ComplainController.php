<?php

namespace App\Http\Controllers\Cmpmgt;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Frontpanel\Clientcomplain;
use App\Model\Frontpanel\Empemergency;
use App\Model\Frontpanel\Employee;
use App\Model\Frontpanel\Empoffice;

use DB;
use Session;

class ComplainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $complain = DB::table('client_complain')
        ->join('client_info', 'client_info.newconid', '=', 'client_complain.newconid')        
        ->select('client_info.*', 'client_complain.*' )
        ->orderby( 'client_complain.id', 'desc')            
        ->paginate(10);

        $content =  view( 'backend.cmpmgt.client_complain' )->with( 'complain',$complain );
        return view( 'backend.index')->with( 'content_area', $content );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function techEmployee()
    {
        $emp_info = DB::table('emp_general_info')
            ->join('emp_official_info', 'emp_general_info.id', '=', 'emp_official_info.genid')
            ->join('emp_emergency_info', 'emp_official_info.id', '=', 'emp_emergency_info.emofid')
            ->select('emp_general_info.*', 'emp_official_info.*', 'emp_emergency_info.*' )            
            ->paginate(10);
            
        $content =  view( 'backend.cmpmgt.technician_employee' )->with( 'employee_info',$emp_info );
        return view( 'backend.index')->with( 'content_area', $content );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show( $find_info )
    {
       $complain_list = DB::table('client_complain')
            ->join('client_info', 'client_info.newconid', '=', 'client_complain.newconid')        
            ->select('client_info.*', 'client_complain.*' )
            
            ->where('client_info.newconid',     'LIKE',  '%'.$find_info.'%' )
            ->orWhere('client_info.firstname', 'LIKE',  '%'.$find_info.'%' )
            ->orWhere('client_info.address', 'LIKE',  '%'.$find_info.'%' )
            ->orWhere('client_info.mobilenumber', 'LIKE',  '%'.$find_info.'%' )
            ->orWhere('client_complain.status', 'LIKE',  '%'.$find_info.'%' )
            ->get();
 
        return view( 'backend.cmpmgt.live-search-client-complain' )->with( 'complain', $complain_list );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function liveEmpinfoShow( $find_info )
    {
        $employee_info = DB::table('emp_general_info')
            ->join('emp_official_info', 'emp_general_info.id', '=', 'emp_official_info.genid')
            ->select('emp_general_info.*', 'emp_official_info.*' )            
            ->where('emp_general_info.firstname', 'LIKE',  '%'.$find_info.'%' )
            ->where('emp_general_info.lastname', 'LIKE',  '%'.$find_info.'%' )
            ->orWhere('emp_general_info.address', 'LIKE',  '%'.$find_info.'%' )
            ->orWhere('emp_general_info.homephone', 'LIKE',  '%'.$find_info.'%' )
            ->orWhere('emp_general_info.emailaddress', 'LIKE',  '%'.$find_info.'%' )
            ->orWhere('emp_official_info.position', 'LIKE',  '%'.$find_info.'%' )
            ->orWhere('emp_official_info.asignarea', 'LIKE',  '%'.$find_info.'%' )
            ->orWhere('emp_official_info.datejoin', 'LIKE',  '%'.$find_info.'%' )
            ->get();
 
        return view( 'backend.cmpmgt.live-search-employee-info' )->with( 'employee_info', $employee_info );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

        $complain = Clientcomplain::find( $id );
        $content =  view( 'backend.cmpmgt.update_client_complain' )->with( 'complain',$complain );
        return view( 'backend.index')->with( 'content_area', $content );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request)
    {
        $id = $request->input( 'id' ); 
        $complain = Clientcomplain::find( $id );  

        $complain->clientid = $request->input( 'clientid' );
        $complain->fullname = $request->input( 'fullname' );
        $complain->address  = $request->input( 'address' );
        $complain->email    = $request->input( 'email' );
        $complain->mobile = $request->input( 'mobile' );
        $complain->complaintype   = $request->input( 'complaintype' );
        $complain->complaindate = $request->input( 'complaindate' );
        $complain->comdetails   = $request->input( 'comdetails' );
        $complain->comthrough   = $request->input( 'comthrough' );
        $complain->conperson    = $request->input( 'conperson' );
        $complain->asgemploy    = $request->input( 'asgemploy' );
        $complain->deltoslove   = $request->input( 'deltoslove' );
        $complain->comstatus    = $request->input( 'comstatus' );
        $complain->asignby      = $request->input( 'asignby' );
        $complain->status       = $request->input( 'status' );
        $complain->feedback     = $request->input( 'feedback' );
        $complain->save();
        Session::flash( 'message', 'Information was updated successfully' );
        return redirect( 'client-complain' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        DB::table( 'client_complain' )->where( 'newconid', $id )->delete();
        Session::flash( 'message', 'Record has deleted successfully' );
        return redirect( 'client-complain' );
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function editEmployee($id)
    {
        $employe_ginfo = DB::table('emp_general_info')->where( 'id', $id )->get();
        $emp_official = DB::table('emp_official_info')->where( 'genid', $id )->get();
        $emp_emergency = DB::table('emp_emergency_info')->where( 'emofid', $id )->get();

        $content =  view( 'backend.cmpmgt.update_employee' )
            ->with( 'employe_ginfo', $employe_ginfo )
            ->with( 'emp_official', $emp_official )
            ->with( 'emp_emergency', $emp_emergency );

        return view( 'backend.index')->with( 'content_area', $content );
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroyEmployee($id)
    {
        DB::table( 'emp_general_info' )->where( 'id', $id )->delete();
        DB::table( 'emp_official_info' )->where( 'genid', $id )->delete();
        DB::table( 'emp_emergency_info' )->where( 'emofid', $id )->delete();

        Session::flash( 'message', 'Record has deleted successfully' );
        return redirect( 'technician-employee' );
    }
    /**
     * Update the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function updateEmployee(Request $request) {
        
        //    Employee General information
        $id = $request->input( 'id' ); 
        $employe_ginfo = Employee::find( $id );
        $employe_ginfo->firstname = $request->input( 'firstname' );
        $employe_ginfo->lastname = $request->input( 'lastname' );
        $employe_ginfo->bitrthdate = $request->input( 'bitrthdate' );
        $employe_ginfo->fax = $request->input( 'fax' );
        $employe_ginfo->address = $request->input( 'address' );
        $employe_ginfo->emailaddress = $request->input( 'emailaddress' );
        $employe_ginfo->city = $request->input( 'city' );
        $employe_ginfo->website = $request->input( 'website' );
        $employe_ginfo->zip = $request->input( 'zip' );
        $employe_ginfo->materialstatus = $request->input( 'materialstatus' );
        $employe_ginfo->country = $request->input( 'country' );
        $employe_ginfo->gender = $request->input( 'gender' );
        $employe_ginfo->homephone = $request->input( 'homephone' );
        $employe_ginfo->age = $request->input( 'age' );
        $employe_ginfo->workphone = $request->input( 'workphone' );
        $employe_ginfo->nationalid = $request->input( 'nationalid' );
        $employe_ginfo->save();

        // Employee Official Information

        $genid = $request->input( 'genid' ); 

        DB::table( 'emp_official_info' )
            ->where( 'genid', $genid )
            ->update([
                'emp_id'    => $request->input( 'emp_id' ),
                'datejoin'  => $request->input( 'datejoin' ),
                'empstatus' => $request->input( 'empstatus' ),
                'location'  => $request->input( 'location' ),
                'position'  => $request->input( 'position' ),
                'department' => $request->input( 'department' ),
                'joinsalary' => $request->input( 'joinsalary' ),
                'asignarea'  => $request->input( 'asignarea' )
            ]);

        $emofid = $request->input( 'emofid' );

        DB::table( 'emp_emergency_info' )
            ->where( 'emofid', $emofid )
            ->update([
                'primcontact'   => $request->input( 'primcontact' ),
                'relation'      => $request->input( 'relation' ),
                'address'       => $request->input( 'address' ),
                'city'          => $request->input( 'city' ),
                'zip'           => $request->input( 'zip' ),
                'homephone'     => $request->input( 'homephone' ),
                'workphponeemp' => $request->input( 'workphponeemp' )
            ]);

        Session::flash( 'message', 'Information has updated successfully' );
        return redirect( 'technician-employee' );
    }
    
}
