<?php

namespace App\Http\Controllers\Report\Emp;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Frontpanel\Employee; 
use App\Model\Frontpanel\Empoffice; 
use App\Model\Hrm\Employees; 
use App\Model\Frontpanel\Empemergency; 
use Session;
use DB;


class EmpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function allEmpList() {

        $emp_list = DB::select( DB::raw("SELECT * FROM tbl_employees where employeeID  not in ('999')") );
        $content  = view( 'backend.employee.report.all-employee-list' )->with( 'emp_list',$emp_list );
       
        return view( 'backend.index')->with( 'content_area', $content );

    }

    public function allEmpListEdit($employeeID) { 

       $emp_list =  DB::select( DB::raw(" SELECT emp.employeeID,emp.fullName,deg.designation,emp.email,emp.mobileNumber,emp.localAddress FROM tbl_department dept 
            INNER JOIN tbl_designation deg ON deg.deptID=dept.id 
            LEFT JOIN tbl_employees emp ON emp.designation=deg.id
            WHERE   emp.employeeID=$employeeID
             GROUP BY emp.employeeID
        "));
     
        $content =  view( 'backend.employee.report.master-employee-selection-edit',[
           'emp_list' => $emp_list 
        ]);
        return view( 'backend.index')->with( 'content_area', $content ); 

    }
    public function allEmpListDestroy($employeeID) {
        
        $emp_list =  DB::select( DB::raw("
            DELETE FROM tbl_employees
            WHERE employeeID= '$employeeID'"));

        Session::put( 'valid', 1 );
        Session::flash( 'message', 'Record Deleted successfully' );
        return redirect( '/hrm-all-reports' );

    }

    public function dateWishSearch() {

        $designation   = \Input::get('designation');
      
        $emp_details = Employees::leftjoin('designation','employees.designation','=','designation.id')
            ->leftjoin('department','designation.deptID','=','department.id')
            ->where('employees.designation',$designation)
            ->whereNotIn('employees.employeeID',[999])
            ->get();
            
        return  view( 'backend.employee.report.all-employee-search',[
            'designationWiseEmployeeDetails' => $emp_details
        ]);
       
         
    }
   
    public function individualEmpInfoBar() {

        $find   = \Input::get('individual-employee-report');
        $emp_list =DB::select( DB::raw("SELECT * FROM tbl_employees WHERE employeeID LIKE '$find%' OR email='$find' OR mobileNumber LIKE '$find%' OR designation LIKE '$find%' OR fullName LIKE '$find%'") );
        return view( 'backend.employee.report.employee-search-result' )->with('emp_list', $emp_list);    
   
    }

    public function EmpInfoByGender($find) {
        $emp_list = DB::table('register_info')
            ->where('gender',     'LIKE',  '%'.$find.'%' ) 
            ->get();
        return view( 'backend.employee.report.employee-search-result' )->with('emp_list', $emp_list);    
    }

    public function EmpInfoByDepartment($find)  {

        $emp_list = DB::table('emp_official_info')
            ->join('emp_general_info','emp_general_info.emp_id','=','emp_official_info.emp_id')       
            ->select('emp_general_info.*','emp_official_info.*' )
            ->where('emp_official_info.department', 'LIKE',  '%'.$find.'%' ) 
            ->get();

        return view( 'backend.employee.report.employee-search-result' )->with('emp_list', $emp_list);    
    }
    
    public function EmpInfoByDesignation($find)
    {
        $emp_list = DB::table('emp_official_info')
            ->join('emp_general_info','emp_general_info.emp_id','=','emp_official_info.emp_id')       
            ->select('emp_general_info.*','emp_official_info.*' )
            ->where('emp_official_info.position',     'LIKE',  '%'.$find.'%' ) 
            ->get();

        return view( 'backend.employee.report.employee-search-result' )->with('emp_list', $emp_list);    
    }
    
    public function EmpInfoByDate(Request $request)
    {
        $from     = $request->input( 'datefrom' );
        $to       = $request->input( 'dateto' );

        $emp_list = DB::table('emp_official_info')
            ->join('emp_general_info','emp_general_info.emp_id','=','emp_official_info.emp_id')       
            ->select('emp_general_info.*','emp_official_info.*' )
            ->whereBetween('emp_official_info.datejoin', [$from, $to])
            ->orderby('emp_general_info.emp_id','desc')
            ->paginate(10);

        $content =  view( 'backend.employee.report.master-employee-selection',[
            'emp_list' => $emp_list 
        ]);
        return view( 'backend.index')->with( 'content_area', $content ); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function masterEmployeeAll()
    {
        $emp_list = DB::table('emp_general_info')
            ->join('emp_official_info','emp_official_info.emp_id','=','emp_general_info.emp_id')
            ->join('emp_emergency_info','emp_emergency_info.emp_id','=','emp_general_info.emp_id')
            ->select('emp_general_info.*','emp_official_info.*','emp_emergency_info.*')
            ->paginate(10);

        $content =  view( 'backend.employee.report.master-employee-selection',[
            'emp_list' => $emp_list 
        ]);
            
        return view( 'backend.index')->with( 'content_area', $content ); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function empDetails($id) {

        $emp_details = Employees::leftjoin('designation','employees.designation','=','designation.id')
            ->leftjoin('department','designation.deptID','=','department.id')
            ->where('employees.employeeID',$id)
            ->first();
        
        $content =  view( 'backend.employee.report.individual-employee-details',[
            'emp_details' => $emp_details
        ]);

        return view( 'backend.index')->with( 'content_area', $content );    

    }
    

}
