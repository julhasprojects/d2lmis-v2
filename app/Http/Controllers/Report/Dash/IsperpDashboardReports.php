<?php

namespace App\Http\Controllers\Report\Dash;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Frontpanel\Empoffice;
use App\Model\Backend\Date;
use App\Model\Frontpanel\Register;

use Session;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Mail;

use Illuminate\Support\Facades\Notification;
class IsperpDashboardReports extends Controller
{  
	
	/**
	 * [isperpDashboard description]
	 * @return [type] [description]
	 */
	public function isperpDashboard() {
		
		if( Session::get('user_id') ) {

			$monthYear    = explode(',', Date::monthYear() );
			$today 	      = date('Y-m-d');
			$empID        = Session::get('user_id');
			$email        = Session::get('email');
			$year         = date("Y");
			$currentTime  = strtotime($year);
			
			$yesterDay    = date('Y-m-d', strtotime('-1 day', strtotime($year))); 
			$tommorrow    = date('Y-m-d', strtotime('+1 day', strtotime($year))); 


			$startDay = Date::StartdayEndday('0');
        	$endDay   = Date::StartdayEndday('1');
        	
        	
        	# L ogin hostory admin log update information
        	$system_ip  = $_SERVER['REMOTE_ADDR'];
	        $user_agent = $_SERVER['HTTP_USER_AGENT'];
	        $login_date = date('Y-m-d H:i:s');

        	
	      	$branchId = Session::get('branchId');

			return  view( 'backend.d2lmis-dashboard');

	  } else {

	  	return Redirect::to('/');

	  }
	}

	
}
