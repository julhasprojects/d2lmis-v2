<?php

namespace App\Http\Controllers\Report\Dash;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Frontpanel\Empoffice;
use App\Model\Backend\Date;
use Session;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class DashComplainReportController extends Controller
{  
	

	public function isperpDashboard() {

		$monthYear = explode(',', Date::monthYear() );
		$today 	   = date('Y-m-d');

		$year        = date("Y");
		$currentTime = strtotime($year);
		$final_con 	 = strtotime('-1 day', $currentTime);
		$yesterDay  = date('Y-m-d', $final_con); 

        $endMonth = $month = date('m',strtotime($monthYear[0] .'-'.$monthYear[1] )); 
		$month    = $month - 1 ;
		$year     = $monthYear[1];

		if ($month == 0) {
			$currentMonth = 12;
			$currentYear  = $year - 1;
		} else {
			$currentMonth = $month;
			$currentYear  = $year;
		}

		$startDay 	=  $currentYear . '-0'. $currentMonth . '-'. $monthYear[4];		
		$endDay 	=  $monthYear[1] . '-'. $endMonth . '-'. $monthYear[5];
		
		return  view( 'backend.isperp-dashboard');

	}

}