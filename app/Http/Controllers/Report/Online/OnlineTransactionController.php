<?php

namespace App\Http\Controllers\Report\Online;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Backend\OnlineTransection; 
use App\Model\Backend\Date; 
use App\Model\Frontpanel\Region; 
use DB;

class OnlineTransactionController extends Controller
{
    public function index() {

    	$monthYear  = explode(',', Date::monthYear() );
    	
    	// Online Transection
    	$onlineTransection = OnlineTransection::groupBy('payment_method')
    		->select(DB::raw('SUM(paid_amount) as totalPayemnt'),'payment_method')
    		->where('billmonth',$monthYear[8])
    		->where('billyear',$monthYear[1])
    		->get('paid_amount');

    	

    	// Online Monthly Transection
    	$onlineMonthlyTransection = OnlineTransection::select(DB::raw('SUM(paid_amount) as totalPayemnt'),'billmonth')
    		->groupBy('billmonth')
    		->get('paid_amount');

    	// Online Monthly Transection
    	$onlineDailyTransection = OnlineTransection::select(DB::raw('SUM(paid_amount) as totalPayemnt'),'collection_date')
    		->groupBy('collection_date')
    		->get('paid_amount');

        // Online customer Type Search
        $onlineCustomerTypeTransection = OnlineTransection::leftjoin('client_info','transaction_history.newconid','=','client_info.newconid')
            ->select(DB::raw('SUM(paid_amount) as totalPayemnt'),'client_info.customertype')
            ->groupBy('client_info.customertype')
            ->where('transaction_history.billmonth',$monthYear[8])
            ->where('transaction_history.billyear',$monthYear[1])
            ->get();
      
    	return view('backend.billing.online-billing.index',[
    		'onlineTransection'             => $onlineTransection,
    		'onlineMonthlyTransection'      => $onlineMonthlyTransection,
            'onlineDailyTransection'        => $onlineDailyTransection,
    		'onlineCustomerTypeTransection' => $onlineCustomerTypeTransection,
    		
    	]);
    }

    /**
     * [onlineCheckoutReports description]
     * @return [type] [description]
     */
    public function onlineCheckoutReports() {

        $monthYear  = explode(',', Date::monthYear() );
        
        // Online Transection
        $onlineTransection = OnlineTransection::groupBy('payment_method')
            ->select(DB::raw('SUM(paid_amount) as totalPayemnt'),'collection_date')
            
            ->where('billyear',$monthYear[1])
            ->groupBy('collection_date')
            ->get();
        
        return view('backend.billing.online-billing.checkout',[
            'onlineTransection' => $onlineTransection,
            'currentMonth'      => $monthYear[0],
            'currentYear'       => $monthYear[1],
        ]);
    }

    /**
     * [checkout Master DateSearch description]
     * @return [type] [description]
     */
    public function checkoutMasterDateSearch() {

        $monthYear  = explode(',', Date::monthYear() );
        $from = \Input::get('from');
        $to   = \Input::get('to');

        // Online Transection
        $onlineTransection = OnlineTransection::groupBy('payment_method')
            ->select(DB::raw('SUM(paid_amount) as totalPayemnt'),'collection_date')
            
            ->where('billyear',$monthYear[1])
            ->whereBetween('collection_date',[$from,$to])
            ->groupBy('collection_date')
            ->get();
        
        return view('backend.billing.online-billing.checkout-search',[
            'onlineTransection' => $onlineTransection,
            'currentMonth'      => $monthYear[0],
            'currentYear'       => $monthYear[1],
        ]);
    }

    public function checkOutMasterMonthlyeSearch() {

        $monthYear     = explode(',', Date::monthYear() );
        $checkoutMonth = \Input::get('checkoutMonth');
        $checkoutYear  = \Input::get('checkoutYear');

        // Online Transection
        $onlineTransection = OnlineTransection::groupBy('payment_method')
            ->select(DB::raw('SUM(paid_amount) as totalPayemnt'),'collection_date')
            ->where('billmonth',$checkoutMonth)
            ->where('billyear',$checkoutYear)
            ->groupBy('collection_date')
            ->orderBy('collection_date','asc')
            ->get();
        
        return view('backend.billing.online-billing.checkout-search',[
            'onlineTransection' => $onlineTransection,
            'currentMonth'      => $monthYear[0],
            'currentYear'       => $monthYear[1],
        ]);

    }
    /**
     * [onlineCollection Master Reports]
     * @return [type] [description]
     */
    public function onlineCollectionMasterReports() {

    	$monthYear  = explode(',', Date::monthYear() );
    	// Online Transection
    	$onlineallTransection = OnlineTransection::leftjoin('client_info','transaction_history.newconid','=','client_info.newconid')->where('billmonth',$monthYear[8])
    		->where('billyear',$monthYear[1])
    		->get();

    	$regionName = Region::all();

    	$allPaymentTransection = OnlineTransection::groupBy('payment_method')
    		->select('payment_method')
    		->get();

    	return view('backend.billing.online-billing.master-transection',[
    		'onlineTransection'     => $onlineallTransection,
    		'region'                => $regionName,
    		'currentMonth'          => $monthYear[0],
    		'currentyear'           => $monthYear[1],
    		'allPaymentTransection' => $allPaymentTransection,
    		
    	]);
    }

    /**
     * [onlineCollectionMasterMonthlSearch description]
     * @return [type] [description]
     */
    public function onlineCollectionMasterMonthlySearch() {

    	$month = \Input::get('billSummeryMonth');
    	$year  = \Input::get('billSummeryYear');

    	$onlineallTransection = OnlineTransection::leftjoin('client_info','transaction_history.newconid','=','client_info.newconid')
    		->where('billmonth',$month)
    		->where('billyear',$year)
    		->get();
    	
    	return view('backend.billing.online-billing.master-transection-search',[
    		'onlineTransection' => $onlineallTransection,
    		
    	]);
    }

    /**
     * [onlineCollectionMasterMonthlSearch description]
     * @return [type] [description]
     */
    public function onlineCollectionMasterdateSearch() {

    	$from = \Input::get('from');
    	$to   = \Input::get('to');

    	$onlineallTransection = OnlineTransection::leftjoin('client_info','transaction_history.newconid','=','client_info.newconid')
    		->whereBetween('collection_date',[$from,$to])
    		->get();
    	
    	return view('backend.billing.online-billing.master-transection-search',[
    		'onlineTransection' => $onlineallTransection,
    		
    	]);
    }

    /**
     * [onlineCollectionMasterMonthlSearch description]
     * @return [type] [description]
     */
    public function onlineCollectionMasterPaymentMethodSearch() {


    	$pymntMethodSearch = \Input::get('pymntMethodSearch');

    	$monthYear  = explode(',', Date::monthYear() );

    	$onlineallTransection = OnlineTransection::leftjoin('client_info','transaction_history.newconid','=','client_info.newconid')
    		->where('transaction_history.payment_method',$pymntMethodSearch)
    		->where('transaction_history.billmonth',$monthYear[8])
            ->where('transaction_history.billyear',$monthYear[1])
    		->get();

    	return view('backend.billing.online-billing.master-transection-search',[
    		'onlineTransection' => $onlineallTransection,
    		
    	]);
    }

     /**
     * [onlineCollectionMasterMonthlSearch description]
     * @return [type] [description]
     */
    public function onlineCollectionMasterRegionSearch() {


        $region_search = \Input::get('region_search');

        $monthYear  = explode(',', Date::monthYear() );

        $onlineallTransection = OnlineTransection::leftjoin('client_info','transaction_history.newconid','=','client_info.newconid')
            ->where('client_info.region',$region_search)
            ->where('transaction_history.billmonth',$monthYear[8])
            ->where('transaction_history.billyear',$monthYear[1])
            ->get();
        
        return view('backend.billing.online-billing.master-transection-search',[
            'onlineTransection' => $onlineallTransection,
            
        ]);
    }
     /**
     * [onlineCollectionMasterMonthlSearch description]
     * @return [type] [description]
     */
    public function onlineCustomerTypeSearch() {


        $type = \Input::get('type');
     
        $monthYear  = explode(',', Date::monthYear() );

        $onlineallTransection = OnlineTransection::leftjoin('client_info','transaction_history.newconid','=','client_info.newconid')
            ->where('client_info.customertype',$type)
            ->where('transaction_history.billmonth',$monthYear[8])
            ->where('transaction_history.billyear',$monthYear[1])
            ->get();
        
        return view('backend.billing.online-billing.master-transection-search',[
            'onlineTransection' => $onlineallTransection,
            
        ]);
    }

    /**
     * [onlineTransactionDueBillSummery description]
     * @return [type] [description]
     */
    public function onlineTransactionDueBillSummery() {
         $type = \Input::get('type');
     
        $monthYear  = explode(',', Date::monthYear() );

        $onlineallTransection = OnlineTransection::leftjoin('client_info','transaction_history.newconid','=','client_info.newconid')
            ->select(DB::raw('sum(paid_amount) as totalTxnAmount,sum(service_charge) as totalServiceAmount,sum(store_amount) as totalStoreAmount'),'transaction_history.billmonth','transaction_history.billyear')
            ->groupBy('transaction_history.billmonth')
            
            ->get();
       
        return view('backend.billing.online-billing.transection-due-amount-summery',[
            'onlineTransection' => $onlineallTransection,
            'currentMonth'      => $monthYear[0],
            'currentyear'       => $monthYear[1],
            
        ]);
    }
    
}
