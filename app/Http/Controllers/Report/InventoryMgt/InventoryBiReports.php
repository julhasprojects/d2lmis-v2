<?php

namespace App\Http\Controllers\Report\InventoryMgt;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Frontpanel\Clientcomplain;
use App\Model\Frontpanel\Region;
use App\Model\Frontpanel\Road;
use App\Model\Frontpanel\Newlines;
use App\Model\Frontpanel\Billgenerate;
use App\Model\Backend\BillCollection;
use App\Model\Backend\Date;
use App\Model\Frontpanel\Linedisconnect;
use Session;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class InventoryBiReports extends Controller {  
	
	public function inventoryAllReports() {

 		/*$areaWisebox = DB::select( DB::raw("SELECT count(DISTINCT box_no) totalBox,rg.region_name FROM tbl_client_info ci
			LEFT JOIN tbl_region rg on rg.region_id=ci.region
			LEFT JOIN tbl_instruments ins on ins.newconid=ci.newconid 
			where ci.linestatus='1' GROUP BY region_id "));
 
 		$areaWiseCableInMeter = DB::select( DB::raw("SELECT SUM(cable_meter) totalMeter,rg.region_name FROM tbl_client_info ci
			LEFT JOIN tbl_region rg on rg.region_id=ci.region
			LEFT JOIN tbl_instruments ins on ins.newconid=ci.newconid where ci.linestatus='1' 
			GROUP BY region_id"));*/
 	
		$content =  view( 'backend.bireports.inventory-box-management', [
			/*'areaWisebox'          => $areaWisebox,
			'areaWiseCableInMeter' => $areaWiseCableInMeter,*/
   		]);

		return view( 'backend.index')->with( 'content_area', $content );
	}
}

