<?php

namespace App\Http\Controllers\Report\SMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SMS\SMSHistory; 
use App\Model\Backend\Date; 
use App\Model\Frontpanel\Region; 
class SMSReportController extends Controller
{

   	public function smsConsumptionReports() {
   		$startDay =  Date::StartdayEndday('0');		
		$endDay   =  Date::StartdayEndday('1');
   		$monthYear  = explode(',', Date::monthYear() );
   		$smsHistory = SMSHistory::leftjoin('sms_type','sms_history.module_code','=','sms_type.module_code')
   		
   			->select('sms_history.*','sms_type.message')
   			->whereBetween('sms_history.created_at',[$startDay.' 00:00:00',$endDay.' 23:59:59'])
   			->get();
   		
   		$regionName = Region::all();

   		return view('backend.sms.index',[ 
   			'smsHistory' => $smsHistory,
   			'region'                => $regionName,
    		'currentMonth'          => $monthYear[0],
    		'currentyear'           => $monthYear[1],
   		]);
   	}

   	// SMS Monthly Search
   	public function smsMonthlySearch() {

   		$billSummeryMonth = \Input::get('billSummeryMonth');
   		$billSummeryYear  = \Input::get('billSummeryYear');

   		$starDate = Date($billSummeryYear."-".$billSummeryMonth.'-'.'01');
   		$endDate  = Date($billSummeryYear."-".$billSummeryMonth.'-'.'31');

   		$monthYear  = explode(',', Date::monthYear() );

   		$smsHistory = SMSHistory::leftjoin('sms_type','sms_history.module_code','=','sms_type.module_code')
   			->select('sms_history.*','sms_type.message')
   			->whereBetween('sms_history.created_at',[$starDate.' 00:00:00',$endDate.' 23:59:59'])
   			->get();
   		
   		$regionName = Region::all();

   		return view('backend.sms.sms-search',[ 
   			'smsHistory' => $smsHistory,
   			'region'                => $regionName,
    		'currentMonth'          => $monthYear[0],
    		'currentyear'           => $monthYear[1],
   		]);
   	}

   	/**
   	 * [smsDateSearch description]
   	 * @return [type] [description]
   	 */
   	public function smsDateSearch( ) {

   		$from = \Input::get('from');
   		$to   = \Input::get('to');

   		$monthYear  = explode(',', Date::monthYear() );

   		$smsHistory = SMSHistory::leftjoin('sms_type','sms_history.module_code','=','sms_type.module_code')
   			->select('sms_history.*','sms_type.message')
   			->whereBetween('sms_history.created_at',[$from.' 00:00:00',$to.' 23:59:59'])
   			->get();
   		
   		$regionName = Region::all();

   		return view('backend.sms.sms-search',[ 
   			'smsHistory'   => $smsHistory,
   			'region'       => $regionName,
    		
   		]);

   	}

   	/**
   	 * [SM Rgion Search description]
   	 * @return [type] [description]
   	 */
   	public function smsRgionSearch() {

   		$region_search   = \Input::get('region');
   		$monthYear  = explode(',', Date::monthYear() );
   		$smsHistory = SMSHistory::leftjoin('sms_type','sms_history.module_code','=','sms_type.module_code')
   			->leftjoin('client_info','sms_history.newconid','=','client_info.newconid')
   			->select('sms_history.*','sms_type.message','client_info.region')
   			->where('client_info.region',$region_search)
   			->get();
   		
   		$regionName = Region::all();

   		return view('backend.sms.sms-search',[ 
   			'smsHistory' => $smsHistory,
   			'region'                => $regionName,
    		'currentMonth'          => $monthYear[0],
    		'currentyear'           => $monthYear[1],
   		]);
   	}
}
