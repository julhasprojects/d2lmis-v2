<?php

namespace App\Http\Controllers\PaymentApi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BkashController extends Controller
{
    protected $apiUrl = "https://www.bkashcluster.com:9081/dreamwave/merchant/trxcheck/sendmsg?user=PRIDELIMITED&pass=pR!d31imIt3d&msisdn=01580254808&trxid=CZWEHXGR";
    
    // https://www.bkashcluster.com:9081/dreamwave/merchant/trxcheck/sendmsg?user=PRIDELIMITED&pass=pR!d31imIt3d&msisdn=01580254808&trxid=CZWEHXGR
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url        = $this->apiUrl;
        $opts = [
            "http" => [
                "header" => "Content-type: application/json" 
            ]
        ];

        $context = stream_context_create($opts);
        $content = @file_get_contents($url, false, $context);

        // echo '<pre>';
        // print_r($content);
        // die();
        /*
        $content = '{

                "transaction" : {

                    "trx_id" : "5652845",
                    "amount" : "1999.5",
                    "counter" : "1",
                    "currency" : "BDT",
                    "datetime" : "2012-06-18T13:35:35+06:00", "receiver" : "01818181818",
                    "reference" : "abc", "sender" : "01717171717", "service" : "Payment", "trx_status" : "0000"
                } 
            }';

            */
        
        $jsondata  = json_decode($content,true);
        
        if ($jsondata) {

            echo '<pre>';
            $statusCode =  $jsondata['transaction']['trxStatus'];
            $transactionState =  $this->transactionState($statusCode);

            print_r($jsondata);
            die();
            

        } else {
            echo 'Sorry! We cannot process this transaction right now. Try again later.';
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function transactionState($statusCode) {
            
        switch ($statusCode) {
            
            case '0000':
                $transaction = 1; # success
                break;
            
            case '0010':
                $transaction = 2; # pending
                break;
            
            case '0011':
                $transaction = 2; # pending
                break;

            case '0100':
                $transaction = 3; # Reversed
                break;

            default:
                $transaction = 4; # failure
                break;
        }

        return $transaction;
    }
}
