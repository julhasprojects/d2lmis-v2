<?php

namespace App\Http\Controllers\PaymentApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RocketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function errorState($errorCode) {

        switch ($errorCode) {
            
            case '01':
                $message = "Invalid Basic Authentication"; 
                break;
            
            case '02':
                $message = "Invalid Host Authentication"; 
                break;
            
            case '03':
                $message = "Invalid Authentication"; 
                break;

            case '04':
                $message = "Invalid Operation Code"; 
                break;

            case '05':
                $message = "Biller Short Code Missing"; 
                break;

            case '06':
                $message = "User Id Missing"; 
                break;

            case '07':
                $message = "Password Missing"; 
                break;

            case '08':
                $message = "Operation Code Missing"; 
                break;

            case '09':
                $message = "Bill Ref No Missing"; 
                break;
            
            case '10':
                $message = "Bill Amount Missing"; 
                break;
            
            case '11':
                $message = "Invalid Bill Amount"; 
                break;
            
            case '13':
                $message = "Txn Id Missing"; 
                break;

            default:
                $message = "Something went wrong!"; 
                break;
        }

        return $message;
    }

    private function transactionState($statusCode) {
            
        switch ($statusCode) {
            
            case '0000':
                $transaction = 1; # success
                break;
            
            case '0010':
                $transaction = 2; # pending
                break;
            
            case '0011':
                $transaction = 2; # pending
                break;

            case '0100':
                $transaction = 3; # Reversed
                break;

            default:
                $transaction = 4; # failure
                break;
        }

        return $transaction;
    }

    public function response($type=0,  $message, $statusCode) {
        
        /**
         *  $type = 0 // Error
         *  $type = 1 // Success
         */
        
        $returnApi = Array();

        if ($type) {

            $returnApi["message"] = $message;

        } else {

            $returnApi["Error"] = $message;
        }

        return Response::json($returnApi, $statusCode);

    }

}
