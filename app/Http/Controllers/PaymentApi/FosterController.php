<?php

namespace App\Http\Controllers\PaymentApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;
use Response;
use App\Model\Backend\Date;
use App\Model\Frontpanel\Newlines; 
use App\Model\Frontpanel\Billgenerate; 
use App\Model\Backend\BillGenerateInfo;
use App\Model\Backend\BillTransaction;
use App\Model\Backend\Billcollection; 
use App\Model\Backend\DefaulterList; 
use App\Model\Frontpanel\Instruments;

use DB; 
use Session;

use App\Model\Backend\Mikrotik; 
use App\Model\PaymentApi\FosterUser; 
use App\Model\PaymentApi\OnlineBillReceived; 
use App\Http\Controllers\Mikrotik\MikrotikController;
use App\Http\Controllers\Frontpanel\Bill\BillController; 


class FosterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function currentMonthBill($tokenID) {

        $data    = [];
        
        if (! $this->userAuthenticaton($tokenID) ) {

            $returnApi["resultCode"]    = 400;
            $returnApi["message"]       = "Wrong credential!";
            $returnApi["Error"]         = 'Your token was incorrect';

            return Response::json($returnApi, 400);

        } else {
            
            $userId  = $this->getUserIdByToken($tokenID);
            $userID  = $userId->id; 

            /**
             *  Get Current Month Bill
             */
            
            $monthYear = explode(',', Date::monthYear() );

            $BillGenerateInfo = BillGenerateInfo::select('created_at')->where('month', $monthYear[0])
                                                ->where('year', $monthYear[1])
                                                ->orderBy('id', 'desc')
                                                ->first();
           
            $bills = DB::table('client_info')
                        ->leftJoin('bill_generate', 'client_info.newconid', '=', 'bill_generate.newconid')
                        ->select('client_info.newconid as clientId', 'client_info.firstname as clientName','client_info.mobilenumber as mobileNo', 'client_info.emailaddress as email',
                            'bill_generate.billno', 'bill_generate.bilmnth as billMonth', 'bill_generate.billyear', 'bill_generate.discount as discountAmount', 'bill_generate.preamount as preDueAmount','bill_generate.currma as monthlyBill','bill_generate.totamount as totalAmount')
                        ->where('client_info.linestatus', 1)
                        ->where('bill_generate.bilmnth', $monthYear[0])
                        ->where('bill_generate.billyear', $monthYear[1])
                        ->where('bill_generate.status', 0)
                        ->where('bill_generate.preamount', 0) 
                        ->orderBy('bill_generate.billno', 'desc')
                        // ->take(20)
                        ->get();

            $messages  = "Current Month Bills";
            $returnApi = [];
            $returnApi["resultCode"]  = 200;
            $returnApi["message"]      = $messages;
            $returnApi["billGenDate"]  = $BillGenerateInfo['created_at'];
            $returnApi["genMonth"]     = $monthYear[0];
            $returnApi["genYear"]      = $monthYear[1];
            $returnApi["totalBills"]   = count($bills);
            $returnApi["billDetails"]  = $bills;
            
            return Response::json($returnApi, 200);

        }

    }

    public function currentMonthBillCollection(Request $request) {
        
        $returnApi = [];

        $tokenID = $request->get('access_token');

        if (is_null($tokenID)) {

            $message = "Wrong access token.";
            return $this->response(0, $message, $returnApi, 400);
            
        }

        if (! $this->userAuthenticaton($tokenID) ) {

            $returnApi["resultCode"]    = 400;
            $returnApi["message"]       = "Wrong credential!";
            $returnApi["Error"]         = 'Your token is incorrect';
            
            return Response::json($returnApi, 400);

        } else {
            
            if( !empty($this->getBillNo($request->get('clientId')) ) ) {

                $info = $this->getBillNo($request->get('clientId'));
                $billno = $info->billno;

            } else {

                $message = "Whoops! something went wrong. Check your data quality.";
                return $this->response(0, $message, $returnApi, 400);
            }

            $data = [];
            $data['newconid']   = $request->get('clientId');
            // $data['billno']     = $request->get('billNo');
            $data['billno']     = $billno;
            $data['billmonth']  = $request->get('billMonth');
            $data['billyear']   = $request->get('billYear');
            $data['transaction_id'] = $request->get('transactionId');

            $response = $this->userValidityCheck($data);

            $result = json_decode($response->getContent());


            if($result->resultCode == 400) {

                return $response->getContent();

            } 

            // echo "<pre>";
            // print_r($result->resultCode);
            // die('gagra g argar g');

            $advance = !empty($request->get('advancedPaid')) ? $request->get('advancedPaid') : 0;

            $bill = new BillTransaction();
            $bill->newconid     = $request->get('clientId');
            $bill->billno       = $billno;
            // $bill->billno       = $request->get('billNo');
            $bill->billmonth    = $request->get('billMonth');
            $bill->billyear     = $request->get('billYear');
            $bill->transaction_id  = $request->get('transactionId');
            $bill->sender       =  "Foster";
            $bill->currency     =  "BDT";
            $bill->paid_amount  = $request->get('paidAmount');
            $bill->advance_paid = $advance;
            $bill->service_charge   = !empty($request->get('serviceCharge') ) ? $request->get('serviceCharge') : 0;
            $bill->store_amount     = !empty($request->get('storeAmount') ) ? $request->get('storeAmount') : 0; 
            
            $bill->collection_date  = $request->get('collectionDate');
            $bill->payment_method   = $request->get('paymentMethod');
            $bill->system_ip        = $request->get('systemIp');
            
            if ($bill->save()) {
                
                $response = $this->billCollectionByFoster($request, $billno);

                $result = json_decode($response->getContent());

                if($result->resultCode == 200) {

                    return $response->getContent();

                } else if($result->resultCode == 400) {

                    return $response->getContent();
                }
                
            } else {

                $message = "Whoops! something went wrong. Check your data quality.";
                return $this->response(0, $message, $returnApi, 400);
            }

        }
        
    }

    private function userValidityCheck($data) {
        
        $returnApi = [];

        $monthYear = explode(',', Date::monthYear() );
        $month     = Date::monthToNumber($monthYear[0]);

        $exist = Billgenerate::where('newconid', $data['newconid'])
                    ->where('billno', $data['billno'])
                    ->where('linestatus', 1)
                    ->count();

        if (!$exist) {
            
            $message = "This client has already disconnected or invalid data.";
            return $this->response(0,$message, $returnApi, 400);
        }

        if ( !($data['billmonth'] == $month && $data['billyear'] == $monthYear[1]) ) {
                
            $message = "Whoops! You can't paid another months bill.";
            return $this->response(0,$message, $returnApi, 400);
        }

        $transaction = BillTransaction::where('transaction_id', $data['transaction_id'])->count();

        if ($transaction > 0) {
            
            $message = "This transaction ID already exist. You can't use same transaction ID more than once.";
            return $this->response(0, $message, $returnApi, 400);
        }

        $message = "Everything alirght for bill payment.";
        return $this->response(0,$message, $returnApi, 200);
    }

    private function response($type=0,  $message, $returnApi, $statusCode) {
        
        /**
         *  $type = 0 // Error
         *  $type = 1 // Success
         */
        
        if ($type) {

            $returnApi["resultCode"]  = $statusCode;
            $returnApi["message"]     = $message;

        } else {

            $returnApi["resultCode"]  = $statusCode;
            $returnApi["message"]     = $message;
            $returnApi["error"]       = $message;
        }

        return Response::json($returnApi, $statusCode);
    }


    public function billRecevied(Request $request) {
        
        $returnApi = [];

        $tokenID = $request->get('access_token');

        if (is_null($tokenID)) {

            $message = "Wrong access token.";
            return $this->response(0, $message, $returnApi, 400);
            
        }

        if (! $this->userAuthenticaton($tokenID) ) {

            $returnApi["resultCode"]    = 400;
            $returnApi["message"]       = "Wrong credential!";
            $returnApi["Error"]         = 'Your token is incorrect';
            
            return Response::json($returnApi, 400);

        } else {

            $monthYear    = explode(',', Date::monthYear() );

            $stored = $notStored = $returnApi = [];

            $month = Date::monthToNumber($monthYear[0]);
            $year  = $monthYear[1];

            $existingBill = OnlineBillReceived::existingBill($month, $year);

            $postedBill  = $request->get('billDetails');

            if (!empty($postedBill)) {
                
                foreach ($postedBill as $key => $bill) {
                    
                    // echo "<pre>";
                    // var_dump($bill);
                    // print_r($bill['clientId']);
                    // die();

                    if (!in_array( $bill['clientId'], $existingBill )) {
                        
                        $store = new OnlineBillReceived();
                        $store->newconid = $bill['clientId'];
                        $store->billno   = $bill['billno'];
                        $store->month    = $bill['billMonth'];
                        $store->year     = $bill['billyear'];
                        $store->discount = $bill['discountAmount'];
                        $store->preamount = $bill['preDueAmount'];
                        $store->monthly   = $bill['monthlyBill'];
                        $store->total     = $bill['totalAmount'];
                        $store->save();

                        $stored[] = $bill['clientId']; 

                    } else  {

                        $notStored[] = $bill['clientId']; # Existing bill
                    }
                }

                $message = "Successfully billing information stored";

                $returnApi["resultCode"]  = 200;
                $returnApi["stored"]      = $stored;
                $returnApi["notStored"]   = $notStored;

                return $this->response(0, $message, $returnApi, 200);

            } else {

                $message = "Please Check your data quality.";

                $returnApi["resultCode"]  = 400;

                return $this->response(0, $message, $returnApi, 400);

            }

        }

    }


    /**
     * [userAuthenticaton using token]
     * @param  [int] $token [Token ID]
     * @return [boolean]        [description]
     */
    private function userAuthenticaton($token) {

        $result     = FosterUser::select('access_token')
                        ->where('access_token', $token)
                        ->whereNull('deleted_at')
                        ->first();
        return $result;
    }

    /**
     * [get User Id By Token]
     * @param  [int] $token [Token ID]
     * @return [boolean]        [description]
     */
    private function getUserIdByToken($token) {

        if(isset($token)) {

            $result  = FosterUser::select('id')
                            ->where('access_token', $token)
                            ->whereNotNull('access_token')
                            ->first();

            return $result;

        } else {
            return false;
        }

    }

    public function billCollectionByFoster($collection, $billno) {

        $returnApi = [];

        $entry_by = Session::get('user_id');

        $entry_by = !empty($entry_by) ? $entry_by : 10001;

        // Check bill is already paid or not
        $billPaid = Billcollection::where( 'billno', $billno )->count();
        // $billPaid = Billcollection::where( 'billno', $collection->input('billNo') )->count();

        if ($billPaid) {

            $message = "This bill already paid.";
            
            return $this->response(0, $message, $returnApi, 400);

        }       

        # Get Data from Bill Generate table and 
        $clinetBillInfo = Billgenerate::where( 'billno', $billno )->first();
        // $clinetBillInfo = Billgenerate::where( 'billno', $collection->input('billNo') )->first();

        # Insert data into Bill Collection Table
        $billing_info   = new Billcollection();
        $billing_info->billno       = $billno;
        // $billing_info->billno       = $collection->input( 'billNo' );
        $billing_info->newconid     = $clinetBillInfo->newconid;      
        $billing_info->branch_id    = $clinetBillInfo->branch_id;      
        $billing_info->emp_id       = $entry_by; // Bill Collected By      
        $billing_info->entry_by     = $entry_by; // Bill Entry By      
        $billing_info->package_id   = $clinetBillInfo->package_id;       
        $billing_info->bilmnth      = $clinetBillInfo->bilmnth;       
        $billing_info->billyear     = $clinetBillInfo->billyear;       

        $billing_info->lapay       = $clinetBillInfo->lapay;
        $billing_info->issudate    = $clinetBillInfo->issudate;
        $billing_info->preamount   = $clinetBillInfo->preamount;
        $billing_info->discount    = $clinetBillInfo->discount;
        $billing_info->billingtype = $clinetBillInfo->billingtype;
        $billing_info->currma      = $clinetBillInfo->currma;
        $billing_info->totamount   = $clinetBillInfo->totamount;
        $billing_info->bilgenby    = $clinetBillInfo->bilgenby;
        $billing_info->pamount     = $collection->input( 'paidAmount' );
        $billing_info->dueamount   = $clinetBillInfo->totamount - $collection->input( 'paidAmount' );
        $billing_info->advance     = 0;
        $billing_info->lastdate    = $clinetBillInfo->lastdate;
        $billing_info->status      = 1; 
        $billing_info->confirm     = 3; // Confirm Bill Collection
        $billing_info->remarks     = $clinetBillInfo->remarks;
        $billing_info->collection_source   = 1; # Online collection
        $billing_info->collection_date     = $collection->input( 'collectionDate' ); 
        
        if( $billing_info->save() ) {

            DB::table( 'bill_generate' )
                ->where( 'billno', $billno )
                // ->where( 'billno', $collection->input('billNo') )
                ->update([
                    'status'      => 3 // Bill collected by Collector
                ]);

            $status = true;
            
            $permission = Mikrotik::isactive();

            if ($permission->is_enable) {

                $mikrotikObj = new MikrotikController;

                $mikrotikStatus = $mikrotikObj->reactiveForDuePaid($clinetBillInfo->newconid);
                /*
                if ($mikrotikStatus == 400) {
                    $status = false;
                }
                */
                if($mikrotikStatus == 200) {

                    Instruments::where('newconid', $clinetBillInfo->newconid)
                    ->update([
                        'mikrotik_enable_status' => 1
                    ]);

                     // Update Bill genreate table 
                    $updateBillGenerateInfo = Billgenerate::where('newconid', $clinetBillInfo->newconid)
                        ->where('billno',$billno)
                        ->first();
                        
                    $updateBillGenerateInfo->is_defaulter  = 0;
                   
                    $updateBillGenerateInfo->save();

                    // duefualter List
                    $updatedBy = ( Session::get('user_id'))? Session::get('emp_id'):0;

                    $defaulterList =  DefaulterList::where('newconid',$clinetBillInfo->newconid)
                        ->orderBy('id','desc')->first();

                    if(!empty($defaulterList)) {

                        $defaulterList->newconid       = $clinetBillInfo->newconid;
                        $defaulterList->status         = 1;
                        $defaulterList->reback_date    = Date('Y-m-d');
                        $defaulterList->updated_by     = $updatedBy;
                        $defaulterList->save();

                    }
                    

                } else if ($mikrotikStatus == 400) {
                    $status = false;
                }

                
                
            }
            
            if ($status) {

                $returnApi['merchantTxnNo'] = $collection->input( 'transactionId' );

                $message = "Bill collected successfully.";
                return $this->response(1, $message, $returnApi, 200);

            } else {

                $message = "Whoops! Something went wrong.";
                return $this->response(1, $message, $returnApi, 400);
            }
            

        }

        $message = "Whoops! Something went wrong.";
        return $this->response(1, $message, $returnApi, 400);

    }

    private function getBillNo($clientId) {

        $monthYear = explode(',', Date::monthYear() );

        return Billgenerate::select('billno')->where('bilmnth', $monthYear[0])
                                                ->where('billyear', $monthYear[1])
                                                ->where('newconid', $clientId)
                                                ->orderBy('id', 'desc')
                                                ->first();

    }


}
