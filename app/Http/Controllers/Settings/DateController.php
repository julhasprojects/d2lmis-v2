<?php
namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Backend\Date; 
use App\Model\Backend\BillSetup; 
use App\Model\Backend\Branch; 
use Session;

class DateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        # Month,current month and next month
        $monthYear     = explode(',', Date::monthYear());


        $currentMonth  = date('m', strtotime($monthYear[0]));
        $NextMonth     = date('m', strtotime($monthYear[2]));

        # current month total day and next month total day
        $thisMonthDay  = cal_days_in_month(CAL_GREGORIAN, $currentMonth, $monthYear[1]);
        $NextsMonthDay = cal_days_in_month(CAL_GREGORIAN,  $NextMonth, $monthYear[1]);
        
        $daysOfMonth = [];

        for ($i=1; $i <= date("t"); $i++) { 
           $daysOfMonth[] = ($i < 10 ) ? '0' . $i : $i;
        }

        return view( 'backend.settings.date.date-setup', [
            'allDate'        => Date::all(),
            'billSystem'     => BillSetup::all(),
            'branchInfo'     => Branch::all(),
            'thisMonthDay'   => $thisMonthDay,
            'NextsMonthDay'  => $NextsMonthDay,
            'daysOfMonth'    => $daysOfMonth
        ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $dateInfo = Date::first();

        if( !empty($dateInfo) ) {

            Session::put('invalid', '2');
            Session::flash("message", "Sorry! Date already exist. Modify existing date.");
            return back();
        }

        if ( Date::insert($request->except('_token')) ) {

            Session::put('valid', '1');
            Session::flash('message', 'Congratulations! Your date setup has done successfully.');

        }  else {

            Session::put('invalid', '2');
            Session::flash('message', 'Sorry! We are unable to setup your bill date.');
            
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        // For Delete
        
        $date = Date::findOrFail($id);
        
        if ( $date->delete() ) {

            Session::put('valid', '1');
            Session::flash('message', 'Congratulations! Your billing date has deleted successfully.');

        }  else {

            Session::put('invalid', '2');
            Session::flash('message', 'Sorry! We are unable to update billing date.');
            
        }

        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        return view( 'backend.settings.date.edit-date',[
            'allDate'    => Date::all(),
            'branchInfo' => Branch::all(),
            'editInfo'   => Date::findorfail($id)
        ] );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        # find date ID
        $date = Date::findOrFail($id);

        $date->setup_by  = $request->Input('setup_by');
        $date->start_day = $request->Input('start_day');
        $date->end_day   = $request->Input('end_day');
        $date->branch_id = $request->Input('branch_id');

        # Update Date Setup
        if ( $date->save() ) {

            Session::put('valid', '1');
            Session::flash('message', 'Your bill date has updated successfully');

        }  else {

            Session::put('invalid', '2');
            Session::flash('message', 'Sorry! We are unable to update your billing date. Please try again!');
            
        }

        return redirect('bill/date/and/system/setup');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        echo 'delete';
    }
}
