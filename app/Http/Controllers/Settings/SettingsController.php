<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Backend\Settings;
use Session;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */


    public function saveHeader(Request $request  ) {

        $settings = new Settings();         
        $settings->title        = $request->input( 'title' );
        $settings->fbprofile    = $request->input( 'fbprofile' );
        $settings->twprofile    = $request->input( 'twprofile' );
        $settings->linprofile   = $request->input( 'linprofile' );
        $settings->gplprofile   = $request->input( 'gplprofile' );
        $settings->save();
        Session::flash( 'message', 'Your information has saved successfully' );
        return redirect( 'header-setup' );

    }

}
