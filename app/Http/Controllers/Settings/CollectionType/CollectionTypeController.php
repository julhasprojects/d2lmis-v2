<?php

namespace App\Http\Controllers\Settings\CollectionType;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Backend\CollectionType; 
use Session;
Use Input;

class CollectionTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        return view('backend.settings.collection-type.index',[
            'collectionType' => CollectionType::leftjoin('employees','collection_type_report.created_by','=','employees.employeeID')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $collectionTypeID = CollectionType::orderBy('id','desc')->first();

        if(!empty($collectionTypeID)) {

            $id = $collectionTypeID->collection_type_id + 1;

        } else {

            $id =  1;
        }

        $collectionType = new CollectionType();
        $collectionType->collection_type_id = $id;
        $collectionType->collection_type    = \Input::get('collectiontype');
        $collectionType->created_by         = Session::get( 'emp_id' );
        $collectionType->save();
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
