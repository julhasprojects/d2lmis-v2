<?php

namespace App\Http\Controllers\Settings\NewlineSetup;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Backend\Settings;
use App\Model\Backend\NewlineSetup;
use Session;

class CustomerTypeSetupController extends Controller
{
	public function index() {

		return view( 'backend.settings.newline.newline-customer-setup',[
			'newlineInfo' => NewlineSetup::all()
		] );
	}

	public function store(Request $request) {

		# validation section
        $this->validate($request, [
            'customer_type'     => 'required'
        ]);

        # customer type setup
		$NewlineSetup = New NewlineSetup();
		$NewlineSetup->customer_type = \Input::get('customer_type');

		if($NewlineSetup->save()) {
				

		}
        
	}	

	/**
	 * [edit description]
	 * @param  [get] $id [description]
	 * @return [get]     [description]
	 */
	public function edit($id) {
		# customer type edit
		return view( 'backend.settings.newline.newline-customer-setup-edit',[
			'newlineInfo' =>NewlineSetup::where('id',$id)->first()
		] );

	} 

	/**
	 * [update description]
	 * @param  Request $request [description]
	 * @param  [type]  $id      [description]
	 * @return [type]           [description]
	 */
	public function update(Request $request, $id) {

		# validation section
        $this->validate($request, [
            'customer_type'     => 'required',
        ]);

		$NewlineSetup = NewlineSetup::where( 'id', $id )->first();
		$NewlineSetup->customer_type = \Input::get('customer_type');

		if($NewlineSetup->save()) {

			Session::put( 'valid', 1 );
            Session::flash('message','Customer type has updated successfully.');

		}  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We unable to update your information. Please try again!');

        }

        return redirect('customer/type/setup');
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id) {
		NewlineSetup::where('id',$id)->delete();
		Session::put( 'valid', 1 );
       	Session::flash('message','Customer type has deleted successfully.');
		return back();
	}
}