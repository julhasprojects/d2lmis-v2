<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Backend\Settings;
use App\Model\Backend\HsSetting;
use Session;

class HsSettingController extends Controller
{
	public function index() {

		return view( 'backend.settings.hs.hs-setup',[
			'hsInfo' =>HsSetting::all()
		] );
	}

	public function store(Request $request) {

		#validation section
        $this->validate($request, [
            'chargeamount'     => 'required',
        ]);

		$HsSetting = New HsSetting();
		$HsSetting->charge_allow     =\Input::get('chargeallow');
		$HsSetting->interface_allow  =\Input::get('interfaceallow');
		$HsSetting->hs_charge_amount =\Input::get('chargeamount');

		if($HsSetting->save()) {

			Session::put( 'valid', 1 );
            Session::flash('message','HS charge has created successfully');

		}  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to create HS charge. Please try again!.');

        }
         return back();   
	}	

	public function edit($id) {

		return view( 'backend.settings.hs.hs-setup-edit',[
			'shInfo' =>HsSetting::where('id',$id)->first()
		] );

	} 

	public function update(Request $request,$id) {

		#validation section
        $this->validate($request, [
            'chargeamount'     => 'required',
        ]);

		$HsSetting = HsSetting::where( 'id', $id )->first();

		$HsSetting->charge_allow     = \Input::get('chargeallow');
		$HsSetting->interface_allow  = \Input::get('interfaceallow');
		$HsSetting->hs_charge_amount = \Input::get('chargeamount');

		if($HsSetting->save()) {

			Session::put( 'valid', 1 );
            Session::flash('message','HS has updated successfully');

		}  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to update HS charge. Try again!.');
        
        }
        return back();
	}

	public function destroy($id) {

		HsSetting::where('id',$id)->delete();
		Session::put( 'valid', 1 );
        Session::flash('message','HS cost setup has deleted successfully');
		return back();

	}
}