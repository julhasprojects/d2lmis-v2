<?php
namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Frontpanel\ComplainSetting; 
use App\Model\Backend\Date; 
use App\Model\Backend\CompanyTableSetup; 
use Session;
use Input;

class ComplainSettings extends Controller
{
    /**
	 * View created Compain Type
	 *
	 * @return Response
	 */
	public function index() {

		$typeInfo = ComplainSetting::orderBy('id','desc')->get();
		
		return  view('frontend.complain.complain-setting', [ 
			'typeInfo'	=> $typeInfo,
			'companyTableSetup'	=> CompanyTableSetup::all(),
		]);
		 
	}

	/**
	 * View created Compain Type
	 *
	 * @return Response
	 */
	public function store(Request $request) {

		$complainType = \Input::get('complaintype');

		# Store complain type
		if(!empty($complainType)) {

			$complain = new ComplainSetting();
			$complain->complain_type = $complainType;
			$complain->type_status   = \Input::get('status');
			$complain->save();

			Session::put('valid', '1');
			Session::flash( 'message', 'Your complain type has created successfully' );

		} else {

			Session::put('invalid', '1');
			Session::flash( 'message', 'Sorry! we are unable to save your complain type information.' );
		
		}

		return redirect( 'complain/setting/' );

		//$complain->save();
	}

	/**
	 * Destroy Complain Type
	 *
	 * @return Response
	 */
	
	public function edit($type_id) {

   		$typeInfo = ComplainSetting::select('id','complain_type','type_status')
   			->where('id',$type_id)
   			->first();
		
		$content = view('frontend.complain.complain-setting-edit',[
			'typeInfo'=>$typeInfo 
		]);
		return view( 'backend.index' )->with('content_area',$content);
	}

	/**
	 * [update description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function update($id) {

		$complainType=\Input::get('complaintype');

		if(!empty($complainType)) {

			$complain = ComplainSetting::find( $id ); 
			$complain->complain_type = \Input::get('complaintype');
			$complain->type_status   = \Input::get('status');
			$complain->save();

			Session::put('valid', '1');
			Session::flash( 'message', 'Your complain type has updated successfully.' );

		} else {

			Session::put('invalid', '1');
			Session::flash( 'message', 'Sorry! we are unable to save your complain type information.' );
		}

		return redirect( 'complain/setting/' );
   }
	/**
	 * Destroy Complain Type
	 *
	 * @return Response
	 */
	
	public function destroy($id) {

   		ComplainSetting::where('id',$id)->delete();
		/*Session::put( 'valid', 1 );
		Session::flash( 'message', 'Your complain type has deleted successfully.' );
		return redirect( '/complain/setting/' );*/
		
	}

	# complain setting delete
	public function complainDelete($id) {

		ComplainSetting::where('id',$id)->delete();
		
	}

	# Complain Setting Search
	public function comlainSettingSearch() {

		$searchTye = \Input::get('searchcomplainstatus');
		$typeInfo = ComplainSetting::where('type_status',$searchTye)
			->orderBy('id','desc')
			->get();
		
		return  view('frontend.complain.complain-setting-search', [ 
			'typeInfo'	=> $typeInfo 
		]);
	}


	public function complainTableSetting() {

		$input = Input::get('cmpSetting');

		$cmpSetting = implode('', $input);

	
		$complanTypeSetup = new CompanyTableSetup();
		$complanTypeSetup->column_name = $cmpSetting;

		if($complanTypeSetup->save()) {

			Session::put('valid', '1');
			Session::flash( 'message', 'Your complain table setup has updated successfully.' );

		} else {

			Session::put('invalid', '1');
			Session::flash( 'message', 'Sorry! we are unable to save your complain type information.' );
		
		}
		return back();
		
	}

	// Complain Table Settings
	public function complainTableSettingDelete() {
		$id = Input::get('id');
		CompanyTableSetup::where('id',$id)->delete();

	}
}