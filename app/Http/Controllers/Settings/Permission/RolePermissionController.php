<?php
namespace App\Http\Controllers\Settings\Permission;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Backend\Date; 
use App\Model\Backend\Role; 
use App\Model\Backend\Permission; 
use App\Model\Backend\PermissionCategory; 
use Illuminate\Support\Facades\Redirect;
use Session;
use Input;
class RolePermissionController extends Controller
{
   public function index() {
        

        $permissionInfo = Permission::leftjoin('permission_category','permissions.category','=','permission_category.id')
            ->select('permission_category.category as category_name','permissions.*')
            ->orderBy('permissions.id','desc')
            ->get();

        return view('backend.settings.permission.permission',[
            'permissionInfo'     => $permissionInfo,
            'permissionCategory' => PermissionCategory::all(),
        ]);
 
   }

    public function show() {

        return view('backend.settings.permission.permission',[
            'permissionInfo' => Permission::orderBy('id','desc')->get(),
        ]);
   }

   public function store(Request $request) {

        $this->validate($request, [
          'permissionname'     => 'required',
          'permissionslug'     => 'required',
          'permissioncategory' => 'required',
        ]);

        $permissionDescription = Input::get('description');

       
        $userRole              = new Permission;
        $userRole->name        = \Input::get('permissionname');
        $userRole->slug        = \Input::get('permissionslug');
        $userRole->description = \Input::get('description');
        $userRole->category    = \Input::get('permissioncategory');

        if($userRole->save()) {

        Session::flash("message", "New permission information has created successfully!");
        return Redirect::back();

        } else {

        Session::flash("message", "Sorry! We can't create new permission information!");
        return Redirect::back();
        }
   }

   public function edit($id) {
        
        return view( 'backend.settings.permission.permission-edit',[
            'permission'         => Permission::where('id',$id)->first(),
            'permissionCategory' => PermissionCategory::all(),
        ] );
  } 

  public function update(Request $request,$id) {

    $this->validate($request, [
      'permissionname'     => 'required',
      'permissionslug'     => 'required',
      'permissioncategory' => 'required',
    ]);
    
    $permissionupdated = Permission::where( 'id', $id )->first();
    $permissionupdated->name        = \Input::get('permissionname');
    $permissionupdated->slug        = \Input::get('permissionslug');
    $permissionupdated->description = \Input::get('description');
    $permissionupdated->category    = \Input::get('permissioncategory');

    if($permissionupdated->save()) {

        Session::put( 'valid', 1 );
        Session::flash('message','Permission setup has updated successfully');

    }  else {

        Session::put( 'invalid', 2 );
        Session::flash('message','We are unable to updated. Please try again!.');

    }

    return Redirect::to('new/role/permission');
    
  }

  public function destroy($id) {

    Permission::where('id',$id)->delete();
    Session::put( 'valid', 1 );
        Session::flash('message','Permission has deleted successfully');
    return back();
  }
}