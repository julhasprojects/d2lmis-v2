<?php

namespace App\Http\Controllers\Settings\Permission;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Backend\PermissionCategory; 
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
use Input; 

class PermissionCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
       return view('backend.settings.permission.permission-category',[
            'permissionCategory' => PermissionCategory::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $permissionInfo = PermissionCategory::where('category',Input::get('permissioncategory'))->first();
        
        if(isset($permissionInfo)) {

            Session::put( 'invalid', 2 );
            Session::flash("message", "Sorry! We are unable to create category information!");
            return back();
        }

        #validation section
        $this->validate($request, [
            'permissioncategory' => 'required',
        ]);

        $permissionsCategory = new PermissionCategory;
        $permissionsCategory->category = \Input::get('permissioncategory');
        
        if($permissionsCategory->save()) {

            Session::put( 'valid', 1 );
            Session::flash("message", "New permission category has created successfully!");


        } else {
             Session::put( 'invalid', 2 );
             Session::flash("error", "Sorry! We are unable to create category information!");
        
        }

        return redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        return view('backend.settings.permission.permission-category-edit',[
            'permissionCategory' => PermissionCategory::find($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id) {
        
         #validation section
        $this->validate($request, [
            'permissioncategory' => 'required',
        ]);

        $permissionCategory =  PermissionCategory::find($id);
        $permissionCategory ->category = \Input::get('permissioncategory');
        
        if($permissionCategory->save()) {

            Session::flash("message", "Permission category has updated successfully!");

        } else {

             Session::flash("message", "Sorry! We are unable to update permission category information!");
        
        }

        return redirect('/permission/menu/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        PermissionCategory::where('id',$id)->delete();
        Session::put( 'valid', 1 );
        Session::flash('message','Permission Category has deleted successfully');
        return back();

    }
}
