<?php
namespace App\Http\Controllers\Settings\Permission;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Backend\Date; 
use App\Model\Backend\Role; 
use App\Model\Backend\Permission; 
use App\Model\Backend\PermissionRole; 
use Illuminate\Support\Facades\Redirect;
use Session;

class PermissionManagementController extends Controller {

   public function index() {

   		return view('backend.settings.permission.permission-management');
 
   }

   public function show() {

   		return view('backend.settings.permission.role-name',[
   			'role' => Role::orderBy('id','desc')->get(),
   		]);

   }

   public function store(Request $request) {

        $this->validate($request, [
            'rolename' => 'required',
            'roleslug' => 'required',
        ]);
        $userRole              = new Role;
    	$userRole->name        = \Input::get('rolename');
    	$userRole->slug        = \Input::get('roleslug');
    	$userRole->description = \Input::get('description');

 		if($userRole->save()) {

 			Session::flash("message", "New role information has created successfully!");
            return Redirect::back();

 		} else {

 			Session::flash("message", "Sorry! We can't create new role information!");
            return Redirect::back();

 		}
   }

   public function edit($id) {

        $roleInfo  = Role::find($id);

        $permissionCategory = Permission::leftjoin('permission_category','permissions.category','=','permission_category.id')
            ->select('permissions.category','permission_category.category as categoryName')
            ->groupBy('category')
            ->get();

        foreach($permissionCategory as $category) {

            $permissionList = Permission::leftjoin('permission_role','permissions.id','=','permission_role.permission_id')
                ->leftjoin('permission_category','permissions.category','=','permission_category.id')
                ->select('permissions.id','permission_role.role_id','permission_role.permission_id','permission_category.category as category_name','permissions.name','permissions.category','permission_role.role_id')
                ->where('permissions.category',$category->category)
                ->where('permission_role.role_id',$id)
                ->orderBy('permissions.category','desc')
                ->get();

            $permissionValue = Permission::leftjoin('permission_category','permissions.category','=','permission_category.id')
                ->select('permissions.id','permission_category.category as category_name','permissions.name','permissions.category')
                ->where('permissions.category',$category->category)
                ->orderBy('permissions.category','desc')
                ->get();

                $categoryUpdated[] = $permissionList;
                $collection        = $permissionValue;
                $diff              = $collection->diff($permissionList );
                $categoryWishPermissionrole[] = $diff->all();

                $unique = $permissionList->unique();
                $categoryWishPermission[] = $unique->values()->all();
                $allCategory[]            = $category->categoryName;
        }

       
        $langth = count( $categoryUpdated);
       
	 	return view( 'backend.settings.permission.role-permission-create',[
            'roleInfo'                    => $roleInfo,
            'allCategory'                 => $allCategory,
            'categoryWishPermission'      => $categoryWishPermission,
            'categoryWishPermissionrole'  => $categoryWishPermissionrole,
            'langth'                      => $langth,
        ]);
        
	} 

	public function update($id) {
      
        $permissionid = \Input::get('permissionid');

        PermissionRole::where('role_id',$id)->delete();
        
        if(!empty($permissionid )) {
            foreach($permissionid as $roleId) {

                $userRole                 = new PermissionRole;
                $userRole->role_id        = $id;
                $userRole->permission_id  = $roleId;

                if($userRole->save()) {

                    Session::flash("message", "Permission information created successfully!");
                } else {

                    Session::flash("message", "Sorry! We are unable to create permission information!");
                }
            }
        } else {
            Session::flash("message", "Permission information created successfully!");
        }
 
        return Redirect::back();  
	}

	public function destroy($id) {

		Role::where('id',$id)->delete();
		Session::put( 'valid', 1 );
       	Session::flash('message','New role has deleted successfully');
		return back();
        
	}
}