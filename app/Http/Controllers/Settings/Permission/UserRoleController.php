<?php
namespace App\Http\Controllers\Settings\Permission;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Backend\Date; 
use App\Model\Backend\Role; 
use App\Model\Backend\RoleUser; 
use Illuminate\Support\Facades\Redirect;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use Session;
use DB;

class UserRoleController extends Controller
{
   public function index() {

        $authUser = DB::table('register_info')
            ->leftjoin('employees','register_info.emp_id','=','employees.employeeID')
            ->leftjoin('designation','employees.designation','=','designation.id')
            ->leftjoin('department','designation.deptID','=','department.id')
            ->select('register_info.emp_id','register_info.username','department.deptName','designation.designation','register_info.email','register_info.name','register_info.id')
            ->orderBy('register_info.emp_id')

            ->whereNotNull('register_info.emp_id')
            ->whereNotIn('register_info.emp_id',[999])
            ->whereNull('register_info.deleted_at')
            ->get();

        return view('backend.settings.permission.user-role',[
            'userInfo' => $authUser,
        ]);

   }

   public function show() {

   		$authUser = DB::table('register_info')->get();

        return view('backend.settings.permission.user-role',[
            'userInfo' => $authUser,
        ]);

   }

   public function store() {

        $userid   = \Input::get('usrid');
        $userrole = \Input::get('usrrole');

        RoleUser::where('user_id',$userid)->delete();

        if(!empty($userrole)) {
            foreach($userrole as $roleId) {

                if(!empty($roleId)) {

                    $UserRole              = new RoleUser;
                	$UserRole->user_id     = $userid;
                	$UserRole->role_id     = $roleId;

                    if($UserRole->save()) {

                        Session::flash("message", "Role permission has created successfully!");

                    } else {

                        Session::flash("message", "Sorry! We are unable to create new role!");
                    }
                 
                }
            }
         }
 
        return Redirect::back();        
   }

   public function edit($id) {
        
        $UsrInfo = DB::table('register_info')->where('id',$id)
            ->first();

        $selectRoll  = Role::leftjoin('role_user','roles.id','=','role_user.role_id')
            ->select('roles.id','roles.name','role_user.role_id','role_user.user_id')
            ->where('role_user.user_id',$id)
            ->groupBy('roles.id')
            ->get();

        $roleUsed =[];
        
        foreach ($selectRoll as  $value) {
            $roleUsed[] = $value->role_id;   
        }

       
        $unSelectAll = Role::groupBy('roles.id')
            ->get();

       

		return view( 'backend.settings.permission.user-role-edit',[
            'roleInfo'      => $selectRoll,
            'unSelectAll'   => $unSelectAll,
            'userInfo'      => $UsrInfo,
            'roleUsed'      => $roleUsed,
            'roleuserInfo'  => RoleUser::all(),
		] );
	} 

	public function update($id) {

		$UserRole = Role::where( 'id', $id )->first();
		
    	$UserRole->name        = \Input::get('rolename');
    	$UserRole->slug        = \Input::get('roleslug');
    	$UserRole->description = \Input::get('description');

		if($UserRole->save()) {

			Session::put( 'valid', 1 );
            Session::flash('message','New role setup has update successfully');

		}  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to update your information. Please try again!.');

        }
        return back();
	}

	public function destroy($id) {
		Role::where('id',$id)->delete();
		Session::put( 'valid', 1 );
       	Session::flash('message','Role has deleted successfully!');
		return back();
	}
}