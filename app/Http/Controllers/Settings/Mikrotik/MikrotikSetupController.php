<?php

namespace App\Http\Controllers\Settings\Mikrotik;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Backend\Settings;
use App\Model\Backend\Mikrotik;
use App\Model\Backend\Branch;
use App\Model\Backend\Mikrotiksetup;
use App\Model\Frontpanel\Region;
use App\Model\Backend\Package;
use Session;
use Input;

class MikrotikSetupController extends Controller
{
	public function index() {

		$Mikrotiksetup = Mikrotiksetup::leftjoin('region', 'mikrotik_list.region_id', '=', 'region.region_id')
            ->leftjoin('employees', 'mikrotik_list.created_by', '=', 'employees.employeeID')
            ->select('mikrotik_list.id','employees.fullName','mikrotik_list.password','region.region_name','mikrotik_list.mikrotik_name','mikrotik_list.ip','mikrotik_list.username')
            ->orderBy('mikrotik_list.id','desc')
            ->get();
    
		return view( 'backend.settings.mikrotik.mikrotik-setup',[
			'mikrotikInfo'  => Mikrotik::orderBy('id','desc')->get(),
			'regionInfo'    => Region::all(),
			'branchInfo'    => Branch::all(),
			'MikrotikList'  => $Mikrotiksetup,
		] );

	}

	/**
	 * [store description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function store(Request $request) {

		$region                    = \Input::get('region');
		$nkname                    = \Input::get('mkname');
		$ip                        = \Input::get('ip');
		$username                  = \Input::get('username');
		$password                  = \Input::get('password');
		$branch                    = \Input::get('branch');

		#validation section
      
		if(empty($region) && empty($nkname)){

	  		$Mikrotik = New Mikrotik();
			$Mikrotik->is_enable       = \Input::get('makrotik');
			$Mikrotik->mikrotik_route  = \Input::get('mikrotikroute');

			# Mikrotk Save
			if($Mikrotik->save()) {

				Session::put( 'valid', 1 );
	            Session::flash('message','Mikrotik settings has completed successfully');

			}  else {

	            Session::put( 'invalid', 2 );
	            Session::flash('message','We are unable to setup mikrotik information. Please try again!.');

	        }

	        return back(); 

		} else {

			if(count($region) > 0) {

				for($key = 0; $key < count($region); $key++ ) {

					if(!empty($nkname[$key])) {
						if(!empty($region[$key]) || $nkname[$key] || $ip[$key]) {

							$entry_by      = Session::get('emp_id');
							$Mikrotiksetup = New Mikrotiksetup();
							$Mikrotiksetup->region_id      = $region[$key];
							$Mikrotiksetup->mikrotik_name  = $nkname[$key];
							$Mikrotiksetup->branchId       = $branch[$key];
							$Mikrotiksetup->ip             = $ip[$key];
							$Mikrotiksetup->username       = $username[$key];
							$Mikrotiksetup->password       = $password[$key];
							$Mikrotiksetup->created_by     = $entry_by;

							# Mikrotik list save
							$Mikrotiksetup->save();
						} else {

							return back();
							
						}
						
					}
				}
			}

			return redirect('mikrotik/integration')->with('status', '2'); 

		}
		 

	}	

	public function edit($id) {

		return view( 'backend.settings.mikrotik.mikrotik-setup-edit',[
			'mikrotikInfo'  => Mikrotik::find($id),
		] );
	} 

	public function mikrotikListEdit($id) {

		return view( 'backend.settings.mikrotik.mikrotik-list-edit',[
			'regionInfo'    => Region::all(),
			'mikrotikList'  => Mikrotiksetup::find($id),
		] );
	} 

	public function mikrotikListUpdate($id) {

		$Mikrotiksetup = Mikrotiksetup::find($id);

		$Mikrotiksetup->region_id      = \Input::get('region');
		$Mikrotiksetup->mikrotik_name  = \Input::get('mkname');
		$Mikrotiksetup->ip             = \Input::get('ip');
		$Mikrotiksetup->username       = \Input::get('username');
		$Mikrotiksetup->password       = \Input::get('password');
		$Mikrotiksetup->created_by     = Session::get('emp_id');

		# Mikrotk Save
		if($Mikrotiksetup->save()) {

			Session::put( 'valid', 1 );
            Session::flash('message','Mikrotik settings has completed successfully');

		}  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to setup mikrotik information. Please try again!.');

        }
        return redirect('mikrotik/integration')->with('status', '2'); 

	}

	public function update($id) {

		$Mikrotik = Mikrotik::find($id);
		$Mikrotik->is_enable       = \Input::get('makrotik');
		$Mikrotik->mikrotik_route  = \Input::get('mikrotikroute');

		# Mikrotk Save
		if($Mikrotik->save()) {

			Session::put( 'valid', 1 );
            Session::flash('message','Mikrotik information has updated successfully');

		}  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to update microtik information. Please try again!.');
        
        }

        return redirect('mikrotik/integration');  
	}

	public function mikrotikIntegrationDelete() { 

		$status = \Input::get('status');
		$id     = \Input::get('id');
		
		if($status == 1) {

			Mikrotik::where('id',$id)->delete();
			return 1;

		} else {

			Mikrotiksetup::where('id',$id)->delete();
			return 2;
		}
		
	}
}