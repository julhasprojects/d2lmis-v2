<?php

namespace App\Http\Controllers\Settings;
use Illuminate\Http\Request;
use App\Model\Frontpanel\Linedisconnect; 
use App\Model\Frontpanel\Newlines; 
use App\Model\Frontpanel\NewlineRequest; 
use App\Model\Frontpanel\PackageChange; 
use App\Model\Frontpanel\HouseChange; 
use App\Model\Frontpanel\Employee; 
use App\Model\Backend\Package;
use App\Model\Backend\Assignregion; 
use App\Model\Backend\EmployeeRegions; 
use App\Model\Frontpanel\Region; 
use App\Model\Frontpanel\Road; 
use App\Model\Frontpanel\Empoffice; 
use App\Model\Backend\Date;
use App\Model\Backend\Branch;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use Input;


class EmployeeRegionController extends Controller
{
    
    public function newEmployeeRegionSetup(Request $request) {

        # District 
        $districtName = DB::table('district')->select('district.*')->get();

        # upzilla name
        $upazilaName  = DB::table('upazila')->select('upazila.*')->get();

        # Employee region reports
        $employeeRegion = EmployeeRegions::leftjoin('employees','employee_regions.emp_id','=','employees.employeeID')
            ->leftjoin('region','employee_regions.region_id','=','region.region_id')
            ->where('employees.status',1)
            ->select('employees.fullName','employee_regions.region_roads','region.region_name','employee_regions.created_at','employee_regions.updated_at')
            ->get();

        $roadInfo = Road::all();
       
        return   view('backend.settings.employee-region-assign', 
            [
                'employee_info'   => Empoffice::employeeCatgoryList('2'),
                'districtName'    => $districtName,
                'upazilaName'     => $upazilaName,
                'regionName'      => Region::all(),
                'branchInfo'      => Branch::all(),
                'assingEmployeer' => $employeeRegion,
                'roadList'        => $roadInfo,
            ]
        );
    }

    public function getEmployeeRegionRoads() {

        $region_id = \Input::get('region');
       
        
        $allRoads = DB::table('road')
            ->select('id','road')
            ->where('region_id', $region_id)
            ->get();

        return view( 'backend.settings.employeeRegionAssignResult',[
             'allRoad' => $allRoads, 
        ]);
       
    }

    public function assignEmployeeRegion(Request $request) {

        $roadid =  $request->input( 'roadid' );
        $regionId= $request->input( 'region_id' );

        $monthYear = explode(',', Date::monthYear());

        $roadnumberID = '';

        if( isset($roadid) && !empty($roadid)) {
            
            foreach ($roadid as $value) {
              $roadnumberID = $value . ','. $roadnumberID;   
              
                // Bill Generate Table update
                $clientInfo = DB::table('client_info')
                    ->select('newconid')
                    ->where('region', $regionId)
                    ->where('road', $value)
                    ->get();   

                for ($i=0; $i <count($clientInfo) ; $i++) {
                    $clientId = $clientInfo[$i]->newconid;

                    DB::table('bill_generate')
                        ->where('newconid', $clientId)
                        ->where('bilmnth', $monthYear[0])
                        ->update([
                                'regional_emp'   => $request->input( 'emp_id' )
                            ]); 
                }          
            }

            $assign = new Assignregion();
            $assign->emp_id         = $request->input( 'emp_id' );
            $assign->district_id    = $request->input( 'district_id' );
            $assign->upazila_id     = $request->input( 'upazila_id' );
            $assign->branch_id      = $request->input( 'branch_id' );
            $assign->region_id      = $request->input( 'region_id' );
            $assign->region_roads   = $roadnumberID;
            $assign->assign_type    = $request->input( 'assigntype' );
            $assign->save();

            Session::flash( 'message', 'Employee new region has assigned successfully' );
            Session::put('valid', '1');
            
        } else {
            
            Session::put('invalid', '2');
            Session::flash( 'message', 'We are unable to assing region. Please select road name before to assing.' );
            
        }

        return redirect( 'employee-regions-assign' );

    }
    
}
