<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Backend\Settings;
use Session;

class SupperAdminController extends Controller
{
	public function index() {
		return view( 'backend.settings.super-admin' );
	}
}