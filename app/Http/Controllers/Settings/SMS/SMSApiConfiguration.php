<?php

namespace App\Http\Controllers\Settings\SMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use App\Model\SMS\SMSSettings;
use App\Model\SMS\SMSHistory;
use App\Model\SMS\SMSResposneCode;
use App\Model\SMS\SMSTypes;
use App\Model\SMS\SMSEnable;
use App\Model\Frontpanel\Region;
use App\Model\Backend\Date; 
use App\Model\Frontpanel\Newlines;
use App\Model\Hrm\Employees;

class SMSApiConfiguration extends Controller 
{
    
    //$branchId  = Session::get('branchId');
    /**
     * Display the specified resource.
     * SMS
     */    
    public function smsMgt () {
        
        $monthYear   = explode(',', Date::monthYear());

        return view( 'backend.settings.sms.send-sms',[
            'smsType'      => SMSTypes::orderBy('id','desc')->get(),
            'regionInfo'   => Region::all(),
            'currentMonth' => $monthYear[0],
            'currentYear'  => $monthYear[1],
            
        ]);
        
    } 

    # Global SMS enable or not 
    public function smsEnableFunction(){

        return SMSEnable::first();
    }

    # operator sms opratin
    public static function smsOperation($number,$message) {
    
         # Operator Basic Information   
        // $operatorInfo = SMSSettings::all();  

        # Operator Basic Information   
       /* $user   = (String)$operatorInfo[0]->username;;
        $pass   = (String)$operatorInfo[0]->password;
        $sid    = (String)$operatorInfo[0]->sender;
        $url    = (String)$operatorInfo[0]->url;

        $param  = "user=$user&pass=$pass&sms[0][0]= $number &sms[0][1]=".urlencode($message)."&sms[0][2]=123456789&sms[1][0]= $number &sid=$sid";
         
        $crl = curl_init(); 
        curl_setopt($crl,CURLOPT_SSL_VERIFYPEER,FALSE); 
        curl_setopt($crl,CURLOPT_SSL_VERIFYHOST,2); 
        curl_setopt($crl,CURLOPT_URL,$url); 
        curl_setopt($crl,CURLOPT_HEADER,0); 
        curl_setopt($crl,CURLOPT_RETURNTRANSFER,1); 
        curl_setopt($crl,CURLOPT_POST,1); 
        curl_setopt($crl,CURLOPT_POSTFIELDS,$param);

         $response = curl_exec($crl); 
         curl_close($crl);

         return $response;*/
          # Operator Basic Information   
        $operatorInfo = SMSSettings::all();  
        $username  = (String)$operatorInfo[0]->username;   
        $password  = (String)$operatorInfo[0]->password;
        $sender    = (String)$operatorInfo[0]->sender;
        $url       = (String)$operatorInfo[0]->url;

        # Base API query parameters  
        if(!empty($operatorInfo[0]->username_param)) {
           
            $usernameParam       = (String)$operatorInfo[0]->username_param;
            $passwordParam       = (String)$operatorInfo[0]->password_param;
            $from_param          = (String)$operatorInfo[0]->from_param;
            $to_param            = (String)$operatorInfo[0]->to_param;
            $message_param       = (String)$operatorInfo[0]->message_param;
            $uri="$url?$usernameParam=$username&$passwordParam=$password&$from_param=$sender";
       
        }   else {

            $param  = "user=$username&pass=$password&sms[0][0]= $number &sms[0][1]=".urlencode($message)."&sms[0][2]=123456789&sms[1][0]= $number &sid=$sender";
         
            $crl = curl_init(); 
            curl_setopt($crl,CURLOPT_SSL_VERIFYPEER,FALSE); 
            curl_setopt($crl,CURLOPT_SSL_VERIFYHOST,2); 
            curl_setopt($crl,CURLOPT_URL,$url); 
            curl_setopt($crl,CURLOPT_HEADER,0); 
            curl_setopt($crl,CURLOPT_RETURNTRANSFER,1); 
            curl_setopt($crl,CURLOPT_POST,1); 
            curl_setopt($crl,CURLOPT_POSTFIELDS,$param);

             $response = curl_exec($crl); 
             curl_close($crl);

             return $response;    
        }
        
        
        return $uri;
    }

    
    # Operators information    
    public static function operatorBasicInformation($number,$message){
        /* $operatorInfo = SMSSettings::all();  

        # Operator Basic Information   
        $username   = (String)$operatorInfo[0]->username;;
        $password   = (String)$operatorInfo[0]->password;
        $sender     = (String)$operatorInfo[0]->sender;
        $url        = (String)$operatorInfo[0]->url;

       
         
        $crl = curl_init(); 
        curl_setopt($crl,CURLOPT_SSL_VERIFYPEER,FALSE); 
        curl_setopt($crl,CURLOPT_SSL_VERIFYHOST,2); 
        curl_setopt($crl,CURLOPT_URL,$url); 
        curl_setopt($crl,CURLOPT_HEADER,0); 
        curl_setopt($crl,CURLOPT_RETURNTRANSFER,1); 
        curl_setopt($crl,CURLOPT_POST,1); 
        curl_setopt($crl,CURLOPT_POSTFIELDS,$uri);

         $response = curl_exec($crl); 
         curl_close($crl);

         return $response;*/
        # Operator Basic Information   
        $operatorInfo = SMSSettings::all();  
      
        $username  = (String)$operatorInfo[0]->username;   
        $password  = (String)$operatorInfo[0]->password;
        $sender    = (String)$operatorInfo[0]->sender;
        $url       = (String)$operatorInfo[0]->url;

        # Base API query parameters  
        if(!empty($operatorInfo[0]->username_param)) {
           
            $usernameParam       = (String)$operatorInfo[0]->username_param;
            $passwordParam       = (String)$operatorInfo[0]->password_param;
            $from_param          = (String)$operatorInfo[0]->from_param;
            $to_param            = (String)$operatorInfo[0]->to_param;
            $message_param       = (String)$operatorInfo[0]->message_param;
            
            $url="$url?$usernameParam=$username&$passwordParam=$password&$from_param=$sender";
            $uri = $url."&To=$number&Message=$message";

        }   else {
            
            $url       = (String)$operatorInfo[0]->url;
            $uri  = "user=$username&pass=$password&sms[0][0]= $number &sms[0][1]=".urlencode($message)."&sms[0][2]=123456789&sms[1][0]= $number &sid=$sender";

            
            $curl = curl_init(); 
            curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,FALSE); 
            curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,2); 
            curl_setopt($curl,CURLOPT_URL,$url); 
            curl_setopt($curl,CURLOPT_HEADER,0); 
            curl_setopt($curl,CURLOPT_RETURNTRANSFER,1); 
            curl_setopt($curl,CURLOPT_POST,1); 
            curl_setopt($curl,CURLOPT_POSTFIELDS,$uri);
            $response = curl_exec($curl); 
            curl_close($curl);
           
            return $response;
        }

        return $uri;

    }

    # SMS sending History updates
    public function smsSendingHistory($newconid,$mobilenumber,$moduleCode,$status_code){

            $history= new SMSHistory();
            $history->newconid      = $newconid;
            $history->mobile        = $mobilenumber;
            $history->module_code   = $moduleCode;
            $history->response_code = $status_code;
            $history->save();
    }

    # Message code return from response_code table

    public function getMessageText($moduleCode){

        $message= DB::table( 'sms_type' )
                ->select('message')
                ->where( 'module_code', $moduleCode )
                ->first(); 
      
        return $message;
    } 
    # Return client mobile number from client_info table

    public function getClientMobileNumber($newconid){
        $mobile = DB::table( 'client_info' )
                ->where( 'newconid', $newconid )
                ->first(); 

        
        return $this->validateMobileNumber($mobile->mobilenumber);
    }

    # Validate mobile number
    public function validateMobileNumber($number){
      
        $strPreg= preg_replace('/[^A-Za-z0-9\-]/', '', $number);
       
        return trim(str_replace("-", "", $strPreg));

    }  

    # Curl Initialization 
    public function curlInitialization($urlLink){
        
        $curl = curl_init();  

        $operatorInfo = SMSSettings::all();  
        # Base API query parameters  
        if(empty($operatorInfo[0]->username_param)) {
   

        } else {

            curl_setopt($curl, CURLOPT_URL,$urlLink );
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $result      = curl_exec($curl);
            $str         = strval($result);

            return array("status_code"=>$status_code,"result"=>$result);   
        }
        
    }

    /**
     * [sendSMSTechnician description]
     */
    public function sendSMSTechnician($id) {

        $smsID        = explode(',', $id);
        $employeeID   =  $smsID[1];

        if(empty($employeeID)) {

            Session::put( 'invalid', 2 );
            Session::flash('message','Please Assing Technichain');

            return back();
        }

        $sendTech     = Employees::where('employeeID', $employeeID)->first();

        $clientInfo   = Newlines::leftjoin('region','client_info.region','=','region.region_id')
            ->leftjoin('road','client_info.road','=','road.id')
            ->leftjoin('instruments','client_info.newconid','=','instruments.newconid')
            ->leftjoin('packages_info','instruments.packageid','=','packages_info.package_id')
            ->select('instruments.box_no','client_info.newconid','client_info.id','packages_info.packagename','client_info.username','client_info.flat_no','client_info.password','road.road','region.region_name','instruments.mac_address','client_info.mobilenumber','client_info.firstname','client_info.house','client_info.lastname')
            ->where('client_info.newconid',  $smsID[0])->first();
        
        if(isset($smsID[2])) {

            $type = $smsID[2];

        } else {

            $type = 'New Connection';
        }

            $smsEnability = SMSEnable::first();

            if($smsEnability->is_enable) {

                    # Call URI for operator basic information
                    $smsObj = new SMSApiConfiguration();
                    
                    $moduleCode="NL";

                    # Global SMS message from sms_type table
                    $getMessage= $smsObj->getMessageText($moduleCode);

                    # Encode the Message for removing space        
                    $smsMessage = $clientInfo->id.' '.$clientInfo->firstname.' '.$clientInfo->lastname . "(".$smsID[0].") PH: ".$clientInfo->mobilenumber."F: ".$clientInfo->flat_no." H:  ".$clientInfo->house." R: ".$clientInfo->road." A: ".$clientInfo->region_name." P: ".$clientInfo->packagename." L: ".$clientInfo->username." P: ".$clientInfo->password." M: ".$clientInfo->mac_address." C:".$type."  B: ".$clientInfo->box_no."";
                    $smsInc     = implode($smsID[0],explode("ID", $smsMessage));
                    $number     = str_replace("_","",trim($sendTech->mobileNumber));
                    $mobilenumber = $smsObj->validateMobileNumber($number); 
                    $to    =  $mobilenumber = "88".$mobilenumber;
                   
                    # CURL GET Method
                    $url= $smsObj->operatorBasicInformation($mobilenumber,$smsMessage);   

                    $resultArray = $smsObj->curlInitialization($url);
                      
                 $smsCode = $smsObj->smsSendingHistory($smsID[0],$mobilenumber,$moduleCode,1); 
                
                /*******************************************************************
                *********************END SMS SENDING *******************************
                ********************************************************************/

            }
            Session::put( 'valid', 2 );
            Session::flash('message','SMS has Send successfully');
            return back();
    }

    # Send Due bill SMS
    public function dueBillSmsSending($sms_category,$billno) {
       
        // print_r(explode(',',$sms_category,0));

        if($sms_category=="ALLDUENOTI") {

            $moduleCode ="ALLDUENOTI";
            $dueClientList = DB::table('client_info')                      
                ->leftjoin('bill_generate', 'client_info.newconid', '=', 'bill_generate.newconid')
                ->leftjoin('bill_collection', 'bill_generate.billno', '=', 'bill_collection.billno')
                ->select('client_info.newconid','client_info.mobilenumber')
                ->where('bill_generate.duestatus', 1)
                ->whereIn('bill_generate.billno',$billno)
                ->where('client_info.linestatus', '!=',2) // disconneted client data is not allowed here
                ->where('bill_generate.linestatus', '!=',2)// disconneted client data is not allowed here
                ->orderby('client_info.road', 'asc')
                ->orderby('client_info.house', 'asc')
                ->paginate(2500);
            
        } else if($sms_category=="PDUENOTI") {

            $moduleCode ="PDUENOTI";

            $dueClientList = DB::table('client_info')                      
                ->leftjoin('bill_collection', 'client_info.newconid', '=', 'bill_collection.newconid')
                ->leftjoin('bill_generate', 'bill_collection.billno', '=', 'bill_generate.billno')
                ->select('client_info.newconid','client_info.mobilenumber')
                ->where('bill_collection.dueamount','>',0)
                ->whereIn('bill_generate.billno',$billno)
                ->paginate(2500);

        }  else if($sms_category=="FDUENOTI") {

            $moduleCode ="FDUENOTI";

            $dueClientList = DB::table('client_info')           
                ->leftjoin('bill_generate', 'client_info.newconid', '=', 'bill_generate.newconid')
                ->leftjoin('bill_collection', 'bill_generate.billno', '=', 'bill_collection.billno')
                ->select('client_info.newconid','client_info.mobilenumber')
                ->where('bill_generate.status', 0)
                ->whereIn('bill_generate.billno',$billno)
                ->where('client_info.linestatus', '!=',2) // disconneted client data is not allowed here
                ->where('bill_generate.linestatus', '!=',2)// disconneted client data is not allowed here
                ->orderby('client_info.road', 'asc')
                ->orderby('client_info.house', 'asc')
                ->paginate(2500);     

        } 
      
        $smsObj = new SMSApiConfiguration();
        foreach ($dueClientList as $dueBill) {
            
            # SMS Sending when bill has collected
            # Mobile number fetching from clicnt_info table  
            $newconid     = $dueBill->newconid;    
            $mnumber      = $this->validateMobileNumber($dueBill->mobilenumber); 
            $mobilenumber = str_replace("_","",trim($mnumber));
            # Call URI for operator basic information
            $uri = $smsObj->operatorBasicInformation();
            
            # Global SMS message from sms_type table
            $getMessage= $smsObj->getMessageText($moduleCode);
            
            # Encode the Message for removing space        
            $smsMessage = urlencode($getMessage->message); 
            $to    =  $mobilenumber = "88".$mobilenumber;
         

          /*  # CURL GET Method
            $url = $uri."&To=$to&Message=$smsMessage";        
            $resultArray = $this->curlInitialization($url);
            $status_code = $resultArray['status_code'];
            $result = $resultArray['result'];
            if($status_code || !$status_code) {
                //echo "Your message has sent.";
            }

            # SMS History Updates  */      
            $smsCode=$smsObj->smsSendingHistory($newconid,$mobilenumber,$moduleCode,$status_code); 
        }

        return 200;
    }

    # Send Client SMS
    public function smsApiConfigure(Request $request){    

        $smsEnability = SMSEnable::first();      

        if($smsEnability->is_enable) {
            # Due bill SMS sending
            $sms_category = $request->input( 'sms_category' );
            $billno       = $request->input( 'billno' );

          
            if($sms_category=='ALLDUENOTI' || $sms_category=='PDUENOTI' || $sms_category='FDUENOTI'){
                
                $dueBill= $this->dueBillSmsSending($sms_category,$billno); 
               
                Session::put('valid', '1');
                Session::flash( 'message', 'Message has send to the due bills' );
                return redirect( 'due/bill/summery' );
            }       

        }    


            // POST Method

           /* $msg="Test message from ISPERP";
            $url="https://api.mobireach.com.bd/SendTextMessage?Username=mazedanetworks&Password=Robi@1234&From=Mazeda.Net&To=8801989442856&Message=Hello";
            $ch = curl_init($url);
            //curl_setopt($ch, CURLOPT_URL,$url);  
            curl_setopt($ch, CURLOPT_USERPWD, "mazedanetworks:Robi@1234");
            //curl_setopt($ch, CURLOPT_POSTFIELDS,"From=Mazeda.Net&To=8801989442856");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);            
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,3);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);

            $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
            $result      = curl_exec ($ch);
            curl_close ($ch);            
            print_r($status_code);*/
    }

    /**
     * [categorySmsCategory description]
     * @return [type] [description]
     */
    public function categorySmsCategory() {

        $code        = \Input::get('code');
        $message     = \Input::get('message');
        // $module_name = \Input::get('module_name');
        $region_id   = \Input::get('region_id');

        $newlineInfo = Newlines::where('region',$region_id)
            ->where('linestatus',1)
            ->take(1)
            ->get();


        $smsObj = new SMSApiConfiguration();

        # SMS Enable option
        $smsEnability = SMSEnable::first();
        if($smsEnability->is_enable) {
            foreach ($newlineInfo as $client) {
                
                # SMS Sending when bill has collected
                # Mobile number fetching from clicnt_info table  
                $newconid     = $client->newconid;    
                $mnumber      = $this->validateMobileNumber($client->mobilenumber); 
                $mobilenumber = str_replace("_","",trim($mnumber));
                # Call URI for operator basic information
                $uri = $smsObj->operatorBasicInformation();
                
                # Global SMS message from sms_type table
                // $getMessage= $smsObj->getMessageText($code);
                // return $getMessage;
                # Encode the Message for removing space  
                // return $message; 
                $smsMessage = urlencode($message ); 
                $to    =  $mobilenumber = "88".$mobilenumber;
             

                # CURL GET Method
                $url = $uri."&To=$to&Message=$smsMessage";        
                $resultArray = $this->curlInitialization($url);
                $status_code = $resultArray['status_code'];
                $result      = $resultArray['result'];
                if($status_code || !$status_code) {
                    //echo "Your message has sent.";
                }

                # SMS History Updates        
                $smsCode=$smsObj->smsSendingHistory($newconid,$mobilenumber,$code,$status_code); 
            }
        }
       /* echo "<pre>";
        print_r($newlineInfo);
        die();*/


    }    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    # SMS Message Search
    public function smsMessageSearch() {

       $code    = \Input::get('code');
       $smsInfo = SMSTypes::where('module_code',$code)->first();

       return $smsInfo->message;
    }
}
