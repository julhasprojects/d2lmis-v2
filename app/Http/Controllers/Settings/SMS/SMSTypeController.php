<?php

namespace App\Http\Controllers\Settings\SMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SMS\SMSTypes; 
use Session;

class SMSTypeController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
        return view('backend.settings.sms.sms-type.index',[
            'smsTypes' => SMSTypes::orderBy('id','desc')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
         $this->validate($request, [
            'name'    => 'required',
            'code'    => 'required',
            'message' => 'required',
        ]);

        $smsTypes = new SMSTypes();
        $smsTypes->module_name   = \Input::get('name');
        $smsTypes->module_code   = \Input::get('code');
        $smsTypes->message       = \Input::get('message');
        $smsTypes->active_status = \Input::get('status');

        // $smsTypes->message_param    = \Input::get('details');

        if($smsTypes->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','SMS Setting has created successfully');

        }  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to create new SMS Setting. Try again!.');

        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        return view('backend.settings.sms.sms-type.edit',[
            'smsTypes' => SMSTypes::find($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

         $this->validate($request, [
            'name'    => 'required',
            'code'    => 'required',
            'message' => 'required',
        ]);

        $smsTypes = SMSTypes::find($id);
        $smsTypes->module_name   = \Input::get('name');
        $smsTypes->module_code   = \Input::get('code');
        $smsTypes->message       = \Input::get('message');
        $smsTypes->active_status = \Input::get('status');

        // $smsTypes->message_param    = \Input::get('details');

        if($smsTypes->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','SMS type setting has updated successfully');

        }  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to update SMS type setting. Please try again!.');

        }

        return redirect('sms/type/setup');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)  {
        
        SMSTypes::where('id',$req->id)->delete();
    
        return response()->json();
    }
}
