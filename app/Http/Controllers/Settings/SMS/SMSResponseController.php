<?php

namespace App\Http\Controllers\Settings\SMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SMS\SMSResponseCode; 
use App\Model\SMS\SMSSettings; 
use Session;
class SMSResponseController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $smsRespones = SMSResponseCode::all();
        $operationId = SMSSettings::all();
        
        return view('backend.settings.sms.sms-response.index',[
            'smsRespones' => $smsRespones,
            'operationId' => $operationId
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
        $this->validate($request, [
            'id'   => 'required',
            'code' => 'required',
        ]);

        $respoes = new SMSResponseCode();
        $respoes->operator_id      = \Input::get('id');
        $respoes->response_code    = \Input::get('code');
        $respoes->response_details = \Input::get('details');

        if($respoes->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','SMS Response has updated successfully');

        }  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to create SMS Response. Please try again!.');

        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $smsRespones = SMSResponseCode::find($id);
        $operationId = SMSSettings::all();
        return view('backend.settings.sms.sms-response.edit',[
            'smsRespones' => $smsRespones,
            'operationId' => $operationId
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( $id) {

        $respoes = SMSResponseCode::find($id);
        $respoes->operator_id      = \Input::get('id');
        $respoes->response_code    = \Input::get('code');
        $respoes->response_details = \Input::get('details');

        if($respoes->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','SMS response has updated successfully');

        }  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to update SMS Response. Please try again!.');

        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req) {

        SMSResponseCode::where('id',$req->id)->delete();
    
        return response()->json();
    }
}
