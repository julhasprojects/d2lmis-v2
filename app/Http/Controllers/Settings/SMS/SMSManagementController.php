<?php

namespace App\Http\Controllers\Settings\SMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SMS\SMSSettings; 
use App\Model\SMS\SMSEnable; 
use App\Model\SMS\SMSTypes; 
use Session;

class SMSManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
        return view('backend.settings.sms.sms-mgt.index',[
            'smsSettings' => SMSSettings::all(),
            'smsEnable'   => SMSEnable::all(),
            
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name'           => 'required',
            'code'           => 'required',
            'usrname'        => 'required',
            'password'       => 'required',
            'sender'         => 'required',
            'url'            => 'required',
            'password_param' => 'required',
            'from_param'     => 'required',
            'to_param'       => 'required',
        ]);

        $smsSettings = new SMSSettings();
        $smsSettings->operator_name    = \Input::get('name');
        $smsSettings->short_code       = \Input::get('code');
        $smsSettings->username         = \Input::get('usrname');
        $smsSettings->password         = \Input::get('password');
        $smsSettings->sender           = \Input::get('sender');
        $smsSettings->url              = \Input::get('url');
        $smsSettings->username_param   = \Input::get('password_param');
        $smsSettings->password_param   = \Input::get('from_param');
        $smsSettings->from_param       = \Input::get('to_param');
        $smsSettings->to_param         = \Input::get('message_param');
        // $smsSettings->message_param    = \Input::get('details');

        if($smsSettings->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','SMS Setting has created successfully');

        }  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to create SMS settings information!.');

        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        return view('backend.settings.sms.sms-mgt.edit',[
            'smsSettings' => SMSSettings::find($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $this->validate($request, [
            'name'           => 'required',
            'code'           => 'required',
            'usrname'        => 'required',
            'password'       => 'required',
            'sender'         => 'required',
            'url'            => 'required',
            'password_param' => 'required',
            'from_param'     => 'required',
            'to_param'       => 'required',
        ]);

        $smsSettings =  SMSSettings::find($id);
        $smsSettings->operator_name    = \Input::get('name');
        $smsSettings->short_code       = \Input::get('code');
        $smsSettings->username         = \Input::get('usrname');
        $smsSettings->password         = \Input::get('password');
        $smsSettings->sender           = \Input::get('sender');
        $smsSettings->url              = \Input::get('url');
        $smsSettings->username_param   = \Input::get('password_param');
        $smsSettings->password_param   = \Input::get('from_param');
        $smsSettings->from_param       = \Input::get('to_param');
        $smsSettings->to_param         = \Input::get('message_param');
        // $smsSettings->message_param    = \Input::get('details');

        if($smsSettings->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','SMS Setting has updated successfully');

        }  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to update SMS Setting. Please try again!.');

        }

        return redirect('sms/management');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)  {

        SMSSettings::where('id',$req->id)->delete();
    
        return response()->json();
    }

    public function smsManagmentGlobal(Request $request) {

        $this->validate($request, [
            'is_enable'           => 'required',
        ]);

        $smsEnable = SMSEnable::first();

        if(!empty($smsEnable)) {
            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to create SMS Setting. Try again!.');
        }

        $smsSettings = new SMSEnable();
        $smsSettings->is_enable = \Input::get('is_enable');


        if($smsSettings->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','SMS Setting has created successfully');

        }  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to create SMS Setting. Try again!.');

        }

        return redirect('sms/management')->with('status', '2');;
    }

    public function smsManagmentGlobalEdit($id) {
        return view('backend.settings.sms.sms-mgt.global-edit',[
            'smsEnable'   => SMSEnable::find($id),
        ]);
    }

    public function smsManagmentGlobalUpdate($id) {

        $smsSettings =  SMSEnable::find($id);
        $smsSettings->is_enable = \Input::get('is_enable');


        if($smsSettings->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','SMS Setting has updated successfully');

        }  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to create SMS Setting. Please try again!.');

        }

        return back();
    }
    
    public function smsManagmentGlobalDestroy() {

        $id = \Input::get('id');
        SMSEnable::where('id',$id)->delete();
    
       
    }
}
