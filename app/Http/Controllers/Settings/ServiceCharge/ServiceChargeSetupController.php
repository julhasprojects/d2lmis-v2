<?php

namespace App\Http\Controllers\Settings\ServiceCharge;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Backend\ServiceCharge; 
use App\Model\Backend\Date; 
use Input;
use DB;
use Session;

class ServiceChargeSetupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $monthYear = explode(',', Date::monthYear() );
        return view('backend.settings.service-charge.index',
            [
                'chargeInfo' => ServiceCharge::all(),
                'currentMonth' => $monthYear[0],
            ]) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $serviceCharge = new ServiceCharge();
        $serviceCharge->firstMonth  = \Input::get('firsMonth');
        $serviceCharge->secondMonth = \Input::get('secondMonth');
        $serviceCharge->charge      = \Input::get('service_charge');

        if($serviceCharge->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','Service charge has created successfully');

        } else {
            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to create service charge. Please try again!.');

        }
        
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $monthYear = explode(',', Date::monthYear() );

        return view('backend.settings.service-charge.edit',
            [
                'chargeInfo' => ServiceCharge::find($id),
                'currentMonth' => $monthYear[0],
            ]) ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $serviceCharge =  ServiceCharge::find($id);
        $serviceCharge->firstMonth  = \Input::get('firsMonth');
        $serviceCharge->secondMonth = \Input::get('secondMonth');
        $serviceCharge->charge      = \Input::get('service_charge');

        if($serviceCharge->save()) {
            
            Session::put( 'valid', 1 );
            Session::flash('message','Service charge has created successfully');

        } else {
            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to create service charge. Please try again!.');

        }
        
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
