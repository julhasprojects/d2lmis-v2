<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Backend\Date; 
use App\Model\Backend\BillSetup; 
use Session;

class BillSetupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $billingInfo = BillSetup::first();


        $billSetup = new BillSetup();

        $billSetup->costing_type = \Input::get('costingtype');

        $billInfo = BillSetup::first();

        if(!empty($billInfo)) {

            Session::put('invalid', '2');
            Session::flash('message', 'Sorry! Billing cycle setup already exist. Please modify billing cycle.');
            return redirect('bill/date/and/system/setup')->with('status', '1');

        }

        if($billSetup->save() ){

            Session::put('valid', '1');
            Session::flash('message', 'Congratulations! Your billing cycle setup has done successfully.');

        
        }  else {

            Session::put('invalid', '2');
            Session::flash("message", "Sorry! We can't setup your billing cycle setup.");
            
        }

        return redirect('bill/date/and/system/setup')->with('status', '1');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         return view( 'backend.settings.date.bill-setup-edit',[
            'billSystem' => BillSetup::find($id),
        ] );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id) {

        $billSetup = BillSetup::find($id);
        
        $billSetup->costing_type = \Input::get('costingtype');

        if($billSetup->save() ){

            Session::put('valid', '1');
            Session::flash('message', 'Congratulations! Your billing cycele information has updated successfully.');

        }  else {

            Session::put('invalid', '2');
            Session::flash("message", "Sorry! We can't update your billing cycle information.");
            
        }
        
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        BillSetup::find($id)->delete();

        Session::flash('message', 'Your billing cycle information has deleted successfully.');
        return back();
    }
}
