<?php

namespace App\Http\Controllers\Settings\Base;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Backend\Settings;
use App\Model\Backend\BaseSetup;
use App\Model\Backend\InterfaceSetup;
use App\Model\Backend\Package;
use Session;

class BaseSetupController extends Controller
{
	public function index() {
		$baseInfo=BaseSetup::leftjoin('interface_setup', 'base_setup.interface_type', '=', 'interface_setup.id')
			->select('interface_setup.interface_name','base_setup.id','base_setup.base_name','base_setup.created_at')
			->get();
		return view( 'backend.settings.base.base-setup',[
			'baseInfo'      =>$baseInfo,
			'interfaceInfo' =>InterfaceSetup::all(),
		] );
	}

	public function store() {

		$BaseSetup = New BaseSetup();
		$BaseSetup->interface_type =\Input::get('interfacename');
		$BaseSetup->base_name      =\Input::get('basename');

		if($BaseSetup->save()) {
			Session::put( 'valid', 1 );
            Session::flash('message','New base information has created successfully');
		}  else {
            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to create your information. Please try again!.');
        }
        return back();   
	}	

	public function edit($id) {
		return view( 'backend.settings.base.base-setup-edit',[
			'baseInfo'    =>BaseSetup::where('id',$id)->first(),
			'interfaceInfo' =>InterfaceSetup::all(),
		] );
	} 

	public function update($id) {
		$BaseSetup=BaseSetup::where( 'id', $id )->first();
		$BaseSetup->interface_type =\Input::get('interfacename');
		$BaseSetup->base_name      =\Input::get('basename');

		if($BaseSetup->save()) {
			Session::put( 'valid', 1 );
            Session::flash('message','Package cost setup Update successfully');
		}  else {
            Session::put( 'invalid', 2 );
            Session::flash('message','We can not charge_amount base setup. Try again!.');
        }
         return back();
	}

	public function destroy($id) {
		BaseSetup::where('id',$id)->delete();
		Session::put( 'valid', 1 );
        Session::flash('message','base delete successfully');
		return back();
	}
}