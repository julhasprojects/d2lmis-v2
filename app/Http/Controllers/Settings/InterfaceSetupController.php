<?php

namespace App\Http\Controllers\Settings;


use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Backend\Settings;
use App\Model\Backend\InterfaceSetup;
use App\Model\Backend\Package;
use Session;

class InterfaceSetupController  extends Controller
{
	public function index() {

		return view( 'backend.settings.interface.interface-setup',[
			'interfaceInfo' => InterfaceSetup::all(),
		] );
	}

	public function store(Request $request) {

		#validation section
        $this->validate($request, [
            'interfacename' => 'required',
        ]);

		$InterfaceSetup = New InterfaceSetup();
		$InterfaceSetup->interface_name  =\Input::get('interfacename');

		if($InterfaceSetup->save()) {

			Session::put( 'valid', 1 );
            Session::flash('message','Interface has created successfully');

		}  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to create new interface. Please Try again!.');

        }
         return back();   
	}	

	public function edit($id) {

		return view( 'backend.settings.interface.interface-setup-edit',[
			'interfaceInfo' =>InterfaceSetup::where('id',$id)->first(),
		] );

	} 

	/**
	 * [Interface Setup Updated]
	 * @param  [get] $id [description]
	 * @return [post]     [description]
	 */
	public function update(Request $request,$id) {
		
		#validation section
        $this->validate($request, [
            'interfacename' => 'required',
        ]);

		$InterfaceSetup = InterfaceSetup::where( 'id', $id )->first();
		$InterfaceSetup->interface_name =\Input::get('interfacename');

		if($InterfaceSetup->save()) {

			Session::put( 'valid', 1 );
            Session::flash('message','Interface information has updated successfully');

		}  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to update data.Please try again!.');

        }
        
        return redirect('interface/setup');
	}

	public function destroy($id) {

		InterfaceSetup::where('id',$id)->delete();
		Session::put( 'valid', 1 );
        Session::flash('message','Interface data has deleted successfully');
		return back();

	}
}