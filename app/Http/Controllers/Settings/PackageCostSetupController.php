<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Backend\Settings;
use App\Model\Backend\PackageCostSetup;
use Session;

class PackageCostSetupController extends Controller
{
	public function index() {

		return view( 'backend.settings.package-cost.package-cost-setup',[
			'chargeInfo' =>PackageCostSetup::all()
		] );
	}

	public function store(Request $request) {

		#validation section
        $this->validate($request, [
            'chargeamount'     => 'required',
        ]);

        $packageCost = PackageCostSetup::first();

        if(isset($packageCost)) {
        	
        	Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to create duplicate package cost. Please try again!.');
            return back();
        }

        # Package Inset Data
		$packagecostsetup = New PackageCostSetup();
		$packagecostsetup->charge_allow  =\Input::get('chargeallow');
		$packagecostsetup->charge_amount =\Input::get('chargeamount');

		if($packagecostsetup->save()) {

			Session::put( 'valid', 1 );
            Session::flash('message','New package cost has created successfully');

		}  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to create package cost. Please try again!.');

        }

        return back();   
	}	

	public function edit($id) {

		return view( 'backend.settings.package-cost.package-cost-setup-edit',[
			'chargeInfo' =>PackageCostSetup::where('id',$id)->first()
		] );

	} 

	public function update(Request $request,$id) {

		# validation section
        $this->validate($request, [
            'chargeamount'     => 'required',
        ]);

        # Update package information
		$packagecostsetup = PackageCostSetup::where( 'id', $id )->first();
		$packagecostsetup->charge_allow  = \Input::get('chargeallow');
		$packagecostsetup->charge_amount = \Input::get('chargeamount');

		if($packagecostsetup->save()) {

			Session::put( 'valid', 1 );
            Session::flash('message','Package cost setup information has updated successfully');

		}  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to update package setup cost information. Please try again!.');

        }

        return back();
	}

	public function destroy($id) {

		PackageCostSetup::where('id',$id)->delete();
		Session::put( 'valid', 1 );
        Session::flash('message','Your package cost setup has deleted succesfully.');
		return back();

	}
}