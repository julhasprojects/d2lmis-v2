<?php

namespace App\Http\Controllers\Settings\UserManual;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Backend\Date; 
use App\Model\Backend\BillSetup; 
use Session;

class UserManualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
        return view('backend.settings.user-manual.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

   
}
