<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Backend\Settings;
use App\Model\Backend\UnamepasswordCombination;
use Session;
use Input;

class NewlineSettingsController extends Controller
{
    public function index() {

        $passwordCombination = UnamepasswordCombination::leftjoin('employees','unamepassword_combination.created_by','employees.employeeID')
            ->select('employees.fullName','unamepassword_combination.*')
            ->get();

        $connectionFeeCharge = UnamepasswordCombination::leftjoin('employees','unamepassword_combination.created_by','employees.employeeID')
            ->select('employees.fullName','unamepassword_combination.*')
            ->first();

        return view( 'backend.settings.newline.newline-setup',[
            'unamepasswordCombination' => $passwordCombination,
            'connectionFeeCharge'      => $connectionFeeCharge,
        ] );
    }

    /**
     * [store description]
     * @return [type] [description]
     */
    public function store() {

        $usernamefield    = \Input::get('username');
        $passwordname     = \Input::get('passwordnames');
        $patternusername  = \Input::get('patternusername');
        $paternpassword   = \Input::get('paternpassword');
        $defualtpasswords = Input::get('defualtpasswords');
        

        $usernameInfo     = UnamepasswordCombination::first();
      
        if(!empty($usernameInfo)) {

            Session::put( 'invalid', 2 );
            Session::flash('message','Sorry! Newline Settings already exist. Please modify Newline Settings.');
            return back();

        }

        if(empty($usernamefield) ) {

            Session::put( 'invalid', 1 );
            Session::flash('message','username Setup can not be empty. Plesae enter username Setup.');
            return back();
        } 

        $usernameCombination = implode(',',$usernamefield);
        $passwordCombination = implode(',',$passwordname);

        $setting = new UnamepasswordCombination();
        $setting->uname_fields     = $usernameCombination.',';
        $setting->pass_fields      = $passwordCombination.','; 
        $setting->uname_pattern    = $patternusername;
        $setting->pass_pattern     = $paternpassword;
        $setting->nl_request_fee   =\Input::get('nlrequestfee');
        $setting->created_by       = Session::get('emp_id');
        $setting->slug             = \Input::get('usernameslug');
        $setting->default_password =  $defualtpasswords;

        if($setting->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','Newline username has created successfully');

        } else {

            Session::put( 'invalid', 2 );
            Session::flash('message','There is an issue to create newline setting. Please Try again!.');
        }

        return back();          
    }

    public function edit($id) {

        $editInfo = UnamepasswordCombination::find($id);

        return view( 'backend.settings.newline.newline-edit',[
            'editInfo' => $editInfo,
        ] );

    }


    public function newlineSettingChargeUpdate($id) {

        $newlineCharge = UnamepasswordCombination::where( 'id', $id )->first();
        $newlineCharge->charge_setup = \Input::get('chargSetup');
        $newlineCharge->save();
        return back();
    }

    public function destroy($id) {

        UnamepasswordCombination::where( 'id', $id )->delete();
        Session::put( 'valid', 1 );
        Session::flash('message','Your record has deleted successfully');
        return back();

    }

    public function newlineSettingDeleted() {

        $id = \Input::get('id');
        UnamepasswordCombination::where( 'id', $id )->delete();
     
    }
}
