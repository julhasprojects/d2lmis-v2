<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Backend\Mikrotik;
use App\Model\Backend\BillSetup; 
use App\Model\Backend\PackageCostSetup;

use App\Model\SMS\SMSEnable;
use App\Model\Frontpanel\Newlines;
use App\Model\Frontpanel\Email;
use App\Model\Inventory\InventoryEnable;

use DB;


class GlobalSystemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
     
        return view( 'backend.settings.global.global-system-setup',[
            'mikortik'     => Mikrotik::first(),
            'billSetup'    => BillSetup::first(),
            'packgeCharge' => PackageCostSetup::first(),
            'smsEnable'    => SMSEnable::first(),
            'emailEnable'  => Email::first(),
            'inventory'  => InventoryEnable::first(),
        ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
        $type = \Input::get('type');

        if( $type == 1) {
            
            $mikrotiks = Mikrotik::all();

            foreach ($mikrotiks as $mikrotik) {
                Mikrotik::where('id',$mikrotik->id)->delete();
            }

            $mikrotik = new Mikrotik();
            $mikrotik->is_enable   = \Input::get('status');

            if($mikrotik->save()) {

                return 101;
            }  else {

                return 102;

            }  

        } elseif( $type == 2) {

            $smsEnable = SMSEnable::first();
            $smsEnable->is_enable  = \Input::get('status');

            if($smsEnable->save()) {

                return 201;
            }  else {

                return 202;

            }            
        } elseif( $type == 3) {

        }   elseif( $type == 3) {

        } elseif( $type == 4) {

        } elseif( $type == 5) {

            $billSetups = BillSetup::all();

            foreach ($billSetups as $billSetup) {
                BillSetup::where('id',$billSetup->id)->delete();
            }

            $billSetup = new BillSetup();
            $billSetup->costing_type   = \Input::get('status');

            if($billSetup->save()) {

                return 501;
            }  else {

                return 502;

            }  
        }  elseif( $type == 6) {

            $PackageCostSetup = PackageCostSetup::first();
            $PackageCostSetup->charge_allow  = \Input::get('status');

            if($PackageCostSetup->save()) {

                return 601;
            }  else {

                return 602;

            }  
        }

        
        
    }

    // Global system all
    public function globalSystemAll() {
    
        $type   = \Input::get('type');
        $status = \Input::get('status');

        // Mikrotik seting
        if( $type == 1) {
            
            $mikrotiks = Mikrotik::first();

            if(empty($mikrotiks)) {  

                $mikrotik = new Mikrotik();

            }   else {
                $mikrotik = Mikrotik::first();
            }

            $mikrotik->is_enable   = $status;

            if($mikrotik->save()) {

                return 101;

            }  else {

                return 102;

            }  

        // SMS Enable Setting

        } elseif( $type == 2) {

            $smsList = SMSEnable::first();

            if(empty($smsList)) {

                $smsEnable = new SMSEnable();

            }   else {

                $smsEnable = SMSEnable::first();
                
            }

            $smsEnable->is_enable  = $status;

            if($smsEnable->save()) {

                return 201;

            }  else {

                return 202;

            }        
            // Email Enable Setting
        } elseif( $type == 3) {
            $emailEnable = Email::first();

            if(empty($emailEnable)) {

                $email = new Email();

            }   else {

                $email = Email::first();
                
            }

            $email->is_enable  = $status;

            if($email->save()) {

                return 201;

            }  else {

                return 202;

            }   
        } elseif( $type == 4) {

            $inventoryList = InventoryEnable::first();

            if(empty($inventoryList)) {

                $InventoryEnable = new InventoryEnable();

            }   else {

                $InventoryEnable = InventoryEnable::first();
                
            }

            $InventoryEnable->is_enable  = $status;

            if($InventoryEnable->save()) {

                return 201;

            }  else {

                return 202;

            } 

        } elseif( $type == 5) {

            $billSetups = BillSetup::all();

            foreach ($billSetups as $billSetup) {
                BillSetup::where('id',$billSetup->id)->delete();
            }

            $billSetup = new BillSetup();
            $billSetup->costing_type   = \Input::get('status');

            if($billSetup->save()) {

                return 501;
            }  else {

                return 502;

            }  
        }  elseif( $type == 6) {

            $PackageCostSetup = PackageCostSetup::first();
            $PackageCostSetup->charge_allow  = \Input::get('status');

            if($PackageCostSetup->save()) {

                return 601;

            }  else {

                return 602;

            }  
        } elseif( $type == 10) {

            $mikrotikSetup = Mikrotik::first();
            $mikrotikSetup->is_defaulter  = \Input::get('status');

            if($mikrotikSetup->save()) {

                return 1001;

            }  else {

                return 1002;

            }  
        }
      
    }

    public function mobileNumberFormation() {

        $numbers = Newlines::select('mobilenumber', 'newconid')->get();
    
        if ( !empty($numbers) ) {
            
            foreach ($numbers as $key => $info) {

                if( ! empty($info->mobilenumber) && ( strpos($info->mobilenumber, '&') || strpos($info->mobilenumber, ',') )  ) {
                 
                    if ( count($number = explode("&", $info->mobilenumber)) >=2 ) {
                   
                        $number = $number[0];

                    } else if (count($number = explode(",", $info->mobilenumber)) >=2) {

                        $number = $number[0];
                    }

                } else {

                    $number = $info->mobilenumber;
                }

                $number = implode("", explode("-", $number));
                
                DB::table('client_info')
                    ->where('newconid', $info->newconid)
                    ->update([
                        'mobilenumber' => $number
                    ]);

            }

            return 200;

        }

        return 504;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
