<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Frontpanel\Newlines; 
use App\Model\Frontpanel\Instruments; 
use App\Model\Frontpanel\Empoffice; 
use App\Model\Backend\NewlineSetup; 
use App\Model\Backend\Package; 
use App\Model\Backend\BillSetup;
use App\Model\Backend\Date;
use App\Model\Backend\BoxInfo;
use App\Model\Inventory\ProductList;
use DB;
use Session;
use Input;
class DataValidationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

            return  view('backend.settings.data-validation.index');
    }
    /*
    * Not matched between two tables
    */
    public function getInvalidClientInfo() {

       /* $invalidClients = DB::table('client_info')    
            ->join('instruments', 'instruments.newconid', '=', 'client_info.newconid')
            ->where('client_info.newconid','=','instruments.clientid')
            ->get(); */

        $invalidClients = DB::select( DB::raw("SELECT ci.*,rg.region_name,rd.road FROM `tbl_client_info` ci 
            inner join tbl_region rg on rg.region_id = ci.region
            inner join tbl_road rd on rd.id = ci.road
             where ci.newconid not in (select newconid from tbl_instruments)"));

       
        return view( 'backend.settings.data-validation.invalid-clients', [
            'invalidClients' => $invalidClients,
        ]);
        
    }

    public function getInvalidClientInfoEdit($id) {

        $client_info = DB::table('client_info')
            ->leftJoin('region', 'client_info.region', '=', 'region.region_id')
            ->leftJoin('newlinerequest', 'client_info.newline_reqid', '=', 'newlinerequest.newline_reqid')
            ->leftJoin('road', 'client_info.road', '=', 'road.id')
            ->leftjoin('instruments', 'client_info.newconid', '=', 'instruments.newconid')
            ->leftjoin('real_ip_setup', 'instruments.real_ip', '=', 'real_ip_setup.id')
            ->leftjoin('packages_info', 'instruments.packageid', '=', 'packages_info.package_id')
            ->select('client_info.*', 'instruments.mikortik_id','real_ip_setup.real_ip as realIP','newlinerequest.nlr_paid', 'instruments.*', 'packages_info.*')
            ->where( 'client_info.newconid', $id )          
            ->where( 'client_info.linestatus',1)          
            ->first(); 

        $clientBoxInfo = DB::table('client_info')
            ->leftJoin('instruments', 'client_info.newconid', '=', 'instruments.newconid')
            ->where( 'client_info.newconid', $id )
            ->first();

        $boxNumer = isset($clientBoxInfo->box_no) ? $clientBoxInfo->box_no : false; 
        $allBox   = BoxInfo::all();

        $boxInfo = DB::table('box_info')
            ->where('box_no',$clientBoxInfo->box_no)
            ->first();

        $packageTypeInfo = BillSetup::limit(1)->get();

        $monthYear = explode( ',', Date::monthYear());

        return view( 'backend.settings.data-validation.edit', [
            'info'            => $client_info,
            'id'              => $id,
            'packageTypeInfo' => $packageTypeInfo,
            'cureentmonth'    => $monthYear[0],
            'currenYear'      => $monthYear[1],
            'allBox'          => $allBox,
            'boxInfo'         => $boxInfo,
            'boxInfo'         => $boxInfo,
            'customerType'    => NewlineSetup::all(),
            'productList'     => ProductList::where('category_id',15)->get(),
            'cpackage'        => Package::where('status',1)->get(),
            'employee'        => Empoffice::employeeCatgoryList('4'),
        ]);
    }

    public function getInvalidClientInfoUpdate(Request $request) {

        $package_id     = \Input::get('packageid');
        $spdiscount     = (Input::get('special_discount'))?Input::get('special_discount'):0;
        $porttotal      = (Input::get('porttotal'))?Input::get('porttotal'):0;
        $advancepaid    = (Input::get('advancepaid'))?Input::get('advancepaid'):0;
        $newline_paid   = (Input::get('newline_paid'))?Input::get('newline_paid'):0;
        $cable_meter    = (Input::get('cable_meter'))?Input::get('cable_meter'):0;
        $box_no         = (Input::get('box_no'))?Input::get('box_no'):0;
        $connction_date = (Input::get('connction_date'))?Input::get('connction_date'):0;
        $due_bill       = (Input::get('due_bill'))?Input::get('due_bill'):0;
        $due_adjustment = (Input::get('due_adjustment'))?Input::get('due_adjustment'):0;

        $insturmentInfo                   = new Instruments();
        $insturmentInfo->newconid         = \Input::get('newconid');
        $insturmentInfo->packageid        = $package_id[0];
        $insturmentInfo->special_discount = $spdiscount;
        $insturmentInfo->newline_paid     = $newline_paid;
        $insturmentInfo->advancepaid      = $advancepaid;
        $insturmentInfo->due_bill         = $due_bill;
        $insturmentInfo->due_adjustement  = $due_adjustment;
        $insturmentInfo->connection_date  = $connction_date;
        $insturmentInfo->box_no           = $box_no;
        $insturmentInfo->switch_port_no   = $porttotal;
        $insturmentInfo->cable_meter      = $cable_meter;

       if($insturmentInfo->save()) {

            Session::flash("message", "Client information has updated successfully!");

            return redirect('data/validations');

       } else {
            return back();
       }
       

    }
    /**
     * [getInvalidClientInfoDelete description]
     * @return [type] [description]
     */
    public function getInvalidClientInfoDelete() {

        $newconid = \Input::get('id');
        Newlines::where('newconid',$newconid)->delete();
    }

}
