<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Backend\Settings;
use App\Model\Backend\realIpSetup;
use App\Model\Backend\Package;
use Session;

class realIpSetupController extends Controller
{
	public function index() {

		$allRealIP =  realIpSetup::leftjoin('packages_info','real_ip_setup.package_id','=','packages_info.package_id')
			->select('packages_info.packagename','real_ip_setup.*')
			->get();
		
		return view( 'backend.settings.real-ip.real-ip-setup',[
			'chargeInfo' => $allRealIP,
			'packagInfo' => Package::all()
		] );

	}

	public function store(Request $request) {

		#validation section
        $this->validate($request, [
            'realip'        => 'required',
            'relipstep'     => 'required',
            'endrealip'     => 'required',
        ]);

		$realIpSetup = \Input::get('realip');
		$realIPStep  = \Input::get('relipstep');
		$endrealip   = \Input::get('endrealip');
		
		if(!empty($realIPStep) && !empty($endrealip)) {

			$startRealIP      = explode('.', $realIpSetup );
			$endRealIPLast    = explode('.', $endrealip );
			$countStartRealIP = count($startRealIP) - 1;
			$countEndRealIP   = count($endRealIPLast) - 1;

			if($countStartRealIP == $countEndRealIP) {

				$i   = $startRealIP[$countEndRealIP];
				$n   = $endRealIPLast[$countStartRealIP];

				$freeRealIp = '';

				for( $j=0; $j<= $countEndRealIP-1; $j++) {

					$freeRealIp .= $startRealIP[$j] . '.';
				}

				for($i; $i<=$n; $i = $i + $realIPStep) {

					$allReaIP = $freeRealIp . $i;
					
					$mulRealIP = New realIpSetup();
					$mulRealIP->package_id  = \Input::get('package');
					$mulRealIP->real_ip     = $allReaIP;

					# Save Real IP
					if($mulRealIP->save()) {

						Session::put( 'valid', 1 );
		        		Session::flash('message','Your Real IP has created successfully');

					} else {
						Session::put( 'invalid', 2 );
		        		Session::flash('message','We are nable to create new real ip information. Please try again.');
					}

					
				}	


			} else {

				Session::put( 'invalid', 2 );
		        Session::flash('message','Your multiple data condition is not valid.');

		        return back(); 
			}

		} else {

			$realIPCreate = New realIpSetup();
			$realIPCreate->package_id  =\Input::get('package');
			$realIPCreate->real_ip     =\Input::get('realip');
			$realIPCreate->save();

			Session::put( 'valid', 1 );
		    Session::flash('message','Your Real IP has created successfully');
		}  


        return back(); 
	}	

	public function edit($id) {

		return view( 'backend.settings.real-ip.real-ip-setup-edit', [
			'realIpInfo' => realIpSetup::where('id',$id)->first(),
			'packagInfo' => Package::where('status',1)->get()
		] );
	} 

	public function update($id) {

		$realIpSetup = realIpSetup::where( 'id', $id )->first();
		$realIpSetup->package_id  = \Input::get('package');
		$realIpSetup->real_ip     = \Input::get('realip');

		if( $realIpSetup->save() ) {

			Session::put( 'valid', 1 );
            Session::flash('message','Your Real IP has updated successfully');

		}  else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unble to update real ip information. Please try again!.');

        }
        
        return redirect('real/ip/setup');

	}

	/**
	 * [destroy description]
	 */
	public function destroy(Request $req) {

		realIpSetup::where('real_ip', $req->id)->delete();
		return response()->json();

	}
}