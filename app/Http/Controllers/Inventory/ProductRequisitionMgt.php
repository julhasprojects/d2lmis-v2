<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Inventory\Supplier; 
use App\Model\Inventory\Store; 
use App\Model\Inventory\CurrentStock; 
use App\Model\Inventory\ManufacturerList; 
use App\Model\Inventory\ProductCategory; 
use App\Model\Inventory\ProductList; 
use App\Model\Inventory\ProductUnits; 
use App\Model\Inventory\StoreTransaction; 
use App\Model\Inventory\StoreDetails; 
use App\Model\Inventory\CustomerInfo; 
use App\Model\Inventory\Sales; 
use App\Model\Inventory\Price; 
use App\Model\Hrm\Employees; 
use App\Model\Backend\BoxInfo; 
use Input;
use Auth;
use DB;
use Session;

class ProductRequisitionMgt extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Al unique values
        $boxList = DB::table('box_info')
            ->select('box_no','pop_name')
            ->groupBy('pop_name')
            ->get();

        return view('modules.inventory.requisition.index',[
            'supplierInfo'      => Supplier::all(),
            'categoryList'      => ProductCategory::all(),
            'productList'       => ProductList::all(),
            'unitList'          => ProductUnits::all(),
            'empployeeList'     => Employees::all(),
            'boxList'           => $boxList

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       // Transaction type 2 is for product requisitions or distributions

        $transaction_type = 2; 
        $this->validate($request, [
            'submitted_by'   => 'required',
            'product_id'     => 'required',
        ]);

        // Store Trasaction table master data update
        $storeTransaction = new StoreTransaction();
        $storeTransaction->transaction_id   = \Input::get('requisition_no');
        $storeTransaction->transaction_type = $transaction_type;
        $storeTransaction->received_date    = \Input::get('requisition_date');
        $storeTransaction->challan_no       = \Input::get('box_no'); // box number
        $storeTransaction->challan_date     = \Input::get('installation_date'); // Installation date
        $storeTransaction->received_by      = \Input::get('submitted_by');// submitted by
        $storeTransaction->entry_by         = Auth::user()->id;
        $storeTransaction->receive_remarks  = \Input::get('remarks'); // Requisition remarks
        $storeTransaction->save();

        $transaction_id         = \Input::get('requisition_no');
        $product_id             = \Input::get('product_id');
        $client_id              = \Input::get('client_id');
        $quantity               = \Input::get('quantity'); // Requested quantity
        $comments               = \Input::get('comments'); // Reason

        foreach ($product_id as $key => $value) {

            if(empty($product_id[$key]))  {
                
                Session::put( 'invalid', 2 );
                Session::flash( 'message', 'Product name can not be empty. Plesae enter Product name..' );
                return back();
            }
            if(empty($client_id[$key])) {

                $clientsid = "0";

            } else {

                $clientsid = $client_id[$key];

            }
            
            $storeDetails = new StoreDetails();
            $storeDetails->transaction_id       = $transaction_id;
            $storeDetails->product_id           = $product_id[$key];
            $storeDetails->client_id            = $clientsid;
            $storeDetails->quantity             = $quantity[$key];
            $storeDetails->comments             = $comments[$key];
            $storeDetails->save();       
            
        }

        Session::flash( 'message', 'Your requisition has successfully submitted.' );
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function saleInsert(Request $request)
    {
        
    
       // Transaction type 2 is for product requisitions or distributions

        $transaction_type =2; 
      
        // Store Trasaction table master data update
        $storeTransaction = new StoreTransaction();
        $storeTransaction->transaction_id   = \Input::get('requisition_no');
        $storeTransaction->transaction_type = $transaction_type;
        $storeTransaction->received_date    = \Input::get('requisition_date');
        $storeTransaction->challan_no       = \Input::get('box_no'); // box number
        $storeTransaction->challan_date     = \Input::get('installation_date'); // Installation date
        $storeTransaction->received_by      = \Input::get('submitted_by');// submitted by
        $storeTransaction->entry_by         = Auth::user()->id;
        $storeTransaction->receive_remarks  = \Input::get('remarks'); // Requisition remarks
        $storeTransaction->save();

        $this->validate($request, [
            'customer_name' => 'required',
        ]);
        $newCustomerCreate = new CustomerInfo();   
        $newCustomerCreate->sale_id       = \Input::get('sales_ID');
        $newCustomerCreate->name          = \Input::get('customer_name');
        $newCustomerCreate->phone_number  = \Input::get('customer_phone_number');
        $newCustomerCreate->email         = \Input::get('customer_email');
        $newCustomerCreate->address       = \Input::get('customer_address');
        $newCustomerCreate->entry_by      = Session::get('user_id');
        $newCustomerCreate->save();

        $transaction_id    = \Input::get('requisition_no');
        $product_id        = \Input::get('product_id');
        $product_price_id  = \Input::get('product_price_id');
        $client_id         = \Input::get('client_id');
        $quantity          = \Input::get('quantity'); // Requested quantity
        $comments          = \Input::get('comments'); // Reason
        $unit_price        = \Input::get('unit_price'); // Reason
        $sale_price        = \Input::get('sale_price'); // Reason
        $quantity          = \Input::get('quantity'); // Reason
        $discount          = !empty(\Input::get('discount'))?Input::get('discount'):0; // Reason
        $adj               = !empty(\Input::get('adj'))?Input::get('adj'):0; // Reason
        $vat               = !empty(\Input::get('vat'))?\Input::get('vat'):0; // Reason
        $totalamount       = !empty(\Input::get('totalamount'))?Input::get('totalamount'):0; // Reason
        $payamount         = !empty(\Input::get('payamount'))?Input::get('chageamount'):0; // Reason
        $charge_amount     = !empty(\Input::get('chageamount'))?Input::get('chageamount'):0; // Reason
        $dueamount         = !empty(\Input::get('dueamount'))?Input::get('dueamount'):0; // Reason
        $salesDate         = \Input::get('selectDatetime'); // Reason

        

        foreach ($product_price_id as $key => $value) {

            if(empty($product_id[$key]))  {
                
                Session::put( 'invalid', 2 );
                Session::flash( 'message', 'Product name can not be empty. Plesae enter Product name..' );
                return back();
            }
            if(empty($client_id[$key])) {

                $clientsid = "0";

            } else {

                $clientsid = $client_id[$key];

            }
          
            $productPrice = Price::where('id',$product_price_id[$key])->first();
            $productPrice->qty   =$productPrice->qty-$quantity[$key];
            $productPrice->save();
            // Current stock 
            $currentStock = CurrentStock::where('product_id',$product_id[$key])->first();
            $currentStock->stock_qty = $currentStock->stock_qty-$quantity[$key];
            $currentStock->save();
            // store details
            $storeDetails = new StoreDetails();
            $storeDetails->transaction_id   = $transaction_id;
            $storeDetails->product_id       = $product_id[$key];
            $storeDetails->client_id        = $clientsid;
            $storeDetails->quantity         = $quantity[$key];
            $storeDetails->comments         = $comments[$key];
            $storeDetails->approval_status  = 1;
            $storeDetails->save();  

            // sales details
            $saleDetails = new Sales();
            $saleDetails->product_id    = $product_id[$key];
            $saleDetails->qty           = $quantity[$key];
            $saleDetails->discount      = $discount[$key];
            $saleDetails->adjustment    = $adj[$key];
            $saleDetails->vat           = $vat[$key];
            $saleDetails->total_amount  = $totalamount[$key];
            $saleDetails->pay_amount    = $payamount[$key];
            $saleDetails->charge_amount = $charge_amount[$key];
            $saleDetails->due_amount    = $dueamount[$key];
            $saleDetails->sale_date     = $salesDate;
            $saleDetails->pay_date      = Date("Y-m-d");
            $saleDetails->entry_by      = Session::get('user_id'); 
            $saleDetails->save();
                 
        }
       
        Session::flash( 'message', 'Your requisition has successfully submitted.' );
        return back();
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        
        $requisitionAll=DB::table('inv_storetransaction')
            ->join('inv_storesdetails','inv_storesdetails.transaction_id','=','inv_storetransaction.transaction_id')
            ->join('inv_productlist','inv_productlist.product_id','=','inv_storesdetails.product_id') 
            ->leftJoin('employees','inv_storetransaction.received_by','=','employees.employeeID') 
            ->select('inv_storetransaction.*','inv_storesdetails.*','inv_storesdetails.id as storeid','employees.fullName','inv_productlist.*','inv_storesdetails.id as transactionID','inv_storesdetails.approval_status')
            ->where('inv_storetransaction.transaction_type',2)
            ->where('inv_storesdetails.approval_status','=',0)
            ->get();  

        return view('modules.inventory.requisition.reauisition-list',[
            'requisitionAll' => $requisitionAll,
            'productList'    => ProductList::all(),
        ]);
    }
    /**
    * All Approved Requisition
    **/
    public function approvedRequisition()
    {
        
        $requisitionAll=DB::table('inv_storetransaction')
            ->join('inv_storesdetails','inv_storesdetails.transaction_id','=','inv_storetransaction.transaction_id')
            ->join('inv_productlist','inv_productlist.product_id','=','inv_storesdetails.product_id') 
            ->leftJoin('client_info','client_info.newconid','=','inv_storesdetails.client_id') 
            ->leftJoin('employees','inv_storetransaction.received_by','=','employees.employeeID') 
            ->select('inv_storetransaction.*','inv_storesdetails.*','inv_storesdetails.id as storeid','employees.fullName','inv_productlist.*','client_info.*','inv_storesdetails.id as transactionID','inv_storesdetails.approval_status')
            ->where('inv_storetransaction.transaction_type',2)
            ->where('inv_storesdetails.approval_status','=',1)
            ->get();  

        return view('modules.inventory.requisition.approved-requisitions',[
            'requisitionAll' => $requisitionAll,
             'productList'    => ProductList::all(),
        ]);
    }

    /**
    * All Disapproved Requisition
    **/
    public function disApprovedRequisition()
    {
        
        $requisitionAll=DB::table('inv_storetransaction')
            ->join('inv_storesdetails','inv_storesdetails.transaction_id','=','inv_storetransaction.transaction_id')
            ->join('inv_productlist','inv_productlist.product_id','=','inv_storesdetails.product_id') 
            ->leftJoin('client_info','client_info.newconid','=','inv_storesdetails.client_id') 
            ->leftJoin('employees','inv_storetransaction.received_by','=','employees.employeeID') 
            ->select('inv_storetransaction.*','inv_storesdetails.*','employees.fullName','inv_productlist.*','client_info.*','inv_storesdetails.id as transactionID','inv_storesdetails.approval_status')
            ->where('inv_storetransaction.transaction_type',2)
            ->where('inv_storesdetails.approval_status','=',2)
            ->get();  

        return view('modules.inventory.requisition.disapproved-requisitions',[
            'requisitionAll' => $requisitionAll
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requisitionAll=DB::table('inv_storesdetails') 
            ->select('inv_storesdetails.*')
            ->where('inv_storesdetails.transaction_id',$id)
            ->get();

        foreach ($requisitionAll as $key => $resuisition) {

               $stock = DB::table('inv_currentstock')
                    ->select('inv_currentstock.stock_qty')
                    ->where('product_id', '=', $resuisition->product_id )
                    ->first();

                if (empty($stock)) {

                    Session::flash( 'message', 'Sorry! There is no stock.' );
                    return back();

                } else {

                    $updateQty = ($stock->stock_qty)-($resuisition->quantity);

                    // Update current stock table
                    DB::table('inv_currentstock')
                        ->where('product_id', $resuisition->product_id)
                        ->update(['stock_qty' => $updateQty]);

                    // Update inv_storesdetails table    
                    DB::table('inv_storesdetails')
                        ->where('product_id', $resuisition->product_id)
                        ->where('transaction_id', $id)
                        ->update(['approval_status' => 1,'approved_by' => Auth::user()->id]);    

                    Session::flash( 'message', 'Successfully approved your product requisition.' );
                    return back();    
                } 


            }    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function salesReports() {

        $saleInfoList = Sales::leftjoin('inv_productlist','inv_sales.product_id','=','inv_productlist.product_id')
            ->leftjoin('inv_productcategory','inv_productlist.category_id','=','inv_productcategory.category_id')
            ->leftjoin('inv_customerinfo','inv_sales.id','=','inv_customerinfo.sale_id')
            ->select('inv_productlist.product_name','inv_customerinfo.name','inv_customerinfo.phone_number','inv_sales.*','inv_productcategory.category_name')
            ->get();

        return view('modules.inventory.sale.index',[
            'saleInfoList' =>$saleInfoList
        ]);
    }

    public function dueSalesReports() {
         $saleInfoList = Sales::leftjoin('inv_productlist','inv_sales.product_id','=','inv_productlist.product_id')
            ->leftjoin('inv_productcategory','inv_productlist.category_id','=','inv_productcategory.category_id')
            ->leftjoin('inv_customerinfo','inv_sales.id','=','inv_customerinfo.sale_id')
            ->select('inv_productlist.product_name','inv_customerinfo.name','inv_customerinfo.phone_number','inv_sales.*','inv_productcategory.category_name')
            ->where('inv_sales.due_amount','>',0)
            ->get();

        return view('modules.inventory.sale.due',[
            'saleInfoList' =>$saleInfoList
        ]);
    }

    // Sales Product Search
    public function salesProductSearch() {

        $productID = Input::get('SearchProduct');
        //Product Info
        $productInfo = Price::leftjoin('inv_productlist','inv_price.product_id','=','inv_productlist.product_id')
            ->select('inv_price.*','inv_productlist.product_name','inv_productlist.product_id')
            ->where('inv_price.product_id',$productID)
            ->where('inv_price.qty','>',0)
            ->get();
       
        return view('modules.inventory.requisition.product-search-result',[
                'productInfo' => $productInfo,
            ]);
    }
}
