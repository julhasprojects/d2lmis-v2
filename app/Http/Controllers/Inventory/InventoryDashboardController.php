<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Inventory\Supplier; 
use App\Model\Inventory\Store; 
use App\Model\Inventory\CurrentStock; 
use App\Model\Inventory\ManufacturerList; 
use App\Model\Inventory\ProductCategory; 
use App\Model\Inventory\ProductList; 
use App\Model\Inventory\ProductUnits; 
use App\Model\Inventory\StoreTransaction; 
use App\Model\Inventory\StoreDetails; 
use App\Model\Inventory\Price; 
use App\Model\Hrm\Employees; 
use App\Model\Inventory\Sales; 
use App\Model\Backend\BoxInfo; 
use App\Model\Backend\Company; 
use Input;
use Auth;
use DB;
use Session;

class InventoryDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

         // Al unique values
        $boxList = DB::table('box_info')
            ->select('box_no','pop_name')
            ->groupBy('pop_name')
            ->get();

        $salesInfo = Sales::orderBy('id','desc')->first();
        $salesID = isset($salesInfo->id)?$salesInfo->id+1:1;
      
        return view('modules.inventory.master.index',[
            'supplierInfo'      => Supplier::all(),
            'categoryList'      => ProductCategory::all(),
            'productList'       => ProductList::all(),
            'unitList'          => ProductUnits::all(),
            'empployeeList'     => Employees::all(),
            'companyinfo'       => Company::orderBy('id','desc')->first(),
            'boxList'           => $boxList,
            'salesID'           => $salesID,
            'entryBy'           => Session::get('user_id')
        ]);
        
    }

    
    /**
     * Product Avaible
     */

    public function productAvaiblibleSearch() {

        $productID = \Input::get('productID');

        $productQty = CurrentStock::where('product_id',$productID)->first();
        $productReq = Sales::select('qty')
            ->where('product_id',$productID)
           
            ->sum('qty');
        
        $sale_price = StoreDetails::where('product_id',$productID)->first();
        $totalAvailebleProduct = ($productQty->stock_qty);
        
        if(!empty($productQty)) {

            return $totalAvailebleProduct.','.$sale_price->sale_price; 

        } else {

            return 0;

        }
        

    }

    // Barcode Generator
    public function barcodeGenerator() {

        $productList = Price::leftjoin('inv_productlist','inv_price.product_id','=','inv_productlist.product_id')
                
                ->orderBy('inv_productlist.id','desc')
                ->get();

        return view('modules.inventory.barcode-generator.index',[
            'productList'=>$productList,
        ]);
        
    }
}
