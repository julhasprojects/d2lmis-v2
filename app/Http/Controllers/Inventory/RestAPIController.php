<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Inventory\Supplier; 
use App\Model\Inventory\Store; 
use App\Model\Inventory\CurrentStock; 
use App\Model\Inventory\ManufacturerList; 
use App\Model\Inventory\ProductCategory; 
use App\Model\Inventory\ProductList; 
use App\Model\Inventory\ProductUnits; 
use App\Model\Inventory\StoreTransaction; 
use App\Model\Inventory\StoreDetails; 
use App\Model\Inventory\GoodsReceivedInvoices; 
use App\Model\Inventory\Price; 
use App\Model\Hrm\Employees; 
use App\Model\Frontpanel\Empoffice; 
use Input;
use DB;
use Session;
class RestAPIController extends Controller
{
    public function getCurrentStock() {

        $currentStock = DB::table('inv_currentstock')
                ->join('inv_productlist','inv_productlist.product_id','=','inv_currentstock.product_id')
                ->join('inv_productcategory','inv_productcategory.category_id','=','inv_productlist.category_id')
                ->select('inv_currentstock.*','inv_currentstock.created_at as stockupdate_date','inv_productlist.*','inv_productcategory.*')
                ->get();       
        return $currentStock->toArray();
    }
    
}
