<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Inventory\Supplier; 
use Input;
use Redirect;
use Session;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        return view('modules.inventory.supplier.index',[
            'supplierInfo' => Supplier::all(),
        ]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
       
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        // Validation 
        $this->validate($request, [
            'supplier_name'   => 'required',
            'company_details' => 'required',
            'mobile'          => 'required',
            'email'           => 'required',
     
        ]);

        // supplier data 
        $suppList = Supplier::orderBy('id','desc')->first();

        if(!empty($suppList)) {

            $supplierId = $suppList->supplier_id + 1;

        } else {

             $supplierId = 1;

        }

        
        $supplier = new Supplier();
        $supplier->supplier_id      = $supplierId;
        $supplier->supplier_name    = \Input::get('supplier_name');
        $supplier->company_details  = \Input::get('company_details');
        $supplier->address          = \Input::get('address');
        $supplier->mobile           = \Input::get('mobile');
        $supplier->email            = \Input::get('email');
        
        // save supplier data
        if($supplier->save()) {

            Session::flash( 'message', 'Supplier  has successfully added.' );
            return back();

        } else {

            Session::flash( 'message', 'We are unable to create Supplier. Please try again!..' );
            return back();

        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        return view('modules.inventory.supplier.view',[
            'supplierInfo' => Supplier::all(),
        ]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return view('modules.inventory.supplier.edit',[
            'supplierInfo' => Supplier::where('id',$id)->first(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $this->validate($request, [
            'supplier_name'   => 'required',
            'company_details' => 'required',
            'mobile'          => 'required',
            'email'           => 'email',
        ]);

        $supplier =Supplier::where('id',$id)->first();
        $supplier->supplier_name   = \Input::get('supplier_name');
        $supplier->company_details = \Input::get('company_details');
        $supplier->address         = \Input::get('address');
        $supplier->mobile          = \Input::get('mobile');
        $supplier->email           = \Input::get('email');

        // save supplier data
        if($supplier->save()) {

            Session::flash( 'message', 'Supplier  has successfully updated.' );

        } else {

            Session::flash( 'message', 'We are unable to updated Supplier. Please try again!..' );

        }
      
        return redirect('supplier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        Supplier::where('supplier_id',$id)->delete();
        Session::put( 'valid', 1 );
        Session::flash('message','Supplier  has deleted successfully');
        return back();

    }
}
