<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Inventory\Supplier; 
use App\Model\Inventory\Store; 
use App\Model\Inventory\CurrentStock; 
use App\Model\Inventory\ManufacturerList; 
use App\Model\Inventory\ProductCategory; 
use App\Model\Inventory\ProductList; 
use App\Model\Inventory\ProductUnits; 
use App\Model\Inventory\StoreTransaction; 
use App\Model\Inventory\StoreDetails; 
use App\Model\Inventory\CustomerInfo; 
use App\Model\Inventory\Sales; 
use App\Model\Inventory\Price; 
use App\Model\Hrm\Employees; 
use App\Model\Backend\BoxInfo; 
use Input;
use Auth;
use DB;
use Session;

class IndentManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Al unique values
        $boxList = DB::table('box_info')
            ->select('box_no','pop_name')
            ->groupBy('pop_name')
            ->get();

        return view('modules.inventory.indent.index',[
            'supplierInfo'      => Supplier::all(),
            'categoryList'      => ProductCategory::all(),
            'productList'       => ProductList::all(),
            'unitList'          => ProductUnits::all(),
            'empployeeList'     => Employees::all(),
            'boxList'           => $boxList

        ]);
    }
}
