<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Frontpanel\Empoffice;
use App\Model\Inventory\ProductCategory;
use App\Model\Inventory\ProductList;
use App\Model\Inventory\StoreTransaction;
use App\Model\Inventory\StoreDetails;
use Session;
use Auth;
use Input;

class StockReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        return view('modules.inventory.return-stock.index',[
            'allEmployeer' => Empoffice::allEmployer(),
            'categoryList' => ProductCategory::all(),
            'productList'  => ProductList::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        # validation section 
        $this->validate($request, [
            'transaction_id'  => 'required',
            'requestion_no'   => 'required',
            'return_date'     => 'required',
            'return_by'       => 'required',
        ]);
        # Store Details Submit
        $transaction_type = 8; 

        // Store Trasaction table master data update
        $storeTransaction = new StoreTransaction();
        $storeTransaction->transaction_id   = \Input::get('transaction_id');
        $storeTransaction->transaction_type = $transaction_type;
        $storeTransaction->received_date    = \Input::get('return_date');
        $storeTransaction->challan_no       = \Input::get('requestion_no'); // box number
        // $storeTransaction->challan_date     = \Input::get('installation_date'); // Installation date
        $storeTransaction->received_by      = \Input::get('return_by');// submitted by
        $storeTransaction->entry_by         = Auth::user()->id;
        $storeTransaction->receive_remarks  = \Input::get('reason_return'); // Requisition remarks
        $storeTransaction->save();


        // Store Transaction details setup
        
        $transaction_id         = \Input::get('requestion_no');
        $product_id             = \Input::get('product_id');
        $total_received         = \Input::get('total_received');
        $referance              = \Input::get('referance');
        $quantity               = \Input::get('quantity'); // Requested quantity
        $comments               = \Input::get('comments'); // Reason

        foreach ($product_id as $key => $value) {
            
            if(empty($product_id[$key]))  {
                
                Session::put( 'invalid', 2 );
                Session::flash( 'message', 'Product name can not be empty. Plesae enter Product name..' );
                return back();
            }

            $storeDetails = new StoreDetails();
            $storeDetails->transaction_id  = \Input::get('transaction_id');
            $storeDetails->product_id      = $product_id[$key];
            $storeDetails->client_id       = $referance[$key];
            $storeDetails->quantity        = $quantity[$key];
            $storeDetails->comments        = $comments[$key];
            $storeDetails->approval_status = 0;
            $storeDetails->save();       
            
        }
        Session::flash( 'message', 'Return stock has created successfully' );
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $stores       = StoreDetails::all();

        $transactions = StoreTransaction::leftjoin('inv_storesdetails','inv_storetransaction.transaction_id','=','inv_storesdetails.transaction_id')
            ->leftjoin('inv_productlist','inv_storesdetails.product_id','=','inv_productlist.product_id')
            ->leftjoin('employees','inv_storetransaction.received_by' ,'=','employees.employeeID')
            ->select('inv_productlist.product_name','inv_storesdetails.id as storeid','receive_remarks','employees.fullName','inv_productlist.product_id','inv_storesdetails.unit_price','inv_storesdetails.quantity','inv_storesdetails.total_price','inv_storesdetails.warranty_period','inv_storesdetails.comments','inv_storesdetails.created_at','inv_storesdetails.updated_at','inv_storesdetails.id as transactionID','inv_storesdetails.approval_status')
            ->where('inv_storetransaction.transaction_type',8)
            ->where('inv_storesdetails.approval_status',0)
            ->orderBy('inv_storetransaction.created_at','desc')
            ->get();

        foreach($transactions as $key=>$str) {
            $store = StoreDetails::find($str->id);
        }

        return view('modules.inventory.return-stock.stock-return',[
            'transactions' => $transactions,
            'productList'  => ProductList::all(),
        ]);
    }

    public function stockApproved() {
       $stores       = StoreDetails::all();

        $transactions = StoreTransaction::leftjoin('inv_storesdetails','inv_storetransaction.transaction_id','=','inv_storesdetails.transaction_id')
            ->leftjoin('inv_productlist','inv_storesdetails.product_id','=','inv_productlist.product_id')
            ->leftjoin('employees','inv_storetransaction.received_by' ,'=','employees.employeeID')
            ->select('inv_productlist.product_name','inv_storesdetails.id as storeid','receive_remarks','employees.fullName','inv_productlist.product_id','inv_storesdetails.unit_price','inv_storesdetails.quantity','inv_storesdetails.total_price','inv_storesdetails.warranty_period','inv_storesdetails.comments','inv_storesdetails.created_at','inv_storesdetails.updated_at','inv_storesdetails.id as transactionID','inv_storesdetails.approval_status')
            ->where('inv_storetransaction.transaction_type',8)
            ->where('inv_storesdetails.approval_status',1)
            ->orderBy('inv_storetransaction.created_at','desc')
            ->get();

        foreach($transactions as $key=>$str) {
            $store = StoreDetails::find($str->id);
        }

        return view('modules.inventory.return-stock.approved-or-disapproved-list',[
            'transactions' => $transactions,
            'productList'  => ProductList::all(),
        ]); 
    }

    public function stockDisapproved() {
        $stores       = StoreDetails::all();
       
        $transactions = StoreTransaction::leftjoin('inv_storesdetails','inv_storetransaction.transaction_id','=','inv_storesdetails.transaction_id')
            ->leftjoin('inv_productlist','inv_storesdetails.product_id','=','inv_productlist.product_id')
            ->leftjoin('employees','inv_storetransaction.received_by' ,'=','employees.employeeID')
            ->select('inv_productlist.product_name','inv_storesdetails.id as storeid','receive_remarks','employees.fullName','inv_productlist.product_id','inv_storesdetails.unit_price','inv_storesdetails.quantity','inv_storesdetails.total_price','inv_storesdetails.warranty_period','inv_storesdetails.comments','inv_storesdetails.created_at','inv_storesdetails.updated_at','inv_storesdetails.id as transactionID','inv_storesdetails.approval_status')
            ->where('inv_storetransaction.transaction_type',8)
            ->where('inv_storesdetails.approval_status',2)
            ->orderBy('inv_storetransaction.created_at','desc')
            ->get();

        foreach($transactions as $key=>$str) {
            $store = StoreDetails::find($str->id);
        }

        return view('modules.inventory.return-stock.approved-or-disapproved-list',[
            'transactions' => $transactions,
            'productList'  => ProductList::all(),
        ]); 
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
