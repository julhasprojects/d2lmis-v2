<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Inventory\Supplier; 
use App\Model\Inventory\Store; 
use App\Model\Inventory\CurrentStock; 
use App\Model\Inventory\ManufacturerList; 
use App\Model\Inventory\ProductCategory; 
use App\Model\Inventory\ProductList; 
use App\Model\Inventory\ProductUnits; 
use App\Model\Inventory\StoreTransaction; 
use App\Model\Inventory\StoreDetails; 
use App\Model\Inventory\GoodsReceivedInvoices; 
use App\Model\Inventory\Price; 
use App\Model\Hrm\Employees; 
use App\Model\Frontpanel\Empoffice; 
use Input;
use DB;
use Session;
class ProductReceiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $productLast = StoreTransaction::orderBy('id','desc')->first();

        
        if(!empty($productLast)) {

            $productID = $productLast->id+1; 

        } else {
            $productID = 1;   
        }
        return view('modules.inventory.receive.index',[
                
            'supplierInfo' => Supplier::all(),
            'categoryList' => ProductCategory::all(),
            'productList'  => ProductList::all(),
            'unitList'     => ProductUnits::all(),
            'employeeInfo' => Empoffice::allEmployer(),
            'employeeInfo' => Empoffice::allEmployer(),
            'productID'    => $productID,

        ]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
        $this->validate($request, [
            'received_date' => 'required',
            'invoice_no'    => 'required',
            'challan_no'    => 'required',
            'challan_date'  => 'required',
            'received_by'   => 'required',
        ]);
         
            
        // Transaction type
        $transaction_type = 1; 

        // Store Trasaction table master data update
        $storeTransaction = new StoreTransaction();
        $storeTransaction->transaction_id   = \Input::get('transaction_id');
        $storeTransaction->transaction_type = $transaction_type;
        $storeTransaction->invoice_no       = \Input::get('invoice_no');
        $storeTransaction->challan_no       = \Input::get('challan_no');
        $storeTransaction->challan_date     = \Input::get('challan_date');
        $storeTransaction->supplier_id      = \Input::get('supplier_id');
        $storeTransaction->received_by      = \Input::get('received_by');
        $storeTransaction->received_date    = \Input::get('received_date');
        //$storeTransaction->entry_by         = Auth::user()->id;
        $storeTransaction->entry_by         = 203;
        $storeTransaction->receive_remarks  = \Input::get('receive_remarks');
        $storeTransaction->save();

        // Store Transaction details setup
        
        $transaction_id         = \Input::get('transaction_id');
        $product_id             = \Input::get('product_id');
        $unit_price             = \Input::get('unit_price');
        $quantity               = \Input::get('quantity');
        $total_price            = \Input::get('total_price');
        $warranty_period        = \Input::get('warranty_period');
        $comments               = \Input::get('comments');

        foreach ($product_id as $key => $value) {

            $storeInfo = Price::where('unit_price',$unit_price[$key])
                ->where('product_id',$product_id[$key])
                ->first();
          
            if(empty($storeInfo)) {

                $priceInfo = new Price();
                $priceInfo->product_id      = $product_id[$key];
                $priceInfo->transaction_id  = $transaction_id;
                $priceInfo->qty             = $quantity[$key];
                $priceInfo->unit_price      = $unit_price[$key];
                $priceInfo->save();

            } 
           

            // Store
            $storeDetails = new StoreDetails();
            $storeDetails->transaction_id       = $transaction_id;
            $storeDetails->product_id           = $product_id[$key];
            $storeDetails->unit_price           = $unit_price[$key];
            $storeDetails->quantity             = $quantity[$key];
            $storeDetails->total_price          = $total_price[$key];
            $storeDetails->warranty_period      = $warranty_period[$key];
            $storeDetails->comments             = $comments[$key];         
            $storeDetails->save();

        }

        Session::flash( 'message', 'Stock record added successfully' );
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show() {

        $stores       = StoreDetails::all();
        $transactions = Price::leftjoin('inv_storesdetails','inv_price.product_id','=','inv_storesdetails.product_id')
            ->leftjoin('inv_storetransaction','inv_price.transaction_id','=','inv_storetransaction.transaction_id')
            ->leftjoin('inv_productlist','inv_price.product_id','=','inv_productlist.product_id')
            ->leftjoin('employees','inv_storetransaction.received_by','employees.employeeID')
            ->select('inv_price.id as invPID','inv_price.unit_price as unitPrice','inv_price.sale_price as salePrice','inv_storesdetails.id as storeid','inv_productlist.product_name','employees.fullName','inv_productlist.product_id','inv_storesdetails.unit_price','inv_storesdetails.quantity','inv_storesdetails.total_price','inv_storesdetails.warranty_period','inv_storesdetails.comments','inv_storesdetails.created_at','inv_storesdetails.updated_at','inv_storesdetails.id as transactionID','inv_storetransaction.transaction_type','inv_storesdetails.approval_status')
            ->where('inv_price.approval_status',0)
            ->orderBy('inv_storetransaction.created_at','desc')
            ->where('inv_storesdetails.approval_status','=',0)
            ->get();

        foreach($transactions as $key=>$str) {
            $store = StoreDetails::find($str->id);
        }

        return view('modules.inventory.receive.approve-good',[
            'transactions' => $transactions,
            'categoryList' => ProductCategory::all(),
            'productList'  => ProductList::all(),
        ]);
    }

    /*
    *All Approved Products
    */
    public function approvedProducts() {

        $stores       = StoreDetails::all();
        $transactions = StoreTransaction::leftjoin('inv_storesdetails','inv_storetransaction.transaction_id','=','inv_storesdetails.transaction_id')
            ->leftjoin('inv_productlist','inv_storesdetails.product_id','=','inv_productlist.product_id')
            ->leftjoin('employees','inv_storetransaction.received_by','employees.employeeID')
            ->select('inv_storesdetails.id as storeid','inv_productlist.product_name','inv_storesdetails.approved_by','employees.fullName','inv_productlist.product_id','inv_storesdetails.unit_price','inv_storesdetails.quantity','inv_storesdetails.total_price','inv_storesdetails.warranty_period','inv_storesdetails.comments','inv_storesdetails.created_at','inv_storesdetails.updated_at','inv_storesdetails.id as transactionID','inv_storesdetails.approval_status')
            ->orderBy('inv_storetransaction.created_at','desc')
            ->where('inv_storesdetails.approval_status','=',1)
            ->get();

        foreach($transactions as $key=>$str) {
            $store = StoreDetails::find($str->id);
        }

        return view('modules.inventory.receive.all-approved-products',[
            'transactions' => $transactions,
            'categoryList' => ProductCategory::all(),
            'productList'  => ProductList::all(),
            'allEmployer'  => Employees::all(),
        ]);
    }

    /**
    * All disapproved products
    */
    
    public function disApprovedProducts() {

        $stores       = StoreDetails::all();
        $transactions = StoreTransaction::leftjoin('inv_storesdetails','inv_storetransaction.transaction_id','=','inv_storesdetails.transaction_id')
            ->leftjoin('inv_productlist','inv_storesdetails.product_id','=','inv_productlist.product_id')
            ->leftjoin('employees','inv_storetransaction.received_by','employees.employeeID')
            ->select('inv_productlist.product_name','inv_storesdetails.approved_by','employees.fullName','inv_productlist.product_id','inv_storesdetails.unit_price','inv_storesdetails.quantity','inv_storesdetails.total_price','inv_storesdetails.warranty_period','inv_storesdetails.comments','inv_storesdetails.created_at','inv_storesdetails.updated_at','inv_storesdetails.id as transactionID','inv_storesdetails.approval_status')
            ->orderBy('inv_storetransaction.created_at','desc')
            ->where('inv_storesdetails.approval_status','=',2)
            ->get();

        foreach($transactions as $key=>$str) {
            $store = StoreDetails::find($str->id);
        }

        return view('modules.inventory.receive.all-disapproved-product',[
            'transactions' => $transactions,
            'categoryList' => ProductCategory::all(),
            'productList'  => ProductList::all(),
            'allEmployer'  => Employees::all(),
        ]);
    }


    /**
    Current Stock display
    */

    public function currentStock() {

        $currentStock = DB::table('inv_currentstock')
                ->join('inv_productlist','inv_productlist.product_id','=','inv_currentstock.product_id')
                ->join('inv_productcategory','inv_productcategory.category_id','=','inv_productlist.category_id')
                ->select('inv_currentstock.*','inv_currentstock.created_at as stockupdate_date','inv_productlist.*','inv_productcategory.*')
                ->get();       
        return view('modules.inventory.stock.current-stock',[
            'currentStock' => $currentStock
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function storeDetailsProducts($id) {

        $quantity   = \Input::get('quantity');
        $unitprice  = \Input::get('unit_price');
        $saleprice  = \Input::get('sale_price');
        $totalprice = \Input::get('total_price');
        $product_id = \Input::get('product_id');

        //Price Info
        $priceInfo = Price::where('id',$id)->first();
        $priceInfo->unit_price   = $unitprice;
        $priceInfo->sale_price   = $saleprice;
        $priceInfo->qty          = $quantity;
        $priceInfo->save();

        // Sotre Details
        $storeDetailsdata  = StoreDetails::where('product_id',$priceInfo->product_id)->first();

        $storeDetailsdata->product_id  = $product_id;
        $storeDetailsdata->unit_price  = $unitprice;
        $storeDetailsdata->quantity    = $quantity;
        $storeDetailsdata->total_price = $totalprice;

        $storeDetailsdata->save();

   
        return back();

    }

    public function productAppoveDisapprove() {

        $pPriceID        = \Input::get('pid');
        $productid        = \Input::get('id');
        $productqty       = \Input::get('qty');
        $status           = \Input::get('status');
        $transactionid    = \Input::get('transactionid');
        $disapprovestatus = \Input::get('disapprovestatus');
        $currentStock     = new CurrentStock();
        $storeDetails     = StoreDetails::find($transactionid);

        $priceInfo = Price::where('id',$pPriceID)->first();
        $priceInfo->approval_status =1;
        $priceInfo->save();
        # Existing stock
        $stock = DB::table('inv_currentstock')
            ->select('inv_currentstock.stock_qty')
            ->where('product_id', '=', $productid )
            ->first();

        $storeDetails->approved_by = Session::get('emp_id');
        

        if($status == 1) {// Status 1 for products approval

            

            # If no stock in current stock table then new record will be inserting   
            if (empty($stock)) {

                $currentStock->product_id  = $productid;
                $currentStock->stock_qty   = $productqty;
                $currentStock->save();

            } else {

                if($disapprovestatus == 3) {

                    $totalStock =$stock->stock_qty - $productqty ;

                } else {

                    $totalStock = $productqty+ $stock->stock_qty;  

                }
                

                DB::table('inv_currentstock')
                    ->where('product_id',$productid)
                    ->update(['stock_qty' => $totalStock]);
            }    

                
        } else if($status == 2) { // Status 2 for requisition approval

            if($disapprovestatus == 0) {

                $storeDetails->approval_status = $status;
                $storeDetails->save();
                return 402;

            }
            if ($stock->stock_qty > 0) {

                $totalStock = $stock->stock_qty-$productqty;
                DB::table('inv_currentstock')
                    ->where('product_id',$productid)
                    ->update(['stock_qty' => $totalStock]);

            } else {
                echo 401;
            } 

        } else {

        }

        $storeDetails->approval_status = $status;
        $storeDetails->save();
       
    }
// Local Purchase 
    public function localPurchase() {

        $productLast = StoreTransaction::orderBy('id','desc')->first();

        
        if(!empty($productLast)) {

            $productID = $productLast->id+1; 

        } else {
            $productID = 1;   
        }
        return view('modules.inventory.receive.local-purchase',[
                
            'supplierInfo' => Supplier::all(),
            'categoryList' => ProductCategory::all(),
            'productList'  => ProductList::all(),
            'unitList'     => ProductUnits::all(),
            'employeeInfo' => Empoffice::allEmployer(),
            'employeeInfo' => Empoffice::allEmployer(),
            'productID'    => $productID,

        ]);
        
    }   

    
}
