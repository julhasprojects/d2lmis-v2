<?php

namespace App\Http\Controllers\Inventory;
use Illuminate\Http\Request;
use App\Model\Frontpanel\Clientcomplain; 
use App\Model\Frontpanel\ButtonPermission; 
use App\Model\Frontpanel\Employee; 
use App\Model\Frontpanel\Region; 
use App\Model\Frontpanel\Road; 
use App\Model\Frontpanel\Instruments;
use App\Model\Backend\Package;  
use App\Model\Backend\BoxInfo; 
use App\Model\Backend\Date; 
use App\Model\Backend\SwitchUpdateInfo; 
use App\Model\Backend\InterfaceSetup; 
use App\Model\Backend\BaseSetup; 
use App\Model\Backend\HsSetting;
use App\Model\Backend\Branch;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Frontpanel\packages; 
use App\Model\Frontpanel\Techinfo; 
use App\Model\Frontpanel\Empoffice; 
use App\Model\Frontpanel\Linedisconnect;
use App\Model\Backend\BillGenerateInfo;
use App\Model\Frontpanel\Newlines;
use Excel;
use PHPExcel_Style_Fill;
use Input; 
use App\Http\Model\Frontpanel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail ;
use Session;
use DB;


class InventoryManagementController  extends Controller {
    /**
     * Display admin Dashboard.
     *
     * @return Response
     */

    public function index() {

        $monthYear = explode(',', Date::monthYear() );
          
        return view('backend.inventory.master-box-report', [
                'box'          => BoxInfo::orderBy('id','desc')->get(),  
                'employee'     => Empoffice::allEmployer(),  
                'currentMonth' => $monthYear[0],  
                'currentYear'  => $monthYear[1],  
                'region'       => Region::all(), 
                'currentUserID'       => Session::get('user_id'), 
            ]);

        
    }

   
    public function create() {

        $content  =  view('backend.inventory.create',[
            'employee'   => Empoffice::allEmployer(),  
            'popName'    => InterfaceSetup::all(),  
            'branchInfo' => Branch::all(),  
            'bracnhID'   => Session::get('branchId'),  
        ]);

        return view( 'backend.index')->with( 'content_area', $content);
    }
 
    public function show() {

        $monthYear = explode(',', Date::monthYear() );
             
        $content =  view('backend.inventory.master-box-report', [
                'box'          => BoxInfo::orderBy('id','desc')->get(),  
                'employee'     => Empoffice::allEmployer(),  
                'currentMonth' => $monthYear[0],  
                'currentYear'  => $monthYear[1],  
                'region'       => Region::all(),  
            ]);

        return view( 'backend.index', [
                'content_area' => $content
            ]);
    }

    /**
     * [updateBoxNumber description]
     * @return [type] [description]
     */
    public function updateBoxNumber() {

        $box  = DB::select( DB::raw("SELECT b.id,b.box_no,a.countPort,b.box_address,b.switch_name,b.switch_port,b.mc_name,b.mc_number,b.totalport,b.pop_address,b.deleted_status
                FROM 
                (
                SELECT ins.box_no,COUNT(ins.switch_port_no) AS countPort  FROM  tbl_instruments ins GROUP BY ins.box_no   ORDER BY ins.id DESC
                ) AS a
                RIGHT JOIN (
                SELECT bi.id,bi.box_no,bi.box_address,bi.deleted_status,bi.switch_name,bi.switch_port,bi.mc_name,bi.mc_number,bi.totalport,bi.pop_address 
                    FROM tbl_box_info bi GROUP BY bi.box_no ORDER BY bi.id DESC
                ) AS b ON b.box_no=a.box_no"));
        
        $content =  view('backend.inventory.update-box-report',    
            [
                'box'      => $box,  
                'employee' => Empoffice::allEmployer(),  
                'currentUserID'       => Session::get('user_id'), 
            ]);
  
        return view( 'backend.index')->with( 'content_area', $content);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {

        # check existing box
        $exist = BoxInfo::where('box_no', Input::get('boxNumber'))->count();

        if ($exist) {

           return 301;
        }

        # save existing box
        $boxInfo = new BoxInfo();
       
        $boxInfo->box_no       = Input::get('boxNumber');
        $boxInfo->box_address  = Input::get('boxaddress');
        $boxInfo->switch_name  = Input::get('switchname');
        $boxInfo->switch_port  = Input::get( 'port' );
        $boxInfo->totalport    = Input::get('totalport');
        $boxInfo->free_port    = Input::get('freeport');
        $boxInfo->mc_name      = Input::get('mcname');
        $boxInfo->mc_number    = Input::get('mcnumber');
        $boxInfo->sfp_name     = Input::get('sfpname');
        $boxInfo->sfp_number   = Input::get('sfpnumber');
        $boxInfo->pop_name     = Input::get('popname');
        $boxInfo->pop_address  = Input::get('popaddress');
        $boxInfo->tg_name      = Input::get('tgname');
        $boxInfo->install_date = Input::get('installdate');
        $boxInfo->install_by   = Input::get('installby');
        $boxInfo->fiber_root   = Input::get('fiberroot');
        $boxInfo->branch_id    = Input::get('branch');
        $boxInfo->pop          = ( Input::get('pop') == 'on' ) ? 1 : 0;

       if($boxInfo->save()) {
        
            /*Session::flash("message", "New box information has created successfully!");
            return Redirect::back();*/
        
        }  else {
            // Session::flash("message", "Sorry! We can't create new box information. Please try again!");
        } 
       
    }
 
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    
    public function edit($id) {

        $info     = BoxInfo::find($id);
           
        $totalBoxUser = Instruments::leftjoin('box_info', 'instruments.box_no', '=', 'box_info.box_no')
            ->leftjoin('client_info', 'instruments.newconid', '=', 'client_info.newconid')
            ->select(DB::raw('count(switch_port_no) as totalUserPort'),'client_info.linestatus')
            ->where('box_info.id',$id)
            ->where('client_info.linestatus',1)
            ->first();
  
        return $content =  view('backend.inventory.edit',    
            [
                'info'         => $info,  
                'totalBoxUser' => $totalBoxUser,  
                'employee'     => Empoffice::allEmployer(),  
                'popName'      => InterfaceSetup::all(),
                'branchInfo'   => Branch::all(),  
                'bracnhID'     => Session::get('branchId'),    
            ]);

       // return view( 'backend.index')->with( 'content_area', $content);

    }
 
 
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        
        $box  = DB::table('client_info') 
            ->rightjoin('instruments', 'client_info.newconid', '=', 'instruments.newconid')
            ->rightjoin( 'box_info', 'instruments.box_no', '=', 'box_info.box_no')
            ->select(DB::raw('count(switch_port_no) as countPort'),'instruments.switch_port_no','box_info.box_no','box_info.switch_name','box_info.box_address','box_info.free_port','box_info.totalport','box_info.switch_port','box_info.mc_name','box_info.mc_number','box_info.mc_number','box_info.pop_address','box_info.id')
            ->orWhere('client_info.linestatus',1)
            ->where('box_info.id',$id)
            ->orderBy('box_info.id','desc')
            ->first();
            
        $totalFreeport=\Input::get('freeport');
        
        // Tetrasoft data quelatiy jonno problem sloved
        // if($totalFreeport >= 0) {
            if($box->box_no!=Input::get('boxNumber')) {
                DB::table( 'instruments' )
                    ->where( 'box_no',$box->box_no)
                    ->update([
                        'box_no'    => Input::get('boxNumber'),
                   ]);
              
            }
            $boxInfo = BoxInfo::find($id);

            if(!empty($boxInfo->box_no)) {

                if($boxInfo->box_no!=Input::get('boxNumber') || $boxInfo->switch_name!=Input::get('switchname') ) {
                    $updatedBy = ($boxInfo->updated_by == 0 ) ? $boxInfo->updated_by : $boxInfo->install_by;
                    $switchInfo = new SwitchUpdateInfo();
                    $switchInfo->box_no           = $boxInfo->box_no;
                    $switchInfo->previouse_switch = $boxInfo->switch_name;
                    $switchInfo->updated_by       = $updatedBy;
                    $switchInfo->save();
                }
            }

            $boxInfo->box_no       = Input::get('boxNumber');
            $boxInfo->box_address  = Input::get('boxaddress');
            $boxInfo->switch_name  = Input::get('switchname');
            $boxInfo->switch_port  = Input::get('port');
            $boxInfo->totalport    = Input::get('totalport');
            $boxInfo->mc_name      = Input::get('mcname');
            $boxInfo->mc_number    = Input::get('mcnumber');
            $boxInfo->sfp_name     = Input::get('sfpname');
            $boxInfo->sfp_number   = Input::get('sfpnumber');
            $boxInfo->pop_name     = Input::get('popname');
            $boxInfo->pop_address  = Input::get('popaddress');
            $boxInfo->tg_name      = Input::get('tgname');
            $boxInfo->install_date = Input::get('installdate');
            $boxInfo->updated_by   = Input::get('updateby');
            $boxInfo->branch_id    = Input::get('branch');

            if($boxInfo->save()) {

                Session::flash("message", "Box information has updated successfully!");
                return Redirect('box-info');

            }  else {
                Session::flash("message", "Sorry! We can't update box information. Try again!");
                return Redirect::back();
            }
        /*} else {
             
            Session::flash("message", "Sorry! We can't update box information.Total port less then the use port. Please try again!");
            return Redirect::back();
        }*/
    }
 
 
    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  * @return Response
    //  */
    public function destroy($id) {

        $softdelete = BoxInfo::where('id',$id)->first();
        $softdelete->deleted_at      = Date('Y-m-d h:m:s');
        $softdelete->deleted_status  = 1;
        $softdelete->deleted_by      = Session::get('user_id');
        if($softdelete->save()) {

            Session::flash("message", "Box information has deleted successfully!");
            
        } else {
            Session::flash("message", "Sorry! We are unable to delete box information. Please try again! ");
        }
        
        return Redirect::back();
    }

    public function boxInfoRebackId($id) {
        $softdelete = BoxInfo::where('id',$id)->first();
       
        $softdelete->deleted_status  = 0;
       
        if($softdelete->save()) {

            Session::flash("message", "Box information has reback successfully!");
            
        } else {
            Session::flash("message", "Sorry! We are unable to reback box information. Please try again! ");
        }
        
        return Redirect::back();
    }

    /**ee
     * [inventoryNumberCount description]
     * @return [type] [description]
     */
    public function inventoryNumberCount() {

        $sum         = 0;
        $switchName  = Input::get('switchname');
        $explode     = explode(",",$switchName);
        $countswitch = count($explode);

        foreach ($explode as $count) {
             $info = filter_var($count, FILTER_SANITIZE_NUMBER_INT);
             $sum+=$info;
        }

        return $sum= $sum-((2*$countswitch)-1);

    }

    public function inventoryUpdatedNumberCount() {
         
        $sum = 0;
        $switchName=Input::get('switchname');
        
        $explode=explode(",",$switchName);
        $countswitch=count($explode);
        foreach ($explode as $count) {
             $info = filter_var($count, FILTER_SANITIZE_NUMBER_INT);
             $sum+=$info;
        }
        
        $boxNumber=Input::get('boxNumber');

        $info =Instruments::leftjoin('box_info', 'instruments.box_no', '=', 'box_info.box_no')
            ->leftjoin('client_info', 'instruments.newconid', '=', 'client_info.newconid')
            ->select(DB::raw('count(switch_port_no) as totalUserPort'),'client_info.linestatus')
            ->where('instruments.box_no',$boxNumber)
            ->where('client_info.linestatus',1)
            ->first();
            
        $boxNumberUpdate = isset($info->totalUserPort) ? $info->totalUserPort : false;

        if($boxNumberUpdate) {
            return $sum= ($sum-((2*$countswitch)-1))-$info->totalUserPort;
        } else {
            return $sum= $sum-((2*$countswitch)-1);
        }
    }

    public function frontBoxSearch() {
 
        $box          =\Input::get("box_no");
        $box_address  =\Input::get('box_address');
       

        if(!empty($box)) {

            $boxInfo = DB::table('box_info')
            ->where('box_no',$box)
            ->first();

            $totalPortUser = DB::table('instruments')
                ->where('box_no',$box)
                ->count('switch_port_no');

        } elseif(!empty($box_address)) {

            $boxInfo = DB::table('box_info')
            ->where('box_address',$box_address)
            ->first();

            $boxCreate = isset($boxInfo->box_info) ? $boxInfo->box_info : false; 

            $totalPortUser = DB::table('instruments')
                ->where('box_no',$boxCreate)
                ->count('switch_port_no');
        }
        
        if (!empty($boxInfo)) {
            $intefaceInfo    = InterfaceSetup::get();
            $baseInfo        = BaseSetup::get();
            $HsSettingInfo   = HsSetting::get();
            return $content =  view( 'backend.linemgt.instrument-box',[
                    'employee'       => Empoffice::allEmployer() ,
                    'totalPortUser'  => $totalPortUser,
                    'boxInfo'        => $boxInfo,
                    'intefaceInfo'   => $intefaceInfo,
                    'baseInfo'       => $baseInfo,
                    'HsSettingInfo'  => $HsSettingInfo,
                ]);
        } else {
            return "<span class='box-not-found'>Box not found</span>";
        }
    }

    public function inventoryBoxSearch() {

        $boxexplode  = \Input::get('box_no');
        $box_address = \Input::get('box_address');
        $explode     = explode(" ",$boxexplode);
        $box         = implode("+", $explode);
        if(!empty($box)) {

            $boxInfo = DB::table('box_info')
                ->where('box_no',$box)
                ->first();
            if(!empty($boxInfo->box_address)) {
                return $boxInfo->box_address;
            }

        } elseif(!empty($box_address)) {

            $boxInfo = DB::table('box_info')
                ->where('box_address',$box_address)
                ->first(); 

            $allBox = BoxInfo::all();

            if(!empty($boxInfo->box_no)) {

                return $content =  view('backend.inventory.all-box-search', [
                    'allBox'    => $allBox,  
                    'boxNumber' => $boxInfo->box_no,  
                ]);

            } else {

                return $content =  view('backend.inventory.all-box-search', [
                    'allBox'    => $allBox,  
                    'boxNumber' => $boxInfo->box_no,  
                ]);

            }
                   
        }
    }

    public function inventoryMasterCableReport() {

        $monthYear = explode(',', Date::monthYear() );
        $region    = DB::table('region') ->get();
            $content =  view('backend.inventory.master-cable-report', [
                'region'       => $region,  
                'currentMonth' => $monthYear[0],  
                'currentYear'  => $monthYear[1],  
            ]);

        return view( 'backend.index', ['content_area' => $content ]);
    }

    public function inventoryAreaWishCableReport() {

         $areasearch   = \Input::get('areasearch');
         
         $cable = DB::table('client_info') 
            ->leftjoin('region', 'client_info.region', '=', 'region.region_id')
            ->join('road', 'client_info.road', '=', 'road.id')
            ->leftjoin('instruments', 'client_info.newconid', '=', 'instruments.newconid')
            ->leftjoin('box_info', 'instruments.box_no', '=', 'box_info.box_no')
            ->select( 'box_info.*','client_info.newconid','road.road','instruments.cable_meter','region.region_name','client_info.username','client_info.house')
            ->where('client_info.region',$areasearch)
            ->where('client_info.linestatus',1)
            ->where('instruments.cable_meter','>',0)
            ->groupBy('client_info.newconid')
            ->orderBy('box_info.box_no','desc')
            ->get();

        return $content =  view('backend.inventory.ajax-cable-report', [
                'cable'  => $cable,  
            ]);

       
    }
    public function inventoryRoadWishCableReport () {

         $road    = \Input::get('road');
         $region  = \Input::get('region');
             
         $cable = DB::table('client_info') 
            ->leftjoin('region', 'client_info.region', '=', 'region.region_id')
            ->join('road', 'client_info.road', '=', 'road.id')
            ->leftjoin('instruments', 'client_info.newconid', '=', 'instruments.newconid')
            ->leftjoin('box_info', 'instruments.box_no', '=', 'box_info.box_no')
            ->select( 'box_info.*','client_info.newconid','road.road','cable_meter','region.region_name','client_info.username','client_info.house')
            ->where('client_info.region',$region)
            ->where('client_info.road',$road)
            ->where('client_info.linestatus',1)
            ->where('instruments.cable_meter','>',0)
            ->groupBy('client_info.newconid')
            ->orderBy('box_info.box_no','desc')
            ->get();

        return $content =  view('backend.inventory.ajax-cable-report', [
            'cable'  => $cable,  
        ]); 
    }

    public function boxWishCableReport() {

        $monthYear = explode(',', Date::monthYear() );
        $boxCable = DB::table('client_info') 
            ->leftjoin('region', 'client_info.region', '=', 'region.region_id')
            ->join('road', 'client_info.road', '=', 'road.id')
            ->leftjoin('instruments', 'client_info.newconid', '=', 'instruments.newconid')
            ->leftjoin('box_info', 'instruments.box_no', '=', 'box_info.box_no')
            ->select( DB::raw('sum(cable_meter) as sum'),'instruments.box_no','box_info.box_address')
            ->where('client_info.linestatus',1)
            ->where('instruments.cable_meter','>',0)
            ->where('instruments.box_no','>',0)
            ->groupBy('instruments.box_no')
            ->orderBy('instruments.box_no')
            ->get();

        $content =  view('backend.inventory.box-wise-cable-report', [
            'boxCable'      => $boxCable,  
            'currentMonth'  => $monthYear[0],  
            'currentYear'   => $monthYear[1],  
        ]);

        return view( 'backend.index', ['content_area' => $content ]);
    }

    public function boxWiseComplain() {

        $monthYear = explode(',', Date::monthYear() );
        $today     = date('Y-m-d');

        $year        = date("Y");
        $currentTime = strtotime($year);
        $final_con   = strtotime('-1 day', $currentTime);
        $yesterDay  = date('Y-m-d', $final_con); 

        $endMonth = $month = date('m',strtotime($monthYear[0] .'-'.$monthYear[1] )); 
        $month    = $month - 1 ;
        $year     = $monthYear[1];

        if ($month == 0) {
            $currentMonth = 12;
            $currentYear  = $year - 1;
        } else {
            $currentMonth = $month;
            $currentYear  = $year;
        }

        $startDay   =  $currentYear . '-0'. $currentMonth . '-'. $monthYear[4];     
        $endDay     =  $monthYear[1] . '-'. $endMonth . '-'. $monthYear[5];

        $region = DB::table('region') ->get();
        $boxComplain = DB::table('client_complain') 
            ->leftjoin('client_info', 'client_complain.newconid', '=', 'client_info.newconid')
            ->leftjoin('region', 'client_info.region', '=', 'region.region_id')
            ->leftjoin('road', 'client_info.road', '=', 'road.id')
            ->leftjoin('instruments', 'client_info.newconid', '=', 'instruments.newconid')
            ->leftjoin('box_info', 'instruments.box_no', '=', 'box_info.box_no')
            ->select( DB::raw('count(clientid) as totalComlain'),'instruments.box_no','box_info.box_address')
            ->where('client_info.linestatus',1)
            ->whereBetween('client_complain.created_at',[$startDay,$endDay])
            ->groupBy('instruments.box_no')
            ->orderBy('totalComlain','dsc')
            ->limit(50)
            ->get();
  
        $content =  view('backend.inventory.box-wise-complain', [
                'boxComplain'  =>  $boxComplain,  
                'currentMonth' =>  $monthYear[0],  
                'currentYear'  =>  $monthYear[1],  
            ]);

        return view( 'backend.index', ['content_area' => $content ]);
    }

    public function boxWiseComplainMonthlySearch() {

        $complainmonth  = \Input::get('complainmonth');
        $complainyear   = \Input::get('complainyear');
        $monthStarday=date("$complainyear-$complainmonth-01");

        $d             = cal_days_in_month(CAL_GREGORIAN,$complainmonth,$complainyear);
        $monthEndday   = date("$complainyear-$complainmonth-$d");
        $monthStarday .=" 00:00:00";
        $monthEndday .=" 23:59:59";

        $boxComplain = DB::table('client_info') 
            ->leftjoin('client_complain', 'client_info.newconid', '=', 'client_complain.newconid')
            ->leftjoin('region', 'client_info.region', '=', 'region.region_id')
            ->leftjoin('road', 'client_info.road', '=', 'road.id')
            ->leftjoin('instruments', 'client_info.newconid', '=', 'instruments.newconid')
            ->leftjoin('box_info', 'instruments.box_no', '=', 'box_info.box_no')
            ->select( DB::raw('count(clientid) as totalComlain'),'instruments.box_no','box_info.box_address')
            ->where('client_info.linestatus',1)
            ->whereBetween('client_complain.complaindate',[$monthStarday,$monthEndday])
            ->groupBy('instruments.box_no')
            ->orderBy('totalComlain','dsc')
            ->get();
        
        return  $content =  view('backend.inventory.box-wise-complain-ajax-search', [
            'boxComplain'  =>  $boxComplain,  
        ]);
    }

    public function boxWiseComplainDateSearch() {

        $complaindateform  = \Input::get('complaindateform');
        $complaindateform .=" 00:00:00";
        $complaindateto    = \Input::get('complaindateto');
        $complaindateto   .=' 23:59:59';
        
        $boxComplain = DB::table('client_complain') 
            ->leftjoin('client_info', 'client_complain.newconid', '=', 'client_info.newconid')
            ->leftjoin('region', 'client_info.region', '=', 'region.region_id')
            ->leftjoin('road', 'client_info.road', '=', 'road.id')
            ->leftjoin('instruments', 'client_info.newconid', '=', 'instruments.newconid')
            ->leftjoin('box_info', 'instruments.box_no', '=', 'box_info.box_no')
            ->select( DB::raw('count(clientid) as totalComlain'),'instruments.box_no','box_info.box_address')
            ->where('client_info.linestatus',1)
            ->whereBetween('client_complain.created_at',[$complaindateform, $complaindateto])
            ->groupBy('instruments.box_no')
            ->orderBy('totalComlain','dsc')
            ->paginate(50);

        return $content =  view('backend.inventory.box-wise-complain-ajax-search', [
            'boxComplain'  =>  $boxComplain,  
        ]);
    }

    public function inventoryFreeportReport() {

        $region = DB::table('region') ->get();

        $boxFreeport = DB::table('client_info') 
            ->leftjoin('instruments', 'client_info.newconid', '=', 'instruments.newconid')
            ->leftjoin( 'box_info', 'instruments.box_no', '=', 'box_info.box_no')
            ->select(DB::raw('count(switch_port_no) as countPort'),'instruments.switch_port_no','box_info.box_no','box_info.switch_name','box_info.box_address','box_info.free_port','box_info.totalport')
            ->where('client_info.linestatus',1)
            ->groupBy('box_info.box_no')
            ->orderBy('box_info.install_date','desc')
            ->get();

        
        $content =  view('backend.inventory.box-free-port-report', [
                'boxFreeport' => $boxFreeport,  
                'region'      => $region, 
            ]);

        return view( 'backend.index', ['content_area' => $content ]); 

    }

    public function inventoryFreePortSearch() {

        $road    = \Input::get('freeport_road_search');
        $region  = \Input::get('freeport_region_search');

        if(!empty($region) && !empty($road)) {

            $boxFreeport = DB::table('client_info') 
                ->leftjoin('instruments', 'client_info.newconid', '=', 'instruments.newconid')
                ->leftjoin( 'box_info', 'instruments.box_no', '=', 'box_info.box_no')
                ->select(DB::raw('count(switch_port_no) as countPort'),'instruments.switch_port_no','box_info.box_no','box_info.switch_name','box_info.box_address','box_info.free_port','box_info.totalport')
                ->where('client_info.linestatus',1)
                ->where('client_info.road', $road)
                ->where('client_info.region', $region)
                ->groupBy('box_info.box_no')
                ->orderBy('box_info.install_date','desc')
                ->get();

        } else {

            $boxFreeport = DB::table('client_info') 
                ->leftjoin('instruments', 'client_info.newconid', '=', 'instruments.newconid')
                ->leftjoin( 'box_info', 'instruments.box_no', '=', 'box_info.box_no')
                ->select(DB::raw('count(switch_port_no) as countPort'),'instruments.switch_port_no','box_info.box_no','box_info.switch_name','box_info.box_address','box_info.free_port','box_info.totalport')
                ->where('client_info.linestatus',1)
                ->where('client_info.region', $region)
                ->groupBy('box_info.box_no')
                ->orderBy('box_info.install_date','desc')
                ->get();   

        }
        
        return $content =  view('backend.inventory.ajax-free-port-search', [
                'boxFreeport'  => $boxFreeport,  
            ]);
    }

    /**
     * [inventoryMonthlyBoxSearch description]
     * @return [type] [description]
     */
    public function inventoryMonthlyBoxSearch() {
        $boxmonth     = \Input::get('boxmonth');
        $boxyear      = \Input::get('boxyear');
        $monthStarday = date("$boxyear-$boxmonth-01");
        $d            = cal_days_in_month(CAL_GREGORIAN,$boxmonth,$boxyear);
        $monthEndday  = date("$boxyear-$boxmonth-$d");

        $box = BoxInfo::whereBetween('install_date',[$monthStarday,$monthEndday])->get();
        
        return $content =  view('backend.inventory.ajax-technichain-by-box-search', [
            'box'  =>$box,  
        ]);
    }

    // Location wise box information
    public function locationWiseBoxReports() {

        $box    = BoxInfo::get();
        $region = Region::all();

        //Region wise box search
        return $content =  view('backend.inventory.location-wise-box-search', [
            'box'    => $box,  
            'region' => $region,  
        ]);
    }

    /**
     * [inventoryTechnchainBoxSearch description]
     * @return [type] [description]
     */
    public function inventoryTechnchainBoxSearch() {

        $installby = \Input::get('installby');
        $updateby  = \Input::get('updateby');

        if(!empty($updateby)) {

            $box=BoxInfo::where('updated_by',$updateby)
                ->get();

        } else {

            $box=BoxInfo::where('install_by',$installby)
                ->get();
            
        }
        
        return $content =  view('backend.inventory.ajax-technichain-by-box-search', [
            'box'  =>$box,  
        ]); 
    }

    public function inventoryRegionRoadBoxSearch() {

        $region  = \Input::get('region');
        $road    = \Input::get('road');

        if(!empty($region) && !empty($road)) {

            $box = DB::table('client_info') 
                ->join('instruments', 'client_info.newconid', '=', 'instruments.newconid')
                ->leftjoin( 'box_info', 'instruments.box_no', '=', 'box_info.box_no')
                ->where( 'client_info.region',$region)
                ->where( 'client_info.road',$road)
                ->groupBY('instruments.box_no')
                ->where('client_info.linestatus',1)
                ->get();

        }   else {

            $box = DB::table('client_info') 
                ->join('instruments', 'client_info.newconid', '=', 'instruments.newconid')
                ->leftjoin( 'box_info', 'instruments.box_no', '=', 'box_info.box_no')
                ->where( 'client_info.region',$region)
                ->groupBY('instruments.box_no')
                ->where('client_info.linestatus',1)
                ->get();

        }
        return $content =  view('backend.inventory.ajax-technichain-by-box-search', [
            'box'  =>$box,  
        ]);
    }

    public function inventoryRegionRoadWiseBoxReport() {

        $region  = \Input::get('region');
        return $content =  view('backend.inventory.map-area-road-search',[
            'region' =>Region::all(),
        ]);   

    }

    public function inventoryBoxNumberCheck() {

        $boxNumber  = \Input::get('boxNumber');
        $box = BoxInfo::where('box_no','=',$boxNumber)
                ->limit(1)
                ->get();

        foreach($box as $boxInfo) {

          if(isset($boxInfo->box_no)==isset($boxNumber)) {
                echo "Box already exit";
            }   
        }
       
    }

    public function boxWishCableMonthly() {

        $cablemonth  = \Input::get('cablemonth');
        $cableyear   = \Input::get('cableyear');

        $monthStarday = date("$cableyear-$cablemonth-01");
        $d            = cal_days_in_month(CAL_GREGORIAN,$cablemonth,$cableyear);
        $monthEndday  = date("$cableyear-$cablemonth-$d");

        $monthStarday .=" 00:00:00";
        $monthEndday .=" 23:59:59";

        $monthYear = explode(',', Date::monthYear() );
        $cable = DB::table('client_info') 
            ->leftjoin('region', 'client_info.region', '=', 'region.region_id')
            ->join('road', 'client_info.road', '=', 'road.id')
            ->leftjoin('instruments', 'client_info.newconid', '=', 'instruments.newconid')
            ->leftjoin('box_info', 'instruments.box_no', '=', 'box_info.box_no')
            ->select( 'region.*','road.*','client_info.*','instruments.newconid','cable_meter','instruments.box_no','box_info.box_address')
            ->where('client_info.linestatus',1)
            ->where('instruments.cable_meter','>',0)
            ->where('instruments.box_no','>',0)
            ->whereBetween('instruments.created_at',[$monthStarday,$monthEndday])
            ->groupBy('instruments.box_no')
            ->orderBy('instruments.box_no')
            ->get();

        return $content =  view('backend.inventory.ajax-cable-report', [
                'cable'  => $cable,  
                
            ]);

        return view( 'backend.index', ['content_area' => $content ]); 
    }

    /**
     *  Master Box Reports
     */
    public function masterBoxManagement() {

        $monthYear = explode(',', Date::monthYear() );

        $content =  view( 'backend.inventory.all-box-reports', 
            [
                'popInfo'       => BoxInfo::groupBy('pop_address')->get(),
                'currentMonth'  => $monthYear[0],
                'currentYear'   => $monthYear[1],
            ]);
        return view( 'backend.index')->with( 'content_area', $content );

    }

    /**
     * [masterBoxManagementswitchSearch description]
     * @return [type] [description]
     */
    public function masterBoxManagementswitchSearch() {

        $typeSearch = \Input::get('switchPort');
        $totalSwitchNumber = 0;

        if($typeSearch == 1) {

           $box        = BoxInfo::get();

            foreach ($box as $key =>$value ) {
               
                $switchCount = count($totalSwitch = explode( ',',$value->switch_name ));

                $totalSwitchNumber += $switchCount;
            }

        } else if( $typeSearch == 2) {

            $singleBox  = BoxInfo::get();

            foreach ($singleBox  as  $value) {

                $switchCount = count($totalSwitch = explode( ',',$value->switch_name ));

                if($switchCount == 1) {
                    
                    $box[] = $value ;
                 
                    $totalSwitchNumber += $switchCount;

                }
            }
           
        }  else if( $typeSearch == 3) {

            $doubleBox        = BoxInfo::get();

            foreach ($doubleBox  as $value) {

                $switchCount = count($totalSwitch = explode( ',',$value->switch_name ));

                if($switchCount == 2) {
                    $box[] = $value ;
                    $totalSwitchNumber += $switchCount;

                }
            }
           
        }
     
        

        
        return view( 'backend.inventory.master-switch-search', 
            [
               
                'employeeList'      => Empoffice::allEmployer(),
                'popInfo'           => BoxInfo::all(),
                'box'               => $box,
                'totalBox'          => count($box),
                'totalSwitchNumber' => $totalSwitchNumber,
              
            ]);
       
    }

    /**
     * [masterBoxManagementPopAddressSearch description]
     * @return [type] [description]
     */
    public function masterBoxManagementPopAddressSearch () {

        $typeSearch = \Input::get('pop');
        $box        = BoxInfo::where('pop_address',$typeSearch)->get();
        $totalBox   = count($box);

        return view( 'backend.inventory.master-box-management-search', 
            [
               
                'employeeList'  => Empoffice::allEmployer(),
                'popInfo'       => BoxInfo::all(),
                'box'           => $box,
                'totalBox'      => $totalBox
            ]);
    }

    /**
     * [masterBoxManagementMcTypeSearchAddressSearch description]
     * @return [type] [description]
     */
    public function masterMcTypeSearch () {

        $typeSearch = \Input::get('mcType');

        if($typeSearch == 1) {

            $box = BoxInfo::where('mc_name','!=','')->get();  

        } else {

            $box = BoxInfo::where('mc_name')->get(); 

        }
       
        $totalBox   = count($box);
        
        return view( 'backend.inventory.master-box-management-search', 
            [
               
                'employeeList'  => Empoffice::allEmployer(),
                'popInfo'       => BoxInfo::all(),
                'box'           => $box,
                'totalBox'      => $totalBox
            ]);
    }
    /**
     * [masterBoxManagementMcTypeSearchAddressSearch description]
     * @return [type] [description]
     */
    public function masterSFPTypeSearch() {

        $sfType = \Input::get('sfType');

        if($sfType == 1) {

            $box = BoxInfo::where('sfp_name','!=','')->get();  

        } else {

            $box = BoxInfo::whereNull('sfp_name')->get(); 

        }
       
        $totalBox   = count($box);
        
        return view( 'backend.inventory.master-box-management-search', 
            [
               
                'employeeList'  => Empoffice::allEmployer(),
                'popInfo'       => BoxInfo::all(),
                'box'           => $box,
                'totalBox'      => $totalBox
            ]);
    }

    /**
     * [Master Port Type Search description]
     * @return [type] [description]
     */
    public function masterPortTypeSearch() {

        $month = \Input::get('month');
        $year  = \Input::get('year');
        $type  = \Input::get('type');


        if($type == 1) {

            $dateInfo = BillGenerateInfo::select('month','year','start_date','end_date')
                ->where('month',$month)
                ->where('year',$year)
                ->first();

            $boxInfo  = Newlines::leftjoin('instruments','client_info.newconid','=','instruments.newconid')
                ->leftjoin('box_info','instruments.box_no','=','box_info.box_no')
                ->where('client_info.linestatus',1)
                ->where('instruments.box_no','!=','')
                ->whereBetween('instruments.connection_date',[$dateInfo->start_date,$dateInfo->end_date])
                ->groupBy('instruments.newconid')
                ->get();

        } else {

            $boxInfo  = Linedisconnect::leftjoin('client_info','line_disconnect.clientid','=','client_info.newconid')
                ->leftjoin('instruments','client_info.newconid','=','instruments.newconid')
                ->leftjoin('box_info','instruments.box_no','=','box_info.box_no')
                ->where('dis_month',$month)
                ->where('year',$year)
                ->where('client_info.linestatus',2)
                ->where('instruments.box_no','!=','')
                ->groupBy('instruments.newconid')
                ->get();
        }
           

        $totalPort = count($boxInfo);

        return view( 'backend.inventory.monthly-newline-disconnect-reports',[
            'employeeList'  => Empoffice::allEmployer(),
            'popInfo'       => BoxInfo::all(),
            'box'           => $boxInfo,
            'totalPort'     => $totalPort,
            'type'          => $type
        ]);
    }
     /**
     * Display list of inventory box data
     * 
     */
    public function inventoryBoxClientDetails($id) {

        $box = DB::table('instruments')   
            ->leftjoin('client_info', 'instruments.newconid', '=', 'client_info.newconid')         
            ->leftjoin('region', 'client_info.region', '=', 'region.region_id')
            ->leftjoin('road', 'client_info.road', '=', 'road.id')
            ->leftjoin('box_info', 'instruments.box_no', '=', 'box_info.box_no')            
            ->select('client_info.*', 'instruments.*','box_info.*','box_info.id as boxid','region.region_name','road.road as road_name')
            ->where('client_info.linestatus', 1) 
            ->where('instruments.box_no',$id)
            ->orderBy('box_info.box_no')
            ->take(1500)          
            ->get();
    
        $boxNoList =  DB::select( DB::raw("select distinct box_no from tbl_instruments") );   

        $region     = DB::table('region')->select('region_id','region_name')->get();
        
        $content =  view( 'backend.linemgt.reports.inventory-box-details', 
            [
                'box'           => $box,
                'regionName'    => $region,
                'employeeList'  => Empoffice::allEmployer(),
                'boxNoList'     => $boxNoList
            ]);
        return view( 'backend.index')->with( 'content_area', $content );

    }

    /**
     * Display list of inventory box data
     * 
     */
    public function inventoryBoxDetails() {
        
        $box = DB::table('client_info') 
            ->join('region', 'client_info.region', '=', 'region.region_id')
            ->join('road', 'client_info.road', '=', 'road.id')
            ->join('instruments', 'client_info.newconid', '=', 'instruments.newconid')            
            ->join('box_info', 'instruments.box_no', '=', 'box_info.box_no')            
            ->select('client_info.*', 'instruments.*','box_info.id as boxid','box_info.*','region.region_name','road.road as road_name')
            ->where('client_info.linestatus', 1) 
            ->limit(50)          
            ->get();

        $boxNoList =  DB::select( DB::raw("select distinct box_no from tbl_instruments") );   

        $region     = DB::table('region')->select('region_id','region_name')->get();
        
        $content =  view( 'backend.linemgt.reports.inventory-box-details', 
            [
                'box'           => $box,
                'regionName'    => $region,
                'employeeList'  => Empoffice::allEmployer(),
                'boxNoList'     => $boxNoList
            ]);
        return view( 'backend.index')->with( 'content_area', $content );

    }
    /**
     * Display list of inventory box data
     * 
     */
    public function inventoryBoxDetailsRegionWise(Request $request) {

        $region   = $request->input( 'region' );
        $road     = $request->input( 'road' );
        
        $box = DB::table('client_info')           
            ->leftjoin('region', 'client_info.region', '=', 'region.region_id')
            ->leftjoin('road', 'client_info.road', '=', 'road.id')
            ->leftjoin('instruments', 'client_info.newconid', '=', 'instruments.newconid')            
            ->leftjoin('box_info', 'instruments.box_no', '=', 'box_info.box_no')            
            ->select('client_info.*', 'instruments.*','box_info.*','box_info.id as boxid','region.region_name','road.road as road_name')
            ->where('client_info.region', $region) 
            ->where('client_info.road', $road) 
            ->get();

        $toalBoxCount = DB::table('client_info')           
            ->leftjoin('instruments', 'client_info.newconid', '=', 'instruments.newconid')  
            ->select('instruments.box_no','client_info.region', 'client_info.road')
            ->groupBy('instruments.box_no') 
            ->where('client_info.region', $region) 
            ->where('client_info.road', $road) 
            ->where('instruments.box_no','!=', "")
            ->get();

        $totalBox=count($toalBoxCount);
        $totalUser=count($box);    
        

        $region     = DB::table('region')->select('region_id','region_name')->get();
        $boxNoList =  DB::select( DB::raw("select distinct box_no from tbl_instruments") ); 
        $content =  view( 'backend.linemgt.reports.inventory-box-details', 
            [
                'box'  => $box,
                'regionName'         => $region,
                'boxNoList'          => $boxNoList,
                'totalUser'          => $totalUser,
                'totalBox'           => $totalBox
            ]);

        return view( 'backend.index')->with( 'content_area', $content );

    }

    /**
     * Export Csv Controller
     */
    
    public function exportInventoryBoxWiseComplain() {

        $monthYear = explode(',', Date::monthYear() );
        $today     = date('Y-m-d');

        $year        = date("Y");
        $currentTime = strtotime($year);
        $final_con   = strtotime('-1 day', $currentTime);
        $yesterDay  = date('Y-m-d', $final_con); 

        $endMonth = $month = date('m',strtotime($monthYear[0] .'-'.$monthYear[1] )); 
        $month    = $month - 1 ;
        $year     = $monthYear[1];

        if ($month == 0) {
            $currentMonth = 12;
            $currentYear  = $year - 1;
        } else {
            $currentMonth = $month;
            $currentYear  = $year;
        }

        $startDay   =  $currentYear . '-0'. $currentMonth . '-'. $monthYear[4];     
        $endDay     =  $monthYear[1] . '-'. $endMonth . '-'. $monthYear[5];

        $region = DB::table('region') ->get();
        $boxComplain = DB::table('client_complain') 
            ->leftjoin('client_info', 'client_complain.newconid', '=', 'client_info.newconid')
            ->leftjoin('region', 'client_info.region', '=', 'region.region_id')
            ->leftjoin('road', 'client_info.road', '=', 'road.id')
            ->leftjoin('instruments', 'client_info.newconid', '=', 'instruments.newconid')
            ->leftjoin('box_info', 'instruments.box_no', '=', 'box_info.box_no')
            ->select( DB::raw('count(clientid) as totalComlain'),'instruments.box_no','box_info.box_address')
            ->where('client_info.linestatus',1)
            ->whereBetween('client_complain.created_at',[$startDay,$endDay])
            ->groupBy('instruments.box_no')
            ->orderBy('totalComlain','dsc')
            ->limit(50)
            ->get();

        $toCsv  = array();
        $sl     = 1;
        
        foreach($boxComplain as $bill) {
            
            $c = array();
            
            
            
            $c['SL']              = $sl++;
            $c['Box No']          = $bill->box_no;
            $c['Box Address']     = $bill->box_address;
            $c['Total Complain']  = $bill->totalComlain;
            $toCsv[]             = $c;
        }

        Excel::create('boxComplain-'.date('Y-m-d Hi'), function($excel) use($toCsv){
            $excel->sheet("boxComplain-".date('Y-m-d Hi'), function($sheet) use($toCsv) {
                $sheet->fromArray($toCsv,null, 'A1', false, true);

            });
        })->export('csv');
    }

    public function exportInventoryBoxWiseFreeport() {

        $monthYear   = explode(',', Date::monthYear() );
        $today       = date('Y-m-d');
        $year        = date("Y");
        $currentTime = strtotime($year);
        $final_con   = strtotime('-1 day', $currentTime);
        $yesterDay   = date('Y-m-d', $final_con); 

        $endMonth = $month = date('m',strtotime($monthYear[0] .'-'.$monthYear[1] )); 
        $month    = $month - 1 ;
        $year     = $monthYear[1];

        if ($month == 0) {

            $currentMonth = 12;
            $currentYear  = $year - 1;

        } else {

            $currentMonth = $month;
            $currentYear  = $year;

        }

        $startDay   =  $currentYear . '-0'. $currentMonth . '-'. $monthYear[4];     
        $endDay     =  $monthYear[1] . '-'. $endMonth . '-'. $monthYear[5];

        $region      = DB::table('region') ->get();
        $boxFreeport = DB::table('client_info') 
                ->leftjoin('instruments', 'client_info.newconid', '=', 'instruments.newconid')
                ->leftjoin( 'box_info', 'instruments.box_no', '=', 'box_info.box_no')
                ->select(DB::raw('count(switch_port_no) as countPort'),'instruments.switch_port_no','box_info.box_no','box_info.switch_name','box_info.box_address','box_info.free_port','box_info.totalport')
                ->where('client_info.linestatus',1)
                ->groupBy('box_info.box_no')
                ->orderBy('box_info.install_date','desc')
                ->get(); 

        $toCsv  = array();
        $sl     = 1;
        
        foreach($boxFreeport as $info) {
            
            $c = array();

            $c['SL']              = $sl++;
            $c['Box No']          = $info->box_no;
            $c['Switch Name']     = $info->switch_name;
            $c['Box Address']     = $info->box_address;
            $c['totalport']       = $info->totalport;
            $c['User Port']       = $info->countPort;
            $c['Free Port']       = $info->totalport-$info->countPort;
            $toCsv[]              = $c;
        }

        Excel::create('boxComplain-'.date('Y-m-d Hi'), function($excel) use($toCsv){
            $excel->sheet("boxComplain-".date('Y-m-d Hi'), function($sheet) use($toCsv) {
                $sheet->fromArray($toCsv,null, 'A1', false, true);

            });
        })->export('csv');
    }

    public function exportInventoryBoxWiseCable() {

        $monthYear = explode(',', Date::monthYear() );
        $boxCable = DB::table('client_info') 
            ->leftjoin('region', 'client_info.region', '=', 'region.region_id')
            ->join('road', 'client_info.road', '=', 'road.id')
            ->leftjoin('instruments', 'client_info.newconid', '=', 'instruments.newconid')
            ->leftjoin('box_info', 'instruments.box_no', '=', 'box_info.box_no')
            ->select( DB::raw('sum(cable_meter) as sum'),'instruments.box_no','box_info.box_address')
            ->where('client_info.linestatus',1)
            ->where('instruments.cable_meter','>',0)
            ->where('instruments.box_no','>',0)
            ->groupBy('instruments.box_no')
            ->orderBy('instruments.box_no')
            ->get();

        $toCsv  = array();
        $sl     = 1;
        
        foreach($boxCable as $info) {
            
            $c = array();
            
            
            
            $c['SL']              = $sl++;
            $c['Box No']          = $info->box_no;
            $c['Box Address']     = $info->box_address;
            $c['Cable Meter']     = $info->sum;
            $toCsv[]               = $c;
        }

        Excel::create('boxwishcable-'.date('Y-m-d Hi'), function($excel) use($toCsv){
            $excel->sheet("boxwishcable-".date('Y-m-d Hi'), function($sheet) use($toCsv) {
                $sheet->fromArray($toCsv,null, 'A1', false, true);

            });
        })->export('csv');
    }

    public function exportInventoryMasterBoxReport() {

        $boxReport = BoxInfo::orderby('id','desc')->get();

        $toCsv  = array();
        $sl     = 1;
        
        foreach($boxReport as $bill) {
            
            $c = array();
            
            $c['SL']              = $sl++;
            $c['Box No']          = $bill->box_no;
            $c['Box Address']     = $bill->box_address;
            $c['Box Address']     = $bill->switch_name;
            $c['Switch No']       = $bill->switch_no;
            $c['Mc Name']         = $bill->mc_name;
            $c['Mc Number']       = $bill->mc_number;
            $c['Switch Port']     = $bill->switch_port;
            $c['Free Port']       = $bill->free_port;
            $c['Sfp Name']        = $bill->sfp_name;
            $c['Sfp Number']      = $bill->sfp_Number;
            $c['pop name']        = $bill->pop_name;
            $c['Pop Address']     = $bill->pop_address;
            $c['Tg Name']         = $bill->tg_name;
            $c['Install Date']    = $bill->install_date;
            $c['Install By']      = $bill->install_by;
            $toCsv[]              = $c;
        }

        Excel::create('masterBox-'.date('Y-m-d Hi'), function($excel) use($toCsv){
            $excel->sheet("masterBox-".date('Y-m-d Hi'), function($sheet) use($toCsv) {
                $sheet->fromArray($toCsv,null, 'A1', false, true);

            });
        })->export('csv');
    }

    
}   
