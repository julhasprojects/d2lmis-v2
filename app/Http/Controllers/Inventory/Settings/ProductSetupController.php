<?php

namespace App\Http\Controllers\Inventory\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Inventory\ProductList; 
use App\Model\Inventory\ProductCategory; 
use App\Model\Inventory\ProductUnits; 
use App\Model\Inventory\ManufacturerList; 
use App\Model\Inventory\Supplier; 
use Session;
use Input;

class ProductSetupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $productSetup = ProductList::leftjoin('inv_productcategory','inv_productlist.category_id','=','inv_productcategory.category_id')
            ->leftjoin('inv_productunits','inv_productlist.punit_id','=','inv_productunits.unit_id')
            ->leftjoin('inv_manufacturer','inv_productlist.manufact_id','=','inv_manufacturer.manufact_id')
            ->select('inv_productlist.product_id','inv_productlist.product_code','inv_productlist.product_name','inv_productlist.product_details','inv_productlist.product_notes','inv_manufacturer.manufact_name','inv_productunits.unit_name','inv_productcategory.category_name')
            ->get();

        $productList =ProductList::orderBy('id','desc')->first();
        if(empty($productList)) {
            $productID = 1;
        } else {
            $productID = $productList->id+1;
        }
        return view('modules.inventory.masterdata.product-setup.index',[

           'productSetup'     => $productSetup,
           'productCategory'  => ProductCategory::all(),
           'productUnits'     => ProductUnits::all(),
           'manufacturerList' => ManufacturerList::all(),
           'productID'        => $productID,

        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $this->validate($request, [
            'productcategory' => 'required',
            'productunit'     => 'required',
            'manufacturer'    => 'required',
            'product_name'    => 'required',
            'product_name'    => 'required',
        ]);

        $productSetupLIst = ProductList::orderBy('id','desc')->first();

        if(!empty($productSetupLIst)) {

            $productId = $productSetupLIst->product_id + 1;

        } else {

             $productId = 1;

        }

        $productSetup = new ProductList();
        $productSetup->product_id      = $productId;
        $productSetup->category_id      = \Input::get('productcategory');
        $productSetup->punit_id         = \Input::get('productunit');
        $productSetup->product_code     = \Input::get('product_code');
        $productSetup->manufact_id      = \Input::get('manufacturer');
        $productSetup->product_name     = \Input::get('product_name');
        $productSetup->product_details  = \Input::get('product_details');
        $productSetup->product_notes    = \Input::get('product_notes');

        if($productSetup->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','Product information has stored successfully');

        } else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to create new product. Please try again!.');
        }        
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $productSetup = ProductList::leftjoin('inv_productcategory','inv_productlist.category_id','=','inv_productcategory.category_id')
            ->leftjoin('inv_productunits','inv_productlist.punit_id','=','inv_productunits.unit_id')
            ->leftjoin('inv_manufacturer','inv_productlist.manufact_id','=','inv_manufacturer.manufact_id')
            ->select('inv_productlist.product_id','inv_productlist.product_name','inv_productlist.product_details','inv_productlist.product_notes','inv_manufacturer.manufact_name','inv_productunits.unit_name','inv_productcategory.category_name')
            ->get();

        return view('modules.inventory.masterdata.product-setup.show',[

           'productSetup'     => $productSetup,
           'productCategory'  => ProductCategory::all(),
           'productUnits'     => ProductUnits::all(),
           'manufacturerList' => ManufacturerList::all(),

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $productSetup = ProductList::leftjoin('inv_productcategory','inv_productlist.category_id','=','inv_productcategory.category_id')
            ->leftjoin('inv_productunits','inv_productlist.punit_id','=','inv_productunits.unit_id')
            ->leftjoin('inv_manufacturer','inv_productlist.manufact_id','=','inv_manufacturer.manufact_id')
            ->select('inv_productlist.product_id','inv_productlist.category_id','inv_productlist.punit_id','inv_productlist.manufact_id','inv_productlist.product_name','inv_productlist.product_details','inv_productlist.product_id','inv_productlist.product_code','inv_productlist.product_notes','inv_manufacturer.manufact_name','inv_productunits.unit_name','inv_productcategory.category_name')
            ->where('inv_productlist.product_id',$id)
            ->first();
            
        return view('modules.inventory.masterdata.product-setup.edit',[

           'productSetup'     => $productSetup,
           'productCategory'  => ProductCategory::all(),
           'productUnits'     => ProductUnits::all(),
           'manufacturerList' => ManufacturerList::all(),

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $this->validate($request, [
            'productcategory' => 'required',
            'productunit'     => 'required',
            'manufacturer'    => 'required',
            'product_name'    => 'required',
        ]);

     

        $productSetup =  ProductList::where('product_id',$id)->first();

        $productSetup->category_id      = \Input::get('productcategory');
        $productSetup->punit_id         = \Input::get('productunit');
        $productSetup->manufact_id      = \Input::get('manufacturer');
        $productSetup->product_name     = \Input::get('product_name');
        $productSetup->product_details  = \Input::get('product_details');
        $productSetup->product_notes    = \Input::get('product_notes');
        $productSetup->product_code     = \Input::get('product_code');
        
        if($productSetup->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','Product information has updated successfully');

        } else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to update product information. Please try again!.');


        }
       
        return redirect('new/product/setup');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req) {
        ProductList::where('product_id',$req->id)->delete();
        return response()->json();
    }

    public function supplierAddress() {

        $ID      = \Input::get('supplier_id');
        $address = Supplier::where('supplier_id',$ID)->first();

        return $address->address;
    }

    public function productCategoryShow() {

        $ID      = \Input::get('categoryId');
        $categoryProduct = ProductList::where('category_id',$ID)->get();
        
        foreach ($categoryProduct as $product) {
            echo "<option>Select Product</option><option value='$product->product_id'> $product->product_name </option>";
        }
        

    }
}
