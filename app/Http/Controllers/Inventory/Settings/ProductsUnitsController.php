<?php

namespace App\Http\Controllers\Inventory\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Inventory\ProductUnits; 
use Session;
use Input;

class ProductsUnitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modules.inventory.masterdata.product-unit.index',[
           'productUnit' => ProductUnits::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $this->validate($request, [
            'unit_name' => 'required',
            'details'  => 'required',
        ]);

        $ProductUnits = ProductUnits::orderBy('id','desc')->first();

        if(!empty($ProductUnits)) {

            $productUnitId = $ProductUnits->unit_id + 1;

        } else {

             $productUnitId = 1;

        }

        $productUnitSetup = new ProductUnits();
        $productUnitSetup->unit_id    = $productUnitId;
        $productUnitSetup->unit_name  = \Input::get('unit_name');
        $productUnitSetup->unit_notes = \Input::get('details');

        if($productUnitSetup->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','Product unit has created successfully');

        } else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to create product unit. Please try again!.');


        }
        
        return redirect('product/unit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        
        return view('modules.inventory.masterdata.product-unit.edit',[

           'productUnits' => productUnits::where('unit_id',$id)->first(),

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( $id )
    {
        
        $productUnitSetup = ProductUnits::find($id);
        $productUnitSetup->unit_name  = \Input::get('unit_name');
        $productUnitSetup->unit_notes = \Input::get('details');

        if($productUnitSetup->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','Product unit has updated successfully');

        } else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We can not update the product unit information. Please try again!.');
        }
        
         return redirect('product/unit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req) {

        ProductUnits::where('unit_id',$req->id)->delete();
        return response()->json();

    }
}
