<?php

namespace App\Http\Controllers\Inventory\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Inventory\ManufacturerList; 
use Session;
use Input;

class ManufacturerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        
        return view('modules.inventory.masterdata.manufacturer.index',[
           'manufacturer' => ManufacturerList::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
        $this->validate($request, [
            'menufacturer_name' => 'required',
            'menufacturer_date' => 'required',
            'details'           => 'required',
        ]);

        $ManufacturerList = ManufacturerList::orderBy('id','desc')->first();

        if(!empty($ManufacturerList)) {

            $ManufacturerID = $ManufacturerList->manufact_id + 1;

        } else {

             $ManufacturerID = 1;

        }

        $menufacaturerSetup = new ManufacturerList();
        $menufacaturerSetup->manufact_id      = $ManufacturerID;
        $menufacaturerSetup->manufact_name    = \Input::get('menufacturer_name');
        $menufacaturerSetup->manufact_date    = \Input::get('menufacturer_date');
        $menufacaturerSetup->manufact_details = \Input::get('details');

        if($menufacaturerSetup->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','Manufacturer has created successfully');

        } else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to create Manufacturer. Try again!.');


        }
        
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('modules.inventory.masterdata.manufacturer.view',[
           'manufacturer' => ManufacturerList::all()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        return view('modules.inventory.masterdata.manufacturer.edit',[

           'manufacturerList' => ManufacturerList::where('manufact_id',$id)->first(),

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( $id) {

        $menufacaturerSetup = ManufacturerList::where('manufact_id',$id)->first();
  
        $menufacaturerSetup->manufact_name    = \Input::get('menufacturer_name');
        $menufacaturerSetup->manufact_date    = \Input::get('menufacturer_date');
        $menufacaturerSetup->manufact_details = \Input::get('details');

        if($menufacaturerSetup->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','Manufacturer information has updated successfully');

        } else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We can not update Manufacturer information. Try again!.');


        }
        
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req) {

        $menufacaturerSetup = ManufacturerList::where('manufact_id',$req->id)->delete();
        return response()->json();

    }
}
