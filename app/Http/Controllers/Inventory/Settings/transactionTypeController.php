<?php

namespace App\Http\Controllers\Inventory\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Inventory\TransactionType; 
use Session;
use Input;
use App\Model\Inventory\StoreTransaction; 
use App\Model\Inventory\StoreDetails; 

class TransactionTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        return view('modules.inventory.masterdata.transaction-type.index',[
           'transactionType' => TransactionType::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $this->validate($request, [
            'type_name'       => 'required',
            'trnastype_notes' => 'required',
        ]);

        $transactionTypeId = TransactionType::orderBy('id','desc')->first();

        if(!empty($transactionTypeId)) {

            $typeID = $transactionTypeId->type_id + 1;

        } else {

             $typeID = 1;

        }

        $transactionSetup = new TransactionType();
        $transactionSetup->type_id         = $typeID;
        $transactionSetup->type_name       = \Input::get('type_name');
        $transactionSetup->trnastype_notes = \Input::get('trnastype_notes');

        if($transactionSetup->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','Transaction type has created successfully');

        } else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to create transaction type. Please try again!.');
        }
        
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('modules.inventory.masterdata.transaction-type.edit',[
           'transactionType' => TransactionType::where('type_id',$id)->first(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $this->validate($request, [
            'type_name'       => 'required',
            'trnastype_notes' => 'required',
        ]);


        $transactionSetup = TransactionType::where('type_id',$id)->first();
        $transactionSetup->type_name       = \Input::get('type_name');
        $transactionSetup->trnastype_notes = \Input::get('trnastype_notes');

        if($transactionSetup->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','Transaction type has created successfully');

        } else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to create transaction type. Please try again!.');
        }
        
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req) {
        
        TransactionType::where('type_id',$req->id)->delete();
        return response()->json();
    }

    public function masterTransection() {
        return view('modules.inventory.master-transection.index',[
           'transactionType' => TransactionType::all(),
        ]);
    }

    public function masterSummaryReports() {

        return view('modules.inventory.master-transection.master-summary-report',[
           'transactionType' => TransactionType::all(),
        ]);
    }

    public function masterTransectionSearchType() {
      
        $type     = \Input::get('transactionType');
        $datefrom = \Input::get('datefrom');
        $dateto   = \Input::get('dateto');

        $stores   = StoreDetails::all();
       

        if(!empty($datefrom) && !empty($dateto)) {

            $transactions = StoreTransaction::leftjoin('inv_storesdetails','inv_storetransaction.transaction_id','=','inv_storesdetails.transaction_id')
                ->leftjoin('inv_productlist','inv_storesdetails.product_id','=','inv_productlist.product_id')
                ->leftjoin('employees','employees.employeeID','=','inv_storetransaction.received_by')
                ->select('inv_productlist.product_name','inv_productlist.product_id','inv_storesdetails.unit_price','inv_storesdetails.quantity','inv_storesdetails.total_price','inv_storesdetails.approval_date','inv_storesdetails.warranty_period','inv_storesdetails.comments','inv_storetransaction.received_by','employees.fullName','inv_storetransaction.received_by','inv_storetransaction.received_date','inv_storesdetails.created_at','inv_storesdetails.updated_at','inv_storesdetails.id as transactionID','inv_storesdetails.approval_status')
                ->where('inv_storetransaction.transaction_type', $type)
                ->whereBetween('inv_storetransaction.received_date', [$datefrom,$dateto])
                ->orderBy('inv_storetransaction.created_at','desc')
                ->get();
        } else {
            $transactions = StoreTransaction::leftjoin('inv_storesdetails','inv_storetransaction.transaction_id','=','inv_storesdetails.transaction_id')
                ->leftjoin('inv_productlist','inv_storesdetails.product_id','=','inv_productlist.product_id')
                ->leftjoin('employees','employees.employeeID','=','inv_storetransaction.received_by')
                ->select('inv_productlist.product_name','inv_productlist.product_id','inv_storesdetails.unit_price','inv_storesdetails.quantity','inv_storesdetails.total_price','inv_storesdetails.warranty_period','inv_storesdetails.comments','inv_storesdetails.approval_date','inv_storesdetails.created_at','employees.fullName','inv_storetransaction.received_by','inv_storetransaction.received_date','inv_storesdetails.updated_at','inv_storesdetails.id as transactionID','inv_storesdetails.approval_status')
                ->where('inv_storetransaction.transaction_type', $type)
                ->orderBy('inv_storetransaction.received_date','desc')
                ->get(); 
        }

        foreach($transactions as $key=>$str) {
            $store = StoreDetails::find($str->id);
        }

        return view('modules.inventory.master-transection.master-transaction-search',[
           'transactions' => $transactions
        ]);
    }

    public function masterSummaryReportsSearch() {
      
        $type     = \Input::get('transactionType');
        $datefrom = \Input::get('datefrom');
        $dateto   = \Input::get('dateto');
        $stores   = StoreDetails::all();
       

        if(!empty($datefrom) && !empty($dateto)) {

            $transactions = StoreTransaction::join('inv_storesdetails','inv_storetransaction.transaction_id','=','inv_storesdetails.transaction_id')
                ->join('inv_productlist','inv_storesdetails.product_id','=','inv_productlist.product_id')
                ->join('employees','employees.employeeID','=','inv_storetransaction.received_by')
                ->select('inv_productlist.product_name','inv_productlist.product_id','inv_storesdetails.unit_price','inv_storesdetails.quantity','inv_storesdetails.total_price','inv_storesdetails.warranty_period','inv_storesdetails.comments','inv_storesdetails.updated_at','inv_storesdetails.created_at','employees.fullName','inv_storetransaction.received_by','inv_storetransaction.received_date','inv_storesdetails.updated_at','inv_storesdetails.id as transactionID','inv_storesdetails.approval_status')
                ->where('inv_storetransaction.transaction_type', $type)
                ->whereBetween('inv_storetransaction.received_date', [$datefrom,$dateto])
                ->orderBy('inv_storetransaction.received_date','desc')
                ->get();

        } else {

            $transactions = StoreTransaction::leftjoin('inv_storesdetails','inv_storetransaction.transaction_id','=','inv_storesdetails.transaction_id')
                ->leftjoin('inv_productlist','inv_storesdetails.product_id','=','inv_productlist.product_id')
                ->leftjoin('employees','employees.employeeID','=','inv_storetransaction.received_by')
                ->select('inv_productlist.product_name','inv_productlist.product_id','inv_storesdetails.unit_price','inv_storesdetails.quantity','inv_storesdetails.total_price','inv_storesdetails.warranty_period','inv_storesdetails.comments','inv_storesdetails.updated_at','inv_storesdetails.created_at','employees.fullName','inv_storetransaction.received_by','inv_storetransaction.received_date','inv_storesdetails.updated_at','inv_storesdetails.id as transactionID','inv_storesdetails.approval_status')
                ->where('inv_storetransaction.transaction_type', $type)
                ->orderBy('inv_storetransaction.received_date','desc')
                ->get(); 
        }

        foreach($transactions as $key=>$str) {
            $store = StoreDetails::find($str->id);
        }

        return view('modules.inventory.master-transection.master-summary-report-search',[
           'transactions' => $transactions
        ]);
    }

}
