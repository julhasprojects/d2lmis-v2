<?php

namespace App\Http\Controllers\Inventory\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Inventory\ProductCategory; 
use Session;
use Input;


class productCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modules.inventory.masterdata.product-category.index',[
           'ProductCategory' => ProductCategory::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $this->validate($request, [
            'category_name'   => 'required',
            'category_notes' => 'required',
        ]);

        $productCategory = ProductCategory::orderBy('id','desc')->first();

        if(!empty($productCategory)) {

            $categoryID = $productCategory->category_id + 1;

        } else {

             $categoryID = 1;

        }

        $productCategorySetup = new ProductCategory();
        $productCategorySetup->category_id    = $categoryID;
        $productCategorySetup->category_name  = \Input::get('category_name');
        $productCategorySetup->category_notes = \Input::get('category_notes');

        if($productCategorySetup->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','Product category has created successfully');

        } else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to create product category. Please try again!.');


        }
        
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
        return view('modules.inventory.masterdata.product-category.show',[
           'ProductCategory' => ProductCategory::all()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('modules.inventory.masterdata.product-category.edit',[

           'productCategory' => ProductCategory::where('category_id',$id)->first(),

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id) {
        
  
        $productCategorySetup = ProductCategory::where('category_id',$id)->first();
        $productCategorySetup->category_name  = \Input::get('category_name');
        $productCategorySetup->category_notes = \Input::get('category_notes');

        if($productCategorySetup->save()) {

            Session::put( 'valid', 1 );
            Session::flash('message','Product category has created successfully');

        } else {

            Session::put( 'invalid', 2 );
            Session::flash('message','We are unable to create product category. Pease try again!.');
        }
        
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        ProductCategory::where('category_id',$req->id)->delete();
        return response()->json();
    }
}
