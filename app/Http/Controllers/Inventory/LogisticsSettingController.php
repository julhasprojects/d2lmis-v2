<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Inventory\LogisticsSetting;
use Input; 

class LogisticsSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        return view('modules.inventory.logistics.index',[
            'logisticsDetails' => LogisticsSetting::all()
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
        $this->validate($request, [
            'menufacturer_name' => 'required',
            'menufacturer_date' => 'required',
        ]);
        
        $input = Input::all();

        $logisticsDetails = new LogisticsSetting();

        $logisticsDetails->type         = $input['type'];
        $logisticsDetails->name         = ucfirst(strtolower($input['name']));
        $logisticsDetails->strength     = $input['strength'];
        $logisticsDetails->generic_name = $input['generic_name'];
        $logisticsDetails->manufacturer = $input['manufacturer'];
        $logisticsDetails->description  = $input['description'];
        $logisticsDetails->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)  {
        
        return view('modules.inventory.logistics.edit',[
            'logisticsDetails' => LogisticsSetting::find($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->validate($request, [
            'type'         => 'required',
            'name'         => 'required',
            'generic_name' => 'required',
            'manufacturer' => 'required',
            'received_by'  => 'required',
        ]);

        $input = Input::all();

        $logisticsDetails =  LogisticsSetting::find($id);

        $logisticsDetails->type         = $input['type'];
        $logisticsDetails->name         = ucfirst(strtolower($input['name']));
        $logisticsDetails->strength     = $input['strength'];
        $logisticsDetails->generic_name = $input['generic_name'];
        $logisticsDetails->manufacturer = $input['manufacturer'];
        $logisticsDetails->description  = $input['description'];
        $logisticsDetails->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)  {
        LogisticsSetting::find($id)->delete();
        return back();
    }
}
