<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Inventory\MasterFacilities; 
use App\Model\Inventory\UpazilaList; 
use App\Model\Inventory\UnionList; 
use App\Model\Inventory\DistrictList; 
use App\Model\Inventory\DivisionList; 
use Input;
use Auth;
use DB;
use Session;

class FacilityInformationManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function masterFacilityList()
    {
        return view('modules.inventory.masterdata.facilityinfo.master-facility',[
            'masterFacilities' => MasterFacilities::all(),
        ]);
    }

    public function unionList()
    {
        return view('modules.inventory.masterdata.facilityinfo.union-list',[
            'unionList' => UnionList::all(),
        ]);
    }
    public function upazilaList()
    {
        return view('modules.inventory.masterdata.facilityinfo.upazila-list',[
            'upazilaList' => UpazilaList::all(),
        ]);
    }

    public function districtList()
    {
        return view('modules.inventory.masterdata.facilityinfo.district-list',[
            'districtList' => DistrictList::all(),
        ]);
    }

    public function divisionList()
    {
        return view('modules.inventory.masterdata.facilityinfo.division-list',[
            'divisionList' => DivisionList::all(),
        ]);
    }
}
