<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
 
use Session;
 
//Includes WebClientPrint classes

use Neodynamic\SDK\Web\WebClientPrint;
 
 
class HomeController extends Controller
{
    public function index(){
 
        $wcppScript = WebClientPrint::createWcppDetectionScript(action('WebClientPrintController@processRequest'), Session::getId());    
 
        return view('home.index', ['wcppScript' => $wcppScript]);
    }
 
    public function printESCPOS(){
        return view('frontend.printESCPOS');
    }
}