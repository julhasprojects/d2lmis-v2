<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;

class InActiveDefaulterUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'InActiveDefaulterUsers:inactiveDefaulterUsers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Inactive defaulter users.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = [

            "username"      => "The Net Heads",
            
        ];
        $clientInfo = 100;

        Mail::send(['text'=> 'emails.welcome'], $data, function($message) use ($clientInfo) {

            $message->to('ratankumarppi86@gmail.com')->subject("Defaulter user inactive from mikrotik");
            // $message->attach(public_path() . '/WelcomeToTETRASOFT.pdf');
            $message->from( "support@jbrsoft.com" );
        });

        var_dump('Hi Rasel');
        // DB::table('users')->delete(4);
    }
}
