<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class RolePermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \App\Role::truncate();
        \App\Permission::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        \App\Role::insert([
            ['name'=> 'Admin','slug'=>'admin', 'description'=>'For Admin User','level'=>'', 'created_at'=>'2016-04-26 06:47:49','updated_at'=>'2016-04-26 06:47:49'],
            ['name'=> 'Doctor','slug'=>'doctor', 'description'=>'For Doctors','level'=>'', 'created_at'=>'2016-04-26 06:47:49','updated_at'=>'2016-04-26 06:47:49'],
            ['name'=> 'Nurse','slug'=>'nurse', 'description'=>'For Nurses','level'=>'', 'created_at'=>'2016-04-26 06:47:49','updated_at'=>'2016-04-26 06:47:49'],
            ['name'=> 'Reception','slug'=>'reception', 'description'=>'For Reception','level'=>'', 'created_at'=>'2016-04-26 06:47:49','updated_at'=>'2016-04-26 06:47:49'],
            ['name'=> 'Store Keeper','slug'=>'store.keeper', 'description'=>'For Store Keeper','level'=>'', 'created_at'=>'2016-04-26 06:47:49','updated_at'=>'2016-04-26 06:47:49'],
            ['name'=> 'RMO','slug'=>'rmo', 'description'=>'For RMO','level'=>'', 'created_at'=>'2016-04-26 06:47:49','updated_at'=>'2016-04-26 06:47:49']
        ]);


        \App\Permission::insert([
            ['name'=> 'Patient Access','slug'=>'patient.access', 'description'=>'For Patient Access', 'created_at'=>'2016-04-26 06:47:49','updated_at'=>'2016-04-26 06:47:49'],
            ['name'=> 'Follow-up Access','slug'=>'follow.up.access', 'description'=>'For Follow-up Access', 'created_at'=>'2016-04-26 06:47:49','updated_at'=>'2016-04-26 06:47:49'],
            ['name'=> 'Billing Access','slug'=>'billing.access', 'description'=>'For Billing Access', 'created_at'=>'2016-04-26 06:47:49','updated_at'=>'2016-04-26 06:47:49'],
            ['name'=> 'Report Access','slug'=>'report.access', 'description'=>'For Report Access', 'created_at'=>'2016-04-26 06:47:49','updated_at'=>'2016-04-26 06:47:49'],
            ['name'=> 'Indents Access','slug'=>'indents.access', 'description'=>'For Indents Access', 'created_at'=>'2016-04-26 06:47:49','updated_at'=>'2016-04-26 06:47:49'],
            ['name'=> 'Store Access','slug'=>'store.access', 'description'=>'For Store Access', 'created_at'=>'2016-04-26 06:47:49','updated_at'=>'2016-04-26 06:47:49'],
            ['name'=> 'Employee Access','slug'=>'employee.access', 'description'=>'For Employee Access', 'created_at'=>'2016-04-26 06:47:49','updated_at'=>'2016-04-26 06:47:49'],
            ['name'=> 'Setting Access','slug'=>'setting.access', 'description'=>'For Setting Access', 'created_at'=>'2016-04-26 06:47:49','updated_at'=>'2016-04-26 06:47:49'],
            ['name'=> 'Indent Approval Access','slug'=>'indent.approval.access', 'description'=>'For Indent Approval Access', 'created_at'=>'2016-04-26 06:47:49','updated_at'=>'2016-04-26 06:47:49'],
            ['name'=> 'Store Approval Access','slug'=>'store.approval.access', 'description'=>'For Store Approval Access', 'created_at'=>'2016-04-26 06:47:49','updated_at'=>'2016-04-26 06:47:49']
        ]);
    }
}
